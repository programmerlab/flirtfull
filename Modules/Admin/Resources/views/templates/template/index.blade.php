@extends('admin::layouts.master')
 
    @section('content') 
      @include('admin::partials.navigation')
      @include('admin::partials.breadcrumb')   

       @include('admin::partials.sidebar')  
          <div class="panel panel-white"> 
                <div class="panel panel-flat">
                  <div class="panel-heading">
                    <h6 class="panel-title"><b> {{$heading }} </b><a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
                    
                  </div> 
                </div>  
                <div class="panel-body"> 
                 
              </div> 
                 @if(Session::has('flash_alert_notice'))
                     <div class="alert alert-success alert-dismissable" style="margin:10px">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                      <i class="icon fa fa-check"></i>  
                     {{ Session::get('flash_alert_notice') }} 
                     </div>
                @endif
                 <div class="table-responsive">
                  <table class="table datatable-basic table-bordered table-hover table-responsive" id="roles_list">
                                   
                                        <thead>
                                            <tr>
                                                <th> S. no. </th>
                                                <th> Template Name </th>
                                                <th> Emasil Subject </th>
                                                <th>Action</th> 
                                            </tr>
                                        </thead>
                                        <tbody>

                                    
                                        @foreach($template as $key => $result)
                                            <tr>
                                                 <td> {{ (++$key) }}</td>
                                                <td> {{$result->template_name}} </td>
                                                <td> {{$result->email_subject}} </td>
                                                  
                                                
                                                <td> 
                                                    <a href="{{ route('template.edit',$result->id)}}" class="btn btn-primary btn-xs" style="margin: 3px">
                                                    <i class="icon-pencil7" title="edit"></i>  
                                                    </a> 
                                                </td>
                                               
                                            </tr>
                                           @endforeach
                                        
                                        </tbody>
                                    </table>
                                    
                                
                 </div> 
               </div>
         </div> 
   @stop
   
   
   