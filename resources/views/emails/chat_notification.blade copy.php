<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="image/jpeg; charset=utf-8" />
<title>Your Chat Notification</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
<link href="https://fonts.googleapis.com/css?family=Arizonia|Cookie|Montserrat:100,200,300,400,500,600,700,800,900|Poppins:100,200,300,400,500,600,700,800,900" rel="stylesheet"> 
<link href="https://fonts.googleapis.com/css?family=Oswald" rel="stylesheet">   
<style type="text/css">
  body{font-family: 'Montserrat', sans-serif; margin:0; font-size:19px;}
  p{ line-height:34px; color:#888}
  p span{color:#000;}
  table{ width:100%;}
  h6{ margin:15px 0}
</style>
</head>
<body>
<div style="width:700px;margin:0 auto;background:url({{ url('public/images/bg1.jpg') }}) no-repeat; background-size:cover; padding:50px ">
  <div style="width:90%; background:#fff; padding:5%; position:relative">
    <table>
      <tr>
      <td colspan="3">
        <h4 style=" text-align:center; font-weight:300; font-size:25px; margin:0 0 15px; font-weight: 400;">You have received a  chat notification </h4>
      </td>
      </tr>
      <tr>
      <td colspan="3">
        <h1 style="text-align:center; font-family: 'Arizonia', cursive; font-size:80px; color:#EC155A; text-transform:capitalize; margin-top:0; margin-bottom: 25px; border-bottom: 1px solid #ccc; padding-bottom: 15px;">
        <img src="{{ url('public/images/logo.png') }}" style="max-width: 250px;">
        </h1>
      </td>
      </tr>
      <tr>
      <td colspan="3">
       <h3 style="font-size: 21px;font-weight: 500; margin-top: 0; color:#df2258;">Dear {{$content['first_name']??'User'}}, {{$content['sender_name']}} has sent you new email.</h3>
      </td>
      </tr>
      <tr>
      <td width="200px">
      <img src="{{ url('public/images/profile_image') }}/{{$content['profile_image']}}" alt="" style="max-width:100%;max-height: 200px;max-width: 200px;">
      </td>
      <td width="10px">
      
      </td>
      <td>
      <p style="margin-top: 0; margin-bottom:10px; font-size:14px; line-height:26px;">{{$content['message']}} </p>
      </td>
     </tr>
     <tr>
      <td colspan="3">
        <h4 style="margin: 10px 0 0;">Regards</h4>
      </td>
     </tr>
     <tr>
       <td colspan="3">
        <p style="margin-top: 0;margin-bottom: 0; font-size:15px;">Team <a href="{{ url('/') }}" style="color:#df2258;">Flirtfull.com</a></p>
       </td>
     </tr>
    </table>
  </div>
</div>

      
        

</body>
</html>
