$(document).ready(function () {
    window.registration = {
        init: function () {
            registration.events();
        },
        events: function () {
            $('.reg_btn_1').on('click', registration.reg_1);
            $('.reg_btn_2').on('click', registration.reg_2);
        },
        reg_1: function () {
            var $valid = $("#register").valid();
            if (!$valid) {
                return false;
            }
            // reglogproc
            $('.answer .reg-2').off('click', landing.reg_2);
            var regBut = $(this);
            regBut.css('opacity', '.7');
            regBut.closest('.step').find('.text').css('opacity', '.7');
            regBut.closest('.step').find('.heading').css('opacity', '.7');
            $('.regProc').show();
            var dotsCount = 0;
            var loadingProc = setInterval(
                function () {
                    var dots = '';
                    dotsCount += 1;
                    if (dotsCount > 3) {
                        dotsCount = 0;
                    }
                    for (var i = 0; i < dotsCount; i++) {
                        dots += '.';
                    }
                    $('.regProc span').html(dots);
                }, 300
            );
            $.post(
                "/" + window.base_path + "/includes/reg.php",
                {email: $('#email').val()},
                function (data) {
                    $('#uname').val(data.u);
                    $('#register').ajaxSubmit({
                        dataType: 'json',
                        success: tryRegister
                    });
                }
            );
        },
        reg_2: function () {
            var $valid = $("#register").valid();
            if (!$valid) {
                return false;
            }
            // reglogproc
            var regBut = $(this);
            regBut.prop('disabled', true);
            regBut.css('opacity', '.7');
            $('#step5 .form-group').css('opacity', '.7');
            $('.regProc').show();
            var dotsCount = 0;
            var loadingProc = setInterval(
                function () {
                    var dots = '';
                    dotsCount += 1;
                    if (dotsCount > 3) {
                        dotsCount = 0;
                    }
                    for (var i = 0; i < dotsCount; i++) {
                        dots += '.';
                    }
                    $('.regProc span').html(dots);
                }, 300
            );
            $('#register').ajaxSubmit({
                dataType: 'json',
                success: tryRegister
            });
        }
    };
    registration.init();
});