 // create chat room
 function createChatRoom(){
        
        var room_id = $('#room_id').val();
        if(room_id != ''){
            firebase.database().ref('chat_rooms/' + room_id).off("child_added");
        }
        
        var uid = $('#receiver_id').val();
        var chatRoom = adminId + '_' + uid;
        if(uid < adminId){
          chatRoom = uid + '_' + adminId;
        }
        
        
        firebase.database().ref('chat_rooms/' + chatRoom).off("child_added");
        
        $('#room_id').val(chatRoom);
        
        
        <?php if(isset($users) && ($users->fake)){ ?>
            var sendrimgurl = '{{ env('PROFILE_MANAGER_ADDR').('/public/thumbnail/') }}/';
        <?php }else{ ?>
            var sendrimgurl = '{{ asset('/public/thumbnail/') }}/';
        <?php } ?>
        
        firebase.database().ref('chat_rooms/' + chatRoom).on('child_added', function(chat){
            var oneMsgVal = chat.val();
            console.log(oneMsgVal);
            if(oneMsgVal.uid == adminId){
                var liposi = 'right';
            }else{
                var liposi = 'left';
            }
            
            $('#removeLi').remove();
            
            if(oneMsgVal.messageType != 'file'){
                $('#messages').append(''+ 
                    '<li class="message">'+                    
                        '<div class="chat-user-'+liposi+'">'+
                        '<div class="chat-user-box">'+
                        '<img src="'+sendrimgurl+oneMsgVal.profilePic+'">'+
                        '<span>'+oneMsgVal.name+'</span>'+
                        '</div>'+
                        '<div class="chat-user-message">'+
                        '<div class="chat-line"><span>'+oneMsgVal.message+'</span></div>'+                                   
                        '<div class="meta-info"><span class="date">'+oneMsgVal.timestamp+'</span></div>'+
                        '</div>'+
                        '</div>'+
                    '</li>'+
                '');
            }else{
                $('#messages').append(''+
                    '<li class="message">'+                    
                    '<div class="chat-user-'+liposi+'">'+
                    '<div class="chat-user-box">'+
                    '<img src="'+sendrimgurl+oneMsgVal.profilePic+'">'+
                    '<span>'+oneMsgVal.name+'</span>'+
                    '</div>'+
                    '<div class="chat-user-message">'+
                    '<div class="chat-line"><span>'+
                    '<img src=' + oneMsgVal.message + ' alt="Ovengo..." height="100" width="100">'+
                    '</span></div>'+                                   
                    '<div class="meta-info"><span class="date">'+oneMsgVal.timestamp+'</span></div>'+
                    '</div>'+
                    '</div>'+
                '</li>'+
                '');
            }
            
            var msgDiv = document.getElementById('msgDiv');
        msgDiv.scrollTop = msgDiv.scrollHeight;
            
            //scrollToBottom();
        });
    }
    
    //Send message function
    function sendMsg(){
    
    
        var msgText1 = CKEDITOR.instances.message.getData();
    
        if(msgText1 == ''){
            return true;
        }
    
    
        var mycredits = jQuery("#initialcredit").val();//localStorage.getItem("credits");
            var mypremiums = jQuery("#initialpremium").val();
            if(mycredits == 'null'){ mycredits = 0;}
            var requiredCredits = 0;
            
            // for text required credit will be 1
            var type = (jQuery("#file").val() == '') ? 'text' : 'file';
            if(type === "text"){
                requiredCredits = 1;
            }
            
            var planType = jQuery("#planType").val();
            if(planType === 'credits'){ requiredCredits = jQuery("#points").val(); }
            
            if(planType === 'diamonds'){ requiredCredits = jQuery("#points").val(); }     
            
            
            if(planType === 'diamonds'){
                $.ajax({
                    headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '<?php echo url("/")?>/update_diamonds',
                    type: 'POST',
                    cache : false,
                    processData: false,
                    data: 'room_id='+jQuery('#room_id').val()+'&amt='+requiredCredits+'&receiver_id='+jQuery('#receiver_id').val(), // serializes the form's elements.
                    success: function(data)
                    {
                     console.log(data);
                     if(data){
                        var d = JSON.parse(data);
                        if(d.credits){
                            jQuery("#initialcredit").val(d.credits);
                            jQuery("#updatedwallet").html(d.credits);
                            jQuery("#updateddiamonds").html(d.diamonds);   
                        }else{                            
                            jQuery("#updateddiamonds").html(d.diamonds);   
                        }                        
                      }                      
                    }
                });
            }else{
                $.ajax({
                    headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '<?php echo url("/")?>/update_conversation',
                    type: 'POST',
                    cache : false,
                    processData: false,
                    data: 'room_id='+jQuery('#room_id').val()+'&amt='+requiredCredits+'&receiver_id='+jQuery('#receiver_id').val(), // serializes the form's elements.
                    success: function(data)
                    {
                     console.log(data);
                      if(data){
                        jQuery("#initialcredit").val(data);
                        jQuery("#updatedwallet").html(data);
                      }
                    }
                });    
            }
            
    
    
    
    
    
        var chat_send_image = $('#chat_send_image').val();
        var messageType = 'text';
        if(chat_send_image != ''){
            messageType = 'text';
           // CKEDITOR.instances.message.setData(chat_send_image);
        }
    
        var userId    = $('#receiver_id').val();
        var userName  = $('#receiver_name').val();
        var userImage = $('#receiver_image').val();
        var msgText = CKEDITOR.instances.message.getData();
    
       
    
        CKEDITOR.instances.message.setData('');
    
        var chatRoom = $('#room_id').val();
    
        firebase.database().ref('/users/'+userId).once('value').then(function(snapshot) {
          var childData = snapshot.val();
          
          firebase.database().ref().child('chat_history/'+adminId).child(userId).once('value').then(function(snapshot1) {
    
            var snapshot1Data = snapshot1.val();
    
            var msgCount = 0;
            var hisType = 1;
            if (snapshot1Data != null){
              var msgCount = Number(snapshot1Data.messageCount)+1;
              if(snapshot1Data.historyType == 1){
                if(snapshot1Data.lastSenderId != adminId){
                  hisType = 2;
                }
              }else{
                hisType = 2;
              }
            }
    
            var insertData = {
              deletteby : '',
              message : msgText,
              messageCount : msgCount,
              name : userName,
              profilePic : userImage,
              timestamp : $.now(),
              uid : userId.toString(),
              historyType : hisType,
              lastSenderId : adminId.toString(),
              messageType : messageType,
              historyLoad : '0'
            };
          if(parseInt(mycredits) >= parseInt(requiredCredits) || (mypremiums >= '{{ time() }}')){
            firebase.database().ref().child('chat_history/'+adminId).child(userId).set(insertData);
          }
          });
        });
    
        firebase.database().ref('/users/'+adminId).once('value').then(function(snapshot) {
          var childData = snapshot.val();
    
          firebase.database().ref().child('chat_history/'+userId).child(adminId).once('value').then(function(snapshot2) {
    
            var snapshot2Data = snapshot2.val();
    
            var msgCount = 0;
            var hisType = 1;
            if (snapshot2Data != null){
              var msgCount = Number(snapshot2Data.messageCount)+1;
              if(snapshot2Data.historyType == 1){
                if(snapshot2Data.lastSenderId != adminId){
                  hisType = 2;
                }
              }else{
                hisType = 2;
              }
            }
    
            var insertData = {
              deletteby : '',
              message : msgText,
              messageCount : msgCount,
              name : childData.userName,
              profilePic : childData.userImage,
              timestamp : $.now(),
              uid : adminId.toString(),
              historyType : hisType,
              lastSenderId : adminId.toString(),
              messageType : messageType,
              historyLoad : '0'
            };
            if(parseInt(mycredits) >= parseInt(requiredCredits) || (mypremiums >= '{{ time() }}')){
                  firebase.database().ref().child('chat_history/'+userId).child(adminId).set(insertData);
            }
          });
        });
    
        firebase.database().ref('/users/'+adminId).once('value').then(function(snapshot) {  
          var childData = snapshot.val();
          var insertData = {
            deleteby:'',
            message : msgText,
            messageCount: 1,
            name : childData.userName,
            profilePic : childData.userImage,
            timestamp : $.now(),
            uid : adminId.toString(),
            messageType : messageType,
          };
          //firebase.database().ref().child('chat_rooms/').child(chatRoom).push(insertData);
          if(parseInt(mycredits) >= parseInt(requiredCredits) || (mypremiums >= '{{ time() }}')){
            firebase.database().ref().child('chat_rooms/').child(chatRoom).push(insertData);
            }else{
            //message failed
            $('#messages').append(''+ 
                        '<li class="message">'+                    
                        '<div class="chat-user-right">'+
                        '<div class="chat-user-box">'+
                        '<img src="{{ asset('/public/thumbnail/') }}/'+jQuery('#sender_image').val()+'">'+
                        '<span>'+jQuery('#sender_name').val()+'</span>'+
                        '</div>'+
                        '<div class="chat-user-message">'+
                        '<div class="chat-line"><span>'+msgText+'</span></div>'+                                   
                        '<div class="meta-info"><span class="date">'+$.now()+'</span></div>'+
                        '<div class="meta-info red"><span class="date">Message Sent Failed!!</span></div>'+
                        '</div>'+
                        '</div>'+
                        '</li>'+
            '');
            setTimeout(function(){ jQuery('.credit_btn').click(); }, 3000); 
            }
        });
      }
      
// send message from profile      
  function sendMsg1(proposal=null){
            var mycredits = jQuery("#initialcredit").val();//localStorage.getItem("credits");
            var mypremiums = jQuery("#initialpremium").val();
            if(mycredits == 'null'){ mycredits = 0;}
            var requiredCredits = 0;
            
            // for text required credit will be 1
            var type = (jQuery("#file").val() == '') ? 'text' : 'file';
            if(type === "text"){
                requiredCredits = 1;
            }
            
            var planType = jQuery("#planType").val();
            if(planType === 'credits'){ requiredCredits = jQuery("#points").val(); }
            
            if(planType === 'diamonds'){
                    requiredCredits = 1;//jQuery("#points").val();
            }
            
            //chatNowtext chatNowuserId
            if(proposal){
                var msgText1 = '<img src="'+proposal+'" width="100">';
                var planType = 'diamonds';
                if(proposal == 'likes'){
                    planType = '';
                }
                if(proposal == 'favorites'){
                    planType = '';
                }
            }else{
                
                var msgText1 = ($('#chatNowtext1').val()) ? $('#chatNowtext1').val() : $('#chatNowtext').val() ; 
            }
            //alert(msgText1);
                if(msgText1 == ''){
                    return true;
                }

            
            
            
    
        localStorage.setItem("chatTab", "Outbox");
    
    
    
        var chat_send_image = $('#chat_send_image').val();
        var messageType = 'text';
        if(chat_send_image != ''){
            messageType = 'text';
           // CKEDITOR.instances.message.setData(chat_send_image);
        }
    
        var userId    = $('#chatNowuserId').val();
        var userName  = $('#chatNowuserName').val();
        var userImage = $('#chatNowuserImage').val();
        
        if(proposal){
            var msgText = '<img src="'+proposal+'">';
            if(proposal === 'likes'){                
                var msgText = 'Hi '+userName+', i like your profile. Check mine ans see if we are a match! Lets connect';
                messageType = 'likes';
            }
            if(proposal === 'favorites'){            
                var msgText = 'Hi '+userName+' I have added you to my favorite list. I hope you like my profile to! Lets get this rolling';
                messageType = 'favorite';
            }
        }else{
            var msgText = ($('#chatNowtext1').val()) ? $('#chatNowtext1').val() : $('#chatNowtext').val() ;
            requiredCredits = 1;
        }        
        //alert(msgText);
       
    
        $('#chatNowtext1').val('');
        
        
        
        var room_id = $('#room_id').val();
        if(room_id != ''){
            firebase.database().ref('chat_rooms/' + room_id).off("child_added");
        }
        
        var uid = $('#chatNowuserId').val();
        var chatRoom = adminId + '_' + uid;
        if(uid < adminId){
          chatRoom = uid + '_' + adminId;
        }
        
        
        firebase.database().ref('chat_rooms/' + chatRoom).off("child_added");
        
        $('#room_id').val(chatRoom);
        
        
        ////////////*******************888
        //alert(planType);
        
            if(planType === 'diamonds'){
            requiredCredits = 1;
                $.ajax({
                    headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '<?php echo url("/")?>/update_diamonds',
                    type: 'POST',
                    cache : false,
                    processData: false,
                    data: 'room_id='+jQuery('#room_id').val()+'&amt=1&receiver_id='+userId, // serializes the form's elements.
                    success: function(data)
                    {
                     console.log(data);
                     if(data){
                        var d = JSON.parse(data);
                        if(d.credits){
                            jQuery("#initialcredit").val(d.credits);
                            jQuery("#updatedwallet").html(d.credits);
                            jQuery("#updateddiamonds").html(d.diamonds);   
                        }else{                            
                            jQuery("#updateddiamonds").html(d.diamonds);   
                        }                        
                      }                      
                    }
                });
            }else{
                $.ajax({
                    headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '<?php echo url("/")?>/update_conversation',
                    type: 'POST',
                    cache : false,
                    processData: false,
                    data: 'room_id='+jQuery('#room_id').val()+'&amt='+requiredCredits+'&receiver_id='+userId, // serializes the form's elements.
                    success: function(data)
                    {
                     console.log(data);
                      if(data){
                        jQuery("#initialcredit").val(data);
                        jQuery("#updatedwallet").html(data);
                      }
                    }
                });    
            }
        //*******************************
    
        var chatRoom = $('#room_id').val();
        $('.alreadyChat').hide();
        firebase.database().ref('/users/'+userId).once('value').then(function(snapshot) {
          var childData = snapshot.val();
          
          firebase.database().ref().child('chat_history/'+adminId).child(userId).once('value').then(function(snapshot1) {
    
            var snapshot1Data = snapshot1.val();
    
            var msgCount = 0;
            var hisType = 1;
            if (snapshot1Data != null){
              var msgCount = Number(snapshot1Data.messageCount)+1;
              if(snapshot1Data.historyType == 1){
                if(snapshot1Data.lastSenderId != adminId){
                  hisType = 2;
                }
              }else{
                hisType = 2;
              }
            }
    
            var insertData = {
              deletteby : '',
              message : msgText,
              messageCount : msgCount,
              name : userName,
              profilePic : userImage,
              timestamp : $.now(),
              uid : userId.toString(),
              historyType : hisType,
              lastSenderId : adminId.toString(),
              messageType : messageType,
              historyLoad : '0'
            };
          if(parseInt(mycredits) >= parseInt(requiredCredits) || (mypremiums >= '{{ time() }}')){
            firebase.database().ref().child('chat_history/'+adminId).child(userId).set(insertData);
          }
          });
        });
    
        firebase.database().ref('/users/'+adminId).once('value').then(function(snapshot) {
          var childData = snapshot.val();
    
          firebase.database().ref().child('chat_history/'+userId).child(adminId).once('value').then(function(snapshot2) {
    
            var snapshot2Data = snapshot2.val();
    
            var msgCount = 0;
            var hisType = 1;
            if (snapshot2Data != null){
              var msgCount = Number(snapshot2Data.messageCount)+1;
              if(snapshot2Data.historyType == 1){
                if(snapshot2Data.lastSenderId != adminId){
                  hisType = 2;
                }
              }else{
                hisType = 2;
              }
            }
    
            var insertData = {
              deletteby : '',
              message : msgText,
              messageCount : msgCount,
              name : childData.userName,
              profilePic : childData.userImage,
              timestamp : $.now(),
              uid : adminId.toString(),
              historyType : hisType,
              lastSenderId : adminId.toString(),
              messageType : messageType,
              historyLoad : '0'
            };
        if(parseInt(mycredits) >= parseInt(requiredCredits) || (mypremiums >= '{{ time() }}')){
              firebase.database().ref().child('chat_history/'+userId).child(adminId).set(insertData);
        }
          });
        });
    
        firebase.database().ref('/users/'+adminId).once('value').then(function(snapshot) {  
          var childData = snapshot.val();
          var insertData = {
            deleteby:'',
            message : msgText,
            messageCount: 1,
            name : childData.userName,
            profilePic : childData.userImage,
            timestamp : $.now(),
            uid : adminId.toString(),
            messageType : messageType,
          };
          //firebase.database().ref().child('chat_rooms/').child(chatRoom).push(insertData);
          if(parseInt(mycredits) >= parseInt(requiredCredits) || (mypremiums >= '{{ time() }}')){
			firebase.database().ref().child('chat_rooms/').child(chatRoom).push(insertData);
                if(proposal){
                    if(proposal != 'likes' && proposal != 'favorites') {
                     $('#proposal_message').show();
                     $(".send_proposal_wrap").slideUp(300);
                    }
               }else{
                     $('#chat_message').show();
               }
               $('#mypriphoto').modal('hide');
		  }else{
			//message failed
			$('#messages').append(''+ 
                    '<li class="message">'+                    
                        '<div class="chat-user-right">'+
                        '<div class="chat-user-box">'+
                        '<img src="{{ asset('/public/thumbnail/') }}/'+jQuery('#sender_image').val()+'">'+
                        '<span>'+jQuery('#sender_name').val()+'</span>'+
                        '</div>'+
                        '<div class="chat-user-message">'+
                        '<div class="chat-line"><span>'+msgText+'</span></div>'+                                   
                        '<div class="meta-info"><span class="date">'+$.now()+'</span></div>'+
						'<div class="meta-info red"><span class="date">Message Sent Failed!!</span></div>'+
                        '</div>'+
                        '</div>'+
                    '</li>'+
                '');
			setTimeout(function(){ jQuery('.credit_btn').click(); }, 3000);	
		  }
          
                    
        });
      }
      
      
      
 // send image
     
      $(document).on('change', '#file', function (e) {
      
        var fileName = e.target.files[0].name;
        $("#selected").html(fileName);
      
        var file = e.target.files[0];
         var storageRef = firebase.storage().ref();
        var dataD = Date.now();
        var uploadTask = storageRef.child('images/' + dataD).put(file);
        
        <?php if(isset($users) && ($users->fake)){ ?>
            var sendrimgurl = '{{ env('PROFILE_MANAGER_ADDR').('/public/thumbnail/') }}/';
        <?php }else{ ?>
            var sendrimgurl = '{{ asset('/public/thumbnail/') }}/';
        <?php } ?>
        
        uploadTask.on('state_changed', function(snapshot) { 
                var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
                console.log('Upload is ' + progress + '% done');

                var imageUrl = "http://www.mizpahpublishing.com/images/loader.gif";

                var myName = $('#sender_name').val();
                var myimage = sendrimgurl+$('#sender_image').val();
                
                $('#messages').append(''+
                    '<li id="removeLi" class="message">'+                    
                    '<div class="chat-user-right">'+
                    '<div class="chat-user-box">'+
                    '<img src="'+myimage+'">'+
                    '<span>'+myName+'</span>'+
                    '</div>'+
                    '<div class="chat-user-message">'+
                    '<div class="chat-line"><span>'+
                    '<img src=' + imageUrl + ' alt="Ovengo..." height="100" width="100">'+
                    '</span></div>'+                                   
                    '<div class="meta-info"><span class="date">Now</span></div>'+
                    '</div>'+
                    '</div>'+
                '</li>'+
                '');
    
                $('#messages').animate({
                    scrollTop: $('#messages').prop("scrollHeight")
                }, 0);

                var msgDiv = document.getElementById('msgDiv');
        msgDiv.scrollTop = msgDiv.scrollHeight;

                switch (snapshot.state) {
                    case firebase.storage.TaskState.PAUSED:
                        ////console.log('Upload is paused');
                        $('#messages').animate({
                            scrollTop: $('#messages').prop("scrollHeight")
                        }, 0);
                        break;
                    case firebase.storage.TaskState.RUNNING:
                        ////console.log('Upload is running');
                        $('#messages').animate({
                            scrollTop: $('#messages').prop("scrollHeight")
                        }, 0);
                        break;
                }

            }, 
            function(error) {

            }, function() {
                $('#removeLi').remove();
                var storageqq = firebase.storage();
                storageqq.ref('images/' + dataD).getDownloadURL().then(function (url) {
                    var downloadURL = url;
                    
                        var messageType = 'file';
                    
                    
                        var userId    = $('#receiver_id').val();
                        var userName  = $('#receiver_name').val();
                        var userImage = $('#receiver_image').val();
                        var msgText = downloadURL;
                
                        //CKEDITOR.instances.message.setData('');
                    
                        var chatRoom = $('#room_id').val();
                    
                        firebase.database().ref('/users/'+userId).once('value').then(function(snapshot) {
                          var childData = snapshot.val();
                          
                          firebase.database().ref().child('chat_history/'+adminId).child(userId).once('value').then(function(snapshot1) {
                    
                            var snapshot1Data = snapshot1.val();
                    
                            var msgCount = 0;
                            var hisType = 1;
                            if (snapshot1Data != null){
                              var msgCount = Number(snapshot1Data.messageCount)+1;
                              if(snapshot1Data.historyType == 1){
                                if(snapshot1Data.lastSenderId != adminId){
                                  hisType = 2;
                                }
                              }else{
                                hisType = 2;
                              }
                            }
                    
                            var insertData = {
                              deletteby : '',
                              message : msgText,
                              messageCount : msgCount,
                              name : userName,
                              profilePic : userImage,
                              timestamp : $.now(),
                              uid : userId.toString(),
                              historyType : hisType,
                              lastSenderId : adminId.toString(),
                              messageType : messageType,
                            };
                          
                          firebase.database().ref().child('chat_history/'+adminId).child(userId).set(insertData);
                          });
                        });
                    
                        firebase.database().ref('/users/'+adminId).once('value').then(function(snapshot) {
                          var childData = snapshot.val();
                    
                          firebase.database().ref().child('chat_history/'+userId).child(adminId).once('value').then(function(snapshot2) {
                    
                            var snapshot2Data = snapshot2.val();
                    
                            var msgCount = 0;
                            var hisType = 1;
                            if (snapshot2Data != null){
                              var msgCount = Number(snapshot2Data.messageCount)+1;
                              if(snapshot2Data.historyType == 1){
                                if(snapshot2Data.lastSenderId != adminId){
                                  hisType = 2;
                                }
                              }else{
                                hisType = 2;
                              }
                            }
                    
                            var insertData = {
                              deletteby : '',
                              message : msgText,
                              messageCount : msgCount,
                              name : childData.userName,
                              profilePic : childData.userImage,
                              timestamp : $.now(),
                              uid : adminId.toString(),
                              historyType : hisType,
                              lastSenderId : adminId.toString(),
                              messageType : messageType,
                            };
                    
                          firebase.database().ref().child('chat_history/'+userId).child(adminId).set(insertData);
                          });
                        });
                    
                        firebase.database().ref('/users/'+adminId).once('value').then(function(snapshot) {  
                          var childData = snapshot.val();
                          var insertData = {
                            deleteby:'',
                            message : msgText,
                            messageCount: 1,
                            name : childData.userName,
                            profilePic : childData.userImage,
                            timestamp : $.now(),
                            uid : adminId.toString(),
                            messageType : messageType,
                          };
                          firebase.database().ref().child('chat_rooms/').child(chatRoom).push(insertData);
                        });
      
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    $('#chat_send_image').val(downloadURL);
                }); 
            
            });
        
        
        });