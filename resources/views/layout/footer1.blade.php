</div><!-- main_wrapper -->
<footer class="fl_footer_section footer_second">
  <div class="container">
    <div class="row">
     <div class="col-sm-12">
      <div class="footer_text">
        <p>
            <span class="full_width">The minimum age for participation on flirtfull.com is 18 years.</span>
            Flirtfull.com is an adult social flirting platform for men and women who are looking for a fun, flirty or exciting contact. Every day, hundreds of new members sign up. Based on your profile settings, you will receive match suggestions. You can also use our search functionality and browse for profiles yourself. Flirtfull.com is designed for adult pleasure and entertainmentand optimised for desktops, mobile phones and tablets.Profiles are partly fictional, physical arrangements with these angel profiles are not possible. We strongly advise you to read our Terms and Conditions before using our Service.
        </p>
      </div>
      <div class="footer_copyright">
        <p><a href="http://35.242.157.120/">Flirtfull.com</a> © {{ date('Y') }} . All Rights Reserved</p>
      </div>
     </div>
    </div>
  </div>
</footer> 
<!--<div id="myModal" class="modal fade sign-in-up" role="dialog" data-keyboard="false" data-backdrop="static">-->
<div id="myModal" class="modal fade sign-in-up" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <!-- Modal content--> <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
    <div class="modal-content">
   
      <div class="modal-body">
          
              <h3>Sign up to start meeting people around the world!</h3>
              <div class="registration_panel">
              	<h4>Create an account with just one click!</h4>
                <a id="glink" href="{{ route('auth.social') }}"><button class="" id="login-google"> <span><svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="50" height="50" viewBox="0 0 48 48"
     style="fill:#000000;"><g id="surface1"><path style=" fill:#FFC107;" d="M 43.609375 20.082031 L 42 20.082031 L 42 20 L 24 20 L 24 28 L 35.304688 28 C 33.652344 32.65625 29.222656 36 24 36 C 17.371094 36 12 30.628906 12 24 C 12 17.371094 17.371094 12 24 12 C 27.058594 12 29.84375 13.152344 31.960938 15.039063 L 37.617188 9.382813 C 34.046875 6.054688 29.269531 4 24 4 C 12.953125 4 4 12.953125 4 24 C 4 35.046875 12.953125 44 24 44 C 35.046875 44 44 35.046875 44 24 C 44 22.660156 43.863281 21.351563 43.609375 20.082031 Z "></path><path style=" fill:#FF3D00;" d="M 6.304688 14.691406 L 12.878906 19.511719 C 14.65625 15.109375 18.960938 12 24 12 C 27.058594 12 29.84375 13.152344 31.960938 15.039063 L 37.617188 9.382813 C 34.046875 6.054688 29.269531 4 24 4 C 16.316406 4 9.65625 8.335938 6.304688 14.691406 Z "></path><path style=" fill:#4CAF50;" d="M 24 44 C 29.164063 44 33.859375 42.023438 37.410156 38.808594 L 31.21875 33.570313 C 29.210938 35.089844 26.714844 36 24 36 C 18.796875 36 14.382813 32.683594 12.71875 28.054688 L 6.195313 33.078125 C 9.503906 39.554688 16.226563 44 24 44 Z "></path><path style=" fill:#1976D2;" d="M 43.609375 20.082031 L 42 20.082031 L 42 20 L 24 20 L 24 28 L 35.304688 28 C 34.511719 30.238281 33.070313 32.164063 31.214844 33.570313 C 31.21875 33.570313 31.21875 33.570313 31.21875 33.570313 L 37.410156 38.808594 C 36.972656 39.203125 44 34 44 24 C 44 22.660156 43.863281 21.351563 43.609375 20.082031 Z "></path></g></svg> <strong>Sign in via Google</strong></span></button></a>
     
     <div class="terms-info">Don't worry! We never post on your behalf.</div>
     
              </div>

              <div class="divider"><span>Or</span></div>
               <div class="row sign-up-form">
               	<div class="col-md-6">
                <h3>Sign Up</h3>
               <div class="signin_panel">
                <form method="get" action="{{ url('/') }}" id="signup_modal">
                   <div class="form-group seeking_group">
                      <div class="width_50">
                        <label class="control-label">I Am A</label>
                        <div class="radio_btns">
                          <span class="radio-btn">
                            <input type="radio" name="gender" id="malee" value="1" class="gender_classs"  {{ (isset($_GET['gender']) && $_GET['gender'] == 1) ? 'checked' : '' }}> 
                            <label for="malee"><img src="{{ asset('public/images/male.png') }}"></label>
                          </span>
                          <span class="radio-btn">
                            <input type="radio" name="gender" id="femalee" value="2" class="gender_classs"  {{ (isset($_GET['gender']) && $_GET['gender'] == 2) ? 'checked' : '' }}> 
                            <label for="femalee"><img src="{{ asset('public/images/female.png') }}"></label>
                          </span>
                          <p class="error-message gender"></p>
                        </div>
                      </div>
                     <div class="width_50">
                        <label class="control-label">Seeking A</label>
                        <div class="radio_btns">
                          <span class="radio-btn">
                            <input type="radio" id="s-malee" name="seeking" value="1" class="seeking_classs"  {{ (isset($_GET['seeking']) && $_GET['seeking'] == 1) ? 'checked' : '' }}> 
                            <label for="s-malee"><img src="{{ asset('public/images/male.png') }}"></label>
                          </span>
                          <span class="radio-btn">
                            <input type="radio" name="seeking" id="s-femalee" value="2" class="seeking_classs"  {{ (isset($_GET['seeking']) && $_GET['seeking'] == 2) ? 'checked' : '' }}> 
                            <label for="s-femalee"><img src="{{ asset('public/images/female.png') }}"></label>
                          </span>
                          <p class="error-message seeking"></p>
                        </div>
                    </div>
                  </div>
                  <div class="form-group">
                      <input id="signup_name" type="text" class="form-control" placeholder="Enter Your Nick Name">
                      <p class="signup-name-wrong error-message"></p>
                  </div>
                  <div class="form-group">
                      <input id="signup_email" type="text" class="form-control" placeholder="Enter Your Email Id">
                      <p class="signup-email-wrong error-message"></p>
                    </div>
                    <div class="form-group">
                      <input id="signup_password" type="password" class="form-control" placeholder="Enter Your Password">
                      <p class="signup-password-wrong error-message"></p>
                      <!-- <p class="both-wrong error-message"></p> -->
                    </div>
                    <div class="form-group birthday_select">
                      <label class="control-label">birthday</label>
                      <div class="col-sm-4">
                        <select id="month" class="form-control" >
                          <option value="" class="label" selected="selected" >month</option>
                          <?php
                            for ($m=1; $m<=12; $m++) 
                            {
                              $month_name = date('F', mktime(0,0,0,$m, 1, date('Y')));
                              echo "<option value='".$m."'>".$month_name."</option>";
                            }
                          ?>
                        </select>
                      </div>
                      <div class="col-sm-4">
                        <select id="day" class="form-control" >
                          <option value="" class="label" selected="selected" >day</option>
                          <?php
                            for ($m=1; $m<=31; $m++) 
                            {
                              echo "<option value='".$m."'>".$m."</option>";
                            }
                          ?>
                        </select>
                      </div>
                      <div class="col-sm-4">
                        <select id="year" class="form-control" >
                          <option value="" class="label" selected="selected" >year</option>
                          <?php 
                          for($i=2001;$i>=1930;$i--)
                          {
                            echo '<option value="'.$i.'">'.$i.'</option>';
                          }
                          ?>
                        </select>
                      </div>
                      <p class="error-message birth_date"></p>
                    </div>
                    <div class="form-group">
						<select name="prof_region" id="prof_region2" class="form-control" onChange="getCityByState(this.value);">
							<option selected="" value="">Region</option>
						</select>
						<p class="error-message region"></p>
					</div>
					<div class="form-group">
                      <input type="hidden" name="country" id="country" value="United Kingdom"/> 
                      <select name="prof_city" id="prof_city1" class="form-control" >
                        <option selected="" value="">City</option>
                      </select>
                    </div>

                    <div class="form-group">
                    <div class="check_box">
                      <input type="checkbox" id="accept_cond" value="1"><span> I accepted <a target="_blank" href="{{ url('terms-conditions') }}">Terms & Conditions</a> &amp; <a target="_blank" href="{{ url('privacy-policy') }}">Privacy Policy</a></span>
                    </div>
                      <div>
                        
                        <p class="error-message accept"></p>
                      </div>
                    </div>

                    <p class="signup-process text-center">Processing...</p>
                    <div class="form-group">
                      <input type="submit" class="btn btn-primary btn-bg" value="Sign Up">
                    </div>
                </form>
               </div>
               </div>
               	<div class="col-md-6">
              <h3>Sign In</h3>
               <div class="signin_panel">
               	<form method="get" action="{{ url('search') }}" id="login_modal">
                	<div class="form-group">
                    	<input id="login_email" type="text" class="form-control" placeholder="Enter Your Email Id">
                      <p class="email-wrong error-message"></p>
                    </div>
                    <div class="form-group">
                    	<input id="login_password" type="password" class="form-control" placeholder="Enter Your Password">
                      <p class="password-wrong error-message"></p>
                      <p class="both-wrong error-message"></p>
                    </div>
                    <div class="form-group">
                    	<input type="submit" class="btn btn-primary btn-bg" value="Sign In">
                    </div>
                </form>
                
               
               </div>
               </div>
               </div>
               
        <p>By clicking «Continue» you agree to our <a href="{{ url('terms-conditions') }}" style="font-weight: 600;color: #000;">Terms and Conditions</a></p>
      </div>
     
    </div>

  </div>
</div>

<div id="myModal_profile" class="modal fade sign-in-up" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <!-- Modal content--> <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
    <div class="modal-content">

     <div class="modal-body">




      <form id="update_profile_form" action="post">
        <!-- One "tab" for each step in the form: -->
        <input type="hidden" id="user_id" value="{{ Auth::id() }}">
        <div class="tab row">
          <h1>About you</h1>
          <div class="row">
            <div class="form-group">
             <label class="col-md-3 control-label">Name or nickname</label><div class="col-md-9"><input id="name" type="text" value="<?php echo (!empty(Auth::user()->name)) ? Auth::user()->name : '' ?>" placeholder="Name or nickname" class="form-control">
              <p class="error-message name"></p>
             </div>
           </div>
         </div>


         <div class="row">
           <div class="col-md-6">
            <div class="form-group">
              <div class="row">
               <div class="col-md-6 ">
                <label class="control-label">I am a</label>
              </div>
              <div class="col-md-6">

               <span class="radio-btn">
                <input type="radio" name="gender" id="male" value="1" class="gender_class"> 
                <label for="male"><img src="{{ asset('public/images/male.png') }}"></label></span>
                <span class="radio-btn">
                  <input type="radio" name="gender" id="female" value="2" class="gender_class"> 
                  <label for="female"><img src="{{ asset('public/images/female.png') }}"></label></span>
                  <p class="error-message gender"></p>
             </div>
           </div>
         </div>
       </div>
       <div class="col-md-6">
        <div class="form-group">
          <div class="row">
            <div class="col-md-6 text-right">
              <label class="control-label">Seeking a</label>
            </div>
            <div class="col-md-6">
             <span class="radio-btn"><input type="radio" id="s-male" name="seeking" value="1" class="seeking_class"> <label for="s-male"><img src="{{ asset('public/images/male.png') }}"></label></span><span class="radio-btn"><input type="radio" name="seeking" id="s-female" value="2" class="seeking_class"> <label for="s-female"><img src="{{ asset('public/images/female.png') }}"></label></span>

             <p class="error-message seeking"></p>
           </div>
         </div>
       </div>
     </div>
   </div>
   <div class="form-group">
    <div class="row">
      <label class="col-md-3 control-label">birthday</label>
      <div class="col-md-9">
        <div class="row">
         <div class="col-sm-4">
          <select id="month" class="selectpicker">

            <option value="" class="label" selected="selected">month</option>
            <option value="1">January</option>
            <option value="2">February</option>
            <option value="3">March</option>
            <option value="4">April</option>
            <option value="5">May</option>
            <option value="6">June</option>
            <option value="7">July</option>
            <option value="8">August</option>
            <option value="9">September</option>
            <option value="10">October</option>
            <option value="11">November</option>
            <option value="12">December</option>

          </select>
        </div>

        <div class="col-sm-4">
          <select id="day" class="selectpicker">

            <option value="" class="label" selected="selected">day</option>
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
            <option value="5">5</option>
            <option value="6">6</option>
            <option value="7">7</option>
            <option value="8">8</option>
            <option value="9">9</option>
            <option value="10">10</option>
            <option value="11">11</option>
            <option value="12">12</option>
            <option value="13">13</option>
            <option value="14">14</option>
            <option value="15">15</option>
            <option value="16">16</option>
            <option value="17">17</option>
            <option value="18">18</option>
            <option value="19">19</option>
            <option value="20">20</option>
            <option value="21">21</option>
            <option value="22">22</option>
            <option value="23">23</option>
            <option value="24">24</option>
            <option value="25">25</option>
            <option value="26">26</option>
            <option value="27">27</option>
            <option value="28">28</option>
            <option value="29">29</option>
            <option value="30">30</option>
            <option value="31">31</option>

          </select>
        </div>
        <div class="col-sm-4">
          <select id="year" class="selectpicker">
           <option value="" class="label" selected="selected">
            year
          </option>
          <?php 
            for($i=1990;$i<=date('Y');$i++)
            {
              echo '<option value="'.$i.'">'.$i.'</option>';
            }
          ?>
          </select>
      </div>
    </div>
    <p class="error-message birth_date"></p>
  </div>
</div>
</div>
<!-- <div class="form-group">
  <div class="row">
    <label class="col-md-3 control-label">hometown </label>
    <div class="col-md-9">
      <input id="prof_address" type="text" placeholder="Hometown" class="form-control">
      <input id="prof_country" type="hidden" value="">
      <input id="prof_city" type="hidden" value="">
      <p class="address error-message"></p>
    </div>
  </div>
</div> -->

<div class="form-group">
  <div class="row">
    <label class="col-md-3 control-label">Country </label>
    <div class="col-md-9">
      <select name="prof_country" id="prof_country" class="form-control" onChange="getCityUpdate(this.value);">
       <?php $countries = DB::table('countries')->where('status',1)->get();?>
       <option selected="" value="">Country</option>
            @if(isset($countries))
            @foreach($countries as $row)
            <option value="{{ $row->name }}" <?php echo (!empty(Auth::user()->country) && (Auth::user()->country == $row->name)) ? 'selected' : '' ?>>{{ $row->name }}</option>
            @endforeach 
            @endif
      </select>

      <!-- <input id="prof_address" type="text" placeholder="Hometown" class="form-control">
      <input id="prof_country" type="hidden" value="">
      <input id="prof_city" type="hidden" value=""> -->
      <p class="address error-message"></p>
    </div>
  </div>
</div>

<div class="form-group">
  <div class="row">
    <label class="col-md-3 control-label">City </label>
    <div class="col-md-9">
      <select name="prof_city" id="prof_city" class="form-control">
       <option selected="" value="">City</option>
           
      </select>
    </div>
  </div>
</div>

</div>

<div>
  <div class="row form-btn-box">
    
    <button type="submit" id="nextBtn">Submit</button>
  </div>
</div>

</form>


</div>
</div>

</div>

</div>

<div class="modal fade" id="upgrade_myModal3" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title text-center"> Become a premium member and get access to all website features.</h4>
          <p></p>
            
        </div>
        <div class="modal-body">
          <div class="row">
          <div class="col-md-12">
                <?php
                $helper = new Helper();
                $diamonds = $helper->getAllDiamonds();
                $credits = $helper->getAllCredits();
                $coins = $helper->getAllCoins();
                $premiums = $helper->getAllPremiums();                
                ?> 
                    @foreach($credits as $k=>$credit)
                      <?php if($k == 0){ $m = 3; }?>
                      <?php if($k == 1){ $m = 2; }?>
                      <?php if($k == 2){ $m = 1; }?>
                      <?php if($k == 3){ $m = 2; }?>
                      <?php if($k == 4){ $m = 3; }?>  
                      @if($k < 5)  
                    <div class="col-5">
                        <div class="upgrade_inner upgrade{{$m}}">
                            <div class="upgrade-img"><img src="{{ $credit->image }}"></div>
                            <div class="upgrade_inner_info"><h2>{{ $credit->qty }} <span>Credit</span></h2>
                            <p>£ {{ $credit->price }} (+ {{ $credit->txn_fee }})</p>
                            <!--<p>£ 1,40 / Message</p>-->
                            <br>
                            <a href="#" id="credit3{{ $credit->id }}" class="upgrade-btn" onclick="select_plan('credits',{{ $credit->id }});">select</a></div>
                        </div>
                    </div>
                     @endif
                    @endforeach 
                    
                    <!--<div class="col-5">
                        <div class="upgrade_inner upgrade3">
                            <div class="upgrade-img"><img src="http://35.242.157.120/public/thumbnail/nolemoniquep1.jpg"></div>
                            <div class="upgrade_inner_info"><h2>20 <span>Credit</span></h2>
                            <p>£ 35,00 (+ £ 05,50)</p>
                            <p>£ 1,40 / Message</p>
                            <a href="#" class="upgrade-btn">select</a></div>
                        </div>
                    </div>
                    <div class="col-5">
                        <div class="upgrade_inner upgrade2">
                            <div class="upgrade-img"><img src="http://35.242.157.120/public/thumbnail/nikitavalentinp1.jpg"></div>
                            <div class="upgrade_inner_info"><h2>20 <span>Credit</span></h2>
                            <p>£ 35,00 (+ £ 05,50)</p>
                            <p>£ 1,40 / Message</p>
                            <a href="#" class="upgrade-btn">select</a></div>
                        </div>
                    </div>
                    <div class="col-5">
                        <div class="upgrade_inner upgrade1">
                            <div class="upgrade-img"><img src="http://35.242.157.120/public/thumbnail/xenacarryp1.jpg"></div>
                            <div class="upgrade_inner_info"><h2>20 <span>Credit</span></h2>
                            <p>£ 35,00 (+ £ 05,50)</p>
                            <p>£ 1,40 / Message</p>
                            <a href="#" class="upgrade-btn">select</a>
                        </div></div>
                    </div>
                    <div class="col-5">
                        <div class="upgrade_inner upgrade2">
                            <div class="upgrade-img"><img src="http://35.242.157.120/public/thumbnail/elledeanq.jpg"></div>
                            <div class="upgrade_inner_info"><h2>20 <span>Credit</span></h2>
                            <p>£ 35,00 (+ £ 05,50)</p>
                            <p>£ 1,40 / Message</p>
                            <a href="#" class="upgrade-btn">select</a></div>
                        </div>
                    </div>  
                    <div class="col-5">
                        <div class="upgrade_inner upgrade3">
                            <div class="upgrade-img"><img src="http://35.242.157.120/public/thumbnail/candynyceq.jpg"></div>
                            <div class="upgrade_inner_info"><h2>20 <span>Credit</span></h2>
                            <p>£ 35,00 (+ £ 05,50)</p>
                            <p>£ 1,40 / Message</p>
                            <a href="#" class="upgrade-btn">select</a></div>
                        </div>
                    </div>-->
                </div>
              </div>
                
                <div class="pymnt-form">
                <div class="row">
                	<div class="col-md-7">
                    	<ul>
                        	<li><a href="#"><img src="http://35.242.157.120/public/images/visa.png"></a></li>
                            <li><a href="#"><img src="http://35.242.157.120/public/images/paypal.png"></a></li>
                            <li><a href="#"><img src="http://35.242.157.120/public/images/master_card.png"></a></li>
                        </ul>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
                    </div>
                    <div class="col-md-5">  
                      <!-- <form id="purchase_credit" action="">
                   <input type="hidden" id="currency" value="GBP">
                   <input type="hidden" id="amount" value="10.00">
                    <div class="row">
                        <div class="col-md-12">
                          <div class="form-group">
                            <input type="text" class="form-control" id="" placeholder="Card number">
                          </div>
                         </div>
                       </div>                       
                   <input type="hidden" name="plan_type" id="plan_type">    
                   <input type="hidden" name="plan" id="plan" >
                  <button type="submit" class="btn btn-default">Purchase Credit</button>
                </form> -->
              </div>
                </div>
              
                </div>
			<div class="row text-center">
				<!--<div class="col-md-12">
					<div class="pypl text-center" id="pyplshw">
					<p>Alternative payment methods</p>
				</div>-->
				<div class="pypl-btn" id="paypal">
					<button type="button"><span class="pay">Pay</span><span class="pal">Pal</span></button>
				</div>
			  </div>
				</div>
                    
                    
            </div>
			
        </div>
        
      </div>

<div class="modal fade" id="upgrade_myModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title text-center" id="premium_title">Premium: Become a premium member and get access to all website features. </h4>
          <h4 class="modal-title text-center" id="credit_title">Credit: Buy credits to send messages and proposals. </h4>  
          <p></p>
            
        </div>
        <?php
                $helper = new Helper();
                $diamonds = $helper->getAllDiamonds();
                $credits = $helper->getAllCredits();
                $coins = $helper->getAllCoins();
                $premiums = $helper->getAllPremiums();                
                ?>  

        <div class="modal-body">
          <div class="row">
              <div class="col-md-12">
                <div class="flipster_tab menulist">
                  <ul id="active_plan">
                    <!--<li data-show="premium_tab">Premium</li>-->
                    <!--<li data-show="credit_tab">Credit</li>-->
                  </ul>
                </div>
                <!-- tab content-->
                <div class="tab_content" id="credit_tab">
                	<div class="flipster">
      		         <ul>
                      @foreach($credits as $credit)
              		  	<li>
              		  		<div class="upgrade_inner ">
                            <div class="upgrade-img">
                              <!-- <img src="http://35.242.157.120/public/thumbnail/nolemoniquep1.jpg"> -->
                              <img src="{{ $credit->image }}" onerror="this.src="http://35.242.157.120/public/thumbnail/nolemoniquep1.jpg">
                            </div>
                            <div class="upgrade_inner_info"><h2>{{ $credit->qty }} <span>Credit</span></h2>
                            <p> <span class="amount"><span class="currency">£</span>{{ $credit->price }}</span></p>
                            <p>£ {{ round(($credit->price/$credit->qty),2) }} / Message</p>
                            <a href="#" class="upgrade-btn" id="credit{{ $credit->id }}" onclick="select_plan('credits',{{ $credit->id }});">select <i class="fa fa-check"></i></a></div>
                          </div>
                		</li>
                        @endforeach
            		    </ul>
                  </div>
                </div>
                <!-- tab content-->
                <!-- tab content-->
                <div class="tab_content" id="premium_tab">
                  <div class="flipster">
                   <ul>
                      @foreach($premiums as $premium)
                      <li>
                        <div class="upgrade_inner ">
                            <div class="upgrade-img">
                              <!-- <img src="http://35.242.157.120/public/thumbnail/nolemoniquep1.jpg"> -->
                              <img src="{{ $credit->image }}" onerror="this.src="http://35.242.157.120/public/thumbnail/nolemoniquep1.jpg">
                            </div>
                            <div class="upgrade_inner_info">
                              <h4><span class="amount"><span class="currency">£</span>{{ $premium->price }}</span></h4>
                              <h2>{{ $premium->month }} <span>Month</span></h2>
                              <h3><span class="currency">£</span>13.5/month</h3>
                            <a href="#" class="upgrade-btn" id="premium{{ $premium->id }}" onclick="select_plan('premiums',{{ $premium->id }});">select <i class="fa fa-check"></i></a></div>
                          </div>
                        </li>
                        @endforeach
                    </ul>
                  </div>
                </div>
                <!-- tab content-->
              </div>
            </div>
                
            <div class="pymnt-form">
                <div class="row">
                    <div class="col-md-5 col-sm-12 payment_right">
                        @if (Auth::check() && Auth::user()->status == 1)  
                        <form id="purchase_credit" action="">
                        @else
                        <form id="" action="">              
                            @endif              
                           <input type="hidden" id="currency" value="GBP">
                           <input type="hidden" id="amount" value="10.00">
                            <div class="row">
                                <div class="col-md-12">
                                  <!-- <div class="form-group">
                                    <input type="text" class="form-control" id="" placeholder="Card number">
                                  </div> -->
                                 </div>
                               </div>                       
                            <div class="pay_radio">
                               <input type="hidden" name="plan_type" id="plan_type">    
                               <input type="hidden" name="plan" id="plan" >
                               <input type="hidden" id="payment_method" name="payment_method" value="pay">
                               <div class="selectbox">
                                 <select id="paynl_id" name="paynl_id"></select>
                               </div>
                            </div>
                	          <span id="plan_error" style="display:none; color:red;">Please Select Plan</span>
                             @if (Auth::check() && Auth::user()->status == 1)  
                            <button type="submit" class="btn btn-default">Purchase</button>
                            @else
                            <button class="btn btn-default" data-toggle="modal" data-target="#notverifyModal">Purchase</button>    
                            @endif  
                        </form>
    			         	</div>
                    <div class="col-md-7 col-sm-12">
                      <ul>
                          <li><a href="#"><img src="http://35.242.157.120/public/images/visa.png"></a></li>                            
                            <li><a href="#"><img src="http://35.242.157.120/public/images/master_card.png"></a></li>
                            <li><a href="#"><img src="http://35.242.157.120/public/images/amex.png"></a></li>
                            <li><a href="#"><img src="http://35.242.157.120/public/images/paypal.png"></a></li>    
                        </ul>
                        <p>All payments are done to and processed by Oddigits BV. To protect your privacy, your bank statement will say: "DTN Oddigits".</p>
                        <p id="premium_footer">We will not renew your subscription or rebill your card.</p>
                        <p>All prices include 25% VAT.</p>
                    </div>
                </div>
              
                </div>
			<div class="row text-center">
				<!--<div class="col-md-12">
					<div class="pypl text-center" id="pyplshw">
					<p>Alternative payment methods</p>
				</div>-->
				<div class="pypl-btn" id="paypal">
					<button type="button"><span class="pay">Pay</span><span class="pal">Pal</span></button>
				</div>
			  </div>
				</div>
                    
                    
            </div>
			
        </div>
        
      </div>
    </div>

<section class="upgrade-popup">
	<div class="modal fade" id="upgrade_myModal1" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title text-center">Purchase coins and get amazing features!</h4>
          <p></p>
            <div class="row">
        <div class="col-md-12">
            <div class="tab" role="tabpanel">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#Section1" aria-controls="home" role="tab" data-toggle="tab">Diamonds</a></li>
                    <li role="presentation"><a href="#Section2" aria-controls="profile" role="tab" data-toggle="tab">Coins</a></li>
                    <li role="presentation"><a href="#Section3" aria-controls="messages" role="tab" data-toggle="tab">Credit</a></li>
                    <li role="presentation"><a href="#Section4" aria-controls="messages" role="tab" data-toggle="tab">Premium</a></li>
                </ul>
                <?php
                $helper = new Helper();
                $diamonds = $helper->getAllDiamonds();
                $credits = $helper->getAllCredits();
                $coins = $helper->getAllCoins();
                $premiums = $helper->getAllPremiums();                
                ?>    
                <!-- Tab panes -->
                <div class="tab-content tabs">
                    <div role="tabpanel" class="tab-pane fade in active" id="Section1">
                    <div class="row">
                        @foreach($diamonds as $diamond)
                        <div class="col-md-3">
                            <div class="pricingTable">
                                <div class="pricingTable-header">
                                    <h3 class="heading">{{ $diamond->qty }} Diamonds</h3>
                                </div>
                                <div class="price-value">
                                    <span class="month">Price / Diamond</span>
                                    <span class="amount"><span class="currency">£</span>{{ $diamond->price }}</span>
                                </div>
                                <div class="pricing-content">
                                    <ul>
                                        <li><i class="fa fa-check"></i> X Diamonds</li>
                                    </ul>
                                </div>
                                <a href="#" class="read" id="diamond{{ $diamond->id }}" onclick="select_plan('diamonds',{{ $diamond->id }});">Select</a>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="Section2">
                        <div class="row">
                            @foreach($coins as $coin)
                            <div class="col-md-3">
                                <div class="pricingTable">
                                    <div class="pricingTable-header">
                                        <h3 class="heading">{{ $coin->qty }} Coin</h3>
                                    </div>
                                    <div class="price-value">
                                        <span class="month">Price / Coin</span>
                                        <span class="amount"><span class="currency">£</span>{{ $coin->price }}</span>
                                    </div>
                                    <div class="pricing-content">
                                        <ul>
                                            <li><i class="fa fa-check"></i> X Coins</li>
                                        </ul>
                                    </div>
                                    <a href="#" class="read" id="coin{{ $coin->id }}" onclick="select_plan('coins',{{ $coin->id }});">Select</a>
                                </div>
                            </div>
                            @endforeach                            
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="Section3" onclick="select_plan('credits',{{ $credit->id }});">
                        <div class="row">
                            @foreach($credits as $credit)
                            <div class="col-md-4">
                                <div class="pricingTable">
                                    <div class="pricingTable-header">
                                        <h3 class="heading">{{ $credit->qty }} Credits</h3>
                                    </div>
                                    <div class="price-value">
                                        <span class="month">Price / Credit</span>
                                        <span class="amount"><span class="currency">£</span>{{ $credit->price }}</span>
                                    </div>
                                    <div class="pricing-content">
                                        <ul>
                                            <li><i class="fa fa-check"></i> X Credits</li>
                                        </ul>
                                    </div>
                                    <a href="#" class="read" id="credit1{{ $credit->id }}" onclick="select_plan('credits',{{ $credit->id }});">Select</a>
                                </div>
                            </div>
                            @endforeach      
                        </div><!-- End of row -->
                    </div>


                    <div role="tabpanel" class="tab-pane fade" id="Section4">
                        <div class="row">
                            @foreach($premiums as $premium)
                            <div class="col-md-3">
                                <div class="pricingTable">
                                    <div class="pricingTable-header">
                                        <h3 class="heading">{{ $premium->month }} Month</h3>
                                    </div>
                                    <div class="price-value">
                                        <span class="month">Price / Month</span>
                                        <span class="amount"><span class="currency">£</span>{{ $premium->price }}</span>
                                    </div>
                                    <div class="pricing-content">
                                        <ul>
                                            <li><i class="fa fa-check"></i> X Coins</li>
                                            <li><i class="fa fa-check"></i> Y Diamonds</li>
                                        </ul>
                                    </div>
                                    <a href="#" class="read" id="premium{{ $premium->id }}" onclick="select_plan('premiums',{{ $premium->id }});">Select</a>
                                </div>
                            </div>
                            @endforeach                           
                        </div>
                    </div><!-- End of row -->
                    
                </div>
            </div>
        </div>
    </div>
        </div>
        <div class="modal-body">
          <div class="pymnt-form">
          	<!-- <form id="purchase_credit" action="">
               <input type="hidden" id="currency" value="EUR">
               <input type="hidden" id="amount" value="10.00">
          		<div class="row">
	      			<div class="col-md-12">
					  
					 </div>
				   </div>
				  
               <input type="hidden" name="plan_type" id="plan_type">    
               <input type="hidden" name="plan" id="plan" >
			  <button type="submit" class="btn btn-default">Purchase Credit</button>
			</form> -->
			<div class="row text-center">
				<!--<div class="col-md-12">
					<div class="pypl text-center" id="pyplshw">
					<p>Alternative payment methods</p>
				</div>-->
				<div class="pypl-btn" id="paypal">
					<button type="button"><span class="pay">Pay</span><span class="pal">Pal</span></button>
				</div>
			  </div>
				</div>
			</div>
			<div class="coin-btm">
			<div class="row text-center">
				<div class="col-md-12">
					<p>You have selected 20 Credits for $2.99including all taxes and additional charges.Purchase appears on your bank statement as: ‘Dating*’</p>
				<div class="checkbox">
			    <label><input type="checkbox"> Update my account to the same amount when my credits end.</label>
			</div>
			    <div class="pymnt-cond text-center">
			    	<p>* 1st purchase discount, $15.99 starting with 2nd purchase.</p>
			    	<p>** 1-month subscription for free, $9.99 starting from 2nd month. Your subscription will renew until you cancel.</p>
			    	<p>By subscribing you authorize us to charge your credit card upon each renewal and accept Terms and Conditions. Payments processed by </p>
			    	<p>IDWS Management Pte. Ltd in Singapore. <a href="#">Learn more</a></p>
			    </div>
			</div>
          </div>
        </div>
        </div>
        <div class="modal-footer">
          <p>If you have any problems please contact Live Support or call us at +1 (800) 665-7633</p>
        </div>
      </div>
    </div>
  </div>
</section>




<div id="payment_status" class="modal fade" role="dialog">
  <div class="modal-dialog modal-sm modal-dialog-centered">

    <!-- Modal content-->
    <div class="modal-content">
      
      <div class="modal-body">
        @if( Session::has( 'payment_status' ))
        <p>Your payment was {{ Session::get( 'payment_status' ) }}.</p>
        @endif
      </div>
      <div class="modal-footer">
            <a href="{{ url('/') }}" class="btn btn-primary">Go To Website</a>
      </div>
    </div>

  </div>
</div>

<div id="notverifyModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-sm modal-dialog-centered">

    <!-- Modal content-->
    <div class="modal-content">
      
      <div class="modal-body">        
        <a href="" class="btn btn-danger">Please Verify Your account to get this service.</a>
      </div>
      <div class="modal-footer">
            
      </div>
    </div>

  </div>
</div>    


<script type="text/javascript">
$(window).on('load',function(){
    var payment_status = '<?php echo (Session::has( 'payment_status' )) ? Session::get( 'payment_status' ) : ""; ?>';
    if(payment_status != "")
    {
      $('#payment_status').modal('show');
    }

    //for active plan
    $('#upgrade_myModal').on('show.bs.modal', function(e) {
      var active_plan = $(e.relatedTarget).data('plan');
      //alert(active_plan);
      if(active_plan == 'premium')
      {
        $('#premium_title').show();
        $('#premium_footer').show();
        $('#credit_title').hide();
        $('#active_plan li:nth-child(1)').show();
        $('#active_plan li:nth-child(1)').addClass('active');
        $('#active_plan li:nth-child(2)').hide();
        
        $('#premium_tab').addClass('active');
        $('#credit_tab').removeClass('active');
      }
      if(active_plan == 'credit')
      {
        $('#premium_title').hide();
        $('#premium_footer').hide();
        $('#credit_title').show();
        $('#active_plan li:nth-child(2)').show();
        $('#active_plan li:nth-child(2)').addClass('active');
        $('#active_plan li:nth-child(1)').hide();

        $('#credit_tab').addClass('active');
        $('#premium_tab').removeClass('active');
      }
    });
});
</script>
<script>
    function select_plan(plan_type,plan){
        $('#plan_type').val(plan_type);
        $('#plan').val(plan);
        
        $('.read').css('background','#ec155a');
        $('.upgrade-btn').removeClass('activebutton');
        if(plan_type == 'diamonds'){
           $('#diamond'+plan).css('background','black'); 
        }
        if(plan_type == 'coins'){
           $('#coin'+plan).css('background','black'); 
        }
        if(plan_type == 'credits'){            
           //$('#credit'+plan).css('background','yellow');
           //alert(credit'+plan);
           $('#credit'+plan).addClass('activebutton');
        }
        if(plan_type == 'premiums'){
           //$('#premium'+plan).css('background','black');
           $('#premium'+plan).addClass('activebutton');
        }
        
    }
</script>
<script>
    function payment_methods(){
		
		$.ajax({
                headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '<?php echo url("/")?>/getPaynl',
                type: 'GET',
                cache : false,
                processData: false,
                success: function(data)
                {
                 console.log(data);
				 $('#paynl_id').show();
                 $('#paynl_id').html(data);
                }
            });
        
    }
	function hidePaynl(){
		$('#paynl_id').hide();
    }
</script>



<script type="text/javascript">
   getCity('United Kingdom');
   getRegion('United Kingdom');	
  function getCity(name){
        //alert(name);
        $.ajax({
                headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '<?php echo url("/")?>/getCity',
                type: 'POST',
                cache : false,
                processData: false,
                data: 'name='+name, // serializes the form's elements.
                success: function(data)
                {
                 console.log(data);
                 $('#city').html(data);
                 $('#prof_city1').html(data);
                }
            });
    }
	function getRegion(name){
        //alert(name);
        $.ajax({
                headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '<?php echo url("/")?>/getRegion',
                type: 'POST',
                cache : false,
                processData: false,
                data: 'name='+name, // serializes the form's elements.
                success: function(data)
                {
                 console.log(data);
                 $('#prof_region').html(data);
                 $('#prof_region1').html(data);
                 $('#prof_region2').html(data);
                }
            });
    }
	function getCityByState(name){
        