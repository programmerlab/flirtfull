
<?php $__env->startSection('content'); ?>

<?php 
if(isset($get_user->id) && ($get_user->id == Auth::id()))
{
  $self_user = $get_user->id;
}  
else{
  $self_user = 0;
}
?>
<section class="search_list_outer search_detail">
	<div class="container-fluid">
    	<div class="col-md-9 col-sm-8 main-content">
        <div class="setting_panel">
            	 <h3>Settings</h3>

            	  <?php if( Session::has( 'success' )): ?>
				  <div class="row">
				   <div class="col-md-12">
				    <div class="alert alert-success">
				      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				      <?php echo e(Session::get( 'success' )); ?>

				    </div>
				   </div>
				  </div>
				  <?php endif; ?>
				  <?php if( Session::has( 'error' )): ?>
				  <div class="row">
				   <div class="col-md-12">
				    <div class="alert alert-danger">
				      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				      <?php echo e(Session::get( 'error' )); ?>

				    </div>
				   </div>
				  </div>
				  <?php endif; ?>
                 	<ul>
                    	<!-- <li>
                        	<div class="SettingsListLink">
                            <div class="row">
                            	<div class="col-sm-4"><span class="setting_tag">Email Id</span></div>
                                <div class="col-sm-4"><span class="setting_field"><?php echo e($get_user->email); ?></span></div>
                                <div class="col-sm-4"><span class="setting_action"><i class="fa fa-pencil"></i> Edit</span></div>
                              </div>
                            </div>
                            <div class="Settings_detail">
                            	<form id="change_email">
                                	<table>
                                    	<tr>
                                    	<td>
                                        	<label>Email</label>
                                            <div class="form-group">
                                            	<input type="email" id="s_email" value="<?php echo e($get_user->email); ?>" class="form-control">
                                            	<span class="error-message s_email"></span>
                                            </div>
                                        </td>
                                    	<td>
                                        	<label>Name</label>
                                            <div class="form-group">
                                            	<input type="text" id="s_name" value="<?php echo e($get_user->name); ?>" class="form-control">
                                            	<span class="error-message s_name"></span>
                                            </div>
                                        </td>
                                        </tr>
                                        <tr>
                                        	<td class="text-center" colspan="3"><button type="submit" class="change-btn">Keep Chnage</button><button type="button" class="cancle_btn">Cancel</button></td>
                                        </tr>
                                    </table>
                                </form>
                            </div>
                        </li>
                        
                        <li>
                        	<div class="SettingsListLink">
                            <div class="row">
                            	<div class="col-sm-4"><span class="setting_tag">Add Phone Number</span></div>
                                <div class="col-sm-4"><span class="setting_field"><?php echo e($get_user->phone); ?></span></div>
                                <div class="col-sm-4"><span class="setting_action"><i class="fa fa-pencil"></i> Edit</span></div>
                             </div>
                            </div>
                            <div class="Settings_detail">
                            	<form  id="change_phone">
                                	<table>
                                    	<tr><td>
                                        	<label>Phone Number</label>
                                            <div class="form-group">
                                            	<input type="tel" id="s_phone" value="<?php echo e($get_user->phone); ?>" class="form-control">
                                            	<span class="error-message s_phone"></span>
                                            </div>
                                        </td>
                                        </tr>
                                        <tr>
                                        	<td class="text-center"><button type="submit" class="change-btn">Keep Chnage</button><button type="button" class="cancle_btn">Cancel</button></td>
                                        </tr>
                                    </table>
                                </form>
                            </div>
                        </li> -->
                         <li>
                        	<div class="SettingsListLink">
                            <div class="row">
                            	<div class="col-sm-4"><span class="setting_tag">Change Password</span></div>
                                <!-- <div class="col-sm-4"><span class="setting_field">*********</span></div>
                                <div class="col-sm-4"><span class="setting_action"><i class="fa fa-pencil"></i> Edit</span></div> -->
                              </div>
                            </div>
                            <div class="Settings_detail">
                            	<form  id="change_password">
                                	<table>
                                    	<!-- <tr>
                                    	<td>
                                        	<label>Password</label>
                                            <div class="form-group">
                                            	<input type="password" value="*********" class="form-control" disabled>
                                            </div>
                                        </td>
                                        </tr> -->
                                        <tr>
                                    	<td>
                                        	<div class="form-group">
                                            	<input type="password" id="current_password" class="form-control" placeholder="Current Password">
                                            	<span class="error-message current_password"></span>
                                            	
                                            </div>
                                        </td>
                                        </tr>
                                        <!-- <tr><td><h5>New Password</h5></td></tr> -->
                                        <tr><td>
                                            <div class="form-group">
                                            	<input type="password" id="s_password" placeholder="Enter Password" class="form-control" >
                                            	<span class="error-message s_password"></span>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="form-group">
                                            	<input type="password" id="s_cpassword"  placeholder="Enter Confirm Password" class="form-control" >
                                            	<span class="error-message s_cpassword"></span>
                                            </div>
                                        </td>
                                        </tr>
                                        <tr>
                                        	<td class="text-center" colspan="2"><button type="submit" class="change-btn">Keep Chnage</button><button type="button" class="cancle_btn">Cancel</button></td>
                                        </tr>
                                    </table>
                                </form>
                            </div>
                        </li>
                        
                        <li>
                        	<div class="SettingsListLink">
                            <div class="row">
                            	<div class="col-sm-4"><span class="setting_tag">Delete my Account</span></div>
                                
                              </div>
                            </div>
                            <div class="Settings_detail">
                            	<form id="delete_ac_form" method="post" action="<?php echo e(url('delete-account')); ?>">
                                	<div class="delete_ac_form">
                                		<p>By clicking the button below, i agree that my complete message history and account will be erased.</p>
                                		<div>
                                			<button type="submit" class="delte_btn">Delete my account</button>	
                                		</div>
                                		
                                	</div>
                                </form>
                            </div>
                        </li>
                    </ul>
                    
        </div>
       
        	
        </div>
        
        
    </div>
</section>
<script>
$(document).ready(function(){ 
    $("#additional-options").click(function() {
        
		$(".additional-options").slideToggle();
    }); 

    $("#delete_ac_form").click(function(e) {
        
		if(!confirm('Are you sure?')){
            e.preventDefault();
            return false;
        }
        return true;
    }); 
});
</script>

<script>
$(document).ready(function(){ 
	$(".Settings_detail").show();
    /*$(".SettingsListLink").click(function() {
       $(".Settings_detail").slideUp();
	   $(".SettingsListLink").show();
	   $(this).hide();
		$(this).next(".Settings_detail").slideDown();
    }); 
	$(".cancle_btn").click(function() {
	   $(".SettingsListLink").show();
	   $(".Settings_detail").slideUp();
	}); */
});
</script>
<script type="text/javascript">
$(document).ready(function(){
	// change email
	$('form#change_email').on('submit',function(e){
	    e.preventDefault();
	    var error =0;
	    var user_id = '<?php echo $self_user; ?>';
	    var email = $('#s_email').val();
	    var name = $('#s_name').val();
	    var mail_validation = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	   

	    if(email.trim() == "")
	    {
	      $('.s_email').text('Please enter email');
	      $("#s_email").css("border-color", "red");
	      error++;
	    }
	    else if(!mail_validation.test(email)){
	      $('.s_email').text('Please enter valid email');
	      $("#s_email").css("border-color", "red");
	      error++;
	    }
	    else{
	      $('.s_email').text('');
	      $("#s_email").css("border-color", "#eee");
	    }

	    if(name.trim() == "")
	    {
	      $('.s_name').text('Please enter name');
	      $("#s_name").css("border-color", "red");
	      error++;
	    }
	    else{
	      $('.s_name').text('');
	      $("#s_name").css("border-color", "#eee");
	    }
	    
	    if(error == 0)
	    {
	    	$.ajax({
		        headers: {
		          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		        },
		        url: '<?php echo url("/")?>/change-email',
		        type: 'POST',
		        data: 'email='+email+'&name='+name+'&user_id='+user_id, // serializes the form's elements.
		        success: function(data)
		        {
		         if(data == 'success')
		         {
		          location.reload();
		         }
		         else{
		          //alert('Something went wrong.');
		          location.reload();
		         }
		        }
		    });
	    }
	    else{

	    }
	});

	//change phone
	$('form#change_phone').on('submit',function(e){
	    e.preventDefault();
	    var error =0;
	    var user_id = '<?php echo $self_user; ?>';
	    var phone = $('#s_phone').val();
	    var phone_validation = /^(?=.*[0-9])[- +()0-9]+$/;
	   
		if(phone.trim() == "")
	    {
	      $('.s_phone').text('Please enter phone');
	      $("#s_phone").css("border-color", "red");
	      error++;
	    }
	    else if(!phone_validation.test(phone)){
	      $('.s_phone').text('Please enter valid phone number');
	      $("#s_phone").css("border-color", "red");
	      error++;
	    }
	    else{
	      $('.s_phone').text('');
	      $("#s_phone").css("border-color", "#eee");
	    }
	    
	    if(error == 0)
	    {
	    	$.ajax({
		        headers: {
		          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		        },
		        url: '<?php echo url("/")?>/change-phone',
		        type: 'POST',
		        data: 'phone='+phone+'&user_id='+user_id, // serializes the form's elements.
		        success: function(data)
		        {
		         if(data == 'success')
		         {
		          location.reload();
		         }
		         else{
		          //alert('Something went wrong.');
		          location.reload();
		         }
		        }
		    });
	    }
	    else{

	    }
	});

	//change password

	$('form#change_password').on('submit',function(e){
	    e.preventDefault();
	    var error =0;
	    var user_id = '<?php echo $self_user; ?>';
	    var current_password = $('#current_password').val();
	    var password = $('#s_password').val();
	    var cpassword = $('#s_cpassword').val();
	    
	    
	    if(current_password.trim() == "")
	    {
	      $('.current_password').text('Please enter current password');
	      $("#current_password").css("border-color", "red");
	      error++;
	    }
	    else{
	    	$('.current_password').text('');
	        $("#current_password").css("border-color", "#eee");
	    }
		if(password.trim() == "")
	    {
	      $('.s_password').text('Please enter password');
	      $("#s_password").css("border-color", "red");
	      error++;
	    }
	    else if(cpassword.trim() == "")
	    {
	      $('.s_cpassword').text('Please enter confirm password');
	      $("#s_cpassword").css("border-color", "red");
	      error++;
	    }
	    else if(password != cpassword){
	      $('.s_cpassword').text('Both password should be same.');
	      $("#s_cpassword").css("border-color", "red");
	      error++;
	    }
	    else{
	      $('.s_password').text('');
	      $("#s_password").css("border-color", "#eee");
	      $('.s_cpassword').text('');
	      $("#s_cpassword").css("border-color", "#eee");
	    }
	    
	    if(error == 0)
	    {
	    	$.ajax({
		        headers: {
		          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		        },
		        url: '<?php echo url("/")?>/change-password',
		        type: 'POST',
		        data: 'password='+password+'&current_password='+current_password+'&user_id='+user_id, // serializes the form's elements.
		        success: function(data)
		        {
		         	$('.current_password').text('');
	        		$("#current_password").css("border-color", "#eee");	
		         if(data == 'success')
		         {
		          location.reload();
		         }
		         else if(data == 'error')
		         {
		         	$('.current_password').text('Current password is not match');
	      			$("#current_password").css("border-color", "red");
		         }
		         else{
		          //alert('Something went wrong.');
		          location.reload();
		         }
		        }
		    });
	    }
	    else{

	    }
	});

});
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout.default', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>