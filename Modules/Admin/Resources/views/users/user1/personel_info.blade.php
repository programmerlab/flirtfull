<div class="tab-pane active" id="tab_1_1"> 
<div class="portlet light bordered">
 
   
    <div class="form-group {{ $errors->first('name', ' has-error') }}">
        <label class="control-label">Display Name</label>
        <input type="text" placeholder="Display Name" class="form-control" name="name" 
        value="{{ ($user->name)?$user->name:old('name')}}"> </div>
    
    <div class="form-group {{ $errors->first('email', ' has-error') }}">
        <label class="control-label ">Email</label>
        <input type="email" placeholder="Email" class="form-control" name="email" value="{{ ($user->email)?$user->email:old('email')}}"> 
    </div>
    <div class="form-group {{ $errors->first('password', ' has-error') }}">
        <label class="control-label">Password</label>
        <input type="password" placeholder="******" class="form-control" name="password"> 
    </div>

     <input type="hidden" value="3" name="role_type">
    
    <div class="form-group {{ $errors->first('phone', ' has-error') }}">
        <label class="control-label">Mobile Number</label>
        <input type="text" placeholder="Mobile or Phone" class="form-control phone" name="phone"  value="{{ ($user->phone)?$user->phone:old('phone')}}">
    </div>
 
    <div class="form-group {{ $errors->first('gender', ' has-error') }}">
        <label class="control-label">Gender</label>
        <select name="gender" class="form-control">
            <option value="1" {{ ($user->gender == '1') ? 'selected' : ''}} >Male</option>
            <option value="2" {{ ($user->gender == '2') ? 'selected' : ''}}>Female</option>
        </select>            
    </div>
    
    <div class="form-group {{ $errors->first('dob', ' has-error') }}">
        <label class="control-label">Date of Birth</label>
        <input type="text" placeholder="Date of Birth" class="form-control" name="dob"  value="{{ ($user->dob)?$user->dob:old('dob')}}">
    </div>
        
    <div class="form-group {{ $errors->first('address', ' has-error') }}">
        <label class="control-label">Address</label>
        <input type="text" placeholder="Address" class="form-control" name="address"  value="{{ ($user->address)?$user->address:old('address')}}">
    </div>
    
    <div class="form-group {{ $errors->first('country', ' has-error') }}">
        <label class="control-label">Country</label>
        <input type="text" placeholder="Country" class="form-control" name="country"  value="{{ ($user->country)?$user->country:old('country')}}">
    </div>
        
    <div class="form-group {{ $errors->first('city', ' has-error') }}">
        <label class="control-label">City</label>
        <input type="text" placeholder="City" class="form-control phone" name="city"  value="{{ ($user->city)?$user->city:old('city')}}">
    </div>    
    
    <div class="form-group {{ $errors->first('status', ' has-error') }}">
        <label class="control-label">Status</label>
        <select name="status" class="form-control">
            <option value="1" {{ ($user->status == '1') ? 'selected' : ''}} >Active</option>
            <option value="0" {{ ($user->status == '0') ? 'selected' : ''}}>Deactive</option>
        </select>            
    </div>    
          
    <div class="margin-top-10"> 
         <button type="submit" class="btn green btn-primary" value="avtar" name="submit"> Save Changes </button>        
        <button type="reset" class="btn default"> Cancel </button>
    </div>      
             
</div>
</div>
 