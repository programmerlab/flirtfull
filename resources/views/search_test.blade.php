@extends('layout.default')
@section('content')

<section class="search_list_outer">
	<div class="container-fluid">
    	<div class="row">
            <div class="col-md-9 col-sm-8 pdng main-content search_content_left">
            	<div class="profile_search_bar">
            		<form>
            			<div class="form_group">
            				<label class="label">Name</label>
            				<div class="form_input">
            					<input type="text" name="profile_name" placeholder="Profile Name">
            				</div>
            			</div>
            			<div class="form_group">
            				<label class="label">find Me</label>
            				<div class="form_input">
            					<select>
            						<option value="all">All</option>
            						<option value="women">women</option>
            						<option value="men">men</option>
            					</select>
            				</div>
            			</div>
            			<div class="form_group age_group">
            				<label class="label">Age</label>
            				<div class="form_input">
            					<select name="min_age">
		              			  <option value="all">all</option>
		              			  <option value="18">18</option>
			                      <option value="20">20</option>
			                      <option value="25">25</option>
			                      <option value="30">30</option>
			                      <option value="35">35</option>
			                      <option value="40">40</option>
			                      <option value="45">45</option>
			                      <option value="50">50</option>
			                      <option value="55">55</option>
			                      <option value="60">60</option>
			                      <option value="65">65</option>
			                      <option value="70">70</option>
			                      <option value="75">75</option>
			                    </select>
			                    <select name="max_age">
			                      <option value="all">all</option>
			                      <option value="20">20</option>
			                      <option value="25">25</option>
			                      <option value="30">30</option>
			                      <option value="35">35</option>
			                      <option value="40">40</option>
			                      <option value="45">45</option>
			                      <option value="50">50</option>
			                      <option value="55">55</option>
			                      <option value="60">60</option>
			                      <option value="65">65</option>
			                      <option value="70">70</option>
			                      <option value="75">75</option>
			                      <option value="80+">80+</option>
			                    </select>
            				</div>
            			</div>
            			<div class="form_group">
            				<label class="label">In</label>
            				<div class="form_input">
            					<select>
            						<option value="all">All Regions</option>
            						<option value="East Midlands">East Midlands</option>
            						<option value="East of England">East of England</option>
            						<option value="Greater London">Greater London</option>
            					</select>
            				</div>
            			</div>
            			<div class="form_group online_group">
            				<div class="form_input check_box">
            					<label>
            						<input type="checkbox" name="">
            						<span class="c_box"></span>
            						<span class="c_text">Onilne Now</span>
            					</label>
            				</div>
            			</div>
            			<div class="form_group button_group">
            				<div class="form_input">
                              <button type="submit" value="Search" class="pro_search_btn">Search</button>
                            </div>
                        </div>
            		</form>
                </div>
	            <div class="user_profile_popup all-users" id="profile_popup">
                   @include('partial/all_users')
	            </div>
	            <textarea id="chat_file_hidden" style="display:none"></textarea>     
           
            	<!--  chat popup  --> 
               <div id="fake_chat" class="profile_fake_chat">
               </div>
           		<!--  chat popup  -->
            </div>
            <div class="clearfix"></div>
            @include('chat_sidebar');
        </div>
    </div>
</section>


<!-- Region modal Start -->
      <div id="region_popup" class="splash_screen_modal">
         <div class="popup_modal_dialog">
            <div class="splash_content_bg">
               <div class="s_overlay"></div>
               <div class="splash_content">
                  <div class="splash_form">
                     <h2>What is your location</h2>
                     <form class="" id="region_form">
                        <div class="form_group">
                           <label class="label">Select your region</label>
                           <div class="input_group">
								<select name="prof_region" id="prof_region2" class="form-control" onChange="getCityByState(this.value);">
									<option selected="" value="">Region</option>
								</select>
							  <p class="error-message region"></p>
                           </div>
                        </div>
						 <p class="region-process text-center">Processing...</p>
                         <div class="button_group">
                           <input type="submit" name="submit" value="Submit" class="form_btn">
                        </div>
                     </form>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!--Region modal End --> 

<!-- Register popup start -->
<div class="popup_modal regisration_modal" style="display:none;" id="passwordModal11">
  <div class="modal_dialog_big">
    <div class="popup_content">
      <div class="reg_form_wrap">
        <div class="form_left_part">
          <div class="reg_img">
            @if($adminid)
            <img src="{{ asset('public/thumbnail/'.$adminid->profile_image) }}">
            @endif
          </div>
          <div class="reg_imgtext">
            <h4>{{ $adminid->name.', '.$adminid->age }} <br> {{ ($adminid->state)?$adminid->state:'' }} </h4>            
          </div>
        </div>
        <div class="form_right_part">
          <h3>Hii, {{ Auth::user()->name }}</h3>
          <p>This is <strong>Kate</strong>, the website admin of Flirtfull.com. Congratulation!!!!! Your Flirtfull account has been activated. We just need three more entries from you!</p>            
          <div class="reg_form">
            <form method="post" id="oform" action="{{ url('/update-outer-user') }}">
              <!--<h4>First lets start with a screen name</h4>
              <div class="form_group">
                <label>Profile Name<span>*</span></label>
                <div class="form_input">
                  <input type="text" name="name" id="oname" placeholder="Enter Your Profile Name">
                  <span id="oname_error" style="display:none; color:red;">This field is required</span>
                  <span id="ouniquename_error" style="display:none; color:red;">This name is already exists, please choose different one.</span>  
                </div>
              </div>-->
              <h4>For better/proper matches on your plateform please enter you birthday</h4>
               <!-- Row Start -->
              <div class="row">
                <div class="col-sm-4">
                  <div class="form_group">
                    <label>Day<span>*</span></label>
                    <div class="form_input">
                      <select id="oday" name="day" class="" >                          
                          <?php
                            for ($m=1; $m<=31; $m++) 
                            {
                              echo "<option value='".$m."'>".$m."</option>";
                            }
                          ?>
                        </select>
                        <span id="oday_error" style="display:none; color:red;">This field is required</span>    
                    </div>
                  </div>
                </div>
                <div class="col-sm-4">
                  <div class="form_group">
                    <label>Month<span>*</span></label>
                    <div class="form_input">
                      <select id="omonth" name="month" class="" >                          
                          <?php
                            for ($m=1; $m<=12; $m++) 
                            {
                              $month_name = date('F', mktime(0,0,0,$m, 1, date('Y')));
                              echo "<option value='".$m."'>".$month_name."</option>";
                            }
                          ?>
                        </select>
                        <span id="omonth_error" style="display:none; color:red;">This field is required</span>    
                    </div>
                  </div>
                </div>
                <div class="col-sm-4">
                  <div class="form_group">
                    <label>Year<span>*</span></label>
                    <div class="form_input">
                      <select id="oyear" name="year" class="" >
                          <option value="" class="label" selected="selected" >year</option>
                          <?php 
                          for($i=2001;$i>=1930;$i--)
                          {
                            echo '<option value="'.$i.'">'.$i.'</option>';
                          }
                          ?>
                        </select>
                        <span id="oyear_error" style="display:none; color:red;">This field is required</span>    
                    </div>
                  </div>
                </div>
              </div>
              <!-- Row end -->
             <!-- <div class="row">
                <div class="col-sm-12">
                  <div class="form_group">
                    <label>Region<span>*</span></label>
                    <div class="form_input">
                        <select id="oregion" name="region" class="" >
                          <option value="" class="label" selected="selected" >Region</option>
                          <?php
                            for ($m=1; $m<=12; $m++) 
                            {
                              $month_name = date('F', mktime(0,0,0,$m, 1, date('Y')));
                              echo "<option value='".$m."'>".$month_name."</option>";
                            }
                          ?>
                        </select>                      
                      <span id="oregion_error" style="display:none; color:red;">This field is required</span>                      
                    </div>
                  </div>
                </div>                
              </div>-->
              <!-- Row end -->  
              <h4>Please set a password to secure your account</h4>
              <!-- Row start -->
              <div class="row">
                <div class="col-sm-6">
                  <div class="form_group">
                    <label>Enter password<span>*</span></label>
                    <div class="form_input">
                      <input type="password" id="opass" name="password" placeholder="Enter Password">
                      <span id="opass_error" style="display:none; color:red;">This field is required</span>
                      <span id="ominpass_error" style="display:none; color:red;">This field should be minimum of 4 characters</span>  
                      <span id="oconfpass_error" style="display:none; color:red;">Confirm Password doesn't match</span>  
                    </div>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form_group">
                    <label>Verify password<span>*</span></label>
                    <div class="form_input">
                      <input type="password" id="orepass" placeholder="confirm Password">
                      <span id="orepass_error" style="display:none; color:red;">This field is required</span>
                    </div>
                  </div>
                </div>
              </div>
              
              <div class="form-group">
                <div class="check_box">
                  <input type="checkbox" id="oterms" value="1"><span> I accept the <a target="_blank" href="{{ url('terms-conditions') }}">Terms & Conditions</a> &amp; <a target="_blank" href="{{ url('privacy-policy') }}">Privacy Policy</a></span>
                  <span id="oterms_error" style="display:none; color:red;">This field is required</span>  
                </div>
              <div>
              <p>By clicking «Sign Up» you agree to our <a href="{{ url('terms-conditions') }}" style="font-weight: 600;color: #000;">Terms and Conditions</a></p>
              <span class="submit_form" onclick="submit_form11();">submit</span>
              <!-- Row end -->
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
    function submit_form11(){
        if($('#oname').val() == ''){
            $('#oname_error').show();
            return false;
        }
        $('#oname_error').hide();
        
        if($('#oday').val() == ''){
            $('#oday_error').show();
            return false;
        }
        $('#oday_error').hide();
        if($('#omonth').val() == ''){
            $('#omonth_error').show();
            return false;
        }
        $('#omonth_error').hide();
        if($('#oyear').val() == ''){
            $('#oyear_error').show();
            return false;
        }
        $('#oyear_error').hide();
        
        if($('#oregion').val() == ''){
            $('#oregion_error').show();
            return false;
        }
        $('#oregion_error').hide();
        
        if($('#opass').val() == ''){
            $('#opass_error').show();
            return false;
        }
        $('#opass_error').hide();
        
        if($('#opass').val().length <= 3){
            $('#ominpass_error').show();
            return false;
        }
        $('#ominpass_error').hide();
        
        if($('#orepass').val() == ''){
            $('#orepass_error').show();
            return false;
        }
        $('#orepass_error').hide();
        
        if($('#opass').val() != $('#orepass').val()){
            $('#oconfpass_error').show();
            return false;
        }
        $('#oconfpass_error').hide();
        
        if($('#oterms').is(':not(:checked)')){
            $('#oterms_error').show();
            return false;
        }
        $('#oterms_error').hide();
        
        $.ajax({
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '<?php echo url("/")?>/update-outer-user',
            data: {
                name: $('#oname').val() ,                
                password: $('#opass').val(),
                month: $('#omonth').val(),
                day: $('#oday').val(),
                year: $('#oyear').val(),
                region: $('#oregion').val()
            },
            type: "POST",
            dataType : "json",
            success: function( data ) {               
                if(data.success)
                {                  
                  location.reload();                  
                }else{
                  $('#ouniquename_error').show();                  
                }                                
                                
              },
              error: function( xhr, status, errorThrown ) {               
                alert( "Sorry, there was a problem!" );
              }
          });
        //$('#oform').submit();
    }
</script>    
<!-- Register popup End -->

<!-- confirm popup -->
<div class="n_conmfirm_popups open_popup" id="n_confrim_popup">
  <div class="modal_dialog_big">
    <div class="confirm_p_content">
      <div class="confirm_content_wrap">
        <div class="cnfrm_right_img">
          <div class="cnfrm_img">
            <img src="public/images/confrim_img.png" alt="" class="img-responsive" />
          </div>
        </div>
        <div class="cnfrm_left_content">
          <div class="cnfrm_inner_cover">
            <h1 class="cong_heading">Congratulations!</h1>
            <h3 class="success_heading">You've successfully created your profile on <span>Flirtfull</span></h3>
            <div class="email_text">Open the email from us and click on the <span>Activate my account</span> button. </div>
            <div class="email_text_2">Your email is <span>{{ Auth::user()->email }}</span></div>
            <div class="email_button">
              <a href="#">Go to your e-mail</a>
            </div>
            <div class="resend_mail_link" id="resend_email1">
              <a href="#" onclick="resend_verification('{{ Auth::id() }}')">Resend activation e-mail</a>
            </div>
        
            <div class="bottom_p_notes">
              <h4>Important:</h4>
              <ul>
                <li>it can take up to 5 minutes for the activation email to drive</li>
                <li>Check your email account's 'Spam/Junk' folder to make sure our email hasn't landed there</li>
                <li>Make us as 'Not Spam/Junk' to ensure that you receive all important e-mail from <span>Flirtfull</span></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- confirm popup -->
<!-- confirm popup -->
<div class="n_conmfirm_popup open_popup" id="n_confrim_popup">
  <div class="modal_dialog_big">
    <div class="confirm_p_content">
      <div class="close_popup"><i class="fa fa-times"></i></div>
      <div class="confirm_content_wrap">
        <div class="cnfrm_right_img">
          <div class="cnfrm_img">
            <img src="public/images/confrim_img.png" alt="" class="img-responsive" />
          </div>
        </div>
        <div class="cnfrm_left_content">
          <div class="cnfrm_inner_cover">
            <h1 class="cong_heading">Congratulations!</h1>
            <h3 class="success_heading">You've successfully created your profile on <span>Flirtfull</span></h3>
            <div class="email_text">Open the email from us and click on the <span>Activate my account</span> button. </div>
            <div class="email_text_2">Your email is <span>eyesmfi@gmail.com</span></div>
            <div class="email_button">
              <a href="#">Go to your e-mail</a>
            </div>
            <div class="resend_mail_link">
              <a href="#">Resend activation e-mail</a>
            </div>
            <div class="bottom_p_notes">
              <h4>Important:</h4>
              <ul>
                <li>it can take up to 5 minutes for the activation email to drive</li>
                <li>Check your email account's 'Spam/Junk' folder to make sure our email hasn't landed there</li>
                <li>Make us as 'Not Spam/Junk' to ensure that you receive all important e-mail from <span>Flirtfull</span></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- confirm popup -->
<script>
    setTimeout(function()
			{
				$(".n_conmfirm_popup").addClass("open_popup");
			}, 2000);
    <?php if((Auth::check()) && Auth::user()->status == 0 && Session::has('resendEmail') && (Session::get('resendEmail') == 1) ) { ?>      
        setTimeout(function()
			{
				$(".n_conmfirm_popup").addClass("open_popup");
			}, 2000);
                    
      
    <?php   } ?>
</script>
<script>
    //confirm popup js
    $(".close_popup").on('click', function(){
    $(this).parents(".n_conmfirm_popup").removeClass("open_popup");
    });
    
    $(".n_conmfirm_popup").on('click', function(){
    $(this).removeClass("open_popup");
    });
    $(".confirm_p_content").on('click', function(event){
    event.stopPropagation();
    });
</script>    
<!-- notice popup -->
  <div class="notice_popup">
    <div class="notice_dialog">
      <div class="notice_popup_content">
        <div class="popup_header">
          <h1>IMPORTANT NOTICE:</h1>
        </div>
        <div class="center_content">
          <h4>You state that the following facts are accurate</h4>
          <ul>
            <li>I am at least 18-years old.</li> 
            <li>I will not redistribute any material from the website.</li>
            <li>I will not allow any minors to access the website or any material found in it</li>
            <li>Any material I view or download from the website is for my own personal use and I will not show it to a minor.</li>
            <li>
              Sexually explicit material depicting kinky and other fetish activities is allowed by the local law governing my jurisdiction.
            </li>
            <li>
              I was not contacted by the suppliers of this material, and I willingly choose to view or download it
            </li>
            <li>
                I am entering this website because it has adult content, and pictures depicting men or women in various sexual situations is not obscene or offensive in any way. In addition, I do not believe that this material could be considered obscene or offensive.
            </li>
             <li>
                I acknowledge that the website includes fictitious angel profiles created and operated by the website that may communicate with me for entertainment and other purposes. I also understand that physical encounters with this angel profiles are not possible. 
            </li>
             <li>
                I acknowledge that individuals appearing in photos on the landing page or in angel profiles might not be actual members of the website and that certain data is provided for illustration and entertainment purposes only.
            </li>
             <li>
                I acknowledge that the website does not inquire into the background of its members and the website does not otherwise attempt to verify the accuracy of statements made by its members.
            </li>
              <li>
                I acknowledge that the website does not guarantee that I will find a date or that I will meet any of its members in person or that any given person or profile manifested on the website is available or interested in dating or communicating with me or anyone else.
            </li> 
            <li>
                I acknowledge that my use of the website is governed by the website’s <a target="_blank" href="{{ url('terms-conditions') }}">Terms and condition</a> and the website’s <a target="_blank" href="{{ url('privacy-policy') }}">Privacy Policy</a>, which I have carefully reviewed and accepted, and I am legally bound by the <a target="_blank" href="{{ url('terms-conditions') }}">Terms and condition</a> and the <a target="_blank" href="{{ url('privacy-policy') }}">Privacy Policy.</a>
            </li>
            <li>
                By entering the website, I am subjecting myself to the exclusive personal jurisdiction of the Netherlands should any dispute arise between the owner or operator of the website and myself in accordance with the <a href="{{ url('terms-conditions') }}">Terms and condition</a>.
            </li>
            <li>
                By entering the website, I am subjecting myself to binding arbitration in the Netherlands should any dispute arise at any time between the owner or operator of the website and myself in accordance with the <a href="{{ url('terms-conditions') }}">Terms and condition</a>.
            </li>
             <li>
                By entering the website, I will have released and discharged the providers, owners, and creators of the website from all liability that might arise.
            </li>
            <li>
              By entering the website, my personal data will be stored and processed, in accordance with the <a target="_blank" href="{{ url('privacy-policy') }}">Privacy Policy.</a>
            </li>
          </ul>
        </div>
        <div class="popup_footer_content">
          <p>
            By clicking Agree and continue, you state that all the above is true, that you want to enter the Website, and that you agree to the <a target="_blank" href="{{ url('terms-conditions') }}">Terms and conditions</a> &amp; <a target="_blank" href="{{ url('privacy-policy') }}">Privacy Policy</a>
          </p>
          <div class="popup_confirm_buttons">
            <button class="leave_btn buttons">Disagree and leave</button>
            <button class="confirm_btn buttons">Agree and continue</button>
          </div>
        </div>
      </div>
    </div>
  </div>
<!-- notice popup -->    
<script>  
  //notice popup open js
  $(window).on('load', function(){
	
	if(localStorage.getItem('popState') != 'shown')
	{
         <?php if($rand != 0) {?>
			setTimeout(function()
			{
				$(".notice_popup").addClass("open_popup");
			}, 2000);
         <?php } ?>   
        localStorage.setItem('popState','shown')
    } 
   
  });
  $(".confirm_btn").on('click', function(){
    $(this).parents(".notice_popup").removeClass("open_popup");
    $('#passwordModal1').modal('show');
    $('#passwordModal1').modal({backdrop: 'static', keyboard: false});
  });
   $(".leave_btn").on('click', function(){
   window.location.href='https://www.google.com/';
  });
  
</script>    
<script type="text/javascript">
  //var page = 1;
  var is_login = '<?php echo (Auth::id()); ?>';
  /*$(window).scroll(function() {  
      if($(window).scrollTop() + $(window).height() >= $(document).height()- 0.5) {
          page++;
          if(is_login){
            loadMoreData(page);
          }
      }
  });*/

  /*$(window).on('hashchange', function() {
        if (window.location.hash) {
            var page = window.location.hash.replace('#', '');
            if (page == Number.NaN || page <= 0) {
                return false;
            }else{
                if(is_login){
                  loadMoreData(page);
                }
            }
        }
    });*/

  $(document).on('click', '.pagination a',function(event){
      event.preventDefault();

      $('li').removeClass('active');
      $(this).parent('li').addClass('active');

      var myurl = $(this).attr('href');
      var page=$(this).attr('href').split('page=')[1];

      if(is_login){
        loadMoreData(page);
      }
  });


  function loadMoreData(page){
    var currentURL = $(location).attr("href");
    var n = currentURL.includes("?");
    var url;
    if(n){
      currentURL = currentURL.split("?");
      url = '?page=' + page + '&' + currentURL[1];
    }
    else{
      url = '?page=' + page
    }
    $.ajax(
          {
              headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              },
              url: url,
              type: "get",
              beforeSend: function()
              {
                  $('.load-more').show();
              }
          })
          .done(function(data)
          { //console.log(data);
              if(data.html == ""){
                  $('.load-more').text("");
                  return;
              }
              $('.load-more').hide();
              //$("#all-users").append(data.html);
              $(".all-users").empty().html(data.html);
              //location.hash = page;
          })
          .fail(function(jqXHR, ajaxOptions, thrownError)
          {
                alert('server not responding...');
          });
  }
</script>

<script>
$(document).ready(function(){     
        $('#myScrollchat').hide();
		$("#profile_popup").removeClass("user_profile_popup");
});


$(window).load(function()
	{
		
		  setTimeout(function(){        
				//$("#region_popup").addClass("open_popup");
                //$('#passwordModal11').modal('show');
			  }, 2000);
		

		
	});
	
	
$(document).ready(function() 
{	
	$('.region-process').hide();
	
	$('#region_form').submit(function(e) 
	{
		e.preventDefault();
	
		var region = $('#prof_region2').val();
		var error =0;
	
		if(region == '')
		{
		  $('.region').show();
		  $('.region').text('Please select region');
		  error++;
		}
		else{
		  $('.region').hide();
		  $('.region').text('');
		}
	
		if(error == 0)
		{
			$('.region-process').show();
			$.ajax(
			{
				headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
				url: '<?php echo url("/")?>/region_credit',
				data: { region: region },
				type: "POST",
				dataType : "json",
				success: function( data ) 
				{
					$('.loader').hide();
					if(data.success)
					{
						$('.region-process').hide();
						location.reload();
					}
				},
				error: function( xhr, status, errorThrown ) 
				{
					$('.region-process').hide();
					alert( "Sorry, there was a problem!" );
				}
			});
		}
	
	});
	
});

</script>    

@endsection
