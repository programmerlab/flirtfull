@extends('layout.default')
@section('content')
    
	<section class="search_list_outer search_detail">
         <div class="container">
          <div class="row">
            <div class="col-md-12">
               <div class="profile_cover_pic">
                  <!-- <div class="profile_cover_pic_outer" style="background-image:url({{ asset('storage/uploads/pages') }}/{{ $page->image  }})"></div> -->
                  <!--<div class="profile_cover_panel">
                     <div class="profile_img"><img  src="{{ asset('public/images/logo.png') }}" alt="Profile Image"></div>
                     <div class="profile_user_info">
                        <div class="user_info_inner">
                           <h3>Flirtfull</h3>
                           <p><a href="mailto:support@oddigits.com">support@oddigits.com</a></p>
                        </div>
                     </div>
                  </div>-->
               </div>
               <div class="about_panel">
                  <h3>{{ ucwords($page->title) }}</h3>
                  {!!$page->description!!}
               </div>
            </div>
            </div>
         </div>
      </section>
      <script src="js/jquery.min.js"></script>
      <script src="js/bootstrap.min.js"></script>
      <script src="js/owl.carousel.js"></script>
      <script >
         $(document).ready(function() {
         var owl = $('#owl-carousel');
         owl.owlCarousel({
         items: 5,
         loop: true,
         margin: 10,
         autoplay: true,
         autoplayTimeout: 1000,
         autoplayHoverPause: true
         });
         $('.play').on('click', function() {
         owl.trigger('play.owl.autoplay', [1000])
         })
         $('.stop').on('click', function() {
         owl.trigger('stop.owl.autoplay')
         })
         })
      </script>
      <script>
         $('#owl-carousel1').owlCarousel({
         items:1,
         margin:10,
         autoHeight:true,
         autoplay: true,
         dots: true,
         });
      </script>
      <script>
         $(document).ready(function(){ 
         $("input[name$='mood']").click(function() {
         var test = $(this).val();
         var test1=$(this).parent(".mood_radio").find("img").attr("src");
         $("#mood_value").find(".mood-title").text( test );
         $("#mood_value").find("img").attr('src',test1)
         }); 
         });
      </script>
      <script>
         $(document).ready(function(){ 
         $("#additional-options").click(function() {
         
         $(".additional-options").slideToggle();
         }); 
         });
      </script>
@endsection	  