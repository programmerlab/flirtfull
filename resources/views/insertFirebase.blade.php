	<title>{{ env('APP_NAME') }}</title>
	<link rel="shortcut icon" type="image/png" href="{{ asset('public/images/fevicon.png') }}"/>
	<link href="{{ asset('public/css/font-awesome.min.css') }}" rel="stylesheet">
	<link href="{{ asset('public/css/bootstrap.min.css') }}" rel="stylesheet">
	<link rel="stylesheet" href="{{ asset('public/css/owl.carousel.min.css') }}">
  <!-- <link href="{{ asset('public/css/ninja-slider.css') }}" rel="stylesheet"> -->

  <link href="{{ asset('public/css/fotorama.css') }}" rel="stylesheet">
  <link rel="stylesheet" href="{{ asset('public/css/croppie.css') }}" />
  
  <link href="{{ asset('public/css/style.css') }}" rel="stylesheet">
  <link href="{{ asset('public/css/responsive.css') }}" rel="stylesheet">
  
  <script src="{{ asset('public/js/jquery.min.js') }}"></script>
  <script src="{{ asset('public/js/bootstrap.min.js') }}"></script>
  <script src="{{ asset('public/js/owl.carousel.js') }}"></script>
  <!-- <script src="{{ asset('public/js/ninja-slider.js') }}"></script> -->
  <script src="{{ asset('public/js/fotorama.js') }}"></script>
  <script src="https://maps.googleapis.com/maps/api/js?key={{ env('GOOGLE_KEY') }}&sensor=false&libraries=places" type="text/javascript"></script>
  <script src="{{ asset('public/js/croppie.js') }}"></script>
  <script src="https://cdn.ckeditor.com/4.11.4/standard/ckeditor.js"></script>
  <!-- Latest compiled and minified CSS -->

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.2/css/bootstrap-select.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>  
<!-- Latest compiled and minified JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.2/js/bootstrap-select.min.js"></script>
    
    
<!--Firebase chat conf.-->
<script src="https://www.gstatic.com/firebasejs/5.0.4/firebase-app.js"></script>
<script src="https://www.gstatic.com/firebasejs/5.0.4/firebase-auth.js"></script>
<script src="https://www.gstatic.com/firebasejs/5.0.4/firebase-database.js"></script>
<script src="https://www.gstatic.com/firebasejs/5.0.4/firebase-storage.js"></script>
<script src="https://www.gstatic.com/firebasejs/5.0.4/firebase-messaging.js"></script>
<script src="https://www.gstatic.com/firebasejs/5.0.4/firebase-functions.js"></script>

<script>
  // Your web app's Firebase configuration
  var firebaseConfig = {
    apiKey: "AIzaSyCh56LGkwDZaSlqs5t0LNZBB-ppT0UQm4c",
    authDomain: "flirtengine.firebaseapp.com",
    databaseURL: "https://flirtengine.firebaseio.com",
    projectId: "flirtengine",
    storageBucket: "flirtengine.appspot.com",
    messagingSenderId: "765124051377",
    appId: "1:765124051377:web:63bf28a0cae41e6a"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);
</script>
<!--Firebase chat conf.-->

@foreach($users as $row)
    {{$row->name}}
    <script>
    
        var insertData = {
            'userId' : '{{ isset($row->id) ? $row->id : '' }}',
            'userName' : '{{ isset($row->name) ? $row->name : '' }}',
            'userImage' : '{{ isset($row->profile_image) ? $row->profile_image : '' }}',
            'fake' : '{{ $row->fake}}',
        }
        firebase.database().ref().child('users').child('{{ isset($row->id) ? $row->id : '' }}').set(insertData);
    </script>
@endforeach

