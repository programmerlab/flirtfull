<?php

Route::group(['middleware' => 'web', 'prefix' => 'admin', 'namespace' => 'Modules\Admin\Http\Controllers'], function()
{
 
 Route::post('delete/all', 'AdminController@deleteAll');

   Route::match(['get','post'],'upload', function(Illuminate\Http\Request $request){
     echo "<pre>";
      print_r($request->all());
   });


    // Login
    Route::post('login', function (Illuminate\Http\Request $request, App\Admin $user) {
        $credentials = ['email' => $request->get('email'), 'password' => $request->get('password')];

        $admin_auth = auth()->guard('admin');
        $user_auth =  auth()->guard('web'); //Auth::attempt($credentials);
        if ($admin_auth->attempt($credentials)) {
            return Redirect::to('admin');
        } else {
            return redirect()
                ->back()
                ->withInput()
                ->withErrors(['message' => 'Invalid email or password. Try again!']);
        }
    });


    //basic routes
    //Route::get('/', 'AdminController@index');
    Route::get('/', 'AuthController@index');
    Route::get('/login', 'AuthController@index');
    Route::get('/forgot-password', 'AuthController@forgetPassword');
    Route::post('password/email', 'AuthController@sendResetPasswordLink');
    Route::get('password/reset', 'AuthController@resetPassword');
    Route::get('logout', 'AuthController@logout')->name('logout');

    
    Route::post('/post_login', 'AdminLoginController@post_login');
    Route::get('/logout', 'AdminLoginController@logout');
    Route::get('/CheckLogin', 'AdminLoginController@CheckLogin');
    Route::get('/404', 'AdminLoginController@not_found');
    
     /* logged admin user opertaions */
    Route::group(['middleware' =>  'admin'], function(){
              
      Route::get('/', 'AdminLoginController@dashboard'); 
       //       module
        Route::get('/module', 'ModuleController@index'); 
        Route::get('/module/create', 'ModuleController@create');
        Route::post('/module/store', 'ModuleController@store'); 
       
      Route::bind('language', function ($value, $route) {
           return Modules\Admin\Models\Language::find($value);
       });

       Route::resource(
           'language',
           'LanguageController',
           [
               'names' => [
                   'edit'    => 'language.edit',
                   'show'    => 'language.show',
                   'destroy' => 'language.destroy',
                   'update'  => 'language.update',
                   'store'   => 'language.store',
                   'index'   => 'language',
                   'create'  => 'language.create',
               ],
           ]
       );


       Route::bind('role', function ($value, $route) {
            return Modules\Admin\Entities\Role::find($value);
        });

        Route::resource(
            'roles',
            'RoleController',
            [
                'names' => [
                    'edit'    => 'role.edit',
                    'show'    => 'role.show',
                    'destroy' => 'role.destroy',
                    'update'  => 'role.update',
                    'store'   => 'role.store',
                    'index'   => 'role',
                    'create'  => 'role.create',
                ],
            ]
        );
       
       /*------------User Model and controller---------*/

        Route::bind('user', function ($value, $route) {
            return Modules\Admin\Models\User::find($value);
        });

        Route::resource(
            'user',
            'UsersController',
            [
                'names' => [
                    'edit'    => 'user.edit',
                    'show'    => 'user.show',
                    'destroy' => 'user.destroy',
                    'update'  => 'user.update',
                    'store'   => 'user.store',
                    'index'   => 'user',
                    'create'  => 'user.create',
                ],
            ]
        );
        
        
        Route::bind('grid', function ($value, $route) {
            return Modules\Admin\Models\User::find($value);
        });

        Route::resource(
            'grid',
            'GridController',
            [
                'names' => [
                    'edit'    => 'grid.edit',
                    'show'    => 'grid.show',
                    'destroy' => 'grid.destroy',
                    'update'  => 'grid.update',
                    'store'   => 'grid.store',
                    'index'   => 'grid',
                    'create'  => 'grid.create',
                ],
            ]
        );


        Route::bind('faq', function ($value, $route) {
            return Modules\Admin\Models\Faq::find($value);
        });

        Route::resource(
            'faq',
            'FaqController',
            [
                'names' => [
                    'edit'    => 'faq.edit',
                    'show'    => 'faq.show',
                    'destroy' => 'faq.destroy',
                    'update'  => 'faq.update',
                    'store'   => 'faq.store',
                    'index'   => 'faq',
                    'create'  => 'faq.create',
                ],
            ]
        );
        
        
        Route::bind('credit', function ($value, $route) {
            return Modules\Admin\Models\Credit::find($value);
        });

        Route::resource(
            'credit',
            'CreditController',
            [
                'names' => [
                    'edit'    => 'credit.edit',
                    'show'    => 'credit.show',
                    'destroy' => 'credit.destroy',
                    'update'  => 'credit.update',
                    'store'   => 'credit.store',
                    'index'   => 'credit',
                    'create'  => 'credit.create',
                ],
            ]
        );
        
        Route::bind('premium', function ($value, $route) {            
            return Modules\Admin\Models\Premium::find($value);
        });
        
        Route::resource(
            'premium',
            'PremiumController',
            [
                'names' => [
                    'edit'    => 'premium.edit',
                    'show'    => 'premium.show',
                    'destroy' => 'premium.destroy',
                    'update'  => 'premium.update',
                    'store'   => 'premium.store',
                    'index'   => 'premium',
                    'create'  => 'premium.create',
                ],
            ]
        );
        
        Route::bind('diamond', function ($value, $route) {
            return Modules\Admin\Models\Diamond::find($value);
        });

        Route::resource(
            'diamond',
            'DiamondController',
            [
                'names' => [
                    'edit'    => 'diamond.edit',
                    'show'    => 'diamond.show',
                    'destroy' => 'diamond.destroy',
                    'update'  => 'diamond.update',
                    'store'   => 'diamond.store',
                    'index'   => 'diamond',
                    'create'  => 'diamond.create',
                ],
            ]
        );
        
        Route::bind('coin', function ($value, $route) {
            return Modules\Admin\Models\Coin::find($value);
        });

        Route::resource(
            'coin',
            'CoinController',
            [
                'names' => [
                    'edit'    => 'coin.edit',
                    'show'    => 'coin.show',
                    'destroy' => 'coin.destroy',
                    'update'  => 'coin.update',
                    'store'   => 'coin.store',
                    'index'   => 'coin',
                    'create'  => 'coin.create',
                ],
            ]
        );
        
        Route::bind('proposal', function ($value, $route) {
            return Modules\Admin\Models\Proposal::find($value);
        });

        Route::resource(
            'proposal',
            'ProposalController',
            [
                'names' => [
                    'edit'    => 'proposal.edit',
                    'show'    => 'proposal.show',
                    'destroy' => 'proposal.destroy',
                    'update'  => 'proposal.update',
                    'store'   => 'proposal.store',
                    'index'   => 'proposal',
                    'create'  => 'proposal.create',
                ],
            ]
        );
        
        Route::bind('proposalCategory', function ($value, $route) {
            return Modules\Admin\Models\ProposalCategory::find($value);
        });

        Route::resource(
            'proposalCategory',
            'ProposalCategoryController',
            [
                'names' => [
                    'edit'    => 'proposalCategory.edit',
                    'show'    => 'proposalCategory.show',
                    'destroy' => 'proposalCategory.destroy',
                    'update'  => 'proposalCategory.update',
                    'store'   => 'proposalCategory.store',
                    'index'   => 'proposalCategory',
                    'create'  => 'proposalCategory.create',
                ],
            ]
        );
        
        
        Route::bind('partner', function ($value, $route) {
            return Modules\Admin\Models\Partner::find($value);
        });

        Route::resource(
            'partner',
            'PartnerController',
            [
                'names' => [
                    'edit'    => 'partner.edit',
                    'show'    => 'partner.show',
                    'destroy' => 'partner.destroy',
                    'update'  => 'partner.update',
                    'store'   => 'partner.store',
                    'index'   => 'partner',
                    'create'  => 'partner.create',
                ],
            ]
        );
        
        Route::get('profile_image_status', 'GridController@profile_image_status');
        Route::get('cover_image_status', 'GridController@cover_image_status');
        Route::get('grid-search', 'GridController@search');
        Route::get('list-search', 'UsersController@search');  
        Route::get('image_delete', 'UsersController@image_delete');
        Route::get('delete_image', 'UsersController@delete_image');
        Route::get('setProfileImage', 'UsersController@setProfileImage');
        
        Route::get('deleteImage', 'GridController@delete_image');
        Route::get('setProfilePicture', 'GridController@setProfileImage');

        Route::get('modified-texts', 'UsersController@modified_texts');
        Route::get('modified-photos', 'UsersController@modified_photos');
        Route::get('fake', 'UsersController@fake');
        Route::get('real', 'UsersController@real');
        Route::get('unconfirmed', 'UsersController@unconfirmed');
        Route::get('deleted', 'UsersController@deleted');
        Route::get('test-table', 'UsersController@test_table');
        Route::get('anyData', 'UsersController@anyData');
        
        Route::post('uploadCSV', 'GridController@importExcel');
        
        Route::get('/track', 'HomeController@track');
        Route::post('/track', 'HomeController@track');
        Route::post('/get_value', 'HomeController@get_value');

        Route::bind('adminUser', function ($value, $route) {
            return Modules\Admin\Models\User::find($value);
        });

       Route::resource(
            'adminUser',
            'AdminUserController',
            [
                'names' => [
                    'edit'    => 'adminUser.edit',
                    'show'    => 'adminUser.show',
                    'destroy' => 'adminUser.destroy',
                    'update'  => 'adminUser.update',
                    'store'   => 'adminUser.store',
                    'index'   => 'adminUser',
                    'create'  => 'adminUser.create',
                ],
            ]
        );
       


        Route::bind('vendor', function ($value, $route) {
            return Modules\Admin\Models\Vendor::find($value);
        });

        Route::resource(
            'vendor',
            'VendorController',
            [
                'names' => [
                    'edit'    => 'vendor.edit',
                    'show'    => 'vendor.show',
                    'destroy' => 'vendor.destroy',
                    'update'  => 'vendor.update',
                    'store'   => 'vendor.store',
                    'index'   => 'vendor',
                    'create'  => 'vendor.create',
                ],
            ]
        );

       
        

        
        /*------------User Category and controller---------*/

        Route::bind('category', function ($value, $route) {
            return Modules\Admin\Models\Category::find($value);
        });

        Route::resource(
                'category',
                'CategoryController',
                [
                    'names' => [
                        'edit'      => 'category.edit',
                        'show'      => 'category.show',
                        'destroy'   => 'category.destroy',
                        'update'    => 'category.update',
                        'store'     => 'category.store',
                        'index'     => 'category',
                        'create'    => 'category.create',
                    ],
                ]
            );
        /*---------End---------*/


        /*------------User Category and controller---------*/

         Route::bind('sub-category', function($value, $route) {
             return Modules\Admin\Models\Category::find($value);
         });

         Route::resource('sub-category', 'SubCategoryController', [
             'names' => [
                 'edit' => 'sub-category.edit',
                 'show' => 'sub-category.show',
                 'destroy' => 'sub-category.destroy',
                 'update' => 'sub-category.update',
                 'store' => 'sub-category.store',
                 'index' => 'sub-category',
                 'create' => 'sub-category.create',
             ]
                 ]
         );


         /*------------ Product Type and controller---------*/

        Route::bind('product-type', function ($value, $route) {
            return Modules\Admin\Models\ProductType::find($value);
        });

        Route::resource(
                'product-type',
                'ProductTypeController',
                [
                    'names' => [
                        'index'     => 'product-type',
                        'create'    => 'product-type.create',
                        'store'     => 'product-type.store',
                        'destroy'   => 'product-type.destroy',
                        'edit'      => 'product-type.edit',
                        'update'    => 'product-type.update',
                    ],
                ]
            );
        /*---------End---------*/

        /*------------ Product Type and controller---------*/

        Route::bind('product-unit', function ($value, $route) {
            return Modules\Admin\Models\ProductUnit::find($value);
        });

        Route::resource(
                'product-unit',
                'ProductUnitController',
                [
                    'names' => [
                        'index'     => 'product-unit',
                        'create'    => 'product-unit.create',
                        'store'     => 'product-unit.store',
                        'destroy'   => 'product-unit.destroy',
                        'edit'      => 'product-unit.edit',
                        'update'    => 'product-unit.update',
                    ],
                ]
            );


         // product

        Route::bind('product', function ($value, $route) {
            return Modules\Admin\Models\Product::find($value);
        });

        Route::resource(
                'product',
                'ProductController',
                [
                    'names' => [
                        'edit'      => 'product.edit',
                        'show'      => 'product.show',
                        'destroy'   => 'product.destroy',
                        'update'    => 'product.update',
                        'store'     => 'product.store',
                        'index'     => 'product',
                        'create'    => 'product.create',
                    ],
                ]
            );
        // product

        Route::bind('vendorProduct', function ($value, $route) {
            return Modules\Admin\Models\Product::find($value);
        });

        Route::resource(
                'vendorProduct',
                'VendorProductController',
                [
                    'names' => [
                        'edit'      => 'vendorProduct.edit',
                        'show'      => 'vendorProduct.show',
                        'destroy'   => 'vendorProduct.destroy',
                        'update'    => 'vendorProduct.update',
                        'store'     => 'vendorProduct.store',
                        'index'     => 'vendorProduct',
                        'create'    => 'vendorProduct.create',
                    ],
                ]
            );
        
        /*---------End---------*/
        // wensite settings

        Route::bind('setting', function ($value, $route) {
            return Modules\Admin\Models\Settings::find($value);
        });

        Route::resource(
            'setting',
            'SettingsController',
            [
                'names' => [
                    'edit'      => 'setting.edit',
                    'show'      => 'setting.show',
                    'destroy'   => 'setting.destroy',
                    'update'    => 'setting.update',
                    'store'     => 'setting.store',
                    'index'     => 'setting',
                    'create'    => 'setting.create',
                ],
            ]
        );
       // category
       
    });

    //manage static pages
    
        Route::bind('page', function ($value, $route) {
                return Modules\Admin\Models\Pages::find($value);
            });

        Route::resource(
            'page',
            'PagesController',
            [
                'names' => [
                    'edit'      => 'page.edit',
                    'show'      => 'page.show',
                    'destroy'   => 'page.destroy',
                    'update'    => 'page.update',
                    'store'     => 'page.store',
                    'index'     => 'page',
                    'create'    => 'page.create',
                ],
            ]
        );
        
        
      // payment history
      Route::bind('payment', function ($value, $route) {
            return Modules\Admin\Models\Payment::find($value);
        });

        Route::resource(
            'payment',
            'PaymentController',
            [
                'names' => [
                    'edit'    => 'payment.edit',
                    'show'    => 'payment.show',
                    'destroy' => 'payment.destroy',
                    'update'  => 'payment.update',
                    'store'   => 'payment.store',
                    'index'   => 'payment',
                    'create'  => 'payment.create',
                ],
            ]
        ); 


        // c history
      Route::bind('country', function ($value, $route) {
            return Modules\Admin\Models\Country::find($value);
        });

        Route::resource(
            'country',
            'CountryController',
            [
                'names' => [
                    'edit'    => 'country.edit',
                    'show'    => 'country.show',
                    'destroy' => 'country.destroy',
                    'update'  => 'country.update',
                    'store'   => 'country.store',
                    'index'   => 'country',
                    'create'  => 'country.create',
                ],
            ]
        ); 


        //add credits to wallet
        Route::resource(
            'reward',
            'RewardController',
            [
                'names' => [
                    'edit'    => 'reward.edit',
                    'show'    => 'reward.show',
                    'destroy' => 'reward.destroy',
                    'update'  => 'reward.update',
                    'store'   => 'reward.store',
                    'index'   => 'reward',
                    'create'  => 'reward.create',
                ],
            ]
        );

        /*------------Manage Pages and controller---------*/

        Route::bind('page', function ($value, $route) {
            return Modules\Admin\Models\Page::find($value);
        });

        Route::resource(
                'page',
                'PageController',
                [
                    'names' => [
                        'edit'      => 'page.edit',
                        'show'      => 'page.show',
                        'destroy'   => 'page.destroy',
                        'update'    => 'page.update',
                        'store'     => 'page.store',
                        'index'     => 'page',
                        'create'    => 'page.create',
                    ],
                ]
            );
        /*---------End---------*/

      // Email templates
      Route::bind('template', function ($value, $route) {
            return Modules\Admin\Models\Template::find($value);
        });

        Route::resource(
            'template',
            'TemplateController',
            [
                'names' => [
                    'edit'    => 'template.edit',
                    'show'    => 'template.show',
                    'destroy' => 'template.destroy',
                    'update'  => 'template.update',
                    'store'   => 'template.store',
                    'index'   => 'template',
                    'create'  => 'template.create',
                ],
            ]
        ); 
    
    
});
