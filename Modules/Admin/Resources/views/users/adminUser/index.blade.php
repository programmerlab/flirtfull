@extends('admin::layouts.master')
 
    @section('content') 
      @include('admin::partials.navigation')
      @include('admin::partials.breadcrumb')   

       @include('admin::partials.sidebar')  
			
            <div class="advanced-search">
            	<form method="get" action="#">
            	<div class="row">
                	<div class="col-md-2">
                    <div class="form-group">
                    <select class="custom-select">
                      <option selected="">Profile Type</option>
                      <option value="1">Generic</option>
                      <option value="2">Mature</option>
                      <option value="3">Kinky</option>
                      <option value="4">Shemale</option>
                    </select>
                    </div>
					</div>
                    <div class="col-md-2"><div class="form-group">
                    <select class="custom-select">
                      <option selected="">Gender</option>
                      <option value="1">Male</option>
                      <option value="2">Female</option>
                      <option value="3">Transsexual </option>
                    </select>
					</div></div>
                    <div class="col-md-2"><div class="form-group">
                    <select class="custom-select">
                        <option selected="">Appearance</option>
                        <option value="1">	Arab</option>
                        <option value="2">Asian</option>
                        <option value="3">Ebony</option>
                        <option value="4">Latin</option>
                        <option value="5">White </option>
                    </select>
					</div></div>
                      <div class="col-md-2"><div class="form-group">
                        <select class="custom-select">
                            <option selected="">Active</option>
                            <option value="1">	Yes</option>
                            <option value="2">No</option>
                           
                        </select>
                        </div></div>
                        <div class="col-md-2"><div class="form-group">
                        	<button class="advance-btn" type="button"><i class="glyphicon glyphicon-cog"></i> More Filter</button>
                        </div></div>
                         <div class="col-md-2"><div class="form-group">
                        	<button type="submit" class="btn btn-primary form-control">Search</button>
                        </div></div>
                </div>
                
                <div class="advance-fields">
                	<div class="row">
                    	<div class="col-md-3"><div class="form-group">
                        	<select class="custom-select">
                              <option selected="">Adult level</option>
                              <option value="1">Sexy</option>
                              <option value="2">Adult</option>
                              <option value="3">Non-adult</option>
                              <option value="4">Explicit</option>
                            </select>
                        </div></div>
                        <div class="col-md-3"><div class="form-group">
                        	<select class="custom-select">
                                <option selected="">Profile status</option>
                                <option value="1">Pictures added</option>
                                <option value="2">Pictures approved</option>
                                <option value="3">Basis info completed</option>
                                <option value="4">Basic info approved</option>
                            </select>
                        </div></div>
                        <div class="col-md-3"><div class="form-group">
                        	<select class="custom-select">
                                <option selected="">Photo setting</option>
                                <option value="1">	Without picture</option>
                                <option value="2">With pictures</option>
                                <option value="3">Only attachements</option>
                                <option value="4">Picture + attachement</option>
                            </select>
                        </div></div>
                           <div class="col-md-3"><div class="form-group">
                        	<select class="custom-select">
                                <option selected="">Number of pictures</option>
                                <option value="1">	1</option>
                                <option value="2">2-3</option>
                                <option value="3">4-5</option>
                                <option value="4">5-6</option>
                                <option value="5">6-7</option>
                                <option value="6">+8</option>
                            </select>
                        </div></div>
                    </div>
                    <div class="row">
                    	<div class="col-md-3"><div class="form-group">
                        	<select class="custom-select">
                              <option selected="">Country</option>
                              <option value="1">Country</option>
                              <option value="2">Website</option>
                            </select>
                        </div></div>
                        <div class="col-md-3"><div class="form-group">
                        	<select class="custom-select">
                                <option selected="">Civil status</option>
                                <option value="1">	Divorced</option>
                                <option value="2">	Married</option>
                                <option value="2">Single</option>
                                <option value="2">Widow</option>
                                <option value="2">LAT relationship</option>
                                <option value="2">Living together</option>
                                <option value="2">In a relationship</option>
                            </select>
                        </div></div>
                    </div>
                </div>
            	</form>
            </div>
            <div class="panel panel-white"> 
                <div class="panel panel-flat">
                  <div class="panel-heading">
                    <h6 class="panel-title"><b> App Players </b><a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
                    <div class="heading-elements">
                      <ul class="icons-list">
                        <li> <a type="button" href="{{route('adminUser.create')}}" class="btn btn-primary text-white btn-labeled btn-rounded "><b><i class="icon-plus3"></i></b>Add Player<span class="legitRipple-ripple" ></span></a></li> 
                      </ul>
                    </div>
                  </div> 
                </div>  
                <div class="panel-body">
                	<div class="row">
                    	
                        
                        
                        
                        
                        
                        <div class="col-md-2">
                        <div class="profile-grid-outer">
                        	<div class="profile-grid">
                            	
                                <section class="regular slider" data-sizes="50vw">
                                    <div><img src="https://democarol.com/DatingApp/upload/profile_image/Susana.jpg"></div>
                                    <div><img src="https://democarol.com/DatingApp/upload/profile_image/Silvia.jpg"></div>
                                    <div><img src="https://democarol.com/DatingApp/upload/profile_image/Leidalys.jpg"></div>
                                </section>
                               <div class="profile-grid-edit col-md-12">
                               	<div class="row">
                               		<div class="col-md-4 view_pro">
                                    	<button type="submit" name="view_levels" class="" value="view"><i class="glyphicon glyphicon-user" title="View"></i></button>
                                    </div>
                                    <div class="col-md-4 edit_pro">
                                    	<button type="submit" name="edit_levels" class="" value="edit"><i class="icon-pencil7" title="edit"></i></button>
                                    </div>
                                    <div class="col-md-4 delete_pro">
                                    	<button type="submit" name="remove_levels" class="" value="delete"><i class="icon-trash" title="Delete"></i></button>
                                    </div>
                                    </div>
                               </div>
                             
                             </div>
                               <h3>Yenifer1 25</h3>
                               <p>  Colombia ,USA</p>
							</div>
                        </div>
                        <div class="col-md-2">
                        <div class="profile-grid-outer">
                        	<div class="profile-grid">
                            	
                                <section class="regular slider" data-sizes="50vw">
                                    <div><img src="https://democarol.com/DatingApp/upload/profile_image/Susana.jpg"></div>
                                    <div><img src="https://democarol.com/DatingApp/upload/profile_image/Silvia.jpg"></div>
                                    <div><img src="https://democarol.com/DatingApp/upload/profile_image/Leidalys.jpg"></div>
                                </section>
                               <div class="profile-grid-edit col-md-12">
                               	<div class="row">
                               		<div class="col-md-4 view_pro">
                                    	<button type="submit" name="view_levels" class="" value="view"><i class="glyphicon glyphicon-user" title="View"></i></button>
                                    </div>
                                    <div class="col-md-4 edit_pro">
                                    	<button type="submit" name="edit_levels" class="" value="edit"><i class="icon-pencil7" title="edit"></i></button>
                                    </div>
                                    <div class="col-md-4 delete_pro">
                                    	<button type="submit" name="remove_levels" class="" value="delete"><i class="icon-trash" title="Delete"></i></button>
                                    </div>
                                    </div>
                               </div>
                             
                             </div>
                               <h3>Yenifer1 25</h3>
                               <p>  Colombia ,USA</p>
							</div>
                        </div>
                        <div class="col-md-2">
                        <div class="profile-grid-outer">
                        	<div class="profile-grid">
                            	
                                <section class="regular slider" data-sizes="50vw">
                                    <div><img src="https://democarol.com/DatingApp/upload/profile_image/Susana.jpg"></div>
                                    <div><img src="https://democarol.com/DatingApp/upload/profile_image/Silvia.jpg"></div>
                                    <div><img src="https://democarol.com/DatingApp/upload/profile_image/Leidalys.jpg"></div>
                                </section>
                               <div class="profile-grid-edit col-md-12">
                               	<div class="row">
                               		<div class="col-md-4 view_pro">
                                    	<button type="submit" name="view_levels" class="" value="view"><i class="glyphicon glyphicon-user" title="View"></i></button>
                                    </div>
                                    <div class="col-md-4 edit_pro">
                                    	<button type="submit" name="edit_levels" class="" value="edit"><i class="icon-pencil7" title="edit"></i></button>
                                    </div>
                                    <div class="col-md-4 delete_pro">
                                    	<button type="submit" name="remove_levels" class="" value="delete"><i class="icon-trash" title="Delete"></i></button>
                                    </div>
                                    </div>
                               </div>
                             
                             </div>
                               <h3>Yenifer1 25</h3>
                               <p>  Colombia ,USA</p>
							</div>
                        </div>
                        <div class="col-md-2">
                        <div class="profile-grid-outer">
                        	<div class="profile-grid">
                            	
                                <section class="regular slider" data-sizes="50vw">
                                    <div><img src="https://democarol.com/DatingApp/upload/profile_image/Susana.jpg"></div>
                                    <div><img src="https://democarol.com/DatingApp/upload/profile_image/Silvia.jpg"></div>
                                    <div><img src="https://democarol.com/DatingApp/upload/profile_image/Leidalys.jpg"></div>
                                </section>
                               <div class="profile-grid-edit col-md-12">
                               	<div class="row">
                               		<div class="col-md-4 view_pro">
                                    	<button type="submit" name="view_levels" class="" value="view"><i class="glyphicon glyphicon-user" title="View"></i></button>
                                    </div>
                                    <div class="col-md-4 edit_pro">
                                    	<button type="submit" name="edit_levels" class="" value="edit"><i class="icon-pencil7" title="edit"></i></button>
                                    </div>
                                    <div class="col-md-4 delete_pro">
                                    	<button type="submit" name="remove_levels" class="" value="delete"><i class="icon-trash" title="Delete"></i></button>
                                    </div>
                                    </div>
                               </div>
                             
                             </div>
                               <h3>Yenifer1 25</h3>
                               <p>  Colombia ,USA</p>
							</div>
                        </div>
                        <div class="col-md-2">
                        <div class="profile-grid-outer">
                        	<div class="profile-grid">
                            	
                                <section class="regular slider" data-sizes="50vw">
                                    <div><img src="https://democarol.com/DatingApp/upload/profile_image/Susana.jpg"></div>
                                    <div><img src="https://democarol.com/DatingApp/upload/profile_image/Silvia.jpg"></div>
                                    <div><img src="https://democarol.com/DatingApp/upload/profile_image/Leidalys.jpg"></div>
                                </section>
                               <div class="profile-grid-edit col-md-12">
                               	<div class="row">
                               		<div class="col-md-4 view_pro">
                                    	<button type="submit" name="view_levels" class="" value="view"><i class="glyphicon glyphicon-user" title="View"></i></button>
                                    </div>
                                    <div class="col-md-4 edit_pro">
                                    	<button type="submit" name="edit_levels" class="" value="edit"><i class="icon-pencil7" title="edit"></i></button>
                                    </div>
                                    <div class="col-md-4 delete_pro">
                                    	<button type="submit" name="remove_levels" class="" value="delete"><i class="icon-trash" title="Delete"></i></button>
                                    </div>
                                    </div>
                               </div>
                             
                             </div>
                               <h3>Yenifer1 25</h3>
                               <p>  Colombia ,USA</p>
							</div>
                        </div>
                        
                            
                            
                        
                    </div>
                </div>
                <!--<div class="panel-body">
                    
                                    <div class="table-toolbar">
                                        <div class="row">
                                            <form action="{{route('adminUser')}}" method="get" id="filter_data">
                                            <div class="col-md-2">
                                                <select name="status" class="form-control" onChange="SortByStatus('filter_data')">
                                                    <option value="">Search by Status</option>
                                                    <option value="active" @if($status==='active') selected  @endif>Active</option>
                                                    <option value="inActive" @if($status==='inActive') selected  @endif>Inactive</option>
                                                </select>
                                            </div>
                                             
                                            <div class="col-md-2">
                                                <input value="{{ (isset($_REQUEST['search']))?$_REQUEST['search']:''}}" placeholder="search by Name/Email" type="text" name="search" id="search" class="form-control" >
                                            </div>
                                            <div class="col-md-2">
                                                <input type="submit" value="Search" class="btn btn-primary form-control">
                                            </div>
                                           
                                        </form>
                                        
                                      
                                        </div>
                                    </div>  
                            </div>-->
                                    <!--<br>
                                     @if(Session::has('flash_alert_notice'))
                                         <div class="alert alert-success alert-dismissable" style="margin:10px">
                                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                          <i class="icon fa fa-check"></i>  
                                         {{ Session::get('flash_alert_notice') }} 
                                         </div>
                                    @endif

                                    @if($users->count()==0)
                                   
                                     <span class="caption-subject font-red sbold uppercase"> Record not found!</span>
                                    @else 
                                  <div class="table-responsive">
                                    <table class="table table-striped table-hover table-bordered table-responsive" id="">
                                        <thead>
                                            <tr>
                                                 <th> Sno. </th>
                                                <th> Full Name </th>
                                                <th> Email </th>
                                                <th> Phone </th>
                                                <th> Role Type </th>
                                                <th>Signup Date</th>
                                                <th>Status</th>
                                                <th>Action</th> 
                                            </tr>
                                        </thead>
                                        <tbody>

                                    
                                        @foreach($users as $key => $result)
                                            <tr>
                                                 <td> {{ (($users->currentpage()-1)*15)+(++$key) }}</td>
                                                <td> {{$result->first_name.'  '.$result->last_name}} </td>
                                                <td> {{$result->email}} </td>
                                                <td> {{$result->phone}} </td>
                                                <td class="center"> 
                                               
                                                    @if($result->role_type==4)
                                                    <a href="{{url('admin/mytask/'.$result->id)}}">
                                                        View Details

                                                    </a>
                                                    @else
                                                    {{$roles[$result->role_type]??'admin'}}
                                                    @endif
                                                </td>
                                                <td>
                                                    {!! Carbon\Carbon::parse($result->created_at)->format('Y-m-d'); !!}
                                                </td>
                                                <td>
                                                    <span class="label label-{{ ($result->status==1)?'success':'warning'}} status" id="{{$result->id}}"  data="{{$result->status}}"  onclick="changeStatus({{$result->id}},'user')" >
                                                            {{ ($result->status==1)?'Active':'Inactive'}}
                                                        </span>
                                                </td>
                                                <td> 
                                      <a href="{{ route('adminUser.edit',$result->id)}}" class="btn btn-primary btn-xs" style="margin: 3px">
                            <i class="icon-pencil7" title="edit"></i>  
                            </a> 

                            {!! Form::open(array('class' => 'form-inline pull-left deletion-form', 'method' => 'DELETE',  'id'=>'deleteForm_'.$result->id, 'style'=>'margin:3px', 'route' => array('adminUser.destroy', $result->id))) !!}

                            <button class='delbtn btn btn-danger btn-xs' type="submit" name="remove_levels" value="delete" id="{{$result->id}}"><i class="icon-trash" title="Delete"></i> 
                            </button> 
                            {!! Form::close() !!} 

                                                    </td>
                                               
                                            </tr>
                                           @endforeach
                                         @endif   
                                        </tbody>
                                    </table>
                                    Showing {{($users->currentpage()-1)*$users->perpage()+1}} to {{$users->currentpage()*$users->perpage()}}
                                    of  {{$users->total()}} entries
                                     <div class="center" align="center">  {!! $users->appends(['search' => isset($_GET['search'])?$_GET['search']:''])->render() !!}</div> 
                </div> -->
               </div>
         </div> 
   @stop