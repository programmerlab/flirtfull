           <div class="user_chat_popup" id="myScrollchat<?php echo e($receiver_id); ?>">
            	<div class="chat-window-panel">
                <div class="chat-window-panel-inner">
            	<div class="chat-window-title">
                	<a href="<?php echo e(url('/profile/'.$receiver_id)); ?>"><img id="chat_image<?php echo e($receiver_id); ?>" ></a>
                    <span>
                        
                    </span> <a href="<?php echo e(url('/profile/'.$receiver_id)); ?>"><span id="chat_name<?php echo e($receiver_id); ?>"></span></a>
                    <a href="<?php echo e(url('inbox')); ?>" id="close" class="pull-right">Close</a>
                </div>
                <div id="msgDiv" class="chat-window-box">                                
                  <ul id="messages" class="chat__messages">
                    <script id="message-template<?php echo e($receiver_id); ?>" type="text/template">
                    </script>
                  </ul>
                         	
                </div>   
                <div class="chat_icon_outer">
                	<div class="emoji-tape">
                        <span class="top-arrow"><i class="fa fa-angle-up"></i></span>
                        	<ul>
                            	
                                <li>
                                	<div class="zoomed">
                                        <div class=""><img src="<?php echo e(asset('/public/thumbnail/')); ?>/chat5.png" onclick="set_smiley('chat5.png','credits','0');">                                        
                                    </div></div>
                                </li>
                                 
                            </ul>
                        </div>
                        <!-- Emoji list start -->
                    	<div class="emoji-list">
                        	<ul>                                
                            	
                            </ul>
                        </div>
                        <!-- Emoji list End -->
                      <!-- send proposal wrap Start -->
                      <div class="col-lg-12 colxs-12">
                        <div class="chat_proposal_wrap">
                          <div class="send_proposal_inner">
                            <div class="proposal_heading">Select proposal <small>("NOTE: By Clicking on images, the proposal will be send directly.")</small>
                              <span class="close_pro">&times;</span>
                            </div>
                            <div class="proposal_tabs">
                              <ul class="tab_menu">                                
                                <?php $__currentLoopData = $proposal_category; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <?php if($key == 0): ?>
                                        <li  class="active" onclick="get_proposal(<?php echo e($row->id); ?>)"><?php echo e($row->title); ?></li>
                                    <?php else: ?>
                                        <li  class="" onclick="get_proposal(<?php echo e($row->id); ?>)"><?php echo e($row->title); ?></li>
                                    <?php endif; ?>    
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>    
                              </ul>
                            </div>
                            <div class="send_pro_img_wrap">
                                <div class="prop_tab_content" id="proposal_0">
                                </div>                                                                
                            </div>
                          </div> 
                        </div>
                      </div>
                    <!-- send proposal wrap End -->
                    </div>
                </div>
                <div class="chat-sender-box"> 
                        <div class="chat__main">
                            <div class="chat__footer">
                                <form id="message-form<?php echo e($receiver_id); ?>" method="POST" enctype="multipart/form-data">
                                	<div class="chat_footer_inr">
                                    <textarea class="form-control Share" name="message<?php echo e($receiver_id); ?>" id="message" placeholder="Type your Message" autofocus autocomplete="false"></textarea>
                                        <script>       
                                            var config = {};
                                            config.placeholder = 'Type Your Message....';
                                            CKEDITOR.replace( 'message<?php echo e($receiver_id); ?>' ,config);                                              
                                        </script>    
	                                    <input type="hidden" id="points">
	                                    <input type="hidden" id="planType">
										<!-- chat icon wrap -->
                                      <div class="chat_icons_wrap">
                                          <div class="chat_btns">
                                            <div class="chat_icons emoji_icon">
                                             <i class="fa fa-smile-o"></i>
                                            </div>
                                           </div>
                                          <div class="chat_btns">
                                            <label class="chat_submit_box_inner chat_btns_design attach_dv"> 
                                              <input type="file" hidden id="file" name="file">
                                              <span class="iconss attach_btn" id="upload">
                                                <i class="fa fa-camera"></i>
                                              </span>
                                              <span class="texts">Photo</span>
                                              <label id="selected"></label>
                                            <!-- <input type="file" hidden id="choose" name="file<?php echo e($receiver_id); ?>" id="file<?php echo e($receiver_id); ?>">-->
                                          </label>
                                          </div>
                                          <div class="chat_btns">
                                             <div class="chat_btns_design chat_proposal_icon">
                                              <span class="iconss"><img src="<?php echo e(asset('public/images/icons/evil_heart.png')); ?>"></span>
                                              <span class="texts">Fantasy</span>
                                            </div>
                                          </div>
                                         <div class="chat_btns">
                                            <button onclick="sendMsg()" type="button" class="chat-btn-text btn btn-bg send_btn">
                                              Send
                                            </button>
                                         </div>
                                       </div>
                                       <!-- chat icon wrap -->
	                                    <img class="load-more" src="https://flirtfull.com/public/images/chat/sending.gif" height="100px" width="200px" style="display:none">
	                                    <!--<a href="#"><i class="fa fa-paperclip"></i>
	                                    </a>-->
	                        		</div>

                          		</form>
                        	</div>
                            <!--<button id="send-location">Send Location</button>-->
                        </div>
                    </div>                    
                </div>
            </div>
            </div> 
           <!-- chat popup-->
           <script>
		   // $(document).ready(function(){ 
		   // $('#upload').click(function(){
     //            $("#file").click();
     //        })
     //                        $("#filee").change(function(e) {
     //                                 var fileName = e.target.files[0].name;
     //                                 $("#selected").html(fileName);
     //                            //alert('The file "' + fileName +  '" has been selected.');
     //                        }); 
     //                    });
     //        var getFile = new selectFile;
     //        getFile.targets('file','selected');
            </script>
           <script>
            function set_smiley(smiley,planType,points){
                $('#planType').val(planType);
                $('#points').val(points);
                //$('#message<?php echo e($receiver_id); ?>').val($('#message<?php echo e($receiver_id); ?>').val()+'<img src="<?php echo e(asset('public/thumbnail')); ?>/'+smiley+'">');
                CKEDITOR.instances.message.insertHtml($('#message').val()+'<img class="cc_img" src="<?php echo e(asset('public/thumbnail')); ?>/'+smiley+'" height="25" style="margin:2px;">');
                //$('#message<?php echo e($receiver_id); ?>').val(smiley);
                //$('#message-form<?php echo e($receiver_id); ?>').submit();
            }
            
            function setproposal(proposal,planType,points){
                //alert(proposal);
                $('#planType').val(planType);
                $('#points').val(points);
                //$('#message').val(proposal);
                CKEDITOR.instances.message.insertHtml('<img class="cc_img" src="<?php echo e(asset('public/images/profile_image')); ?>/'+proposal+'" style="margin:2px;">');
                sendMsg();
                $(".chat_proposal_wrap").slideUp(300);
            }
           </script>
          
    <script>
      //tab menu css
      $(document).ready(function(){
        //tab menu js script
        $('.tab_menu li').on('click',function(){
          $('.tab_menu li').removeClass("active");
          $(this).addClass("active");
          var content= $(this).attr('data-show');
          $(".tab_content").removeClass("active");
          $("#"+content).addClass("active");
        });
      });
    </script>
    <script>
       // Show chat proposal on click Proposal icon
        $(".chat_proposal_icon").on("click",function(e){
          $(".chat_proposal_wrap").slideToggle(300);
          get_proposal(8);
          e.stopPropagation();
        });
         //hide proposal div on click body anywhere
          $('body').click(function() {
             $('.chat_proposal_wrap').slideUp(300);
          });
          //stop event on click proposal div
          $('.chat_proposal_wrap').click(function(event){
             event.stopPropagation();
          });
        //hide proposal div on click remove button
        $(".close_pro").on("click",function(){
          $(".chat_proposal_wrap").slideUp(300);
        });
    </script>
    <script>    
        function get_proposal(propsal_category_id){
            //alert(image);            
            $.ajax({
                headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '<?php echo url("/");?>/getProposal',
                type: 'POST',
                data: 'Id='+propsal_category_id,
                cache : false,
                dataType: 'html',
                async: false,
                processData: false,
                success: function(data)
                {
                  //console.log(data);
                  $('#proposal_0').html(data);
                }
              });
        }
    </script>
    <script>
    
    function get_emoji(){        
        $.ajax({
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '<?php echo url("/");?>/get-emoji',
            type: 'GET',            
            cache : false,
            dataType: 'html',
            async: false,
            processData: false,
            success: function(data)
            {
              //console.log(data);
              $('.emoji-list').html(data);
            }
          });
    }
</script>        