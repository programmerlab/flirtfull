<?php

declare(strict_types=1);

namespace Modules\Admin\Http\Controllers;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Redirect;
use Input;
use Modules\Admin\Http\Requests\CoinRequest;
use Modules\Admin\Models\Coin;
use Route;
use View;
use Session;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Datatables;


/**
 * Class AdminController
 */
class CoinController extends Controller
{
    /**
     * @var  Repository
     */

    /**
     * Displays all admin.
     *
     * @return \Illuminate\View\View
     */
    public function __construct()
    {
        $this->middleware('admin');
        View::share('viewPage', 'Plan');
        View::share('helper', new Helper);
        View::share('heading', 'Coins');
        View::share('route_url', route('coin'));

        $this->record_per_page = Config::get('app.record_per_page');
    }

    protected $coins;

    /*
     * Dashboard
     * */

    public function index(Coin $coin, Request $request)
    {        
        $page_title  = 'Coins';
        $page_action = 'Coins';
        

        if ($request->ajax()) {
            $id           = $request->get('id');
            $status       = $request->get('status');
            $coin         = Coin::find($id);
            $s            = ($status == 1) ? $status=0:$status=1;
            $coin->status = $s;
            $coin->save();
            echo $s;

            exit();
        }
        
        $coins = Coin::orderBy('id', 'desc')->where('status', '1')->where('is_deleted', '0')->Paginate($this->record_per_page);
        //print_r($coins); die;        

        $js_file = ['common.js','bootbox.js','formValidate.js'];

        return view('admin::coins.coin.index', compact('js_file', 'status', 'coins', 'page_title', 'page_action'));
    }


    public function anyData()
    {
        return Datatables::of(Coin::query())->make(true);
    }

    public function test_table(Coin $coin, Request $request)
    {        
        $page_title  = 'Admin Coin';
        $page_action = 'Admin Coin';
        

        if ($request->ajax()) {
            $id           = $request->get('id');
            $status       = $request->get('status');
            $coin         = Coin::find($id);
            $s            = ($status == 1) ? $status=0:$status=1;
            $coin->status = $s;
            $coin->save();
            echo $s;

            exit();
        }
        // Search by name ,email and group
        $search    = Input::get('search');
        $status    = Input::get('status');
        $role_type = Input::get('role_type');

        if ((isset($search) && !empty($search)) or  (isset($status) && !empty($status)) or !empty($role_type)) {
            $search = isset($search) ? Input::get('search') : '';

            $coins = Coin::where(function ($query) use ($search,$status,$role_type) {
                if (!empty($search)) {
                    $query->Where('first_name', 'LIKE', "%$search%")
                        ->OrWhere('last_name', 'LIKE', "%$search%")
                        ->OrWhere('email', 'LIKE', "%$search%");
                }

                if (!empty($status)) {
                    
                    $status =  ($status == 'active')?1:0;
                    $query->Where('status', $status);
                }

                if ($role_type) {
                    $query->Where('role_type', $role_type);
                }
            })->where('role_type', 3)->Paginate($this->record_per_page);
        } else {
            $coins = Coin::orderBy('id', 'desc')->where('role_type', 3)->where('is_deleted', 0)->Paginate($this->record_per_page);
        }
        $roles = Roles::all();

        $js_file = ['common.js','bootbox.js','formValidate.js'];

        return view('admin::coins.coin.test_table', compact('js_file', 'roles', 'status', 'page_title', 'page_action', 'roles' ,'role_type'));
    }

    /*
     * create Group method
     * */

    public function create(Coin $coin)
    {
        $page_title  = 'Coin';
        $page_action = 'Create Coin';
        
        $js_file     = ['common.js','bootbox.js','formValidate.js'];


        return view('admin::coins.coin.create', compact('js_file', 'coin', 'page_title', 'page_action'));
    }

    /*
     * Save Group method
     * */

    public function store(CoinRequest $request, Coin $coin)
    {
        //echo "<pre>"; print_r(Input::all()); die;
        $coin->fill(Input::all());
        
        
        $action = $request->get('submit');        
        
        
        $coin->save();
        
        $js_file = ['common.js','bootbox.js','formValidate.js'];

        return Redirect::to(route('coin'))
            ->with('flash_alert_notice', 'New record successfully created.');
    }

    /*
     * Edit Group method
     * @param
     * object : $coin
     * */

    public function edit(Coin $coin)
    {
        //echo "<pre>"; print_r($coin); die;
        $page_title  = 'Coin';
        $page_action = 'Show Coin';                
        
        $js_file     = ['common.js','bootbox.js','formValidate.js'];
                
        //echo "<pre>"; print_r($coin); die;
        return view('admin::coins.coin.edit', compact('js_file', 'coin', 'page_title', 'page_action'));
    }

    public function update(Request $request, Coin $coin, CoinRequest $coinrequest)
    {        
        //echo "<pre>"; print_r($coin->password); die;        
        $coin->fill(Input::all());
        $action          = $request->get('submit');
        $coin->save();
                
        if ($request->get('role') == 3) {
            $Redirect = 'clientcoin';
        } else {
            $Redirect = 'coin';
        }

        return Redirect::to(route($Redirect))
            ->with('flash_alert_notice', 'Record successfully updated.');
    }
    /*
     *Delete Coin
     * @param ID
     *
     */
    public function destroy(Request $request, $coin)
    { //print_r($coin);
        //echo $coin->id;die;
        $coin->is_deleted = 1;
        $coin->save();
        //$coin->delete();
        return Redirect::to(route('coin'))
            ->with('flash_alert_notice', 'Record  successfully deleted.');
    }

    public function show(Coin $coin)
    {
    }
    
    public function image_delete(Coin $coin, Request $request, Gallery_image $gallery_image)
    {
        //echo $request->ajax('id');        
            $id           = $request->get('id');            
            $status       = $request->get('is_deleted');
            $coin         = Gallery_image::find($id);
            $s            = ($status == 1) ? $status=0:$status=1;
            $coin->is_deleted = $s;
            $coin->save();
            echo $s;

            exit();        
    }
    
    public function delete_image(Coin $coin, Request $request, Gallery_image $gallery_image)
    {
        //echo $request->ajax('id');        
            $id           = $request->get('id');
            $coinid           = $request->get('coinid');
            DB::table('gallery_images')->where('id', '=', $id)->delete();
            return Redirect::to(url('admin/coin/'.$coinid.'/edit'))
                ->with('flash_alert_notice', 'Image successfully deleted.');
            exit();        
    }
    
    public function search1(Coin $coin, Request $request)
    {
        //echo $request->ajax('id');        
            $country      = $request->get('country');
            $age      = $request->get('age');
            $gender      = $request->get('gender');
            $status      = $request->get('status');
            
            
            $coins = Coin::where(function ($query) use ($country,$age,$gender,$status) {
                if (!empty($country)) {
                    $query->Where('country', 'LIKE', "%$country%");                        
                }

                if (!empty($age)) {
                    $age = explode('_',$age);
                    $query->Where('age', '>=', $age[0]);
                    $query->Where('age', '<=', $age[1]);
                }

                if ($gender) {
                    $query->Where('gender', $gender);
                }
                
                if (!empty($status)) {
                    $status = ($status == 'yes') ? '1' : '0';
                    $query->Where('status', $status);
                }
            })->where('role_type', 3)->Paginate(18);
            
            //echo "<pre>"; print_r($coins); die;            
            $roles = Roles::all();

        $js_file = [];

        return view('admin::coins.coin.index', compact('js_file', 'roles', 'country', 'coins', 'page_title', 'page_action', 'roles', 'role_type')); 
    }
    
    public function search(Coin $coin, Request $request)
    {      $last_part = $request->get('last_part'); 
        //echo $request->ajax('id');        
            $country      = $request->get('country');
            $age      = $request->get('age');
            $gender      = $request->get('gender');
            $status      = $request->get('status');
            $relationship      = $request->get('relationship');
            $photo      = $request->get('photo');
            $appearance      = $request->get('appearance');
            $profile_type      = $request->get('profile_type');
            $adult_level      = $request->get('adult_level');
            
            //DB::enableQueryLog();
            
            $reslt = DB::table('coins')
                    ->join('coin_details', 'coins.id', '=', 'coin_details.coin_id')            
                    ->select('coins.*', 'coin_details.relationship');
            
            if (!empty($country)) {
                $reslt->Where('coins.country', 'LIKE', "%$country%");                        
            }
                
            if (!empty($age)) {
                $age = explode('_',$age);
                $reslt->Where('coins.age', '>=', $age[0]);
                $reslt->Where('coins.age', '<=', $age[1]);
            }
            
            if ($gender) {
                $reslt->Where('coins.gender', $gender);
            }
            
            if ($profile_type) {
                $reslt->Where('coins.profile_type', $profile_type);
            }
            
            if ($adult_level) {
                $reslt->Where('coins.adult_level', $adult_level);
            }
            
            if ($appearance) {
                $reslt->Where('coins.appearance', $appearance);
            }
            
            if (!empty($status)) {
                $status = ($status == 'yes') ? '1' : '0';
                $reslt->Where('coins.status', $status);
            }
            
            if (isset($photo)) {
                if($photo == '1'){
                    $reslt->Where('coins.profile_image', '!=',  '');
                }
                if($photo == '0'){
                    $reslt->Where('coins.profile_image','');
                    //$reslt->or_Where('coins.profile_image', '=',  'default.png');
                }
            }
            
            if ($relationship) {
                $reslt->Where('coin_details.relationship', $relationship);
            }

            if($last_part == 'deleted') {
                $page_title  = 'Player';
                $page_action = 'Deleted Players';
                $heading = 'Deleted Players';
                $reslt->Where('coins.is_deleted', '=',  '1');
            }
            else if($last_part == 'real') {
                $page_title  = 'Player';
                $page_action = 'Real Players';
                $heading = 'Real Players';
                $reslt->Where('coins.fake', '=',  '0');
                $reslt->Where('coins.is_deleted', '=',  '0');
            }
            else if($last_part == 'fake') {
                $page_title  = 'Player';
                $page_action = 'Fake Players';
                $heading = 'Fake Players';
                $reslt->Where('coins.fake', '=',  '1');
                $reslt->Where('coins.is_deleted', '=',  '0');
            }
            else if($last_part == 'unconfirmed') {
                $page_title  = 'Player';
                $page_action = 'Unconfirmed Players';
                $heading = 'Unconfirmed Players';
                $reslt->Where('coins.status', '=',  '0');
                $reslt->Where('coins.is_deleted', '=',  '0');
            }
                
            $coins = $reslt->where('coins.role_type', 3)->Paginate(30);
            
            //echo "<pre>"; print_r($coins); die;            
            $roles = Roles::all();

        $js_file = [];

        $search_slug = 'list-search';

        return view('admin::coins.coin.index', compact('js_file', 'roles', 'country', 'coins', 'page_title', 'page_action','heading',  'roles', 'role_type', 'search_slug')); 
    }
    
    public function setProfileImage(Coin $coin, Request $request, Gallery_image $gallery_image)
    {        
            $id           = $request->get('id');
            $coinid           = $request->get('coinid');
            
            $coin = DB::table('coins')->where('id', $coinid)->first();            
            $old_image = $coin->profile_image;
            
            $gallery = DB::table('gallery_images')->where('id', $id)->first();            
            $new_image = $gallery->gallery_image;
            
            DB::table('coins')
            ->where('id', $coinid)
            ->update(array('profile_image' => $new_image));
            
            DB::table('gallery_images')
            ->where('id', $id)
            ->update(array('gallery_image' => $old_image));
                        
            return Redirect::to(url('admin/coin/'.$coinid.'/edit'))
                ->with('flash_alert_notice', 'Image Set as Profile successfully ');
            exit();        
    }


    public function deleted(Coin $coin, Request $request)
    {        
        $page_title  = 'Player';
        $page_action = 'Deleted Players';
        $heading = 'Deleted Players';
        

        if ($request->ajax()) {
            $id           = $request->get('id');
            $status       = $request->get('status');
            $coin         = Coin::find($id);
            $s            = ($status == 1) ? $status=0:$status=1;
            $coin->status = $s;
            $coin->save();
            echo $s;

            exit();
        }
        // Search by name ,email and group
        $search    = Input::get('search');
        $status    = Input::get('status');
        $role_type = Input::get('role_type');

        if ((isset($search) && !empty($search)) or  (isset($status) && !empty($status)) or !empty($role_type)) {
            $search = isset($search) ? Input::get('search') : '';

            $coins = Coin::where(function ($query) use ($search,$status,$role_type) {
                if (!empty($search)) {
                    $query->Where('first_name', 'LIKE', "%$search%")
                        ->OrWhere('last_name', 'LIKE', "%$search%")
                        ->OrWhere('email', 'LIKE', "%$search%");
                }

                if (!empty($status)) {
                    
                    $status =  ($status == 'active')?1:0;
                    $query->Where('status', $status);
                }

                if ($role_type) {
                    $query->Where('role_type', $role_type);
                }
            })->where('role_type', 3)->Paginate($this->record_per_page);
        } else {
            $coins = Coin::orderBy('id', 'desc')->where('role_type', 3)->where('is_deleted', 1)->Paginate($this->record_per_page);
        }
        $roles = Roles::all();

        $js_file = ['common.js','bootbox.js','formValidate.js'];

        return view('admin::coins.coin.index', compact('js_file', 'roles', 'status', 'coins', 'page_title', 'page_action','heading', 'roles', 'role_type'));
    }

    public function fake(Coin $coin, Request $request)
    {        
        $page_title  = 'Player';
        $page_action = 'Deleted Players';
        $heading = 'Fake Players';
        

        if ($request->ajax()) {
            $id           = $request->get('id');
            $status       = $request->get('status');
            $coin         = Coin::find($id);
            $s            = ($status == 1) ? $status=0:$status=1;
            $coin->status = $s;
            $coin->save();
            echo $s;

            exit();
        }
        // Search by name ,email and group
        $search    = Input::get('search');
        $status    = Input::get('status');
        $role_type = Input::get('role_type');

        if ((isset($search) && !empty($search)) or  (isset($status) && !empty($status)) or !empty($role_type)) {
            $search = isset($search) ? Input::get('search') : '';

            $coins = Coin::where(function ($query) use ($search,$status,$role_type) {
                if (!empty($search)) {
                    $query->Where('first_name', 'LIKE', "%$search%")
                        ->OrWhere('last_name', 'LIKE', "%$search%")
                        ->OrWhere('email', 'LIKE', "%$search%");
                }

                if (!empty($status)) {
                    
                    $status =  ($status == 'active')?1:0;
                    $query->Where('status', $status);
                }

                if ($role_type) {
                    $query->Where('role_type', $role_type);
                }
            })->where('role_type', 3)->Paginate($this->record_per_page);
        } else {
            $coins = Coin::orderBy('id', 'desc')->where('role_type', 3)->where('fake', 1)->where('is_deleted', 0)->Paginate($this->record_per_page);
        }
        $roles = Roles::all();

        $js_file = ['common.js','bootbox.js','formValidate.js'];

        return view('admin::coins.coin.index', compact('js_file', 'roles', 'status', 'coins', 'page_title', 'page_action','heading', 'roles', 'role_type'));
    }

    public function real(Coin $coin, Request $request)
    {        
        $page_title  = 'Player';
        $page_action = 'Real Players';
        $heading = 'Real Players';
        

        if ($request->ajax()) {
            $id           = $request->get('id');
            $status       = $request->get('status');
            $coin         = Coin::find($id);
            $s            = ($status == 1) ? $status=0:$status=1;
            $coin->status = $s;
            $coin->save();
            echo $s;

            exit();
        }
        // Search by name ,email and group
        $search    = Input::get('search');
        $status    = Input::get('status');
        $role_type = Input::get('role_type');

        if ((isset($search) && !empty($search)) or  (isset($status) && !empty($status)) or !empty($role_type)) {
            $search = isset($search) ? Input::get('search') : '';

            $coins = Coin::where(function ($query) use ($search,$status,$role_type) {
                if (!empty($search)) {
                    $query->Where('first_name', 'LIKE', "%$search%")
                        ->OrWhere('last_name', 'LIKE', "%$search%")
                        ->OrWhere('email', 'LIKE', "%$search%");
                }

                if (!empty($status)) {
                    
                    $status =  ($status == 'active')?1:0;
                    $query->Where('status', $status);
                }

                if ($role_type) {
                    $query->Where('role_type', $role_type);
                }
            })->where('role_type', 3)->Paginate($this->record_per_page);
        } else {
            $coins = Coin::orderBy('id', 'desc')->where('role_type', 3)->where('fake', 0)->where('is_deleted', 0)->Paginate($this->record_per_page);
        }
        $roles = Roles::all();

        $js_file = ['common.js','bootbox.js','formValidate.js'];

        return view('admin::coins.coin.index', compact('js_file', 'roles', 'status', 'coins', 'page_title', 'page_action','heading', 'roles', 'role_type'));
    }

    public function unconfirmed(Coin $coin, Request $request)
    {        
        $page_title  = 'Player';
        $page_action = 'Unconfirmed Players';
        $heading = 'Unconfirmed Players';
        

        if ($request->ajax()) {
            $id           = $request->get('id');
            $status       = $request->get('status');
            $coin         = Coin::find($id);
            $s            = ($status == 1) ? $status=0:$status=1;
            $coin->status = $s;
            $coin->save();
            echo $s;

            exit();
        }
        // Search by name ,email and group
        $search    = Input::get('search');
        $status    = Input::get('status');
        $role_type = Input::get('role_type');

        if ((isset($search) && !empty($search)) or  (isset($status) && !empty($status)) or !empty($role_type)) {
            $search = isset($search) ? Input::get('search') : '';

            $coins = Coin::where(function ($query) use ($search,$status,$role_type) {
                if (!empty($search)) {
                    $query->Where('first_name', 'LIKE', "%$search%")
                        ->OrWhere('last_name', 'LIKE', "%$search%")
                        ->OrWhere('email', 'LIKE', "%$search%");
                }

                if (!empty($status)) {
                    
                    $status =  ($status == 'active')?1:0;
                    $query->Where('status', $status);
                }

                if ($role_type) {
                    $query->Where('role_type', $role_type);
                }
            })->where('role_type', 3)->Paginate($this->record_per_page);
        } else {
            $coins = Coin::orderBy('id', 'desc')->where('role_type', 3)->where('status', 0)->where('is_deleted', 0)->Paginate($this->record_per_page);
        }
        $roles = Roles::all();

        $js_file = ['common.js','bootbox.js','formValidate.js'];

        return view('admin::coins.coin.index', compact('js_file', 'roles', 'status', 'coins', 'page_title', 'page_action','heading', 'roles', 'role_type'));
    }
    
    
}
