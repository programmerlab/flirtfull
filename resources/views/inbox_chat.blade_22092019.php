@extends('layout.default')
@section('content')
<!-- Chat script -->
<script src="{{ URL::asset('assets/js/emojis/config.js')}}"></script>
<script src="{{ URL::asset('assets/js/emojis/util.js')}}"></script>
<script src="{{ URL::asset('assets/js/emojis/jquery.emojiarea.js')}}"></script>
<script src="{{ URL::asset('assets/js/emojis/emoji-picker.js')}}"></script>
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.11/lodash.core.js"></script>
<script src="https://cdn.socket.io/socket.io-1.0.0.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/1.7.4/socket.io.min.js"></script>-->
<script src="{{ URL::asset('assets/js/libs/moment.js')}}"></script>
<!-- <script src="https://rawgit.com/thielicious/selectFile.js/master/selectFile.js"></script> -->
<!--Chat script -->    
    <style>
        body {
            /*background:url('https://kabayanzone.ae/carol/datingApp/public/images/heart-bg.png');*/
        }
        .tab-contents {
            margin-top: 20px;
            height: 500px;
            overflow-y: auto;
            margin:0;
        }
    .search_list_outer {
      height: 100vh;
      max-height: 100vh;
      position: relative;
  }
.inbox_outer .chat-window-panel-inner{ padding-bottom:70px;}
.inbox_outer .chat-sender-box {
    padding: 10px 30px 10px 15px;
    position: absolute;
    bottom: 52px;
}
.inbox_outer .chat-window-box {
    padding-bottom: 60px;
}
.chat__messages{/* height:100%; overflow:auto;*/}
    .inbox_outer .container-fluid,.inbox_outer .container-fluid .col-md-4,.inbox_outer .container-fluid .col-md-8,.inbox_outer #fake_chat,.inbox_outer .user_chat_popup,.inbox_outer .chat-window-panel,.inbox_outer .chat-window-panel-inner {
    height: 100%;
}
.inbox_outer .tabs{ height:100%;}
.nav > li > a:focus, .nav > li > a:hover,.nav-pills > li.active > a, .nav-pills > li.active > a:focus, .nav-pills > li.active > a:hover{ background:#858585; color:#fff;}
@media(max-width: 990px){
  .search_list_outer {
    padding-top: 70px;
  }
}
@media(max-width: 767px){
  header{
    position:relative;
  }
  .search_list_outer {
    padding-top: 0px;
    height:auto;
  }
  .fl_footer_section{
    display:none;
  }
  .inbox_outer .container-fluid, 
  .inbox_outer .container-fluid 
  .col-md-4, .inbox_outer 
  .container-fluid .col-md-8{ height:auto;}
}
    </style>
<!--Chat script -->
    
<section class="inbox_outer search_list_outer">
  <div class="container">
    <div class="back_btn_wrap">
      <a href="{{ url('inbox') }}" class="back_btn" onclick="localStorage.setItem('chatTab', 'inbox');">Back to Inbox</a>
    </div>
    <!--left part start -->
    <div class="row">
      <div class="col-md-4 col-sm-12">
        <div class="chat_user_profile">
          <div class="user_chat_cover">
            <div class="user_chat_img">
                        @if($highliteuser->fake)
                            
                            <a class="chat_gallery_img" href="{{ env('PROFILE_MANAGER_ADDR').('/public/thumbnail/'.$highliteuser->profile_image) }}"><img src="{{ env('PROFILE_MANAGER_ADDR').('/public/images/profile_image/'.$highliteuser->profile_image) }}" align="" class="img-responsive"></a>
                            
                        @else
                            <img src="{{ asset('/public/images/profile_image/'.$highliteuser->profile_image) }}" align="" class="img-responsive">
                        @endif    
            </div>
            <div class="user_chat_title">
              <h3>{{ $highliteuser->name }}</h3>
              <p>From {{ $highliteuser->state }}</p>
            </div>
          </div>
          <div class="user_desc_table table-responsive">
            <table class="table">
              <tr>
                <td>Age</td>
                <td>{{ $highliteuser->age }}</td>
              </tr>
              <tr>
                <td>Relationship</td>
                <td>
                                    @if($highliteuser->relationship == 1)
                                       Never Married
                                    @endif
                                    @if($highliteuser->relationship == 2)
                                       Separated 
                                    @endif
                                    @if($highliteuser->relationship == 3)
                                       Divorced 
                                    @endif
                                    @if($highliteuser->relationship == 4)
                                       Widowed 
                                    @endif
                                </td>
              </tr>
              <tr>
                <td>Smoke</td>
                                <td>    
                                    @if($highliteuser->smoke == 1)
                                       Never 
                                    @endif
                                    @if($highliteuser->smoke == 2)
                                       Regularly 
                                    @endif
                                    @if($highliteuser->smoke == 3)
                                       Occassonally 
                                    @endif
                </td>
              </tr>
              <tr>
                <td>Drink</td>
                <td>@if($highliteuser->drink == 1)
                                       Never 
                                    @endif
                                    @if($highliteuser->drink == 2)
                                       Regularly 
                                    @endif
                                    @if($highliteuser->drink == 3)
                                       Occassonally 
                                    @endif
                                    </td>
              </tr>
              <tr>
                <td>Height</td>
                <td>{{ $highliteuser->height }}</td>
              </tr>
            </table>
          </div>
        </div>
        </div>
      <div class="col-md-8 col-sm-12">
        <div id="fake_chat">
        </div>
        </div>
      <div class="modal fade modaltwo" id="reply-modal">
        <div class="modal-dialog">
          <div class="modal-content">
                        <div class="modal-header">
                           <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                           <div class="inbox_detail_wrapper text-center">
                              <a href="#">
                                 <img alt="" src="{{ asset('public/images/user3.jpg') }}">
                                 <h4 class="modal-title">My Email to  <strong>CAROLINA</strong></h4>
                              </a>
                           </div>
                        </div>
                        <div class="modal-body">
                            <!--  chat popup  --> 
                                
                               
                                <!--  chat popup  -->                            
                        </div>
                        <div class="modal-footer">                           
                            
                        </div>
          </div>
                     <!-- /.modal-content -->
        </div>
                  <!-- /.modal-dialog -->
      </div>
    </div>
    <!--left part End -->
   </div>
</section>



<!--<button style='display:none' id="imageText" data-toggle="modal" data-target="#exampleModal"></button>-->

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">New message</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form>
          
      <div class="form-group">
             <img id="selectedImage" src="https://moneoangels.com/public/images/profile_image/crop_1552546735.png" alt="Smiley face" height="500" width="600"> 
          </div>
      
          <div class="form-group">
            <label for="message-text" class="col-form-label">Message:</label>
            <input style="border: 1px solid #cccccc;" type="text" class="form-control" id="message-text">
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onclick="sendImage()" data-dismiss="modal">Send message</button>
      </div>
    </div>
  </div>
</div>

<script>

</script>



    
<input type="hidden"  name="sender_id" id="sender_id" value="{{ $users->id }}">
<input type="hidden"  name="sender_name" id="sender_name" value="{{ $users->name }}">
<input type="hidden"  name="sender_image" id="sender_image" value="{{ $users->profile_image }}">

<input type="hidden"  name="receiver_id" id="receiver_id" value="">
<input type="hidden"  name="receiver_name" id="receiver_name" value="">
<input type="hidden"  name="receiver_image" id="receiver_image" value="">

<input type="hidden"  name="room_id" id="room_id" value="">
<input type="hidden"  name="chat_image" id="chat_image" value="">
<input type="hidden"  name="chat_send_image" id="chat_send_image" value="">


<script src="https://cdnjs.cloudflare.com/ajax/libs/push.js/0.0.11/push.min.js"></script>

<input type="hidden" value="0" id="startFrom" name="">

<div style="display:none;" class="paginationChat" ></div>

<script>
    
    /*Push.create('Hi there!', {
    body: 'This is a notification.',
    icon: 'icon.png',
    timeout: 8000,               // Timeout before notification closes automatically.
    vibrate: [100, 100, 100],    // An array of vibration pulses for mobile devices.
    onClick: function() {
        // Callback for when the notification is clicked. 
        console.log(this);
    }  
});*/
    
   var adminId = $('#sender_id').val();

    function checkdata(sender,receiver,msg,imgurl){
      sender = $('#sender_name').val()+'__'+sender;
      receiver = $('#receiver_name').val()+'__'+receiver;
      //console.log(imgurl);
      imgurl = imgurl.replace(/(<p[^>]+?>|<p>|<\/p>)/img, "");
      imgurl = imgurl.replace(/style="[a-zA-Z0-9:;\.\s\(\)\-\,]*"/gi, "");
        
        var m,
            urls = [], 
            str = imgurl,
            rex = /<img[^>]+src="?([^"\s]+)"?\s*\/>/g;
        
        while ( m = rex.exec( str ) ) {
            urls.push( m[1] );
        }

      //console.log(urls);
      var attachment = urls;
      if((attachment.length) > 0) { 
        var json = {
          "username" : "Moneo",
          "password" : "Moneo2019@",
          "service_code" : "cd9271fa",
          "to" : receiver,
          "from" : sender,
          "message" : msg,
          "attachment": attachment
        };
      }else{
        var json = {
          "username" : "Moneo",
          "password" : "Moneo2019@",
          "service_code" : "cd9271fa",
          "to" : receiver,
          "from" : sender,
          "message" : msg          
        };
      }

      var myJSON = JSON.stringify(json); 

      $.ajax({
        beforeSend: function() { },
        type: "POST",
        crossDomain: true,        
        url: "https://im.operatorplatform.com/api/inbound",
        ContentType: "application/x-www-form-urlencoded",
        
        /*data: {
          data : '{"username":"Moneo","password":"Moneo2019@","service_code":"cd9271fa","to":"'+receiver+'","from":"'+sender+'","message":"'+msg+'"}'
        }, */

        data: {
          data : myJSON
        }, 

        success: function(response){
          console.log('hhhhhhhhhhhhhhhhhhhhhhhhhh');  
          console.log(response);



        },
        error: function(XMLHttpRequest, textStatus, errorThrown) { 
         console.log(XMLHttpRequest);
         console.log(textStatus);
         console.log(errorThrown);
        }       
      });
    }
    


    function getFakeMsg(uId){
      $.ajax({
        url: "https://www.flirtfull.com/api/v1/getThirdPartyMessages", 
        type: 'POST',
        data:'{"id":"'+uId+'"}',
       // contentType: "application/json",
       // dataType: "application/json",
        success: function(result){
          console.log(result);

          if(result.status == 1){
          
            if(result.data.length){
              $.each(result.data, function( index, value ) {

                value.sender_id = value.from;
                value.receiver_id = value.to;

                var msgText = value.message;
                var messageType =  'text';
                var imageText =  '';

                if(value.message_type == 1){
                  if(value.attachment != 0){
                    messageType = 'file';
                    imageText = msgText;
                    msgText = value.attachment;
                  }
                }else if(value.message_type == 2){
                  messageType = 'proposal';
                }else if(value.message_type == 3){
                  messageType = 'likes';
                }else if(value.message_type == 4){
                  messageType = 'favorite';
                }

                //console.log(msgText);
                //console.log(messageType);
                //return true;

                firebase.database().ref('/users/'+value.sender_id).once('value').then(function(snapshot1) {

                  var snapshot1 = snapshot1.val();

                  var senderId = value.sender_id;
                  var senderName = snapshot1.userName;
                  var senderImage = snapshot1.userImage;

                  firebase.database().ref('/users/'+value.receiver_id).once('value').then(function(snapshot2) {

                    var snapshot2 = snapshot2.val();

                    var receiverId = value.receiver_id;
                    var receiverName = snapshot2.userName;
                    var receiverImage = snapshot2.userImage;


                    var chatRoom = senderId + '_' + receiverId;
                    if(receiverId < senderId){
                      chatRoom = receiverId + '_' + senderId;
                    }
                    
                    
                    //updtae read status start                    
                    firebase.database().ref('chat_rooms/' + chatRoom).limitToLast(15).on('child_added', function(chat){                             
                     var oneMsgVall = chat.val();
                     console.log(oneMsgVall);
                     updateMsgStatus(chatRoom,chat.key,oneMsgVall.uid,oneMsgVall.messageCount);                                   
                    });
                    //updtae read status end

                    firebase.database().ref().child('chat_history/'+senderId).child(receiverId).once('value').then(function(snapshot3) {
                      var snapshot3 = snapshot3.val();
                      var msgCount = 0;
                      var hisType = 1;
                      
                      if (snapshot3 != null){
                        var msgCount = Number(snapshot3.messageCount)+1;
                        if(snapshot3.historyType == 1){
                          if(snapshot3.lastSenderId != senderId){
                            hisType = 2;
                          }
                        }else{
                          hisType = 2;
                        }
                      }

                      var insertData = {
                        deletteby : '',
                        message : msgText,
                        messageCount : msgCount,
                        name : receiverName,
                        profilePic : receiverImage,
                        timestamp : $.now(),
                        uid : receiverId.toString(),
                        historyType : hisType,
                        lastSenderId : senderId.toString(),
                        messageType : messageType,
                        historyLoad : '0',
                      };
                      firebase.database().ref().child('chat_history/'+senderId).child(receiverId).set(insertData);
                    });



                    firebase.database().ref().child('chat_history/'+receiverId).child(senderId).once('value').then(function(snapshot4) {
                      var snapshot4 = snapshot4.val();
                      var msgCount = 0;
                      var hisType = 1;
                      if (snapshot4 != null){
                        var msgCount = Number(snapshot4.messageCount)+1;
                        if(snapshot4.historyType == 1){
                          if(snapshot4.lastSenderId != senderId){
                            hisType = 2;
                          }
                        }else{
                          hisType = 2;
                        }
                      }


                      var insertData = {
                        deletteby : '',
                        message : msgText,
                        messageCount : msgCount,
                        name : senderName,
                        profilePic : senderImage,
                        timestamp : $.now(),
                        uid : senderId.toString(),
                        historyType : hisType,
                        lastSenderId : senderId.toString(),
                        messageType : messageType,
                        historyLoad : '0',
                      };
                      firebase.database().ref().child('chat_history/'+receiverId).child(senderId).set(insertData);
                    });


                    var insertData = {
                      deleteby:'',
                      message : msgText,
                      messageCount: 1,
                      name : senderName,
                      profilePic : senderImage,
                      timestamp : $.now(),
                      uid : senderId.toString(),
                      messageType : messageType,
                      imageText : imageText,
                    };
                    firebase.database().ref().child('chat_rooms/').child(chatRoom).push(insertData);
                  });
                });
              });
            }
          }
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) { 
         console.log(XMLHttpRequest);
         console.log(textStatus);
         console.log(errorThrown);
        }       
      });
    }

    var interval = null;
    $(document).on('ready',function(){
      interval = setInterval(function() {
        getFakeMsg(adminId);
      }, 100000);
    });
    
  
    function changeMsgStatus(chatRoom,msgKey,uId,addListnerKey){
        
        if(uId != adminId){
            firebase.database().ref('chat_rooms/' + chatRoom).child(msgKey).child('messageCount').set(2);
        }else{
            if(addListnerKey == 1){
                var starCountRef = firebase.database().ref('chat_rooms/' + chatRoom);
                starCountRef.on('child_changed', function(snapshot) {
                    $('#right'+snapshot.key).html('<p>Read</p>');
                });
            }
        }
        return true;
    }
    
    function updateMsgStatus(chatRoom,msgKey,uId,addListnerKey){                
            firebase.database().ref('chat_rooms/' + chatRoom).child(msgKey).child('messageCount').set(2);        
        return true;
    }

    

    function pagination() {

      <?php if(isset($users) && ($users->fake)){ ?>
            var sendrimgurl = '{{ env('PROFILE_MANAGER_ADDR').('/public/thumbnail/') }}/';
        <?php }else{ ?>
            var sendrimgurl = '{{ asset('/public/thumbnail/') }}/';
        <?php } ?>

        var chatRoom = $.trim($('#room_id').val());
        var i = 0;
        firebase.database().ref("chat_rooms").child(chatRoom).orderByChild("timestamp").endAt(Number($('#startFrom').val())).limitToLast(15).on('value', function(snapshot) {
        i++;
            $('#startFrom').val(0);
            var msgC = 1;
            var runPagination = 1;
            if(snapshot.val()){
                $.each(snapshot.val(), function(key, oneMsgVal) {
                    
                    $('#'+key).remove();
    
                   // console.log(oneMsgVal);
                    if($('#startFrom').val() == 0){
                      $('#startFrom').val(oneMsgVal.timestamp);
                    }
    
    
    
                var readStatus = '<p>Read</p>';
                if(oneMsgVal.messageCount == 1){
                   readStatus = '<p style="font-weight: bold;">Unread</p>'; 
                }
    
    
                if(oneMsgVal.uid == adminId){
                    var liposi = 'right';
                    var srimage = $('#sender_image').val();
                }else{
                    var liposi = 'left';
                    var srimage = $('#receiver_image').val();
                    readStatus = ''; 
                }
                
                if(oneMsgVal.profilePic.indexOf('crop_') != -1){
                var sendrimgurl = '{{ env('PROFILE_MANAGER_ADDR').('/public/thumbnail/') }}';
                }else{
                    var sendrimgurl = '{{ asset('/public/thumbnail').'/' }}';
                }
    
                
                $('#removeLi').remove();
                $("#selected").html('');
                if(oneMsgVal.messageType != 'file'){
                    $('.paginationChat').append(''+ 
                        '<li id="'+key+'" class="message">'+                    
                            '<div class="chat-user-'+liposi+'">'+
                            '<div class="chat-user-box">'+
                            '<img src="'+sendrimgurl+srimage+'">'+
                            '<span>'+oneMsgVal.name+'</span>'+
                            '</div>'+
                            '<div class="chat-user-message">'+
                            '<div class="chat-line"><div class="chat_border">'+oneMsgVal.message+'</div></div>'+                                   
                            '<div class="meta-info"><span class="date">'+timeDifference(oneMsgVal.timestamp)+'</span></div>'+
                            '<div class="meta-info"><span id="'+liposi+key+'" class="date">'+readStatus+'</span></div>'+
                            '</div>'+
                            '</div>'+
                        '</li>'+
                    '');
                }else{
    
                    if(oneMsgVal.message.indexOf('firebasestorage') != -1){
                      oneMsgVal.message = '<a href="#" data-toggle="modal" data-target="#chat_modal" onclick="myfunc('+oneMsgVal.timestamp+');"><img src="'+oneMsgVal.message+'" width="100" height="100"><input type="hidden" id="chatimg'+oneMsgVal.timestamp+'" value="'+oneMsgVal.message+'"></a>';
                    }else if(oneMsgVal.message.indexOf('<img') != -1){
                      oneMsgVal.message = '<a href="#" data-toggle="modal" data-target="#chat_modal" onclick="myfunc('+oneMsgVal.timestamp+');">'+oneMsgVal.message+'<input type="hidden" id="chatimg'+oneMsgVal.timestamp+'" value="'+oneMsgVal.message+'"></a>';
                    }else{
                      oneMsgVal.message = '<a href="#" data-toggle="modal" data-target="#chat_modal" onclick="myfunc('+oneMsgVal.timestamp+');"><img src="https://moneoangels.com/public/images/profile_image/'+oneMsgVal.message+'" width="100" height="100"><input type="hidden" id="chatimg'+oneMsgVal.timestamp+'" value="'+oneMsgVal.message+'" ></a>';
                    }
                    /* '<img src=' + oneMsgVal.message + ' alt="Ovengo..." height="100" width="100">'+*/
    
                    $('.paginationChat').append(''+
                        '<li id="'+key+'" class="message">'+                    
                        '<div class="chat-user-'+liposi+'">'+
                        '<div class="chat-user-box">'+
                        '<img src="'+sendrimgurl+srimage+'">'+
                        '<span>'+oneMsgVal.name+'</span>'+
                        '</div>'+
                        '<div class="chat-user-message">'+
                          '<div class="chat-line">'+
                  '<div class="chat_border">'+
                  '<div class="chat_border_inr">'+
                  '<span class="chat_imgs">'+oneMsgVal.message+'</span>'+
                  '<div class="chat_txt_wrp">'+
                    '<p>'+oneMsgVal.imageText+'</p>'+
                  '</div>'+
                  '</div>'+
                            '</div>'+
                '</div>'+                                   
                        '<div class="meta-info"><span class="date">'+timeDifference(oneMsgVal.timestamp)+'</span></div>'+
                        '<div class="meta-info"><span class="date">'+readStatus+'</span></div>'+
                        '</div>'+
                        '</div>'+
                    '</li>'+
                    '');
                }
                
                
                changeMsgStatus(chatRoom,key,oneMsgVal.uid,oneMsgVal.messageCount);
    
    
    
                    msgC++;
                });
            }
            //alert(msgC);
            if (msgC <= 15) {
                $('#startFrom').val(0);
            }


            var current_top_element = $("#messages").children().first();
           
            $("#messages").prepend($('.paginationChat').html());
            $('.paginationChat').html('');



            var previous_height = 0;
            current_top_element.prevAll().each(function() {
                previous_height += $(this).outerHeight();
            });

            //alert(current_top_element);
            //alert(previous_height);

            $("#msgDiv").scrollTop(previous_height + 10);

           
        })
    }


   
    function createChatRoom(){
        
        var room_id = $('#room_id').val();
        if(room_id != ''){
            firebase.database().ref('chat_rooms/' + room_id).off("child_added");
        }
        
        var uid = $('#receiver_id').val();
        var chatRoom = adminId + '_' + uid;
        if(uid < adminId){
          chatRoom = uid + '_' + adminId;
        }
        
           
    
    
        
        firebase.database().ref('chat_rooms/' + chatRoom).off("child_added");
        
        $('#room_id').val(chatRoom);                     
        
        
        
        var i=0;
        firebase.database().ref('chat_rooms/' + chatRoom).limitToLast(15).on('child_added', function(chat){
       
            i++;
            

            setTimeout(function() {
              firebase.database().ref().child('chat_history/'+adminId).child(uid).child('historyLoad').set('1');
              firebase.database().ref().child('chat_history/'+adminId).child(uid).child('messageCount').set('0');
            }, 1000); 
      
            var oneMsgVal = chat.val();
            //console.log(oneMsgVal.profilePic);
            if(oneMsgVal.profilePic.indexOf('crop_') != -1){
            var sendrimgurl = '{{ env('PROFILE_MANAGER_ADDR').('/public/thumbnail/') }}';
            }else{
                var sendrimgurl = '{{ asset('/public/thumbnail').'/' }}';
            }
            

            var readStatus = '<p>Read</p>';
            if(oneMsgVal.messageCount == 1){
               readStatus = '<p style="font-weight: bold;">Unread</p>'; 
            }


            if(oneMsgVal.uid == adminId){
                var liposi = 'right';
                var srimage = $('#sender_image').val();
            }else{
                var liposi = 'left';
                var srimage = $('#receiver_image').val();
                readStatus = ''; 
            }

            //console.log(oneMsgVal);
            
            $('#removeLi').remove();
            $("#selected").html('');
            if(oneMsgVal.messageType != 'file'){
                $('#messages').append(''+ 
                    '<li id="'+chat.key+'" class="message">'+                    
                        '<div class="chat-user-'+liposi+'">'+
                        '<div class="chat-user-box">'+
                        '<img src="'+sendrimgurl+srimage+'">'+
                        '<span>'+oneMsgVal.name+'</span>'+
                        '</div>'+
                        '<div class="chat-user-message">'+
                        '<div class="chat-line"><div class="chat_border">'+oneMsgVal.message+'</div></div>'+                                   
                        '<div class="meta-info"><span class="date">'+timeDifference(oneMsgVal.timestamp)+'</span></div>'+
                        '<div class="meta-info"><span id="'+liposi+chat.key+'" class="date">'+readStatus+'</span></div>'+
                        '</div>'+
                        '</div>'+
                    '</li>'+
                '');
            }else{

                if(oneMsgVal.message.indexOf('firebasestorage') != -1){
                  oneMsgVal.message = '<a href="#" data-toggle="modal" data-target="#chat_modal" onclick="myfunc('+oneMsgVal.timestamp+');"><img src="'+oneMsgVal.message+'" width="100" height="100"><input type="hidden" id="chatimg'+oneMsgVal.timestamp+'" value="'+oneMsgVal.message+'"></a>';
                }else if(oneMsgVal.message.indexOf('<img') != -1){
                  oneMsgVal.message = '<a href="#" data-toggle="modal" data-target="#chat_modal" onclick="myfunc('+oneMsgVal.timestamp+');">'+oneMsgVal.message+'<input type="hidden" id="chatimg'+oneMsgVal.timestamp+'" value="'+oneMsgVal.message+'"></a>';
                }else{
                  oneMsgVal.message = '<a href="#" data-toggle="modal" data-target="#chat_modal" onclick="myfunc('+oneMsgVal.timestamp+');"><img src="https://moneoangels.com/public/images/profile_image/'+oneMsgVal.message+'" width="100" height="100"><input type="hidden" id="chatimg'+oneMsgVal.timestamp+'" value="'+oneMsgVal.message+'" ></a>';
                }
                /* '<img src=' + oneMsgVal.message + ' alt="Ovengo..." height="100" width="100">'+*/

                $('#messages').append(''+
                    '<li id="'+chat.key+'" class="message">'+                    
                    '<div class="chat-user-'+liposi+'">'+
                    '<div class="chat-user-box">'+
                    '<img src="'+sendrimgurl+srimage+'">'+
                    '<span>'+oneMsgVal.name+'</span>'+
                    '</div>'+
                    '<div class="chat-user-message">'+
                    '<div class="chat-line">'+
            '<div class="chat_border">'+
            '<div class="chat_border_inr">'+
              '<span class="chat_imgs">'+oneMsgVal.message+'</span>'+
              '<div class="chat_txt_wrp">'+
               '<p>'+oneMsgVal.imageText+'</p>'+
               '</div>'+
            '</div>'+
                      '</div>'+
           '</div>'+                                   
                    '<div class="meta-info"><span class="date">'+timeDifference(oneMsgVal.timestamp)+'</span></div>'+
                    '<div class="meta-info"><span class="date">'+readStatus+'</span></div>'+
                    '</div>'+
                    '</div>'+
                '</li>'+
                '');
            }
           
            changeMsgStatus(chatRoom,chat.key,oneMsgVal.uid,oneMsgVal.messageCount);
            
            var msgDiv = document.getElementById('msgDiv');
            msgDiv.scrollTop = msgDiv.scrollHeight;

            if($('#startFrom').val() == 0){
              $('#startFrom').val(oneMsgVal.timestamp);
              $('#msgDiv').scroll(function() {
                if ($('#msgDiv').scrollTop() == 0) {
                   pagination();
                }
              });
            }

           
        });
    }


    function timeDifference(previous) {
        
        var current= new Date();

        var msPerMinute = 60 * 1000;
        var msPerHour = msPerMinute * 60;
        var msPerDay = msPerHour * 24;
        var msPerMonth = msPerDay * 30;
        var msPerYear = msPerDay * 365;
        
        var elapsed = current - previous;
        
        if (elapsed < msPerMinute) {
             return Math.round(elapsed/1000) + ' sec ago';   
        }
        
        else if (elapsed < msPerHour) {
             return Math.round(elapsed/msPerMinute) + ' min ago';   
        }
        
        else if (elapsed < msPerDay ) {
             return Math.round(elapsed/msPerHour ) + ' hours ago';   
        }

        else if (elapsed < msPerMonth) {
             return Math.round(elapsed/msPerDay) + ' days ago';   
        }
        
        else if (elapsed < msPerYear) {
             return Math.round(elapsed/msPerMonth) + ' months ago';   
        }
        
        else {
             return Math.round(elapsed/msPerYear ) + ' years ago';   
        }
    }
    
    function removeHtmlCssJs(text){
    text = $.trim(text);
    text = text.replace(/\&/g, '&amp;');
        text = text.replace(/\>/g, '&gt;');
        text = text.replace(/\</g, '&lt;');
        text = text.replace(/\"/g, '&quot;');
        text = text.replace(/\'/g, '&apos;');
        return text;
  }
    

    

    function sendMsg(){
      
        if($('#message-text').val() == 'Image'){
          $('#message-text').val('');
          sendImage();
          return true;
        };
      
        var sender_is_fake = '{{ $users->fake }}';

    
        var msgText1 = CKEDITOR.instances.message.getData();
    
        if(msgText1 == ''){
            return true;
        }
    
    
    var mycredits = jQuery("#initialcredit").val();//localStorage.getItem("credits");
            var mypremiums = jQuery("#initialpremium").val();
            if(mycredits == 'null'){ mycredits = 0;}
            var requiredCredits = 0;
            
            
            // for text required credit will be 1
            var type = (jQuery("#file").val() == '') ? 'text' : 'file';
            if(type === "text"){
                requiredCredits = 1;
            }
            
            var planType = jQuery("#planType").val();
            if(planType === 'credits'){ requiredCredits = jQuery("#points").val(); }
            
            if(planType === 'diamonds'){ requiredCredits = jQuery("#points").val(); }     
            
            
            if(planType === 'diamonds'){                
                //if(mycredits === 0){
                    var diamnd = jQuery("#updateddiamonds").html();
                    //console.log(diamnd);
                    if(diamnd > 0){
                        mycredits = diamnd;   
                    }                    
                //}                                            
                //console.log(mycredits);
                if(parseInt(mycredits) >= parseInt(requiredCredits) || (sender_is_fake == 1) ){
                    $.ajax({
                        headers: {
                          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: '<?php echo url("/")?>/update_diamonds',
                        type: 'POST',
                        cache : false,
                        processData: false,
                        data: 'room_id='+jQuery('#room_id').val()+'&amt='+requiredCredits+'&receiver_id='+jQuery('#receiver_id').val()+'&msg='+msgText1, // serializes the form's elements.
                        success: function(data)
                        {
                    
                         if(data){
                            var d = JSON.parse(data);
                                                    
                            if(d.credits){
                                jQuery("#initialcredit").val(d.credits);
                                jQuery("#updatedwallet").html(d.credits);
                                jQuery("#updateddiamonds").html(d.diamonds);   
                            }else{                            
                                jQuery("#updateddiamonds").html(d.diamonds);   
                            }                        
                          }                      
                        }
                    });
                }
            }else{
                if(parseInt(mycredits) >= parseInt(requiredCredits) || (sender_is_fake == 1) ){
                    $.ajax({
                        headers: {
                          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: '<?php echo url("/")?>/update_conversation',
                        type: 'POST',
                        cache : false,
                        processData: false,
                        data: 'room_id='+jQuery('#room_id').val()+'&amt='+requiredCredits+'&receiver_id='+jQuery('#receiver_id').val()+'&msg='+msgText1, // serializes the form's elements.
                        success: function(data)
                        {
                       
                          if(data){
                            jQuery("#initialcredit").val(data);
                            jQuery("#updatedwallet").html(data);
                          }
                        }
                    });
                }
            }
            
    
    
    
    
    
        var chat_send_image = $('#chat_send_image').val();
        var messageType = 'text';
        if(chat_send_image != ''){
            messageType = 'text';
           // CKEDITOR.instances.message.setData(chat_send_image);
        }
    
        var userId    = $('#receiver_id').val();
        var userName  = $('#receiver_name').val();
        var userImage = $('#receiver_image').val();
        var msgText = CKEDITOR.instances.message.getData();
    
       
        

    
        CKEDITOR.instances.message.setData('');
    
        var chatRoom = $('#room_id').val();
    
        firebase.database().ref('/users/'+userId).once('value').then(function(snapshot) {
          var childData = snapshot.val();
          
          if(childData.fake == '1'){
            if(parseInt(mycredits) >= parseInt(requiredCredits) || (sender_is_fake == 1) ){
                if(msgText.substring(0, 11) === '<p><img src'){
                    checkdata(adminId,userId,'image',msgText);
                }else{
                    checkdata(adminId,userId,msgText,'');   
                }                                               
            }            
          }

          firebase.database().ref().child('chat_history/'+adminId).child(userId).once('value').then(function(snapshot1) {
    
            var snapshot1Data = snapshot1.val();
    
            var msgCount = 0;
            var hisType = 1;
            if (snapshot1Data != null){
              var msgCount = Number(snapshot1Data.messageCount)+1;
              if(snapshot1Data.historyType == 1){
                if(snapshot1Data.lastSenderId != adminId){
                  hisType = 2;
                }
              }else{
                hisType = 2;
              }
            }
    
            var insertData = {
              deletteby : '',
              message : msgText,
              messageCount : msgCount,
              name : userName,
              profilePic : userImage,
              timestamp : $.now(),
              uid : userId.toString(),
              historyType : hisType,
              lastSenderId : adminId.toString(),
              messageType : messageType,
              historyLoad : '0',
            };
            if(parseInt(mycredits) >= parseInt(requiredCredits)  || (sender_is_fake == 1) ){
                firebase.database().ref().child('chat_history/'+adminId).child(userId).set(insertData);
            }
          });
        });
    
        firebase.database().ref('/users/'+adminId).once('value').then(function(snapshot) {
          var childData = snapshot.val();
    
          firebase.database().ref().child('chat_history/'+userId).child(adminId).once('value').then(function(snapshot2) {
    
            var snapshot2Data = snapshot2.val();
    
            var msgCount = 0;
            var hisType = 1;
            if (snapshot2Data != null){
              var msgCount = Number(snapshot2Data.messageCount)+1;
              if(snapshot2Data.historyType == 1){
                if(snapshot2Data.lastSenderId != adminId){
                  hisType = 2;
                }
              }else{
                hisType = 2;
              }
            }
    
            var insertData = {
              deletteby : '',
              message : msgText,
              messageCount : msgCount,
              name : childData.userName,
              profilePic : childData.userImage,
              timestamp : $.now(),
              uid : adminId.toString(),
              historyType : hisType,
              lastSenderId : adminId.toString(),
              messageType : messageType,
        historyLoad : '0',
            };
            if(parseInt(mycredits) >= parseInt(requiredCredits) || (sender_is_fake == 1) ){
                firebase.database().ref().child('chat_history/'+userId).child(adminId).set(insertData);
            }
          });
        });
    
        firebase.database().ref('/users/'+adminId).once('value').then(function(snapshot) {  
          var childData = snapshot.val();
          var insertData = {
            deleteby:'',
            message : msgText,
            messageCount: 1,
            name : childData.userName,
            profilePic : childData.userImage,
            timestamp : $.now(),
            uid : adminId.toString(),
            messageType : messageType,
            imageText : '',
          };
    
      if(parseInt(mycredits) >= parseInt(requiredCredits)  || (sender_is_fake == 1) ){
      firebase.database().ref().child('chat_rooms/').child(chatRoom).push(insertData);
      }else{
            //console.log();
            <?php if(isset($users) && ($users->fake)){ ?>
                var sendrimgurl = '{{ env('PROFILE_MANAGER_ADDR').('/public/thumbnail/') }}/';
            <?php }else{ ?>
                var sendrimgurl = '{{ asset('/public/thumbnail/') }}/';
            <?php } ?>
            //message failed
            $('#messages').append(''+ 
                          '<li class="message">'+                    
                              '<div class="chat-user-right">'+
                              '<div class="chat-user-box">'+
                              '<img src="'+sendrimgurl+jQuery('#sender_image').val()+'">'+
                              '<span>'+jQuery('#sender_name').val()+'</span>'+
                              '</div>'+
                              '<div class="chat-user-message">'+
                              '<div class="chat-line"><div class="chat_border">'+msgText+'</div></div>'+                                   
                              '<div class="meta-info"><span class="date">Now</span></div>'+
                  '<div class="meta-info red"><span class="date">Message Sent Failed!! Upgrade Your Account</span></div>'+
                              '</div>'+
                              '</div>'+
                          '</li>'+
                      '');
          //setTimeout(function(){ jQuery('.credit_btn').click(); }, 3000); 
         
              $('#msgDiv').animate({
                  scrollTop: $('#messages').prop("scrollHeight")
              }, 0);
              setTimeout(function(){ window.location.replace("{{ url('/credits') }}"); }, 5000); 
          }
        });
        
        $("#planType").val('credits');
        $("#points").val(1);
      }
      
          
      $(document).on('change', '#file', function (e) {
      //$('#imageText').click();
      $('#selectedImage').attr('src',URL.createObjectURL(e.target.files[0]));
      $('#message-text').val('Image');      
     // alert(e.target.files[0].name)
      var fileName = e.target.files[0].name;
        $("#selected").html(fileName);

      //sendImage();
    });
    
    function sendImage(){
    //var imageText = $('#message-text').val();
    var imageText = CKEDITOR.instances.message.getData(); 
    var file = $('input#file')[0].files[0];
    
    CKEDITOR.instances.message.setData('');
    $("#selected").html('');

    var storageRef = firebase.storage().ref();
        var dataD = Date.now();
        var uploadTask = storageRef.child('images/' + dataD).put(file);
        
        <?php if(isset($users) && ($users->fake)){ ?>
            var sendrimgurl = '{{ env('PROFILE_MANAGER_ADDR').('/public/thumbnail/') }}/';
        <?php }else{ ?>
            var sendrimgurl = '{{ asset('/public/thumbnail/') }}/';
        <?php } ?>
    
    var imageUrl = "https://thumbs.gfycat.com/JointRevolvingAntelopegroundsquirrel-size_restricted.gif"; //"http://www.mizpahpublishing.com/images/loader.gif";

    var myName = $('#sender_name').val();
    var myimage = sendrimgurl+$('#sender_image').val();
    
    
    $('#messages').append(''+
      '<li id="removeLi" class="message">'+                    
      '<div class="chat-user-right">'+
      '<div class="chat-user-box">'+
      '<img src="'+myimage+'">'+
      '<span>'+myName+'</span>'+
      '</div>'+
      '<div class="chat-user-message">'+
      '<div class="chat-line"><div class="chat_border">'+
      '<img src=' + imageUrl + ' alt="Ovengo..." height="100" width="100">'+
      '</div></div>'+                                   
      '<div class="meta-info"><span class="date">Now</span></div>'+
      '</div>'+
      '</div>'+
    '</li>'+
    '');
        
        uploadTask.on('state_changed', function(snapshot) { 
                var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
               

                
                
                // $('#messages').append(''+
                    // '<li id="removeLi" class="message">'+                    
                    // '<div class="chat-user-right">'+
                    // '<div class="chat-user-box">'+
                    // '<img src="'+myimage+'">'+
                    // '<span>'+myName+'</span>'+
                    // '</div>'+
                    // '<div class="chat-user-message">'+
                    // '<div class="chat-line"><span>'+
                    // '<img src=' + imageUrl + ' alt="Ovengo..." height="100" width="100">'+
                    // '</span></div>'+                                   
                    // '<div class="meta-info"><span class="date">Now</span></div>'+
                    // '</div>'+
                    // '</div>'+
                // '</li>'+
                // '');
    
                

                switch (snapshot.state) {
                    case firebase.storage.TaskState.PAUSED:
                        ////console.log('Upload is paused');
                        $('#messages').animate({
                            scrollTop: $('#messages').prop("scrollHeight")
                        }, 0);
                        break;
                    case firebase.storage.TaskState.RUNNING:
                        ////console.log('Upload is running');
                        $('#messages').animate({
                            scrollTop: $('#messages').prop("scrollHeight")
                        }, 0);
                        break;
                }

            }, 
            function(error) {

            }, function() {
                $('#removeLi').remove();
                var storageqq = firebase.storage();
                storageqq.ref('images/' + dataD).getDownloadURL().then(function (url) {
                    var downloadURL = url;
                    
                        var messageType = 'file';
                    
                    
                        var userId    = $('#receiver_id').val();
                        var userName  = $('#receiver_name').val();
                        var userImage = $('#receiver_image').val();
                        var msgText = downloadURL;
                
                        //CKEDITOR.instances.message.setData('');
                    
                        var chatRoom = $('#room_id').val();
                    
                        firebase.database().ref('/users/'+userId).once('value').then(function(snapshot) {
                          var childData = snapshot.val();
                          
                          firebase.database().ref().child('chat_history/'+adminId).child(userId).once('value').then(function(snapshot1) {
                    
                            var snapshot1Data = snapshot1.val();
                    
                            var msgCount = 0;
                            var hisType = 1;
                            if (snapshot1Data != null){
                              var msgCount = Number(snapshot1Data.messageCount)+1;
                              if(snapshot1Data.historyType == 1){
                                if(snapshot1Data.lastSenderId != adminId){
                                  hisType = 2;
                                }
                              }else{
                                hisType = 2;
                              }
                            }
                    
                            var insertData = {
                              deletteby : '',
                              message : msgText,
                              messageCount : msgCount,
                              name : userName,
                              profilePic : userImage,
                              timestamp : $.now(),
                              uid : userId.toString(),
                              historyType : hisType,
                              lastSenderId : adminId.toString(),
                              messageType : messageType,
                historyLoad : '0',
                            };
                          
                          firebase.database().ref().child('chat_history/'+adminId).child(userId).set(insertData);
                          });
                        });
                    
                        firebase.database().ref('/users/'+adminId).once('value').then(function(snapshot) {
                          var childData = snapshot.val();
                    
                          firebase.database().ref().child('chat_history/'+userId).child(adminId).once('value').then(function(snapshot2) {
                    
                            var snapshot2Data = snapshot2.val();
                    
                            var msgCount = 0;
                            var hisType = 1;
                            if (snapshot2Data != null){
                              var msgCount = Number(snapshot2Data.messageCount)+1;
                              if(snapshot2Data.historyType == 1){
                                if(snapshot2Data.lastSenderId != adminId){
                                  hisType = 2;
                                }
                              }else{
                                hisType = 2;
                              }
                            }
                    
                            var insertData = {
                              deletteby : '',
                              message : msgText,
                              messageCount : msgCount,
                              name : childData.userName,
                              profilePic : childData.userImage,
                              timestamp : $.now(),
                              uid : adminId.toString(),
                              historyType : hisType,
                              lastSenderId : adminId.toString(),
                              messageType : messageType,
                historyLoad : '0',
                            };
                    
                          firebase.database().ref().child('chat_history/'+userId).child(adminId).set(insertData);
                          });
                        });
                    
                        firebase.database().ref('/users/'+adminId).once('value').then(function(snapshot) {  
                          var childData = snapshot.val();
                          var insertData = {
                            deleteby:'',
                            message : msgText,
                            messageCount: 1,
                            name : childData.userName,
                            profilePic : childData.userImage,
                            timestamp : $.now(),
                            uid : adminId.toString(),
                            messageType : messageType,
                            imageText : imageText,
                          };
                          firebase.database().ref().child('chat_rooms/').child(chatRoom).push(insertData);
                        });
      
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    $('#chat_send_image').val(downloadURL);
                }); 
            
            });
    
    
    
    
    }
    
    
      $(document).on('changee', '#file', function (e) {
      
        var fileName = e.target.files[0].name;
        $("#selected").html(fileName);
      
        var file = e.target.files[0];
        var storageRef = firebase.storage().ref();
        var dataD = Date.now();
        var uploadTask = storageRef.child('images/' + dataD).put(file);
        
        <?php if(isset($users) && ($users->fake)){ ?>
            var sendrimgurl = '{{ env('PROFILE_MANAGER_ADDR').('/public/thumbnail/') }}/';
        <?php }else{ ?>
            var sendrimgurl = '{{ asset('/public/thumbnail/') }}/';
        <?php } ?>
    
    var imageUrl = "http://www.mizpahpublishing.com/images/loader.gif";

    var myName = $('#sender_name').val();
    var myimage = sendrimgurl+$('#sender_image').val();
    
    
    $('#messages').append(''+
      '<li id="removeLi" class="message">'+                    
      '<div class="chat-user-right">'+
      '<div class="chat-user-box">'+
      '<img src="'+myimage+'">'+
      '<span>'+myName+'</span>'+
      '</div>'+
      '<div class="chat-user-message">'+
      '<div class="chat-line"><div class="chat_border">'+
      '<img src=' + imageUrl + ' alt="Ovengo..." height="100" width="100">'+
      '</div></div>'+                                   
      '<div class="meta-info"><span class="date">Now</span></div>'+
      '</div>'+
      '</div>'+
    '</li>'+
    '');
        
    var msgDiv = document.getElementById('msgDiv');
    msgDiv.scrollTop = msgDiv.scrollHeight;
    
        uploadTask.on('state_changed', function(snapshot) { 
                var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
               

                
        msgDiv.scrollTop = msgDiv.scrollHeight;
                
                // $('#messages').append(''+
                    // '<li id="removeLi" class="message">'+                    
                    // '<div class="chat-user-right">'+
                    // '<div class="chat-user-box">'+
                    // '<img src="'+myimage+'">'+
                    // '<span>'+myName+'</span>'+
                    // '</div>'+
                    // '<div class="chat-user-message">'+
                    // '<div class="chat-line"><span>'+
                    // '<img src=' + imageUrl + ' alt="Ovengo..." height="100" width="100">'+
                    // '</span></div>'+                                   
                    // '<div class="meta-info"><span class="date">Now</span></div>'+
                    // '</div>'+
                    // '</div>'+
                // '</li>'+
                // '');
    
                

                switch (snapshot.state) {
                    case firebase.storage.TaskState.PAUSED:
                        ////console.log('Upload is paused');
                        $('#messages').animate({
                            scrollTop: $('#messages').prop("scrollHeight")
                        }, 0);
                        break;
                    case firebase.storage.TaskState.RUNNING:
                        ////console.log('Upload is running');
                        $('#messages').animate({
                            scrollTop: $('#messages').prop("scrollHeight")
                        }, 0);
                        break;
                }

            }, 
            function(error) {

            }, function() {
                $('#removeLi').remove();
                var storageqq = firebase.storage();
                storageqq.ref('images/' + dataD).getDownloadURL().then(function (url) {
                    var downloadURL = url;
                    
                        var messageType = 'file';
                    
                    
                        var userId    = $('#receiver_id').val();
                        var userName  = $('#receiver_name').val();
                        var userImage = $('#receiver_image').val();
                        var msgText = downloadURL;
                
                        //CKEDITOR.instances.message.setData('');
                    
                        var chatRoom = $('#room_id').val();
                    
                        firebase.database().ref('/users/'+userId).once('value').then(function(snapshot) {
                          var childData = snapshot.val();
                          
                          firebase.database().ref().child('chat_history/'+adminId).child(userId).once('value').then(function(snapshot1) {
                    
                            var snapshot1Data = snapshot1.val();
                    
                            var msgCount = 0;
                            var hisType = 1;
                            if (snapshot1Data != null){
                              var msgCount = Number(snapshot1Data.messageCount)+1;
                              if(snapshot1Data.historyType == 1){
                                if(snapshot1Data.lastSenderId != adminId){
                                  hisType = 2;
                                }
                              }else{
                                hisType = 2;
                              }
                            }
                    
                            var insertData = {
                              deletteby : '',
                              message : msgText,
                              messageCount : msgCount,
                              name : userName,
                              profilePic : userImage,
                              timestamp : $.now(),
                              uid : userId.toString(),
                              historyType : hisType,
                              lastSenderId : adminId.toString(),
                              messageType : messageType,
                            };
                          
                          firebase.database().ref().child('chat_history/'+adminId).child(userId).set(insertData);
                          });
                        });
                    
                        firebase.database().ref('/users/'+adminId).once('value').then(function(snapshot) {
                          var childData = snapshot.val();
                    
                          firebase.database().ref().child('chat_history/'+userId).child(adminId).once('value').then(function(snapshot2) {
                    
                            var snapshot2Data = snapshot2.val();
                    
                            var msgCount = 0;
                            var hisType = 1;
                            if (snapshot2Data != null){
                              var msgCount = Number(snapshot2Data.messageCount)+1;
                              if(snapshot2Data.historyType == 1){
                                if(snapshot2Data.lastSenderId != adminId){
                                  hisType = 2;
                                }
                              }else{
                                hisType = 2;
                              }
                            }
                    
                            var insertData = {
                              deletteby : '',
                              message : msgText,
                              messageCount : msgCount,
                              name : childData.userName,
                              profilePic : childData.userImage,
                              timestamp : $.now(),
                              uid : adminId.toString(),
                              historyType : hisType,
                              lastSenderId : adminId.toString(),
                              messageType : messageType,
                            };
                    
                          firebase.database().ref().child('chat_history/'+userId).child(adminId).set(insertData);
                          });
                        });
                    
                        firebase.database().ref('/users/'+adminId).once('value').then(function(snapshot) {  
                          var childData = snapshot.val();
                          var insertData = {
                            deleteby:'',
                            message : msgText,
                            messageCount: 1,
                            name : childData.userName,
                            profilePic : childData.userImage,
                            timestamp : $.now(),
                            uid : adminId.toString(),
                            messageType : messageType,
                          };
                          firebase.database().ref().child('chat_rooms/').child(chatRoom).push(insertData);
                        });
      
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    $('#chat_send_image').val(downloadURL);
                }); 
            
            });
        
        
        });
</script>


<script>
$("#profile_popup").removeClass("user_profile_popup");
    function set_receiver(id, name, image,fake=0){
        //alert(name);
        //gethistory();
        $('#receiver_id').val(id);
        $('#receiver_name').val(name);
        $('#chat_name'+id).text(name);
        //$('#chat_image'+id).attr('src','{{ asset('public/thumbnail') }}/'+image);
        if(fake == 1){
            $('#chat_image'+id).attr('src','{{ env('PROFILE_MANAGER_ADDR').('/public/thumbnail/') }}/'+image);
            var rcvrimgurl = '{{ env('PROFILE_MANAGER_ADDR').('/public/thumbnail/') }}/';
        }else{
            $('#chat_image'+id).attr('src','{{ asset('/public/thumbnail/') }}/'+image);
            var rcvrimgurl = '{{ env('PROFILE_MANAGER_ADDR').('/public/thumbnail/') }}/';
        }
        $('#receiver_image').val(image);
        
        var sender_id = '{{ $users->id }}';
        var receiver_id = id;

        $.ajax({
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '<?php echo url("/");?>/fake_chat',
            type: 'POST',
            data: 'receiver_id='+receiver_id,
            cache : false,
            dataType: 'html',
            async: false,
            processData: false,
            success: function(data)
            {
              $('#fake_chat').html(data);
              $('.user_chat_popup').hide();
              $('.chat__messages').html('');                    
              $('#myScrollchat'+receiver_id).show();
              $("#profile_popup").addClass("user_profile_popup");

              $('#receiver_id').val(id);
              $('#receiver_name').val(name);
              $('#chat_name'+id).text(name);
              //$('#chat_image'+id).attr('src','{{ asset('public/thumbnail') }}/'+image);
              if(fake == 1){
                    $('#chat_image'+id).attr('src','{{ env('PROFILE_MANAGER_ADDR').('/public/thumbnail/') }}/'+image);        
              }else{
                    $('#chat_image'+id).attr('src','{{ asset('/public/thumbnail/') }}/'+image);            
              } 
              $('#receiver_image').val(image);

              
            }
          });
          //alert();
             createChatRoom()    
        return true;
        
        
        var roomid = $('#room_id').val();
        //alert(roomid);
        
    }


    $('#msgDiv').scroll(function() {
              if ($('#msgDiv').scrollTop() == 0) {
                alert(123);
              }
            });

</script>   
    
    <script src="{{ URL::asset('assets/js/libs/moustache.js')}}"></script>
    <script src="{{ URL::asset('assets/js/libs/deparam.js')}}"></script>
<!-- chat scripts --->

<textarea id="chat_file_hidden" style="display:none"></textarea>

<input type="hidden" id="rec_id">
<input type="hidden" id="rec_name">
<input type="hidden" id="rec_img">
<script type="text/javascript">
    $(document).ready(function(){        
     /* $(".dropdown").click(function(){
        $(this).find(".dropdown-menu").slideToggle("fast");
      });
      $(".inbox_outer").on("click", function(event){
          var $trigger = $(".dropdown");
          if($trigger !== event.target && !$trigger.has(event.target).length){
              $(".dropdown-menu").slideUp("fast");
          }  
      });*/
    });
</script>
@if(!empty($highlite))    
<script>
$(document).ready(function(){
    //if(screen.width > 768){
        setTimeout(function(){            
                set_receiver('{{$highlite['id']}}','{{$highlite['name']}}','{{$highlite['profile_image']}}','{{$highlite['fake']}}');
        }, 1000);
    //}
});
</script>
<script>
//chat gallery script
$(document).ready(function() {
    $('.chat_gallery_img').magnificPopup({
      type: 'image',
      closeOnContentClick: true,
      closeBtnInside: true,
      fixedContentPos: true,
      mainClass: 'mfp-with-zoom', // class to remove default margin from left and right side
      image: {
        verticalFit: true
      },
      zoom: {
        enabled: true,
        duration: 300 // don't foget to change the duration also in CSS
      }
    });
  });
</script>
<!-- chat Modal start -->
<div class="modal fade" id="chat_modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <!-- Modal Header -->
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <!-- Modal body -->
      <div class="modal-body">
       <div class="chat_modal_img">
          <script>            
          </script>  
      <img src="http://placehold.it/800x1200"  alt="" id="modalimg" class="img-responsive">
     </div>
      </div>
    </div>
  </div>
</div>
<!-- chat Modal End -->
@endif    

<script>
    function myfunc(i){
        //alert(i);
        var str = $('#chatimg'+i).val();
        if(str.indexOf('https') != -1){
            $('#modalimg').attr('src',$('#chatimg'+i).val());
        }else{            
            var imgsrc = 'https://moneoangels.com/public/images/profile_image/'+str;
            //alert(imgsrc);
            $('#modalimg').attr('src',imgsrc);
        }
        
    }
</script>



@endsection