@extends('admin::layouts.master')
 
    @section('content') 
      @include('admin::partials.navigation')
      @include('admin::partials.breadcrumb')   

       @include('admin::partials.sidebar')                         
						<div class="panel panel-white"> 
                                <div class="panel panel-flat">
                                              <div class="panel-heading">
                                            <h6 class="panel-title"><b> {{$heading ?? ''}}</b><a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
                                            <div class="heading-elements">
                                              <ul class="icons-list">
                                                <li> <a type="button" href="{{route('diamond')}}" class="btn btn-primary text-white   btn-rounded "> View Diamonds<span class="legitRipple-ripple" ></span></a></li> 
                                              </ul>
                                            </div>
                                          </div> 
                                    </div>
                                    <div class="panel-body">
                                    {!! Form::model($diamond, ['method' => 'PATCH', 'route' => ['diamond.update', $diamond->id],'enctype'=>'multipart/form-data','class'=>
                                                  'form-basic ui-formwizard diamond-form']) !!}
                                                  
                                           <div class="form-group {{ $errors->first('qty', ' has-error') }}">
                                                <label class="control-label col-md-3">Diamonds Quantity <span class="required"> * </span></label>
                                                <div class="col-md-12"> 
                                                    {!! Form::text('qty',null, ['class' => 'form-control','data-required'=>1])  !!} 
                                                    
                                                    <span class="help-block">{{ $errors->first('qty', ':message') }}</span>
                                                </div>
                                            </div>
                                            
                                           <input type="hidden" name="discount" value="0"/>
                                            <input type="hidden" name="txn_fee" value="0"/>
										   <!--<div class="form-group {{ $errors->first('discount', ' has-error') }}">
                                                <label class="control-label col-md-3">Discount (in %) <span class="required"> * </span></label>
                                                <div class="col-md-12"> 
                                                    {!! Form::text('discount',null, ['class' => 'form-control','data-required'=>1])  !!} 
                                                    
                                                    <span class="help-block">{{ $errors->first('discount', ':message') }}</span>
                                                </div>
                                            </div>-->
                                                
                                                
                                             <div class="form-group {{ $errors->first('price', ' has-error') }}">
                                                <label class="control-label col-md-3">Credits <span class="required"> * </span></label>
                                                <div class="col-md-12"> 
                                                    {!! Form::text('price',null, ['class' => 'form-control','data-required'=>1])  !!} 
                                                    
                                                    <span class="help-block">{{ $errors->first('price', ':message') }}</span>
                                                </div>
                                            </div>
                                                
                                                
                                            <!--<div class="form-group {{ $errors->first('txn_fee', ' has-error') }}">
                                                <label class="control-label col-md-3">Transaction Charge <span class="required"> * </span></label>
                                                <div class="col-md-12"> 
                                                    {!! Form::text('txn_fee',null, ['class' => 'form-control','data-required'=>1])  !!} 
                                                    
                                                    <span class="help-block">{{ $errors->first('txn_fee', ':message') }}</span>
                                                </div>
                                            </div>  -->           
                          
                              
                                           <div class="col-md-12">
                                                  <div class="margin-top-10"> 
                                                       <button type="submit" class="btn green btn-primary" value="avtar" name="submit"> Save Changes </button>
                                                      <button type="reset" class="btn default "> Cancel </button>
                                                  </div> 
                                                 
                                           </div>
                                    </form>        
                                    </div>                                                                                          
                        </div>
                              
                           
                     
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
            
            
            <!-- END QUICK SIDEBAR -->
        </div>
        

        
@stop