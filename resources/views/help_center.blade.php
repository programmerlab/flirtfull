@extends('layout.default')
@section('content')
<section class="help_page_section">
  <div class="container">
      <div class="row">
        <!-- Page Heading End --> 
        <div class="col-md-12">
          <div class="page_heading">
            <h2 class="heading_tg">Help Center</h2>
          </div>
         </div>
         <!-- Page Heading End -->
          @if( Session::has( 'success' ))
          <div class="row">
           <div class="col-md-12">
            <div class="alert alert-success">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              {{ Session::get( 'success' ) }}
            </div>
           </div>
          </div>
          @endif
         <div class="col-lg-8 col-md-10 col-lg-offset-2 col-md-offset-1 col-xs-12">
            <div class="help_form_wrap">
               <div class="form_heading">
                  <h3>Need To Help</h3>
                  <p>Please contact us!</p>
               </div>
               <div class="contact_form">
                  <form method="post" action="{{ url('post-help-center') }}" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                     <div class="form_group">
                        <div class="width_50">
                          <label>Name</label>
                          <div class="form_input">
                             <input type="text" name="h_name" placeholder="Enter Your Name" value="{{ Auth::user()->name}}">
                             @if ($errors->has('h_name'))
                              <p class="error-message">{{ $errors->first('h_name') }}</p>
                            @endif
                          </div>
                        </div>
                        <div class="width_50">
                           <label>Email</label>
                           <div class="form_input">
                              <input type="email" name="h_email" placeholder="Enter Your Email" value="{{ Auth::user()->email}}">
                              @if ($errors->has('h_email'))
                                <p class="error-message">{{ $errors->first('h_email') }}</p>
                              @endif
                           </div>
                        </div>
                     </div>
                     <!--<div class="form_group">-->
                        <!-- changed by saif -->
                     <!--    <div class="width_50">
                           <label>Contact</label>
                           <div class="form_input">
                              <input type="text" name="h_phone" placeholder="Enter Your Mobile Number" value="{{ old('h_phone') }}">
                              @if ($errors->has('h_phone'))
                                <p class="error-message">{{ $errors->first('h_phone') }}</p>
                              @endif
                           </div>
                        </div>
                     </div>
                     <div class="form_group">
                        <label>Address</label>
                        <div class="form_input">
                           <textarea name="h_address" placeholder="Enter Your Address">{{ old('h_address') }}</textarea>
                           @if ($errors->has('h_address'))
                            <p class="error-message">{{ $errors->first('h_address') }}</p>
                          @endif
                        </div>
                     </div> -->
                     <!-- changed by saif -->
                     <div class="form_group">
                        <label>Message</label>
                        <div class="form_input">
                           <textarea name="h_message" placeholder="Enter Your Problem">{{ old('h_message') }}</textarea>
                           @if ($errors->has('h_message'))
                            <p class="error-message">{{ $errors->first('h_message') }}</p>
                          @endif
                        </div>
                     </div>
                     <div class="form_group button_group">
                        <button type="submit" class="submit_btns">Submit</button>
                     </div>
                  </form>
               </div>
            </div>
         </div>
     </div>
   </div>
</section>
@endsection 