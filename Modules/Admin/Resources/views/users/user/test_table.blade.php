@extends('admin::layouts.master')
 
    @section('content') 
      @include('admin::partials.navigation')
      @include('admin::partials.breadcrumb')   

       @include('admin::partials.sidebar')  
           <div class="panel panel-white"> 
                <div class="panel panel-flat">
                  <div class="panel-heading">
                    <h6 class="panel-title"><b> {{$heading }} </b><a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
                    <div class="heading-elements">
                      <ul class="icons-list">
                        <li> <a type="button" href="{{route('user.create')}}" class="btn btn-primary text-white btn-labeled btn-rounded "><b><i class="icon-plus3"></i></b> Add Player<span class="legitRipple-ripple" ></span></a></li> 
                      </ul>
                    </div>
                  </div> 
                </div>  
                <div class="panel-body"> 
                 
              </div> 
                 @if(Session::has('flash_alert_notice'))
                     <div class="alert alert-success alert-dismissable" style="margin:10px">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                      <i class="icon fa fa-check"></i>  
                     {{ Session::get('flash_alert_notice') }} 
                     </div>
                @endif
                 <div class="table-responsive">
                  <table class="table datatable-basic table-bordered table-hover table-responsive" id="roles_list">
                                   
                        <thead>
                            <tr>
                                <th> Sno. </th>
                                <th> Full Name</th>
                                <th> Email </th>
                                <th> Gender </th>
                                <th>Signup Date</th>
                                
                            </tr>
                        </thead>
                        <tfoot>
                          <th> Sno. </th>
                          <th> Full Name</th>
                          <th> Email</th>
                          <th> 
                            <select id="gender">
                            <option>Gender</option>
                            <option value="1">Male</option>
                            <option value="1">Female</option>
                            </select> 
                          </th>
                          <th>Signup Date</th>
                          
                        </tfoot>
                    </table>
                                    
					                     
                                
                 </div> 
               </div>
         </div> 
         <script>
$(document).ready(function(){
    $('#roles_list').DataTable({
        processing: true,
        serverSide: true,
        ajax: '<?php echo url('admin/anyData') ?>',
        columns: [
            { data: 'id', name: 'id' },
            { data: 'name', name: 'name' },
            { data: 'email', name: 'email' },
            { data: 'gender', name: 'gender' },
            { data: 'created_at', name: 'created_at' },
            
        ],
        initComplete: function () {
            this.api().columns().every(function () {
                var column = this;
                var input = document.createElement("input");
                /*$(input).appendTo($(column.footer()).empty())
                .on('change', function () {
                    var val = $.fn.dataTable.util.escapeRegex($(this).val());

                    column.search(val ? val : '', true, false).draw();
                });*/
                var select = $('<select><option value=""></option></select>')
                    .appendTo( $(column.footer()).empty() )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
 
                        column
                            .search( val ? '^'+val+'$' : '', true, false )
                            .draw();
                    } );
 
                column.data().unique().sort().each( function ( d, j ) {
                    select.append( '<option value="'+d+'">'+d+'</option>' )
                } );
            });
        }
    });
});

$(document).on('change', '#gender', function(){
  var gender = $(this).val();
  $('#roles_list').DataTable().destroy();
  if(gender != '')
  {
   load_data(gender);
  }
  else
  {
   load_data();
  }
 });

</script>
   @stop
   
   
   