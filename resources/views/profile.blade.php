
<?php $__env->startSection('content'); ?>
<style>
.gallery_delete {
    background: #EC155A;
    color: #fff;
    position: absolute;
    z-index: 99;
    top: 10px;
    right: 10px;
    padding: 3px 7px;
    border-radius: 50%;
  }
    .chat__messages {        
        padding-bottom: 60px;
    } 
}
#owl-carousel-video video{ width: 100%; }
.upload_btn_gallery h2 {    font-size: 18px;    line-height: 27px;    margin: 35px 0 20px;}
  #owl-carousel-video .owl-dots,#owl-carousel-photo .owl-dots{ display:none}
  /*.owl-carousel .owl-item{ margin:0 !important}*/
.owl-nav button.owl-prev {    left: 0;background: linear-gradient(to left, rgba(0, 0, 0, 0), #000) !important;}
.owl-nav button.owl-next {    right: 0;background: linear-gradient(to right, rgba(0, 0, 0, 0), #000) !important;}
#owl-carousel-video img,#owl-carousel-photo img{ width:158px; height:150px;}
.owl-nav button {
  font-size: 34px !important;   
  position: absolute;    
  top: 50%;    
  transform: translateY(-50%);        
  height: 100%;    
  width: 60px;
  color: #fff !important;
 }
.owl-nav button.disabled {
  display: none;
}

/*my css start*/
.profile_cover_pic_outer{
    height:auto;
}

/*my css end*/
</style>

<?php 
if(isset($user_detail->user_id) && ($user_detail->user_id == Auth::id()))
{
  $self_user = 1;
}  
else{
  $self_user = 0;
}
$helper = new Helper();
if(Auth::id())
{
  $wallet = $helper->getWallet(Auth::user()->id);
}
else{
  $wallet = [];
}
?>
<!-- Chat script -->
<script src="<?php echo e(URL::asset('assets/js/emojis/config.js')); ?>"></script>
<script src="<?php echo e(URL::asset('assets/js/emojis/util.js')); ?>"></script>
<script src="<?php echo e(URL::asset('assets/js/emojis/jquery.emojiarea.js')); ?>"></script>
<script src="<?php echo e(URL::asset('assets/js/emojis/emoji-picker.js')); ?>"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.11/lodash.core.js"></script>

<script src="<?php echo e(URL::asset('assets/js/libs/moment.js')); ?>"></script>
<!-- <script src="https://rawgit.com/thielicious/selectFile.js/master/selectFile.js"></script> -->
<!--Chat script -->    
<?php //die; ?>
<section class="search_list_outer search_detail">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-9 col-sm-8 main-content" id="lft-wrpr">
        <?php
        $default_img = '';
        if( $user_detail->fake ){
            $profileimage = env('PROFILE_MANAGER_ADDR').'/public/images/profile_image'.'/'.$user_detail->profile_image;
        }else{
            $profileimage = asset('public/images/profile_image').'/'.$user_detail->profile_image;
        }
        
        if (@GetImageSize($profileimage) && ($user_detail->profile_image_status == 1)) 
        {
            
                $profile_img = $profileimage;
                      
        }
        elseif($user_detail->provider_name == 'google' && ($user_detail->profile_image_status == 1))
        {
          $profile_img = $user_detail->profile_image;
        }
        else{
          $profileimg = ($user_detail->gender == 1)  ? 'male_icon.png' : 'lady_icon.png';
          $profile_img = asset('public/images/'.$profileimg);
          $default_img = $profileimg;
        }
        
        ?>
        
        <div class="" id="profile_popup">
        <div class="profile_cover_pic">
          <?php if(!empty($user_detail->cover_image) && ($user_detail->cover_image_status == 1)): ?>
          <div class="profile_cover_pic_outer">
            <div class="back-search-link"><a href="<?php echo e(url('search')); ?>">Back To Search Results</a></div>
            <div class="pro_detail_cover">
                <!-- profile left -->
                <div class="pro_detail_left">
                    <div class="profile_cover_panel">
                        <div class="pro_img_cover" onclick="get_myPhoto();">
                            <?php if(!empty($profile_image)): ?>
                              <a href="#" class="profile_img" data-toggle="modal" data-target="#myphoto" style="background-image:url(<?php echo e($profile_img); ?>)"></a>
                                <?php if($self_user): ?>
                                <label class="profile-label"><input type="file" name="profile_img" id="edit_profileptc"> <i class="fa fa-camera"></i></label>
                                <?php endif; ?>              
                            <?php else: ?>
                            <a href="#" class="profile_img" data-toggle="modal" data-target="#myphoto" style="background-image:url(<?php echo e($profile_img); ?>)"></a>
                              <?php if($self_user): ?>
                              <label class="profile-label"><input type="file" name="profile_img" id="edit_profileptc"> <i class="fa fa-camera"></i></label>
                              <?php endif; ?>
                            <?php endif; ?>
                        
                            <?php if(!$self_user): ?>
                            <?php if($user_detail->is_login): ?>
                                <div class="status online"><i class="fa fa-circle"></i> Online</div>
                            <?php else: ?>
                                <div class="status offline"><i class="fa fa-circle"></i> Offline</div>
                            <?php endif; ?>                
                            <?php endif; ?>
                        </div>
                        <div class="profile_user_info">
                          <div class="user_info_inner">
                            <h3><?php echo e(ucwords($user_detail->name)); ?> 
                              <?php if($self_user): ?>
                                <span><button type="button" class="btn btn-info pp_btn" data-toggle="modal" data-target="#infoModal"><i class="fa fa-pencil" aria-hidden="true"></i></button></span>
                              <?php endif; ?>
                            </h3>
                            <?php 
                            $country = (!empty($user_detail->state)) ?  ucwords($user_detail->state) :  ucwords($user_detail->country);
                            ?>
                            <p><?php echo e($user_detail->age); ?> year old <?php echo e($country); ?></p>
                          </div>
                        <!-- like fav button -->
                            <?php if(!$self_user): ?>
                            <div class="like-main">
                            <?php if(count($liked)<=0): ?>
                                <?php if(Auth::check() && Auth::user()->status == 1): ?>
                                    <div class="edit-link profile-like" data-reciever="<?php echo e($user_detail->user_id); ?>"><label>                                
                                    <a href="javascript:void(0)"> <i class="fa fa-star"></i> Favorites</a></label></div>  
                                <?php else: ?>
                                    <div class="edit-link profile-liked"><label><a data-toggle="modal" data-target="#notverifyModal"> <i class="fa fa-star"></i> Favorites</a></label></div>
                                <?php endif; ?>    
                              
                            <?php else: ?>
                              <?php if($liked[0]->status == 1): ?>
                              <div class="edit-link profile-liked"><label><a href="javascript:void(0)"> <i class="fa fa-star"></i> Favorited</a></label></div>
                              <?php endif; ?>
                            <?php endif; ?> 
                            </div>
                        <?php else: ?>
                          <!-- <div class="edit-link"><label><input type="file" id="edit_cover"> <i class="fa fa-camera"></i> Change Cover</label></div> -->
                        <?php endif; ?>
                      <!-- -->
                       <?php if(!$self_user): ?>
                        <div class="like-main1">
                          <?php if(count($liking)<=0): ?>
                            <?php if(Auth::check() && Auth::user()->status == 1): ?>
                                <div class="edit-link profile-liking" data-like="<?php echo e($user_detail->user_id); ?>">
                                    <label>
                                      <a href="javascript:void(0)"> <i class="fa fa-heart"></i> Like</a>
                                    </label>
                                </div>
                            <?php else: ?>
                                <div class="edit-link profile-liked">
                                    <label>
                                      <a data-toggle="modal" data-target="#notverifyModal"><i class="fa fa-heart"></i> Like</a>
                                    </label>
                                </div>                                
                            <?php endif; ?>    
                          
                          <?php else: ?>
                            <div class="edit-link profile-liked">
                              <label>
                                <a href="javascript:void(0)"><i class="fa fa-heart"></i> Liked</a>
                              </label>
                            </div>
                          <?php endif; ?>  
                        </div>
                      <?php endif; ?>
                      <!-- -->
                        <!-- like fav button -->
                      </div>
                    </div>
                </div>
                 <!-- profile left -->
                 <!-- profile right -->
                 <div class="pro_detail_right">
                    <div class="user_search_inner">
                      <?php if(!$self_user && $users): ?>
                         <?php if(session()->has('message')): ?>                    
                            <div class="alert alert-success alert-dismissible">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <strong>Success!</strong> <?php echo e(session()->get('message')); ?>

                            </div>    
                        <?php endif; ?> 
                        <p id="proposal_message" style="color:green; display:none;">Proposal is sent, please check <a href="<?php echo e(url('/inbox')); ?>" onclick="localStorage.setItem('chatTab', 'Outbox');" target="_blank">outbox</a>.</p>
                        <p id="chat_message" style="color:green; display:none;">your message has been sent, please check <a href="<?php echo e(url('/inbox')); ?>" onclick="localStorage.setItem('chatTab', 'Outbox');" target="_blank">outbox</a>.</p>
                            <div class="pro_chat_form">
                                <h4>Send a message</h4>
                                <?php if($users->status): ?>
                                  <div class="chat_now_box">
                                <?php endif; ?>
                                    <div class="chat_footer_inr">
                                        <textarea class="" name="mesage" id="message" rows="10" placeholder="Write a Message.." autofocus
                                          autocomplete="false" required></textarea>
                                    </div>
                                    <script>       
                                        var config = { };
                                        config.placeholder = 'Type Your Message....';                                        
                                        var editor = CKEDITOR.replace( 'mesage' ,{
                                                        height: 400
                                                    });
                                        //editor.resize( '850', '850' );
                                    </script>    
                                    <!--<input class="alreadyChat" type="search" id="chatNowtext" name="mesage" placeholder="Type Your Message Here" required> -->
                                    <!--<input id="chatNowuserId" type="hidden" name="receiverId" value="<?php echo e($user_detail->user_id); ?>">
                                    <input id="chatNowuserName" type="hidden" name="receiverId" value="<?php echo e($user_detail->name); ?>">
                                    <input id="chatNowuserImage" type="hidden" name="receiverId" value="<?php echo e($user_detail->profile_image); ?>">-->
                                    <input id="chatNowuserId" type="hidden" name="receiverId" value="{{ ($user_detail->user_id) ? $user_detail->user_id : $users->id }}">
                                    <input id="receiver_id" type="hidden" name="receiverId" value="<?php echo e($user_detail->user_id); ?>">
                                    <input id="receiver_name" type="hidden" name="receiverId" value="<?php echo e($user_detail->name); ?>">
                                    <input id="receiver_image" type="hidden" name="receiverId" value="<?php echo e($user_detail->profile_image); ?>">
                                    <input type="hidden" id="points">
                                  <input type="hidden" id="planType">
                                  </div>
                            </div>
                            <div class="pro_chat_form_btn">
                                <div class="chat_btns">
                                    <span class="emoji_btn emoji_icon">
                                        <i class="fa fa-smile-o"></i>
                                    </span>
                                </div>
                                <div class="chat_btns">
                                    <div class="upload_pic_btn">
                                        <input type="file" id="file" name="file">
                                        <label for="file" class="chat_btns_design">
                                            <span class="iconss"><i class="fa fa-camera"></i></span>
                                            <span class="texts">Photo</span>
                                        </label>                                        
                                    </div>   
                                </div> 
                                <div class="chat_btns">
                                  <?php if($users->status): ?>
                                  <!--<form action="<?php echo e(url('chat-proposal')); ?>" method="post">                                -->
                                    <?php endif; ?>
                                    <!--<input type="search" name="mesage" placeholder="Send your Proposal" required>-->
                                    <input type="hidden" name="proposalreceiverId" value="<?php echo e($user_detail->user_id); ?>">
                                    <?php if($users->status): ?>
                                      <div class="chat_btns_design send_proposal_btn">
                                        <span class="iconss"><img src="<?php echo e(asset('public/images/icons/evil_heart.png')); ?>"></span>
                                        
                                        <span class="texts">Fantasy</span>
                                      </div>
                                      <!--</form>  -->  
                                    <?php else: ?>
                                      <div class="chat_btns_design" data-toggle="modal" data-target="#notverifyModal">
                                        <span class="iconss"><img src="<?php echo e(asset('public/images/icons/evil_heart.png')); ?>"></span>
                                        <span class="texts">Fantasy</span>
                                      </div>
                                    <?php endif; ?>
                                </div>
                                <div class="chat_btns">
                                    <?php if($users->status): ?>
                                        <button style="display:none;" onclick="sendMsg();" class="alreadyChat btn btn-bg">Send</button>
                                    <?php else: ?>
                                        <button class="btn btn-bg" data-toggle="modal" data-target="#notverifyModal">Send</button>  
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="image_path_dv">
                              <div id="selected"></div> 
                            </div>
                        <?php endif; ?>

                        <!-- Emoji list start -->
                      <div class="emoji-list">
                          <ul>                                
                              
                            </ul>
                        </div>
                        <!-- Emoji list end -->
                        <!-- send proposal wrap Start -->
                          <div class="col-lg-12 colxs-12">
                            <div class="send_proposal_wrap">
                              <div class="send_proposal_inner">
                                <div class="proposal_heading">Select proposal <small>("NOTE: By Clicking on images, the proposal will be send directly.")</small>
                                  <span class="close_pro">&times;</span>
                                </div>
                                <div class="proposal_tabs">
                                  <ul class="tab_menu">
                                    <?php $__currentLoopData = $proposal_category; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <?php if($key == 0): ?>
                                            <li  class="active" onclick="get_proposal(<?php echo e($row->id); ?>)"><?php echo e($row->title); ?></li>
                                        <?php else: ?>
                                            <li  class="" onclick="get_proposal(<?php echo e($row->id); ?>)"><?php echo e($row->title); ?></li>
                                        <?php endif; ?>    
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>                            
                                  </ul>
                                </div>
                                <div class="send_pro_img_wrap">
                                  <?php $__currentLoopData = $proposal_category; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <?php if($key == 0): ?>
                                        <div class="prop_tab_content" id="proposal_0">
                                          <!--
                                          <?php $__currentLoopData = $proposals; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $rows): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <?php if($rows->category_id == $row->id): ?>    
                                                    <div class="send_pro_img">
                                                        <img src="<?php echo e(asset('public/images/profile_image/'.$rows->image)); ?>" class="img-responsive" onClick="sendMsg1('<?php echo e(asset('public/images/profile_image/'.$rows->image)); ?>');">
                                                    </div>
                                                <?php endif; ?>    
                                          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                          -->
                                        </div>
                                    <?php else: ?>
                                        <div class="tab_content" id="proposal_<?php echo e($key); ?>">
                                            <!--
                                            <?php $__currentLoopData = $proposals; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $rows): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <?php if($rows->category_id == $row->id): ?>    
                                                    <div class="send_pro_img">
                                                        <img src="<?php echo e(asset('public/images/profile_image/'.$rows->image)); ?>" class="img-responsive" onClick="sendMsg1('<?php echo e(asset('public/images/profile_image/'.$rows->image)); ?>');">
                                                    </div>
                                                <?php endif; ?>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            -->
                                        </div>
                                    <?php endif; ?>
                                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                  
                                </div>
                              </div> 
                            </div>
                          </div>
                      <!-- send proposal wrap End -->                        
                    </div>
                 </div>
                 <!-- profile right -->
             </div>
          </div>
          <?php else: ?>
          <div class="profile_cover_pic_outer no_cover1" style="background-image:url(<?php echo e(asset('public/images/cover_img/default_cover_1.jpg')); ?>)">
            <div class="back-search-link col-md-6"><a href="<?php echo e(url('search')); ?>">Back To Search Results</a></div>

            <?php if(!$self_user): ?>
              
                  <div class="like-main">
                  <?php if(count($liked)<=0): ?>  
                  <div class="edit-link profile-like col-md-6" data-reciever="<?php echo e($user_detail->user_id); ?>"><label>
                    <!-- <span class="load-like"></span> -->
                    <a href="javascript:void(0)"> <i class="fa fa-star"></i> Favorites</a></label></div>
                <?php else: ?>
                  <?php if($liked[0]->status == 1): ?>
                  <div class="edit-link profile-liked"><label><a href="javascript:void(0)"> <i class="fa fa-star"></i> Favorites</a></label></div>
                  <?php endif; ?>
                <?php endif; ?>
                </div>
            <?php else: ?>
              <div class="edit-link "><label><input type="file" id="edit_cover"> <i class="fa fa-camera"></i> Add Cover</label></div>
            <?php endif; ?>
            
            <?php if(!$self_user): ?>
            <?php if($user_detail->is_login): ?>
                <div class="status online"><i class="fa fa-circle"></i> Online</div>
            <?php else: ?>
                <div class="status offline"><i class="fa fa-circle"></i> Offline</div>
            <?php endif; ?>                
            <?php endif; ?>
          
          </div>
          <?php endif; ?>


          
        <div class="clearfix"></div>
        <div class="row">
          <div class="col-md-12">
            <div class="profile-user-information">
                <?php if(!empty($user_detail->about_me)): ?>

                  <?php if($self_user): ?>
                  <span class="abt_myslf"><button type="button" class="btn btn-info pp_btn" data-toggle="modal" data-target="#myslfModal"><i class="fa fa-pencil" aria-hidden="true"></i></button></span>
                  <?php endif; ?>
                  <p class="more">
                  <?php
                    $about_me = trim($user_detail->about_me);
                    echo ucfirst($about_me); 
                  ?>
                  </p>
                <?php else: ?>
                
                  <?php if($self_user): ?>
                    <span class="abt_myslf"><button type="button" class="btn btn-info pp_btn" data-toggle="modal" data-target="#myslfModal"><i class="fa fa-pencil" aria-hidden="true"></i></button></span>
                  <?php endif; ?>
                  <p class="more">
                    A Few Words About Myself 
                  </p>
                <?php endif; ?>
              </p> 
            </div>
          </div>
        </div>
        
        <?php if($self_user): ?>
          <div class="row gallery-box-panel">
          <div class="col-md-2" style="padding-right:0">
            <div class="upload_btn_gallery">
            <h2>Add more photos </h2>
              <div class="upload_btn_gallery_inner"><input type="file" name="my_photo" id="my_photo"><label for="my_photo">Upload Photo</label></div>
          
                      
            </div>
          </div>
          <div class="col-md-10">
             
            
            <div class="owl-carousel owl-theme" id="owl-carousel-photo">
                <?php if(!empty($user_detail->my_photos) && count($user_detail->my_photos)): ?>
                  <?php $__currentLoopData = $user_detail->my_photos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $single): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    
                    <div class="item">
                      <div class="img_item_iner">                        
                        <?php if($user_detail->fake): ?>
                            <img data-toggle="modal" data-target="#photo_edit_modal" src="<?php echo e(env('PROFILE_MANAGER_ADDR').('/public/images/profile_image')); ?>/<?php echo e($single->gallery_image); ?>" onerror="this.src='<?php echo e(asset("public/images/".$default_img)); ?>'">
                        <?php else: ?>    
                            <img data-toggle="modal" data-target="#photo_edit_modal" src="<?php echo e(asset('public/images/profile_image')); ?>/<?php echo e($single->gallery_image); ?>" onerror="this.src='<?php echo e(asset("public/images/".$default_img)); ?>'">
                        <?php endif; ?>    
                        <span id="<?php echo e($single->id); ?>" class="gallery_delete"><i class="fa fa-trash"></i></span>
                      </div>
                    </div>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php endif; ?>

                
                  <?php if(!empty($user_detail->private_photos) && count($user_detail->private_photos)): ?>
                  <?php $__currentLoopData = $user_detail->private_photos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $single): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="item">
                      <div class="img_item_iner">
                      <?php if($user_detail->fake): ?>
                          <img data-toggle="modal" data-target="#photo_edit_modal" src="<?php echo e(env('PROFILE_MANAGER_ADDR').('/public/images/profile_image')); ?>/<?php echo e($single->gallery_image); ?>" onerror="this.src='<?php echo e(asset("public/images/".$default_img)); ?>'">
                      <?php else: ?>
                          <img data-toggle="modal" data-target="#photo_edit_modal" src="<?php echo e(asset('public/images/profile_image')); ?>/<?php echo e($single->gallery_image); ?>" onerror="this.src='<?php echo e(asset("public/images/".$default_img)); ?>'">
                      <?php endif; ?>    
                      <span id="<?php echo e($single->id); ?>" class="gallery_delete"><i class="fa fa-trash"></i></span>
                    </div>
                  </div>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  <?php endif; ?>
                              
            </div>
          </div>
          </div>

        <?php endif; ?>

        <?php if(!$self_user): ?>
        <div class="row gallery-box-panel">         
          <div class="col-md-4" onclick="get_myPhoto();">
            <?php if(count($user_detail->my_photos) > 0): ?>
                <?php if($user_detail->fake): ?>
                <a href="#" class="gallery_bg" data-toggle="modal"  style="background-image:url(<?php echo e(env('PROFILE_MANAGER_ADDR').('/public/images/profile_image')); ?>/<?php echo e($user_detail->my_photos[0]->gallery_image); ?>)">            
                  <div class="gallery_icon"><i class="fa fa-heart"></i><i class="fa fa-camera small-icon"></i></div>
                  <div class="gallery-title"><h4>My Photos (<?php echo e(count($user_detail->my_photos)); ?>)</h4>
                </div>
                </a>
                <?php else: ?>
                <a href="#" class="gallery_bg" data-toggle="modal" data-target="#myphoto" style="background-image:url(<?php echo e(asset('public/images/profile_image')); ?>/<?php echo e($user_detail->my_photos[0]->gallery_image); ?>)">            
                  <div class="gallery_icon"><i class="fa fa-heart"></i><i class="fa fa-camera small-icon"></i></div>
                  <div class="gallery-title"><h4>My Photos (<?php echo e(count($user_detail->my_photos)); ?>)</h4>
                </div>
                </a>
                <?php endif; ?>    
            <?php else: ?>
                <?php if($user_detail->fake): ?>
                <div class="gallery_bg" style="background-image:url(<?php echo e(env('PROFILE_MANAGER_ADDR').('/public/images/photo-bg.jpg')); ?>)"> 
                  <div class="gallery_icon"><i class="fa fa-heart"></i> <i class="fa fa-play small-icon"></i>
                  </div>
                  <div class="gallery-title"><h4>My Photos (<?php echo e(count($user_detail->my_photos)); ?>)</h4>
                  
                  </div>
                </div>
                <?php else: ?>
                <div class="gallery_bg" style="background-image:url(<?php echo e(asset('public/images/photo-bg.jpg')); ?>)"> 
                  <div class="gallery_icon"><i class="fa fa-heart"></i> <i class="fa fa-play small-icon"></i>
                  </div>
                  <div class="gallery-title"><h4>My Photos (<?php echo e(count($user_detail->my_photos)); ?>)</h4>
                  
                  </div>
                </div>    
                <?php endif; ?>                         
            <?php endif; ?>


          </div>


          <div class="col-md-4" onclick="get_myPrivatePhoto();">
            <?php if(count($user_detail->private_photos) > 0): ?>
            
            
            
              <a href="#" class="gallery_bg private_bt_blur">
            
                <div class="private_thumb">
                    <?php if($user_detail->fake): ?>
                    <img src="<?php echo e(env('PROFILE_MANAGER_ADDR').('/public/images/profile_image')); ?>/<?php echo e($user_detail->private_photos[0]->gallery_image); ?>" alt="" class="img-responsive">
                    <?php else: ?>
                    <img src="<?php echo e(asset('public/images/profile_image')); ?>/<?php echo e($user_detail->private_photos[0]->gallery_image); ?>" alt="" class="img-responsive">
                    <?php endif; ?>               
                    <div class="gallery_text_cvr">
                      <div class="gallery_icon"><i class="fa fa-heart"></i> <i class="fa fa-camera small-icon"></i>
                      </div>
                      <div class="gallery-title"><h4>My Private Photos (<?php echo e(count($user_detail->private_photos)); ?>)</h4>
                    </div>
                  </div>
                </div>
            </a>
            <?php else: ?>
            <a href="#" class="gallery_bg private_bt_blur">    
                <div class="gallery_bg" style="background-image:url(<?php echo e(asset('public/images/private-photo-bg.jpg')); ?>)"> <div class="gallery_icon"><i class="fa fa-heart"></i> <i class="fa fa-play small-icon"></i></div>
                <div class="gallery-title"><h4>My Private Photos (<?php echo e(count($user_detail->private_photos)); ?>)</h4></div></div>                                        
            </a>
            <?php endif; ?>
          </div>
        </div>
        <?php endif; ?>


        <div class="row user_about_info">
          <div class="col-md-4 col-sm-6">
            <div class="user_about_info_inner">
              <h4>
                My Interests
                
                <?php if($self_user): ?>
                  <span><button type="button" class="btn btn-info cmn_btn" data-toggle="modal" data-target="#interestModal"><i class="fa fa-pencil" aria-hidden="true"></i></button></span>
                <?php endif; ?>
              </h4>
              <ul class="intrest_list">
                <?php $get_interests = Helper::getAllInterests($user_detail->interests);?>
                <?php if(!empty($get_interests)): ?>
                <?php $__currentLoopData = $get_interests; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <?php 
                $name = (!empty($val->name)) ? $val->name : '';
                //$icon = (!empty($val->icon)) ? $val->icon : 'fa fa-motorcycle';
                ?>
                  <?php if(!empty($name)): ?>
                  <li>
                    <div class="interest biking"><img src="<?php echo e(asset('public/icons/'.$val->icon)); ?>" width="50" height="50"> <span><?php echo e(ucwords($name)); ?> </span></div>
                  </li>
                  <?php endif; ?>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php else: ?>
                No answer
                <?php endif; ?>

              </ul>
            </div>
          </div>
          <div class="col-md-4 col-sm-6">
            <div class="user_about_info_inner">
              <h4>About Me
              <?php if($self_user): ?>
                <span><button type="button" class="btn btn-info cmn_btn" data-toggle="modal" data-target="#aboutModal"><i class="fa fa-pencil" aria-hidden="true"></i></button></span>
              <?php endif; ?>
              </h4>
              <?php 
              $get_lang = Helper::getAllLanguages($user_detail->languages);

              $lang_know = (!empty($get_lang[0]) && count($get_lang)>0) ? implode(', ',$get_lang) : 'No answer';

              if($user_detail->kids == 1)
              {
                $kids = 'Yes';
              }
              else if($user_detail->kids == 2)
              {
                $kids = 'No';
              }
              else{
                $kids = 'No answer';
              }

              if($user_detail->smoke == 1)
              {
                $smoke = 'Never';
              }
              else if($user_detail->smoke == 2)
              {
                $smoke = 'Regularly';
              }
              else if($user_detail->smoke == 3)
              {
                $smoke = 'Occasionally';
              }
              else{
                $smoke = 'No answer';
              }

              if($user_detail->drink == 1)
              {
                $drink = 'Never';
              }
              else if($user_detail->drink == 2)
              {
                $drink = 'Regularly';
              }
              else if($user_detail->drink == 3)
              {
                $drink = 'Occasionally';
              }
              else{
                $drink = 'No answer';
              }


              if($user_detail->relationship == 1)
              {
                $relationship = 'Single';
              }
              else if($user_detail->relationship == 2)
              {
                $relationship = 'Living Apart Together';
              }
              else if($user_detail->relationship == 3)
              {
                $relationship = 'Divorced';
              }
              else if($user_detail->relationship == 4)
              {
                $relationship = 'Widowed';
              }
        else if($user_detail->relationship == 5)
              {
                $relationship = 'In Relashionship';
              }
        else if($user_detail->relationship == 6)
              {
                $relationship = 'Living Together';
              }
        else if($user_detail->relationship == 7)
              {
                $relationship = 'Married';
              }
              else{
                $relationship = 'No answer';
              }

              $live_in =  (!empty($user_detail->address)) ? $user_detail->address : 'No answer';
              $work_as =  (!empty($user_detail->work_as)) ? $user_detail->work_as : 'No answer';
              $education =  (!empty($user_detail->education)) ? $user_detail->education : 'No answer';
              $height =  (!empty($user_detail->height)) ? $user_detail->height : 'No answer'; 
              ?>
              <ul>
                <li> Live in: <strong><?php echo e(ucfirst($live_in)); ?></strong></li>
                <li>Work as: <strong><?php echo e(ucfirst($work_as)); ?></strong></li>
                <!--<li>Education: <strong><?php echo e(ucfirst($education)); ?></strong></li>-->
                <!--<li>Know: <strong><?php echo e($lang_know); ?></strong></li>-->
                <li>Relationship: <strong><?php echo e($relationship); ?></strong></li>
                <!--<li>Have kids: <strong><?php echo e($kids); ?></strong></li>-->
                <li>Smoke: <strong><?php echo e($smoke); ?></strong></li>
                <li>Drink: <strong><?php echo e($drink); ?></strong></li>
                <li>Height:<strong> <?php echo e(ucfirst($height)); ?></strong></li>

              </ul>
            </div>
          </div>
          <div class="col-md-4 col-sm-6">
            <div class="user_about_info_inner">
              <h4>I'm Looking for
                <?php if($self_user): ?>
                  <span><button type="button" class="btn btn-info cmn_btn" data-toggle="modal" data-target="#lkgModal"><i class="fa fa-pencil" aria-hidden="true"></i></button></span>
                <?php endif; ?>
              </h4>
              <!-- <h6> Man, 18 years and older</h6> -->
              <?php 
              if($user_detail->seeking_val == 1)
              {
                $look_for = 'Man';
              }
              if($user_detail->seeking_val == 2)
              {
                $look_for = 'Woman';
              }

              if(!empty($user_detail->min_age) && !empty($user_detail->max_age))
              { 
                $seeking = $look_for.', '.$user_detail->min_age.' - '.$user_detail->max_age.' years old';
              }
              else if(!empty($user_detail->min_age))
              {
                $seeking = $look_for.', older than '.$user_detail->min_age.' years old';
              }
              else{
                $seeking = $user_detail->seeking;
              }
              ?>
              <h6> <?php echo e(ucfirst($seeking)); ?></h6>
              <p><?php echo e(ucfirst($user_detail->about_partner)); ?> </p>
            </div>
          </div>
        </div>
        
        <?php if(Auth::id() != $user_detail->user_id): ?>
        <div id="search-page" class="row">

          <div class="col-md-12"><h5>See more Like <?php echo e(ucwords($user_detail->name)); ?></h5></div>
        </div>
        <div class="nw-profiles">
         <div class="nw-profiles-otr">
          
      </div>
      </div>

      <script>
       
      setTimeout(function(){ get_similar(); }, 3000);  
      function get_similar(){
        var userid = $('#chatNowuserId').val();
        $.ajax({
                headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '<?php echo url("/")?>/get_similar',
                type: 'POST',
                cache : false,
                processData: false,
                data: 'user_id='+userid, // serializes the form's elements.
                success: function(data)
                {
                 //console.log(data);
                 if(data){
                        $('.nw-profiles-otr').html(data);
                  }                      
                }
            });
      }
    </script>

      <?php endif; ?>
      </div> <!-- end profile_popup-->
            
      </div>
        <!--  chat popup  --> 
      <div id="fake_chat" class="profile_fake_chat">
      </div>
        <!--  chat popup  -->
      <?php echo $__env->make('chat_sidebar', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    </div>
  </div>
</section>


@if($self_user)
<div id="photo_edit_modal" class="modal fade profile_gallery_popup" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content"> <button type="button" class="close" data-dismiss="modal" onclick="fotorama3();">&times;</button>

      <div class="modal-body">
        <div class="fotorama" id="fotorama3" data-fit="scaledown" data-nav="false" data-width="100%" data-height="100%" data-max-width="100%" data-maxheight="100%" data-keyboard="true" data-loop="true" data-fit="cover">
          <?php if($user_detail->source_img): ?>
            <img src="<?php echo e($user_detail->source_img); ?>"/>
          <?php else: ?>
            <img src="<?php echo e(asset('public/images/profile_image/'.$user_detail->profile_image)); ?>"/>  
          <?php endif; ?> 
          
          <?php if(!empty($user_detail->my_photos) && count($user_detail->my_photos)): ?>
          <?php $__currentLoopData = $user_detail->my_photos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $single): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
              <?php if($user_detail->fake): ?>
                <img src="<?php echo e(env('PROFILE_MANAGER_ADDR').('/public/images/profile_image')); ?>/<?php echo e($single->gallery_image); ?>" onerror="this.src='<?php echo e(asset("public/images/profile_image/no_image.jpg")); ?>'">
              <?php else: ?>  
                <img src="<?php echo e(asset('public/images/profile_image')); ?>/<?php echo e($single->gallery_image); ?>" onerror="this.src='<?php echo e(asset("public/images/profile_image/no_image.jpg")); ?>'">
              <?php endif; ?>  
          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
          <?php endif; ?>

          
          <?php if(!empty($user_detail->private_photos) && count($user_detail->private_photos)): ?>
          <?php $__currentLoopData = $user_detail->private_photos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $single): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
              <?php if($user_detail->fake): ?>
                <img src="<?php echo e(env('PROFILE_MANAGER_ADDR').('/public/images/profile_image')); ?>/<?php echo e($single->gallery_image); ?>" onerror="this.src='<?php echo e(asset("public/images/profile_image/no_image.jpg")); ?>'">
              <?php else: ?>  
                <img src="<?php echo e(asset('public/images/profile_image')); ?>/<?php echo e($single->gallery_image); ?>" onerror="this.src='<?php echo e(asset("public/images/profile_image/no_image.jpg")); ?>'">
              <?php endif; ?>  
          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
          <?php endif; ?>
          
        </div>
            
            <script>
            function fotorama3() {
              // 1. Initialize fotorama manually.
              var $fotoramaDiv = $('#fotorama3').fotorama();
          
              // 2. Get the API object.
              var fotorama = $fotoramaDiv.data();
          
              // 3. Inspect it in console.
              //console.log('parvez');
              //console.log(fotorama.fotorama);
              fotorama.fotorama.show(0);
            }
          </script>
      </div>

    </div>

  </div>
</div>
@endif

<div id="myphoto" class="modal fade profile_gallery_popup" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content"> <button type="button" class="close" data-dismiss="modal" onclick="fotorama();">&times;</button>

      <div class="modal-body">
        <div class="fotorama"  id="fotorama" data-fit="scaledown" data-nav="false" data-width="100%" data-height="100%" data-max-width="100%" data-maxheight="100%" data-keyboard="true" data-loop="true" data-fit="cover" >
            <img src="https://moneoangels.com/public/images/profile_image/8494324_1.jpg">
            <img src="https://moneoangels.com/public/images/profile_image/8494324_3.jpg">
            <img src="https://moneoangels.com/public/images/profile_image/8494324_5.jpg">  
          
        </div>
            
        <script>
            function fotorama() {
              // 1. Initialize fotorama manually.
              var $fotoramaDiv = $('#fotorama').fotorama();
          
              // 2. Get the API object.
              var fotorama = $fotoramaDiv.data();
          
              // 3. Inspect it in console.
              //console.log('parvez');
              //console.log(fotorama.fotorama);
              fotorama.fotorama.show(0);
            }
          </script>    
      </div>

    </div>

  </div>
</div>

<script>  
  setTimeout(function(){ getmyPhoto(); }, 1000); 
  function getmyPhoto(){
    $('#fotorama').html('');
    var userid = $('#chatNowuserId').val();    
    $.ajax({
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '<?php echo url("/")?>/get_myPhoto',
            type: 'POST',
            cache : false,
            processData: false,
            data: 'user_id='+userid, // serializes the form's elements.
            success: function(data)
            {
             //console.log(data);
             if(data){
                     $('#fotorama').html(data);                     
              }                      
            }
        });
  }

  function get_myPhoto(){
    var userid = $('#chatNowuserId').val();
    $.ajax({
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '<?php echo url("/")?>/get_myPhoto',
            type: 'POST',
            cache : false,
            processData: false,
            data: 'user_id='+userid, // serializes the form's elements.
            success: function(data)
            {
             //console.log(data);
             if(data){
                     //$('#fotorama').html(data);
                     $('#myphoto').modal('show');
              }                      
            }
        });
  }
</script>

<div id="mypriphoto" class="modal fade profile_gallery_popup" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content"> <button type="button" class="close" data-dismiss="modal" onclick="fotoramapri();">&times;</button>    
      <div class="modal-body">
      <?php if((count($user_detail->private_photos) == 0)): ?>
        <div class="request_photo">                               
        <div class="request_photo_inner">
                <p class="">Request a private picture from <?php echo e($user_detail->name); ?>. Send her a message to get het attention! </p>
                <div class="request_input">
                <?php if($users->status): ?>
                <!--<form action="" method="post"> -->
                  <div class="chat_now_box1">
              <?php endif; ?> 
                    <input  type="search" id="chatNowtext1" name="mesage" placeholder="Type Your Message Here" required>                    
                    <?php if($users->status): ?>
                    <button  onclick="sendMsg1();" class=" btn btn-bg">Chat Now</button>
                  </div>
                <!--</form>    -->
                  <?php else: ?>
                    <button class="btn btn-bg" data-toggle="modal" data-target="#notverifyModal">Chat Now</button>  
                  <?php endif; ?>
                  </div>    
            </div>    
        </div>
      <?php else: ?>      
            <div class="fotorama" id="fotoramapri" data-fit="scaledown" data-nav="false" data-width="100%" data-height="100%" data-max-width="100%" data-maxheight="100%" data-keyboard="true" data-loop="true" data-fit="cover" >          
                            
                        
            </div>
                
            <script>
                function fotoramapri() {
                  // 1. Initialize fotorama manually.
                  var $fotoramaDiv = $('#fotoramapri').fotorama();
              
                  // 2. Get the API object.
                  var fotorama = $fotoramaDiv.data();
              
                  // 3. Inspect it in console.
                  //console.log('parvez');
                  //console.log(fotorama.fotorama);
                  fotorama.fotorama.show(0);
                }
              </script>    
      <?php endif; ?>      
            
            
            
      </div>

    </div>

  </div>
</div>


<script>

  setTimeout(function(){ getmyPrivatePhoto(); }, 1500); 
  function getmyPrivatePhoto(){
    $('#fotoramapri').html('');   
    var userid = $('#chatNowuserId').val();
    $.ajax({
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '<?php echo url("/")?>/get_myPrivatePhoto',
            type: 'POST',
            cache : false,
            processData: false,
            data: 'user_id='+userid, // serializes the form's elements.
            success: function(data)
            {
             //console.log(data);
             if(data){
                     $('#fotoramapri').html(data);                     
              }                      
            }
        });
  }

  function get_myPrivatePhoto(){
    var userid = $('#chatNowuserId').val();
    $.ajax({
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '<?php echo url("/")?>/get_myPrivatePhoto',
            type: 'POST',
            cache : false,
            processData: false,
            data: 'user_id='+userid, // serializes the form's elements.
            success: function(data)
            {
             //console.log(data);
             //if(data){                     
                     $('#mypriphoto').modal('show');
              //}                      
            }
        });
  }
</script>
  
<!--  modal starts here  -->
<!-----------------------------  if self user then this modal will load----------------------------------------->
<?php if($self_user): ?>
<!-- interest Modal -->
  <div class="modal fade" id="interestModal" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content modal-intrst">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <form id="edit_interests_form">
            <div class="editable-section">
                  <ul class=" bubbles clearfix">
                    <?php if(!empty($all_interests) && count($all_interests)>0): ?>
                    <?php $__currentLoopData = $all_interests; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $single): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <?php 
                    if(!empty($user_detail->interests))
                    {
                      $array_interests = explode(',',$user_detail->interests);
                    
                      if(in_array($single->id, $array_interests))
                      {
                        $checked_interest = 'checked="checked"';
                      }
                      else{
                        $checked_interest = '';
                      }
                    }
                    else{
                      $checked_interest = '';
                    }
                    ?>
                      <li class="field group">
                        <label><input type="checkbox" class="interest bubble" name="edit_interests" value="<?php echo e($single->id); ?>" tabindex="2" <?php echo e($checked_interest); ?>><span class="name"><?php echo e($single->name); ?></span></label>
                      </li>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                  </ul>
              </div>
            <div class="modal-footer md-center">
              <button type="submit" class="btn btn-default">Save</button>
            </div>
          </form>
        </div>
      </div>
      
    </div>
  </div><!--End of interest modal -->  

  <!--About me modal -->
  <div class="modal fade" id="aboutModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content modal-intrst">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <form class="form-inline" id="edit_about_detail_form">
          <div class="intrst-frm-frm">
            <div class="row">
             <div class="col-md-5 text-right"><div class="form-group"><label for="email">Where do you live?</label></div></div>
              <div class="col-md-7 abt_me_md">
                <input type="text" class="form-control" id="edit_address"  name="" value="<?php echo e($user_detail->address); ?>"></div>
                <input type="hidden" id="edit_city" name="" value="<?php echo e($user_detail->city); ?>">
                <input type="hidden" id="edit_country" name="" value="<?php echo e($user_detail->country); ?>">
             </div>
           </div>
           <div class="intrst-frm-frm">
            <div class="row">
             <div class="col-md-5 text-right"><div class="form-group"><label for="email">What is your occupation?</label></div></div>
              <div class="col-md-7 abt_me_md">
                <input type="text" class="form-control" id="edit_work_as" name="" value="<?php echo e($user_detail->work_as); ?>"></div>
             </div>
           </div>
          
           <div class="intrst-frm-frm">
            <div class="row">
             <div class="col-md-5 text-right"><div class="form-group"><label for="email">What's your relationship status?</label></div></div>
              <div class="col-md-7 abt_me_md">
                  <select class="form-control" id="edit_relationship">
                    <option value="1" <?php echo e(($user_detail->relationship == '1')  ? 'selected="selected"' : ''); ?>>Single</option>
                    <option value="3" <?php echo e(($user_detail->relationship == '3')  ? 'selected="selected"' : ''); ?>>Divorced</option>
                    <option value="7" <?php echo e(($user_detail->relationship == '7')  ? 'selected="selected"' : ''); ?>>Married</option>
                    <option value="5" <?php echo e(($user_detail->relationship == '5')  ? 'selected="selected"' : ''); ?>>In Relashionship</option>
                    <option value="6" <?php echo e(($user_detail->relationship == '6')  ? 'selected="selected"' : ''); ?>>Living Together</option>
                    <option value="2" <?php echo e(($user_detail->relationship == '2')  ? 'selected="selected"' : ''); ?>>Living Apart Together</option>
                    <option value="4" <?php echo e(($user_detail->relationship == '4')  ? 'selected="selected"' : ''); ?>>Widowed</option>
                  </select>
                </div>
             </div>
           </div>
           
           <div class="intrst-frm-frm">
            <div class="row">
             <div class="col-md-5 text-right"><div class="form-group"><label for="email">Do you smoke?</label></div></div>
              <div class="col-md-7 abt_me_md">
                  <select class="form-control" id="edit_smoke">
                    <option value="" selected="selected"></option>
                    <option value="1" <?php echo e(($user_detail->smoke == '1')  ? 'selected="selected"' : ''); ?>>Never</option>
                    <option value="2" <?php echo e(($user_detail->smoke == '2')  ? 'selected="selected"' : ''); ?>>Regular</option>
                    <option value="3" <?php echo e(($user_detail->smoke == '3')  ? 'selected="selected"' : ''); ?>>Occasionally</option>
                  </select>
                </div>
             </div>
           </div>
           <div class="intrst-frm-frm">
            <div class="row">
             <div class="col-md-5 text-right"><div class="form-group"><label for="email">Do you drink?</label></div></div>
              <div class="col-md-7 abt_me_md">
                  <select class="form-control" id="edit_drink">
                    <option value=""></option>
                     <option value="1" <?php echo e(($user_detail->drink == '1')  ? 'selected="selected"' : ''); ?>>Never</option>
                    <option value="2" <?php echo e(($user_detail->drink == '2')  ? 'selected="selected"' : ''); ?>>Regular</option>
                    <option value="3" <?php echo e(($user_detail->drink == '3')  ? 'selected="selected"' : ''); ?>>Occasionally</option>
                  </select>
                </div>
             </div>
           </div>
           <div class="intrst-frm-frm">
            <div class="row">
             <div class="col-md-5 text-right"><div class="form-group"><label for="email">How tall are you?</label></div></div>
              <div class="col-md-7 abt_me_md">
                  <select class="form-control" id="edit_height">
                    <option value=""></option>
                    <option value="3'"  <?php echo ($user_detail->height == '3\'')  ? 'selected="selected"' : ''; ?>>3'</option>
                    <option value="3'1"" <?php echo ($user_detail->height == '3\'1"')  ? 'selected="selected"' : ''; ?>>3'1"</option>
                    <option value="3'2"" <?php echo ($user_detail->height == '3\'2"')  ? 'selected="selected"' : ''; ?>>3'2"</option>
                    <option value="3'3"" <?php echo ($user_detail->height == '3\'3"')  ? 'selected="selected"' : ''; ?>>3'3"</option>
                    <option value="3'4"" <?php echo ($user_detail->height == '3\'4"')  ? 'selected="selected"' : ''; ?>>3'4"</option>
                    <option value="3'5"" <?php echo ($user_detail->height == '3\'5"')  ? 'selected="selected"' : ''; ?>>3'5"</option>
                    <option value="3'6"" <?php echo ($user_detail->height == '3\'6"')  ? 'selected="selected"' : ''; ?>>3'6"</option>
                    <option value="3'7"" <?php echo ($user_detail->height == '3\'7"')  ? 'selected="selected"' : ''; ?>>3'7"</option>
                    <option value="3'8"" <?php echo ($user_detail->height == '3\'8"')  ? 'selected="selected"' : ''; ?>>3'8"</option>
                    <option value="3'9"" <?php echo ($user_detail->height == '3\'9"')  ? 'selected="selected"' : ''; ?>>3'9"</option>
                    <option value="3'10"" <?php echo ($user_detail->height == '3\'10"')  ? 'selected="selected"' : ''; ?>>3'10"</option>
                    <option value="3'11"" <?php echo ($user_detail->height == '3\'11"')  ? 'selected="selected"' : ''; ?>>3'11"</option>
                    <option value="4'" <?php echo ($user_detail->height == '4\'')  ? 'selected="selected"' : ''; ?>>4'</option>
                    <option value="4'1"" <?php echo ($user_detail->height == '4\'1"')  ? 'selected="selected"' : ''; ?>>4'1"</option>
                    <option value="4'2"" <?php echo ($user_detail->height == '4\'2"')  ? 'selected="selected"' : ''; ?>>4'2"</option>
                    <option value="4'3"" <?php echo ($user_detail->height == '4\'3"')  ? 'selected="selected"' : ''; ?>>4'3"</option>
                    <option value="4'4"" <?php echo ($user_detail->height == '4\'4"')  ? 'selected="selected"' : ''; ?>>4'4"</option>
                    <option value="4'5"" <?php echo ($user_detail->height == '4\'5"')  ? 'selected="selected"' : ''; ?>>4'5"</option>
                    <option value="4'6"" <?php echo ($user_detail->height == '4\'6"')  ? 'selected="selected"' : ''; ?>>4'6"</option>
                    <option value="4'7"" <?php echo ($user_detail->height == '4\'7"')  ? 'selected="selected"' : ''; ?>>4'7"</option>
                    <option value="4'8"" <?php echo ($user_detail->height == '4\'8"')  ? 'selected="selected"' : ''; ?>>4'8"</option>
                    <option value="4'9"" <?php echo ($user_detail->height == '4\'9"')  ? 'selected="selected"' : ''; ?>>4'9"</option>
                    <option value="4'10"" <?php echo ($user_detail->height == '4\'10"')  ? 'selected="selected"' : ''; ?>>4'10"</option>
                    <option value="4'11"" <?php echo ($user_detail->height == '4\'11"')  ? 'selected="selected"' : ''; ?>>4'11"</option>
                    <option value="5'" <?php echo ($user_detail->height == '5\'')  ? 'selected="selected"' : ''; ?>>5'</option>
                    <option value="5'1"" <?php echo ($user_detail->height == '5\'1"')  ? 'selected="selected"' : ''; ?>>5'1"</option>
                    <option value="5'2"" <?php echo ($user_detail->height == '5\'2"')  ? 'selected="selected"' : ''; ?>>5'2"</option>
                    <option value="5'3"" <?php echo ($user_detail->height == '5\'3"')  ? 'selected="selected"' : ''; ?>>5'3"</option>
                    <option value="5'4"" <?php echo ($user_detail->height == '5\'4"')  ? 'selected="selected"' : ''; ?>>5'4"</option>
                    <option value="5'5"" <?php echo ($user_detail->height == '5\'5"')  ? 'selected="selected"' : ''; ?>>5'5"</option>
                    <option value="5'6"" <?php echo ($user_detail->height == '5\'6"')  ? 'selected="selected"' : ''; ?>>5'6"</option>
                    <option value="5'7"" <?php echo ($user_detail->height == '5\'7"')  ? 'selected="selected"' : ''; ?>>5'7"</option>
                    <option value="5'8"" <?php echo ($user_detail->height == '5\'8"')  ? 'selected="selected"' : ''; ?>>5'8"</option>
                    <option value="5'9"" <?php echo ($user_detail->height == '5\'9"')  ? 'selected="selected"' : ''; ?>>5'9"</option>
                    <option value="5'10"" <?php echo ($user_detail->height == '5\'10"')  ? 'selected="selected"' : ''; ?>>5'10"</option>
                    <option value="5'11"" <?php echo ($user_detail->height == '5\'11"')  ? 'selected="selected"' : ''; ?>>5'11"</option>
                    <option value="6'"  <?php echo ($user_detail->height == '6\'')  ? 'selected="selected"' : ''; ?>>6'</option>
                    <option value="6'1""  <?php echo ($user_detail->height == '6\'1"')  ? 'selected="selected"' : ''; ?>>6'1"</option>
                    <option value="6'2""  <?php echo ($user_detail->height == '6\'2"')  ? 'selected="selected"' : ''; ?>>6'2"</option>
                    <option value="6'3""  <?php echo ($user_detail->height == '6\'3"')  ? 'selected="selected"' : ''; ?>>6'3"</option>
                    <option value="6'4""  <?php echo ($user_detail->height == '6\'4"')  ? 'selected="selected"' : ''; ?>>6'4"</option>
                    <option value="6'5""  <?php echo ($user_detail->height == '6\'5"')  ? 'selected="selected"' : ''; ?>>6'5"</option>
                    <option value="6'6""  <?php echo ($user_detail->height == '6\'6"')  ? 'selected="selected"' : ''; ?>>6'6"</option>
                    <option value="6'7""  <?php echo ($user_detail->height == '6\'7"')  ? 'selected="selected"' : ''; ?>>6'7"</option>
                    <option value="6'8""  <?php echo ($user_detail->height == '6\'8"')  ? 'selected="selected"' : ''; ?>>6'8"</option>
                    <option value="6'9""  <?php echo ($user_detail->height == '6\'9"')  ? 'selected="selected"' : ''; ?>>6'9"</option>
                    <option value="6'10""  <?php echo ($user_detail->height == '6\'10"')  ? 'selected="selected"' : ''; ?>>6'10"</option>
                    <option value="6'11""  <?php echo ($user_detail->height == '6\'11"')  ? 'selected="selected"' : ''; ?>>6'11"</option>
                    <option value="7'"  <?php echo ($user_detail->height == '7\'')  ? 'selected="selected"' : ''; ?>>7'</option>
                    <option value="7'1""  <?php echo ($user_detail->height == '7\'1"')  ? 'selected="selected"' : ''; ?>>7'1"</option>
                    <option value="7'2""  <?php echo ($user_detail->height == '7\'2"')  ? 'selected="selected"' : ''; ?>>7'2"</option>
                    <option value="7'3"" <?php echo ($user_detail->height == '7\'3"')  ? 'selected="selected"' : ''; ?>>7'3"</option>
                    <option value="7'4""<?php echo ($user_detail->height == '7\'4"')  ? 'selected="selected"' : ''; ?>>7'4"</option>
                    <option value="7'5""<?php echo ($user_detail->height == '7\'5"')  ? 'selected="selected"' : ''; ?>>7'5"</option>
                    <option value="7'6""<?php echo ($user_detail->height == '7\'6"')  ? 'selected="selected"' : ''; ?>>7'6"</option>
                    <option value="7'7""<?php echo ($user_detail->height == '7\'7"')  ? 'selected="selected"' : ''; ?>>7'7"</option>
                    <option value="7'8""<?php echo ($user_detail->height == '7\'8"')  ? 'selected="selected"' : ''; ?>>7'8"</option>
                    <option value="7'9""<?php echo ($user_detail->height == '7\'9"')  ? 'selected="selected"' : ''; ?>>7'9"</option>
                    <option value="7'10""<?php echo ($user_detail->height == '7\'10"')  ? 'selected="selected"' : ''; ?>>7'10"</option>
                    <option value="7'11""<?php echo ($user_detail->height == '7\'11"')  ? 'selected="selected"' : ''; ?>>7'11"</option>
                  </select>
                </div>
             </div>
           </div>
        <div class="modal-footer md-center">
          <button type="submit" class="btn btn-default">Save</button>
        </div>
      </form>
          </div>
      </div>
      
    </div>
  </div><!--End of about me modal -->


  <!-- interest Modal -->
  <div class="modal fade" id="lkgModal" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content modal-intrst">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          
          <form class="" id="edit_looking_for_form">
          <div class="intrst-frm-frm">
            <div class="row">
             <div class="col-md-6"><div class="form-group">
              <label for="email">I am <?php echo e(($user_detail->gender == 1) ? 'a Man' : 'a Woman'); ?>:</label>
              <select class="form-control" id="edit_seeking_val">
                    <option value="2" <?php echo e(($user_detail->seeking_val == 2)  ? 'selected="selected"' : ''); ?>>Looking for a Woman</option>
                    <option value="1" <?php echo e(($user_detail->seeking_val == 1)  ? 'selected="selected"' : ''); ?>>Looking for a Man</option>                        
                  </select>
            </div>
          </div>
              <div class="col-md-6 abt_me_md">
                <div class="row">
                  <div class="col-md-6">
                <div class="form-group">
              <label for="email">Between ages:</label>
              <select class="form-control" id="edit_min_age">
                    <option value="18">18</option>
                    <?php 
                      for($i=20;$i<=75;$i+=5)
                      {
                        $selected_min_age = (isset($user_detail->min_age) && $user_detail->min_age == $i)  ? 'selected="selected"' : '';
                        echo '<option value="'.$i.'" '.$selected_min_age.'>'.$i.'</option>';
                      }
                    ?>
                  </select>
            </div>
          </div>
          <div class="col-md-6">
                <div class="form-group">
              <label for="email">and</label>
              <select class="form-control" id="edit_max_age">
                    <?php 
                      for($i=20;$i<=80;$i+=5)
                      {
                        $selected_max_age = (isset($user_detail->max_age) && $user_detail->max_age == $i)  ? 'selected="selected"' : '';
                        if($i == 80)
                        {
                          $j = '80+';
                        }
                        else{
                          $j = $i;
                        }
                        echo '<option value="'.$i.'" '.$selected_max_age.'>'.$j.'</option>';
                      }
                    ?>
                  </select>
            </div>
          </div>
          </div>
              </div>
             </div>
             <div class="row">
               <div class="col-md-12">
                 <div class="form-group">
                  <label for="comment">Few words about your ideal partner:</label>
                  <textarea class="form-control" placeholder="Few words about your ideal partner" rows="5" id="edit_partner"><?php echo e($user_detail->about_partner); ?></textarea>
                </div>
               </div>
             </div>
           </div>
           <div class="modal-footer md-center">
            <button type="submit" class="btn btn-default">Save</button>
           </div>
         </form>
        </div>
      </div>
      
    </div>
  </div><!--End of interest modal -->

  <!-- interest Modal -->
  <div class="modal fade" id="infoModal" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content modal-intrst gndr-pp">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <form class="form-inner frm-flrt" id="edit_profile_form">
            <input type="hidden" id="user_id" value="<?php echo e(Auth::id()); ?>">

            <input type="hidden" id="same_address" value="<?php echo e($user_detail->address); ?>">
            <input type="hidden" id="same_city" value="<?php echo e($user_detail->city); ?>">
            <input type="hidden" id="same_country" value="<?php echo e($user_detail->country); ?>">
                    <div class="intrst-frm-frm">
                    <div class="row">
                     <div class="col-md-4 text-right"><div class="form-group"><label for="email">Name or nickname</label></div></div>
                      <div class="col-md-8 abt_me_md">
                        <input type="text" class="form-control f-bg" id="edit_name" value="<?php echo e($user_detail->name); ?>"  name="">

                        <p class="error-message edit_name_err"></p>
                      </div>
                     </div>
                   </div>
                  <div class="row">
                     <div class="col-md-6">
                <div class="form-group">
                  <div class="row">
                     <div class="col-md-8 text-right">
                  <label>I am a</label>
               </div>
                  <div class="col-md-4 text-right">
                   
               <span class="radio-btn"><input type="radio" name="gender" id="edit_male" class="edit_gender_class" value="1" <?php echo e(($user_detail->gender == 1)  ? 'checked="checked"' : ''); ?>> <label for="male"><img src="<?php echo e(asset('public/images/male.png')); ?>"></label></span><span class="radio-btn"><input type="radio" name="gender" id="edit_female" class="edit_gender_class" value="2" <?php echo e(($user_detail->gender == 2)  ? 'checked="checked"' : ''); ?>> <label for="female"><img src="<?php echo e(asset('public/images/female.png')); ?>"></label></span>
                  
                  <p class="error-message edit_gender_err"></p>
                  </div>
               </div>
                </div>
             </div>
                <div class="col-md-6">
                <div class="form-group">
                  <div class="row">
                  <div class="col-md-8 text-right">
                  <label>Seeking a</label>
               </div>
                  <div class="col-md-4 text-right">
               <span class="radio-btn"><input type="radio" id="s-male" name="seeking" class="edit_seeking_class" value="1" <?php echo e(($user_detail->seeking_val == 1)  ? 'checked="checked"' : ''); ?>> <label for="s-male"><img src="<?php echo e(asset('public/images/male.png')); ?>"></label></span><span class="radio-btn"><input type="radio" name="seeking" id="s-female" value="2" class="edit_seeking_class" <?php echo e(($user_detail->seeking_val == 2)  ? 'checked="checked"' : ''); ?>> <label for="s-female"><img src="<?php echo e(asset('public/images/female.png')); ?>"></label></span>
                
                <p class="error-message edit_seeking_err"></p>
                  </div>
               </div>
                </div>
             </div>
             </div><!--end of row -->
             <div class="row">
               <div class="col-md-4 text-right">
                 <div class="form-group">
                  <label for="comment">Birthday:</label>
                </div>
               </div>

               <?php 
        if(!empty($user_detail->dob))
        {   
               $edit_dob = explode('-',$user_detail->dob); 
         $month = $edit_dob[1];
         $year = $edit_dob[0];
         $day = $edit_dob[2]; 
         }
         else
         {
           $month ='';
           $year = '';
           $day = ''; 
         }
               ?>
               <div class="col-md-8">
                 <div class="row">
                   <div class="col-md-5">
                     <div class="form-group">
                        <select class="form-control" id="edit_month">
                              <option value="">Month</option>
                              <option value="1" <?php echo e(($month == 1)  ? 'selected="selected"' : ''); ?>>January</option>
                              <option value="2" <?php echo e(($month == 2)  ? 'selected="selected"' : ''); ?>>February</option>
                              <option value="3" <?php echo e(($month == 3)  ? 'selected="selected"' : ''); ?>>March</option>
                              <option value="4" <?php echo e(($month == 4)  ? 'selected="selected"' : ''); ?>>April</option>
                              <option value="5" <?php echo e(($month == 5)  ? 'selected="selected"' : ''); ?>>May</option>
                              <option value="6" <?php echo e(($month == 6)  ? 'selected="selected"' : ''); ?>>June</option>
                              <option value="7" <?php echo e(($month == 7)  ? 'selected="selected"' : ''); ?>>July</option>
                              <option value="8" <?php echo e(($month == 8)  ? 'selected="selected"' : ''); ?>>August</option>
                              <option value="9" <?php echo e(($month == 9)  ? 'selected="selected"' : ''); ?>>September</option>
                              <option value="10" <?php echo e(($month == 10)  ? 'selected="selected"' : ''); ?>>October</option>
                              <option value="11" <?php echo e(($month == 11)  ? 'selected="selected"' : ''); ?>>November</option>
                              <option value="12" <?php echo e(($month == 12)  ? 'selected="selected"' : ''); ?>>December</option>
                            </select>
                      </div>
                   </div>
                   <div class="col-md-3">
                     <div class="form-group">
                        <select class="form-control" id="edit_day">
                              <option value="" class="label" selected="selected">day</option>
                              <?php 
                                for($i=1;$i<=31;$i++)
                                {
                                  $selected_day = ($day == $i)  ? 'selected="selected"' : '';
                                  echo '<option value="'.$i.'" '.$selected_day.'>'.$i.'</option>';
                                }
                              ?>
                            </select>
                      </div>
                   </div>
                   <div class="col-md-4">
                     <div class="form-group">
                        <select class="form-control" id="edit_year">
                              <option value="" class="label" selected="selected">
                                year
                              </option>
                              <?php 
                                for($i=2001;$i>=1930;$i--)
                                {
                                  $selected_year = ($year == $i)  ? 'selected="selected"' : '';
                                  echo '<option value="'.$i.'" '.$selected_year.'>'.$i.'</option>';
                                }
                              ?>
                            </select>
                      </div>
                   </div>
                 </div>
                 <p class="error-message edit_birth_date_err"></p>
               </div>
             </div>
            
        </div>
        <div class="modal-footer md-center">
          <button type="submit" class="btn btn-default">Save</button>
        </div>
        </form>
      </div>
      
    </div>
  </div><!--End of interest modal -->


  <!--About myself modal -->
  <div class="modal fade" id="myslfModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content modal-intrst">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <div class="exmp_text text-center">
          <p>"Hello, I’m looking for a companion. Someone with a big personality but able to give me plenty of attention too. Please message me if you’ve got a good appetite, interesting conversation and the ability to laugh at yourself."</p>
        </div>
          <form id="edit_about_me_form">
            <div class="form-group">
              <textarea class="form-control" rows="5" id="edit_about_me" placeholder="some interesting details about me.."><?php echo e($user_detail->about_me); ?></textarea>
            </div>
            <div class="modal-footer md-center">
              <button type="submit" class="btn btn-default">Save</button>
            </div>
            </form>
          </div>
      </div>
      
    </div>
  </div>
<!--End of about my self me modal -->
<div id="profileImageModal" class="modal" role="dialog">
  <div class="modal-dialog" style="width: 800px">
    <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Crop & Save</h4>
          </div>
          <div class="modal-body">
            <div class="row">
            <div class="col-md-12 text-center">
              <div id="profile_image_demo" style="width:100%; margin-top:30px"></div>
            </div>
            <div style="display: none" class="text-center profile-load-more"><img width="100px" src="<?php echo e(asset('public/images/loader.gif')); ?>"><p>Uploading..</p></div>
            <div class="col-md-12 text-center">
              
              <button class="btn btn-success profile_crop_image ">Crop & Save</button>
              <input type="hidden" id="original_profile" value="">
          </div>
        </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
      </div>
    </div>
</div>


<div id="uploadimageModal" class="modal" role="dialog">
  <div class="modal-dialog" style="width: 800px">
    <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Crop & Save</h4>
          </div>
          <div class="modal-body">
            <div class="row">
            <div class="col-md-12 text-center">
              <div id="image_demo" style="width:100%; margin-top:30px"></div>
            </div>
            <div style="display: none" class="text-center load-more"><img width="100px" src="<?php echo e(asset('public/images/loader.gif')); ?>"><p>Uploading..</p></div>
            <div class="col-md-12 text-center">
              
              <button class="btn btn-success crop_image ">Crop & Save</button>
          </div>
        </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
      </div>
    </div>
</div>


<div id="uploadMyPhotoModal" class="modal" role="dialog">
  <div class="modal-dialog" style="width: 800px">
    <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Crop & Save</h4>
          </div>
          <div class="modal-body">
            <div class="row">
            <div class="col-md-12 text-center">
              <div id="image_gallery" style="width:100%; margin-top:30px"></div>
            </div>
            <div style="display: none" class="text-center load-more"><img width="100px" src="<?php echo e(asset('public/images/loader.gif')); ?>"><p>Uploading..</p></div>
            <div class="col-md-12 text-center">
              
              <input type="radio" name="is_private" value="0" checked="checked"> Public
              <input type="radio" name="is_private" value="1"> Private
          </div>
          <br>
            <div class="col-md-12 text-center">
              
              <button class="btn btn-success crop_image_gallery ">Crop & Save</button>
          </div>
        </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
      </div>
    </div>
</div>

<?php endif; ?>

<!-- Like popup Start -->
<div id="like_popup" class="modal fade" role="dialog">  
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <div class="like_popup_box">
          <div class="like_popup_img_wrap">
            <div class="like_popup_img first_img">
              <img src="<?php echo e(asset('public/images/profile_image/'.Auth::user()->profile_image)); ?>" alt="" class="img-responsive">
            </div>
            <div class="like_popup_img second_img">
            <?php if($user_detail->provider_name == 'google'): ?>
                <img id="match_img" src="<?php echo e($user_detail->profile_image); ?>" alt="" class="img-responsive">
            <?php else: ?>
                <?php if($user_detail->fake): ?>
                    <img id="match_img" src="<?php echo e(env('PROFILE_MANAGER_ADDR')); ?>/public/images/profile_image/<?php echo e($user_detail->profile_image); ?>" alt="" class="img-responsive">
                <?php else: ?>
                    <img id="match_img" src="<?php echo e(asset('public/images/profile_image/'.$user_detail->profile_image)); ?>" alt="" class="img-responsive">          
                <?php endif; ?>
            <?php endif; ?>    
            </div>
            <span class="img_icon"><i class="fa fa-heart"></i></span>
          </div>
          <div class="like_popup_text">
            <h3>it's a match!</h3>
            <p id="matched">You and <?php echo e($user_detail->name); ?> liked each other!</p>
          </div>
          <div class="like_form">
            <!--<form id="like_message" method="post">-->
                    <div id="successdiv" style="display:none; color:red;">Your Message has been sent successfully.</div>
                    <div id="faileddiv" style="display:none; color:red;">You haven't enough credit to send this message. Please purchase credits.</div>    
              <div class="form_group">
                <textarea name="message_p" id="message_p" placeholder="write a message"></textarea>
                        <input type="hidden" name="receiver_id" id="matched_receiver_id" value="<?php echo e($user_detail->user_id); ?>">
                        <input type="hidden" name="receiver_name" id="matched_receiver_name" value="<?php echo e($user_detail->name); ?>">
                        <input type="hidden" name="receiver_image" id="matched_receiver_image" value="<?php echo e($user_detail->profile_image); ?>">
              </div>
              <div class="form_group">
                <button class="lk_send_btn" onclick="send_message();">Send</button>
              </div>
            <!--</form>-->
            
          </div>
        </div>
     </div>
    </div>
  </div>
</div>
<!-- Like popup End -->

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">New message</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form>
          
      <div class="form-group">
             <img id="selectedImage" src="" alt="Smiley face" height="500" width="600"> 
          </div>
      
          <div class="form-group">
            <label for="message-text" class="col-form-label">Message:</label>
            <input style="border: 1px solid #cccccc;" type="text" class="form-control" id="message-text">
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onclick="sendImage()" data-dismiss="modal">Send message</button>
      </div>
    </div>
  </div>
</div>

<!-- chat notification -->
  <div class="chat_notifi_box_wrap">
    <span class="close_box">
      <i class="fa fa-times"></i>
    </span>
    <div class="chat_notifi_box">
      <div class="img_wrapper">
        <img src="<?php echo e(asset('public/images/lady_icon.png')); ?>" class="noti_image" alt="profile_img">
      </div>
      <div class="ct_info_wrapper">
        <div class="title_head">
          <h4 id="noti_name">MarinaBeauty, <span class="age">27</span></h4>
          <span class="active_status online">
            <i class="fa fa-circle"></i>
          </span>
        </div>
        <p>is online ! You may get lucky with her! </p>
        <a href="#" class="noti_chat_btn"><i class="fa fa-commenting-o"></i>Chat</a>
      </div>
    </div>
  </div>
  <!-- chat notification -->

<input type="hidden"  name="sender_id" id="sender_id" value="<?php echo e(isset($users->id) ? $users->id : ''); ?>">
<input type="hidden"  name="sender_name" id="sender_name" value="<?php echo e(isset($users->name) ? $users->name : ''); ?>">
<input type="hidden"  name="sender_image" id="sender_image" value="<?php echo e(isset($users->profile_image) ? $users->profile_image : ''); ?>">

<input type="hidden"  name="receiver_id" id="receiver_id" value="<?php echo e(($user_detail) ? $user_detail->user_id : ''); ?>">
<input type="hidden"  name="receiver_name" id="receiver_name" value="">
<input type="hidden"  name="receiver_image" id="receiver_image" value="">
<input type="hidden"  name="room_id" id="room_id" value="">
<input type="hidden"  name="chat_image" id="chat_image" value="">

<input type="hidden"  name="chat_send_image" id="chat_send_image" value="">

<script>
     var adminId = $('#sender_id').val();
     
     // send message from profile      
  function sendMsg1(proposal=null){
            //alert(proposal);
            var mesageType = 1;
            var mycredits = jQuery("#initialcredit").val();//localStorage.getItem("credits");
            var mypremiums = jQuery("#initialpremium").val();
            if(mycredits == 'null'){ mycredits = 0;}
            var requiredCredits = 0;
            
            var sender_is_fake = '<?php echo e($users->fake); ?>';
            
            // for text required credit will be 1
            var type = (jQuery("#file").val() == '') ? 'text' : 'file';
            if(type === "text"){
                requiredCredits = 1;
            }
            
            var planType = jQuery("#planType").val();
            if(planType === 'credits'){ requiredCredits = jQuery("#points").val(); }
            
            if(planType === 'diamonds'){
                    requiredCredits = 1;//jQuery("#points").val();
            }
            
            //chatNowtext chatNowuserId
            if(proposal){
                var msgText1 = '<img src="'+proposal+'" width="100">';
                var planType = 'diamonds';
                if(proposal == 'likes'){
                    planType = '';
                    requiredCredits = 0;
                    mesageType = 3;
                }
                if(proposal == 'favorites'){
                    planType = '';
                    requiredCredits = 0;
                    mesageType = 4;
                }
            }else{
                
                var msgText1 = ($('#chatNowtext1').val()) ? $('#chatNowtext1').val() : $('#chatNowtext').val() ; 
            }
            //alert(msgText1);
                if(msgText1 == ''){
                    return true;
                }       
    
        var chat_send_image = $('#chat_send_image').val();
        var messageType = 'text';
        if(chat_send_image != ''){
            messageType = 'text';
           // CKEDITOR.instances.message.setData(chat_send_image);
        }
    
        var userId    = $('#receiver_id').val();
        var userName  = $('#receiver_name').val();
        var userImage = $('#receiver_image').val();
        
        if(proposal){
            var msgText = '<img src="'+proposal+'">';
            if(proposal === 'likes'){                
                var msgText = 'Hi '+userName+', i like your profile. Check mine and see if we are a match! Lets connect';
                messageType = 'likes';
            }
            if(proposal === 'favorites'){            
                var msgText = 'Hi '+userName+' I have added you to my favorite list. I hope you like my profile to! Lets get this rolling';
                messageType = 'favorite';
            }
        }else{
            var msgText = ($('#chatNowtext1').val()) ? $('#chatNowtext1').val() : $('#chatNowtext').val() ;
            requiredCredits = 1;
        }        
        //alert(msgText);
       
    
        $('#chatNowtext1').val('');
        
        
        
        var room_id = $('#room_id').val();
        if(room_id != ''){
            firebase.database().ref('chat_rooms/' + room_id).off("child_added");
        }
        
        var uid = $('#receiver_id').val();
        var chatRoom = adminId + '_' + uid;
        if(uid < adminId){
          chatRoom = uid + '_' + adminId;
        }
        
        
        firebase.database().ref('chat_rooms/' + chatRoom).off("child_added");
        
        $('#room_id').val(chatRoom);
        
        
        ////////////*******************888
        //alert(planType);
        
            if(planType === 'diamonds'){
            var diamnd = jQuery("#updateddiamonds").html();
            //console.log(diamnd);
            if(diamnd > 0){
                mycredits = diamnd;   
            }
            requiredCredits = 1;
                $.ajax({
                    headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '<?php echo url("/")?>/update_diamonds',
                    type: 'POST',
                    cache : false,
                    processData: false,
                    data: 'room_id='+jQuery('#room_id').val()+'&amt=1&receiver_id='+userId, // serializes the form's elements.
                    success: function(data)
                    {
                     //console.log(data);
                     if(data){
                        var d = JSON.parse(data);
                        if(d.credits){
                            jQuery("#initialcredit").val(d.credits);
                            jQuery("#updatedwallet").html(d.credits);
                            jQuery("#updateddiamonds").html(d.diamonds);   
                        }else{                            
                            jQuery("#updateddiamonds").html(d.diamonds);   
                        }                        
                      }                      
                    }
                });
            }else{
                $.ajax({
                    headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '<?php echo url("/")?>/update_conversation',
                    type: 'POST',
                    cache : false,
                    processData: false,
                    data: 'room_id='+jQuery('#room_id').val()+'&amt='+requiredCredits+'&receiver_id='+userId+'&mesageType='+mesageType+'&msg='+msgText, // serializes the form's elements.
                    success: function(data)
                    {
                     //console.log(data);
                      if(data){
                        jQuery("#initialcredit").val(data);
                        jQuery("#updatedwallet").html(data);
                      }
                    }
                });    
            }
        //*******************************
    
        var chatRoom = $('#room_id').val();
        if(requiredCredits){
            //$('.alreadyChat').attr('disabled',true);   
        }        
        firebase.database().ref('/users/'+userId).once('value').then(function(snapshot) {
          var childData = snapshot.val();
          
          if(childData.fake == '1'){
            if(parseInt(mycredits) >= parseInt(requiredCredits) || (sender_is_fake == 1) ){
                if(msgText.substring(0, 11) === '<p><img src'){
                    checkdata(adminId,userId,'image',msgText);
                }else{
                    checkdata(adminId,userId,msgText,'');   
                }
            }
          }
          
          firebase.database().ref().child('chat_history/'+adminId).child(userId).once('value').then(function(snapshot1) {
    
            var snapshot1Data = snapshot1.val();
    
            var msgCount = 0;
            var hisType = 1;
            if (snapshot1Data != null){
              var msgCount = Number(snapshot1Data.messageCount)+1;
              if(snapshot1Data.historyType == 1){
                if(snapshot1Data.lastSenderId != adminId){
                  hisType = 2;
                }
              }else{
                hisType = 2;
              }
            }
    
            var insertData = {
              deletteby : '',
              message : msgText,
              messageCount : msgCount,
              name : userName,
              profilePic : userImage,
              timestamp : $.now(),
              uid : userId.toString(),
              historyType : hisType,
              lastSenderId : adminId.toString(),
              messageType : messageType,
              historyLoad : '0'
            };
          if(parseInt(mycredits) >= parseInt(requiredCredits)  || (sender_is_fake == 1) ){
            firebase.database().ref().child('chat_history/'+adminId).child(userId).set(insertData);
          }
          });
        });
    
        firebase.database().ref('/users/'+adminId).once('value').then(function(snapshot) {
          var childData = snapshot.val();
    
          firebase.database().ref().child('chat_history/'+userId).child(adminId).once('value').then(function(snapshot2) {
    
            var snapshot2Data = snapshot2.val();
    
            var msgCount = 0;
            var hisType = 1;
            if (snapshot2Data != null){
              var msgCount = Number(snapshot2Data.messageCount)+1;
              if(snapshot2Data.historyType == 1){
                if(snapshot2Data.lastSenderId != adminId){
                  hisType = 2;
                }
              }else{
                hisType = 2;
              }
            }
    
            var insertData = {
              deletteby : '',
              message : msgText,
              messageCount : msgCount,
              name : childData.userName,
              profilePic : childData.userImage,
              timestamp : $.now(),
              uid : adminId.toString(),
              historyType : hisType,
              lastSenderId : adminId.toString(),
              messageType : messageType,
              historyLoad : '0'
            };
        if(parseInt(mycredits) >= parseInt(requiredCredits) || (sender_is_fake == 1) ){
              firebase.database().ref().child('chat_history/'+userId).child(adminId).set(insertData);
        }
          });
        });
    
        firebase.database().ref('/users/'+adminId).once('value').then(function(snapshot) {  
          var childData = snapshot.val();
          var insertData = {
            deleteby:'',
            message : msgText,
            messageCount: 1,
            name : childData.userName,
            profilePic : childData.userImage,
            timestamp : $.now(),
            uid : adminId.toString(),
            messageType : messageType,
          };
          //firebase.database().ref().child('chat_rooms/').child(chatRoom).push(insertData);
          if(parseInt(mycredits) >= parseInt(requiredCredits) || (sender_is_fake == 1) ){
      firebase.database().ref().child('chat_rooms/').child(chatRoom).push(insertData);
                if(proposal){
                    if(proposal != 'likes' && proposal != 'favorites') {
                     $('#proposal_message').show();
                     $(".send_proposal_wrap").slideUp(300);
                    }
               }else{
                     $('#chat_message').show();
               }
               $('#mypriphoto').modal('hide');
      }else{
      //message failed
            $('#messages').append(''+ 
                          '<li class="message">'+                    
                              '<div class="chat-user-right">'+
                              '<div class="chat-user-box">'+
                              '<img src="<?php echo e(asset('/public/thumbnail/')); ?>/'+jQuery('#sender_image').val()+'">'+
                              '<span>'+jQuery('#sender_name').val()+'</span>'+
                              '</div>'+
                              '<div class="chat-user-message">'+
                              '<div class="chat-line"><span>'+msgText+'</span></div>'+                                   
                              '<div class="meta-info"><span class="date">'+$.now()+'</span></div>'+
                  '<div class="meta-info red"><span class="date">Message Sent Failed!!</span></div>'+
                              '</div>'+
                              '</div>'+
                          '</li>'+
                      '');
            //setTimeout(function(){ jQuery('.credit_btn').click(); }, 3000); 
            $('#msgDiv').animate({
                  scrollTop: $('#messages').prop("scrollHeight")
              }, 0);
            setTimeout(function(){ window.location.replace("{{ url('/credits') }}"); }, 500); 
      }
          
                    
        });
      }
      
      
      
      function sendMsg2(proposal=null){                
            
            var mycredits = jQuery("#initialcredit").val();//localStorage.getItem("credits");
            var mypremiums = jQuery("#initialpremium").val();
            if(mycredits == 'null'){ mycredits = 0;}
            var requiredCredits = 0;
            
            var sender_is_fake = '<?php echo e($users->fake); ?>';
            
            // for text required credit will be 1
            var type = (jQuery("#file").val() == '') ? 'text' : 'file';
            if(type === "text"){
                requiredCredits = 1;
            }
            
            var planType = jQuery("#planType").val();
            if(planType === 'credits'){ requiredCredits = jQuery("#points").val(); }
            
            if(planType === 'diamonds'){
                    requiredCredits = 1;//jQuery("#points").val();
            }
            
            //chatNowtext chatNowuserId
            if(proposal){
                var msgText1 = '<img src="'+proposal+'" width="100">';
                var planType = 'diamonds';
                if(proposal == 'likes'){
                    planType = '';
                }
                if(proposal == 'favorites'){
                    planType = '';
                }
            }else{
                var msgText1 = $.trim($('#message_p').val());
                
            }
            //alert(msgText1);
                if(msgText1 == ''){
                    return true;
                }

            
            
            
    
        
    
    
    
        var chat_send_image = $('#chat_send_image').val();
        var messageType = 'text';
        if(chat_send_image != ''){
            messageType = 'text';
           // CKEDITOR.instances.message.setData(chat_send_image);
        }
    
        var userId    = $('#matched_receiver_id').val();
        var userName  = $('#matched_receiver_name').val();
        var userImage = $('#matched_receiver_image').val();
        
        if(proposal){
            var msgText = '<img src="'+proposal+'">';
            if(proposal === 'likes'){                
                var msgText = 'Hi '+userName+', i like your profile. Check mine and see if we are a match! Lets connect';
            }
            if(proposal === 'favorites'){            
                var msgText = 'Hi '+userName+' I have added you to my favorite list. I hope you like my profile to! Lets get this rolling';
            }
            var free_message = 0;
        }else{
            var msgText = $('#message_p').val();
            requiredCredits = 1;                        
            //alert(msgText);
            if($('#free_message').val() > 0) { 
                requiredCredits = 0;
                var free_message = 1;
            }else{
                var free_message = 0;
            }
        }
        
        
        //alert(msgText);
       
        
        $('#message_p').val('');
        
        
        
        var room_id = $('#room_id').val();
        if(room_id != ''){
            firebase.database().ref('chat_rooms/' + room_id).off("child_added");
        }
        
        var uid = userId; //$('#chatNowuserId').val();
        var chatRoom = adminId + '_' + uid;
        if(uid < adminId){
          chatRoom = uid + '_' + adminId;
        }
        
        
        firebase.database().ref('chat_rooms/' + chatRoom).off("child_added");
        
        $('#room_id').val(chatRoom);
        
        
        ////////////*******************888
         //if(planType === 'diamonds'){
            //requiredCredits = 1;
            //    $.ajax({
            //        headers: {
            //          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            //        },
            //        url: '<?php echo url("/")?>/update_diamonds',
            //        type: 'POST',
            //        cache : false,
            //        processData: false,
            //        data: 'room_id='+jQuery('#room_id').val()+'&amt=1&receiver_id='+userId, // serializes the form's elements.
            //        success: function(data)
            //        {
            //         console.log(data);
            //         if(data){
            //            var d = JSON.parse(data);
            //            if(d.credits){
            //                jQuery("#initialcredit").val(d.credits);
            //                jQuery("#updatedwallet").html(d.credits);
            //                jQuery("#updateddiamonds").html(d.diamonds);   
            //            }else{                            
            //                jQuery("#updateddiamonds").html(d.diamonds);   
            //            }                        
            //          }                      
            //        }
            //    });
            //}else{
                $.ajax({
                    headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '<?php echo url("/")?>/update_conversation',
                    type: 'POST',
                    cache : false,
                    processData: false,
                    data: 'room_id='+jQuery('#room_id').val()+'&amt='+requiredCredits+'&receiver_id='+userId+'&free_message='+free_message, // serializes the form's elements.
                    success: function(data)
                    {
                     console.log(data);
                      if(data){
                        jQuery("#initialcredit").val(data);
                        jQuery("#updatedwallet").html(data);
                      }
                      if(free_message){
                        $('#free_message').val(0);
                      }
                    }
                });    
          //  }
        //*******************************
    
        var chatRoom = $('#room_id').val();
        if(requiredCredits){
            //$('.alreadyChat').attr('disabled',true);
        }
        firebase.database().ref('/users/'+userId).once('value').then(function(snapshot) {
          var childData = snapshot.val();
          
          firebase.database().ref().child('chat_history/'+adminId).child(userId).once('value').then(function(snapshot1) {
    
            var snapshot1Data = snapshot1.val();
    
            var msgCount = 0;
            var hisType = 1;
            if (snapshot1Data != null){
              var msgCount = Number(snapshot1Data.messageCount)+1;
              if(snapshot1Data.historyType == 1){
                if(snapshot1Data.lastSenderId != adminId){
                  hisType = 2;
                }
              }else{
                hisType = 2;
              }
            }
    
            var insertData = {
              deletteby : '',
              message : msgText,
              messageCount : msgCount,
              name : userName,
              profilePic : userImage,
              timestamp : $.now(),
              uid : userId.toString(),
              historyType : hisType,
              lastSenderId : adminId.toString(),
              messageType : messageType,
              historyLoad : '0'
            };
          if(parseInt(mycredits) >= parseInt(requiredCredits) || (sender_is_fake == 1) ){
            firebase.database().ref().child('chat_history/'+adminId).child(userId).set(insertData);
          }
          });
        });
    
        firebase.database().ref('/users/'+adminId).once('value').then(function(snapshot) {
          var childData = snapshot.val();
    
          firebase.database().ref().child('chat_history/'+userId).child(adminId).once('value').then(function(snapshot2) {
    
            var snapshot2Data = snapshot2.val();
    
            var msgCount = 0;
            var hisType = 1;
            if (snapshot2Data != null){
              var msgCount = Number(snapshot2Data.messageCount)+1;
              if(snapshot2Data.historyType == 1){
                if(snapshot2Data.lastSenderId != adminId){
                  hisType = 2;
                }
              }else{
                hisType = 2;
              }
            }
    
            var insertData = {
              deletteby : '',
              message : msgText,
              messageCount : msgCount,
              name : childData.userName,
              profilePic : childData.userImage,
              timestamp : $.now(),
              uid : adminId.toString(),
              historyType : hisType,
              lastSenderId : adminId.toString(),
              messageType : messageType,
              historyLoad : '0'
            };
        if(parseInt(mycredits) >= parseInt(requiredCredits) || (sender_is_fake == 1) ){
              firebase.database().ref().child('chat_history/'+userId).child(adminId).set(insertData);
        }
          });
        });
    
        firebase.database().ref('/users/'+adminId).once('value').then(function(snapshot) {  
          var childData = snapshot.val();
          var insertData = {
            deleteby:'',
            message : msgText,
            messageCount: 1,
            name : childData.userName,
            profilePic : childData.userImage,
            timestamp : $.now(),
            uid : adminId.toString(),
            messageType : messageType,
          };
          //firebase.database().ref().child('chat_rooms/').child(chatRoom).push(insertData);
          if(parseInt(mycredits) >= parseInt(requiredCredits) || (sender_is_fake == 1) ){
      firebase.database().ref().child('chat_rooms/').child(chatRoom).push(insertData);
                if(proposal){
                    if(proposal != 'likes' && proposal != 'favorites') {
                     $('#proposal_message').show();
                    }
               }else{
                     $('#chat_message').show();
               }
               $('#mypriphoto').modal('hide');
               if(requiredCredits || free_message){
                    $('#successdiv').show();
               }               
      }else{
            $('#faileddiv').show();
      //message failed
            $('#messages').append(''+ 
                          '<li class="message">'+                    
                              '<div class="chat-user-right">'+
                              '<div class="chat-user-box">'+
                              '<img src="<?php echo e(asset('/public/thumbnail/')); ?>/'+jQuery('#sender_image').val()+'">'+
                              '<span>'+jQuery('#sender_name').val()+'</span>'+
                              '</div>'+
                              '<div class="chat-user-message">'+
                              '<div class="chat-line"><span>'+msgText+'</span></div>'+                                   
                              '<div class="meta-info"><span class="date">'+$.now()+'</span></div>'+
                  '<div class="meta-info red"><span class="date">Message Sent Failed!!</span></div>'+
                              '</div>'+
                              '</div>'+
                          '</li>'+
                      '');
            //setTimeout(function(){ jQuery('.credit_btn').click(); }, 3000); 
            $('#msgDiv').animate({
                  scrollTop: $('#messages').prop("scrollHeight")
              }, 0);
            setTimeout(function(){ window.location.replace("{{ url('/credits') }}"); }, 500); 
      }
          
                    
        });
               
      }
</script>


<script>
$("#profile_popup").removeClass("user_profile_popup");
    // function set_receiver(id, name, image,fake=0){
    //     //alert(name);
    //     //gethistory();
    //     $('#receiver_id').val(id);
    //     $('#receiver_name').val(name);
    //     $('#chat_name'+id).text(name);
    //     if(fake == 1){
    //         $('#chat_image'+id).attr('src','<?php echo e(env('PROFILE_MANAGER_ADDR').('/public/thumbnail/')); ?>/'+image);
    //         var rcvrimgurl = '<?php echo e(env('PROFILE_MANAGER_ADDR').('/public/thumbnail/')); ?>/';
    //     }else{
    //         $('#chat_image'+id).attr('src','<?php echo e(asset('/public/thumbnail/')); ?>/'+image);
    //         var rcvrimgurl = '<?php echo e(env('PROFILE_MANAGER_ADDR').('/public/thumbnail/')); ?>/';
    //     }       
    //     $('#receiver_image').val(image);
        
    //     var sender_id = '<?php echo e(isset($users->id) ? $users->id : ''); ?>';
    //     var receiver_id = id;

    //     $.ajax({
    //         headers: {
    //           'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    //         },
    //         url: '<?php echo url("/");?>/fake_chat',
    //         type: 'POST',
    //         data: 'receiver_id='+receiver_id,
    //         cache : false,
    //         dataType: 'html',
    //         async: false,
    //         processData: false,
    //         success: function(data)
    //         {
    //           $('#fake_chat').html(data);
    //           $('.user_chat_popup').hide();
    //           $('.chat__messages').html('');                    
    //           $('#myScrollchat'+receiver_id).show();
    //           $("#profile_popup").addClass("user_profile_popup");

    //           $('#receiver_id').val(id);
    //           $('#receiver_name').val(name);
    //           $('#chat_name'+id).text(name);
    //           if(fake == 1){
    //                 $('#chat_image'+id).attr('src','<?php echo e(env('PROFILE_MANAGER_ADDR').('/public/thumbnail/')); ?>/'+image);        
    //           }else{
    //                 $('#chat_image'+id).attr('src','<?php echo e(asset('/public/thumbnail/')); ?>/'+image);            
    //           }        
    //           $('#receiver_image').val(image);

              
    //         }
    //       });            
                 
        
    //     createChatRoom();
    //     return true;        
        
    // }

  $(document).ready(function() {
    // Configure/customize these variables.
    var showChar = 260;  // How many characters are shown by default
    var ellipsestext = "...";
    var moretext = "Show more";
    var lesstext = "Show less";
    

    $('.more').each(function() {
      var content = $(this).html();

      if(content.length > showChar) {

        var c = content.substr(0, showChar);
        var h = content.substr(showChar, content.length - showChar);

        var html = c + '<span class="moreellipses">' + ellipsestext+ '&nbsp;</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a class="morelink cont-read">' + moretext + '</a></span>';

        $(this).html(html);
      }

    });

    $(".morelink").click(function(){
      if($(this).hasClass("less")) {
        $(this).removeClass("less");
        $(this).html(moretext);
      } else {
        $(this).addClass("less");
        $(this).html(lesstext);
      }
      $(this).parent().prev().toggle();
      $(this).prev().toggle();
      return false;
    });
  });
</script>

<?php if($self_user): ?>
<script>
$('form#edit_profile_form').on('submit',function(e){
  e.preventDefault();
    var err=0;
    var user_id = $('#user_id').val();
    var name = $('#edit_name').val().trim();
    var gender = $("input:radio.edit_gender_class:checked").val();
    var seeking = $("input:radio.edit_seeking_class:checked").val();
    var month = $('#edit_month').val();
    var day = $('#edit_day').val();
    var year = $('#edit_year').val();
    var address = $('#same_address').val();
    var city = $('#same_city').val();
    var country = $('#same_country').val();
    
    if(name == '')
    {
      $('.edit_name_err').show();
      $('.edit_name_err').text('Enter your name or nickname');
      err++;
    }
    else{
      $('.edit_name_err').hide();
      $('.edit_name_err').text('');
    }

    if(gender == '' || typeof gender === "undefined")
    {
      $('.edit_gender_err').show();
      $('.edit_gender_err').text('Select gender');
      err++;
    }
    else{
      $('.edit_gender_err').hide();
      $('.edit_gender_err').text('');
    }

    if(seeking == '' || typeof seeking === "undefined")
    {
      $('.edit_seeking_err').show();
      $('.edit_seeking_err').text('Select seeking');
      err++;
    }
    else{
      $('.edit_seeking_err').hide();
      $('.edit_seeking_err').text('');
    }

    if(year == '')
    {
      $('.edit_birth_date_err').show();
      $('.edit_birth_date_err').text('Select your date of birth');
      err++;
    }
    

    if(month == '')
    {
      $('.edit_birth_date_err').show();
      $('.edit_birth_date_err').text('Select your date of birth');
      err++;
    }
    
    
    if(day == '')
    {
      $('.edit_birth_date_err').show();
      $('.edit_birth_date_err').text('Select your date of birth');
      err++;
    }
    

    if(day != '' && month != '' && year != '')
    {
      $('.edit_birth_date_err').hide();
      $('.edit_birth_date_err').text('');
    }

    if(err == 0)
    {
      $.ajax({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: '<?php echo url("/")?>/user-profile-update',
        type: 'POST',
        cache : false,
        processData: false,
        data: 'name='+name+'&gender='+gender+'&seeking='+seeking+'&day='+day+'&month='+month+'&year='+year+'&address='+address+'&city='+city+'&country='+country+'&user_id='+user_id, // serializes the form's elements.
        success: function(data)
        {
         if(data == 'success')
         {
          $('#infoModal').modal('hide');
          location.reload();
         }
         else{
          alert('Something went wrong.');
          //location.reload();
         }
        }
      });
    }
    else{
      return false;
    } 
});

$('form#edit_about_me_form').on('submit',function(e){
    e.preventDefault();
    
    var about_me = $('#edit_about_me').val().trim();
    var user_id = $('#user_id').val();
    $.ajax({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: '<?php echo url("/")?>/about-me-update',
        type: 'POST',
        cache : false,
        processData: false,
        data: 'about_me='+about_me+'&user_id='+user_id, // serializes the form's elements.
        success: function(data)
        {
         if(data == 'success')
         {
          $('#myslfModal').modal('hide');
          location.reload();
         }
         else{
          alert('Something went wrong.');
          //location.reload();
         }
        }
    });
}); 


$('.gallery_delete').on('click',function(e){
    e.preventDefault();
    
    if (confirm("Are you sure?")) {
        // your deletion code
        var image_id = $(this).attr('id');
    
        $.ajax({
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '<?php echo url("/")?>/gallery-image-delete',
            type: 'POST',
            data: 'image_id='+image_id, // serializes the form's elements.
            success: function(data)
            {
             if(data)
             {
              //$('#myslfModal').modal('hide');
              location.reload();
             }
             else{
              alert('Something went wrong.');
              //location.reload();
             }
            }
        });
    }
    return false;
});



$('form#edit_looking_for_form').on('submit',function(e){
    e.preventDefault();
    
    var seeking_val = $('#edit_seeking_val').val();
    var min_age = $('#edit_min_age').val();
    var max_age = $('#edit_max_age').val();
    var about_partner = $('#edit_partner').val();
    var user_id = $('#user_id').val();
    $.ajax({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: '<?php echo url("/")?>/looking-for-update',
        type: 'POST',
        cache : false,
        processData: false,
        data: 'seeking_val='+seeking_val+'&min_age='+min_age+'&max_age='+max_age+'&about_partner='+about_partner+'&user_id='+user_id, // serializes the form's elements.
        success: function(data)
        {
         if(data == 'success')
         {
          $('#lkgModal').modal('hide');
          location.reload();
         }
         else{
          alert('Something went wrong.');
          //location.reload();
         }
        }
    });
}); 

$('form#edit_interests_form').on('submit',function(e){
    e.preventDefault();
    
    var arr=[];
    
    $('input:checked[name=edit_interests').each(function(){
        arr.push($(this).val());
    });
             
    var interests = arr.join(',');
    var user_id = $('#user_id').val();
    $.ajax({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: '<?php echo url("/")?>/interests-update',
        type: 'POST',
        cache : false,
        processData: false,
        data: 'interests='+interests+'&user_id='+user_id, // serializes the form's elements.
        success: function(data)
        {
         if(data == 'success')
         {
          $('#interestModal').modal('hide');
          location.reload();
         }
         else{
          alert('Something went wrong.');
          //location.reload();
         }
        }
    });
});

$('form#edit_about_detail_form').on('submit',function(e){
    e.preventDefault();
    
    var address = $('#edit_address').val();
    
    if(address == "")
    {
      $('#edit_city').val('');
      $('#edit_country').val(''); 
    }

    var city = $('#edit_city').val();
    var country = $('#edit_country').val();
    var lang = $('#edit_lang').val();
    var work_as = $('#edit_work_as').val();
    var education = $('#edit_education').val();
    var relationship = $('#edit_relationship').val();
    var kids = $('#edit_kids').val();
    var smoke = $('#edit_smoke').val();
    var drink = $('#edit_drink').val();
    var height = $('#edit_height option:selected').text();

    if(lang == null)
    {
      lang = '';
    }
    
    var user_id = $('#user_id').val();
    $.ajax({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: '<?php echo url("/")?>/about-detail-update',
        type: 'POST',
        cache : false,
        processData: false,
        data: 'work_as='+work_as+'&lang='+lang+'&address='+address+'&city='+city+'&country='+country+'&education='+education+'&relationship='+relationship+'&kids='+kids+'&smoke='+smoke+'&drink='+drink+'&height='+height+'&user_id='+user_id, // serializes the form's elements.
        success: function(data)
        {
         if(data == 'success')
         {
          $('#aboutModal').modal('hide');
          location.reload();
         }
         else{
          alert('Something went wrong.');
          //location.reload();
         }
        }
    });
});

$('.lang-selectpicker').selectpicker();
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=<?php echo e(env('GOOGLE_KEY')); ?>&libraries=places" type="text/javascript"></script>    
<script type="text/javascript">
   function edit_profile() {
      var input = document.getElementById('edit_address');

      var componentForm = {
        street_number: 'short_name',
        route: 'long_name',
        locality: 'long_name',
        administrative_area_level_1: 'short_name',
        country: 'long_name',
        postal_code: 'short_name'
      };

      var options = {
        types: ['geocode'], //this should work !
        //componentRestrictions: {country: "pk"}
      };
      var autocomplete = new google.maps.places.Autocomplete(input, options);

        google.maps.event.addListener(autocomplete, 'place_changed', function() {
            var place = autocomplete.getPlace();
    
    
            //alert("'"+place.formatted_address+"'");
            //document.getElementById('prof_address').value= place.formatted_address;

            var add_arr = [];
            //code for remove other locality
            for (var i = 0; i < place.address_components.length; i++) {
              var addressType = place.address_components[i].types[0];
              
              
              add_arr.push(addressType);
              //console.log(add_arr);
              if (componentForm[addressType]) { 
                var val = place.address_components[i][componentForm[addressType]];
                
                if(add_arr.indexOf('locality') <= -1)
                {
                  document.getElementById('edit_city').value= '';
                }
                if(addressType == 'locality'){
                  //alert(addressType +'==='+val);
                  document.getElementById('edit_city').value= val;
                }
                if(addressType == 'country'){
                //alert(addressType +'==='+val);
                  document.getElementById('edit_country').value= val;
                }
              }
            }
            //code for remove other locality
        });
      
   }
   google.maps.event.addDomListener(window, 'load', edit_profile);
</script>
<?php endif; ?>
    
<script type="text/javascript">
$(document).ready(function(){
  $(document).on('click','.profile-like',function(e){
    e.preventDefault();
    sendMsg1('favorites');
    var receiverId = $(this).attr('data-reciever');
    $('.load-like').text('load..');
  $('.like-main').html('<div class="edit-link profile-liked "><label><a href="javascript:void(0)"> <i class="fa fa-star"></i> Favorited</a></label></div>');
    $.ajax({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: '<?php echo url("/")?>/profile-like',
        type: 'POST',
        cache : false,
        processData: false,
        data: 'receiverId='+receiverId, // serializes the form's elements.
        success: function(data)
        {
         $('.load-like').text(''); 
         if(data == 'success')
         {
          //alert('success');
          //$('#aboutModal').modal('hide');
          $('.like-main').html('<div class="edit-link profile-liked "><label><a href="javascript:void(0)"> <i class="fa fa-star"></i> Favorited</a></label></div>');
         }
         if(data == 'exist')
         {
          //alert('exist.');
          //$('.like-main').html('<div class="edit-link profile-liked col-md-6"><label><a href="javascript:void(0)"> <i class="fa fa-star"></i> Favorites</a></label></div>');
          $('.like-main').html('<div class="edit-link profile-liked "><label><a href="javascript:void(0)"> <i class="fa fa-star"></i> Favorited</a></label></div>');
         }
        }
    });
  });
  
  //Like
  $(document).on('click','.profile-liking',function(e){
    e.preventDefault();
    sendMsg1('likes');
    var receiverId = $(this).attr('data-like');
    $('.load-like').text('load..');
  $('.like-main1').html('<div class="edit-link profile-liked"><label><a href="javascript:void(0)"> <i class="fa fa-heart"></i> Liked</a></label></div>');
    $.ajax({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: '<?php echo url("/")?>/profile-liking',
        type: 'POST',
        cache : false,
        processData: false,
        data: 'receiverId='+receiverId, // serializes the form's elements.
        success: function(data)
        {
       $('.load-like').text(''); 
       if(data == '0')
       {
        $('.like-main1').html('<div class="edit-link profile-liked "><label><a href="javascript:void(0)"> <i class="fa fa-heart"></i> Liked</a></label></div>');
        //$('#like_popup').modal('show');
       }
       if(data == '1')
       {
        $('.like-main1').html('<div class="edit-link profile-liked "><label><a href="javascript:void(0)"> <i class="fa fa-heart"></i> Liked</a></label></div>');
        $('#like_popup').modal('show');
       }
       //if(data == 'exist')
       //{
       // $('.like-main1').html('<div class="edit-link profile-liked col-md-6"><label><a href="javascript:void(0)"> <i class="fa fa-heart"></i> Favorites</a></label></div>');
       //}
        }
    });
  });
  
  
  
  
});
</script>  
    
    
<script>
    /*$(document).on('change','#edit_profileptc',function () {
        if ($(this).val() != '') {
            upload(this);

        }
    });
    function upload(img) {
        var form_data = new FormData();
        form_data.append('profile_img', img.files[0]);
        form_data.append('_token', '<?php echo e(csrf_token()); ?>');
        form_data.append("user_id",$("#user_id").val());
        //$('#loading').css('display', 'block');
        $.ajax({
            url: "<?php echo e(url('ajax-image-upload')); ?>",
            data: form_data,
            type: 'POST',
            contentType: false,
            processData: false,
            success: function (data) {
                if (data.fail) {
                    //$('#preview_image').attr('src', '<?php echo e(asset('images/noimage.jpg')); ?>');
                    alert(data.errors['profile_img']);
                }
                else {
                    $('.profile_img').css('background-image', 'url(' + '<?php echo e(asset('public/thumbnail')); ?>/' + data + ')');
                    //$('#file_name').val(data);
                    //$('#preview_image').attr('src', '<?php echo e(asset('uploads')); ?>/' + data);
                }
                //$('#loading').css('display', 'none');
            },
            error: function (xhr, status, error) {
                alert(xhr.responseText);
                //$('#preview_image').attr('src', '<?php echo e(asset('images/noimage.jpg')); ?>');
            }
        });
    }*/
    
</script>
<textarea id="chat_file_hidden" style="display:none"></textarea>    
<script>  
$(document).ready(function(){

$.ajaxSetup({
  //headers: {
  //    'X-CSRF-TOKEN': '<?php echo e(csrf_token()); ?>'
  //}
});
  
  //for profile photo
  $profile_crop = $('#profile_image_demo').croppie({
    enableExif: true,
    viewport: {
      width:200,
      height:200,
      type:'circle' //square
    },
    boundary:{
      width:465,
      height:300
    }
  });
  

  $('#edit_profileptc').on('change', function(){
    var reader = new FileReader();
    reader.onload = function (event) {
      $profile_crop.croppie('bind', {
        url: event.target.result
      }).then(function(){
        $('#original_profile').val(event.target.result);
        //console.log(event.target.result);
      });
    }
    reader.readAsDataURL(this.files[0]);
    $('#profileImageModal').modal('show');
  });

  $('.profile_crop_image').click(function(event){
    var original_profile  = $('#original_profile').val();
    $profile_crop.croppie('result', {
      type: 'canvas',
      size: 'viewport'
    }).then(function(response){
        $('.profile-load-more').show();
      $.ajax({
        headers: {
              'X-CSRF-TOKEN': '<?php echo e(csrf_token()); ?>'
        },
        url: "<?php echo e(url('ajax-image-upload')); ?>",
        type: "POST",
        data:{"profile_img": response,'user_id': $("#user_id").val(),"original_profile":original_profile},
        success:function(data)
        {
          if (data.success) {
              $('.profile-load-more').hide();
              $('.profile_img').css('background-image', 'url(' + '<?php echo e(asset('public/images/profile_image')); ?>/' + data.img + ')');
              $('#profileImageModal').modal('hide');
              location.reload();
          }
          else{
          $('.profile-load-more').hide();
            $('#profileImageModal').modal('hide');
            alert('Something went wrong.Please try again.');
          }
          
          //$('#uploaded_image').html(data);
        }
      });
    })
  });
  
  //for cover photo
  $image_crop = $('#image_demo').croppie({
    enableExif: true,
    viewport: {
      width:700,
      height:250,
      type:'square' //circle
    },
    boundary:{
      width:765,
      height:300
    }
  });
  
  $('#file1').on('change', function(){
    var reader = new FileReader();
    reader.onload = function (event) {
      //console.log(event.target.result);
      //$('#chat_file_hidden').val(reader.result);
    }
    reader.readAsDataURL(this.files[0]);
    //console.log(reader.result);
  });

  $('#edit_cover').on('change', function(){
    var reader = new FileReader();
    reader.onload = function (event) {
      $image_crop.croppie('bind', {
        url: event.target.result
      }).then(function(){
       // console.log('jQuery bind complete');
      });
    }
    reader.readAsDataURL(this.files[0]);
    $('#uploadimageModal').modal('show');
  });

  $('.crop_image').click(function(event){
    $image_crop.croppie('result', {
      type: 'canvas',
      size: 'viewport'
    }).then(function(response){
        $('.load-more').show();
      $.ajax({
        headers: {
              'X-CSRF-TOKEN': '<?php echo e(csrf_token()); ?>'
        },
        url: "<?php echo e(url('cover-image-upload')); ?>",
        type: "POST",
        data:{"cover_image": response,'user_id': $("#user_id").val()},
        success:function(data)
        {
          if (data.success) {
                $('.load-more').hide();
              $('.profile_cover_pic_outer').css('background-image', 'url(' + '<?php echo e(asset('public/images/cover_img')); ?>/' + data.img + ')');
              $('#uploadimageModal').modal('hide');
              location.reload();
          }
          else{
          $('.load-more').hide();
            $('#uploadimageModal').modal('hide');
            alert('Something went wrong.Please try again.');
          }
          
          //$('#uploaded_image').html(data);
        }
      });
    })
  });

  //for gallery upload
  $image_crop_galley = $('#image_gallery').croppie({
    enableExif: true,
    viewport: {
      width:700,
      height:250,
      type:'square' //circle
    },
    boundary:{
      width:765,
      height:300
    }
  });

  $('#my_photo').on('change', function(){
    var reader = new FileReader();
    reader.onload = function (event) {
      $image_crop_galley.croppie('bind', {
        url: event.target.result
      }).then(function(){
        //console.log('jQuery bind complete');
      });
    }
    reader.readAsDataURL(this.files[0]);
    $('#uploadMyPhotoModal').modal('show');
  });

  $('.crop_image_gallery').click(function(event){
    $image_crop_galley.croppie('result', {
      type: 'canvas',
      size: 'viewport'
    }).then(function(response){
        $('.load-more').show();
      $.ajax({
        headers: {
              'X-CSRF-TOKEN': '<?php echo e(csrf_token()); ?>'
        },
        url: "<?php echo e(url('gallery-image-upload')); ?>",
        type: "POST",
        data:{"gallery_image": response,'user_id': $("#user_id").val(),'is_private':$('input[name=is_private]:checked').val()},
        success:function(data)
        {
          if (data.success) {
                $('.load-more').hide();
              //$('.profile_cover_pic_outer').css('background-image', 'url(' + '<?php echo e(asset('public/images/cover_img')); ?>/' + data.img + ')');
              $('#uploadMyPhotoModal').modal('hide');
              location.reload();
          }
          else{
          $('.load-more').hide();
            $('#uploadMyPhotoModal').modal('hide');
            location.reload();
            alert('Something went wrong.Please try again.');
          }
          
          //$('#uploaded_image').html(data);
        }
      });
    })
  });

});  
</script>

    <script src="<?php echo e(URL::asset('assets/js/libs/moustache.js')); ?>"></script>
    <script src="<?php echo e(URL::asset('assets/js/libs/deparam.js')); ?>"></script>
<!-- chat scripts --->



<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">    
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<script>
    $(function() {
        $("#chat_input").autocomplete({            
            source: '<?php echo url("/")?>/get_chat_contacts',
            select: function( event, ui ) {
                event.preventDefault();
                //$("#chat_input").val('');              
                    $('.no-chat-scrl').hide();
                    $("#chat_input_btn").html('<div class="col-md-4"><div class=""><img src="<?php echo e(asset('/public/thumbnail/')); ?>/'+ ui.item.image + '"></div></div><div class="col-md-4">'+ ui.item.value + '</div><div class="col-md-4"><button class="btn-bg" onclick="set_receiver('+ui.item.id+',"'+ui.item.value+'","'+ui.item.image+'")">Chat</button></div>');                    
              
                //set_receiver(ui.item.id, ui.item.value, ui.item.image);
            }
        });
        $("#chat_input").on('keyup',function(){            
            if($("#chat_input").val() == ''){
                $('.no-chat-scrl').show();
                $('#chat_input_btn').hide();                
            }
        });
        
    });
</script>


<script>
$(document).ready(function(){
  $('#owl-carousel-photo').owlCarousel({
    loop:false,
    mouseDrag  : false,
    margin:0,
    responsiveClass:true,

    responsive:{
      0:{
        items:1,
        nav:true
      },
      600:{
        items:3,
        nav:false
      },
      1000:{
        items:5,
        nav:true,
        loop:false,
        margin:20
      }
    }
  })
})
</script>
<script>
function isMobile(width) {
  if(width == undefined){
    width = 719;
  }
  if(window.innerWidth <= width) {
    return true;
  } else {
    return false;
  }
}

</script>

<script>
  //Show proposal div on click Proposal button
  $(".send_proposal_btn").on("click",function(e){
    get_proposal(8);
    $(".send_proposal_wrap").slideToggle(300);
    e.stopPropagation();
  });

  //hide proposal div on click body anywhere
  $('body').click(function() {
     $('.send_proposal_wrap').slideUp(300);
  });
  //stop event on click proposal div
  $('.send_proposal_wrap').click(function(event){
     event.stopPropagation();
  });
  //hide proposal div on click remove button
  $(".close_pro").on("click",function(){
    $(".send_proposal_wrap").slideUp(300);
  });
</script>
<script>
    function send_proposal(image){
        //alert(image);
        var receiver_id = '<?php echo e($user_detail->user_id); ?>';
        $.ajax({
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '<?php echo url("/");?>/chat-proposal',
            type: 'POST',
            data: 'receiverId='+receiver_id+'&image='+image,
            cache : false,
            dataType: 'html',
            async: false,
            processData: false,
            success: function(data)
            {
              //console.log(data);
              //location.reload();
              $('#proposal_message').show();
              $(".send_proposal_wrap").slideUp(300);
            }
          });
    }
</script>
    
<script>
    
    function get_proposal(propsal_category_id){
        //alert(image);        
        $.ajax({
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '<?php echo url("/");?>/getProposal',
            type: 'POST',
            data: 'Id='+propsal_category_id,
            cache : false,
            dataType: 'html',
            async: false,
            processData: false,
            success: function(data)
            {
              //console.log(data);
              $('#proposal_0').html(data);
            }
          });
    }
</script>


<script>
  //tab menu css
  $(document).ready(function(){
    //tab menu js script
    $('.tab_menu li').on('click',function(){
      $('.tab_menu li').removeClass("active");
      $(this).addClass("active");
      var content= $(this).attr('data-show');
      $(".tab_content").removeClass("active");
      $("#"+content).addClass("active");
    });
  });
</script>

<script>
    function send_message(){        
        var receiver_id = $('#matched_receiver_id').val();
        var message = $('#message_p').val();
        if(message == ''){ return false; }
        //alert(receiver_id);        
        $.ajax({
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '<?php echo url("/");?>/send-like-message',
            type: 'POST',
            data: 'receiver_id='+receiver_id+'&message='+message,
            cache : false,
            dataType: 'json',
            async: false,
            processData: false,
            success: function(data)
            {
              console.log(data);
              sendMsg2();
              //setInterval(function(){ location.reload(); }, 2000);              
              return false;
            }
          });
          
          //$('#like_popup').modal('hide');
          return false;
    }
</script>
<script>
    function set_smiley(smiley,planType,points){
                $('#planType').val(planType);
                $('#points').val(points);
                //$('#message<?php echo e($receiver_id); ?>').val($('#message<?php echo e($receiver_id); ?>').val()+'<img src="<?php echo e(asset('public/thumbnail')); ?>/'+smiley+'">');
                CKEDITOR.instances.message.insertHtml($('#message').val()+'<img class="cc_img" src="<?php echo e(asset('public/thumbnail')); ?>/'+smiley+'" height="25" style="margin:2px;">');
                
                //$('#message<?php echo e($receiver_id); ?>').val(smiley);
                //$('#message-form<?php echo e($receiver_id); ?>').submit();
            }
            
    function setproposal(proposal,planType,points){
                //alert(proposal);
                $('#planType').val('diamonds');
                $('#points').val('1');
                //$('#message').val(proposal);
                CKEDITOR.instances.message.insertHtml('<img class="cc_img" src="<?php echo e(asset('public/images/profile_image')); ?>/'+proposal+'" style="margin:2px;">');
                sendMsg();
                $(".chat_proposal_wrap").slideUp(300);
            }        
//load emoji icons 
    function get_emoji(){        
        $.ajax({
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '<?php echo url("/");?>/get-emoji',
            type: 'GET',            
            cache : false,
            dataType: 'html',
            async: false,
            processData: false,
            success: function(data)
            {
              //console.log(data);
              $('.emoji-list').html(data);
            }
          });
    }
</script>
<script>
    function checkdata(sender,receiver,msg,imgurl){
      sender = $('#sender_name').val()+'__'+sender;
      receiver = $('#receiver_name').val()+'__'+receiver;
      //console.log(imgurl);
      imgurl = imgurl.replace(/(<p[^>]+?>|<p>|<\/p>)/img, "");
      imgurl = imgurl.replace(/style="[a-zA-Z0-9:;\.\s\(\)\-\,]*"/gi, "");
        
        var m,
            urls = [], 
            str = imgurl,
            rex = /<img[^>]+src="?([^"\s]+)"?\s*\/>/g;
        
        while ( m = rex.exec( str ) ) {
            urls.push( m[1] );
        }

      //console.log(urls);
      var attachment = urls;
      if((attachment.length) > 0) { 
        var json = {
          "username" : "Moneo",
          "password" : "Moneo2019@",
          "service_code" : "cd9271fa",
          "to" : receiver,
          "from" : sender,
          "message" : msg,
          "attachment": attachment
        };
      }else{
        var json = {
          "username" : "Moneo",
          "password" : "Moneo2019@",
          "service_code" : "cd9271fa",
          "to" : receiver,
          "from" : sender,
          "message" : msg          
        };
      }

      var myJSON = JSON.stringify(json); 

      $.ajax({
        beforeSend: function() { },
        type: "POST",
        crossDomain: true,        
        url: "https://im.operatorplatform.com/api/inbound",
        ContentType: "application/x-www-form-urlencoded",
        
        /*data: {
          data : '{"username":"Moneo","password":"Moneo2019@","service_code":"cd9271fa","to":"'+receiver+'","from":"'+sender+'","message":"'+msg+'"}'
        }, */

        data: {
          data : myJSON
        }, 

        success: function(response){
          console.log('hhhhhhhhhhhhhhhhhhhhhhhhhh');  
          console.log(response);



        },
        error: function(XMLHttpRequest, textStatus, errorThrown) { 
         console.log(XMLHttpRequest);
         console.log(textStatus);
         console.log(errorThrown);
        }       
      });
    }
    
   
    function createChatRoom(){
        
        var room_id = $('#room_id').val();
        if(room_id != ''){
            firebase.database().ref('chat_rooms/' + room_id).off("child_added");
        }
        
        var uid = $('#receiver_id').val();
        var chatRoom = adminId + '_' + uid;
        if(uid < adminId){
          chatRoom = uid + '_' + adminId;
        }
        
    
    
    
        
        firebase.database().ref('chat_rooms/' + chatRoom).off("child_added");
        
        $('#room_id').val(chatRoom);                     
                
    }


    function timeDifference(previous) {
        
        var current= new Date();

        var msPerMinute = 60 * 1000;
        var msPerHour = msPerMinute * 60;
        var msPerDay = msPerHour * 24;
        var msPerMonth = msPerDay * 30;
        var msPerYear = msPerDay * 365;
        
        var elapsed = current - previous;
        
        if (elapsed < msPerMinute) {
             return Math.round(elapsed/1000) + ' sec ago';   
        }
        
        else if (elapsed < msPerHour) {
             return Math.round(elapsed/msPerMinute) + ' min ago';   
        }
        
        else if (elapsed < msPerDay ) {
             return Math.round(elapsed/msPerHour ) + ' hours ago';   
        }

        else if (elapsed < msPerMonth) {
             return Math.round(elapsed/msPerDay) + ' days ago';   
        }
        
        else if (elapsed < msPerYear) {
             return Math.round(elapsed/msPerMonth) + ' months ago';   
        }
        
        else {
             return Math.round(elapsed/msPerYear ) + ' years ago';   
        }
    }
    
    function removeHtmlCssJs(text){
    text = $.trim(text);
    text = text.replace(/\&/g, '&amp;');
        text = text.replace(/\>/g, '&gt;');
        text = text.replace(/\</g, '&lt;');
        text = text.replace(/\"/g, '&quot;');
        text = text.replace(/\'/g, '&apos;');
        return text;
  }
    

    
    createChatRoom();
    function sendMsg(){
        var msgText = CKEDITOR.instances.message.getData();
        if(msgText){
            //$('.alreadyChat').attr('disabled',true);    
        }
        
        if($('#message-text').val() == 'Image'){
          $('#message-text').val('');
          sendImage();
          return true;
        };
      
        var sender_is_fake = '<?php echo e($users->fake); ?>';
    
        var msgText1 = CKEDITOR.instances.message.getData();
    
        if(msgText1 == ''){
            return true;
        }
    
    
    var mycredits = jQuery("#initialcredit").val();//localStorage.getItem("credits");
            var mypremiums = jQuery("#initialpremium").val();
            if(mycredits == 'null'){ mycredits = 0;}
            var requiredCredits = 0;
            
            
            // for text required credit will be 1
            var type = (jQuery("#file").val() == '') ? 'text' : 'file';
            if(type === "text"){
                requiredCredits = 1;
            }
            
            var planType = jQuery("#planType").val();
            
            if(planType === 'credits'){ requiredCredits = jQuery("#points").val(); }
            
            if(planType === 'diamonds'){ requiredCredits = jQuery("#points").val(); }     
            
            
            if(planType === 'diamonds'){                
                //if(mycredits === 0){
                    var diamnd = jQuery("#updateddiamonds").html();
                    //console.log(diamnd);
                    if(diamnd > 0){
                        mycredits = diamnd;   
                    }                    
                //}                                            
                //console.log(mycredits);
                if(parseInt(mycredits) >= parseInt(requiredCredits) || (sender_is_fake == 1) ){
                    $.ajax({
                        headers: {
                          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: '<?php echo url("/")?>/update_diamonds',
                        type: 'POST',
                        cache : false,
                        processData: false,
                        data: 'room_id='+jQuery('#room_id').val()+'&amt='+requiredCredits+'&receiver_id='+jQuery('#receiver_id').val()+'&msg='+msgText1, // serializes the form's elements.
                        success: function(data)
                        {
                    
                         if(data){
                            var d = JSON.parse(data);
                                                    
                            if(d.credits){
                                jQuery("#initialcredit").val(d.credits);
                                jQuery("#updatedwallet").html(d.credits);
                                jQuery("#updateddiamonds").html(d.diamonds);   
                            }else{                            
                                jQuery("#updateddiamonds").html(d.diamonds);   
                            }                        
                          }                      
                        }
                    });
                }
            }else{
                if(parseInt(mycredits) >= parseInt(requiredCredits) || (sender_is_fake == 1) ){
                    $.ajax({
                        headers: {
                          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: '<?php echo url("/")?>/update_conversation',
                        type: 'POST',
                        cache : false,
                        processData: false,
                        data: 'room_id='+jQuery('#room_id').val()+'&amt='+requiredCredits+'&receiver_id='+jQuery('#receiver_id').val()+'&msg='+msgText1, // serializes the form's elements.
                        success: function(data)
                        {
                       
                          if(data){
                            jQuery("#initialcredit").val(data);
                            jQuery("#updatedwallet").html(data);
                          }
                        }
                    });
                }
            }
            
    
    
    
    
    
        var chat_send_image = $('#chat_send_image').val();
        var messageType = 'text';
        if(chat_send_image != ''){
            messageType = 'text';
           // CKEDITOR.instances.message.setData(chat_send_image);
        }
    
        var userId    = $('#receiver_id').val();
        var userName  = $('#receiver_name').val();
        var userImage = $('#receiver_image').val();
        
    
       
        

    
        CKEDITOR.instances.message.setData('');
        
        var chatRoom = $('#room_id').val();
    
        firebase.database().ref('/users/'+userId).once('value').then(function(snapshot) {
          var childData = snapshot.val();                    
          
          if(childData.fake == '1'){
            if(parseInt(mycredits) >= parseInt(requiredCredits) || (sender_is_fake == 1) ){
                if(msgText.substring(0, 11) === '<p><img src'){
                    checkdata(adminId,userId,'image',msgText);
                }else{
                    checkdata(adminId,userId,msgText,'');   
                }
            }
          }

          firebase.database().ref().child('chat_history/'+adminId).child(userId).once('value').then(function(snapshot1) {
    
            var snapshot1Data = snapshot1.val();
    
            var msgCount = 0;
            var hisType = 1;
            if (snapshot1Data != null){
              var msgCount = Number(snapshot1Data.messageCount)+1;
              if(snapshot1Data.historyType == 1){
                if(snapshot1Data.lastSenderId != adminId){
                  hisType = 2;
                }
              }else{
                hisType = 2;
              }
            }
    
            var insertData = {
              deletteby : '',
              message : msgText,
              messageCount : msgCount,
              name : userName,
              profilePic : userImage,
              timestamp : $.now(),
              uid : userId.toString(),
              historyType : hisType,
              lastSenderId : adminId.toString(),
              messageType : messageType,
              historyLoad : '0',
            };
            if(parseInt(mycredits) >= parseInt(requiredCredits) || (sender_is_fake == 1) ){
                firebase.database().ref().child('chat_history/'+adminId).child(userId).set(insertData);
            }
          });
        });
    
        firebase.database().ref('/users/'+adminId).once('value').then(function(snapshot) {
          var childData = snapshot.val();
    
          firebase.database().ref().child('chat_history/'+userId).child(adminId).once('value').then(function(snapshot2) {
    
            var snapshot2Data = snapshot2.val();
    
            var msgCount = 0;
            var hisType = 1;
            if (snapshot2Data != null){
              var msgCount = Number(snapshot2Data.messageCount)+1;
              if(snapshot2Data.historyType == 1){
                if(snapshot2Data.lastSenderId != adminId){
                  hisType = 2;
                }
              }else{
                hisType = 2;
              }
            }
    
            var insertData = {
              deletteby : '',
              message : msgText,
              messageCount : msgCount,
              name : childData.userName,
              profilePic : childData.userImage,
              timestamp : $.now(),
              uid : adminId.toString(),
              historyType : hisType,
              lastSenderId : adminId.toString(),
              messageType : messageType,
        historyLoad : '0',
            };
            if(parseInt(mycredits) >= parseInt(requiredCredits) || (sender_is_fake == 1) ){
                firebase.database().ref().child('chat_history/'+userId).child(adminId).set(insertData);
            }
          });
        });
    
        firebase.database().ref('/users/'+adminId).once('value').then(function(snapshot) {  
          var childData = snapshot.val();
          var insertData = {
            deleteby:'',
            message : msgText,
            messageCount: 1,
            name : childData.userName,
            profilePic : childData.userImage,
            timestamp : $.now(),
            uid : adminId.toString(),
            messageType : messageType,
            imageText : '',
          };
    
      if(parseInt(mycredits) >= parseInt(requiredCredits) || (sender_is_fake == 1) ){
      firebase.database().ref().child('chat_rooms/').child(chatRoom).push(insertData);
      
            if(planType == 'diamonds'){
                $('#proposal_message').show();
                     $(".send_proposal_wrap").slideUp(300);
            }else{
                     $('#chat_message').show();
            }
              
      }else{
            //console.log();
            <?php if(isset($users) && ($users->fake)){ ?>
                var sendrimgurl = '<?php echo e(env('PROFILE_MANAGER_ADDR').('/public/thumbnail/')); ?>/';
            <?php }else{ ?>
                var sendrimgurl = '<?php echo e(asset('/public/thumbnail/')); ?>/';
            <?php } ?>
            //message failed
            $('#messages').append(''+ 
                          '<li class="message">'+                    
                              '<div class="chat-user-right">'+
                              '<div class="chat-user-box">'+
                              '<img src="'+sendrimgurl+jQuery('#sender_image').val()+'">'+
                              '<span>'+jQuery('#sender_name').val()+'</span>'+
                              '</div>'+
                              '<div class="chat-user-message">'+
                              '<div class="chat-line"><div class="chat_border">'+msgText+'</div></div>'+                                   
                              '<div class="meta-info"><span class="date">'+$.now()+'</span></div>'+
                  '<div class="meta-info red"><span class="date">Message Sent Failed!!</span></div>'+
                              '</div>'+
                              '</div>'+
                          '</li>'+
                      '');
            $('#msgDiv').animate({
                  scrollTop: $('#messages').prop("scrollHeight")
              }, 0);
              setTimeout(function(){ window.location.replace("{{ url('/credits') }}"); }, 500); 
      }
        });
        
        $("#planType").val('credits');
        $("#points").val(1);
      }
      
          
      $(document).on('change', '#file', function (e) {
      //$('#imageText').click();
      $('#selectedImage').attr('src',URL.createObjectURL(e.target.files[0]));
      $('#message-text').val('Image');      
     // alert(e.target.files[0].name)
      var fileName = e.target.files[0].name;
        $("#selected").html(fileName);

      //sendImage();
    });
    
    function sendImage(){
    //var imageText = $('#message-text').val();
    var imageText = CKEDITOR.instances.message.getData(); 
    var file = $('input#file')[0].files[0];
    
    CKEDITOR.instances.message.setData('');
    $("#selected").html('');

    var storageRef = firebase.storage().ref();
        var dataD = Date.now();
        var uploadTask = storageRef.child('images/' + dataD).put(file);
        
        <?php if(isset($users) && ($users->fake)){ ?>
            var sendrimgurl = '<?php echo e(env('PROFILE_MANAGER_ADDR').('/public/thumbnail/')); ?>/';
        <?php }else{ ?>
            var sendrimgurl = '<?php echo e(asset('/public/thumbnail/')); ?>/';
        <?php } ?>
    
    var imageUrl = "https://thumbs.gfycat.com/JointRevolvingAntelopegroundsquirrel-size_restricted.gif"; //"http://www.mizpahpublishing.com/images/loader.gif";

    var myName = $('#sender_name').val();
    var myimage = sendrimgurl+$('#sender_image').val();
    
    
    $('#messages').append(''+
      '<li id="removeLi" class="message">'+                    
      '<div class="chat-user-right">'+
      '<div class="chat-user-box">'+
      '<img src="'+myimage+'">'+
      '<span>'+myName+'</span>'+
      '</div>'+
      '<div class="chat-user-message">'+
      '<div class="chat-line"><div class="chat_border">'+
      '<img src=' + imageUrl + ' alt="Ovengo..." height="100" width="100">'+
      '</div></div>'+                                   
      '<div class="meta-info"><span class="date">Now</span></div>'+
      '</div>'+
      '</div>'+
    '</li>'+
    '');
        
        uploadTask.on('state_changed', function(snapshot) { 
                var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;                                             

            }, 
            function(error) {

            }, function() {
                $('#removeLi').remove();
                var storageqq = firebase.storage();
                storageqq.ref('images/' + dataD).getDownloadURL().then(function (url) {
                    var downloadURL = url;
                    
                        var messageType = 'file';
                    
                    
                        var userId    = $('#receiver_id').val();
                        var userName  = $('#receiver_name').val();
                        var userImage = $('#receiver_image').val();
                        var msgText = downloadURL;
                        //alert(msgText);
                        //CKEDITOR.instances.message.setData('');
                    
                        var chatRoom = $('#room_id').val();
                    
                        firebase.database().ref('/users/'+userId).once('value').then(function(snapshot) {
                          var childData = snapshot.val();
                          
                          firebase.database().ref().child('chat_history/'+adminId).child(userId).once('value').then(function(snapshot1) {
                    
                            var snapshot1Data = snapshot1.val();
                    
                            var msgCount = 0;
                            var hisType = 1;
                            if (snapshot1Data != null){
                              var msgCount = Number(snapshot1Data.messageCount)+1;
                              if(snapshot1Data.historyType == 1){
                                if(snapshot1Data.lastSenderId != adminId){
                                  hisType = 2;
                                }
                              }else{
                                hisType = 2;
                              }
                            }
                    
                            var insertData = {
                              deletteby : '',
                              message : msgText,
                              messageCount : msgCount,
                              name : userName,
                              profilePic : userImage,
                              timestamp : $.now(),
                              uid : userId.toString(),
                              historyType : hisType,
                              lastSenderId : adminId.toString(),
                              messageType : messageType,
                                historyLoad : '0',
                            };
                          console.log(insertData);
                          firebase.database().ref().child('chat_history/'+adminId).child(userId).set(insertData);
                          });
                        });
                    
                        firebase.database().ref('/users/'+adminId).once('value').then(function(snapshot) {
                          var childData = snapshot.val();
                    
                          firebase.database().ref().child('chat_history/'+userId).child(adminId).once('value').then(function(snapshot2) {
                    
                            var snapshot2Data = snapshot2.val();
                    
                            var msgCount = 0;
                            var hisType = 1;
                            if (snapshot2Data != null){
                              var msgCount = Number(snapshot2Data.messageCount)+1;
                              if(snapshot2Data.historyType == 1){
                                if(snapshot2Data.lastSenderId != adminId){
                                  hisType = 2;
                                }
                              }else{
                                hisType = 2;
                              }
                            }
                    
                            var insertData = {
                              deletteby : '',
                              message : msgText,
                              messageCount : msgCount,
                              name : childData.userName,
                              profilePic : childData.userImage,
                              timestamp : $.now(),
                              uid : adminId.toString(),
                              historyType : hisType,
                              lastSenderId : adminId.toString(),
                              messageType : messageType,
                              historyLoad : '0',
                            };
                            console.log(insertData);
                          firebase.database().ref().child('chat_history/'+userId).child(adminId).set(insertData);
                          });
                        });
                    
                        firebase.database().ref('/users/'+adminId).once('value').then(function(snapshot) {  
                          var childData = snapshot.val();
                          var insertData = {
                            deleteby:'',
                            message : msgText,
                            messageCount: 1,
                            name : childData.userName,
                            profilePic : childData.userImage,
                            timestamp : $.now(),
                            uid : adminId.toString(),
                            messageType : messageType,
                            imageText : imageText,
                          };
                          firebase.database().ref().child('chat_rooms/').child(chatRoom).push(insertData);
                        });
      
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    $('#chat_send_image').val(downloadURL);
                }); 
            
            });
    
    
    
    
    }
    
          
</script>    
    
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layout.default', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>