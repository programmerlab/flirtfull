<?php

declare(strict_types=1);

namespace Modules\Admin\Http\Controllers;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Redirect;
use Input;
use Modules\Admin\Http\Requests\UserRequest;
use Modules\Admin\Models\Roles;
use Modules\Admin\Models\Language;
use Modules\Admin\Models\Interest;
use Modules\Admin\Models\User;
use Modules\Admin\Models\User_detail;
use Modules\Admin\Models\Gallery_image;
use Modules\Admin\Models\Gallery_video;
use Route;
use View;
use Session;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use DateTime;

use Excel;
/**
 * Class AdminController
 */
class GridController extends Controller
{
    /**
     * @var  Repository
     */

    /**
     * Displays all admin.
     *
     * @return \Illuminate\View\View
     */
    public function __construct()
    {
        $this->middleware('admin');
        View::share('viewPage', 'Users');
        View::share('helper', new Helper);
        View::share('heading', 'App Players');
        View::share('route_url', route('user'));

        $this->record_per_page = Config::get('app.record_per_page');        
    }

    protected $users;

    /*
     * Dashboard
     * */

    public function index(User $user, Request $request)
    {        
        $page_title  = 'Grid Players';
        $page_action = 'Admin Users';

        if ($request->ajax()) {
            $id           = $request->get('id'); 
            $status       = $request->get('status');
            $user         = Gallery_image::find($id);
            $s            = ($status == 1) ? $status=0:$status=1;
            $user->status = $s;
            $user->save();
            echo $s;

            exit();
        }
        // Search by name ,email and group
        $search    = Input::get('search');
        $status    = Input::get('status');
        $role_type = Input::get('role_type');

        if ((isset($search) && !empty($search)) or  (isset($status) && !empty($status)) or !empty($role_type)) {
            $search = isset($search) ? Input::get('search') : '';

            $users = User::where(function ($query) use ($search,$status,$role_type) {
                if (!empty($search)) {
                    $query->Where('first_name', 'LIKE', "%$search%")
                        ->OrWhere('last_name', 'LIKE', "%$search%")
                        ->OrWhere('email', 'LIKE', "%$search%");
                }

                if (!empty($status)) {
                    
                    $status =  ($status == 'active')?1:0;
                    $query->Where('status', $status);
                }

                if ($role_type) {
                    $query->Where('role_type', $role_type);
                }
            })->where('role_type', 3)->Paginate($this->record_per_page);
        } else {
            $users = User::orderBy('id', 'desc')->where('role_type', 3)->Paginate(30);
            
            foreach($users as $key => $user){                
                $gallery_images = Gallery_image::where('user_id',$user['id'])->get();
                $g_images = [];
                foreach($gallery_images as $img){
                    $g_images[] = array($img->gallery_image,$img->status);
                }
                $users[$key]['gallery_images'] = $g_images;
            }                                    
        }
        
        //echo "<pre>"; print_r($user); die;
        $roles = Roles::all();

        $js_file = [];

        return view('admin::users.grid.index', compact('js_file', 'roles', 'status', 'users', 'page_title', 'page_action', 'roles', 'role_type'));
    }

    /*
     * create Group method
     * */

    public function create(User $user)
    {
        $page_title  = 'Player';
        $page_action = 'Create Player';
        $roles       = Roles::all();
        $languages       = Language::all();
        $interests       = Interest::all();
        $role_id     = 3;
        $js_file     = ['common.js','bootbox.js','formValidate.js'];


        return view('admin::users.user.create', compact('js_file', 'role_id', 'roles', 'user', 'page_title', 'page_action', 'groups','languages','interests'));
    }

    /*
     * Save Group method
     * */

    public function store(UserRequest $request, User $user)
    {
        $user->fill(Input::all());
        $user->password = Hash::make($request->get('password'));

        $action = $request->get('submit');

        $request->file('profile_image');
        if ($action == 'avtar') {
            if ($request->file('profile_image')) {
                $profile_image = User::createImage($request, 'profile_image');
                $request->merge(['profilePic' => $profile_image]);
                $user->profile_image = $request->get('profilePic');
            }
        }
        $user->save();
        $js_file = ['common.js','bootbox.js','formValidate.js'];

        return Redirect::to(route('grid'))
            ->with('flash_alert_notice', 'New record successfully created.');
    }

    /*
     * Edit Group method
     * @param
     * object : $user
     * */

    public function edit(User $user, User_detail $user_detail)
    {
        //echo "<pre>"; print_r($user); die;
        $page_title  = 'Admin User';
        $page_action = 'Show Users';
        $role_id     = $user->role_type;
        $roles       = Roles::all();
        $languages       = Language::all();
        $interests       = Interest::all();
        $js_file     = ['common.js','bootbox.js','formValidate.js'];
        
        $user = User::leftJoin('user_details', function($join) {
            $join->on('users.id', '=', 'user_details.user_id');
          })->leftJoin('user_languages', function($join1) {
            $join1->on('users.id', '=', 'user_languages.user_id');
          })->leftJoin('user_interests', function($join2) {
            $join2->on('users.id', '=', 'user_interests.user_id');
          })
          ->where('users.id','=',$user->id)
          ->first();
       
        $user['gallery_images'] = Gallery_image::where('user_id',$user->id)->get();
        $user['gallery_videos'] = Gallery_video::where('user_id',$user->id)->get();
        
        return view('admin::users.grid.edit', compact('js_file', 'role_id', 'roles', 'user', 'page_title', 'page_action','languages','interests'));
    }
    
    
    public function show(User $user, User_detail $user_detail)
    {
        //echo "<pre>"; print_r($user->id); die;
        $page_title  = 'Admin User';
        $page_action = 'Show Users';
        $role_id     = $user->role_type;
        $roles       = Roles::all();
        $languages       = Language::all();
        $interests       = Interest::all();
        $js_file     = ['common.js','bootbox.js','formValidate.js'];
        $userid      = $user->id;    
        $user = User::leftJoin('user_details', function($join) {
            $join->on('users.id', '=', 'user_details.user_id');
          })->leftJoin('user_languages', function($join1) {
            $join1->on('users.id', '=', 'user_languages.user_id');
          })->leftJoin('user_interests', function($join2) {
            $join2->on('users.id', '=', 'user_interests.user_id');
          })
          ->where('users.id','=',$user->id)
          ->first();
       
        $user['gallery_images'] = Gallery_image::where('user_id',$userid)->get();
        $user['gallery_videos'] = Gallery_video::where('user_id',$userid)->get();
        $user->id = $userid;
        return view('admin::users.grid.show', compact('js_file', 'role_id', 'roles', 'user', 'page_title', 'page_action','languages','interests'));
    }

    public function update(Request $request, User $user)
    {
        if($request->get('copy')){            
            $users = DB::table('users')->where('id', $user->id)->first();
            
            if(($request->get('profile_language') != $users->profile_language) || ($request->get('country') != $users->country)){
                if(!empty($request->get('dob')) && $request->get('dob') != 'NULL')
                {
                    $from = new DateTime($request->get('dob'));
                    $to   = new DateTime('today');
                    $age = $from->diff($to)->y;
                }
                else{
                    $age = '';
                }
                               
                
                $newuserid = DB::table('users')->insertGetId(
                        array(
                                'name' => $request->get('name'),
                                'email' => $request->get('email'),
                                'password' =>  bcrypt($request->get('password')),
                                'phone' => $request->get('phone'),
                                'gender' => $request->get('gender'),
                                'dob' => date('Y-m-d', strtotime($request->get('dob'))),
                                'age' => $age,  
                                'country' => $request->get('country'),
                                'city' => $request->get('city'),
                                'address' => $request->get('address'),
                                'profile_image' => $users->profile_image,
                                'cover_image' => $users->cover_image,
                                'profile_type' => $request->get('profile_type'),
                                'appearance' => $request->get('appearance'),
                                'adult_level' => $request->get('adult_level'),
                                'profile_language' => $request->get('profile_language'),
                                'is_update' => 0,//$value->is_update,
                                'is_login' => 0,//$value->is_login,
                                'last_login' => date('Y-m-d H:i:s'),//$value->last_login,
                                'status' => 1,//$value->status,
                                //'created_at' => date('Y-m-d H:i:s', strtotime($value->created_at)),
                                'updated_at' => date('Y-m-d H:i:s')
                              )
                    );
                
                if($newuserid){
                    $userdetail['user_id'] = $newuserid;
                    $userdetail['about_me'] = $request->get('about_me');        
                    $userdetail['seeking'] = $request->get('seeking');
                    $userdetail['about_partner'] = $request->get('about_partner');
                    $userdetail['work_as'] = $request->get('work_as');
                    $userdetail['education'] = $request->get('education');
                    $userdetail['relationship'] = $request->get('relationship');
                    $userdetail['kids'] = $request->get('kids');
                    $userdetail['smoke'] = $request->get('smoke');
                    $userdetail['drink'] = $request->get('drink');
                    $userdetail['height'] = $request->get('height');
                    $userdetail['updated_at'] = date('Y-m-d h:i:s');
                    
                    DB::table('user_details')->insert(
                        $userdetail
                    );
                    
                    $userlanguage['user_id'] = $newuserid;
                    $userlanguage['languages'] = (!empty($request->get('languages')) ) ? implode(',',$request->get('languages')) : '' ;
                    $userlanguage['updated_at'] = date('Y-m-d h:i:s');
                    DB::table('user_languages')->insert(
                        $userlanguage
                    );
                    
                    $userinterests['user_id'] = $newuserid;
                    $userinterests['interests'] = (!empty($request->get('interests')) ) ? implode(',',$request->get('interests')) : '' ;
                    $userinterests['updated_at'] = date('Y-m-d h:i:s');
                    DB::table('user_interests')->insert(
                        $userinterests
                    );
                    
                    $galleryimage = DB::table('gallery_images')->where('user_id', $user->id)->get();
                    foreach($galleryimage as $imgs){                                        
                        DB::table('gallery_images')->insert(
                            ['user_id' => $newuserid, 'gallery_image' => $imgs->gallery_image, 'updated_at' => date('Y-m-d h:i:s')]
                        );
                        //echo "<pre>"; print_r($gallery_image);
                    }
                }
                return Redirect::to(route('grid.edit',$newuserid))
                        ->with('flash_alert_notice', 'New Record successfully created.');
            }
            

            return Redirect::to(route('grid'))
                ->with('flash_alert_notice', 'Record successfully updated.');
        }
        
        $userid = $user->id;
        $password = $user->password;
        $user->fill(Input::all());
        //echo "<pre>"; print_r($user); die;
        if (!empty($request->get('password'))) {            
            $user->password = Hash::make($request->get('password'));
        }else{
            $user->password = $password;
        }

        $action          = $request->get('submit');
        $user->role_type = $request->get('role_type');

        if ($action == 'avtar') {
            if ($request->file('profile_image')) {                
                $profile_image = User::createImage($request, 'profile_image');
                $request->merge(['profilePic' => $profile_image]);
                $user->profile_image = $request->get('profilePic');
            }
        } elseif ($action == 'businessInfo') {
        } elseif ($action == 'paymentInfo') {
        } else {
        }


        $validator_email = User::where('email', $request->get('email'))
            ->where('id', '!=', $user->id)->first();
            
        //echo "<pre>"; print_r($validator_email); die;    

        //if ($validator_email) {
        //    if ($validator_email->id == $user->id) {
        //        $user->save();
        //    } else {
        //        return  Redirect::back()->withInput()->with(
        //            'field_errors',
        //              'The Email already been taken!'
        //         );
        //    }
        //}
        
        $user->save();
        
        if ($request->file('cover_image')) {
            $profile_image = User::createCoverImage($request, 'cover_image');
            $request->merge(['profilePic' => $profile_image]);
            $user->cover_image = $request->get('profilePic');
            
            DB::table('users')
            ->where('id',$userid)
            ->update(["cover_image" => $profile_image, 'updated_at' => date('Y-m-d h:i:s')]);
        }
        
        $userdetail['about_me'] = $request->get('about_me');        
        $userdetail['seeking'] = $request->get('seeking');
        $userdetail['about_partner'] = $request->get('about_partner');
        $userdetail['work_as'] = $request->get('work_as');
        $userdetail['education'] = $request->get('education');
        $userdetail['relationship'] = $request->get('relationship');
        $userdetail['kids'] = $request->get('kids');
        $userdetail['smoke'] = $request->get('smoke');
        $userdetail['drink'] = $request->get('drink');
        $userdetail['height'] = $request->get('height');        
        $userdetail['updated_at'] = date('Y-m-d h:i:s');
        DB::table('user_details')
            ->where('user_id', $userid)
            ->update($userdetail);
        //echo "<pre>"; print_r($user_detail); die;
                                    
        $userlanguage['languages'] = (!empty($request->get('languages')) ) ? implode(',',$request->get('languages')) : '' ;
        $userlanguage['updated_at'] = date('Y-m-d h:i:s');
        DB::table('user_languages')
            ->where('user_id', $userid)
            ->update($userlanguage);
                            
        $userinterests['interests'] = (!empty($request->get('interests')) ) ? implode(',',$request->get('interests')) : '' ;
        $userinterests['updated_at'] = date('Y-m-d h:i:s');
        DB::table('user_interests')
            ->where('user_id', $userid)
            ->update($userinterests);
            
        if($request->file('gallery_image')){
            //print_r($request->file('gallery_image'));                            
                $galleryimg = User::createGalleryImage($request, 'gallery_image');
                $request->merge(['profilePic' => $galleryimg]);
                $galleryimage = $request->get('profilePic');
                
                
                foreach($galleryimage as $imgs){                                        
                    DB::table('gallery_images')->insert(
                        ['user_id' => $userid, 'gallery_image' => $imgs, 'updated_at' => date('Y-m-d h:i:s')]
                    );
                    //echo "<pre>"; print_r($gallery_image);
                }
                
        }
            

        if ($request->get('role') == 3) {
            $Redirect = 'clientuser';
        } else {
            $Redirect = 'grid';
        }

        return Redirect::to(route($Redirect))
            ->with('flash_alert_notice', 'Record successfully updated.');
    }
    /*
     *Delete User
     * @param ID
     *
     */
    public function destroy(Request $request, $user)
    {
         $user->delete();
        return Redirect::to(route('grid'))
            ->with('flash_alert_notice', 'Record  successfully deleted.');
    }
    
    
    public function profile_image_status(User $user, Request $request)
    {        
        //echo $request->ajax('id');        
            $id           = $request->get('id'); 
            $status       = $request->get('status');
            $user         = User::find($id);
            $s            = ($status == 1) ? $status=0:$status=1;
            $user->profile_image_status = $s;
            $user->save();
            echo $s;

            exit();        
    }
    
    public function cover_image_status(User $user, Request $request)
    {
        //echo $request->ajax('id');        
            $id           = $request->get('id');
            $id           = ($id - 1);
            $status       = $request->get('status');
            $user         = User::find($id);
            $s            = ($status == 1) ? $status=0:$status=1;
            $user->cover_image_status = $s;
            $user->save();
            echo $s;

            exit();        
    }
        
    
    public function search(User $user, Request $request)
    {
        //echo $request->ajax('id');        
            $name      = $request->get('name');
            $country      = $request->get('country');
            $age      = $request->get('age');
            $gender      = $request->get('gender');
            $status      = $request->get('status');
            $relationship      = $request->get('relationship');
            $photo      = $request->get('photo');
            $appearance      = $request->get('appearance');
            $profile_type      = $request->get('profile_type');
            $adult_level      = $request->get('adult_level');
            
            //DB::enableQueryLog();
            
            $reslt = DB::table('users')
                    ->join('user_details', 'users.id', '=', 'user_details.user_id')                    
                    ->select('users.*', 'user_details.relationship');
            
            if (!empty($country)) {
                $reslt->Where('users.country', 'LIKE', "%$country%");                        
            }

            if (!empty($name)) {
                $reslt->Where('users.name', 'LIKE', "%$name%");                        
            }
                
            if (!empty($age)) {
                $age = explode('_',$age);
                $reslt->Where('users.age', '>=', $age[0]);
                $reslt->Where('users.age', '<=', $age[1]);
            }
            
            if ($gender) {
                $reslt->Where('users.gender', $gender);
            }
            
            if ($profile_type) {
                $reslt->Where('users.profile_type', $profile_type);
            }
            
            if ($adult_level) {
                $reslt->Where('users.adult_level', $adult_level);
            }
            
            if ($appearance) {
                $reslt->Where('users.appearance', $appearance);
            }
            
            if (!empty($status)) {
                $status = ($status == 'yes') ? '1' : '0';
                $reslt->Where('users.status', $status);
            }
            
            if (isset($photo)) {
                if($photo == '1'){
                    $reslt->Where('users.profile_image', '!=',  '');
                }
                if($photo == '0'){
                    $reslt->Where('users.profile_image','');
                    //$reslt->or_Where('users.profile_image', '=',  'default.png');
                }
            }
            
            if ($relationship) {
                $reslt->Where('user_details.relationship', $relationship);
            }
                
            $users = $reslt->where('users.role_type', 3)->Paginate(30);
            
            //$users['gallery_images'] = Gallery_image::where('user_id',$user->id)->get();
            
            foreach($users as $k=>$u){
                $users[$k]->gallery_images = Gallery_image::where('user_id',$u->id)->get();    
            }
            //echo "<pre>"; print_r($users); die;            
            $roles = Roles::all();

        $js_file = [];

        return view('admin::users.grid.index', compact('js_file', 'roles', 'country', 'users', 'page_title', 'page_action', 'roles', 'role_type', 'age')); 
    }
    
    public function search1(User $user, Request $request)
    {
        //echo $request->ajax('id');        
            $country      = $request->get('country');
            $age      = $request->get('age');
            $gender      = $request->get('gender');
            $status      = $request->get('status');
            
            $users = User::where(function ($query) use ($country,$age,$gender,$status) {
                if (!empty($country)) {
                    $query->Where('country', 'LIKE', "%$country%");                        
                }

                if (!empty($age)) {                                        
                    //$query->Where('age', $age);
                }

                if ($gender) {
                    $query->Where('gender', $gender);
                }
                
                if (!empty($status)) {
                    $status = ($status == 'yes') ? '1' : '0';
                    $query->Where('status', $status);
                }
            })->where('role_type', 3)->Paginate(30);
            
            foreach($users as $k=>$u){
                $users[$k]['gallery_images'] = Gallery_image::where('user_id',$u->id)->get();    
            }
            
            
            //echo "<pre>"; print_r($users); die;            
            $roles = Roles::all();

        $js_file = [];

        return view('admin::users.grid.index', compact('js_file', 'roles', 'country', 'users', 'page_title', 'page_action', 'roles', 'role_type')); 
    }
    
    public function delete_image(User $user, Request $request, Gallery_image $gallery_image)
    {
        //echo $request->ajax('id');        
            $id           = $request->get('id');
            $userid           = $request->get('userid');
            DB::table('gallery_images')->where('id', '=', $id)->delete();
            return Redirect::to(url('admin/grid/'.$userid.'/edit'))
                ->with('flash_alert_notice', 'Image successfully deleted.');
            exit();        
    }
    
    public function setProfileImage(User $user, Request $request, Gallery_image $gallery_image)
    {        
            $id           = $request->get('id');
            $userid           = $request->get('userid');
            
            $user = DB::table('users')->where('id', $userid)->first();            
            $old_image = $user->profile_image;
            
            $gallery = DB::table('gallery_images')->where('id', $id)->first();            
            $new_image = $gallery->gallery_image;
            
            DB::table('users')
            ->where('id', $userid)
            ->update(array('profile_image' => $new_image));
            
            DB::table('gallery_images')
            ->where('id', $id)
            ->update(array('gallery_image' => $old_image));
                        
            return Redirect::to(url('admin/grid/'.$userid.'/edit'))
                ->with('flash_alert_notice', 'Image Set as Profile successfully ');
            exit();        
    }
    
    public function importExcel(Request $request)
    {
        
        $request->validate([
            'import_file' => 'required'
        ]);
 
        $path = $request->file('import_file')->getRealPath();
        $data = Excel::load($path)->get();
        //print_r($data); die;
        if($data->count()){
            foreach ($data as $key => $value) {
                echo '<pre>';
                //print_r($value);
            
                /*for users*/
                $gender = $value->gender;
                if(!empty($gender))
                {
                    if($gender == 'male')
                    {
                        $gender_val = '1';
                    }
                    else if($gender == 'female')
                    {
                        $gender_val = '2';
                    }
                }

                if(!empty($value->dob) && $value->dob != 'NULL')
                {
                    $from = new DateTime($value->dob);
                    $to   = new DateTime('today');
                    $age = $from->diff($to)->y;
                }
                else{
                    $age = '';
                }

                $users = array(
                    'name' => $value->name,//$value->name,//$value->f_name.' '.$value->l_name, 
                    'email' => $value->email,
                    'password' => bcrypt($value->password),
                    'phone' => (!empty($value->phone) && $value->phone != 'NULL') ? $value->phone : '',
                    'gender' => $gender_val,
                    'dob' => date('Y-m-d', strtotime($value->dob)),
                    'age' => $age,  
                    'country' => $value->country,
                    'city' => $value->city,
                    'address' => (!empty($value->address) && $value->address != 'NULL') ? $value->address : '',
                    'profile_image' => $value->profile_image,
                    'cover_image' => $value->cover_image,
                    'profile_type' => $value->profile_type,
                    'appearance' => $value->appearance,
                    'adult_level' => $value->adult_level,
                    'is_update' => 0,//$value->is_update,
                    'is_login' => 0,//$value->is_login,
                    'last_login' => date('Y-m-d H:i:s'),//$value->last_login,
                    'status' => 1,//$value->status,
                    //'created_at' => date('Y-m-d H:i:s', strtotime($value->created_at)),
                    'updated_at' => date('Y-m-d H:i:s')

                );

                $user_id = DB::table('users')->insertGetId($users);
                //$user_id = 1;
                /*for users*/
                
                /*for user details*/
                if(!empty($value->kids))
                {
                    $kids = ($value->kids == 'Yes') ? '1' : '0';
                }

                $smoke = $value->smoke;
                if(!empty($smoke))
                {
                    if($smoke == 'Never')
                    {
                        $smoke_val = '1';
                    }
                    else if($smoke == 'Regularly')
                    {
                        $smoke_val = '2';
                    }
                    else if($smoke == 'Occasionally')
                    {
                        $smoke_val = '3';
                    }
                }

                $drink = $value->drink;
                if(!empty($drink))
                {
                    if($drink == 'Never')
                    {
                        $drink_val = '1';
                    }
                    else if($drink == 'Regularly')
                    {
                        $drink_val = '2';
                    }
                    else if($drink == 'Occasionally')
                    {
                        $drink_val = '3';
                    }
                }

                $relationship = $value->relationship;
                if(!empty($relationship))
                {
                    if($relationship == 'Never married')
                    {
                        $relationship_val = '1';
                    }
                    else if($relationship == 'Separated')
                    {
                        $relationship_val = '2';
                    }
                    else if($relationship == 'Divorced')
                    {
                        $relationship_val = '3';
                    }
                }

                $user_details = array(
                    'user_id' => $user_id, 
                    'seeking' => $value->seeking,
                    'about_me' => $value->about_me,
                    'about_partner' => $value->about_partner,
                    'work_as' => $value->work_as,
                    'education' => $value->education,
                    'relationship' => $relationship_val,
                    'kids' => $kids,
                    'smoke' => $smoke_val,
                    'drink' => $drink_val,
                    'height' => $value->height,
                    'updated_at' => date('Y-m-d H:i:s')
                );

                DB::table('user_details')->insertGetId($user_details);
                /*for user details*/

                /*for interests*/
                $interests = strtolower($value->interests);
                if(!empty($interests))
                {
                    if (strpos($interests, ';') === false) {
                        $interests_id = DB::table('interests')->where('name', '=', $interests)->first();
                        $interests_val = (!empty($interests_id)) ? $interests_id->id : '';
                    }
                    else{ 
                        $interests_arr = explode(';',$interests);
                        $interests_id_arr = array();
                        foreach($interests_arr as $val)
                        {
                            $interests_id = DB::table('interests')->where('name', '=', trim($val))->first();
                            $interests_id_arr[] = (!empty($interests_id)) ? $interests_id->id : '';
                        }
                        
                        $interests_val = (!empty($interests_id_arr)) ? implode(',',$interests_id_arr) : '';
                    }
                    
                }
                
                $user_interests = array(
                    'user_id' => $user_id, 
                    'interests' => $interests_val,
                    'updated_at' => date('Y-m-d H:i:s')
                );

                DB::table('user_interests')->insertGetId($user_interests);

                /*for interests*/


                /*for languages*/
                $languages = strtolower($value->languages);
                if(!empty($languages))
                {
                    if (strpos($languages, ';') === false) {
                        $languages_id = DB::table('languages')->where('name', '=', $languages)->first();
                        $languages_val = (!empty($languages_id)) ? $languages_id->id : '2';
                    }
                    else{ 
                        $languages_arr = explode(';',$languages);
                        $languages_id_arr = array();
                        foreach($languages_arr as $val)
                        {
                            $languages_id = DB::table('languages')->where('name', '=', trim($val))->first();

                            $languages_id_arr[] = (!empty($languages_id)) ? $languages_id->id : '2';

                        }
                        
                        $languages_val = (!empty($languages_id)) ? implode(',',$languages_id_arr) : '';
                    }
                    
                }
                
                $user_languages = array(
                    'user_id' => $user_id, 
                    'languages' => $languages_val,
                    'updated_at' => date('Y-m-d H:i:s')
                );
                

                DB::table('user_languages')->insertGetId($user_languages);
                /*for languages*/

                /*for gallery images*/
                $images = $value->gallery_image;
                
                if(!empty($images))
                {
                    if (strpos($images, ';') === false) {
                        $images_val = (!empty($images)) ? $images : '';
                            
                        $gallery_images = array(
                                'user_id' => $user_id, 
                                'gallery_image' => $images_val,
                                'updated_at' => date('Y-m-d H:i:s')
                            );

                        DB::table('gallery_images')->insertGetId($gallery_images);
                    }
                    else
                    { 
                        $images_arr = explode(';',$images);
                       
                        foreach($images_arr as $val)
                        {
                            $images_val = (!empty($val)) ? $val : '';
                            
                            $gallery_images = array(
                                'user_id' => $user_id, 
                                'gallery_image' => $images_val,
                                'updated_at' => date('Y-m-d H:i:s')
                            );

                            DB::table('gallery_images')->insertGetId($gallery_images);
                        }
                    }
                    
                }


                $private_photos = $value->private_photos;
                if(!empty($private_photos) && ($private_photos != 'NULL'))
                {
                    if (strpos($private_photos, ';') === false) {
                    
                        $private_images_val = (!empty($private_photos) && ($private_photos != 'NULL')) ? $private_photos : ''; 

                        if($private_images_val != '')
                        {
                            $gallery_images_private = array(
                                'user_id' => $user_id, 
                                'gallery_image' => $private_images_val,
                                'updated_at' => date('Y-m-d H:i:s')
                            );

                            DB::table('gallery_images')->insertGetId($gallery_images_private);
                        }
                    }
                    else
                    { 
                        $private_photos_arr = explode(';',$private_photos);
                            foreach($private_photos_arr as $val)
                            {
                                $private_images_val = (!empty($val) && ($val != 'NULL')) ? $val : ''; 

                                $gallery_images_private = array(
                                    'user_id' => $user_id, 
                                    'gallery_image' => $private_images_val,
                                    'is_private' => '1',
                                    'updated_at' => date('Y-m-d H:i:s')
                                );
                                
                                DB::table('gallery_images')->insertGetId($gallery_images_private);
                            }
                    
                    }
                    
                }
                
                /*for gallery images*/

                /*for gallery videos*/
                $videos = $value->gallery_video;
                if(!empty($videos))
                {
                    if (strpos($videos, ';') === false) {
                        
                        $videos_val = (!empty($videos)) ? $videos : '';

                        $gallery_videos = array(
                            'user_id' => $user_id, 
                            'gallery_video' => $videos_val,
                            'updated_at' => date('Y-m-d H:i:s')
                        );

                        DB::table('gallery_videos')->insertGetId($gallery_videos);
                    }
                    else{ 
                        $videos_arr = explode(';',$videos);
                        
                        foreach($videos_arr as $val)
                        {
                            
                            $videos_val = (!empty($val)) ? $val : '';

                            $gallery_videos = array(
                                'user_id' => $user_id, 
                                'gallery_video' => $videos_val,
                                'updated_at' => date('Y-m-d H:i:s')
                            );

                            DB::table('gallery_videos')->insertGetId($gallery_videos);

                        }
                        
                    }
                    
                }

                echo '<pre>';
                //print_r($gallery_videos);

                /*for gallery videos*/
                
            }
            echo 'Insert Record successfully.';
            die;
 
           
        }
        
            
        return back()->with('success', 'Insert Record successfully.');
    }
    
}
