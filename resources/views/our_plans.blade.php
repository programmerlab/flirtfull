@extends('layout.default')
@section('content')
<style>
.payment_table table {
    width: 70% !important;
    border-bottom-color: #ddd !important;
    text-align: center;
}
.payment_table table tbody tr td {
    padding: 8px 0px;
    color: #777;
    text-align: center;
}
</style>
<div class="activity_wrapper">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
			   <div class="activity_tab">
				   	<ul class="tab_menu">
				   		<li class="active" data-show="credits">Credits</li>
				   		<li data-show="diamonds">Diamonds</li>
				   		<li data-show="premium">Premium</li>
				   	</ul>
			   </div> 
			   <div class="tab_content_wrapper active_accordion">
			   	   <!-- tab Start -->
			   	   <div class="tab_content active" id="credits">
			   	   	  <div class="tab_heading">Credits<span class="tab_btn">+</span></div>
					   <div class="tab_content_inner">
					   	 <div class="payment_table table-responsive">
					   	 	<table class="table">
								<thead>
									<tr>
										<td>Credits</td>
										<td>Package Price</td>
										<td>Price Per Message</td>
										<td>discount</td>
										<td>Free Premium</td>
									</tr>
								</thead>
					   	 		<tbody>
									<?php 
										if(!$credits->isEmpty())
										{	
											
											//type = 4 credits
											foreach($credits as $credit)
											{
												$qty 			= $credit->qty;	
												$discount 		= $credit->discount;		
												$price 			= $credit->price;		
												$transaction_fee= $credit->txn_fee;		
									?>
									<tr>
										<td>{{ $qty }}</td>
										<td>&pound; {{ $price }}</td>
										<td>&pound; {{ round(($credit->price/$credit->qty),2) }}</td>
										<td>{{ $discount }}%</td>
										<td>{{ ($credit->free_premium) ? $credit->free_premium.' Days' : '' }}</td>
									</tr>
									<?php 	}
										} 
										else 
										{
										?>
									<tr>
										<td colspan="3"><center>NO CREDITS</center></td>
									</tr>
										<?php } ?>
								</tbody>
					   	 	</table>
							<p>The payment service provider is charging additional transaction costs or &pound; 0.5 per transaction.</p>
							<p>
							<strong>Pricing conditions</strong><br/>
							1. All payments are a one-off and non-reoccurring<br/>
							2. All prices are including VAT<br/>
							3. The cooling-off periode laid down in article 7:46C par 1DCC cannot be claimed for this entertainment service<br/>
							4. 1 credit is worth 3 Diamonds
							</p>
						 </div>
					   </div>
				   </div>
				   <!-- tab End -->
				    <!-- tab Start -->
			   	   <div class="tab_content" id="diamonds">
			   	   	  <div class="tab_heading">Diamonds<span class="tab_btn">+</span></div>
					   <div class="tab_content_inner">
					   	 <div class="payment_table table-responsive">
					   	 	<table class="table">
								<thead>
									<tr>
										<td>Diamonds</td>
										<td>Credits</td>
										<!--<td>Price Per Diamond</td>-->
										<!--<td>discount</td>-->
									</tr>
								</thead>
					   	 		<tbody>
									<?php 
										if(!$diamonds->isEmpty())
										{	
											//type = 4 credits
											foreach($diamonds as $diamond)
											{
												$qty 			= $diamond->qty;	
												$price 			= $diamond->price;	
												$discount 		= $diamond->discount;			
												$transaction_fee= $diamond->txn_fee;		
									?>
									<tr>
										<td>{{ $qty }}</td>
										<td>{{ $price }}</td>
										<!--<td>&pound; {{ $price }}</td>-->
										<!--<td>{{ $discount }}%</td>-->
									</tr>
									<?php 	}
										} 
										else 
										{
										?>
									<tr>
										<td colspan="3"><center>NO DIAMONDS</center></td>
									</tr>
										<?php } ?>
								</tbody>
					   	 	</table>
							<p>
							<strong>Pricing conditions</strong><br/>
							1. Diamonds can only be converted from credits<br/>
							2. 1 credit is worth 3 Diamonds<br/>
							3. Diamonds can be used to send fantasy proposals
							</p>
					   	 </div>
					   </div>
				   </div>
				   <!-- tab End -->
				   <!-- tab Start -->
			   	   <div class="tab_content" id="premium">
			   	   	    <div class="tab_heading">Premium<span class="tab_btn">+</span></div>
						<div class="tab_content_inner">
					   	 <div class="payment_table table-responsive">
					   	 	<table class="table">
								<thead>
									<tr>
										<td>Month</td>
										<td>Package Price</td>
										<td>Price Per Month</td>
										<td>Discount</td>
										<td>Free credits Per Month</td>
									</tr>
								</thead>
					   	 		<tbody>
									<?php 
										if(!$premiums->isEmpty())
										{	
											//type = 4 credits
											foreach($premiums as $premium)
											{
												$month 			= $premium->month;
												$qty 			= $premium->qty;		
												$discount 		= $premium->discount;		
												$price 			= $premium->price;		
												$transaction_fee= $premium->txn_fee;		
									?>
									<tr>
										<td>{{ $month }}</td>
										<td>&pound; {{ $price }}</td>
										<td>&pound; {{ $qty }}</td>
										<td>{{ $discount }}%</td>
										<td>{{ ($premium->free_credits) ? $premium->free_credits.' Credits' : '' }}</td>
									</tr>
									<?php 	}
										} 
										else 
										{
										?>
									<tr>
										<td colspan="3"><center>NO PREMIUMS</center></td>
									</tr>
										<?php } ?>
								</tbody>
					   	 	</table>
							<p>The payment service provider is charging additional transaction costs of &pound; 0.5 per transaction</p>
							<p>
							<strong>Pricing conditions</strong><br/>
							1. All payments are a one-off and non-reoccurring<br/>
							2. All prices are including VAT<br/>
							3. The cooling-off periode laid down in article 7:46C par 1DCC cannot be claimed for this entertainment service<br/>
							4. Premium will give you access to all features of the website, including a set of free credits per month (see table).
							</p>
					   	 </div>
					   </div>
				   </div>
				   <!-- tab End -->
			   </div>
			</div>
		</div>
	</div>
</div>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.css"/>
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.js"></script>
<script>
	//tab menu css
	$(document).ready(function(){
	  //tab menu js script
	  $('.tab_menu li').on('click',function(){
	    $('.tab_menu li').removeClass("active");
	    $(this).addClass("active");
	    var content= $(this).attr('data-show');
	    $(".tab_content").removeClass("active");
	    $("#"+content).addClass("active");
	  });
	  //tab accordion
	  $(".tab_heading").on("click", function(){
	  	$(this).next(".tab_content_inner").slideToggle(300).toggleClass("c_active");
	  	 $(".tab_heading").not(this).next(".tab_content_inner").slideUp(300).removeClass("c_active");
	  
		if ($(".tab_content_inner").hasClass("c_active")) {
		  	$(this).addClass("active");
		  }
		  else if($('.tab_content_inner').not().hasClass("c_active")) { 
			 $("this").removeClass("active");
		}
	  });
	  //payment table
	  $('.payment_tables').DataTable();
	});
</script>
@endsection