@extends('layout.default')
@section('content')

<!-- Chat script -->
<script src="{{ URL::asset('assets/js/emojis/config.js')}}"></script>
<script src="{{ URL::asset('assets/js/emojis/util.js')}}"></script>
<script src="{{ URL::asset('assets/js/emojis/jquery.emojiarea.js')}}"></script>
<script src="{{ URL::asset('assets/js/emojis/emoji-picker.js')}}"></script>
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.11/lodash.core.js"></script>
<script src="https://cdn.socket.io/socket.io-1.0.0.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/1.7.4/socket.io.min.js"></script>-->
<script src="{{ URL::asset('assets/js/libs/moment.js')}}"></script>
<script src="https://rawgit.com/thielicious/selectFile.js/master/selectFile.js"></script>
<div class="modal fade" id="upgrade_myModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title text-center" id="premium_title">Premiumss: Become a premium member and get access to all website features. </h4>
          <h4 class="modal-title text-center" id="credit_title">Credit: Buy credits to send messages and proposals. 
          </h4>    
            @if( isset($users) && ($users->campaign_id == 2) && (time() <= (strtotime($users->profile_activated_at) + (3600 * 24 * 7))) )
            <p id="double_offer">     
            Hurry!! Purchase Credits before <b>{{ date('d-M-Y',(strtotime($users->profile_activated_at) + (3600 * 24 * 7))) }}</b> & you will get double credits.
            </p>
            @endif    
            
        </div>
        <?php
                $helper = new Helper();
                $diamonds = $helper->getAllDiamonds();
                $credits = $helper->getAllCredits();
                $coins = $helper->getAllCoins();
                $premiums = $helper->getAllPremiums();                
                ?>  

        <div class="modal-body">
          <div class="row">
              <div class="col-md-12">
                <div class="flipster_tab menulist">
                  <ul id="active_plan">
                  </ul>
                </div>
                <!-- tab content-->
                <div class="tab_content" id="credit_tab">
                    <div class="wallet_text">
                      <h4>Get <span class="prem_days" id="prem_days">30</span> Days Premium Access for FREE</h4>
                    </div>
                	<div class="flipster">
      		         <ul>
                      @foreach($credits as $credit)
              		  	<li>
              		  		<div class="upgrade_inner ">
                            <div class="upgrade-img">
                              <!-- <img src="{{ asset('public')  }}/thumbnail/nolemoniquep1.jpg"> -->
                              <img src="{{ $credit->image }}" onerror="this.src="{{ asset('public')  }}/thumbnail/nolemoniquep1.jpg">
                               <!-- offer banner -->
                              <div class="credit_bnr">
                                <img src="{{ asset('public')}}/images/icons/offer_banner.png">
                                <div class="credit_bnr_txt">
                                  @if($credit->qty == 15)  
                                  <span class="bnr_txt_1">{{ $v = 7 }}</span>
                                  @endif
                                  @if($credit->qty == 35)  
                                  <span class="bnr_txt_1">{{ $v = 15 }}</span>
                                  @endif
                                  @if($credit->qty == 75)  
                                  <span class="bnr_txt_1">{{ $v = 30 }}</span>
                                  @endif
                                  @if($credit->qty == 120)  
                                  <span class="bnr_txt_1">{{ $v = 45 }}</span>
                                  @endif
                                  @if($credit->qty == 250)  
                                  <span class="bnr_txt_1">{{ $v = 60 }}</span>
                                  @endif
                                  <span class="bnr_txt_2">days free premium</span>
                                </div>
                              </div>
                              <!-- offer banner -->
                            </div>
                            <div class="upgrade_inner_info"><h2>{{ $credit->qty }} <span>Credit</span></h2>
                            <p class="amount_p"><span class="amount"><span class="currency">£</span>{{ $credit->price }}</span></p>
                            <p>£ {{ round(($credit->price/$credit->qty),2) }} / Message</p>
                            <a href="#" class="upgrade-btn" id="credit{{ $credit->id }}" onclick="select_plan('credits',{{ $credit->id }},{{ $v }});">select <i class="fa fa-check"></i></a></div>
                          </div>
                		</li>
                        @endforeach
            		    </ul>
                  </div>
                </div>
                <!-- tab content-->
                <!-- tab content-->
                <div class="tab_content" id="premium_tab">
                  <div class="wallet_text">
                    <h4>See private pictures</h4>
                    <h5>See who visited / liked your profile</h5>
                  </div>
                  <div class="flipster">
                   <ul>
                      <?php $i = 1; ?>
                      @foreach($premiums as $premium)
                      <li>
                        <div class="upgrade_inner">
                            <div class="upgrade-img">
                              <!-- <img src="{{ asset('public')  }}/thumbnail/nolemoniquep1.jpg"> -->
                              <img src="{{ asset('public/thumbnail/prem_'.$i.'.jpg') }}" onerror="this.src="{{ asset('public')  }}/thumbnail/nolemoniquep1.jpg">
                              <!-- offer banner -->
                              <div class="credit_bnr">
                                <img src="{{ asset('public')}}/images/icons/offer_banner.png">
                                <div class="credit_bnr_txt">
                                  <span class="bnr_txt_1">{{ ($i == 1) ? 10 : (($i ==2) ? 15 : 25) }}  </span>
                                  <span class="bnr_txt_2">Free credits <br/>per month</span>
                                </div>
                              </div>
                              <!-- offer banner -->
                            </div>
                            <div class="upgrade_inner_info">
                              <h4><span class="amount"><span class="currency">£</span>{{ $premium->price }}</span></h4>
                              <h2>{{ $premium->month }} <span>Month</span></h2>
                              <h3><span class="currency">£</span>{{ $premium->qty }}/month</h3>
                              <a href="#" class="upgrade-btn" id="premium{{ $premium->id }}" onclick="select_plan('premiums',{{ $premium->id }});">select <i class="fa fa-check"></i></a>
                            </div>
                          </div>
                        </li>
                        <?php $i++; ?>    
                        @endforeach
                    </ul>
                  </div>
                </div>
                <!-- tab content-->
              </div>
            </div>
            <div class="pymnt-form">
                <div class="row">
                    <div class="col-md-5 col-sm-12 payment_right">
                        @if (Auth::check() && Auth::user()->status == 1)  
                        <form id="purchase_credits" action="">
                        @else
                        <form id="" action="">              
                            @endif              
                           <input type="hidden" id="currency" value="GBP">
                           <input type="hidden" id="amount" value="10.00">
                            <div class="row">
                                <div class="col-md-12">
                                  <!-- <div class="form-group">
                                    <input type="text" class="form-control" id="" placeholder="Card number">
                                  </div> -->
                                 </div>
                               </div>                       
                            <div class="pay_radio">
                               <input type="hidden" name="plan_type" id="plan_type">    
                               <input type="hidden" name="plan" id="plan" >
                               <input type="hidden" id="payment_method" name="payment_method" value="flexpay">
                               <!--<div class="selectbox">
                                 <select id="paynl_id" name="paynl_id"></select>
                               </div>-->
                            </div>
                	          <span id="plan_error" style="display:none; color:red;">Please Select Plan</span>
                             @if (Auth::check() && Auth::user()->status == 1)  
                            <button  class="btn btn-default" id="purchase_credit">Purchase</button>
                            @else
                            <button class="btn btn-default" data-toggle="modal" data-target="#notverifyModal">Purchase</button>    
                            @endif
                            
                        </form>
                            
    			         	</div>
                    <div class="col-md-7 col-sm-12 col-xs-12">
                      <ul>
                          <li><a href="#"><img src="{{ asset('public')  }}/images/visa.png"></a></li>                            
                            <li><a href="#"><img src="{{ asset('public')  }}/images/master_card.png"></a></li>
                            <li><a href="#"><img src="{{ asset('public')  }}/images/amex.png"></a></li>
                            <li><a href="#"><img src="{{ asset('public')  }}/images/paypal.png"></a></li>    
                        </ul>
                        <p>All payments are done to and processed by Oddigits BV. To protect your privacy, your bank statement will say: "DTN Oddigits". All prices include 25% VAT.</p>
                        <p id="premium_footer">We will not renew your subscription or rebill your card.</p>
                        
                    </div>
                    <!--<button  class="btn btn-default" id="purchase_credi">Purchase</button>    -->
                </div>
              
                </div>
			<div class="row text-center">
				<div class="pypl-btn" id="paypal">
					<button type="button"><span class="pay">Pay</span><span class="pal">Pal</span></button>
				</div>
			  </div>
		</div>
                    
                    
            </div>
			
        </div>
        
      </div>
    </div>

    <!-- chat notification -->
    <div class="chat_notifi_box_wrap open">
      <div class="chat_notifi_box">
        <div class="img_wrapper">
          <img src="https://moneoangels.com/public/images/profile_image/crop_1556187992.png" alt="profile_img">
        </div>
        <div class="ct_info_wrapper">
          <div class="title_head">
            <h4>MarinaBeauty, <span class="age">27</span></h4>
            <span class="active_status online">
              <i class="fa fa-circle"></i>
            </span>
          </div>
          <p>hii i like your profile. Check mine</p>
          <a href="#" class="noti_chat_btn"><i class="fa fa-commenting-o"></i>Reply</a>
        </div>
      </div>
    </div>
    <!-- chat notification -->
<script type="text/javascript">
$(window).on('load',function(){
    var payment_status = '<?php echo (Session::has( 'payment_status' )) ? Session::get( 'payment_status' ) : ""; ?>';
    var add_diamonds = '<?php echo (Session::has( 'add_diamonds' )) ? Session::get( 'add_diamonds' ) : ""; ?>';
    if(payment_status != "" || add_diamonds != '')
    {
      $('#payment_status').modal('show');
    }

    //for active plan
    $('#upgrade_myModal').on('show.bs.modal', function(e) {
      var active_plan = $(e.relatedTarget).data('plan');
      //alert(active_plan);
      if(active_plan == 'premium')
      {
        $('#premium_title').show();
        $('#premium_footer').show();
        $('#credit_title').hide();
        $('#double_offer').hide();
        $('#active_plan li:nth-child(1)').show();
        $('#active_plan li:nth-child(1)').addClass('active');
        $('#active_plan li:nth-child(2)').hide();
        
        $('#premium_tab').addClass('active');
        $('#credit_tab').removeClass('active');
      }
      if(active_plan == 'credit')
      {
        $('#premium_title').hide();
        $('#premium_footer').hide();
        $('#credit_title').show();
        $('#double_offer').show();
        $('#active_plan li:nth-child(2)').show();
        $('#active_plan li:nth-child(2)').addClass('active');
        $('#active_plan li:nth-child(1)').hide();

        $('#credit_tab').addClass('active');
        $('#premium_tab').removeClass('active');
      }
    });
});
</script>
@endsection    