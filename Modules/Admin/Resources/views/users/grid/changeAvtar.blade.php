<div class="tab-pane active" id="tab_1_3">  
<div class="portlet light bordered">
 
 
<input type="hidden" name="tab" value="avtar">
    <div class="form-group">
         
        <div class="col-md-12">
        <div class="form-group  {{ $errors->first('profile_image', ' has-error') }}">
            <label class="control-label col-md-2"> Profile Image <span class="required"> * </span></label>
            <div class="col-lg-6">
                @if(isset($user->profile_image))
                <div class="" >
                @if($user->fake == 0)
                    <img src=" {{ (URL::asset('public/images/profile_image/'.$user->profile_image)) ?? 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image'}}" alt="" height="200px" width="150px" onerror="this.src='{{ asset("public/images/profile_image/no_image.jpg") }}'">                                                        
                @else
                    <img src=" {{ (env('PROFILE_MANAGER_ADDR').('/public/images/profile_image/'.$user->profile_image)) ?? 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image'}}" alt="" height="200px" width="150px" onerror="this.src='{{ asset("public/images/profile_image/no_image.jpg") }}'">                                                        
                @endif                 
                </div>
                <span class="label label-{{ ($user->profile_image_status==1)?'success':'warning'}} status" id="{{$user->id}}"  data="{{$user->profile_image_status}}"  onclick="changeStatus({{$user->id}},'profile_image_status')" >
                    {{ ($user->profile_image_status==1)?'Active':'Inactive'}}
                </span>    
                @endif                
                <!--<input type="file" class="file-input" name="profile_image">-->
                 <span class="help-block" style="color:#e73d4a">{{ $errors->first('profile_image', ':message') }}</span>
            
            </div>
        </div>
    </div>
        
    <div class="form-group">
         
        <div class="col-md-12">
        <div class="form-group  {{ $errors->first('cover_image', ' has-error') }}">
            <label class="control-label col-md-2"> Cover Image <span class="required"> * </span></label>
            <div class="col-lg-6">
                @if(isset($user->cover_image))
                <div class="" >
                <img src=" {{ (URL::asset('public/images/cover_img/'.$user->cover_image)) ?? 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image'}}" alt="" height="200px" width="400px" onerror="this.src='{{ asset("public/images/profile_image/no_image.jpg") }}'">                                                        
                </div>
                <span class="label label-{{ ($user->cover_image_status==1)?'success':'warning'}} status" id="{{$user->id + 1}}"  data="{{$user->cover_image_status}}"  onclick="changeStatus({{$user->id + 1 }},'cover_image_status')" >
                    {{ ($user->cover_image_status==1)?'Active':'Inactive'}}
                </span>    
                @endif                
                <!--<input type="file" class="file-input" name="profile_image">-->
                 <span class="help-block" style="color:#e73d4a">{{ $errors->first('cover_image', ':message') }}</span>
            
            </div>
        </div>
    </div>
        
        
    <div class="form-group">
         
        <div class="col-md-12">
        <div class="form-group  {{ $errors->first('gallery_images', ' has-error') }}">
            <label class="control-label col-md-2"> Gallery Images <span class="required"> * </span></label>
            <div class="col-lg-6">
                @if(isset($user->gallery_images))
                    <div class="row">
                    @foreach($user->gallery_images as $row)                    
                    <div class="col-md-4" >
                    @if($user->fake == 0)
                        <img src=" {{ (URL::asset('public/images/profile_image/'.$row->gallery_image)) ?? 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image'}}" alt="" height="200px" width="150px" onerror="this.src='{{ asset("public/images/profile_image/no_image.jpg") }}'">
                    @else
                        <img src=" {{ (env('PROFILE_MANAGER_ADDR').('/public/images/profile_image/'.$row->gallery_image)) ?? 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image'}}" alt="" height="200px" width="150px" onerror="this.src='{{ asset("public/images/profile_image/no_image.jpg") }}'">                                                        
                    @endif                      
                    <span class="label label-{{ ($row->status==1)?'success':'warning'}} status" id="{{$row->id}}"  data="{{$row->status}}"  onclick="changeStatus({{$row->id}},'grid')" >
                        {{ ($row->status==1)?'Active':'Inactive'}}
                    </span>
                    </div>                    
                    @endforeach
                    </div>        
                @endif
                
               
                 <span class="help-block" style="color:#e73d4a">{{ $errors->first('gallery_images', ':message') }}</span>
            
            </div>
        </div>
    </div>    
     
    </div>
          
</div>