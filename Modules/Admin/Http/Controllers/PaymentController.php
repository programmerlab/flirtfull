<?php

declare(strict_types=1);

namespace Modules\Admin\Http\Controllers;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Redirect;
use Input;

use Modules\Admin\Models\Payment;
use Route;
use View;
use Session;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Datatables;


/**
 * Class AdminController
 */
class PaymentController extends Controller
{
    /**
     * @var  Repository
     */

    /**
     * Displays all admin.
     *
     * @return \Illuminate\View\View
     */
    public function __construct()
    {
        $this->middleware('admin');
        View::share('viewPage', 'Payment');
        View::share('helper', new Helper);
        View::share('heading', 'Payment History');
        View::share('route_url', route('payment'));

        $this->record_per_page = 10;
    }

    protected $payment;

    /*
     * Dashboard
     * */

    public function index(Payment $payment, Request $request)
    {        
        $page_title  = 'Payments';
        $page_action = 'Payments';
        

        if ($request->ajax()) {
            $id           = $request->get('id');
            $status       = $request->get('status');
            $payment         = Payment::find($id);
            $s            = ($status == 1) ? $status=0:$status=1;
            $payment->status = $s;
            $payment->save();
            echo $s;

            exit();
        }
        
        $payments = DB::table('credit_payment')->leftjoin('users','credit_payment.user_id','=','users.id')->select('credit_payment.*','users.name')->orderBy('credit_payment.id', 'desc')->Paginate($this->record_per_page);
        foreach($payments as $key => $res)
        {
            $get_plan = DB::table($res->plan_type)->where('id',$res->plan_id)->first();

            $payments[$key]->qty = $get_plan->qty;
            $payments[$key]->status_class = 'primary';
            if($res->status == 'success')
            {
                $payments[$key]->status_class = 'success';
            }  
            if($res->status == 'cancelled')
            {
                $payments[$key]->status_class = 'danger';
            }   
            if($res->status == 'expired')
            {
                $payments[$key]->status_class = 'primary';
            }
            //echo $res->status;
        }      
        //print_r($payments); die;  

        $js_file = ['common.js','bootbox.js','formValidate.js'];

        return view('admin::payments.payment.index', compact('js_file', 'status', 'payments', 'page_title', 'page_action'));
    }


    
    
    
}
