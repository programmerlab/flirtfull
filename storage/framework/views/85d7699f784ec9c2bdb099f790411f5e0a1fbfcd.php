 <div class="nw-profiles">
<div class="nw-profiles-otr">
  <div style="display: none" class="text-center load-more"><img width="50px" src="<?php echo e(asset('public/images/loader.gif')); ?>"></div>
 <?php if(!empty($all_users) && count($all_users)>0): ?>
 <ul>
              
<?php $__currentLoopData = $all_users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $keys => $single): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
  <?php
    $rand = rand(1,3);
    $name = strlen($single->name) > 8 ? substr($single->name,0,8)."..." : $single->name;
    
    if( $single->fake ){

        $profileimage = env('PROFILE_MANAGER_ADDR').'/public/thumbnail'.'/'.$single->profile_image;
       // $profileimage = 'https://moneoangels.com'.'/public/thumbnail'.'/'.$single->profile_image;
    }else{
        $profileimage = asset('public/thumbnail').'/'.$single->profile_image;
    }
    //echo env('PROFILE_MANAGER_ADDR');
    if (($single->profile_image_status == 1)) 
    { 
      $profile_img = $profileimage; //asset('public/thumbnail') .'/'. $single->profile_image ;
    }
    elseif($single->provider_name == 'google' && ($single->profile_image_status == 1))
    {
      $profile_img = $single->profile_image;
    }
    else{  
      if($single->gender == '1')
      {
        $profile_img = asset('public/images/male_icon.png');
      }
      else{

        $profile_img = asset('public/images/lady_icon.png');
      }
    }

      $auth_id = Auth::id();
      $user_id = $single->user_id;
      DB::enableQueryLog();
    $liked = DB::table('chat_room')
             ->where('status', '=', '1')
            ->where(function ($query1) use($user_id) {
                $query1->where('sender_id', '=', $user_id)
                      ->orWhere('receiver_id', '=', $user_id);
            })
            ->where(function ($query) use($auth_id) {
                $query->where('sender_id', '=', $auth_id)
                      ->orWhere('receiver_id', '=', $auth_id);
            })->count();
	$likes = DB::table('likes')
             ->where('type', '=', '1')
             ->where('sender_id', '=', $auth_id)
             ->where('receiver_id', '=', $user_id)->count();
            							
           // dd(DB::getQueryLog());

  ?>
  
<?php  
	require_once base_path().'/mobile-detect-api/Mobile_Detect.php';;
	$detect = new Mobile_Detect();

	// Check for any mobile device or desktop.
	if ( $detect->isMobile() )		{ $screen = 0; }  
	elseif( $detect->isTablet() )	{ $screen = 0; }
	else 							{ $screen = 7; }
  
  if($keys == $screen && (Auth::check()) && Auth::user()->status == 0) { ?>
  
  <li>
    <a href="#">
      <div class="prfl-otr not_verif_otr">
        <div class="prfl-img">          
          <div class="ver_text">
          	An activation link is sent to <u><?php echo e(Auth::user()->email); ?></u>
          </div>
        </div>
        <div class="prfl-img-cnt">
          <p>Please Activate your account to proceed</p>
		      <span id="resend_email"><span onclick="resend_verification('<?php echo e(Auth::id()); ?>')" >Resend Email</span></span>
        </div>
      </div>
    </a>
  </li>
  <?php }  ?>
  <li>
    <a href="<?php echo e(url('profile')); ?>/<?php echo e(($single->user_id) ? ($single->user_id) : $single->id); ?>">
      <div class="prfl-otr">
        <div class="prfl-img">
								  <?php if($likes): ?>
          <div class="liked_profile"><i class="fa fa-heart"></i></div>
										<?php endif; ?>	
          <?php if($liked): ?>
          <div class="liked"><i class="fa fa-star"></i></div>
          <?php endif; ?>
          <img src="<?php echo e($profile_img); ?>">
        </div>
        <div class="prfl-img-cnt">
          <h4><?php echo ucwords($name); ?>,<span class="age"><?php echo e($single->age); ?></span><span class="conn-status"><i class="fa fa-video-camera" aria-hidden="true"></i></span></h4>
            <p><?php echo e(substr($single->state,0,45)); ?></p>
          <?php if($single->is_login): ?>
                <div class="search_status online"><i class="fa fa-circle"></i> </div>
          <?php else: ?>
              <?php if($rand == 2): ?>
                  <?php
                  $helper = new Helper();
                  if($single->fake){
                    $helper->updateUserLogin($single->id);
                  }
                  
                  ?>
                  <div class="search_status online"><i class="fa fa-circle"></i></div>
              <?php else: ?>
                  <div class="search_status offline"><i class="fa fa-circle"></i></div>
              <?php endif; ?>                  
          <?php endif; ?>
          
        </div>
      </div>
    </a>
  </li>
  
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
     </ul>
     <?php else: ?>
       <div class="text-center load-more">Oops, your search criteria does not match any members yet.</div>
     <?php endif; ?>
   </div>
 </div>
 <div style="display: none" class="text-center load-more"><img width="50px" src="<?php echo e(asset('public/images/loader.gif')); ?>"></div>
<div class="flirt_pagination"><?php echo $all_users->appends(['seeking'=>$seeking,'min_age'=>$min_age,'max_age'=>$max_age,'state'=>$state,'online'=>$online])->links('custom-pagination'); ?></div>
