<div class="wait"> loading... <img width="50px" src="{{ asset('public/images/loader.gif') }}"></div>
<div class="all_userss" style="display: none">
 <div class="nw-profiles">
<div class="nw-profiles-otr">
 @if(!empty($all_users) && count($all_users)>0)
 <ul>
              
@foreach($all_users as $keys => $single)
  <?php
    $rand = rand(1,3);
    $name = strlen($single->name) > 8 ? substr($single->name,0,8)."..." : $single->name;
    
    if( $single->fake ){
        $profileimage = env('PROFILE_MANAGER_ADDR').'/public/thumbnail'.'/'.$single->profile_image;
    }else{
        $profileimage = asset('public/thumbnail').'/'.$single->profile_image;
    }
    
    if (($single->profile_image_status == 1)) 
    { 
      $profile_img = $profileimage; //asset('public/thumbnail') .'/'. $single->profile_image ;
    }
    elseif($single->provider_name == 'google' && ($single->profile_image_status == 1))
    {
      $profile_img = $single->profile_image;
    }
    else{  
      if($single->gender == '1')
      {
        $profile_img = asset('public/images/male_icon.png');
      }
      else{

        $profile_img = asset('public/images/lady_icon.png');
      }
    }

      $auth_id = Auth::id();
      $user_id = $single->user_id;
//      DB::enableQueryLog();
    $liked = DB::table('chat_room')
             ->where('status', '=', '1')
            ->where(function ($query1) use($user_id) {
                $query1->where('sender_id', '=', $user_id)
                      ->orWhere('receiver_id', '=', $user_id);
            })
            ->where(function ($query) use($auth_id) {
                $query->where('sender_id', '=', $auth_id)
                      ->orWhere('receiver_id', '=', $auth_id);
            })->count();
	$likes = DB::table('likes')
             ->where('type', '=', '1')
             ->where('sender_id', '=', $auth_id)
             ->where('receiver_id', '=', $user_id)->count();
            							
           // dd(DB::getQueryLog());

  ?>
  
<?php  
	require_once base_path().'/mobile-detect-api/Mobile_Detect.php';
	$detect = new Mobile_Detect();

	// Check for any mobile device or desktop.
	if ( $detect->isMobile() )		{ $screen = 0; }  
	elseif( $detect->isTablet() )	{ $screen = 0; }
	else 							{ $screen = 7; }
  
  if($keys == $screen && (Auth::check()) && Auth::user()->status == 0) { ?>
  
  <li>
    <a href="#">
      <div class="prfl-otr not_verif_otr">
        <div class="prfl-img">          
          <div class="ver_text">
          	An activation link is sent to <u>{{ Auth::user()->email }}</u>
          </div>
        </div>
        <div class="prfl-img-cnt">
          <p>Please Activate your account to proceed</p>
		      <span id="resend_email"><span onclick="resend_verification('{{ Auth::id() }}')" >Resend Email</span></span>
        </div>
      </div>
    </a>
  </li>
  <?php }  ?>
  <li>
    <a href="{{ url('profile') }}/{{ ($single->user_id) ? ($single->user_id) : $single->id }}">
      <div class="prfl-otr">
        <div class="prfl-img">
								  @if($likes)
          <div class="liked_profile"><i class="fa fa-heart"></i></div>
										@endif	
          @if($liked)
          <div class="liked"><i class="fa fa-star"></i></div>
          @endif
          <img src="{{ $profile_img }}">
        </div>
        <div class="prfl-img-cnt">
          <h4><?php echo ucwords($name); ?>,<span class="age">{{ $single->age }}</span><span class="conn-status"><i class="fa fa-video-camera" aria-hidden="true"></i></span></h4>
            <p>{{ substr($single->state,0,45) }}</p>
          @if($single->is_login)
                <div class="search_status online"><i class="fa fa-circle"></i> </div>
          @else
              @if($rand == 2)
                  <?php
                  $helper = new Helper();
                  if($single->fake){
                    $helper->updateUserLogin($single->id);
                  }
                  
                  ?>
                  <div class="search_status online"><i class="fa fa-circle"></i></div>
              @else
                  <div class="search_status offline"><i class="fa fa-circle"></i></div>
              @endif                  
          @endif
          
        </div>
      </div>
    </a>
  </li>
  
@endforeach
     </ul>
     @else
       <div class="text-center load-more">Oops, your search criteria does not match any members yet.</div>
     @endif
   </div>
 </div>
 <div style="display: none" class="text-center load-more"><img width="50px" src="{{ asset('public/images/loader.gif') }}"></div>
<div class="flirt_pagination">{!! $all_users->render() !!}</div>



</div>

<script type="text/javascript">
  
  $(function(){

    setTimeout(function(){
      $('.all_userss').show();
      $('.wait').hide();

    },2000);

  });
</script>
