</div><!-- main_wrapper -->
<footer class="fl_footer_section footer_second kandy">
  <div class="container">
    <div class="row">
     <div class="col-sm-12">
      <div class="footer_menu">
        <ul>
           <li><a href="{{ url('about') }}">About Us</a></li>
           <li><a href="{{ url('our-plans') }}">Pricing</a></li>
           <li><a href="{{ url('faq') }}">FAQ</a></li>
           <li><a href="{{ url('privacy-policy') }}">Privacy Policy</a></li>
           <li><a href="{{ url('terms-conditions') }}">Terms &amp; Conditions</a></li>
        </ul>
      </div>
      <div class="footer_text">
        <p>
            <span class="full_width">The minimum age for participation on flirtfull.com is 18 years.</span>
            Flirtfull.com is an adult social flirting platform for men and women who are looking for a fun, flirty or exciting contact. Every day, hundreds of new members sign up. Based on your profile settings, you will receive match suggestions. You can also use our search functionality and browse for profiles yourself. Flirtfull.com is designed for adult pleasure and entertainmentand optimised for desktops, mobile phones and tablets.Profiles are partly fictional, physical arrangements with these angel profiles are not possible. We strongly advise you to read our Terms and Conditions before using our Service.
        </p>
      </div>
      <div class="footer_copyright">
        <p><a href="https://www.flirtfull.com/">Flirtfull.com</a> © {{ date('Y') }} . All Rights Reserved</p>
      </div>
     </div>
    </div>
  </div>
</footer> 

<!-- NEW LOGIN SIGNUP POPUP BY ARJUN -->
<!--splash modal Start -->
<style>.rrr{ color:#880000 !important; }</style>
      <div class="splash_screen_modal modal_main">
         <div class="popup_modal_dialog">
            <div class="splash_content_bg">
               <div class="s_overlay"></div>
               <div class="splash_content">
                  <div class="splash_form">
                      <div class="splash_heading">
                        <span class="splash_icon">
                          <img src="https://www.flirtfull.com/public/images/splash/splash_logo.png" alt="">
                        </span>
                        <h2>Welcome to Flirtfull</h2>
                      </div>
                     <div class="login_sign_links">
                        <button type="button" class="log_sign_btn popup_btn" data-show="s_login_popup">Login</button>
                        <button type="button" class="log_sign_btn popup_btn" data-show="s_signup_popup">Sign Up</button>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!--splash modal End -->

     <!-- signup  modal Start -->
      <div id="s_signup_popup" class="splash_screen_modal">
         <div class="popup_modal_dialog">
            <div class="splash_content_bg">
               <div class="s_overlay"></div>
               <div class="splash_content">
                  <div class="splash_form">
                     <h2>Please create an account</h2>
                     <form class="" method="get" action="{{ url('/') }}" id="signup_modal">
                        <div class="form_group text-center">
                           <div class="radio_group">
                                <label class="control-label">I Am A</label>
                                  <div>
                                  <span class="radio-btn">
                                    <input type="radio" name="gender" id="malee" value="1" class="gender_classs" checked="" {{ (isset($_GET['gender']) && $_GET['gender'] == 1) ? 'checked' : '' }}> 
                                    <label for="malee"><img src="{{ asset('public/images/male.png') }}"></label>
                                  </span>
                                  <span class="radio-btn">
                                    <input type="radio" name="gender" id="femalee" value="2" class="gender_classs" {{ (isset($_GET['gender']) && $_GET['gender'] == 2) ? 'checked' : '' }}> 
                                    <label for="femalee"><img src="{{ asset('public/images/female.png') }}"></label>
                                  </span>
								  <p class="error-message gender rrr"></p>
                                  </div>
                              </div>
                              <div class="radio_group">
                                  <label class="control-label">Seeking A</label>
                                  <div>
                                    <span class="radio-btn">
                                    <input type="radio" id="s-malee" name="seeking" value="1" class="seeking_classs" {{ (isset($_GET['seeking']) && $_GET['seeking'] == 1) ? 'checked' : '' }}> 
                                    <label for="s-malee"><img src="{{ asset('public/images/male.png') }}"></label>
                                  </span>
                                  <span class="radio-btn">
                                    <input type="radio" name="seeking" id="s-femalee" value="2" class="seeking_classs" checked="" {{ (isset($_GET['seeking']) && $_GET['seeking'] == 2) ? 'checked' : '' }}> 
                                    <label for="s-femalee"><img src="{{ asset('public/images/female.png') }}"></label>
                                  </span>
								  <p class="error-message seeking rrr"></p>
                                  </div>
                              </div>
                        </div>
                        <div class="form_group">
                           <label class="label">Email</label>
                           <div class="input_group">
                              <input id="signup_email" type="email" placeholder="Enter Your Email"/>
                              <p class="signup-email-wrong error-message rrr"></p>
						   </div>
                        </div>
                        <div class="form_group">
                           <label class="label">Password</label>
                           <div class="input_group">
                              <input id="signup_password" type="password" name="password" placeholder="Enter Your Password"/>
							  <p class="signup-password-wrong error-message rrr"></p>	
						   </div>
                        </div>
                        <div class="form_group birthday_select">
                            <label class="label">Age</label>
                            <div class="input_group">
                                <select id="year">
                                  <option value="" selected="selected">Select Year Of Birth</option>
									<?php 
									  for($i=2001;$i>=1930;$i--)
									  {
										echo '<option value="'.$i.'">'.$i.'</option>';
									  }
									?>
                                </select>
							 <p class="error-message birth_date rrr"></p>
                           </div>
                        </div>
                        <div class="form_group">
                           <div class="check_box">
                              <label>
                              <input type="checkbox" id="accept_cond" value="1"/>
                                 <span class="c_box"></span>
                                 <span class="c_text">I accept the <a target="_blank" href="{{ url('terms-conditions') }}">Terms & Conditions</a> </span>
                                 <input type="hidden" name="partner_id" id="partnerid" value="{{ isset($_GET['partner']) ? $_GET['partner'] : '' }}">    
                              </label>
                           </div>
						   <p class="error-message accept rrr"></p>
                        </div>
						<p class="signup-process text-center rrr">Processing...</p>
                        <div class="button_group">
                           <input type="submit" name="submit" value="Sign Up" class="form_btn">
                        </div>
                        <div class="form_txt_wrap">
                          <div>
                            <span class="form_txt">Are you already registered</span>
                            <a href="#" class="hide_model popup_btn" data-show="s_login_popup">Login</a>
                          </div>
                        </div>
                     </form>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!--signup modal End -->
      <!-- login  modal Start -->
      <div id="s_login_popup" class="splash_screen_modal">
         <div class="popup_modal_dialog">
            <div class="splash_content_bg">
               <div class="s_overlay"></div>
               <div class="splash_content">
                  <div class="splash_form">
                     <h2>Please login To your account</h2>
                     <form class="" method="get" action="{{ url('search') }}" id="login_modal">
                        <div class="form_group">
                           <label class="label">Email</label>
                           <div class="input_group">
                              <input id="login_email" type="email" placeholder="Enter Your Email">
							  <p class="email-wrong error-message rrr"></p>
						   </div>
                        </div>
                        <div class="form_group">
                           <label class="label">Password</label>
                           <div class="input_group">
                              <input id="login_password" type="password" placeholder="Enter Your Password">
                              <p class="password-wrong error-message rrr"></p>
                              <p class="both-wrong error-message rrr"></p>
						   </div>
                        </div>
                        <div class="form_group remember_area">
                           <div class="check_box">
                              <label>
                              <input type="checkbox" name="remember_me" value="1">
                                 <span class="c_box"></span>
                                 <span class="c_text">Remember me</span>
                              </label>
                           </div>
                           <div class="forget_pass">
                              <a href="#" class="hide_model popup_btn" data-show="forget_popup">Forget password</a>
                           </div>
                        </div>
                         <div class="button_group">
                           <input type="submit" name="submit" value="Login" class="form_btn">
                        </div>
                        <div class="form_txt_wrap">
                          <div>
                            <span class="form_txt">Are you not registered Please</span>
                            <a href="#" class="hide_model popup_btn" data-show="s_signup_popup">
                              Signup Now
                            </a>
                          </div>
                        </div>
                     </form>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!--login modal End -->
      
      <!-- Forget modal Start -->
      <div id="forget_popup" class="splash_screen_modal">
         <div class="popup_modal_dialog">
            <div class="splash_content_bg">
               <div class="s_overlay"></div>
               <div class="splash_content">
                  <div class="splash_form">
                     <h2>Enter your registered Email Id</h2>
                     <form class="" id="forgot">
                        <div class="form_group">
                           <label class="label">Your Email Id</label>
                           <div class="input_group">
                             <input type="email" id="forgot_email" placeholder="Enter Your Email">
							<p class="forgot-email-wrong error-message rrr"></p>
							<p class="forgot-email-success success-message" style="color:#fff;"></p>
						   </div>
                        </div>
						<p style="display: none;color:#fff;" class="forgot-process text-center">Processing...</p>
                        <div class="button_group">
                           <input type="submit" name="submit" value="Submit" class="form_btn">
                        </div>
                     </form>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!--Forget modal End -->
<!-- NEW LOGIN SIGNUP POPUP BY ARJUN END-->




<div id="passwordModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-sm modal-dialog-centered">

    <!-- Modal content-->
    <div class="modal-content">
      
      <div class="modal-body">
        <h3>Congrats! you are successfully registered with us.</h3>
         Below is your system generated password. Save it to login in future. You may change it whenever you want.
         <p><strong>password - {{ (isset($rand) ? $rand : '') }}</strong></p>
      </div>
      <div class="modal-footer">
            <a href="{{ url('/') }}" class="btn btn-primary">Go To Website</a>
      </div>
    </div>

  </div>
</div>


<!-- <div class="modal fade" id="upgrade_myModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title text-center" id="premium_title">Premium: Become a premium member and get access to all website features. </h4>
          <h4 class="modal-title text-center" id="credit_title">Credit: Buy credits to send messages and proposals. </h4>  
                     
            @if( isset($users) && ($users->campaign_id == 2) && (time() <= (strtotime($users->profile_activated_at) + (3600 * 24 * 7))) )
            <p id="double_offer">     
            Hurry!! Purchase Credits before <b>{{ date('d-M-Y',(strtotime($users->profile_activated_at) + (3600 * 24 * 7))) }}</b> & you will get double credits.
            </p>
            @endif    
            
        </div>
        <?php
                $helper = new Helper();
                //$diamonds = $helper->getAllDiamonds();
                $credits = $helper->getAllCredits();
                //$coins = $helper->getAllCoins();
                $premiums = $helper->getAllPremiums();                
                ?>  

        <div class="modal-body">
          <div class="row">
              <div class="col-md-12">
                <div class="flipster_tab menulist">
                  <ul id="active_plan">
                  </ul>
                </div>
                <!-- tab content--
                <div class="tab_content" id="credit_tab">
                    <div class="wallet_text">
                      <h4>Get <span class="prem_days" id="prem_days">30</span> Days Premium Access for FREE</h4>
                    </div>
                	<div class="flipster">
      		         <ul>
                      @foreach($credits as $credit)
              		  	<li>
              		  		<div class="upgrade_inner ">
                            <div class="upgrade-img">                              
                              <img src="{{ $credit->image }}" onerror="this.src="{{ asset('public')  }}/thumbnail/nolemoniquep1.jpg">
                               <!-- offer banner --
                              <div class="credit_bnr">
                                <img src="{{ asset('public')}}/images/icons/offer_banner.png">
                                <div class="credit_bnr_txt">
                                  @if($credit->qty == 15)  
                                  <span class="bnr_txt_1">{{ $v = 7 }}</span>
                                  @endif
                                  @if($credit->qty == 35)  
                                  <span class="bnr_txt_1">{{ $v = 15 }}</span>
                                  @endif
                                  @if($credit->qty == 75)  
                                  <span class="bnr_txt_1">{{ $v = 30 }}</span>
                                  @endif
                                  @if($credit->qty == 120)  
                                  <span class="bnr_txt_1">{{ $v = 45 }}</span>
                                  @endif
                                  @if($credit->qty == 250)  
                                  <span class="bnr_txt_1">{{ $v = 60 }}</span>
                                  @endif
                                  <span class="bnr_txt_2">days free premium</span>
                                </div>
                              </div>
                              <!-- offer banner --
                            </div>
                            <div class="upgrade_inner_info"><h2>{{ $credit->qty }} <span>Credit</span></h2>
                            <p class="amount_p"><span class="amount"><span class="currency">£</span>{{ $credit->price }}</span></p>
                            <p>£ {{ round(($credit->price/$credit->qty),2) }} / Message</p>
                            <a href="#" class="upgrade-btn" id="credit{{ $credit->id }}" onclick="select_plan('credits',{{ $credit->id }},{{ $v }});">select <i class="fa fa-check"></i></a></div>
                          </div>
                		</li>
                        @endforeach
            		    </ul>
                  </div>
                </div>
                <!-- tab content-->
                <!-- tab content--
                <div class="tab_content" id="premium_tab">
                  <div class="wallet_text">
                    <h4>See private pictures</h4>
                    <h5>See who visited / liked your profile</h5>
                  </div>
                  <div class="flipster">
                   <ul>
                      <?php $i = 1; ?>
                      @foreach($premiums as $premium)
                      <li>
                        <div class="upgrade_inner">
                            <div class="upgrade-img">                              
                              <img src="{{ asset('public/thumbnail/prem_'.$i.'.jpg') }}" onerror="this.src="{{ asset('public')  }}/thumbnail/nolemoniquep1.jpg">
                              <!-- offer banner --
                              <div class="credit_bnr">
                                <img src="{{ asset('public')}}/images/icons/offer_banner.png">
                                <div class="credit_bnr_txt">
                                  <span class="bnr_txt_1">{{ ($i == 1) ? 10 : (($i ==2) ? 15 : 25) }}  </span>
                                  <span class="bnr_txt_2">Free credits <br/>per month</span>
                                </div>
                              </div>
                              <!-- offer banner --
                            </div>
                            <div class="upgrade_inner_info">
                              <h4><span class="amount"><span class="currency">£</span>{{ $premium->price }}</span></h4>
                              <h2>{{ $premium->month }} <span>Month</span></h2>
                              <h3><span class="currency">£</span>{{ $premium->qty }}/month</h3>
                              <a href="#" class="upgrade-btn" id="premium{{ $premium->id }}" onclick="select_plan('premiums',{{ $premium->id }});">select <i class="fa fa-check"></i></a>
                            </div>
                          </div>
                        </li>
                        <?php $i++; ?>    
                        @endforeach
                    </ul>
                  </div>
                </div>
                <!-- tab content--
              </div>
            </div>
                
            <div class="pymnt-form">
                <div class="row">
                    <div class="col-md-5 col-sm-12 payment_right">
                        @if (Auth::check() && Auth::user()->status == 1)  
                        <form id="purchase_credits" action="">
                        @else
                        <form id="" action="">              
                            @endif              
                           <input type="hidden" id="currency" value="GBP">
                           <input type="hidden" id="amount" value="10.00">
                            <div class="row">
                                <div class="col-md-12">
                                  <!-- <div class="form-group">
                                    <input type="text" class="form-control" id="" placeholder="Card number">
                                  </div> --
                                 </div>
                               </div>                       
                            <div class="pay_radio">
                               <input type="hidden" name="plan_type" id="plan_type">    
                               <input type="hidden" name="plan" id="plan" >
                               <input type="hidden" id="payment_method" name="payment_method" value="flexpay">
                               <!--<div class="selectbox">
                                 <select id="paynl_id" name="paynl_id"></select>
                               </div>--
                            </div>
                	          <span id="plan_error" style="display:none; color:red;">Please Select Plan</span>
                             @if (Auth::check() && Auth::user()->status == 1)  
                            <button  class="btn btn-default" id="purchase_credit">Purchase</button>
                            @else
                            <button class="btn btn-default" data-toggle="modal" data-target="#notverifyModal">Purchase</button>    
                            @endif
                            
                        </form>
                            
    			         	</div>
                    <div class="col-md-7 col-sm-12 col-xs-12">
                      <ul>
                          <li><a href="#"><img src="{{ asset('public')  }}/images/visa.png"></a></li>                            
                            <li><a href="#"><img src="{{ asset('public')  }}/images/master_card.png"></a></li>
                            <li><a href="#"><img src="{{ asset('public')  }}/images/amex.png"></a></li>
                            <li><a href="#"><img src="{{ asset('public')  }}/images/paypal.png"></a></li>    
                        </ul>
                        <p>All payments are done to and processed by Oddigits BV. To protect your privacy, your bank statement will say: "DTN Oddigits". All prices include 25% VAT.</p>
                        <p id="premium_footer">We will not renew your subscription or rebill your card.</p>
                        
                    </div>
                    <!--<button  class="btn btn-default" id="purchase_credi">Purchase</button>    --
                </div>
              
                </div>
			<div class="row text-center">
				<div class="pypl-btn" id="paypal">
					<button type="button"><span class="pay">Pay</span><span class="pal">Pal</span></button>
				</div>
			  </div>
		</div>
                    
                    
            </div>
			
        </div>
        
      </div>
    </div> -->

 


<div id="payment_status" class="modal fade" role="dialog">
  <div class="modal-dialog modal-sm modal-dialog-centered">

    <!-- Modal content-->
    <div class="modal-content">
      
      <div class="modal-body">
        @if( Session::has( 'payment_status' ))
        <p>Your payment was {{ Session::get( 'payment_status' ) }}.</p>
        @endif
        @if( Session::has( 'add_diamonds' ))
        <p>{{ Session::get( 'add_diamonds' ) }}.</p>
        @endif
      </div>
      <div class="modal-footer">
            <a href="{{ url('/') }}" class="btn btn-primary">Go To Website</a>
      </div>
    </div>

  </div>
</div>

<div id="notverifyModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-sm modal-dialog-centered">

    <!-- Modal content-->
    <div class="modal-content">
      
      <div class="modal-body">        
        <a href="" class="btn btn-danger">Please Verify Your account to get this service.</a>
      </div>
      <div class="modal-footer">
            
      </div>
    </div>

  </div>
</div>    

<!-- Register popup start -->
<div class="popup_modal regisration_modal" style="display:none;" id="passwordModal123">
  <div class="modal_dialog_big">
    <div class="popup_content">
      <div class="reg_form_wrap">
        <div class="form_left_part">
          <div class="reg_img">
            @if($adminid)
            <img src="{{ asset('public/thumbnail/'.$adminid->profile_image) }}">
            @endif
          </div>
          <div class="reg_imgtext">
            <h4>{{ $adminid->name.', '.$adminid->age }} <br> {{ ($adminid->state)?$adminid->state:'' }} </h4>            
          </div>
        </div>
        <div class="form_right_part">
          <h3>Hii, {{ Auth::user()->name }}</h3>
          <p>This is <strong>Kate</strong>, the website admin of Flirtfull.com. Congratulation!!!!! Your Flirtfull account has been activated. We just need three more entries from you!</p>            
          <div class="reg_form">
            <form method="post" id="oform" action="{{ url('/update-outer-user') }}">
              <h4>First lets start with a screen name</h4>
              <div class="form_group">
                <label>Profile Name<span>*</span></label>
                <div class="form_input">
                  <input type="text" name="name" id="oname" placeholder="Enter Your Profile Name">
                  <span id="oname_error" style="display:none; color:red;">This field is required</span>
                  <span id="ouniquename_error" style="display:none; color:red;">This name is already exists, please choose different one.</span>  
                </div>
              </div>
              <h4>For better/proper matches on your plateform please enter you birthday</h4>
               <!-- Row Start -->
              <div class="row">
                <div class="col-sm-4">
                  <div class="form_group">
                    <label>Day<span>*</span></label>
                    <div class="form_input">
                      <select id="oday" name="day" class="" >
                          <option value="" class="label" selected="selected" >day</option>
                          <?php
                            for ($m=1; $m<=31; $m++) 
                            {
                              echo "<option value='".$m."'>".$m."</option>";
                            }
                          ?>
                        </select>
                        <span id="oday_error" style="display:none; color:red;">This field is required</span>    
                    </div>
                  </div>
                </div>
                <div class="col-sm-4">
                  <div class="form_group">
                    <label>Month<span>*</span></label>
                    <div class="form_input">
                      <select id="omonth" name="month" class="" >
                          <option value="" class="label" selected="selected" >month</option>
                          <?php
                            for ($m=1; $m<=12; $m++) 
                            {
                              $month_name = date('F', mktime(0,0,0,$m, 1, date('Y')));
                              echo "<option value='".$m."'>".$month_name."</option>";
                            }
                          ?>
                        </select>
                        <span id="omonth_error" style="display:none; color:red;">This field is required</span>    
                    </div>
                  </div>
                </div>
                <div class="col-sm-4">
                  <div class="form_group">
                    <label>Year<span>*</span></label>
                    <div class="form_input">
                      <select id="oyear" name="year" class="" >
                          <option value="" class="label" selected="selected" >year</option>
                          <?php 
                          for($i=2001;$i>=1930;$i--)
                          {
                            echo '<option value="'.$i.'">'.$i.'</option>';
                          }
                          ?>
                        </select>
                        <span id="oyear_error" style="display:none; color:red;">This field is required</span>    
                    </div>
                  </div>
                </div>
              </div>
              <!-- Row end -->
              <div class="row">
                <div class="col-sm-12">
                  <div class="form_group">
                    <label>Region<span>*</span></label>
                    <div class="form_input">
                        <select id="oregion" name="region" class="" >
                          <option value="" class="label" selected="selected" >Region</option>
                          <?php
                            for ($m=1; $m<=12; $m++) 
                            {
                              $month_name = date('F', mktime(0,0,0,$m, 1, date('Y')));
                              echo "<option value='".$m."'>".$month_name."</option>";
                            }
                          ?>
                        </select>                      
                      <span id="oregion_error" style="display:none; color:red;">This field is required</span>                      
                    </div>
                  </div>
                </div>                
              </div>
              <!-- Row end -->  
              <h4>Please set a password to secure your account</h4>
              <!-- Row start -->
              <div class="row">
                <div class="col-sm-6">
                  <div class="form_group">
                    <label>Enter password<span>*</span></label>
                    <div class="form_input">
                      <input type="password" id="opass" name="password" placeholder="Enter Password">
                      <span id="opass_error" style="display:none; color:red;">This field is required</span>
                      <span id="ominpass_error" style="display:none; color:red;">This field should be minimum of 4 characters</span>  
                      <span id="oconfpass_error" style="display:none; color:red;">Confirm Password doesn't match</span>  
                    </div>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form_group">
                    <label>Verify password<span>*</span></label>
                    <div class="form_input">
                      <input type="password" id="orepass" placeholder="confirm Password">
                      <span id="orepass_error" style="display:none; color:red;">This field is required</span>
                    </div>
                  </div>
                </div>
              </div>
              
              <div class="form-group">
                <div class="check_box">
                  <input type="checkbox" id="oterms" value="1"><span> I accept the <a target="_blank" href="{{ url('terms-conditions') }}">Terms & Conditions</a> &amp; <a target="_blank" href="{{ url('privacy-policy') }}">Privacy Policy</a></span>
                  <span id="oterms_error" style="display:none; color:red;">This field is required</span>  
                </div>
              <div>
              <p>By clicking «Sign Up» you agree to our <a href="{{ url('terms-conditions') }}" style="font-weight: 600;color: #000;">Terms and Conditions</a></p>
              <span class="submit_form" onclick="submit_form();">submit</span>
              <!-- Row end -->
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
    function submit_form(){
        if($('#oname').val() == ''){
            $('#oname_error').show();
            return false;
        }
        $('#oname_error').hide();
        
        if($('#oday').val() == ''){
            $('#oday_error').show();
            return false;
        }
        $('#oday_error').hide();
        if($('#omonth').val() == ''){
            $('#omonth_error').show();
            return false;
        }
        $('#omonth_error').hide();
        if($('#oyear').val() == ''){
            $('#oyear_error').show();
            return false;
        }
        $('#oyear_error').hide();
        
        if($('#oregion').val() == ''){
            $('#oregion_error').show();
            return false;
        }
        $('#oregion_error').hide();
        
        if($('#opass').val() == ''){
            $('#opass_error').show();
            return false;
        }
        $('#opass_error').hide();
        
        if($('#opass').val().length <= 3){
            $('#ominpass_error').show();
            return false;
        }
        $('#ominpass_error').hide();
        
        if($('#orepass').val() == ''){
            $('#orepass_error').show();
            return false;
        }
        $('#orepass_error').hide();
        
        if($('#opass').val() != $('#orepass').val()){
            $('#oconfpass_error').show();
            return false;
        }
        $('#oconfpass_error').hide();
        
        if($('#oterms').is(':not(:checked)')){
            $('#oterms_error').show();
            return false;
        }
        $('#oterms_error').hide();
        
        $.ajax({
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '<?php echo url("/")?>/update-outer-user',
            data: {
                name: $('#oname').val() ,                
                password: $('#opass').val(),
                month: $('#omonth').val(),
                day: $('#oday').val(),
                year: $('#oyear').val(),
                region: $('#oregion').val()
            },
            type: "POST",
            dataType : "json",
            success: function( data ) {               
                if(data.success)
                {                  
                  location.reload();                  
                }else{
                  $('#ouniquename_error').show();                  
                }                                
                                
              },
              error: function( xhr, status, errorThrown ) {               
                alert( "Sorry, there was a problem!" );
              }
          });
        //$('#oform').submit();
    }
</script>    
<!-- Register popup End -->


<?php if (Auth::check() && Auth::user()->status == 1){ ?>
<script>

	var adminId = '{{ Auth::user()->id }}';
	var chatCount = 0;
	localStorage.setItem("chatCount", chatCount);
	

  function chatCountSet(){
    firebase.database().ref('/chat_history/'+adminId).once('value').then(function(snapshot) {
  		var aa = snapshot.val();
  		$.each(aa, function (key, val) {

        addListnerForUnreadMsg12(adminId,key);

  			if(val.lastSenderId != adminId){
  				chatCount = Number(val.messageCount) + Number(chatCount);
  			}
  		});
  		if(chatCount == 0){
  			$('#notify').hide();
  		}else{
  			$('#notify').show();
  		}
  		localStorage.setItem("chatCount", chatCount);
  	});
  }
  chatCountSet();


  function addListnerForUnreadMsg12(adminId,key){
    var starCountRef = firebase.database().ref('/chat_history/'+adminId).child(key);
    starCountRef.off("child_changed");
    starCountRef.on('child_changed', function(snapshot) {
      var oneMsgkey = snapshot.key;
      if(oneMsgkey == 'message'){
        chatCountSet()
      }
    });
    
  }



</script>

<?php } ?> 


<script type="text/javascript">
$(window).on('load',function(){
    var payment_status = '<?php echo (Session::has( 'payment_status' )) ? Session::get( 'payment_status' ) : ""; ?>';
    var add_diamonds = '<?php echo (Session::has( 'add_diamonds' )) ? Session::get( 'add_diamonds' ) : ""; ?>';
    if(payment_status != "" || add_diamonds != '')
    {
      $('#payment_status').modal('show');
    }

    //for active plan
    $('#upgrade_myModal').on('show.bs.modal', function(e) {
      var active_plan = $(e.relatedTarget).data('plan');
      //alert(active_plan);
      if(active_plan == 'premium')
      {
        $('#premium_title').show();
        $('#premium_footer').show();
        $('#credit_title').hide();
        $('#double_offer').hide();
        $('#active_plan li:nth-child(1)').show();
        $('#active_plan li:nth-child(1)').addClass('active');
        $('#active_plan li:nth-child(2)').hide();
        
        $('#premium_tab').addClass('active');
        $('#credit_tab').removeClass('active');
      }
      if(active_plan == 'credit')
      {
        $('#premium_title').hide();
        $('#premium_footer').hide();
        $('#credit_title').show();
        $('#double_offer').show();
        $('#active_plan li:nth-child(2)').show();
        $('#active_plan li:nth-child(2)').addClass('active');
        $('#active_plan li:nth-child(1)').hide();

        $('#credit_tab').addClass('active');
        $('#premium_tab').removeClass('active');
      }
    });
});
</script>
<script>
    function select_plan(plan_type,plan,qty=30){
        if(qty){            
            $('#prem_days').html(qty);
        }
        $('#plan_type').val(plan_type);
        $('#plan').val(plan);
        
        $('.read').css('background','#ec155a');
        $('.upgrade-btn').removeClass('activebutton');
        if(plan_type == 'diamonds'){
           $('#diamond'+plan).css('background','black'); 
        }
        if(plan_type == 'coins'){
           $('#coin'+plan).css('background','black'); 
        }
        if(plan_type == 'credits'){            
           //$('#credit'+plan).css('background','yellow');
           //alert(credit'+plan);
           $('#credit'+plan).addClass('activebutton');
        }
        if(plan_type == 'premiums'){
           //$('#premium'+plan).css('background','black');
           $('#premium'+plan).addClass('activebutton');
        }
        
    }
</script>



<script type="text/javascript">   
   getRegion('United Kingdom');	
  
	function getRegion(name=null){
        if(name == null){ name = 'United Kingdom'; }
        //alert(name);
        var selected = '{{ isset($_GET['state']) ? $_GET['state'] : '' }}';
        $.ajax({
                headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '<?php echo url("/")?>/getRegion',
                type: 'POST',
                cache : false,
                processData: false,
                data: 'name='+name+'&selected='+selected, // serializes the form's elements.
                success: function(data)
                {
                 
                 $('#prof_region').html(data);
                 $('#prof_region1').html(data);
                 $('#prof_region2').html(data);
                 $('#prof_region3').html(data);
                 $('#oregion').html(data);
                }
            });
    }
	
   
//RESEND VERIFICATION EMAIL
function resend_verification(userid)
{
	//alert(name);
	$.ajax(
	{
		headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
		url: '<?php echo url("/")?>/resend-verification',
		type: 'GET',
		cache : false,
		processData: false,
		data: 'user_id='+userid, // serializes the form's elements.
		success: function(data)
		{
			//console.log(data);
			$('#resend_email').html('Email Sent');
            $('#resend_email1').html('<a>Email Sent</a>');
		}
	});
} 
</script>

<script type="text/javascript">
$(document).ready(function() {
  //for login
  $('#login_modal').submit(function(e) {
    e.preventDefault();
    var login_email = $('#login_email').val();
    var login_password = $('#login_password').val();
    var err =0;
    var mail_validation = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if(login_email.trim() == "")
    {
      $('.email-wrong').text('Please enter email');
      err++;
    }
    else if(!mail_validation.test(login_email)){
      $('.email-wrong').text('Please enter valid email');
      err++;
    }
    else{
     $('.email-wrong').text('');
    }
    
    if(login_password.trim() == "")
    {
      $('.both-wrong').text('');
      $('.password-wrong').text('Please enter password');
      err++;
    }
    else{
     $('.password-wrong').text('');
    }

    if(err == 0)
    { 
      $('.loader').show();
      $.ajax({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: '<?php echo url("/")?>/login',
        data: {
            email: login_email ,
            password: login_password
        },
        type: "POST",
        dataType : "json",
        success: function( data ) {
            //alert(JSON.stringify(data));
            //alert(data.errors.password);
            $('.loader').hide();
            if(data.success)
            {
              window.location.reload();
              //window.location.href = "<?php echo url('/')?>/search";
            }
            else if(data.errors.email){
              $('.email-wrong').text(data.errors.email);
            }
            else if(data.errors.password){
              $('.both-wrong').text('');
              $('.password-wrong').text(data.errors.password);
            }
            else if((data.errors.email) && (data.errors.password)){
              $('.email-wrong').text(data.errors.email);
              $('.password-wrong').text(data.errors.password);
            }
            else{
              $('.password-wrong').text('');
              $('.both-wrong').text(data.errors);
            }
          },
          error: function( xhr, status, errorThrown ) {
            
          }
      });
    }
    else{
      //$('.both-wrong').text('Please fill all the details');
    }
    
  });

  //for signup
  $('#signup_modal').submit(function(e){
    e.preventDefault();
    
	var gender = $("input:radio.gender_classs:checked").val();
    var seeking = $("input:radio.seeking_classs:checked").val();
	
	var email = $('#signup_email').val();
    var password = $('#signup_password').val();
    
	//age
    var month = $('#month').val();
    var day = $('#day').val();
    var year = $('#year').val();
    
    var partnerid = $('#partnerid').val();
	
    var mail_validation = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
   
    var error =0;
    if(email.trim() == "")
    {
      $('.signup-email-wrong').text('Please enter email');
      $("#signup_email").css("border-color", "red");
      error++;
    }
    else if(!mail_validation.test(email)){
      $('.signup-email-wrong').text('Please enter valid email');
      $("#signup_email").css("border-color", "red");
      error++;
    }
    else{
      $('.signup-email-wrong').text('');
      $("#signup_email").css("border-color", "#eee");
    }
    if(password.trim() == "")
    {
      $('.signup-password-wrong').text('Please enter password');
      $("#signup_password").css("border-color", "red");
      error++;
    }
    else{
      $('.signup-password-wrong').text('');
      $("#signup_password").css("border-color", "#eee");
    }

    if(gender == '' || typeof gender === "undefined")
    {
      $('.gender').show();
      $('.gender').text('Please select gender');
      error++;
    }
    else{
      $('.gender').hide();
      $('.gender').text('');
    }

    if(seeking == '' || typeof seeking === "undefined")
    {
      $('.seeking').show();
      $('.seeking').text('Please select seeking');
      error++;
    }
    else{
      $('.seeking').hide();
      $('.seeking').text('');
    }

    if(year == '')
    {
      $('.bith_date').show();
      $('.birth_date').text('Select your date of birth');
      error++;
    } 

    if(month == '')
    {
      $('.bith_date').show();
      $('.birth_date').text('Please select birth date');
      error++;
    }
    
    
    if(day == '')
    {
      $('.bith_date').show();
      $('.birth_date').text('Please select birth date');
      error++;
    }
    

    if(day != '' && month != '' && year != '')
    {
      $('.bith_date').hide();
      $('.birth_date').text('');
    }

    if($('#accept_cond').is(':not(:checked)'))
    {
      $('.accept').show();
      $('.accept').text('Please accept conditions');
      error++;
    }
    else{
      $('.accept').hide();
      $('.accept').text('');
    }

    if(error == 0)
    {
      $('.signup-process').show();
      $.ajax({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: '<?php echo url("/")?>/signup',
        data: {
            email: email ,
            password: password,
            month: month,
            day: day,
            year: year,
            gender: gender,
            seeking: seeking,
            partner_id : partnerid
        },
        type: "POST",
        dataType : "json",
        success: function( data ) {
            $('.loader').hide();
            if(data.success)
            {
              $('.signup-process').hide();
              location.reload();
            }
            if(data.errors.name){
              $('.signup-process').hide();
              $('.signup-name-wrong').text(data.errors.name);
              $("#signup_name").css("border-color", "red");
            }
            else{
              $("#signup_name").css("border-color", "#eee");
            }
           
            
            if(data.errors.email){
              $('.signup-process').hide();
              $.each( data.errors.email, function( i, val ) {
                  $('.signup-email-wrong').append('<p class="error-message">'+val+'</p>');
              });
              $("#signup_email").css("border-color", "red");
            }
            else{
              $("#signup_email").css("border-color", "#eee");

            }
            if(data.errors.password){
              //clear_signup_errors();
              $('.signup-process').hide();
              $.each( data.errors.password, function( i, val ) {
                  $('.signup-password-wrong').append('<p class="error-message">'+val+'</p>');
              });
              $("#signup_password").css("border-color", "red");
              //$('.password-wrong').text(data.errors.password);
            }
            else
            {
              $("#signup_password").css("border-color", "#eee");
            }
          },
          error: function( xhr, status, errorThrown ) {
            $('.signup-process').hide();
            alert( "Sorry, there was a problem!" );
          }
      });
    }
    else{
      //$('.all-wrong').text('Please fill all the details');
    }

  });
});
</script>
<script>
  <?php if(Request::segment(1) == 'profile'){ ?>
	$('#owl-carousel1').owlCarousel({
		items:1,
		margin:10,
		autoHeight:true,
		autoplay: true,
		dots: true,
	});
  <?php }  ?>
</script>
<script>
  var is_update = '<?php echo (isset(Auth::user()->is_update) && Auth::user()->is_update == '0') ? '1' : '0'; ?>';
  var is_login = '<?php echo (Auth::id()); ?>';
  var profile_id = $(location).attr("href").split('/').pop();
  var is_partner =  '{{ ($rand == 0) ? '0' : '1' }}';
  
	//FOR CAMPAIGN SIGNUP POPUP BY ARJUN
	var ready_gender =  '<?php echo $_GET["gender"] ?>';
	var ready_seeking =  '<?php echo $_GET["seeking"] ?>';
	var ready_min_age =  '<?php echo $_GET["min_age"] ?>';
	$(window).load(function()
	{
		if(!is_login && ready_gender =='1' && ready_seeking =='2' && ready_seeking =='2' && ready_min_age == '')
		{
		  setTimeout(function(){        
				$("#s_signup_popup").addClass("open_popup");
				$('.modal_main').modal({backdrop: 'static', keyboard: false});
			  }, 2000);
		}

		
	});
	//FOR CAMPAIGN SIGNUP POPUP END 

	//LOGIN SIGNUP POPUP NEW SCRIPT BY ARJUN -->
	
	
	$(window).load(function(){
		if(!is_login && (profile_id != 'terms-conditions') && (profile_id != 'about') && (profile_id != 'privacy-policy') && (profile_id != 'our-plans') && (profile_id != 'faq') && (is_partner == '0'))
    {
		  setTimeout(function(){        
				$(".modal_main").addClass("open_popup");
				$('.modal_main').modal({backdrop: 'static', keyboard: false});
			  }, 2000);
		}
     
        
	});

 //hide modal main
	 $(".log_sign_btn").on('click',function(){
		$(this).parents(".modal_main").removeClass("open_popup");
	 });
	 $(".hide_model").on('click',function(){
		$(this).parents(".splash_screen_modal").removeClass("open_popup");
	 });
	 //show popup on button click
	 
	  $(".popup_btn").on('click',function(){
		var data_id  = $(this).attr("data-show");
		$("#"+data_id).addClass("open_popup");
	 });
	  //Datepicker
	if ($(".age_datepicker").length > 0) {
	   $(".age_datepicker").datepicker({
		   dateFormat: "dd-mm-yy",
		   changeYear: true,
		   minDate: '-100y',
		   maxDate: '-18y',
		   yearRange: "-100:+0"
		});
	 }
    
	//forgot passowrd
	$(document).ready(function() 
	{
		
		$('#forgot').submit(function(e) 
		{
			e.preventDefault();
			var forgot_email = $('#forgot_email').val();
			
			var err =0;
			var mail_validation = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
			if(forgot_email.trim() == "")
			{
			  $('.forgot-email-wrong').text('Please enter email');
			  err++;
			}
			else if(!mail_validation.test(forgot_email)){
			  $('.forgot-email-wrong').text('Please enter valid email');
			  err++;
			}
			else{
			 $('.forgot-email-wrong').text('');
			}
			

			if(err == 0)
			{ 
			  $('.forgot-process').show();
			  $.ajax({
				headers: {
				  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				url: '<?php echo url("/")?>/forgetPassword',
				data: {
					email: forgot_email ,
				},
				type: "POST",
				dataType : "json",
				success: function( data ) {
					$('.forgot-process').hide();
					if(data.status)
					{
					  $('.forgot-email-success').text(data.message);
					  $('.forgot-email-wrong').text('');
					  
					  setTimeout(function(){        
						$("#forget_popup").removeClass("open_popup"); 
						$("#s_login_popup").addClass("open_popup"); 
						}, 3000);
					  
					}
					else if(data.message){
					  $('.forgot-email-wrong').text(data.message);
					  $('.forgot-email-success').text('');
					}
					
				  },
				  error: function( xhr, status, errorThrown ) {
					$('.forgot-process').hide();
					console.log( "Sorry, there was a problem!" );
				  }
			  });
			}
			else{
			}
			
		});
	});
	
	
	//LOGIN SIGNUP POPUP NEW SCRIPT BY ARJUN  END
  

  $(window).load(function(){
		if((is_update == '1') && (is_login))
    { 
      /* setTimeout(function(){
  		  $('#myModal_profile').modal('show');
		$('#myModal_profile').modal({backdrop: 'static', keyboard: false});
  		}, 2000); */
	  }
  });
</script>

<script>
$(document).ready(function(){ 
    $("#additional-options").click(function() {
        
		$(".additional-options").slideToggle();
    }); 
});
</script>

<script>
     function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah')
                        .attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
</script>

<script>
$('form#update_profile_form').on('submit',function(e){
  
  e.preventDefault();
    var err=0;
    var user_id = $('#user_id').val();
    var name = $('#name').val().trim();
    var gender = $("input:radio.gender_class:checked").val();
    var seeking = $("input:radio.seeking_class:checked").val();
    var month = $('#month').val();
    var day = $('#day').val();
    var year = $('#year').val();
    
    var city = $('#prof_city').val();
    var country = $('#prof_country').val();
    
    if(name == '')
    {
      $('.name').show();
      $('.name').text('Enter your name or nickname');
      err++;
    }
    else{
      $('.name').hide();
      $('.name').text('');
    }

    if(gender == '' || typeof gender === "undefined")
    {
      $('.gender').show();
      $('.gender').text('Please select gender');
      err++;
    }
    else{
      $('.gender').hide();
      $('.gender').text('');
    }

    if(seeking == '' || typeof seeking === "undefined")
    {
      $('.seeking').show();
      $('.seeking').text('Please select seeking');
      err++;
    }
    else{
      $('.seeking').hide();
      $('.seeking').text('');
    }

    if(year == '')
    {
      $('.bith_date').show();
      $('.birth_date').text('Select your date of birth');
      err++;
    }
    

    if(month == '')
    {
      $('.bith_date').show();
      $('.birth_date').text('Please select birth date');
      err++;
    }
    
    
    if(day == '')
    {
      $('.bith_date').show();
      $('.birth_date').text('Please select birth date');
      err++;
    }
    

    if(day != '' && month != '' && year != '')
    {
      $('.bith_date').hide();
      $('.birth_date').text('');
    }

    if(country == '')
    {
      $('.address').show();
      $('.address').text('Please enter address');
      err++;
    }
    else{
      $('.address').hide();
      $('.address').text('Please select address');
    }

    if(err == 0)
    {
      $.ajax({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: '<?php echo url("/")?>/user-profile-update',
        type: 'POST',
        cache : false,
        processData: false,
        data: 'name='+name+'&gender='+gender+'&seeking='+seeking+'&day='+day+'&month='+month+'&year='+year+'&address='+''+'&city='+city+'&country='+country+'&user_id='+user_id, // serializes the form's elements.
        success: function(data)
        {
         if(data == 'success')
         {
          $('#myModal_profile').modal('hide');
          location.reload();
         }
         else{
          alert('Something went wrong.');
          //location.reload();
         }
        }
      });
    }
    else{
      return false;
    } 
});
</script>
<script type="text/javascript">
//on load call all paymentmethods to dropdown
$(window).on('load',function(){
//payment_methods();
});

$(document).ready(function(){
  $('.coin-list ul li div').click(function(){
    $('.coin-list li div.active').removeClass('active');
    $(this).addClass('active');
    var get_amt = $('.coin-list li div.active span').text();
    $('#amount').val(get_amt+'.00');
    $('.credit_text').text(get_amt);

  });

  $('#purchase_credit').click(function(e) {
    console.log('clicked');
    e.preventDefault();
    //var get_amt = $('.coin-list li div.active span').text();
    var plan_type = $('#plan_type').val();
    var plan = $('#plan').val();
    if(plan == ''){            
            $('#plan_error').show()
            return false;
        }
    var gateway = $('#payment_method').val();
	console.log(gateway);
    //var description = $('#description').val();
	if(gateway == 'flexpay')
	{				
		$.ajax(
		{
			headers: {
			  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			url: '<?php echo url("/")?>/purchase-credit',
			type: 'POST',
			data: 'plan_type='+plan_type+'&plan='+plan, // serializes the form's elements.
			success: function(data)
			{
				 if(data.success)
				 {
					window.location.href = data.redirect;
				 }
			}
		});
	}
 //    if(gateway == 'pay')
	// {
	// 	var paynl_id = $('#paynl_id').val();
		
	// 	$.ajax(
	// 	{
	// 		headers: {
	// 		  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	// 		},
	// 		url: '<?php echo url("/")?>/purchase-credit-paynl',
	// 		type: 'POST',
	// 		data: 'plan_type='+plan_type+'&plan='+plan+'&paynl_id='+paynl_id, // serializes the form's elements.
	// 		success: function(data)
	// 		{
	// 			 if(data.success)
	// 			 {
	// 				window.location.href = data.redirect;
	// 			 }
	// 		}
	// 	});
	// }	
	// if(gateway == 'card')
	// {	
 //     $.ajax({
 //        headers: {
 //          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
 //        },
 //        url: '<?php echo url("/")?>/purchase-credit',
 //        type: 'POST',
 //        data: 'plan_type='+plan_type+'&plan='+plan, // serializes the form's elements.
 //        success: function(data)
 //        {
 //         if(data.success)
 //         {
 //          window.location.href = data.redirect;
 //         }
 //        }
 //      });
	// }  
  });
});
</script>

<!-- load local and ssl script src="http://a.drien.com/src/js/jquery.flipster.js"></script-->
<script src="{{ asset('public/js/jquery.flipster.js') }}"></script>
<script src="{{ URL::asset('public/js/jquery.magnific-popup.js')}}"></script> 
<script>
$(window).load(function(){
	$(".flipster").flipster({});
	 });
	
    var carousel = $(".flipster").flipster({
        style: 'carousel',
        spacing: -0.5,
        nav: true,
        buttons:   true,
    });
</script>
<script type="text/javascript">
  $(document).ready(function(){
    //show data on click
    $('.menulist ul li').on('click',function(){
      $('.menulist ul li').removeClass("active");
      $(this).addClass("active");
      var content= $(this).attr('data-show');
      $(".tab_content").removeClass("active");
      $("#"+content).addClass("active");
    });
  });
</script>
<script> 
$(document).ready(function(){
  //close chat sidebar on click
  $(".chat_toggle").on('click', function(){
    $(this).parents(".chat_sidebar").toggleClass("close_sidebar");
    $(this).toggleClass("close_toggle");
    $(".main-content").toggleClass("full_content");
  });
  //set height of main wrapper
  var header_h = $("header").height();
  var win_h = $(window).height();
  var full_height = (win_h - header_h) ;
  var full_height2 = (win_h - header_h)-100 ;
  $(".main_wrapper").css("min-height", full_height);
});
</script>

<script> 
//open emoji icons in chat
$(document).ready(function(){
  $(document).on('click',".emoji_icon",function(){
    get_emoji();
    $(".emoji-list").slideToggle("slow");
    //$(".emoji-tape").hide();
  });
   $(document).on('click',".chat__messages",function(){
    $(".emoji-list").slideUp("slow");
    //$(".emoji-tape").show();
  });
});
</script>
<script>
  $( function() {
    $( "#player" ).autocomplete({
       source: function( request, response ) {
       // Fetch data
       $.ajax({
        url: "<?php echo url('/');?>/fetch_users",
        type: 'post',
        dataType: "json",
        data: {
         search: request.term
        },
        success: function( data ) {
         if(data != "")
         {
            response( data );
         }
         else{
            //$('#player').val('');
            $('#player_id').val('');
            //$('#player').text('');
         }
        }
       });
      },
      select: function (event, ui) {
       // Set selection
       $('#player').val(ui.item.label); // display the selected text
       $('#player_id').val(ui.item.value); // save selected id to input
       return false;
      }
    });
  });
  </script>
  <!-- Happy -- Mautic -->
  
  <script>
	function getUrlParameter(name) {
		name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
		var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
		var results = regex.exec(location.search);
		return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
	};
	
    (function(w,d,t,u,n,a,m){w['MauticTrackingObject']=n;
        w[n]=w[n]||function(){(w[n].q=w[n].q||[]).push(arguments)},a=d.createElement(t),
        m=d.getElementsByTagName(t)[0];a.async=1;a.src=u;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://emm.oddigits.com/mtc.js','mt');

	var email  = getUrlParameter('email');

	if(email !== ''){
		mt('send', 'pageview', {email: email});
	} else {
		mt('send', 'pageview');
	}
</script>
   
  <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-138205075-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
    
      gtag('config', 'UA-138205075-1');
    </script>    
    
    
    
    
    
    <script>
        var insertData = {
            'userId' : '{{ isset($users->id) ? $users->id : '' }}',
            'userName' : '{{ isset($users->name) ? $users->name : '' }}',
            'userImage' : '{{ isset($users->profile_image) ? $users->profile_image : '' }}',
            'fake' : '{{ $users->fake}}',
        }
        
        <?php if(isset($users->id)){ ?>
        firebase.database().ref().child('users').child('{{ isset($users->id) ? $users->id : '' }}').set(insertData);
        <?php } ?>
    </script>
    
</body>
</html>

<input type="hidden"  name="interval_time" id="interval_time" value="20000">

<input type="hidden"  name="sender_id" id="sender_ids1" value="{{ $users->id }}">
<!-- <script type="text/javascript">
    //setup before functions
    var typingTimer1;                //timer identifier
    var doneTypingInterval1 = 2000;  //time in ms, 5 second for example
    var $input1 = $('#myInput1');

    //on keyup, start the countdown
    $input1.on('keyup', function () {
      clearTimeout(typingTimer1);
      typingTimer1 = setTimeout(doneTyping1, doneTypingInterval1);
    });

    //on keydown, clear the countdown 
    $input1.on('keydown', function () {
      clearTimeout(typingTimer1);
    });

    //user is "finished typing," do something
    function doneTyping1 () {
      var value1 = $('#myInput1').val().toLowerCase();
      getChatHistory1('hedChatHis','inbox',value1,'chat');
    }


    var adminId1 = $('#sender_ids1').val();
   
    function getChatHistory1(type, show, search, btnText){

      var userId    = $('#chatNowuserId').val();

      var showHideBtn = showNoMsg = false;

      $('#'+type).html(''+
       '<li id="noMsgText1" >'+
          '<div class="no_chat_requests">'+
            '<h5>You have currently no chat.</h5>'+
          '</div>'+
        '</li>'+
      '');

 
      firebase.database().ref('/chat_history/'+adminId).once('value').then(function(snapshot) {
        console.log('parvez');
        console.log(snapshot);
      $('.alreadyChat').show();
      snapshot.forEach(function(oneUser) {
        var childData = oneUser.val();

        if(childData.message.length > 3)
              childData.message = childData.message.substring(0,3) + '...';

        var chatTabType = 'all';
        if(childData.historyType == 1){
          if(childData.lastSenderId == adminId){
            chatTabType = 'send';
          }else{
            chatTabType = 'receive';
          }
        }

        
        

       
        if(childData.uid == userId){
           $('.alreadyChat').attr('disabled',true);
           showHideBtn = true;
        }else{
          if(showHideBtn == false){
             $('.alreadyChat').show();
          }
        }



        var str1 = childData.name.toLowerCase();
        var str2 = search.toLowerCase();
        


        if(str1.indexOf(str2) != -1){
      
          if(chatTabType == 'all' || chatTabType == 'send'){
            $('#noMsgText1').remove();
          }

          $('#'+type).append(''+
            '<li class="confirmchat '+chatTabType+'">'+
              '<div class="chat-user-img">'+
                '<a href="{{ url('inbox-chat') }}/'+childData.uid+'">'+
                  '<img alt="" src="{{ asset('public/thumbnail') }}/'+childData.profilePic+'" height="55px">'+
                  '<img alt="" class="zoomed" src="{{ asset('public/thumbnail') }}/'+childData.profilePic+'"  height="55px">'+
                '</a>'+
              '</div>'+
              //'<div class="chat-user-detail">'+
              //'<h6>'+childData.name+'</h6>'+              
              //'<p>'+childData.message+'</p>'+
              //'</div>'+
              //'<div class="chat-user-btn" id="chat-user-btn'+childData.uid+'">'+
              //'<button class="btn-bg" onclick="getCaht('+childData.uid+');">'+btnText+'</button>'+
              //'<a href="{{ url('inbox-chat') }}/'+childData.uid+'"><button class="btn-bg">'+btnText+'</button></a>'+
              //'</div>'+
            '</li>'+
          '')
        }

        if(chatTabType == 'receive'){
          showNoMsg = true;
          $('#chatRequest').append(''+
            '<li class="confirmchat">'+
              '<div class="chat-user-img">'+
                '<a href="{{ url('inbox-chat') }}/'+childData.uid+'">'+
                  '<img alt="" src="{{ asset('public/thumbnail') }}/'+childData.profilePic+'" height="55px">'+
                  '<img alt="" class="zoomed" src="{{ asset('public/thumbnail') }}/'+childData.profilePic+'"  height="55px">'+
                '</a>'+
              '</div>'+
              //'<div class="chat-user-detail">'+
             // '<h6>'+childData.name+'</h6>'+              
              //'<p>'+childData.message+'</p>'+
              //'</div>'+
             // '<div class="chat-user-btn" id="chat-user-btn'+childData.uid+'">'+
              //'<button class="btn-bg" onclick="getCaht('+childData.uid+');">Reply</button>'+
              //'<a href="{{ url('inbox-chat') }}/'+childData.uid+'"><button class="btn-bg">Reply</button></a>'+
              //'</div>'+
            '</li>'+
          '')
        }else{
          if(showNoMsg == false){
            $('#chatRequest').html(''+
             '<li id="noMsgText" >'+
                '<div class="no_chat_requests">'+
                  '<h5>You have currently no chat request.</h5>'+
                '</div>'+
              '</li>'+
            '');
          }else{
            $('#noMsgText').remove();
          }
        }


        showHideChat1(show);
      });
    });
  }
  //getChatHistory1('hedChatHis','inbox','','chat'); // for show My Contacts

  function getCaht1(userId){
    firebase.database().ref('/users/'+userId).once('value').then(function(snapshot) {
      var childData = snapshot.val();
      var id = childData.userId;
      var name = childData.userName;
      var image = childData.userImage;
      set_receiver(id,name,image,0);;
    });
  }
  
  function showHideChat1(type){
   
    $('.chating_list').hide();
    if(type == 'inbox'){
        //$('.receive').show();
        $('.all').show();
        $('.send').hide();
        $('.receive').hide();
    }else if(type == 'Outbox'){
        $('.send').show();
    }else if(type == 'Conversation'){
        $('.all').show();
    }
  }
</script> -->
<!-- <script>
  //click to show search bar
    $(".search-bar-btn").on('click', function(){
      $(".profile_search_bar").slideToggle(300);
    });
    //hide dropdown in touch
    /*$(document).on("click touchend", function(){
      $(".dropdown").removeClass("open");
    });*/
</script> -->
<script>  

  function chatPopupFunction() {
    var min = 15,
      max = 30;
    var rand = Math.floor(Math.random() * (max - min + 1) + min); //Generate Random number between 5 - 10
    $('.noti_image').attr('src','');
    $.ajax({
              headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              },
              url: '<?php echo url("/")?>/getRandomFakeUser',
              type: 'GET',              
              dataType : "json",
              success: function(data)
              {
                  console.log(data.name);
                  $('#noti_name').html(data.name);
                  $('.noti_chat_btn').attr('href','{{ url("inbox-chat") }}/'+data.id);
                  $('.noti_image').attr('src','https://moneoangels.com/public/thumbnail/'+data.image);

                  
                  $(".chat_notifi_box_wrap").addClass("open");
                  
                  
                  if ($(".chat_notifi_box_wrap").hasClass("open")) {
                    setTimeout(function(){
                          $(".chat_notifi_box_wrap").removeClass("open");
                      }, 10000);
                  } 
              }
          }); 

    setTimeout(chatPopupFunction, rand * 1000);
  }

  var min = 5, max = 15;
  var rand = Math.floor(Math.random() * (max - min + 1) + min); //Generate Random number between 5 - 10
  setTimeout(chatPopupFunction, rand * 1000);
  
</script>

<script>
  if ($('.splash_form .error-message').is(':empty')){
  $(this).addClass("err_msg_show");
  }
  else{
  $(this).removeClass("err_msg_show");
  }
</script>
<script type="text/javascript">
  //close notification
  $(".close_box").on('click',function(){
    $(this).parents(".chat_notifi_box_wrap").removeClass("open");
  }); 
</script>

 
