<?php

declare(strict_types=1);

namespace Modules\Admin\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use URL;

class User extends Authenticatable
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    /**
     * The primary key used by the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',                
        'profile_image',
        'cover_image',
        'phone',        
        'email',
        'gender',
        'password',
        'status',        
        'address',
        'country',
        'city',
        'dob',
        'profile_type',
        'adult_level',
        'appearance'
    ];  // All field of user table h


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $guarded = ['created_at', 'updated_at', 'id'];

    // Return user record
    public function getUserDetail($id = null)
    {
        if ($id) {
            return User::find($id);
        }

        return User::all();
    }

    public static function createImage($request, $fielName)
    {        
        try {
            if ($request->file($fielName)) {
                $photo = $request->file($fielName);

                $destinationPath = 'public/images/profile_image/';
                $photo->move($destinationPath, time() . $photo->getClientOriginalName());
                $photo_name = time() . $photo->getClientOriginalName();

                return  $photo_name;
            //$request->merge(['photo'=>$photo_name]);
            } else {
                return false;
            }
        } catch (Exception $e) {
            return false;
        }
    }
    
    public static function createCoverImage($request, $fielName)
    {        
        try {
            if ($request->file($fielName)) {
                $photo = $request->file($fielName);

                $destinationPath = 'public/images/cover_img/';
                $photo->move($destinationPath, time() . $photo->getClientOriginalName());
                $photo_name = time() . $photo->getClientOriginalName();

                return  $photo_name;
            //$request->merge(['photo'=>$photo_name]);
            } else {
                return false;
            }
        } catch (Exception $e) {
            return false;
        }
    }
        
    public static function createGalleryImage($request, $fielName)
    {        
        try {
            if ($request->file($fielName)) {
                $photo_name = [];
                foreach($request->file($fielName) as $img){
                    $photo = $img;                    
                    $destinationPath = 'public/images/profile_image/';
                    $photo->move($destinationPath, time() . $photo->getClientOriginalName());                    
                    $photo_name[] = time() . $photo->getClientOriginalName();
                }

                return  $photo_name;
            //$request->merge(['photo'=>$photo_name]);
            } else {
                return false;
            }
        } catch (Exception $e) {
            return false;
        }
    }

    public function role()
    {
        return $this->belongsTo('Modules\Admin\Models\Role', 'id','role_type');
    }

}
