<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use Helper;

class Profile extends Model
{
    public function user_detail($user_id)
    {
    	$get_user = DB::table('users')->select('users.*','user_details.*')
            ->leftJoin('user_details', 'user_details.user_id', '=', 'users.id')->where('users.id','=',$user_id)->first();

        return (!empty($get_user) ? $get_user : false);
    }

    public function get_videos($user_id)
    {
        $get_videos = DB::table('gallery_videos')->where('user_id','=',$user_id)->where('is_private','=',0)->where('status','=',1)->get();

        return (!empty($get_videos) ? $get_videos : '');
    }

    public function get_public_images($user_id)
    {
    	$get_public_images = DB::table('gallery_images')->where('user_id','=',$user_id)->where('is_private','=',0)->where('status','=',1)->get();

    	return (!empty($get_public_images) ? $get_public_images : '');
    }

    public function get_private_images($user_id)
    {
    	$get_private_images = DB::table('gallery_images')->where('user_id','=',$user_id)->where('is_private','=',1)->where('status','=',1)->get();

    	return (!empty($get_private_images) ? $get_private_images : '');
    }

    public function get_interests($user_id)
    {
    	$get_interests = DB::table('user_interests')->where('user_interests.user_id','=',$user_id)->first();

    	return (!empty($get_interests->interests) ? $get_interests->interests : '');
    }

    public function get_languages($user_id)
    {
    	$get_languages = DB::table('user_languages')->where('user_languages.user_id','=',$user_id)->first();

    	return (!empty($get_languages->languages) ? $get_languages->languages : '');
    }
}
