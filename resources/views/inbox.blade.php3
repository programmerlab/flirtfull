@extends('layout.default')
@section('content')

    <style>
        body {
            /*background:url('https://kabayanzone.ae/carol/datingApp/public/images/heart-bg.png');*/
        }
        .tab-contents {
            margin-top: 20px;
            height: 500px;
            overflow-y: auto;
            margin:0;
        }
    .search_list_outer {
      padding-top: 80px;
     /* height: 100vh;
      max-height: 100vh;*/
      position: relative; 
  }
.inbox_outer .chat-window-panel-inner{ padding-bottom:70px;}
.inbox_outer .chat-sender-box {
    padding: 10px 50px;
    position: absolute;
    bottom: 0;
}
.inbox_outer .chat-window-box {
    height: 500px;
    padding-bottom: 70px;
}
.chat__messages{/* height:100%; overflow:auto;*/}
    .inbox_outer .container-fluid,.inbox_outer .container-fluid .col-md-4,.inbox_outer .container-fluid .col-md-8,.inbox_outer #fake_chat,.inbox_outer .user_chat_popup,.inbox_outer .chat-window-panel,.inbox_outer .chat-window-panel-inner {
    height: 100%;
}
.inbox_outer .tabs{ height:100%;}
.nav > li > a:focus, .nav > li > a:hover,.nav-pills > li.active > a, .nav-pills > li.active > a:focus, .nav-pills > li.active > a:hover{ background:#858585; color:#fff;}
@media(max-width: 990px){
  .search_list_outer {
    padding-top: 70px;
  }
}
@media(max-width: 767px){
  .search_list_outer {
    padding-top: 60px;
  }
.inbox_outer .chat-sender-box {
    padding: 10px;
  }
  .inbox_outer .container-fluid, .inbox_outer .container-fluid .col-md-4, .inbox_outer .container-fluid .col-md-8{ height:auto;}
  .search_list_outer{ height:auto;}
  .fl_footer_section{
    display: none;
  }
}

  img {
    height="50px";
    width="50px";
  }
  .search_input_center {
      position: relative;
      width: 100%;
      display: inline-block;
      margin-bottom: 15px;
  }
  .search_input_center input {
      font-size: 14px;
      border: 1px solid #ccc;
      width: 100%;
      height: 45px;
      border-radius: 2em;
      padding: 0 43px 0 15px;
      font-weight: 500;
  }
  .search_input_center input:foucs{
    border: 1px solid #ccc;
    outline: none;
    box-shadow: none;
    -webkit-box-shadow: none;
    -moz-box-shadow: none;
  }
  .search_input_center .in_search {
    position: absolute;
    top: 0;
    right: 0;
    background-color: #EC155A;
    color: #fff;
    border: none;
    height: 45px;
    line-height: 45px;
    width: 40px;
    text-align: center;
    padding: 0;
    border-radius: 0em 2em 2em 0;
}
    </style>
<!--Chat script -->        
    
<section class="inbox_outer search_list_outer">
   <div class="container">
    <!--left part start -->
         <div class="row">
            <div class="tabs chat_tabs" id="exTab3">
              <div class="col-md-9 col-md-offset-3">
                <div class="search_input_inbox">
                  <div class="search_input_center">
                    <input type="text" id="myInput" name="" placeholder="Search by Name or Text">
                    <button class="in_search"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </div>
              <div class="col-md-3 col-sm-12">
                 <ul class="nav nav-pills">
                      <li id="liinbox"><a href="#" data-toggle="tab" onclick="showHideChat('inbox');">Inbox</a></li>  
                      <li id="liOutbox" ><a href="#" data-toggle="tab" onclick="showHideChat('Outbox');">Outbox</a></li>
                      <li id="liConversation"><a href="#" data-toggle="tab" onclick="showHideChat('Conversation');">Conversation</a></li>
                  </ul>
                </div>
                <div class="col-md-9 col-sm-12">
                  <div class="tab-content">
                    <div class="tab-pane active" id="tab1">                        
                        <div>
                          <div class="panel-body">
                          <div class="table-responsive chating_table">
                             <table class="table table-hover data_table">
                                <thead>
                                  <tr>
                                    <td></td>
                                    <td></td>                   
                                  </tr>
                                </thead>
                                <tbody id="table011">
                                  
                                </tbody>
                             </table>
                            </div>
                          </div>
                        </div>
                    </div>
                    <!--<div class="tab-pane" id="tab2">
                        <div>
                          <div class="panel-body">
                             <table class="table table-hover table-responsive" id="table022">
                                        
                                  </table>
                          </div>
                        </div>
                    </div>-->
                    <!--<div class="tab-pane <?php if(empty($inbox)){?>active<?php } ?>" id="tab3">                       
                        <div>
                          <div class="panel-body">
                           <div class="chating_list_wrap" id="table03">
                                     
                            </div>
                          </div>
                        </div>
                    </div>-->
                  </div>
                  <p id="pagination-here"></p>
                </div>

               <!--<div class="modal fade modaltwo" id="reply-modal">
                  <div class="modal-dialog">
                     <div class="modal-content">
                        <div class="modal-header">
                           <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                           <div class="inbox_detail_wrapper text-center">
                              <a href="#">
                                 <img alt="" src="{{ asset('public/images/user3.jpg') }}">
                                 <h4 class="modal-title">My Email to  <strong>CAROLINA</strong></h4>
                              </a>
                           </div>
                        </div>
                        <div class="modal-body">
                            <!--  chat popup  --> 
                                
                               
                                <!--  chat popup  --                            
                        </div>
                        <div class="modal-footer">                           
                            
                        </div>
                     </div>
                     <!-- /.modal-content --
                  </div>
                  <!-- /.modal-dialog --
               </div>-->
            </div>
         </div>
        <!--left part End -->
   </div>

</section>

    
<input type="hidden"  name="sender_id" id="sender_id" value="{{ $users->id }}">
<input type="hidden"  name="receive_count" id="receive_count" value="{{ $receive_count }}">
<input type="hidden"  name="send_count" id="send_count" value="{{ $send_count }}">
<input type="hidden"  name="all_count" id="all_count" value="{{ $all_count }}">
<input type="hidden"  name="receive" id="receive" value="{{ $receive }}">
<input type="hidden"  name="send" id="send" value="{{ $send }}">
<input type="hidden"  name="active_tab" id="active_tab" value="receive">
<input type="hidden"  name="chat_count" id="chat_count" value="{{ $chat_count }}">
<input type="hidden"  name="pagination" id="pagination" value="1">

<!--<input type="hidden"  name="sender_name" id="sender_name" value="{{ $users->name }}">
<input type="hidden"  name="sender_image" id="sender_image" value="{{ $users->profile_image }}">

<input type="hidden"  name="receiver_id" id="receiver_id" value="">
<input type="hidden"  name="receiver_name" id="receiver_name" value="">
<input type="hidden"  name="receiver_image" id="receiver_image" value="">
<input type="hidden"  name="room_id" id="room_id" value="">
<input type="hidden"  name="chat_image" id="chat_image" value="">-->
<!--
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.css"/>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script>
setTimeout(function(){
    $('.data_tables').DataTable({
        
    });
}, 3000);
    
</script> -->     
<script type="text/javascript" src="http://botmonster.com/jquery-bootpag/jquery.bootpag.js"></script>
<script>
    var receive = {!! json_encode($receive) !!};
    var send = {!! json_encode($send) !!};
    var all = {!! json_encode($all) !!};
    var adminId = $('#sender_id').val();
    
    make_chat_design(receive,0,15,1);
    get_message_ajax();
    //page click action
    $('#pagination-here').on("page", function(event, num){
        $("#content").html("Page " + num); 
        $("#pagination").val(num); 
        if($("#active_tab").val() == "receive"){
         make_chat_design(receive,parseInt(parseInt(num) * 15) -15,15,num);
        }else if($("#active_tab").val() == "send"){
         make_chat_design(send,parseInt(parseInt(num) * 15) -15,15,num);
        }else if($("#active_tab").val() == "all"){
         make_chat_design(all,parseInt(parseInt(num) * 15) -15,15,num);
        }
        
    });

    function make_chat_design(data,start,end,current_row){
        
        var remaining_data = parseInt(data.length) - parseInt(start);
        var actual_end = (remaining_data > end) ? end : remaining_data;
        var total_end = parseInt(start) + parseInt(actual_end);
        var current= new Date();
        var i;
        var total_rows = Math.ceil(parseInt(data.length) / parseInt(end));

        $('#pagination-here').bootpag({
            total: total_rows,          
            page: current_row,            
            maxVisible: 5,     
            leaps: true,
            href: "#result-page-{{number}}",
        })
        $("#table011 tr").remove();
        for (i = start; i < total_end; i++){
          var str = data[i].message;
          var $temp = $("<div>").html(str);
          $temp.find("p").each(function() { $(this).replaceWith(this.childNodes); });
          var output = $temp.html();

          $('#table011').append(''+'<tr id="class'+data[i].uid+'" class="chating_list '+data[i].chatTabType+' '+data[i].unreadClass+' ">'+            
                  '<td class="chat_thumb">'+
                    '<a href="{{ url('inbox-chat/') }}/'+data[i].uid+'">'+
                      '<span id="status'+data[i].uid+'" class="unread_msg">'+data[i].readStatus+'</span></div>'+
                      '<div class="inbox_img">'+
                          '<img src="'+data[i].imgurll+data[i].profilePic+'" alt="">'+
                      '</div>'+
                    '</a>'+
                  '</td>'+
                  '<td class="chat_name_wrap">'+
                     '<a href="{{ url('inbox-chat/') }}/'+data[i].uid+'">'+
                      '<div class="chat_name">'+
                          '<div class="inbox-name">'+data[i].name+'</div>'+
                          '<div class="chat_time">'+
                              '<div  class="inbox-date">'+
                                        '<h5 id="date'+data[i].uid+'" >'+timeDifference(current, new Date(data[i].timestamp))+'<h5>'+
                              '<span class="inbox_icons like">'+
                                  '<i class="'+data[i].icon+'"></i>'+
                              '</span>'+                        
                          '</div>'+
                      '</div>'+
                      '<div id="msg'+data[i].uid+'" class="chat_msg">'+((output)? output:"")+'</div>'+
                     '</a>'+
                  '</td>'+            
              '</tr>'+
            '');
        }
    }

    function lastword(words) {
        var n = words.split(" ");
        return n[n.length - 1];    
    }
    
     function thirdlastword(words) {
        var n = words.split(" ");
        return n[n.length - 3];    
    }
    
    

    function getMyHistory(){
        var tabName = 'inbox';
        var chatTab = localStorage.getItem("chatTab");
        if(chatTab){
          tabName = chatTab;                    
          //localStorage.removeItem("chatTab");
          
        }
      
        $('#li'+tabName).addClass("active");
        
        firebase.database().ref('/chat_history/'+adminId).once('value').then(function(snapshot) {
        
          var aa = snapshot.val();
    
          var newarr = [];
          $.each(aa, function (key, val) {
              //addListnerForUnreadMsg(adminId,key);
            //newarr.push(val)
          });
          
          
         /* newarr.sort(function(x, y){
            return y.timestamp - x.timestamp;
          })
         
         
         
        var i=1;
        newarr.forEach(function(childData) {  
           
          var icon = 'fa fa-comments-o';
          if(childData.messageType == 'favorite'){
            icon = 'fa fa-star-o';
          }else if(childData.messageType == 'likes'){
            icon = 'fa fa-heart-o';
          }else if(childData.messageType == 'file'){
            if(childData.message){
                if(childData.message.indexOf('firebasestorage') != -1){
                  childData.message = '<img src="'+childData.message+'" width="80" height="80">';
                }else if(childData.message.indexOf('<img') != -1){
                  childData.message = childData.message;
                }else{
                  childData.message = '<img src="https://moneoangels.com/public/images/profile_image/'+childData.message+'" width="80" height="80">';
                }
                return;
            }
          }else if(childData.messageType == 'text'){
    
          }else if(childData.messageType == 'proposal'){
    
          }
          
    
          var chatTabType = 'all';
          if(childData.historyType == 1){
            if(childData.lastSenderId == adminId){
              chatTabType = 'send';
            }else{
              chatTabType = 'receive';
            }
          }
    
    
          var current= new Date();
          
                      
        
        
                var readStatus = '<p class="read_msg"></p>';
                var unreadClass = '';
        
        
                   
          if(childData.historyLoad == '0'){
            readStatus = '<i class="fa fa-circle"></i>';
            unreadClass = "unreadClass";
          }
                     
          if(chatTabType == 'send'){
            readStatus = '';
          }
          
          if(childData.name){

                  var str1 = childData.name.toLowerCase();
                  var str2 = $('#myInput').val().toLowerCase();
                  var str3 = (childData.message) ? childData.message.toLowerCase() : '';
                  
                  if(childData.profilePic.indexOf('crop_') != -1){
                    var imgurll = '{{ env('PROFILE_MANAGER_ADDR').('/public/thumbnail/') }}';
                  }else{
                    var imgurll = '{{ asset('/public/thumbnail').'/' }}';
                  }
                  
                  //if(childData.message){
                    if(str1.indexOf(str2) != -1 || str3.indexOf(str2) != -1){
                        
                        /*$('#table011').append(''+
                        '<tr id="class'+childData.uid+'" class="chating_list '+chatTabType+' '+unreadClass+' ">'+            
                            '<td class="chat_thumb">'+
                              '<a href="{{ url('inbox-chat/') }}/'+childData.uid+'">'+
                                '<span id="status'+childData.uid+'" class="unread_msg">'+readStatus+'</span></div>'+
                                '<div class="inbox_img">'+
                                    '<img src="'+imgurll+childData.profilePic+'" alt="">'+
                                '</div>'+
                              '</a>'+
                            '</td>'+
                            '<td class="chat_name_wrap">'+
                               '<a href="{{ url('inbox-chat/') }}/'+childData.uid+'">'+
                                '<div class="chat_name">'+
                                    '<div class="inbox-name">'+childData.name+'</div>'+
                                    '<div class="chat_time">'+
                                        '<div  class="inbox-date">'+
                                                  '<h5 id="date'+childData.uid+'" >'+timeDifference(current, new Date(childData.timestamp))+'<h5>'+
                                        '<span class="inbox_icons like">'+
                                            '<i class="'+icon+'"></i>'+
                                        '</span>'+                        
                                    '</div>'+
                                '</div>'+
                                '<div id="msg'+childData.uid+'" class="chat_msg">'+((childData.message)? childData.message:"")+'</div>'+
                               '</a>'+
                            '</td>'+            
                        '</tr>'+
                      '');
                  }
          }
         
          $('#li'+tabName).addClass("active");
          showHideChat(tabName);
        
          
      });*/
    
      
      });
    
    }
    
    getMyHistory();


  //setup before functions
    var typingTimer;                //timer identifier
    var doneTypingInterval = 1000;  //time in ms, 5 second for example
    var $input = $('#myInput');

    //on keyup, start the countdown
    $input.on('keyup', function () {
      clearTimeout(typingTimer);
      typingTimer = setTimeout(doneTyping, doneTypingInterval);
    });

    //on keydown, clear the countdown 
    $input.on('keydown', function () {
      clearTimeout(typingTimer);
    });

    //user is "finished typing," do something
    function doneTyping () {
      $('#table011').html('');
        //getMyHistory()
    }


    localStorage.removeItem("chatTab");
  
  function addListnerForUnreadMsg(adminId,key){
    
    var starCountRef = firebase.database().ref('/chat_history/'+adminId).child(key);
    starCountRef.on('child_changed', function(snapshot) {
      
      var oneMsgVal = snapshot.val();
      var oneMsgkey = snapshot.key;
      
      
      if(oneMsgkey == 'message'){
        $('#msg'+key).html(oneMsgVal);
        $('#status'+key).html('<i class="fa fa-circle"></i>');
      }
      
      if(oneMsgkey == 'messageCount'){
        
      }
      var current= new Date();
      if(oneMsgkey == 'timestamp'){
        $('#date'+key).html(timeDifference(current, new Date(oneMsgVal)));
      }
    });
    
  }
  
    function timeDifference(current, previous) {
    
        var msPerMinute = 60 * 1000;
        var msPerHour = msPerMinute * 60;
        var msPerDay = msPerHour * 24;
        var msPerMonth = msPerDay * 30;
        var msPerYear = msPerDay * 365;
        
        var elapsed = current - previous;
        
        if (elapsed < msPerMinute) {
             return Math.round(elapsed/1000) + ' seconds ago';   
        }
        
        else if (elapsed < msPerHour) {
             return Math.round(elapsed/msPerMinute) + ' minutes ago';   
        }
        
        else if (elapsed < msPerDay ) {
             return Math.round(elapsed/msPerHour ) + ' hours ago';   
        }
    
        else if (elapsed < msPerMonth) {
             return Math.round(elapsed/msPerDay) + ' days ago';   
        }
        
        else if (elapsed < msPerYear) {
             return Math.round(elapsed/msPerMonth) + ' months ago';   
        }
        
        else {
             return Math.round(elapsed/msPerYear ) + ' years ago';   
        }
    }

  function showHideChat(type){
    $('.chating_list').hide();
    if(type == 'inbox'){

        localStorage.setItem('chatTab', 'inbox');
        $("#active_tab").val('receive');
        make_chat_design(receive,0,15,1);
        $('#noMsg').remove();
        if($('.receive').length == 0 && $('.all').length == 0){
          $('#table011').append('<center style="color:red;" id="noMsg">No Inbox Chat Available</center>')
        }

        $('.receive').show();
        $('.all').show();
        $('.send').hide();
    }else if(type == 'Outbox'){

        localStorage.setItem('chatTab', 'Outbox');
        $("#active_tab").val('send');
        make_chat_design(send,0,15,1);
        $('.send').show();

        $('#noMsg').remove();
        if($('.send').length == 0){
          $('#table011').append('<center style="color:red;" id="noMsg">No Outbox Available</center>')
        }

    }else if(type == 'Conversation'){

        localStorage.setItem('chatTab', 'Conversation');
        $("#active_tab").val('all');
        make_chat_design(all,0,15,1);
        $('.all').show();

        $('#noMsg').remove();
        if($('.all').length == 0){
          $('#table011').append('<center style="color:red;" id="noMsg">No Conversation Available</center>')
        }
    }
  }
  $("#profile_popup").removeClass("user_profile_popup"); 

  function get_message_ajax(){
    $.get("inbox_ajax/"+$("#sender_id").val(), function(data){
      var obj = JSON.parse(data);
      window.receive = obj.receive;
      window.send = obj.send;
      window.all = obj.all;
      console.log(obj);
      if(data != ''){
        assign_values();
        //get_message_ajax();
      }
    });
  }

  function assign_values(){
    pagination = parseInt($("#pagination").val());
    end = parseInt(pagination) * 15;
    start = parseInt(end) - 15;
    if($("#active_tab").val() == "receive"){
     make_chat_design(receive,start,end,pagination);
    }else if($("#active_tab").val() == "send"){
     make_chat_design(send,start,end,pagination);
    }else if($("#active_tab").val() == "all"){
     make_chat_design(all,start,end,pagination);
    }
  }
</script>
<!--

<textarea id="chat_file_hidden" style="display:none"></textarea>


<input type="hidden" id="rec_id">
<input type="hidden" id="rec_name">
<input type="hidden" id="rec_img">
<script type="text/javascript">

$(document).ready(function(){
$(".dropdown").click(function(){
$(this).find(".dropdown-menu").slideToggle("fast");
});
});
$(".inbox_outer").on("click", function(event){
var $trigger = $(".dropdown");
if($trigger !== event.target && !$trigger.has(event.target).length){
$(".dropdown-menu").slideUp("fast");
}            
});
</script>-->


@endsection
