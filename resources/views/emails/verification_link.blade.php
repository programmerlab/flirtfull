<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="image/jpeg; charset=utf-8" />
<title>Welcome! Verify your email address to get started</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
<link href="https://fonts.googleapis.com/css?family=Arizonia|Cookie|Montserrat:100,200,300,400,500,600,700,800,900|Poppins:100,200,300,400,500,600,700,800,900" rel="stylesheet"> 
<link href="https://fonts.googleapis.com/css?family=Oswald" rel="stylesheet">   
<style type="text/css">
  body{font-family: 'Montserrat', sans-serif; margin:0; font-size:19px;}
  p{ line-height:34px; color:#888}
  p span{color:#000;}
  table{ width:100%;}
  h6{ margin:15px 0}
</style>
</head>
  <body>
    <div style="width:100%; background: #DF2358; background-size:cover; position: fixed; top: 0; right: 0; bottom: 0; left: 0;overflow-y: auto;">
      <div style="max-width: 600px; margin: 20px auto 0; background:#fff; position:relative">
        <table>
        <tr>
          <td>
          <table align="center">
            <tr>
              <td height="10"></td>
            </tr>
            <tr>
              <td></td>
              <td width="205"><img src="https://flirtfull.com/public/images/logo.png" style="max-width: 100%;"></td>
              <td></td>
            </tr>
            <tr>
              <td height="10"></td>
            </tr>
          </table>
          </td>
        </tr>
        </table>
      </div>
      <div style="max-width: 600px; margin: 0 auto; position:relative; background-image:url('https://flirtfull.com/public/images/tablebanner.jpg'); background-repeat:no-repeat; background-size:cover; background-position: center;">
        <table>
          <tr>
            <td>
              <table align="center">
                <tr><td height="15"></td></tr>
                <tr>
                  <td>
                    <p style="color:#ffffff; font-family:Ubuntu, Helvetica, Arial, sans-serif; font-size:20px; text-align:right; margin-bottom: 0; margin-top: 0; ">Dear [user_name],</p>
                  </td>
                </tr>
                <tr><td height="15"></td></tr>
              </table>
            </td>
          </tr>
        </table>
      </div>

      <div style="max-width: 600px; margin: 0 auto; background:#fff; position:relative">
        <table align="center" style="max-width:550px; margin:0 auto; ">
      <tr>
        <td height="50"></td>
      </tr>
      <tr>
        <td>
          <h4 style="word-wrap:break-word; text-align:center; font-weight:300; font-size:18px; margin:0px; color: #000000; font-family: Ubuntu, Helvetica, Arial, sans-serif; ">Thank you for your registration</h4>
        </td>
      </tr>
      <tr>
        <td height="10"></td>
      </tr>
      <tr>
        <td>
        <h4 style="word-wrap:break-word; text-align:center; font-weight:300; font-size: 15px; line-height: 23px; margin:0 0 15px; 
        color: #555; font-family: Ubuntu, Helvetica, Arial, sans-serif;">
        To activate your account, please click the button below to confirm your email address and get started. 
        </h4> 
        </td>
      </tr>
      <tr>
        <td align="center">
        <a href="[verification_link]" style="text-decoration: none; display:inline-block; padding: 12px 36px;background: #EC155A; color: #fff; border-radius: 2em; font-family: Ubuntu, Helvetica, Arial, sans-serif, Helvetica, Arial, sans-serif;font-size: 16px; text-transform: capitalize;">Activate my account now</a>
        </td>
      </tr>
      <tr>
        <td height="30"></td>
      </tr>
      <tr>
        <td style="border-top: 1px solid #000; height: 1px;">
        </td>
      </tr>
      <tr>
        <td height="20"></td>
      </tr>
      <tr>
        <td>
          <h4  style="word-wrap:break-word; text-align:center; font-weight:300; font-size: 15px; line-height: 23px; 
          margin:0 0 15px; color: #555; font-family: Ubuntu, Helvetica, Arial, sans-serif;">
          if you have trouble to click on button to activate your account then please copy this url to your browser.
          </h4> 
          </td>
      </tr>
      <tr>
        <td align="center">
          <a href="#" style="font-size: 8px; color: #EC155A; display: block; text-align: center; line-height: 24px; word-wrap:break-word;">[verification_link]</a>
        </td>
      </tr>
      <tr>
        <td height="15"></td>
      </tr>
      <tr>
        <td align="center">
          <h4 style=" margin:0; color: #000000; font-weight: 500; font-size: 16px;">Regards</h4>
        </td>
      </tr>
      <tr>
        <td align="center">
          <p style="margin:0; color: #000000; font-weight: 500; font-size: 14px; ">Team <a style="color: #000000; font-weight: 500; text-decoration: none;" href="[site_url]">flirtfull.com</a></p>
        </td>
      </tr>
      <tr>
        <td height="40"></td>
      </tr>
        </table>
      </div>

    <div style="max-width: 600px; margin: 0 auto; position:relative">
        <table>
          <tr>
            <td height="10"></td>
          </tr>
          <tr>
            <td>
              <p style="margin:0; color: #ffffff; font-size: 13px; line-height: 22px; word-wrap:break-word;"> Copyright © 2019 Flirtfull, All Rights Reserved</p>
             </td>
          </tr>
          <tr>
             <td>
              <p style="margin:0; color:#ffffff; font-size: 13px; line-height: 22px; word-wrap:break-word;">You subscribed to our Newsletter via our website, <a href="#" style="color:#ffffff; text-decoration: none;">flirtfull.com</a></p>
             </td>
          </tr>
           <tr>
             <td>
              <a href="#" style="margin:0; color: #ffffff; font-size: 13px; line-height: 22px; word-wrap:break-word;">Unsubscribe from this list</a>
             </td>
          </tr>
        </table>
      </div>
    
    </div>
  </body>
</html>
