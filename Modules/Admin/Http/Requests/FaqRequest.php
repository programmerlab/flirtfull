<?php

declare(strict_types=1);

namespace Modules\Admin\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FaqRequest extends FormRequest
{
    /**
     * The metric validation rules.
     *
     * @return array
     */
    public function rules()
    {        
        //if ( $metrics = $this->metrics ) {
        switch ($this->method()) {
                case 'GET':
                case 'DELETE': {
                        return [];
                    }
                case 'POST': {
                        return [
                            'question'      => 'required',
                            'answer'       => 'required'                            
                        ];
                    }
                case 'PUT':
                case 'PATCH': {
//                    if ($user = $this->user) {
                        return [
                           'question'      => 'required',
                            'answer'       => 'required'     
                        ];
                    //}
                }
                default:break;
            }
        //}
    }

    /**
     * The
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
