<div class="page-container" style="min-height:360px">

<!-- Page content -->
<div class="page-content">

<!-- Main sidebar -->
<div class="sidebar sidebar-main sidebar-default">
<div class="sidebar-content">
<!-- Main navigation -->
<div class="sidebar-category sidebar-category-visible">
    <div class="category-content no-padding">
        <ul class="navigation navigation-main navigation-accordion">
        <!-- Main -->
            <li class="navigation-header"><span>Main Navigation</span> <i class="icon-menu" title="" data-original-title="admin"></i></li>
            <li class="active"><a href="{{url('admin')}}" class="legitRipple"><i class="icon-home4"></i> <span>Dashboard</span></a></li> 
            

            <li class="{{($viewPage=='Users')?'active':''}}">
                <a href="#" class="has-ul legitRipple"><i class="icon-stack2"></i> <span>Manage Players</span>
                    <span class="legitRipple-ripple"></span></a>
                <ul class="hidden-ul" style="display: {{($viewPage=='Users')?'block':'none'}};">
                    <li><a href="{{route('user.create')}}" class="legitRipple"><span class=" icon-arrow-right13"></span>Add Player</a></li> 
                    <li>
                        <a href="{{route('grid')}}" class="legitRipple"> <span class=" icon-arrow-right13"></span>Grid Players</a>
                    </li> 
                    <li><a href="{{route('user')}}" class="legitRipple"><span class=" icon-arrow-right13"></span>List Players</a>
                    </li> 
                    <!-- <li><a href="{{ url('admin/modified-texts')}}" class="legitRipple"><span class=" icon-arrow-right13"></span>Modified texts</a>
                    </li>
                    <li><a href="{{ url('admin/modified-photos')}}" class="legitRipple"><span class=" icon-arrow-right13"></span>Modified photos</a>
                    </li> -->
                    <li><a href="{{ url('admin/fake')}}" class="legitRipple"><span class=" icon-arrow-right13"></span>fake</a>
                    </li>
                    <li><a href="{{ url('admin/real')}}" class="legitRipple"><span class=" icon-arrow-right13"></span>real</a>
                    </li>
                    <li><a href="{{ url('admin/unconfirmed')}}" class="legitRipple"><span class=" icon-arrow-right13"></span>Unconfirmed</a>
                    </li>
                    <li><a href="{{ url('admin/deleted')}}" class="legitRipple"><span class=" icon-arrow-right13"></span>Deleted</a>
                    </li>
                     <!--<li><a href="{{route('role')}}" class="legitRipple"><span class=" icon-arrow-right13"></span>Role & Permission</a></li> -->
                </ul>
            </li> 

            <li class="{{($viewPage=='Page')?'active':''}}">
                <a href="#" class="has-ul legitRipple"><i class="icon-stack2"></i> <span>Manage Pages </span>
                    <span class="legitRipple-ripple"></span></a>
                <ul class="hidden-ul" style="display: {{($viewPage=='Page')?'block':'none'}};">
                    <li><a href="{{route('page')}}" class="legitRipple"><span class=" icon-arrow-right13"></span>Page</a></li>  
                </ul>

            </li>

            <li class="{{($viewPage=='Faq')?'active':''}}">
                <a href="#" class="has-ul legitRipple"><i class="icon-stack2"></i> <span>Faq </span>
                    <span class="legitRipple-ripple"></span></a>
                <ul class="hidden-ul" style="display: {{($viewPage=='Faq')?'block':'none'}};">
                    <li><a href="{{route('faq')}}" class="legitRipple"><span class=" icon-arrow-right13"></span>Faq</a></li>  
                </ul>

            </li>
            
            <li class="{{($viewPage=='Plan')?'active':''}}">
                <a href="#" class="has-ul legitRipple"><i class="icon-stack2"></i> <span>Manage Plans</span>
                    <span class="legitRipple-ripple"></span></a>
                <ul class="hidden-ul" style="display: {{($viewPage=='Plan')?'block':'none'}};">
                    <li><a href="{{route('credit')}}" class="legitRipple"><span class=" icon-arrow-right13"></span>Credits</a></li>
                    <!--<li><a href="{{route('coin')}}" class="legitRipple"><span class=" icon-arrow-right13"></span>Coins</a></li>-->
                    <li><a href="{{route('diamond')}}" class="legitRipple"><span class=" icon-arrow-right13"></span>Diamonds</a></li>
                    <li><a href="{{route('premium')}}" class="legitRipple"><span class=" icon-arrow-right13"></span>Premiums</a></li>                         
                </ul>
            </li>

            <li class="{{($viewPage=='Payment')?'active':''}}">
                <a href="#" class="has-ul legitRipple"><i class="icon-stack2"></i> <span>Payments</span>
                    <span class="legitRipple-ripple"></span></a>
                <ul class="hidden-ul" style="display: {{($viewPage=='Payment')?'block':'none'}};">
                    <li><a href="{{route('payment')}}" class="legitRipple"><span class=" icon-arrow-right13"></span>Payment History</a></li>
                                            
                </ul>
            </li>
                
            <li class="{{($viewPage=='Proposal')?'active':''}}">
                <a href="#" class="has-ul legitRipple"><i class="icon-stack2"></i> <span>Manage Proposals</span>
                    <span class="legitRipple-ripple"></span></a>
                <ul class="hidden-ul" style="display: {{($viewPage=='Proposal')?'block':'none'}};">
                    <li><a href="{{route('proposalCategory')}}" class="legitRipple"><span class=" icon-arrow-right13"></span>Categories</a></li>
                    <li><a href="{{route('proposal')}}" class="legitRipple"><span class=" icon-arrow-right13"></span>Proposals</a></li>
                                            
                </ul>
            </li>
                
            <li class="{{($viewPage=='Countries')?'active':''}}">
                <a href="#" class="has-ul legitRipple"><i class="icon-stack2"></i> <span>Manage Countries</span>
                    <span class="legitRipple-ripple"></span></a>
                <ul class="hidden-ul" style="display: {{($viewPage=='Countries')?'block':'none'}};">
                    <li><a href="{{route('country')}}" class="legitRipple"><span class=" icon-arrow-right13"></span>Country List</a></li>
                                            
                </ul>
            </li>

            <li class="{{($viewPage=='Reward')?'active':''}}">
                <a href="#" class="has-ul legitRipple"><i class="icon-stack2"></i> <span>Rewards</span>
                    <span class="legitRipple-ripple"></span></a>
                <ul class="hidden-ul" style="display: {{($viewPage=='Reward')?'block':'none'}};">
                    <li><a href="{{route('reward')}}" class="legitRipple"><span class=" icon-arrow-right13"></span>Reward</a></li>
                                            
                </ul>
            </li>
            

            <li class="{{($viewPage=='Template')?'active':''}}">
                <a href="#" class="has-ul legitRipple"><i class="icon-stack2"></i> <span>Email Template</span>
                    <span class="legitRipple-ripple"></span></a>
                <ul class="hidden-ul" style="display: {{($viewPage=='Template')?'block':'none'}};">
                    <li><a href="{{route('template')}}" class="legitRipple"><span class=" icon-arrow-right13"></span>Templates</a></li>
                                            
                </ul>
            </li>

              <li class="{{($viewPage=='Setting')?'active':''}}">
                <a href="{{route('setting')}}" class="has-ul legitRipple"><i class="icon-stack2"></i> <span>Website Settings</span>
                    <span class="legitRipple-ripple"></span></a>
                 <ul class="hidden-ul" style="display: {{($viewPage=='Setting')?'block':'none'}};">
                <li><a href="{{route('setting')}}" class="legitRipple"><span class=" icon-arrow-right13"></span> Settings</a></li>  
                </ul>
            </li>
            <!--<li class=""><a href="{{url('admin/partner')}}" class="legitRipple"><i class="icon-home4"></i> <span>Partner</span></a></li>   
            <li class=""><a href="{{url('admin/track')}}" class="legitRipple"><i class="icon-home4"></i> <span>Turnover</span></a></li> -->
        </ul>
    </div>
</div>          
</div>
</div>
 <div class="content-wrapper">


