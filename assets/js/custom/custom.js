$(document).ready(function() {
  //for login
  $('#login_modal').submit(function(e) {
    e.preventDefault();
    var login_email = $('#login_email').val();
    var login_password = $('#login_password').val();
    var err =0;
    var mail_validation = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if(login_email.trim() == "")
    {
      $('.email-wrong').text('Please enter email');
      err++;
    }
    else if(!mail_validation.test(login_email)){
      $('.email-wrong').text('Please enter valid email');
      err++;
    }
    else{
     $('.email-wrong').text('');
    }
    
    if(login_password.trim() == "")
    {
      $('.both-wrong').text('');
      $('.password-wrong').text('Please enter password');
      err++;
    }
    else{
     $('.password-wrong').text('');
    }

    if(err == 0)
    { 
      $('.loader').show();
      $.ajax({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: '<?php echo url("/")?>/login',
        data: {
            email: login_email ,
            password: login_password
        },
        type: "POST",
        dataType : "json",
        success: function( data ) {
            //alert(JSON.stringify(data));
            //alert(data.errors.password);
            $('.loader').hide();
            if(data.success)
            {
              window.location.reload();
              //window.location.href = "<?php echo url('/')?>/search";
            }
            else if(data.errors.email){
              $('.email-wrong').text(data.errors.email);
            }
            else if(data.errors.password){
              $('.both-wrong').text('');
              $('.password-wrong').text(data.errors.password);
            }
            else if((data.errors.email) && (data.errors.password)){
              $('.email-wrong').text(data.errors.email);
              $('.password-wrong').text(data.errors.password);
            }
            else{
              $('.password-wrong').text('');
              $('.both-wrong').text(data.errors);
            }
          },
          error: function( xhr, status, errorThrown ) {
            
          }
      });
    }
    else{
      //$('.both-wrong').text('Please fill all the details');
    }
    
  });

  //for signup
  $('#signup_modal').submit(function(e){
    e.preventDefault();
    
	var gender = $("input:radio.gender_classs:checked").val();
    var seeking = $("input:radio.seeking_classs:checked").val();
	
	var email = $('#signup_email').val();
    var password = $('#signup_password').val();
    
	//age
    var month = $('#month').val();
    var day = $('#day').val();
    var year = $('#year').val();
    
    var partnerid = $('#partnerid').val();
	
    var mail_validation = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
   
    var error =0;
    if(email.trim() == "")
    {
      $('.signup-email-wrong').text('Please enter email');
      $("#signup_email").css("border-color", "red");
      error++;
    }
    else if(!mail_validation.test(email)){
      $('.signup-email-wrong').text('Please enter valid email');
      $("#signup_email").css("border-color", "red");
      error++;
    }
    else{
      $('.signup-email-wrong').text('');
      $("#signup_email").css("border-color", "#eee");
    }
    if(password.trim() == "")
    {
      $('.signup-password-wrong').text('Please enter password');
      $("#signup_password").css("border-color", "red");
      error++;
    }
    else{
      $('.signup-password-wrong').text('');
      $("#signup_password").css("border-color", "#eee");
    }

    if(gender == '' || typeof gender === "undefined")
    {
      $('.gender').show();
      $('.gender').text('Please select gender');
      error++;
    }
    else{
      $('.gender').hide();
      $('.gender').text('');
    }

    if(seeking == '' || typeof seeking === "undefined")
    {
      $('.seeking').show();
      $('.seeking').text('Please select seeking');
      error++;
    }
    else{
      $('.seeking').hide();
      $('.seeking').text('');
    }

    if(year == '')
    {
      $('.bith_date').show();
      $('.birth_date').text('Select your date of birth');
      error++;
    } 

    if(month == '')
    {
      $('.bith_date').show();
      $('.birth_date').text('Please select birth date');
      error++;
    }
    
    
    if(day == '')
    {
      $('.bith_date').show();
      $('.birth_date').text('Please select birth date');
      error++;
    }
    

    if(day != '' && month != '' && year != '')
    {
      $('.bith_date').hide();
      $('.birth_date').text('');
    }

    if($('#accept_cond').is(':not(:checked)'))
    {
      $('.accept').show();
      $('.accept').text('Please accept conditions');
      error++;
    }
    else{
      $('.accept').hide();
      $('.accept').text('');
    }

    if(error == 0)
    {
      $('.signup-process').show();
      $.ajax({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: '<?php echo url("/")?>/signup',
        data: {
            email: email ,
            password: password,
            month: month,
            day: day,
            year: year,
            gender: gender,
            seeking: seeking,
            partner_id : partnerid
        },
        type: "POST",
        dataType : "json",
        success: function( data ) {
            $('.loader').hide();
            if(data.success)
            {
              $('.signup-process').hide();
              location.reload();
            }
            if(data.errors.name){
              $('.signup-process').hide();
              $('.signup-name-wrong').text(data.errors.name);
              $("#signup_name").css("border-color", "red");
            }
            else{
              $("#signup_name").css("border-color", "#eee");
            }
           
            
            if(data.errors.email){
              $('.signup-process').hide();
              $.each( data.errors.email, function( i, val ) {
                  $('.signup-email-wrong').append('<p class="error-message">'+val+'</p>');
              });
              $("#signup_email").css("border-color", "red");
            }
            else{
              $("#signup_email").css("border-color", "#eee");

            }
            if(data.errors.password){
              //clear_signup_errors();
              $('.signup-process').hide();
              $.each( data.errors.password, function( i, val ) {
                  $('.signup-password-wrong').append('<p class="error-message">'+val+'</p>');
              });
              $("#signup_password").css("border-color", "red");
              //$('.password-wrong').text(data.errors.password);
            }
            else
            {
              $("#signup_password").css("border-color", "#eee");
            }
          },
          error: function( xhr, status, errorThrown ) {
            $('.signup-process').hide();
            alert( "Sorry, there was a problem!" );
          }
      });
    }
    else{
      //$('.all-wrong').text('Please fill all the details');
    }

  });
});

//forgot passowrd
	$(document).ready(function() 
	{
		
		$('#forgot').submit(function(e) 
		{
			e.preventDefault();
			var forgot_email = $('#forgot_email').val();
			
			var err =0;
			var mail_validation = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
			if(forgot_email.trim() == "")
			{
			  $('.forgot-email-wrong').text('Please enter email');
			  err++;
			}
			else if(!mail_validation.test(forgot_email)){
			  $('.forgot-email-wrong').text('Please enter valid email');
			  err++;
			}
			else{
			 $('.forgot-email-wrong').text('');
			}
			

			if(err == 0)
			{ 
			  $('.forgot-process').show();
			  $.ajax({
				headers: {
				  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				url: '<?php echo url("/")?>/forgetPassword',
				data: {
					email: forgot_email ,
				},
				type: "POST",
				dataType : "json",
				success: function( data ) {
					$('.forgot-process').hide();
					if(data.status)
					{
					  $('.forgot-email-success').text(data.message);
					  $('.forgot-email-wrong').text('');
					  
					  setTimeout(function(){        
						$("#forget_popup").removeClass("open_popup"); 
						$("#s_login_popup").addClass("open_popup"); 
						}, 3000);
					  
					}
					else if(data.message){
					  $('.forgot-email-wrong').text(data.message);
					  $('.forgot-email-success').text('');
					}
					
				  },
				  error: function( xhr, status, errorThrown ) {
					$('.forgot-process').hide();
					console.log( "Sorry, there was a problem!" );
				  }
			  });
			}
			else{
			}
			
		});
	});