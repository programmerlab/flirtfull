<?php $__env->startSection('content'); ?>
<style type="text/css">
  .flipster{
    visibility: hidden;
  }
</style>
  <section class="search_list_outer search_detail credits_page"> 
         <div class="container">
            <div class="row">
               <div class="col-md-12">               
                              <h4 class="modal-title text-center" id="credit_title">Credits: Buy credits to send messages and proposals. </h4>  
                                         
                                <?php if( isset($users) && ($users->campaign_id == 2) && (time() <= (strtotime($users->profile_activated_at) + (3600 * 24 * 7))) ): ?>
                                <p id="double_offer">     
                                Hurry!! Purchase Credits before <b><?php echo e(date('d-M-Y',(strtotime($users->profile_activated_at) + (3600 * 24 * 7)))); ?></b> & you will get double credits.
                                </p>
                                <?php endif; ?>    
                                
                                  <?php
                                  $helper = new Helper();
                                  $diamonds = $helper->getAllDiamonds();
                                  $credits = $helper->getAllCredits();
                                  $coins = $helper->getAllCoins();
                                  $premiums = $helper->getAllPremiums();                
                                  ?>  

                            <div class="row">
                                <div class="col-md-12">
                                  <div class="flipster_tab menulist">
                                    <ul id="active_plan">
                                    </ul>
                                  </div>
                                  <!-- tab content-->
                                  <div class="" id="">
                                      <div class="wallet_text">
                                        <h4>Get <span class="prem_days" id="prem_days">30</span> Days Premium Access for FREE</h4>
                                      </div>
                                      <div class="f_flipster_wrap">
                                        <div class="flipster">
                                           <ul>
                                            <?php $__currentLoopData = $credits; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $credit): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                           <li>
                                              <div class="upgrade_inner ">
                                                  <div class="upgrade-img">
                                                    <!-- <img src="<?php echo e(asset('public')); ?>/thumbnail/nolemoniquep1.jpg"> -->
                                                    <img src="<?php echo e($credit->image); ?>" onerror="this.src="<?php echo e(asset('public')); ?>/thumbnail/nolemoniquep1.jpg">
                                                     <!-- offer banner -->
                                                    <div class="credit_bnr">
                                                      <img src="<?php echo e(asset('public')); ?>/images/icons/offer_banner.png">
                                                      <div class="credit_bnr_txt">
                                                       
                                                        <span class="bnr_txt_1"><?php echo e($credit->free_premium); ?></span>
                                                        <span class="bnr_txt_2">days free premium</span>
                                                      </div>
                                                    </div>
                                                    <!-- offer banner -->
                                                  </div>
                                                  <div class="upgrade_inner_info"><h2><?php echo e($credit->qty); ?> <span>Credit</span></h2>
                                                  <p class="amount_p"><span class="amount"><span class="currency">£</span><?php echo e($credit->price); ?></span></p>
                                                  <p>£ <?php echo e(round(($credit->price/$credit->qty),2)); ?> / Message</p>
                                                  <a href="#" class="upgrade-btn" id="credit<?php echo e($credit->id); ?>" onclick="select_plan('credits',<?php echo e($credit->id); ?>,<?php echo e($credit->free_premium); ?>);">select <i class="fa fa-check"></i></a></div>
                                                </div>
                                           </li>
                                              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </ul>
                                        </div>
                                      </div>
                                  </div>
                                  <!-- tab content-->
                                  
                                </div>
                              </div>
                                  
                              <div class="pymnt-form">
                                  <div class="row">
                                      <div class="col-md-5 col-sm-12 payment_right">
                                          <?php if(Auth::check() && Auth::user()->status == 1): ?>  
                                          <form id="purchase_credits" action="">
                                          <?php else: ?>
                                          <form id="" action="">              
                                              <?php endif; ?>              
                                             <input type="hidden" id="currency" value="GBP">
                                             <input type="hidden" id="amount" value="10.00">
                                              <div class="row">
                                                  <div class="col-md-12">
                                                    <!-- <div class="form-group">
                                                      <input type="text" class="form-control" id="" placeholder="Card number">
                                                    </div> -->
                                                   </div>
                                                 </div>                       
                                              <div class="pay_radio">
                                                 <input type="hidden" name="plan_type" id="plan_type">    
                                                 <input type="hidden" name="plan" id="plan" >
                                                 <input type="hidden" id="payment_method" name="payment_method" value="flexpay">
                                                 <!--<div class="selectbox">
                                                   <select id="paynl_id" name="paynl_id"></select>
                                                 </div>-->
                                              </div>
                                              <span id="plan_error" style="display:none; color:red;">Please Select Plan</span>
                                               <?php if(Auth::check() && Auth::user()->status == 1): ?>  
                                              <button  class="btn btn-default" id="purchase_credit">Purchase</button>
                                              <?php else: ?>
                                              <button class="btn btn-default" data-toggle="modal" data-target="#notverifyModal">Purchase</button>    
                                              <?php endif; ?>
                                              
                                          </form>
                                              
                                          </div>
                                      <div class="col-md-7 col-sm-12 col-xs-12">
                                        <ul>
                                            <li><a href="#"><img src="<?php echo e(asset('public')); ?>/images/visa.png"></a></li>                            
                                              <li><a href="#"><img src="<?php echo e(asset('public')); ?>/images/master_card.png"></a></li>
                                              <li><a href="#"><img src="<?php echo e(asset('public')); ?>/images/amex.png"></a></li>
                                             <!--  <li><a href="#"><img src="<?php echo e(asset('public')); ?>/images/paypal.png"></a></li>  -->   
                                          </ul>
                                          <p>All payments are done to and processed by Oddigits BV. To protect your privacy, your bank statement will say: "DTN Oddigits". All prices include all applicable taxes.</p>
                                          <p id="premium_footer">We will not renew your subscription or rebill your card.</p>
                                          
                                      </div>
                                      <!--<button  class="btn btn-default" id="purchase_credi">Purchase</button>    -->
                                  </div>
                                
                                  </div>
                                <div class="row text-center">
                                  <div class="pypl-btn" id="paypal">
                                     <button type="button"><span class="pay">Pay</span><span class="pal">Pal</span></button>
                                  </div>
                                </div>
                              </div>
               </div>
            </div>
         </div>
      </section>
       <script type="text/javascript">
        $(window).load(function() {
          $(".flipster").css({'visibility':'visible'});
        })
      </script>
<?php $__env->stopSection(); ?>   
<?php echo $__env->make('layout.default', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>