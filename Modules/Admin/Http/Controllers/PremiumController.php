<?php
declare(strict_types=1);

namespace Modules\Admin\Http\Controllers;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Redirect;
use Input;
use Modules\Admin\Http\Requests\PremiumRequest;
use Modules\Admin\Models\Premium;
use Route;
use View;
use Session;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Datatables;


/**
 * Class AdminController
 */
class PremiumController extends Controller
{
    /**
     * @var  Repository
     */

    /**
     * Displays all admin.
     *
     * @return \Illuminate\View\View
     */
    public function __construct()
    {
        $this->middleware('admin');
        View::share('viewPage', 'Plan');
        View::share('helper', new Helper);
        View::share('heading', 'Premiums');
        View::share('route_url', route('premium'));

        $this->record_per_page = Config::get('app.record_per_page');
    }

    protected $premiums;

    /*
     * Dashboard
     * */

    public function index(Premium $premium, Request $request)
    {        
        $page_title  = 'Premiums';
        $page_action = 'Premiums';
        

        if ($request->ajax()) {
            $id           = $request->get('id');
            $status       = $request->get('status');
            $premium         = Premium::find($id);
            $s            = ($status == 1) ? $status=0:$status=1;
            $premium->status = $s;
            $premium->save();
            echo $s;

            exit();
        }
        
        $premiums = Premium::orderBy('id', 'desc')->where('status', '1')->where('is_deleted', '0')->Paginate($this->record_per_page);
        //print_r($premiums); die;        

        $js_file = ['common.js','bootbox.js','formValidate.js'];

        return view('admin::premiums.premium.index', compact('js_file', 'status', 'premiums', 'page_title', 'page_action'));
    }


    public function anyData()
    {
        return Datatables::of(Premium::query())->make(true);
    }

    public function test_table(Premium $premium, Request $request)
    {        
        $page_title  = 'Admin Premium';
        $page_action = 'Admin Premium';
        

        if ($request->ajax()) {
            $id           = $request->get('id');
            $status       = $request->get('status');
            $premium         = Premium::find($id);
            $s            = ($status == 1) ? $status=0:$status=1;
            $premium->status = $s;
            $premium->save();
            echo $s;

            exit();
        }
        // Search by name ,email and group
        $search    = Input::get('search');
        $status    = Input::get('status');
        $role_type = Input::get('role_type');

        if ((isset($search) && !empty($search)) or  (isset($status) && !empty($status)) or !empty($role_type)) {
            $search = isset($search) ? Input::get('search') : '';

            $premiums = Premium::where(function ($query) use ($search,$status,$role_type) {
                if (!empty($search)) {
                    $query->Where('first_name', 'LIKE', "%$search%")
                        ->OrWhere('last_name', 'LIKE', "%$search%")
                        ->OrWhere('email', 'LIKE', "%$search%");
                }

                if (!empty($status)) {
                    
                    $status =  ($status == 'active')?1:0;
                    $query->Where('status', $status);
                }

                if ($role_type) {
                    $query->Where('role_type', $role_type);
                }
            })->where('role_type', 3)->Paginate($this->record_per_page);
        } else {
            $premiums = Premium::orderBy('id', 'desc')->where('role_type', 3)->where('is_deleted', 0)->Paginate($this->record_per_page);
        }
        $roles = Roles::all();

        $js_file = ['common.js','bootbox.js','formValidate.js'];

        return view('admin::premiums.premium.test_table', compact('js_file', 'roles', 'status', 'page_title', 'page_action', 'roles' ,'role_type'));
    }

    /*
     * create Group method
     * */

    public function create(Premium $premium)
    {
        $page_title  = 'Premium';
        $page_action = 'Create Premium';
        
        $js_file     = ['common.js','bootbox.js','formValidate.js'];


        return view('admin::premiums.premium.create', compact('js_file', 'premium', 'page_title', 'page_action'));
    }

    /*
     * Save Group method
     * */

    public function store(PremiumRequest $request, Premium $premium)
    {
        //echo "<pre>"; print_r(Input::all()); die;
        $premium->fill(Input::all());
        
        
        $action = $request->get('submit');        
        
        
        $premium->save();
        
        $js_file = ['common.js','bootbox.js','formValidate.js'];

        return Redirect::to(route('premium'))
            ->with('flash_alert_notice', 'New record successfully created.');
    }

    /*
     * Edit Group method
     * @param
     * object : $premium
     * */

    public function edit(Premium $premium)
    {
        //echo "<pre>"; print_r($premium); die;
        $page_title  = 'Premium';
        $page_action = 'Show Premium';                
        
        $js_file     = ['common.js','bootbox.js','formValidate.js'];
                
        //echo "<pre>"; print_r($premium); die;
        return view('admin::premiums.premium.edit', compact('js_file', 'premium', 'page_title', 'page_action'));
    }

    public function update(Request $request, Premium $premium, PremiumRequest $premiumrequest)
    {        
        //echo "<pre>"; print_r($premium->password); die;        
        $premium->fill(Input::all());
        $action          = $request->get('submit');
        $premium->save();
                
        if ($request->get('role') == 3) {
            $Redirect = 'clientpremium';
        } else {
            $Redirect = 'premium';
        }

        return Redirect::to(route($Redirect))
            ->with('flash_alert_notice', 'Record successfully updated.');
    }
    /*
     *Delete Premium
     * @param ID
     *
     */
    public function destroy(Request $request, $premium)
    { //print_r($premium);
        //echo $premium->id;die;
        $premium->is_deleted = 1;
        $premium->save();
        //$premium->delete();
        return Redirect::to(route('premium'))
            ->with('flash_alert_notice', 'Record  successfully deleted.');
    }

    public function show(Premium $premium)
    {
    }
    
    public function image_delete(Premium $premium, Request $request, Gallery_image $gallery_image)
    {
        //echo $request->ajax('id');        
            $id           = $request->get('id');            
            $status       = $request->get('is_deleted');
            $premium         = Gallery_image::find($id);
            $s            = ($status == 1) ? $status=0:$status=1;
            $premium->is_deleted = $s;
            $premium->save();
            echo $s;

            exit();        
    }
    
    public function delete_image(Premium $premium, Request $request, Gallery_image $gallery_image)
    {
        //echo $request->ajax('id');        
            $id           = $request->get('id');
            $premiumid           = $request->get('premiumid');
            DB::table('gallery_images')->where('id', '=', $id)->delete();
            return Redirect::to(url('admin/premium/'.$premiumid.'/edit'))
                ->with('flash_alert_notice', 'Image successfully deleted.');
            exit();        
    }
    
    public function search1(Premium $premium, Request $request)
    {
        //echo $request->ajax('id');        
            $country      = $request->get('country');
            $age      = $request->get('age');
            $gender      = $request->get('gender');
            $status      = $request->get('status');
            
            
            $premiums = Premium::where(function ($query) use ($country,$age,$gender,$status) {
                if (!empty($country)) {
                    $query->Where('country', 'LIKE', "%$country%");                        
                }

                if (!empty($age)) {
                    $age = explode('_',$age);
                    $query->Where('age', '>=', $age[0]);
                    $query->Where('age', '<=', $age[1]);
                }

                if ($gender) {
                    $query->Where('gender', $gender);
                }
                
                if (!empty($status)) {
                    $status = ($status == 'yes') ? '1' : '0';
                    $query->Where('status', $status);
                }
            })->where('role_type', 3)->Paginate(18);
            
            //echo "<pre>"; print_r($premiums); die;            
            $roles = Roles::all();

        $js_file = [];

        return view('admin::premiums.premium.index', compact('js_file', 'roles', 'country', 'premiums', 'page_title', 'page_action', 'roles', 'role_type')); 
    }
    
    public function search(Premium $premium, Request $request)
    {      $last_part = $request->get('last_part'); 
        //echo $request->ajax('id');        
            $country      = $request->get('country');
            $age      = $request->get('age');
            $gender      = $request->get('gender');
            $status      = $request->get('status');
            $relationship      = $request->get('relationship');
            $photo      = $request->get('photo');
            $appearance      = $request->get('appearance');
            $profile_type      = $request->get('profile_type');
            $adult_level      = $request->get('adult_level');
            
            //DB::enableQueryLog();
            
            $reslt = DB::table('premiums')
                    ->join('premium_details', 'premiums.id', '=', 'premium_details.premium_id')            
                    ->select('premiums.*', 'premium_details.relationship');
            
            if (!empty($country)) {
                $reslt->Where('premiums.country', 'LIKE', "%$country%");                        
            }
                
            if (!empty($age)) {
                $age = explode('_',$age);
                $reslt->Where('premiums.age', '>=', $age[0]);
                $reslt->Where('premiums.age', '<=', $age[1]);
            }
            
            if ($gender) {
                $reslt->Where('premiums.gender', $gender);
            }
            
            if ($profile_type) {
                $reslt->Where('premiums.profile_type', $profile_type);
            }
            
            if ($adult_level) {
                $reslt->Where('premiums.adult_level', $adult_level);
            }
            
            if ($appearance) {
                $reslt->Where('premiums.appearance', $appearance);
            }
            
            if (!empty($status)) {
                $status = ($status == 'yes') ? '1' : '0';
                $reslt->Where('premiums.status', $status);
            }
            
            if (isset($photo)) {
                if($photo == '1'){
                    $reslt->Where('premiums.profile_image', '!=',  '');
                }
                if($photo == '0'){
                    $reslt->Where('premiums.profile_image','');
                    //$reslt->or_Where('premiums.profile_image', '=',  'default.png');
                }
            }
            
            if ($relationship) {
                $reslt->Where('premium_details.relationship', $relationship);
            }

            if($last_part == 'deleted') {
                $page_title  = 'Player';
                $page_action = 'Deleted Players';
                $heading = 'Deleted Players';
                $reslt->Where('premiums.is_deleted', '=',  '1');
            }
            else if($last_part == 'real') {
                $page_title  = 'Player';
                $page_action = 'Real Players';
                $heading = 'Real Players';
                $reslt->Where('premiums.fake', '=',  '0');
                $reslt->Where('premiums.is_deleted', '=',  '0');
            }
            else if($last_part == 'fake') {
                $page_title  = 'Player';
                $page_action = 'Fake Players';
                $heading = 'Fake Players';
                $reslt->Where('premiums.fake', '=',  '1');
                $reslt->Where('premiums.is_deleted', '=',  '0');
            }
            else if($last_part == 'unconfirmed') {
                $page_title  = 'Player';
                $page_action = 'Unconfirmed Players';
                $heading = 'Unconfirmed Players';
                $reslt->Where('premiums.status', '=',  '0');
                $reslt->Where('premiums.is_deleted', '=',  '0');
            }
                
            $premiums = $reslt->where('premiums.role_type', 3)->Paginate(30);
            
            //echo "<pre>"; print_r($premiums); die;            
            $roles = Roles::all();

        $js_file = [];

        $search_slug = 'list-search';

        return view('admin::premiums.premium.index', compact('js_file', 'roles', 'country', 'premiums', 'page_title', 'page_action','heading',  'roles', 'role_type', 'search_slug')); 
    }
    
    public function setProfileImage(Premium $premium, Request $request, Gallery_image $gallery_image)
    {        
            $id           = $request->get('id');
            $premiumid           = $request->get('premiumid');
            
            $premium = DB::table('premiums')->where('id', $premiumid)->first();            
            $old_image = $premium->profile_image;
            
            $gallery = DB::table('gallery_images')->where('id', $id)->first();            
            $new_image = $gallery->gallery_image;
            
            DB::table('premiums')
            ->where('id', $premiumid)
            ->update(array('profile_image' => $new_image));
            
            DB::table('gallery_images')
            ->where('id', $id)
            ->update(array('gallery_image' => $old_image));
                        
            return Redirect::to(url('admin/premium/'.$premiumid.'/edit'))
                ->with('flash_alert_notice', 'Image Set as Profile successfully ');
            exit();        
    }


    public function deleted(Premium $premium, Request $request)
    {        
        $page_title  = 'Player';
        $page_action = 'Deleted Players';
        $heading = 'Deleted Players';
        

        if ($request->ajax()) {
            $id           = $request->get('id');
            $status       = $request->get('status');
            $premium         = Premium::find($id);
            $s            = ($status == 1) ? $status=0:$status=1;
            $premium->status = $s;
            $premium->save();
            echo $s;

            exit();
        }
        // Search by name ,email and group
        $search    = Input::get('search');
        $status    = Input::get('status');
        $role_type = Input::get('role_type');

        if ((isset($search) && !empty($search)) or  (isset($status) && !empty($status)) or !empty($role_type)) {
            $search = isset($search) ? Input::get('search') : '';

            $premiums = Premium::where(function ($query) use ($search,$status,$role_type) {
                if (!empty($search)) {
                    $query->Where('first_name', 'LIKE', "%$search%")
                        ->OrWhere('last_name', 'LIKE', "%$search%")
                        ->OrWhere('email', 'LIKE', "%$search%");
                }

                if (!empty($status)) {
                    
                    $status =  ($status == 'active')?1:0;
                    $query->Where('status', $status);
                }

                if ($role_type) {
                    $query->Where('role_type', $role_type);
                }
            })->where('role_type', 3)->Paginate($this->record_per_page);
        } else {
            $premiums = Premium::orderBy('id', 'desc')->where('role_type', 3)->where('is_deleted', 1)->Paginate($this->record_per_page);
        }
        $roles = Roles::all();

        $js_file = ['common.js','bootbox.js','formValidate.js'];

        return view('admin::premiums.premium.index', compact('js_file', 'roles', 'status', 'premiums', 'page_title', 'page_action','heading', 'roles', 'role_type'));
    }

    public function fake(Premium $premium, Request $request)
    {        
        $page_title  = 'Player';
        $page_action = 'Deleted Players';
        $heading = 'Fake Players';
        

        if ($request->ajax()) {
            $id           = $request->get('id');
            $status       = $request->get('status');
            $premium         = Premium::find($id);
            $s            = ($status == 1) ? $status=0:$status=1;
            $premium->status = $s;
            $premium->save();
            echo $s;

            exit();
        }
        // Search by name ,email and group
        $search    = Input::get('search');
        $status    = Input::get('status');
        $role_type = Input::get('role_type');

        if ((isset($search) && !empty($search)) or  (isset($status) && !empty($status)) or !empty($role_type)) {
            $search = isset($search) ? Input::get('search') : '';

            $premiums = Premium::where(function ($query) use ($search,$status,$role_type) {
                if (!empty($search)) {
                    $query->Where('first_name', 'LIKE', "%$search%")
                        ->OrWhere('last_name', 'LIKE', "%$search%")
                        ->OrWhere('email', 'LIKE', "%$search%");
                }

                if (!empty($status)) {
                    
                    $status =  ($status == 'active')?1:0;
                    $query->Where('status', $status);
                }

                if ($role_type) {
                    $query->Where('role_type', $role_type);
                }
            })->where('role_type', 3)->Paginate($this->record_per_page);
        } else {
            $premiums = Premium::orderBy('id', 'desc')->where('role_type', 3)->where('fake', 1)->where('is_deleted', 0)->Paginate($this->record_per_page);
        }
        $roles = Roles::all();

        $js_file = ['common.js','bootbox.js','formValidate.js'];

        return view('admin::premiums.premium.index', compact('js_file', 'roles', 'status', 'premiums', 'page_title', 'page_action','heading', 'roles', 'role_type'));
    }

    public function real(Premium $premium, Request $request)
    {        
        $page_title  = 'Player';
        $page_action = 'Real Players';
        $heading = 'Real Players';
        

        if ($request->ajax()) {
            $id           = $request->get('id');
            $status       = $request->get('status');
            $premium         = Premium::find($id);
            $s            = ($status == 1) ? $status=0:$status=1;
            $premium->status = $s;
            $premium->save();
            echo $s;

            exit();
        }
        // Search by name ,email and group
        $search    = Input::get('search');
        $status    = Input::get('status');
        $role_type = Input::get('role_type');

        if ((isset($search) && !empty($search)) or  (isset($status) && !empty($status)) or !empty($role_type)) {
            $search = isset($search) ? Input::get('search') : '';

            $premiums = Premium::where(function ($query) use ($search,$status,$role_type) {
                if (!empty($search)) {
                    $query->Where('first_name', 'LIKE', "%$search%")
                        ->OrWhere('last_name', 'LIKE', "%$search%")
                        ->OrWhere('email', 'LIKE', "%$search%");
                }

                if (!empty($status)) {
                    
                    $status =  ($status == 'active')?1:0;
                    $query->Where('status', $status);
                }

                if ($role_type) {
                    $query->Where('role_type', $role_type);
                }
            })->where('role_type', 3)->Paginate($this->record_per_page);
        } else {
            $premiums = Premium::orderBy('id', 'desc')->where('role_type', 3)->where('fake', 0)->where('is_deleted', 0)->Paginate($this->record_per_page);
        }
        $roles = Roles::all();

        $js_file = ['common.js','bootbox.js','formValidate.js'];

        return view('admin::premiums.premium.index', compact('js_file', 'roles', 'status', 'premiums', 'page_title', 'page_action','heading', 'roles', 'role_type'));
    }

    public function unconfirmed(Premium $premium, Request $request)
    {        
        $page_title  = 'Player';
        $page_action = 'Unconfirmed Players';
        $heading = 'Unconfirmed Players';
        

        if ($request->ajax()) {
            $id           = $request->get('id');
            $status       = $request->get('status');
            $premium         = Premium::find($id);
            $s            = ($status == 1) ? $status=0:$status=1;
            $premium->status = $s;
            $premium->save();
            echo $s;

            exit();
        }
        // Search by name ,email and group
        $search    = Input::get('search');
        $status    = Input::get('status');
        $role_type = Input::get('role_type');

        if ((isset($search) && !empty($search)) or  (isset($status) && !empty($status)) or !empty($role_type)) {
            $search = isset($search) ? Input::get('search') : '';

            $premiums = Premium::where(function ($query) use ($search,$status,$role_type) {
                if (!empty($search)) {
                    $query->Where('first_name', 'LIKE', "%$search%")
                        ->OrWhere('last_name', 'LIKE', "%$search%")
                        ->OrWhere('email', 'LIKE', "%$search%");
                }

                if (!empty($status)) {
                    
                    $status =  ($status == 'active')?1:0;
                    $query->Where('status', $status);
                }

                if ($role_type) {
                    $query->Where('role_type', $role_type);
                }
            })->where('role_type', 3)->Paginate($this->record_per_page);
        } else {
            $premiums = Premium::orderBy('id', 'desc')->where('role_type', 3)->where('status', 0)->where('is_deleted', 0)->Paginate($this->record_per_page);
        }
        $roles = Roles::all();

        $js_file = ['common.js','bootbox.js','formValidate.js'];

        return view('admin::premiums.premium.index', compact('js_file', 'roles', 'status', 'premiums', 'page_title', 'page_action','heading', 'roles', 'role_type'));
    }
    
    
}
