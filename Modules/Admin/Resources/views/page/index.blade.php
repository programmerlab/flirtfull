
@extends('admin::layouts.master')
@section('content') 
@include('admin::partials.navigation')
      @include('admin::partials.breadcrumb')   

       @include('admin::partials.sidebar')

<div class="panel panel-white"> 
                  <div class="panel panel-flat">
                  <div class="panel-heading">
                    <h6 class="panel-title"><b> {{$heading }} </b><a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
                    <!-- <div class="heading-elements">
                      <ul class="icons-list">
                        <li> <a type="button" href="{{route('page.create')}}" class="btn btn-primary text-white btn-labeled btn-rounded "><b><i class="icon-plus3"></i></b> Add Pages<span class="legitRipple-ripple" ></span></a></li> 
                      </ul>
                    </div> -->
                  </div> 
                </div> 
              <!-- <div class="panel-body"> 
                  <div class="table-toolbar">
                    <div class="row">
                      <form action="{{route('page')}}" method="get" id="filter_data">
                      <div class="col-md-2">
                          <input value="{{ (isset($_REQUEST['search']))?$_REQUEST['search']:''}}" placeholder="search by page" type="text" name="search" id="search" class="form-control" >
                      </div>
                      <div class="col-md-2">
                          <input type="submit" value="Search" class="btn btn-primary form-control">
                      </div>
                    </form>
                    </div>
                </div> 
            </div> -->
               @if(Session::has('flash_alert_notice'))
                   <div class="alert alert-success alert-dismissable" style="margin:10px">
                      <button aria-hidden="true" data-dismiss="alert alert-success"  class="close" type="button">×</button>
                    <i class="icon fa fa-check"></i>  
                   {{ Session::get('flash_alert_notice') }} 
                   </div>
              @endif
                                      
                                   <div class="table-responsive">
                                    <table class="table table-striped table-hover table-bordered" id="">
                                            <thead><tr>
                                                    <th>Sno</th>
                                                    <th>Page Title</th>
                                                    <th>Page Heading</th>
                                                    <th>Content</th>
                                                    <th>Banner Image </th>                                                
                                                    <th>Created Date</th>                                                    
                                                    <th>Action</th>
                                                </tr>
                                                @if(count($pages )==0)
                                                    <tr>
                                                      <td colspan="7">
                                                        <div class="alert alert-danger alert-dismissable">
                                                          <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                                          <i class="icon fa fa-check"></i>  
                                                          {{ 'Record not found. Try again !' }}
                                                        </div>
                                                      </td>
                                                    </tr>
                                                  @endif
                                                @foreach ($pages  as $key => $result)  
                                             <thead>
                                              <tbody>    
                                                <tr>
                                                    <td>{{ ++$key }}</td>
                                                    <td>
                                                        {!! ucfirst($result->page_title)     !!}
                                                    </td>
                                                    <td>
                                                        {!! ucfirst($result->title)     !!}
                                                    </td>
                                                    <td>
                                                        {!! ucfirst(substr($result->description,0,300))     !!}
                                                    </td>
                                                    
                                                     <td> 
                                                        @if(!empty($result->image))
                                                        <img src="{!! url('storage/uploads/pages/'.$result->image) !!}" width="100px">
                                                        @endif
                                                     </td>                                                    
                                                    <td>
                                                        {!! date('d M Y',strtotime($result->created_at)) !!}
                                                    </td>                                                   
                                                    
                                                    <td> 
                                    
                                                    <a href="{{ route('page.edit',$result->id)}}" class="btn btn-primary btn-xs" style="margin: 5px">
                                                    <i class="icon-pencil7" title="edit"></i>  
                                                    </a> 

                                                    {!! Form::open(array('class' => 'form-inline pull-left deletion-form', 'method' => 'DELETE',  'id'=>'deleteForm_'.$result->id, 'style'=>'margin:5px', 'route' => array('page.destroy', $result->id))) !!}

                                                    <!-- <button class='delbtn btn btn-danger btn-xs' type="submit" name="remove_levels" value="delete" id="{{$result->id}}"><i class="icon-trash" title="Delete"></i> 
                                                    </button>  -->
                                                    {!! Form::close() !!}

                                                    </td>
                                                </tr>
                                                @endforeach 
                                            </tbody></table>
                                            <div class="center" align="center">  {!! $pages->appends(['search' => isset($_GET['search'])?$_GET['search']:''])->render() !!}</div>
                                        </div>

                </div>
                                    </div><!-- /.box-body --> 
                                    </div>
              </div> 

@stop
