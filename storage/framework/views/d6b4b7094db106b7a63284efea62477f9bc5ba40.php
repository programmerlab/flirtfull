<?php $__env->startSection('content'); ?> 
<?php echo $__env->make('admin::partials.navigation', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('admin::partials.breadcrumb', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>   
<?php echo $__env->make('admin::partials.sidebar', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
      <!-- /main sidebar -->

<div class="panel panel-white"> 
    <div class="panel panel-flat">
        <div class="panel-heading">
                <h6 class="panel-title"><b> <?php echo e($heading); ?></b><a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
                
        </div> 
    </div>  
                
    <div class="panel-body">

        <?php echo Form::model($setting, ['method' => 'PATCH', 'route' => ['setting.update', $setting->id],'class'=>'form-basic ui-formwizard user-form','id'=>'form_sample_3','enctype'=>'multipart/form-data']); ?>

            <?php echo $__env->make('admin::setting.form', compact('setting'), \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <?php echo Form::close(); ?> 
        <!-- END CONTENT BODY -->
    </div>
</div>    
    <!-- END QUICK SIDEBAR -->
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin::layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>