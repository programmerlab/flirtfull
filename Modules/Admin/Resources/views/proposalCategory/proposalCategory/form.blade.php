

        <div class="form-body">
            <div class="alert alert-danger display-hide">
                <button class="close" data-close="alert"></button> Please fill required field! </div>
            <div class="alert alert-success display-hide">
                <button class="close" data-close="alert"></button> Your form validation is successful! </div>

		 	<div class="form-group {{ $errors->first('title', ' has-error') }}">
		        <label class="control-label col-md-3">Title <span class="required"> * </span></label>
		        <div class="col-md-4"> 
		            {!! Form::text('title',null, ['class' => 'form-control','data-required'=>1])  !!} 
		            
		            <span class="help-block">{{ $errors->first('title', ':message') }}</span>
		        </div>
		    </div> 

             
        <div class="form-group {{ $errors->first('status', ' has-error') }}">
	         <label class="control-label col-md-3">Status
                    <span class="required">  </span>
                </label>
	        <div class="col-md-4"> 
	           <select name="status" class="form-control select2me">
                    <option value="1">Active</option>
                    <option value="0">Inactive</option>	            
	            </select>
	            <span class="help-block">{{ $errors->first('status', ':message') }}</span>
	        </div>
	    </div>  
            
</div>
<div class="form-actions">
    <div class="row">
        <div class="col-md-offset-3 col-md-9">
            <div class="col-md-2">
                <button type="submit" class="btn col-md-12 blue mt-ladda-btn ladda-button" data-style="slide-up" id="saveBtn"> 
                <span class="ladda-label"> Save </span>
                <span class="ladda-spinner"></span>
                <div class="ladda-progress" style="width: 0px;"></div>
                </button>
            </div>
            <div class="col-md-2">    

                <a href="{{route('user')}}">
                {!! Form::button('Back', ['class'=>'btn btn-warning col-md-12 text-white']) !!} </a>
            </div>            
        </div>
    </div>
</div>

 
