@extends('admin::layouts.master')
 
    @section('content') 
      @include('admin::partials.navigation')
      @include('admin::partials.breadcrumb')   

       @include('admin::partials.sidebar')  
            <div class="panel panel-white"> 
                <div class="panel panel-flat">
                  <div class="panel-heading">
                    <h6 class="panel-title"><b> {{$heading }} </b><a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
                    
                  </div> 
                </div>  
                <div class="panel-body"> 
                 
              </div> 
                 @if(Session::has('flash_alert_notice'))
                     <div class="alert alert-success alert-dismissable" style="margin:10px">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                      <i class="icon fa fa-check"></i>  
                     {{ Session::get('flash_alert_notice') }} 
                     </div>
                @endif
                 <div class="table-responsive">
                  <table class="table datatable-basic table-bordered table-hover table-responsive" id="roles_list">
                                   
                                        <thead>
                                            <tr>
                                                <th> S no. </th>
                                                <th> Order ID </th>
                                                <th> Profile Name</th>
                                                <th> Amount</th>
                                                <th> Plan Type </th>
                                                <th> Quantity </th>
                                                <th> Status </th>
                                                <th> Date </th>
                                                <th> Transaction ID </th>                  

                                            </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($payments as $key => $result)
                                        <tr>
                                          <td>{{ (($payments->currentpage()-1)*15)+(++$key) }}</td>
                                          <td> {{$result->order_id}} </td>
                                          <td> {{ucwords($result->name)}} </td>
                                          @if(!empty($result->amount))
                                            <td>£ {{$result->amount}}</td>
                                          @else
                                            <td></td>
                                          @endif
                                          <td> {{$result->plan_type}} </td>
                                          <td> {{$result->qty}} </td>
                                          <td>
                                            <span class="label label-{{ $result->status_class }}">
                                              {{$result->status}}
                                            </span> 
                                          </td>
                                          <td> {{$result->created_at}} </td>
                                          <td> {{$result->payment_id}} </td>
                                        </tr>
                                        @endforeach 
                                        </tbody>
                                    </table>
                                    Showing {{($payments->currentpage()-1)*$payments->perpage()+1}} to {{$payments->currentpage()*$payments->perpage()}}
					                of  {{$payments->total()}} entries
					                 <div class="center" align="center"> {!! $payments->render() !!} </div>    
                                
                 </div> 
               </div>
         </div> 
   @stop
   
   
   