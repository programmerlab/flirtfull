@extends('admin::layouts.master')
 
    @section('content') 
      @include('admin::partials.navigation')
      @include('admin::partials.breadcrumb')   

       @include('admin::partials.sidebar')  
      <div class="panel panel-white"> 
        
 
        <div class="panel panel-flat">
                      <div class="panel-heading">
                    <h6 class="panel-title"><b>{{$page_action ?? ''}}</b><a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
                    <div class="heading-elements">
                      <ul class="icons-list">
                        <li> <a type="button" href="{{route('partner')}}" class="btn btn-primary text-white   btn-rounded "> View {{$page_title}}<span class="legitRipple-ripple" ></span></a></li> 
                      </ul>
                    </div>
                  </div> 
            </div>
                      
                     <div class="panel-body">
                           
                            <!-- END BEGIN PROFILE SIDEBAR -->
                            <!-- BEGIN PROFILE CONTENT -->
                            <div class="profile-content col-md-10">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="portlet light bordered">
                                            
                                    
                                    
                                    {!! Form::model($partner, ['route' => ['partner.store'],'class'=>'form-basic ui-formwizard partner-form','id'=>'partners_form','enctype'=>'multipart/form-data']) !!}
                                    <div class="portlet-body">
                                        <div class="tab-content">
                                            <!-- PERSONAL INFO TAB --> 
                                                <div class="margin-top-10">
                                                    @if (count($errors) > 1000)
                                                      <div class="alert alert-danger">
                                                          <ul>
                                                              @foreach ($errors->all() as $error)
                                                                  <li>{!! $error !!}</li>
                                                              @endforeach
                                                          </ul>
                                                      </div>
                                                    @endif
                                                </div>

                                                <div class="form-group {{ $errors->first('name', ' has-error') }}">
                                                <label class="control-label col-md-3">Name <span class="required"> * </span></label>
                                                <div class="col-md-12"> 
                                                    {!! Form::text('name',null, ['class' => 'form-control','data-required'=>1])  !!} 
                                                    
                                                    <span class="help-block">{{ $errors->first('name', ':message') }}</span>
                                                </div>
                                            </div>
                                            
                                            <div class="form-group {{ $errors->first('contact', ' has-error') }}">
                                                <label class="control-label col-md-3">Contact<span class="required"> * </span></label>
                                                <div class="col-md-12"> 
                                                    {!! Form::text('contact',null, ['class' => 'form-control','data-required'=>1])  !!} 
                                                    
                                                    <span class="help-block">{{ $errors->first('contact', ':message') }}</span>
                                                </div>
                                            </div>
                                                
                                                
                                             <div class="form-group {{ $errors->first('commission', ' has-error') }}">
                                                <label class="control-label col-md-3">Commission (in %)<span class="required"> * </span></label>
                                                <div class="col-md-12"> 
                                                    {!! Form::text('commission',null, ['class' => 'form-control','data-required'=>1])  !!} 
                                                    
                                                    <span class="help-block">{{ $errors->first('commission', ':message') }}</span>
                                                </div>
                                            </div>                                                                                        
                                                                                                    
                          
                              
                                           <div class="col-md-12">
                                                  <div class="margin-top-10"> 
                                                       <button type="submit" class="btn green btn-primary" value="avtar" name="submit"> Save Changes </button>
                                                      <button type="reset" class="btn default "> Cancel </button>
                                                  </div> 
                                                 
                                           </div>

                                            {!! Form::close() !!} 
                                            
                                             
                                        </div>

                                    </div>
                                    </form>
                                </div>
                            </div>
                        
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
        
@stop