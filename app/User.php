<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'role_type', 'name', 'email','country','state','city', 'password','phone','dob','age','gender','address','profile_image','cover_image','is_update','is_login','last_login','provider_name','status','fake','partner_id','profile_created_at','profile_activated_at','profile_deleted_at','activation_time','campaign_id','token','hash','cid','parameters' 
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}
