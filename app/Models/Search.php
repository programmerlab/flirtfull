<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Search extends Model
{
    public function get_users()
    {
    	$users = DB::table('users')->select('users.*','user_details.*')
            ->leftJoin('user_details', 'user_details.user_id', '=', 'users.id')->where('users.status','=',1)->orderBy('users.created_at', 'desc')->paginate(30);
        return (!empty($users) ? $users : false);
    }
}
