<?php

declare(strict_types=1);

namespace App\Helpers;

use App\CorporateProfile;
use App\Interview;
use App\InterviewRating;
use App\RatingFeedback;
use App\User;
use Hash;
use Input;
use Mail;
use View;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use PHPMailerAutoload;
use DB;

class Helper
{
     public static function getUser($user_id=null){
        return User::find($user_id);
    }

    public static function setting(){

        $setting = \DB::table('settings')->select('field_key','field_value')->get()->toArray();
        $data = [];
        foreach ($setting as $key => $value) {

           $data[$value->field_key] = $value->field_value;
        }

        return (object) $data;


    }
    /**
     * function used to check stock in kit
     *
     * @param = null
     */
    public static function generateRandomString($length)
    {
        $key  = '';
        $keys = array_merge(range(0, 9), range('a', 'z'));

        for ($i = 0; $i < $length; $i++) {
            $key .= $keys[array_rand($keys)];
        }

        return $key;
    }


    public static function FormatPhoneNumber($number)
    {
        return preg_replace('~.*(\d{3})[^\d]{0,7}(\d{3})[^\d]{0,7}(\d{4}).*~', '($1) $2-$3', $number) . "\n";
    }



    /* @method : send Mail
        * @param : email
        * Response :
        * Return : true or false
        */
    public function test_mail(){                        
        $mail = new PHPMailer;
        
        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = 'smtp.mailgun.org';                     // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = 'postmaster@flirtfull.com';   // SMTP username
        $mail->Password = '31170d40b988e16b1049d348609590b8-6140bac2-246be94d';                           // SMTP password
        $mail->SMTPSecure = 'tls';                            // Enable encryption, only 'tls' is accepted
        
        $mail->From = 'support@flirtfull.com';
        $mail->FromName = 'Mailer';
        $mail->addAddress('mss.parvezkhan@gmail.com');                 // Add a recipient
        
        $mail->WordWrap = 50;                                 // Set word wrap to 50 characters
        
        $mail->Subject = 'Hello';
        $mail->Body    = 'Testing some Mailgun awesomness';
        //print_r($mail); die;
        if(!$mail->send()) {
            echo 'Message could not be sent.';
            echo 'Mailer Error: ' . $mail->ErrorInfo;
        } else {
            echo 'Message has been sent';
        }
        die;
    }

    public function sendMailFrontEnd($email_content, $template)
    {
        $email_content['verification_token'] =  Hash::make($email_content['receipent_email']);
        $email_content['email']              = isset($email_content['receipent_email'])?$email_content['receipent_email']:'';
        //dd($email_content);
        $mail = new PHPMailer;

        $get_template = DB::table('email_templates')->where('id', '=', $email_content['template_id'])->first();

        $site_url = url('/');
        $verification_link = url('api/v1/email_verification?verification_code='.urlencode($email_content['verification_token']).'&email='.urlencode($email_content['email']));
        $email_content['subject'] = $get_template->email_subject;
        $html = $get_template->email_message;
        
        $html = str_replace('[user_name]', ($email_content['first_name']), $html);
        $html = str_replace('[site_url]', $site_url, $html);
        $html = str_replace('[verification_link]', $verification_link, $html);
        //$html = view::make('emails.' . $template, ['content' => $email_content]);
        //$html = $html->render();
        //header("Content-Type: text/html");
        //print_r($html); die;
        
        try {
            $mail->isSMTP(); // tell to use smtp
            $mail->CharSet = 'utf-8'; // set charset to utf8

            $mail->SMTPAuth   = true;                  // enable SMTP authentication
            $mail->Host       = isset(Helper::setting()->MAIL_HOST)?Helper::setting()->MAIL_HOST:getenv('MAIL_HOST'); // sets the SMTP server
            $mail->Port       = isset(Helper::setting()->MAIL_PORT)?Helper::setting()->MAIL_PORT:getenv('MAIL_PORT');
            $mail->SMTPSecure = isset(Helper::setting()->MAIL_ENCRYPTION)?Helper::setting()->MAIL_ENCRYPTION:getenv('MAIL_ENCRYPTION');                 // set the SMTP port for the GMAIL server
            $mail->Username   = isset(Helper::setting()->MAIL_USERNAME)?Helper::setting()->MAIL_USERNAME:getenv('MAIL_USERNAME'); // SMTP account username
            $mail->Password   = isset(Helper::setting()->MAIL_PASSWORD)?Helper::setting()->MAIL_PASSWORD:getenv('MAIL_PASSWORD');

            $username       =  isset(Helper::setting()->MAIL_USERNAME)?Helper::setting()->MAIL_USERNAME:getenv('MAIL_USERNAME');

            $email_from       =  isset(Helper::setting()->MAIL_FROM)?Helper::setting()->MAIL_FROM:getenv('MAIL_FROM');

            $mail->setFrom($email_from);
            $mail->Subject = $email_content['subject'];
            $mail->MsgHTML($html);
            $mail->addAddress($email_content['receipent_email'], 'Admin');

            //$mail->addAttachment(‘/home/kundan/Desktop/abc.doc’, ‘abc.doc’); // Optional name
            $mail->SMTPOptions = [
                'ssl' => [
                    'verify_peer'       => false,
                    'verify_peer_name'  => false,
                    'allow_self_signed' => true,
                ],
            ];

            $mail->send();
            //echo "success";
        } catch (phpmailerException $e) {
        } catch (Exception $e) {
        }
    }



    public function sendMailFrontEnd_1($email_content, $template)
    {
        $email_content['verification_token'] =  Hash::make($email_content['receipent_email']);
        $email_content['email']              = isset($email_content['receipent_email'])?$email_content['receipent_email']:'';
        //dd($email_content);
        $mail = new PHPMailer;

        $get_template = DB::table('email_templates')->where('id', '=', $email_content['template_id'])->first();

        $site_url = url('/');
        $verification_link = url('api/v1/email_verification?verification_code='.urlencode($email_content['verification_token']).'&email='.urlencode($email_content['email']));
        $email_content['subject'] = $get_template->email_subject;

        $html = view::make('emails.' . $template, ['content' => $email_content]);
        //$html = $get_template->email_message;
        
        $html = str_replace('[user_name]', ($email_content['first_name']), $html);
        $html = str_replace('[site_url]', $site_url, $html);
        $html = str_replace('[verification_link]', $verification_link, $html);
        //$html = view::make('emails.' . $template, ['content' => $email_content]);
        //$html = $html->render();
        //header("Content-Type: text/html");
        //print_r($html); die;
        
        try {
            $mail->isSMTP(); // tell to use smtp
            $mail->CharSet = 'utf-8'; // set charset to utf8

            $mail->SMTPAuth   = true;                  // enable SMTP authentication
            $mail->Host       = isset(Helper::setting()->MAIL_HOST)?Helper::setting()->MAIL_HOST:getenv('MAIL_HOST'); // sets the SMTP server
            $mail->Port       = isset(Helper::setting()->MAIL_PORT)?Helper::setting()->MAIL_PORT:getenv('MAIL_PORT');
            $mail->SMTPSecure = isset(Helper::setting()->MAIL_ENCRYPTION)?Helper::setting()->MAIL_ENCRYPTION:getenv('MAIL_ENCRYPTION');                 // set the SMTP port for the GMAIL server
            $mail->Username   = isset(Helper::setting()->MAIL_USERNAME)?Helper::setting()->MAIL_USERNAME:getenv('MAIL_USERNAME'); // SMTP account username
            $mail->Password   = isset(Helper::setting()->MAIL_PASSWORD)?Helper::setting()->MAIL_PASSWORD:getenv('MAIL_PASSWORD');

            $username       =  isset(Helper::setting()->MAIL_USERNAME)?Helper::setting()->MAIL_USERNAME:getenv('MAIL_USERNAME');

            $email_from       =  isset(Helper::setting()->MAIL_FROM)?Helper::setting()->MAIL_FROM:getenv('MAIL_FROM');

            $mail->setFrom($email_from);
            $mail->Subject = $email_content['subject'];
            $mail->MsgHTML($html);
            $mail->addAddress($email_content['receipent_email'], 'Admin');

            //$mail->addAttachment(‘/home/kundan/Desktop/abc.doc’, ‘abc.doc’); // Optional name
            $mail->SMTPOptions = [
                'ssl' => [
                    'verify_peer'       => false,
                    'verify_peer_name'  => false,
                    'allow_self_signed' => true,
                ],
            ];

            $mail->send();
            //echo "success";
        } catch (phpmailerException $e) {
        } catch (Exception $e) {
        }
    }
    
    
    public function sendMailSuccess($email_content, $password)
    {
        $email_content['verification_token'] =  Hash::make($email_content['receipent_email']);
        $email_content['email']              = isset($email_content['receipent_email'])?$email_content['receipent_email']:'';
        //dd($email_content);
        $mail = new PHPMailer;

        $get_template = DB::table('email_templates')->where('id', '=', $email_content['template_id'])->first();

        $site_url = url('/');        
        $email_content['subject'] = "Welcome to Flirtfull! You are successfully registered with us.";
        $html = $get_template->email_message;
        
        $html = str_replace('[user_name]', ucwords($email_content['first_name']), $html);
        $html = str_replace('[site_url]', $site_url, $html);
        $html = str_replace('[generated_password]', $password, $html);
        //$html = view::make('emails.' . $template, ['content' => $email_content]);
        //$html = $html->render();
        //header("Content-Type: text/html");
        //print_r($html); die;
        
        try {
            $mail->isSMTP(); // tell to use smtp
            $mail->CharSet = 'utf-8'; // set charset to utf8

            $mail->SMTPAuth   = true;                  // enable SMTP authentication
            $mail->Host       = isset(Helper::setting()->MAIL_HOST)?Helper::setting()->MAIL_HOST:getenv('MAIL_HOST'); // sets the SMTP server
            $mail->Port       = isset(Helper::setting()->MAIL_PORT)?Helper::setting()->MAIL_PORT:getenv('MAIL_PORT');
            $mail->SMTPSecure = isset(Helper::setting()->MAIL_ENCRYPTION)?Helper::setting()->MAIL_ENCRYPTION:getenv('MAIL_ENCRYPTION');                 // set the SMTP port for the GMAIL server
            $mail->Username   = isset(Helper::setting()->MAIL_USERNAME)?Helper::setting()->MAIL_USERNAME:getenv('MAIL_USERNAME'); // SMTP account username
            $mail->Password   = isset(Helper::setting()->MAIL_PASSWORD)?Helper::setting()->MAIL_PASSWORD:getenv('MAIL_PASSWORD');

            $username       =  isset(Helper::setting()->MAIL_USERNAME)?Helper::setting()->MAIL_USERNAME:getenv('MAIL_USERNAME');

            $email_from       =  isset(Helper::setting()->MAIL_FROM)?Helper::setting()->MAIL_FROM:getenv('MAIL_FROM');

            $mail->setFrom($email_from);
            $mail->Subject = $email_content['subject'];
            $mail->MsgHTML($html);
            $mail->addAddress($email_content['receipent_email'], 'Admin');

            //$mail->addAttachment(‘/home/kundan/Desktop/abc.doc’, ‘abc.doc’); // Optional name
            $mail->SMTPOptions = [
                'ssl' => [
                    'verify_peer'       => false,
                    'verify_peer_name'  => false,
                    'allow_self_signed' => true,
                ],
            ];

            $mail->send();
            //echo "success";
        } catch (phpmailerException $e) {
        } catch (Exception $e) {
        }
    }
    
    
    public function sendMail($email_content, $template)
    {
        $mail       = new PHPMailer;        
        $html = view::make('emails.' . $template, ['content' => $email_content]);
        //$html = $html->render();
        //header("Content-Type: text/html");
        //print_r($html); die;
        
        try {
            $mail->isSMTP(); // tell to use smtp
            $mail->CharSet = 'utf-8'; // set charset to utf8

            $mail->SMTPAuth   = true;                  // enable SMTP authentication
            $mail->Host       = isset(Helper::setting()->MAIL_HOST)?Helper::setting()->MAIL_HOST:getenv('MAIL_HOST'); // sets the SMTP server
            $mail->Port       = isset(Helper::setting()->MAIL_PORT)?Helper::setting()->MAIL_PORT:getenv('MAIL_PORT');
            $mail->SMTPSecure = isset(Helper::setting()->MAIL_ENCRYPTION)?Helper::setting()->MAIL_ENCRYPTION:getenv('MAIL_ENCRYPTION');                 // set the SMTP port for the GMAIL server
            $mail->Username   = isset(Helper::setting()->MAIL_USERNAME)?Helper::setting()->MAIL_USERNAME:getenv('MAIL_USERNAME'); // SMTP account username
            $mail->Password   = isset(Helper::setting()->MAIL_PASSWORD)?Helper::setting()->MAIL_PASSWORD:getenv('MAIL_PASSWORD');

            $username       =  isset(Helper::setting()->MAIL_USERNAME)?Helper::setting()->MAIL_USERNAME:getenv('MAIL_USERNAME');

            $email_from       =  isset(Helper::setting()->MAIL_FROM)?Helper::setting()->MAIL_FROM:getenv('MAIL_FROM');

            $mail->setFrom($email_from);
            $mail->Subject = $email_content['subject'];
            $mail->MsgHTML($html);
            $mail->addAddress($email_content['receipent_email'], 'Admin');

            //$mail->addAttachment(‘/home/kundan/Desktop/abc.doc’, ‘abc.doc’); // Optional name
            $mail->SMTPOptions = [
                'ssl' => [
                    'verify_peer'       => false,
                    'verify_peer_name'  => false,
                    'allow_self_signed' => true,
                ],
            ];

            $mail->send();
            //echo "success";
        } catch (phpmailerException $e) {
        } catch (Exception $e) {
        }
    }
    
    
    /* @method : send Mail
      * @param : email
      * Response :
      * Return : true or false
      */
    public function sendMail1($email_content, $template)
    {
        $billing     = $email_content['billing']     ?? null;
        $cart_detail = $email_content['cart_detail'] ?? null;

        $mail       = new PHPMailer;
        $html       = view::make('emails.' . $template, ['content' => $email_content,'billing' => $billing,'cart_detail' => $cart_detail]);
        
        //print_r($html);die;
        $html       = $html->render();
        $subject    = $email_content['subject'];

        try {
            $mail->isSMTP(); // tell to use smtp
            $mail->CharSet = 'utf-8'; // set charset to utf8

            $mail->SMTPAuth   = true;                  // enable SMTP authentication
            $mail->Host       = isset(Helper::setting()->MAIL_HOST)?Helper::setting()->MAIL_HOST:getenv('MAIL_HOST'); // sets the SMTP server
            $mail->Port       = isset(Helper::setting()->MAIL_PORT)?Helper::setting()->MAIL_PORT:getenv('MAIL_PORT');
            $mail->SMTPSecure = isset(Helper::setting()->MAIL_ENCRYPTION)?Helper::setting()->MAIL_ENCRYPTION:getenv('MAIL_ENCRYPTION');                 // set the SMTP port for the GMAIL server
            $mail->Username   = isset(Helper::setting()->MAIL_USERNAME)?Helper::setting()->MAIL_USERNAME:getenv('MAIL_USERNAME'); // SMTP account username
            $mail->Password   = isset(Helper::setting()->MAIL_PASSWORD)?Helper::setting()->MAIL_PASSWORD:getenv('MAIL_PASSWORD');

            $username       =  isset(Helper::setting()->MAIL_USERNAME)?Helper::setting()->MAIL_USERNAME:getenv('MAIL_USERNAME');
            $email_from     =  isset(Helper::setting()->MAIL_FROM)?Helper::setting()->MAIL_FROM:getenv('MAIL_FROM');

            $mail->setFrom($username, $email_from);
            $mail->Subject = $email_content['subject'];
            $mail->MsgHTML($html);
            $mail->addAddress($email_content['receipent_email'], $email_content['first_name']);


            //$mail->addAttachment(‘/home/kundan/Desktop/abc.doc’, ‘abc.doc’); // Optional name
            $mail->SMTPOptions = [
                'ssl' => [
                    'verify_peer'       => false,
                    'verify_peer_name'  => false,
                    'allow_self_signed' => true,
                ],
            ];

            $mail->send();
            //echo "success";
        } catch (phpmailerException $e) {
        } catch (Exception $e) {
        }
    }


    public function sendMailToAdmin($email_content, $template)
    {
        $mail       = new PHPMailer;
        $html       = view::make('emails.' . $template, ['content' => $email_content]);
        $html       = $html->render();
        $subject    = $email_content['subject'];

        try {
            $mail->isSMTP(); // tell to use smtp
            $mail->CharSet = 'utf-8'; // set charset to utf8


           $mail->SMTPAuth    = true;                  // enable SMTP authentication
            $mail->Host       = getenv('MAIL_HOST'); // sets the SMTP server
            $mail->Port       = 465; //getenv('MAIL_PORT');
            $mail->SMTPSecure = 'ssl';                 // set the SMTP port for the GMAIL server

            $mail->Username   = isset(Helper::setting()->MAIL_USERNAME)?Helper::setting()->MAIL_USERNAME:getenv('MAIL_USERNAME'); // SMTP account username
            $mail->Password   = isset(Helper::setting()->MAIL_PASSWORD)?Helper::setting()->MAIL_PASSWORD:getenv('MAIL_PASSWORD');

            $uname = isset(Helper::setting()->MAIL_USERNAME)?Helper::setting()->MAIL_USERNAME:getenv('MAIL_USERNAME'); // SMTP account username

            $sendMail = isset(Helper::setting()->MAIL_TO)?Helper::setting()->MAIL_TO:getenv('MAIL_TO');

            if(!$sendMail){
                $sendMail = $email_content['receipent_email'];
            }

            $mail->setFrom($uname, $email_content['from']);
            $mail->Subject = $email_content['subject'];
            $mail->MsgHTML($html);
            $mail->addAddress($sendMail, $email_content['first_name']);

            //$mail->addAttachment(‘/home/kundan/Desktop/abc.doc’, ‘abc.doc’); // Optional name
            $mail->SMTPOptions = [
                'ssl' => [
                    'verify_peer'       => false,
                    'verify_peer_name'  => false,
                    'allow_self_signed' => true,
                ],
            ];

            $mail->send();
            //echo "success";
        } catch (phpmailerException $e) {
        } catch (Exception $e) {
        }
    }
    
    public function settings()
    {
        
        try {    
            $meta_title = isset(Helper::setting()->meta_title)?Helper::setting()->meta_title:'';
            $meta_description = isset(Helper::setting()->meta_description)?Helper::setting()->meta_description:'';
            $meta_key = isset(Helper::setting()->meta_key)?Helper::setting()->meta_key:''; 
            return ["meta_title"=>$meta_title,"meta_description"=>$meta_description,"meta_key"=>$meta_key];
        } catch (Exception $e) {
        }
    }

    public static function getAllLanguages($language_id)
    {
        $lang_know = array();
        if (strpos($language_id, ',') !== false) {
            $lang_id = explode(',',$language_id);
            foreach($lang_id as $val)
            {
                $language_name = DB::table('languages')->where('id','=',$val)->first();
                $lang_know[] = $language_name->name;
            }
        }
        else if(!empty($language_id))
        {
            $language_name = DB::table('languages')->where('id','=',$language_id)->first();
            $lang_know[] = $language_name->name;
        }
        else
        {
            $lang_know[] = '';
        }

        return $lang_know;
        
    }

    public static function getAllInterests($interest_id)
    {
        $interests = array();
        if (strpos($interest_id, ',') !== false) {
            $explode = explode(',',$interest_id);
            foreach($explode as $val)
            {
                $interest_name = DB::table('interests')->where('id','=',$val)->first();
                $interests[] = $interest_name;
            }
        }
        else if(!empty($interest_id))
        {
            $interest_name = DB::table('interests')->where('id','=',$interest_id)->first();
            $interests[] = $interest_name;
        }
        else
        {
            $interests[] = '';
        }
        
        return $interests;
    }

    public static function update_data($table,$input,$id)
    {
        return DB::table($table)->where('id', $id)->update($input);
    }

    public static function insert_data($table,$data)
    {
        $insert_id = DB::table($table)->insertGetId($data);
        return $insert_id;
    }

    public static function all_data($table,$where)
    {
        return DB::table($table)->where($where)->get();
    }

    public static function getAllDiamonds()
    {        
        $diamonds = DB::table('diamonds')->where('is_deleted','0')->where('status','1')->get();    
        return $diamonds;
    }
    public static function getAllCredits()
    {        
        $diamonds = DB::table('credits')->where('is_deleted','0')->where('status','1')->get();    
        return $diamonds;
    }
    public static function getAllCoins()
    {        
        $diamonds = DB::table('coins')->where('is_deleted','0')->where('status','1')->get();    
        return $diamonds;
    }
    public static function getAllPremiums()
    {        
        $diamonds = DB::table('premiums')->where('is_deleted','0')->where('status','1')->get();    
        return $diamonds;
    }
    public static function getWallet($userid)
    {        
        $wallet = DB::table('user_wallet')->where('user_id',$userid)->first();    
        return $wallet;
    }
    public static function getChatRequest($userid)
    {   
        $request = DB::table('chat_room as cr')->leftjoin('users as u','cr.sender_id','=','u.id')->select('u.*','cr.message','cr.created_at as like_date')->where('cr.receiver_id',$userid)->where("cr.message","!=",NULL)->where("u.is_deleted","=",0)->get();
        /*if(!empty($request))
        {
            foreach($request as $key => $val)
            {
                $is_fake = DB::table('users')->where('id',$val->id)->first();
                $request[$key]->is_fake = $is_fake->fake;
            }
        } */   
        
        //print_r($request);die;
        return $request;
    }
    public static function getConfirmChatRequest($userid)
    {        
        $request1 = DB::table('chat_room as cr')->leftjoin('users as u','cr.sender_id','=','u.id')->select('u.*','cr.message')->where(array('cr.receiver_id'=>$userid))->where("cr.message","=",NULL)->where("u.is_deleted","=",0)->get();
        $request2 = DB::table('chat_room as cr')->leftjoin('users as u','cr.receiver_id','=','u.id')->select('u.*','cr.message')->where(array('cr.sender_id'=>$userid))->where("cr.message","=",NULL)->where("u.is_deleted","=",0)->get();
        //echo "<pre>"; print_r($request1); print_r($request2);
        //echo "<pre>"; print_r(array($request1,$request2)); die;
        return array($request1,$request2);
    }
    
    public static function getFakeChatRequest($userid)
    {
        $request1 = DB::table('chat_room as cr')->leftjoin('users as u','cr.sender_id','=','u.id')->select('u.*')->where(array('cr.receiver_id'=>$userid))->where("u.is_deleted","=",0)->get();
        $request2 = DB::table('chat_room as cr')->leftjoin('users as u','cr.receiver_id','=','u.id')->select('u.*')->where(array('cr.sender_id'=>$userid))->where("u.is_deleted","=",0)->get();
        $req1 = [];
        foreach($request1 as $r1){
            $req1[] = $r1->id;
        }
        $req2 = [];
        foreach($request2 as $r2){
            $req2[] = $r2->id;
        }
        $req = array_merge($req1,$req2);
        $res = DB::table('users')->whereNotIn('id',$req)->where('id','!=',$userid)->limit(5)->get();
        //echo "<pre>"; print_r($res); die;
        return $res;
    }
    
    public static function getChatRequestNotification($userid)
    {        
        $request = DB::table('chat_room')->select('*')->where('receiver_id',$userid)->where("notification_status","=",1)->get();    
        return count($request);
    }
    
    public static function updateUserLogin($id)
    {
        $random_time = array(200,250,300,350,400);
        $random_time = time() + array_rand($random_time);
        return DB::table('users')->where('id', $id)->update(array("is_login"=>'1',"login_time"=>$random_time));
    }
}
