@extends('admin::layouts.master')
 
    @section('content') 
      @include('admin::partials.navigation')
      @include('admin::partials.breadcrumb')   

       @include('admin::partials.sidebar')  
      <div class="panel panel-white"> 
				
 
        <div class="panel panel-flat">
                      <div class="panel-heading">
                    <h6 class="panel-title"><b> {{$heading ?? ''}}</b><a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
                    <div class="heading-elements">
                      <ul class="icons-list">
                        <li> <a type="button" href="{{route('grid')}}" class="btn btn-primary text-white   btn-rounded "> View Players<span class="legitRipple-ripple" ></span></a></li> 
                      </ul>
                    </div>
                  </div> 
            </div>
				 @if(Session::has('flash_alert_notice'))
                     <div class="alert alert-success alert-dismissable" style="margin:10px">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                      <i class="icon fa fa-check"></i>  
                     {{ Session::get('flash_alert_notice') }} 
                     </div>
                @endif
                     <div class="panel-body">
                     {!! Form::model($user, ['method' => 'PATCH', 'route' => ['grid.update', $user->id],'enctype'=>'multipart/form-data','class'=>
                                   'form-basic ui-formwizard user-form']) !!}
                                   <div class="profile_edit_outer">
                            <div class="profile-sidebar col-md-2">
                                <!-- PORTLET MAIN -->
                                <div class="portlet light profile-sidebar-portlet bordered">
                                    <!-- SIDEBAR USERPIC -->
                                    <div class="profile-userpic">
                                    <h3>Profile Image </h3>
                                     <div class=" image_panel" >
                                    @if(isset($user->profile_image))
                                        @if($user->fake == 0)
                                            <img src=" {{ (URL::asset('public/images/profile_image/'.$user->profile_image)) ?? 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image'}}" alt="" height="200px" width="150px">
                                        @else
                                            <img src=" {{ (env('PROFILE_MANAGER_ADDR').('/public/images/profile_image/'.$user->profile_image)) ?? 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image'}}" alt="" height="200px" width="150px">
                                        @endif    
                                    @endif                
                               
								<div class="active_profile">
                    	<span class="label label-{{ ($user->profile_image_status==1)?'success':'warning'}} status" id="{{$user->id}}"  data="{{$user->profile_image_status}}"  onclick="changeStatus({{$user->id}},'profile_image_status')" >
												{{ ($user->profile_image_status==1)?'Active':'Inactive'}}
										</span>
								</div>	

           </div>
           	 <input type="file"  class="file-input edit_button" name="profile_image">
           </div>
                    
                    <div class="profile-userpic">
                                    <h3>Cover Image </h3>
                                     <div class=" image_panel" >
                                   @if(isset($user->cover_image))
                <div class="" >
                 
                <img src=" {{ (URL::asset('public/images/cover_img/'.$user->cover_image)) ?? 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image'}}" alt="" height="200px" width="150px">
                </div>
                @endif                
                <span class="help-block" style="color:#e73d4a">{{ $errors->first('cover_image', ':message') }}</span>
                               
								<div class="active_profile">
                    	<span class="label label-{{ ($user->cover_image_status==1)?'success':'warning'}} status" id="{{$user->id + 1}}"  data="{{$user->cover_image_status}}"  onclick="changeStatus({{$user->id + 1 }},'cover_image_status')" >
												{{ ($user->cover_image_status==1)?'Active':'Inactive'}}
										</span>    
								</div> 	

           </div>
           	  <input type="file" class="file-input" name="cover_image">
           </div>
           
           <div class="profile-userpic">
         
        <div>
        <div class="form-group  {{ $errors->first('gallery_image', ' has-error') }}">
            <h3> Gallery Images </h3>
            <div>
            
                @if(isset($user->gallery_images))
                    <div class="row">
                    @foreach($user->gallery_images as $row)
                        
                    <div class="image_panel">
                    <!--<img src=" {{ (URL::asset('public/images/profile_image/'.$row->gallery_image)) ?? 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image'}}" alt="" height="200px" width="150px">-->
                    
                        @if($user->fake == 0)
                            <img src=" {{ (URL::asset('public/images/profile_image/'.$row->gallery_image)) ?? 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image'}}" alt=""  onerror="this.src='{{ asset("public/images/profile_image/no_image.jpg") }}'">
                        @else
                            <img src=" {{ (env('PROFILE_MANAGER_ADDR').('/public/images/profile_image/'.$row->gallery_image)) ?? 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image'}}" alt=""  onerror="this.src='{{ asset("public/images/profile_image/no_image.jpg") }}'">
                        @endif                        
                    
                    <!--<span class="label label-primary status" id="{{$row->id}}"  data="{{$row->is_deleted}}"   >-->
                       <div class="remove_panel">
                   
               <a href="{{ url('admin/deleteImage?id='.$row->id.'&userid='.$user->id) }}"> <i class="glyphicon glyphicon-trash"></i> </a> </div> 
               <div class="make_profile">
                    	<a href="{{ url('admin/setProfilePicture?id='.$row->id.'&userid='.$user->id) }}"><i class="glyphicon glyphicon-user"></i></a>
                    </div> 
                    <div class="active_profile">
                    	<!--<a href="#"><i class="glyphicon glyphicon-check"></i></a>-->
											<span class="label label-{{ ($row->status==1)?'success':'warning'}} status" id="{{$row->id}}"  data="{{$row->status}}"  onclick="changeStatus({{$row->id}},'grid')" >
                        {{ ($row->status==1)?'Active':'Inactive'}}
											</span>
                    </div> 
                    <!--</span>-->
                    </div>  
                                     
                    @endforeach
                    </div>        
                @endif
                
                <input type="file" class="file-input" name="gallery_image[]" multiple>                            
                 <span class="help-block" style="color:#e73d4a">{{ $errors->first('gallery_image', ':message') }}</span>
            
            </div>
        </div>
    </div>    
     <div class="col-md-12">
            <div class="margin-top-10"> 
                 <button type="submit" class="btn green btn-primary" value="avtar" name="submit"> Save Changes </button>
                <button type="reset" class="btn default "> Cancel </button>
            </div> 
           
            </div>
    </div>
                    
                                      
                                   
                                </div>
                              
                            </div>
                            <!-- END BEGIN PROFILE SIDEBAR -->
                            <!-- BEGIN PROFILE CONTENT -->
                            <div class="col-md-5">
                            	<div class="row">
                                    <div class="col-md-12">
                                    	 @include('admin::users.grid.personel_info', compact('user'))
                                    </div>
                                </div>
                            </div>
                            
                            <div class="profile-content col-md-5">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="portlet light bordered">
                                    @include('admin::users.grid.user_details', compact('user'))
                                         
                                   
                                   
                                   
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                     </form>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
            
            
            <!-- END QUICK SIDEBAR -->
        </div>
        

        
@stop