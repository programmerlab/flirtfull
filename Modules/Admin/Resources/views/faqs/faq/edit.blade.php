@extends('admin::layouts.master')
 
    @section('content') 
      @include('admin::partials.navigation')
      @include('admin::partials.breadcrumb')   

       @include('admin::partials.sidebar')  
                        <div class="panel panel-white"> 

 
                                <div class="panel panel-flat">
                                              <div class="panel-heading">
                                            <h6 class="panel-title"><b> {{$heading ?? ''}}</b><a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
                                            <div class="heading-elements">
                                              <ul class="icons-list">
                                                <li> <a type="button" href="{{route('faq')}}" class="btn btn-primary text-white   btn-rounded "> View faqs<span class="legitRipple-ripple" ></span></a></li> 
                                              </ul>
                                            </div>
                                          </div> 
                                    </div>
                                    <div class="panel-body">
                                    {!! Form::model($faq, ['method' => 'PATCH', 'route' => ['faq.update', $faq->id],'enctype'=>'multipart/form-data','class'=>
                                                  'form-basic ui-formwizard faq-form']) !!}
                                                  
                                           <div class="form-group {{ $errors->first('question', ' has-error') }}">
                                                <label class="control-label col-md-3">Question <span class="required"> * </span></label>
                                                <div class="col-md-12"> 
                                                    {!! Form::text('question',null, ['class' => 'form-control','data-required'=>1])  !!} 
                                                    
                                                    <span class="help-block">{{ $errors->first('question', ':message') }}</span>
                                                </div>
                                            </div>
                                            
                                            <div class="form-group {{ $errors->first('answer', ' has-error') }}">
                                                <label class="control-label col-md-3">Answer <span class="required"> * </span></label>
                                                <div class="col-md-12"> 
                                                    {!! Form::textarea('answer',null, ['class' => 'form-control','data-required'=>1])  !!} 
                                                    
                                                    <span class="help-block">{{ $errors->first('answer', ':message') }}</span>
                                                </div>
                                            </div>
                                                
                                                
                                           <div class="col-md-12">
                                                  <div class="margin-top-10"> 
                                                       <button type="submit" class="btn green btn-primary" value="avtar" name="submit"> Save Changes </button>
                                                      <button type="reset" class="btn default "> Cancel </button>
                                                  </div> 
                                                 
                                           </div>
                                    </form>        
                                    </div>                                                                                          
                        </div>
                              
                           
                     
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
            
            
            <!-- END QUICK SIDEBAR -->
        </div>
        

        
@stop