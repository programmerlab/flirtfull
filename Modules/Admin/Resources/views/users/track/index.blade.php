@extends('admin::layouts.master')
 
    @section('content') 
      @include('admin::partials.navigation')
      @include('admin::partials.breadcrumb')   

       @include('admin::partials.sidebar')  
           <?php 
           $last = basename(request()->path());
           if($last == 'list-search')
           {
            $last_part = $_GET['last_part'];
           }
           else{
            $last_part = $last;
           }
           ?> 
            <div class="advanced-search">
            	<form method="get" action="{{ url('admin/track') }}">
                <input type="hidden" name="last_part" value="{{ $last_part }}">    
            	<div class="row">            

                	<div class="col-md-2">
                    <div class="form-group">
                    <select name="type" class="custom-select" onClick="change_value(this.value);">
                      <option value="partnerid" selected="">Partner Id</option>
                      <!--<option value="affiliateid">Affiliate Id</option>  -->
                      <!--<option value="domain" {{ (isset($_GET['domain']) ) ? "selected" : '' }}>Domain</option>-->
                      <option value="agegroup" {{ ( isset($_GET['type']) && ($_GET['type'] == 'agegroup')) ? 'selected' : '' }}>Age Group</option>
                      <option value="paymenttype" {{ ( isset($_GET['type']) && ($_GET['type'] == 'paymenttype')) ? 'selected' : '' }}>Payment Type</option>  
                    </select>
                    </div>
					</div>
                    
                    <div class="col-md-2">
                    <div class="form-group">
                    <select name="option" class="custom-select">
                      <option value="1" selected="">Equal To</option>
                      <option value="0" {{ (isset($_GET['option']) && ($_GET['option'] == 0) ) ? "selected" : '' }}>Not Equal To</option>                      
                    </select>
                    </div>
					</div>
                        
                    <div class="col-md-2">
                    <div class="form-group">
                    <select name="value" id="value" class="custom-select">
                    @if( !isset($_GET['type']))    
                        @foreach($partner as $row)
                            <option value="{{$row->id}}" {{ ( isset($_GET['type']) && ($_GET['type'] == 'partnerid') && ($_GET['value'] == $row->id) ) ? 'selected' : '' }}>{{$row->name}}</option>
                        @endforeach
                    @endif    
                    @if( isset($_GET['type']) && ($_GET['type'] == 'partnerid'))    
                        @foreach($partner as $row)
                            <option value="{{$row->id}}" {{ ( isset($_GET['type']) && ($_GET['type'] == 'partnerid') && ($_GET['value'] == $row->id) ) ? 'selected' : '' }}>{{$row->name}}</option>
                        @endforeach
                    @endif    
                    @if( isset($_GET['type']) && ($_GET['type'] == 'agegroup'))
                        <option value="18-25" {{ ( isset($_GET['type']) && ($_GET['type'] == 'agegroup') && ($_GET['value'] == '18-25') ) ? 'selected' : '' }}>18-25</option>
                        <option value="26-35" {{ ( isset($_GET['type']) && ($_GET['type'] == 'agegroup') && ($_GET['value'] == '26-35') ) ? 'selected' : '' }}>26-35</option>
                        <option value="36-45" {{ ( isset($_GET['type']) && ($_GET['type'] == 'agegroup') && ($_GET['value'] == '36-45') ) ? 'selected' : '' }}>36-45</option>
                        <option value="46-55" {{ ( isset($_GET['type']) && ($_GET['type'] == 'agegroup') && ($_GET['value'] == '46-55') ) ? 'selected' : '' }}>46-55</option>
                        <option value="56-99" {{ ( isset($_GET['type']) && ($_GET['type'] == 'agegroup') && ($_GET['value'] == '56-99') ) ? 'selected' : '' }}>56-99</option>
                    @endif    
                    @if( isset($_GET['type']) && ($_GET['type'] == 'paymenttype'))
                    <option value="credits" {{ ( isset($_GET['type']) && ($_GET['type'] == 'paymenttype') && ($_GET['value'] == 'credits') ) ? 'selected' : '' }}>credits</option>
                    <option value="premiums" {{ ( isset($_GET['type']) && ($_GET['type'] == 'paymenttype') && ($_GET['value'] == 'premiums') ) ? 'selected' : '' }}>premiums</option>
                    @endif
                    </select>
                    </div>
					</div>    
                     
                        <!--<div class="col-md-2"><div class="form-group">
                        	<button class="advance-btn" type="button"><i class="glyphicon glyphicon-cog"></i> More Filter</button>
                        </div></div>-->
                         <div class="col-md-2"><div class="form-group">
                        	<button type="submit" class="btn btn-primary form-control">Search</button>
                        </div></div>
                </div>
                
                <div class="advance-fields">
                	<div class="row">

                     <!--<div class="col-md-2"><div class="form-group">
                        <select name="status" class="custom-select">
                            <option selected="" value="">Profile Status</option>
                            <option value="yes" {{ (isset($_GET['status']) && ($_GET['status'] == 'yes') ) ? "selected" : '' }}>Active</option>
                            <option value="no" {{ (isset($_GET['status']) && ($_GET['status'] == 'no') ) ? "selected" : '' }}>Inactive</option>
                           
                        </select>
                        </div></div> -->                   	                        
                    </div>                    
                </div>
            	</form>
            </div>
       

            <div class="panel panel-white"> 
                <div class="panel panel-flat">
                  <div class="panel-heading">
                    <h6 class="panel-title"><b> {{$heading }} </b><a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
                    <div class="heading-elements">                     
                    </div>
                  </div> 
                </div>  
                <div class="panel-body">
                    <!--<h6><strong>Total UserCount</strong> :  {{ $total_usercount }},  <strong>Total Turnover</strong> :  {{ $total_turnover }}€,  <strong>Total Payout</strong> :  {{ (30/100)*$total_turnover }}€</h6> ‎                    -->
                </div> 
                 @if(Session::has('flash_alert_notice'))
                     <div class="alert alert-success alert-dismissable" style="margin:10px">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                      <i class="icon fa fa-check"></i>  
                     {{ Session::get('flash_alert_notice') }} 
                     </div>
                @endif
                 <div class="table-responsive">
                  <table class="table datatable-basic table-bordered table-hover table-responsive" id="roles_list">
                                   
                                        <thead>
                                            <tr>
                                                <th> Sno. </th>
                                                <th> Date </th>
                                                <th> # </th>
                                                <th> Amount </th>
                                                <th> Payout </th>                                                
                                            </tr>
                                        </thead>
                                        <tbody>

                                        <?php $amount = 0; $usercount = 0; $payout = 0; ?>
                                        @foreach($turnover as $key => $result)
                                            <?php $page = (isset($_GET['page'])) ? $_GET['page'] : '1'; ?>
                                            <tr>
                                                <td> {{ ((($page-1)*10)+(++$key)) }}</td>
                                                <td> {{ $result[0] }} </td>
                                                <td> {{ $result[1] }} </td>                                                
                                                <td> {{ $result[2] }} </td>
                                                <td> {{ $result[3] }} </td>                                                
                                            </tr>                                            
                                            <?php $usercount += $result[1]; ?>
                                            <?php $amount += $result[2]; ?>
                                            <?php $payout += $result[3]; ?>
                                           @endforeach
                                        
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th> </th>
                                                <th> </th>
                                                <th> {{ $usercount }} </th>
                                                <th> {{ $amount }} </th>
                                                <th> {{ $payout }} </th>                                                
                                            </tr>
                                        </tfoot>    
                                    </table>                                    
                                
                 </div>
                 <div class="pagination">{!! $pagination !!}</div>
                 <h3>SOI / DOI</h3>   
                 <div class="table-responsive">
                  <table class="table datatable-basic table-bordered table-hover table-responsive" id="roles_list">
                                   
                                        <thead>
                                            <tr>
                                                <th> Sno. </th>
                                                <th> Date </th>
                                                <th> SOI </th>
                                                <th> DOI </th>
                                                <th> Deletions </th>
                                                <th> CR </th>
                                                <th> Del % </th>      
                                            </tr>
                                        </thead>
                                        <tbody>

                                        <?php $soi = 0; $doi = 0; $deletions = 0; $cr = 0; $del = 0; ?>
                                        @foreach($revenue as $key => $result)
                                            <?php $page = (isset($_GET['page'])) ? $_GET['page'] : '1'; ?>
                                            <tr>
                                                <td> {{ ((($page-1)*10)+(++$key)) }}</td>
                                                <td> {{ $result[0] }} </td>
                                                <td> {{ $result[1] }} </td>
                                                <td> {{ $result[2] }} </td>
                                                <td> {{ $result[3] }} </td>
                                                <td> {{ ($result[4]*100).'%' }} </td>
                                                <td> {{ ($result[5]*100).'%' }} </td>    
                                            </tr>
                                            <?php $soi += $result[1]; ?>
                                            <?php $doi += $result[2]; ?>
                                            <?php $deletions += $result[3]; ?>
                                            <?php $cr += ($result[4]*100); ?>
                                            <?php $del += ($result[5]*100); ?>
                                           @endforeach
                                        
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th> </th>
                                                <th> </th>
                                                <th> {{ $soi }} </th>
                                                <th> {{ $doi }} </th>
                                                <th> {{ $deletions }} </th>
                                                <th> {{ $cr/10 }}% </th>
                                                <th> {{ $del/10 }}% </th>    
                                            </tr>
                                        </tfoot>    
                                    </table>                                    
                                
                 </div>
                 <h3>Average Revenue</h3>   
                 <div class="table-responsive">
                  <table class="table datatable-basic table-bordered table-hover table-responsive" id="roles_list">
                                   
                                        <thead>
                                            <tr>
                                                <th> Sno. </th>
                                                <th> Date </th>
                                                <th> ARPPU </th>
                                                <th> ARPU </th>
                                                <th> L2S </th>
                                                <th> CR </th>
                                                <th> Del % </th>      
                                            </tr>
                                        </thead>
                                        <tbody>

                                        <?php $soi = 0; $doi = 0; $deletions = 0; $cr = 0; $del = 0; ?>
                                        @foreach($turnover as $key => $result)
                                            <?php $page = (isset($_GET['page'])) ? $_GET['page'] : '1'; ?>
                                            <tr>
                                                <td> {{ ((($page-1)*10)+($key+1)) }}</td>
                                                <td> {{ $result[0] }} </td>
                                                <td> {{ ($result[1]) ? ($result[3] / $result[1]) : '0' }} </td>
                                                <td> {{ ($revenue[$key][2]) ? ($result[3] / $revenue[$key][2]) : '0' }} </td>
                                                <td> {{ ($revenue[$key][2]) ? ($result[1] / $revenue[$key][2]) : '0' }} </td>
                                                <td> {{ ($revenue[$key][4]*100).'%' }} </td>
                                                <td> {{ ($revenue[$key][5]*100).'%' }} </td>    
                                            </tr>                                            
                                           @endforeach
                                        
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th> </th>
                                                <th> </th>
                                                <th> </th>
                                                <th> </th>
                                                <th> </th>
                                                <th> </th>
                                                <th> </th>    
                                            </tr>
                                        </tfoot>    
                                    </table>                                    
                                
                 </div>
                    
                    <div id = "chart" style = "width: 550px; height: 400px; margin: 0 auto">
                    </div>
               </div>
         </div>
   <script>
        function change_value(type){
            $.ajax({
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '<?php echo url("/admin/");?>/get_value',
            type: 'POST',
            data: 'type='+type,
            cache : false,
            dataType: 'html',
            async: false,
            processData: false,
            success: function(data)
            {
              $('#value').html(data);              
            }
          });
        }
   </script>
      <script type = "text/javascript" src = "https://www.gstatic.com/charts/loader.js">
      </script>
      <script type = "text/javascript">
         google.charts.load('current', {packages: ['corechart', 'bar']}); 
      </script>
      <script language = "JavaScript">
            function drawChart() {
               // Define the chart to be drawn.
               var data = google.visualization.arrayToDataTable([
               
                  ['Year', 'SOI', 'DOI','Deletions'],
               <?php foreach($revenue as $key => $result){ ?>                
                  ['{{$result[0]}}',  {{$result[1]}},{{$result[2]}},{{$result[3]}}],  
               <?php } ?>                    
               ]);
   
               var options = {title: 'SOI / DOI', isStacked:true, hAxis: {
           direction:-1,
           slantedText:true,
           slantedTextAngle:45 // here you can even use 180
       }};  
   
               // Instantiate and draw the chart.
               var chart = new google.visualization.ColumnChart(document.getElementById('chart'));
               chart.draw(data, options);
            }
            google.charts.setOnLoadCallback(drawChart);
    </script> 
   @stop
   
   
   