<form style="border: 4px solid #a1a1a1;margin-top: 15px;padding: 10px;" action="{{ url('importExcel') }}" class="form-horizontal" method="post" enctype="multipart/form-data">
@csrf

<input type="file" name="import_file" />
<button class="btn btn-primary">Import File</button>
</form>