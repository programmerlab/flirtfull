<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use Validator;
use Session;
use Hash;
use Auth;
use Illuminate\Support\Facades\Input;
use App\Models\Profile;
use Excel;
use App\Models\Search;
use Carbon\Carbon;
use DateTime;
use Helper;
use Image;

class InboxController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $search;

    public function __construct(Request $request,Search $search, Profile $profile)
    {
        error_reporting(env('ERROR_SHOW'));
        ini_set('display_errors', env('ERROR_SHOW'));
        $this->middleware('web');
        $this->search = $search;
        $this->profile = $profile;
        date_default_timezone_set('Asia/Calcutta');        
        
        if(Auth::check()){           
            DB::table('users')->where("id",Auth::user()->id)->update(array("is_login"=>"1","login_time"=>time()));
        }
    }            
    
    public function index()
    {
        $user_id = Auth::id();
        if(!Auth::check())
        {
            return redirect('search');
        }        

        $users = DB::table('users')->select('users.*')->where('users.id', '=',Auth::id())->first();
        
        
        return view('inbox',compact('users'));
    }



    public function message($email,$sender_id)
    {
        $email = base64_decode($email);
        $sender_id = base64_decode($sender_id);
        $users = DB::table('users')->select('users.id')->where('users.email', '=',$email)->first(); 

        //print_r($users->id); die;
        Auth::loginUsingId($users->id);
        return redirect('inbox-chat/'.$sender_id);

    }
    
    
    
    public function chat($uid)
    {
        $user_id = Auth::id();
        if(!Auth::check())
        {
            return redirect('search');
        }        
        $users = DB::table('users')->select('users.*')->where('users.id', '=',Auth::id())->first();        
        $highliteuser = DB::table('users as u')->join('user_details as ud','u.id','=','ud.user_id')->select('u.*','ud.*')->where('u.id', '=',$uid)->first();        
        $highlite = ["id"=>$uid,"name"=>$highliteuser->name,"profile_image"=>$highliteuser->profile_image,"fake"=>$highliteuser->fake];
        
        return view('inbox_chat',compact('highlite','users','highliteuser'));
    }
    
    
    public function insertFirebase(){
        
        
        $users = DB::table('users')->select('users.*')->get();
        echo count($users);
        //foreach($users as $row){                
        //        $data = [
        //                 "username"=>"Moneo",
        //                 "password"=>"Moneo2019@",
        //                 "service_code"=>"cd9271fa",
        //                 "name"=> $row->name.'__'.$row->id,
        //                 "group"=> "subscriber",
        //                 "profile"=> [
        //                                "avatar"=> "avatar.jpg",
        //                                "age"=> "23",
        //                                "location"=> "indore",
        //                                "images"=> [$row->profile_image]
        //                             ],
        //                  "additional_info"=> [
        //                                "membership"=> "free",
        //                                "link"=> "http://xyz.com",
        //                                "about"=> "about"
        //                             ],                                            
        //                 ];
        //        $ch = curl_init();
        //        curl_setopt($ch, CURLOPT_URL,"https://im.operatorplatform.com/api/users/add");
        //        curl_setopt($ch, CURLOPT_POST, 1);
        //        curl_setopt($ch, CURLOPT_POSTFIELDS,
        //                    "data=".json_encode($data));
        //        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
        //        
        //        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        //        echo $row->name.'__'.$row->id;
        //        $server_output = curl_exec ($ch);
        //        print_r($server_output);
        //        curl_close ($ch);
        //}
        
                            
        return view('insertFirebase',compact('users'));
    }
	
    public function registerThirdparty(){
        
        
        $users = DB::table('users')->select('users.*')->where('fake','1')->where('status','1')->skip(13000)->limit(49)->get();
        
        foreach($users as $row){                
                $data = [
                         "username"=>"Moneo",
                         "password"=>"Moneo2019@",
                         "service_code"=>"cd9271fa",
                         "name"=> $row->name.'__'.$row->id,
                         "group"=> "subscriber",
                         "profile"=> [
                                        "avatar"=> "avatar.jpg",
                                        "age"=> "23",
                                        "location"=> "indore",
                                        "images"=> [$row->profile_image]
                                     ],
                          "additional_info"=> [
                                        "membership"=> "free",
                                        "link"=> "http://xyz.com",
                                        "about"=> "about"
                                     ],                                            
                         ];
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL,"https://im.operatorplatform.com/api/users/add");
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS,
                            "data=".json_encode($data));
                curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
                
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                echo $row->name.'__'.$row->id;
                $server_output = curl_exec ($ch);
                print_r($server_output);
                curl_close ($ch);
                
                $data1 = [
                         "username"=>"Moneo",
                         "password"=>"Moneo2019@",
                         "service_code"=>"cd9271fa",
                         "name"=> $row->name.'__'.$row->id,
                         "group"=> "persona",
                         "profile"=> [
                                        "avatar"=> "avatar.jpg",
                                        "age"=> "23",
                                        "location"=> "indore",
                                        "images"=> [$row->profile_image]
                                     ],
                          "additional_info"=> [
                                        "membership"=> "free",
                                        "link"=> "http://xyz.com",
                                        "about"=> "about"
                                     ],                                            
                         ];
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL,"https://im.operatorplatform.com/api/users/add");
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS,
                            "data=".json_encode($data1));
                curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
                
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                echo $row->name.'__'.$row->id;
                $server_output = curl_exec ($ch);
                print_r($server_output);
                curl_close ($ch);
        }
                                        
    }
    
	public function callBackFunFirebase(Request $request){
        //echo 'Hello this is test';
        //$data = json_encode($request->all());
        //$mdata = '{"data":"{\"username\":\"Moneo\",\"password\":\"Moneo2019@\",\"service_code\":\"cd9271fa\",\"service_name\":\"CL - MNS\",\"from\":\"macto222\",\"to\":\"carol111\",\"message\":\"Hi there! May I get to know your name,  please? I am doing good\",\"attachment\":null,\"additional_info\":{\"operatorId\":\"RKFL4718\"}}"}';
        $mdata = json_encode($_POST);
        //$mdata = '{"data":"{\"username\":\"Moneo\",\"password\":\"Moneo2019@\",\"service_code\":\"cd9271fa\",\"service_name\":\"CL - MNS\",\"from\":\"Sensuous_karol_324\",\"to\":\"carol111\",\"message\":\"Hi! have you been busy? I hope you got a moment to chat with me?\",\"attachment\":null,\"additional_info\":{\"operatorId\":\"LSD745\"}}"}';
        if($mdata){            
            $detail = (json_decode($mdata)->data);
            $detail = json_decode($detail);
            //print_r($detail); die;
            if($detail){
                $udata['from'] = explode('__',$detail->from)[1];
                $udata['to'] = explode('__',$detail->to)[1];
                $udata['message'] = $detail->message;
                $udata['attachment'] = json_encode($detail->attachment);
                $udata['additional_info'] = json_encode($detail->additional_info);
                
                DB::table('users')->where('id',$udata['from'])->update(array('is_login'=>'1'));                
            }
            //print_r($udata); die;
        }
        
        //print_r($detail->from); die;
        $udata['insertdata'] = $mdata;
        
        
        //print_r($udata); die;
        DB::table('thirdparty')->insert(
            $udata
        );
    }
    
    
    public function get_proposal(Request $request){
        //print_r($request->all());
        $proposal_images = DB::table('proposals_images')->select('image')->where('category_id',$request->get('Id'))->where('status',1)->where('is_deleted',0)->get();
        $htm = "";
        foreach($proposal_images as $row){
            $htm .= '<div class="send_pro_img">'.
                '<img src="'.asset("public/images/profile_image/".$row->image).'" class="img-responsive" onclick="setproposal(\''.$row->image.'\',\'diamonds\',1);">'.
            '</div>';
        }
        return $htm;
    }
}