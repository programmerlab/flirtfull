 
    <?php $__env->startSection('content'); ?> 
      <?php echo $__env->make('admin::partials.navigation', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      <?php echo $__env->make('admin::partials.breadcrumb', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>   

       <?php echo $__env->make('admin::partials.sidebar', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>  
           <?php 
           $last = basename(request()->path());
           if($last == 'list-search')
           {
            $last_part = $_GET['last_part'];
           }
           else{
            $last_part = $last;
           }
           ?> 
            <div class="advanced-search">
            	<form method="get" action="<?php echo e(url('admin/list-search')); ?>">
                <input type="hidden" name="last_part" value="<?php echo e($last_part); ?>">    
            	<div class="row">

                <div class="col-md-2"><div class="form-group">
                    <input class="custom-select" placeholder="Profile Name" type="text" name="name" value="<?php echo e((isset($_GET['name']) && ($_GET['name']))  ? $_GET['name'] : ''); ?>">
              </div></div>

                	<div class="col-md-2">
                    <div class="form-group">
                    <select name="age" class="custom-select">
                      <option value="" selected="">Age</option>
                      <option value="18_25" <?php echo e((isset($_GET['age']) && ($_GET['age'] == '18_25') ) ? "selected" : ''); ?>>18 year to 25 year</option>
                      <option value="25_35" <?php echo e((isset($_GET['age']) && ($_GET['age'] == '25_35') ) ? "selected" : ''); ?>>25 year to 35 year</option>
                      <option value="35_50" <?php echo e((isset($_GET['age']) && ($_GET['age'] == '35_50') ) ? "selected" : ''); ?>>35 year to 50 year</option>
                      <option value="50_100" <?php echo e((isset($_GET['age']) && ($_GET['age'] == '50_100') ) ? "selected" : ''); ?>>Above 50 year</option>
                    </select>
                    </div>
					</div>
                    <div class="col-md-2"><div class="form-group">
                    <select name="gender" class="custom-select">
                      <option value="" selected="">Gender</option>
                      <option value="1" <?php echo e((isset($_GET['gender']) && ($_GET['gender'] == '1') ) ? "selected" : ''); ?>>Male</option>
                      <option value="2" <?php echo e((isset($_GET['gender']) && ($_GET['gender'] == '2') ) ? "selected" : ''); ?>>Female</option>
                      <option value="3" <?php echo e((isset($_GET['gender']) && ($_GET['gender'] == '3') ) ? "selected" : ''); ?>>Transsexual </option>
                    </select>
					</div></div>
                    <!-- saif changes starts -->
                   <div class="col-md-2">
                      <div class="form-group">
                         <select name="region" class="custom-select">
                          <?php if(empty($state)): ?>
                            <option selected="" name="states" value="">States</option>
                           <?php $__currentLoopData = $states; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                           <option value="<?php echo e($row->name); ?>"><?php echo e($row->name); ?></option>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                          <?php else: ?>
                           <?php $__currentLoopData = $states; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
  <option value="<?php echo e($row->name); ?>" <?php echo e(($row->name==$state) ? "selected" : ''); ?>>   <?php echo e($row->name); ?></option>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                            <?php endif; ?>
                         </select>
                      </div>
                   </div>  
                    <!-- saif changes end -->
                      <!-- <div class="col-md-2">
                        <div class="form-group"> -->
                        	<!--<input name="country" type="text" class="form-control" value="<?php echo e((isset($_GET['country']) && ($_GET['country'] != '') ) ? $_GET['country'] : ''); ?>">-->
                        <!-- <select name="country" class="custom-select">
                            <option selected="" name="country" value="">Country</option>
                            <option value="Ukraine" <?php echo e((isset($_GET['country']) && ($_GET['country'] == 'Ukraine') ) ? "selected" : ''); ?>>Ukraine</option>
                            <option value="India" <?php echo e((isset($_GET['country']) && ($_GET['country'] == 'India') ) ? "selected" : ''); ?>>India</option>
                            <option value="Morocco" <?php echo e((isset($_GET['country']) && ($_GET['country'] == 'Morocco') ) ? "selected" : ''); ?>>Morocco</option>
                            <option value="China" <?php echo e((isset($_GET['country']) && ($_GET['country'] == 'China') ) ? "selected" : ''); ?>>China</option>
                            <option value="Colombia" <?php echo e((isset($_GET['country']) && ($_GET['country'] == 'Colombia') ) ? "selected" : ''); ?>>Colombia</option>                                
                        </select>
                        </div>
                        </div>   -->
                     
                        <div class="col-md-2"><div class="form-group">
                        	<button class="advance-btn" type="button"><i class="glyphicon glyphicon-cog"></i> More Filter</button>
                        </div></div>
                         <div class="col-md-2"><div class="form-group">
                        	<button type="submit" class="btn btn-primary form-control">Search</button>
                        </div></div>
                </div>
                
                <div class="advance-fields">
                	<div class="row">

                     <div class="col-md-2"><div class="form-group">
                        <select name="status" class="custom-select">
                            <option selected="" value="">Profile Status</option>
                            <option value="yes" <?php echo e((isset($_GET['status']) && ($_GET['status'] == 'yes') ) ? "selected" : ''); ?>>Active</option>
                            <option value="no" <?php echo e((isset($_GET['status']) && ($_GET['status'] == 'no') ) ? "selected" : ''); ?>>Inactive</option>
                           
                        </select>
                        </div></div>
                    	<div class="col-md-2"><div class="form-group">
                        	<select name="adult_level" class="custom-select">
                              <option value="" selected="">Adult level </option>
                              <option value="Sexy" <?php echo e((isset($_GET['adult_level']) && ($_GET['adult_level'] == 'Sexy') ) ? "selected" : ''); ?>>Sexy</option>
                              <option value="Adult" <?php echo e((isset($_GET['adult_level']) && ($_GET['adult_level'] == 'Adult') ) ? "selected" : ''); ?>>Adult</option>
                              <option value="Non-adult" <?php echo e((isset($_GET['adult_level']) && ($_GET['adult_level'] == 'Non-adult') ) ? "selected" : ''); ?>>Non-adult</option>
                              <option value="Explicit" <?php echo e((isset($_GET['adult_level']) && ($_GET['adult_level'] == 'Explicit') ) ? "selected" : ''); ?>>Explicit</option>
                            </select>
                        </div></div>
                        <div class="col-md-2"><div class="form-group">
                        	<select name="profile_type" class="custom-select">
                                <option value="" selected="">Profile Type</option>
                                <option value="Generic" <?php echo e((isset($_GET['profile_type']) && ($_GET['profile_type'] == 'Generic') ) ? "selected" : ''); ?>>Generic</option>
                                <option value="Mature" <?php echo e((isset($_GET['profile_type']) && ($_GET['profile_type'] == 'Mature') ) ? "selected" : ''); ?>>Mature</option>
                                <option value="Kinky" <?php echo e((isset($_GET['profile_type']) && ($_GET['profile_type'] == 'Kinky') ) ? "selected" : ''); ?>>Kinky</option>
                                <option value="Shemale" <?php echo e((isset($_GET['profile_type']) && ($_GET['profile_type'] == 'Shemale') ) ? "selected" : ''); ?>>Shemale</option>
                            </select>
                        </div></div>
                        <div class="col-md-3"><div class="form-group">
                        	<select name="photo" class="custom-select">
                                <option value="" selected="">Photo setting</option>
                                <option value="0" <?php echo e((isset($_GET['photo']) && ($_GET['photo'] == '0') ) ? "selected" : ''); ?>>	Without picture</option>
                                <option value="1" <?php echo e((isset($_GET['photo']) && ($_GET['photo'] == '1') ) ? "selected" : ''); ?>>With pictures</option>
                                <!--<option value="3">Only attachements</option>
                                <option value="4">Picture + attachement</option>-->
                            </select>
                        </div></div>
                           <div class="col-md-3"><div class="form-group">
                        	<select name="no_of_pictures" class="custom-select">
                                <option selected="" value="">Number of pictures</option>
                                <option value="0" <?php echo e((isset($_GET['no_of_pictures']) && ($_GET['no_of_pictures'] == '0') ) ? "selected" : ''); ?>>0</option>
                                <option value="1" <?php echo e((isset($_GET['no_of_pictures']) && ($_GET['no_of_pictures'] == '1') ) ? "selected" : ''); ?>>1</option>
                                <option value="2" <?php echo e((isset($_GET['no_of_pictures']) && ($_GET['no_of_pictures'] == '2') ) ? "selected" : ''); ?>>2</option>
                                <option value="3" <?php echo e((isset($_GET['no_of_pictures']) && ($_GET['no_of_pictures'] == '3') ) ? "selected" : ''); ?>>3</option>
                                <option value="4" <?php echo e((isset($_GET['no_of_pictures']) && ($_GET['no_of_pictures'] == '4') ) ? "selected" : ''); ?>>4</option>
                                <option value="5" <?php echo e((isset($_GET['no_of_pictures']) && ($_GET['no_of_pictures'] == '5') ) ? "selected" : ''); ?>>5</option>
                                <option value="6" <?php echo e((isset($_GET['no_of_pictures']) && ($_GET['no_of_pictures'] == '6') ) ? "selected" : ''); ?>>6+</option>
                            </select>
                        </div></div>
                    </div>
                    <div class="row">
                    	<div class="col-md-3"><div class="form-group">
                        <select name="appearance" class="custom-select">
                            <option value="" selected="">Appearance</option>
                            <option value="Arab" <?php echo e((isset($_GET['appearance']) && ($_GET['appearance'] == 'Arab') ) ? "selected" : ''); ?>>Arab</option>
                            <option value="Asian" <?php echo e((isset($_GET['appearance']) && ($_GET['appearance'] == 'Asian') ) ? "selected" : ''); ?>>Asian</option>
                            <option value="Ebony" <?php echo e((isset($_GET['appearance']) && ($_GET['appearance'] == 'Ebony') ) ? "selected" : ''); ?>>Ebony</option>
                            <option value="Latin" <?php echo e((isset($_GET['appearance']) && ($_GET['appearance'] == 'Latin') ) ? "selected" : ''); ?>>Latin</option>
                            <option value="White" <?php echo e((isset($_GET['appearance']) && ($_GET['appearance'] == 'White') ) ? "selected" : ''); ?>>White </option>
                        </select>
                        </div></div>
                        <div class="col-md-3"><div class="form-group">
                        	<select name="relationship" class="custom-select">
                                <option value="" selected="">Civil status</option>
                                <option value="1" <?php echo e((isset($_GET['relationship']) && ($_GET['relationship'] == '1') ) ? "selected" : ''); ?>>Never Married</option>
                                <option value="2" <?php echo e((isset($_GET['relationship']) && ($_GET['relationship'] == '2') ) ? "selected" : ''); ?>>Separated</option>
                                <option value="3" <?php echo e((isset($_GET['relationship']) && ($_GET['relationship'] == '3') ) ? "selected" : ''); ?>>Divorced</option>
                                <option value="4" <?php echo e((isset($_GET['relationship']) && ($_GET['relationship'] == '4') ) ? "selected" : ''); ?>>Marrioed</option>
                                <option value="5" <?php echo e((isset($_GET['relationship']) && ($_GET['relationship'] == '5') ) ? "selected" : ''); ?>>LAT relationship</option>
                                <option value="6" <?php echo e((isset($_GET['relationship']) && ($_GET['relationship'] == '6') ) ? "selected" : ''); ?>>Living together</option>
                                <option value="7" <?php echo e((isset($_GET['relationship']) && ($_GET['relationship'] == '7') ) ? "selected" : ''); ?>>In a relationship</option>
                            </select>
                        </div></div>
                    </div>
                </div>
            	</form>
            </div>
       

            <div class="panel panel-white"> 
                <div class="panel panel-flat">
                  <div class="panel-heading">
                    <h6 class="panel-title"><b> <?php echo e($heading); ?> </b><a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
                    <div class="heading-elements">
                      <ul class="icons-list">
                        <li> <a type="button" href="<?php echo e(route('user.create')); ?>" class="btn btn-primary text-white btn-labeled btn-rounded "><b><i class="icon-plus3"></i></b> Add Player<span class="legitRipple-ripple" ></span></a></li> 
                      </ul>
                    </div>
                  </div> 
                </div>  
                <div class="panel-body"> 
                 
              </div> 
                 <?php if(Session::has('flash_alert_notice')): ?>
                     <div class="alert alert-success alert-dismissable" style="margin:10px">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                      <i class="icon fa fa-check"></i>  
                     <?php echo e(Session::get('flash_alert_notice')); ?> 
                     </div>
                <?php endif; ?>
                 <div class="table-responsive">
                  <table class="table datatable-basic table-bordered table-hover table-responsive" id="roles_list">
                                   
                                        <thead>
                                            <tr>
                                                <th> Sno. </th>
                                                <th> Full Name </th>
                                                <th> Email </th>
                                                <th> Gender </th>
                                                <th> Age </th>
                                                <th> Region </th>
                                                <!-- <th> City </th> -->
                                                <th> Profile Image </th>
                                                <th> Messages </th>
                                                <th> Poke </th>
                                                <th>Activation Date</th>    
                                                <th>Signup Date</th>
                                                <th>Last Login</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                                
                                            </tr>
                                        </thead>
                                        <tbody>

                                    
                                        <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $result): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <tr>
                                                 <td> <?php echo e((($users->currentpage()-1)*30)+(++$key)); ?></td>
                                                <td> <?php echo e($result->name); ?> <br> <?php echo e(($result->campaign_id == 1) ? '( Landers Page )' : ''); ?> <?php echo e(($result->campaign_id == 2) ? '( One Click )' : ''); ?></td>
                                                <td> <?php echo e($result->email); ?> </td>
                                                <td> 
                                                    <?php 
                                                    if($result->gender == 1) 
                                                    {
                                                        echo 'Male';
                                                    }else if($result->gender == 2)
                                                    {
                                                        echo 'Female';
                                                    }
                                                    ?>
                                                </td>
                                                <td> <?php echo e($result->age); ?> </td>
                                                <td> <?php echo e($result->state); ?> </td>
                                                <!-- <td> <?php echo e($result->city); ?> </td> -->
                                                <td class="center"> 
                                               
                                                    <?php if($result->role_type==5): ?>
                                                    <a href="<?php echo e(url('admin/mytask/'.$result->id)); ?>">
                                                        View Details

                                                    </a>
                                                    <?php else: ?>
                                                        <?php if($result->profile_image): ?>
                                                            <?php if($result->fake == 0): ?>
                                                                <img src="<?php echo e(URL::asset('public/images/profile_image/'.$result->profile_image)); ?>" width="100px" height="100px" onerror="this.src='<?php echo e(asset("public/images/profile_image/no_image.jpg")); ?>'">
                                                            <?php endif; ?>
                                                            <?php if($result->fake == 1): ?>
                                                                <img src="<?php echo e(env('PROFILE_MANAGER_ADDR').('/public/thumbnail/'.$result->profile_image)); ?>" width="100px" height="100px" onerror="this.src='<?php echo e(asset("public/images/profile_image/no_image.jpg")); ?>'">
                                                            <?php endif; ?>
                                                        <?php endif; ?>
                                                    <?php endif; ?>
                                                </td>
                                                <td>  </td>
                                                <td> </td>
                                                <td>
                                                <?php if($result->activation_time): ?>
                                                    <?php echo Carbon\Carbon::parse($result->activation_time)->format('Y-m-d h:i:s');; ?>

                                                <?php else: ?>
                                                    --
                                                <?php endif; ?>    
                                                </td>     
                                                <td>
                                                    <?php echo Carbon\Carbon::parse($result->created_at)->format('Y-m-d h:i:s');; ?>

                                                </td>
                                                <td>
                                                <?php if($result->fake == 0 && $result->last_login != Null): ?>
                                                    <?php echo Carbon\Carbon::parse($result->last_login)->format('Y-m-d h:i:s');; ?>

                                                <?php else: ?>
                                                    --
                                                <?php endif; ?>    
                                                </td>    
                                                <td>
                                                    <?php if(($result->is_deleted == 0)): ?>
                                                    <span class="label label-<?php echo e(($result->status==1)?'success':'warning'); ?> status" id="<?php echo e($result->id); ?>"  data="<?php echo e($result->status); ?>"  onclick="changeStatus(<?php echo e($result->id); ?>,'user')" >
                                                            <?php echo e(($result->status==1)?'Active':'Inactive'); ?>

                                                        </span>
                                                     <?php endif; ?>   
                                                </td>
                                                <td>
                                                    <?php if(($result->is_deleted == 0)): ?>
														<?php if(($result->fake == 0)): ?>
													<a href="<?php echo e(route('user.edit',$result->id)); ?>" class="btn btn-primary btn-xs" style="margin: 3px">
                                                    <i class="icon-pencil7" title="edit"></i>  
                                                    </a> 
													<?php endif; ?>
                                                    <?php echo Form::open(array('class' => 'form-inline pull-left deletion-form', 'method' => 'DELETE',  'id'=>'deleteForm_'.$result->id, 'style'=>'margin:3px', 'route' => array('user.destroy', $result->id))); ?>

                        
                                                    <button class='delbtn btn btn-danger btn-xs' type="submit" name="remove_levels" value="delete" id="<?php echo e($result->id); ?>" onclick=" var c = confirm('Are you sure want to delete?'); if(!c) return false;"><i class="icon-trash" title="Delete"></i> 
                                                    </button> 
                                                    <?php echo Form::close(); ?> 
                                                    <?php endif; ?>
                                                </td>
                                                
                                            </tr>
                                           <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        
                                        </tbody>
                                    </table>
                                    Showing <?php echo e(($users->currentpage()-1)*$users->perpage()+1); ?> to <?php echo e($users->currentpage()*$users->perpage()); ?>

					                of  <?php echo e($users->total()); ?> entries
					                 <div class="center" align="center">  <?php echo $users->appends(['gender' => isset($_GET['gender'])?$_GET['gender']:'', 'country' => isset($_GET['country'])?$_GET['country']:'' ,'age' => isset($_GET['age'])?$_GET['age']:'', 'photo' => isset($_GET['photo'])?$_GET['photo']:'', 'relationship' => isset($_GET['relationship'])?$_GET['relationship']:'' , 'profile_type' => isset($_GET['profile_type'])?$_GET['profile_type']:'' , 'adult_level' => isset($_GET['adult_level'])?$_GET['adult_level']:'' , 'appearance' => isset($_GET['appearance'])?$_GET['appearance']:'' ,
                                     'last_part' => isset($_GET['last_part'])?$_GET['last_part']:'' , 'no_of_pictures' => isset($_GET['no_of_pictures'])?$_GET['no_of_pictures']:'' , 'status' => isset($_GET['status'])?$_GET['status']:''])->render(); ?></div>    
                                
                 </div> 
               </div>
         </div> 
   <?php $__env->stopSection(); ?>
   
   
   
<?php echo $__env->make('admin::layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>