<style>
.accordion {
  background-color: #eee;
  color: #444;
  cursor: pointer;
  padding: 12px;
  width: 100%;
  border: none;
  text-align: left;
  outline: none;
  font-size: 15px;
  transition: 0.4s;
}

.active, .accordion:hover {
  background-color: #ccc; 
}

.panel {
  padding: 0 18px;
  display: none;
  background-color: white;
  overflow: hidden;
}
</style>



<div class="profile_img">
	<img  src="{{ asset('public/images/cover_img') }}/cover_img_13.jpg" alt="Profile Image">
</div>
                 
<h3>POST BACK DOCUMENTATION</h3>
<p>
	When you hit our post back url <br>
	we should required some data of user compulsory<br>
	
	we will receive the data by GET method <br>
	the field are as follows<br>
	
	
	<p>
		url names and values<br/>
		<table>
			<thead>
				<th>Form name</th>
				<th>Form value</th>
			</thead>
			<tbody>
				<tr>
					<td>name</td>
					<td>xyz</td>
				</tr>
				<tr>
					<td>email</td>
					<td>xyz@xyz.com</td>
				</tr>
				<tr>
					<td>password</td>
					<td>123abc</td>
				</tr>
				<tr>
					<td>phone</td>
					<td>123456789</td>
				</tr>
				<tr>
					<td>gender</td>
					<td>1 or 2 (1=male and 2=female)</td>
				</tr>
				<tr>
					<td>dob</td>
					<td>10-05-2000</td>
				</tr>
				<tr>
					<td>country</td>
					<td>United Kingdom</td>
				</tr>
				<tr>
					<td>region</td>
					<td>Greater London</td>
				</tr>
				<tr>
					<td>city</td>
					<td>Oxford</td>
				</tr>
				<tr>
					<td>partner_id</td>
					<td>xy123456</td>
				</tr>
				<tr>
					<td>address</td>
					<td>1,sole home street</td>
				</tr>
				<tr>
					<td>termsaccept</td>
					<td>1 (1=true)</td>
				</tr>
			</tbody>
		</table>
	</p>
	
	1.Partner id (Required) - The partner id should be correct and registered with our system.<br/>
	2.Name (Required) - We Required Name of the user (Name should not be empty)<br/>
	3.Email (Required) - We Required Email of the user (Email should not be empty or not alredy registered with our system)<br/>
	4.Password (Required) - We Required Password of the user (Password should not be empty and must be 4 or more length character)<br/>
	5.Date of Birth (Required) - We Required Date of birth of the user (Date of Birth should not be empty and must be in format of dd-mm-yyyy or mm-dd-yyyy)<br/>
	6.Gender (Required) - We Required Gender of the user (Gender should not be empty and values must be 1 or 2 (1 for male and 2 for female))<br/>
	7.Phone No.(Optional) - Contact no. of the user <br/>
	8.Country and Region(Required) - We Required Country of the user (Country should not be empty and country must be in our system)<br/>
	these are our countries and their regions list:<br/>
	
	<div>
		 
		@foreach($countries as $country) 
			
			<?php $country_id = $country->id; $country_name = $country->name; ?>
			
			<h3><?php echo $country_name ?></h3>
			
			<?php $region_lists = DB::table('states')->select('states.*')->where('states.country_id',$country_id)->get(); ?>
			
			@foreach($region_lists as $region_list)
				<button class="accordion"><?php echo $region_list->name; $region_id = $region_list->id;?></button>
				<div class="panel">		
					<?php $cities_lists = DB::table('cities')->select('cities.*')->where('cities.state_id',$region_id)->get(); ?>
					@foreach($cities_lists as $cities_list)
						<p><?php echo $cities_list->name; ?></p>	
					@endforeach	
			</div>
			@endforeach
			
		@endforeach
	</div>
	<br/>
	9.City(Optional) - City of the user otional<br/>
	10.Address(Optional) - Address of the user optional<br/>
	11.Terms And Conditions (Required) - We Required that users must accept our terms and conditions (to accept t&c we accept value 1 as true)<br/>
	<br>

<script>
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.display === "block") {
      panel.style.display = "none";
    } else {
      panel.style.display = "block";
    }
  });
}
</script>