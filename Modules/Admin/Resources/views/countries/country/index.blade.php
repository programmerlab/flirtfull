@extends('admin::layouts.master')
 
    @section('content') 
      @include('admin::partials.navigation')
      @include('admin::partials.breadcrumb')   

       @include('admin::partials.sidebar')  
            <div class="panel panel-white"> 
                <div class="panel panel-flat">
                  <div class="panel-heading">
                    <h6 class="panel-title"><b> {{$heading }} </b><a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
                    
                  </div> 
                </div>  
                <div class="panel-body"> 
                  <div class="table-toolbar">
                    <div class="row">
                      <form action="{{route('country')}}" method="get" id="filter_data">
                      <div class="col-md-2">
                          <input value="{{ (isset($_REQUEST['search']))?$_REQUEST['search']:''}}" placeholder="search by country name" type="text" name="search" id="search" class="form-control" >
                      </div>
                      <div class="col-md-2">
                          <input type="submit" value="Search" class="btn btn-primary form-control">
                      </div>
                    </form>
                    </div>
                </div> 
            </div> 
                 @if(Session::has('flash_alert_notice'))
                     <div class="alert alert-success alert-dismissable" style="margin:10px">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                      <i class="icon fa fa-check"></i>  
                     {{ Session::get('flash_alert_notice') }} 
                     </div>
                @endif
                 <div class="table-responsive">
                  <table class="table datatable-basic table-bordered table-hover table-responsive" id="roles_list">
                                   
                                        <thead>
                                            <tr>
                                                <th> S no. </th>
                                                <th> Shortname </th>
                                                <th> Country Name</th>
                                                <th> Status </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($countries as $key => $result)
                                        <tr>
                                          <td>{{ (($countries->currentpage()-1)*15)+(++$key) }}</td>
                                          <td> {{$result->sortname}} </td>
                                          <td> {{ucwords($result->name)}} </td>
                                          <td>
                                              <span class="label label-{{ ($result->status==1)?'success':'warning'}} status" id="{{$result->id}}"  data="{{$result->status}}"  onclick="changeStatus({{$result->id}},'country')" >
                                                      {{ ($result->status==1)?'Active':'Inactive'}}
                                                  </span>
                                          </td>
                                          
                                        </tr>
                                        @endforeach 
                                        </tbody>
                                    </table>
                                    Showing {{($countries->currentpage()-1)*$countries->perpage()+1}} to {{$countries->currentpage()*$countries->perpage()}}
					                of  {{$countries->total()}} entries
					                 <div class="center" align="center"> {!! $countries->render() !!} </div>    
                                
                 </div> 
               </div>
         </div> 
   @stop
   
   
   