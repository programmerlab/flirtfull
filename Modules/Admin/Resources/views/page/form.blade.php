<fieldset class="step ui-formwizard-content" id="step1" style="display: block;">

    <div class="row">
        <div class="col-md-10">
           <!-- <h4><center>{{ implode(' ',explode('-',$page->slug)) }}</center></h4> -->

           <div class="form-group {{ $errors->first('title', ' has-error') }}">
            <label class="col-lg-4 col-md-4 control-label"> Page Heading <span class="error">*</span></label>
            <div class="col-lg-8 col-md-8"> 
                {!! Form::text('title',null, ['class' => 'form-control form-cascade-control input-small'])  !!} 
                <span class="help-block" style="color:red">{{ $errors->first('title', ':message') }}</span>
            </div>
        </div>  



        <div class="form-group {{ $errors->first('description', ' has-error') }}">
            <label class="col-lg-4 col-md-4 control-label">Page Content <span class="error">*</span></label>
            <div class="col-lg-8 col-md-8"> 
                {!! Form::textarea('description',null, ['class' => 'form-control summernote','data-required'=>1,'rows'=>3,'cols'=>5])  !!}
                <span class="help-block"  style="color:red">{{ $errors->first('description', ':message') }}</span>
                @if(Session::has('flash_alert_notice')) 
                <span class="label label-danger">

                    {{ Session::get('flash_alert_notice') }} 

                </span>@endif
            </div>
        </div> 


        <div class="form-group {{ $errors->first('image', ' has-error') }}">
            <label class="col-lg-4 col-md-4 control-label">Banner Image </label>
            <div class="col-lg-8 col-md-8">  

               {!! Form::file('image',null,['class' => 'form-control form-cascade-control input-small'])  !!}
               <br>
               @if(!empty($page->image))
               <img src="{!! url('storage/uploads/pages/'.$page->image) !!}" width="100px">
               <input type="hidden" name="images" value="{!! $page->image !!}">
               @endif                                       
               <span class="help-block"  style="color:red">{{ $errors->first('image', ':message') }}</span>
               @if(Session::has('flash_alert_notice')) 
               <span class="label label-danger">

                {{ Session::get('flash_alert_notice') }} 

            </span>@endif
        </div>
    </div>


    <div class="form-group {{ $errors->first('meta_key', ' has-error') }}">
        <label class="col-lg-4 col-md-4 control-label"> Meta Key <span class="error"> </span></label>
        <div class="col-lg-8 col-md-8"> 
            {!! Form::text('meta_key',null, ['class' => 'form-control form-cascade-control input-small'])  !!} 
            <span class="help-block" style="color:red">{{ $errors->first('meta_key', ':message') }}</span>
        </div>
    </div> 


    <div class="form-group {{ $errors->first('meta_description', ' has-error') }}">
        <label class="col-lg-4 col-md-4 control-label">Meta Description<span class="error"> </span></label>
        <div class="col-lg-8 col-md-8"> 
            {!! Form::text('meta_description',null, ['class' => 'form-control form-cascade-control input-small'])  !!} 
            <span class="help-block" style="color:red">{{ $errors->first('meta_description', ':message') }}</span>
        </div>
    </div> 

    <div class="form-group {{ $errors->first('page_title', ' has-error') }}">
        <label class="col-lg-4 col-md-4 control-label">Page Title <span class="error"> </span></label>
        <div class="col-lg-8 col-md-8"> 
            {!! Form::text('page_title',null, ['class' => 'form-control form-cascade-control input-small'])  !!} 
            <span class="help-block" style="color:red">{{ $errors->first('page_title', ':message') }}</span>
        </div>
    </div> 
    <div class="form-group">
        <label class="col-lg-4 col-md-4 control-label"></label>
        <div class="col-lg-8 col-md-8">

            {!! Form::submit(' Save ', ['class'=>'btn  btn-primary text-white','id'=>'saveBtn']) !!}

            <a href="{{route('page')}}">
                {!! Form::button('Back', ['class'=>'btn btn-warning text-white']) !!} </a>
            </div>
        </div>

    </div> 
</div> 

</fieldset > 