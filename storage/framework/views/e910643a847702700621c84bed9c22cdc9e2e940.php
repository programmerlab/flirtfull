
<?php $__env->startSection('content'); ?>

<div class="activity_wrapper">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
			   <div class="activity_tab">
				   	<ul class="tab_menu">
				   		<a href="<?php echo e(url('activities')); ?>"><li <?php echo e(($segment == 'like') ? 'class=active' : ''); ?>>like</li></a>
				   		<a href="<?php echo e(url('activities/likeme')); ?>"><li <?php echo e(($segment == 'likeme') ? 'class=active' : ''); ?>>Liked Me</li></a>
				   		<a href="<?php echo e(url('activities/favorite')); ?>"><li <?php echo e(($segment == 'favorite') ? 'class=active' : ''); ?>>favorite</li></a>
				   		<a href="<?php echo e(url('activities/favoriteme')); ?>"><li <?php echo e(($segment == 'favoriteme') ? 'class=active' : ''); ?>>favorited Me</li></a>
				   		<a href="<?php echo e(url('activities/match')); ?>"><li <?php echo e(($segment == 'match') ? 'class=active' : ''); ?>>match</li></a>
				   		<a href="<?php echo e(url('activities/visit')); ?>"><li <?php echo e(($segment == 'visit') ? 'class=active' : ''); ?>>visit</li></a>
				   		<a href="<?php echo e(url('activities/visitme')); ?>"><li <?php echo e(($segment == 'visitme') ? 'class=active' : ''); ?>>visited Me</li></a>
				   		<li data-show="payment_tab">payment</li>
				   	</ul>
			   </div>  
			   <div class="tab_content_wrapper active_accordion">
			   	   <!-- tab Start -->
			   	   <div class="tab_content <?php echo e(($segment == 'like') ? 'active' : ''); ?>" id="like_tab">
			   	   	  <div class="tab_heading active">Like<span class="tab_btn"></span></div>
					   <div class="tab_content_inner">
						<?php if($like && !$like->isEmpty()){ ?>
						<?php $__currentLoopData = $like; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $likes): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						<?php  //type = 3 that Visited by Me	
							$like_id 		= $likes->receiver_id;	
							$like_details	= DB::table('users')->select('users.*')->where('users.id',$like_id)->get();
                            if($like_details[0]->fake){
                                $visited_image	= env('PROFILE_MANAGER_ADDR').('/public/thumbnail/'.$like_details[0]->profile_image);
                            }else{
                                $visited_image	= url('/public/thumbnail/'.$like_details[0]->profile_image);
                            }
							$provider_name	= $like_details[0]->provider_name;
							$gender			= $like_details[0]->gender;// 1=male   2=female
							if($gender == 1){	  $blank_image = url('/public/images/male_icon.png'); }
							else{				  $blank_image = url('/public/images/lady_icon.png'); }
						?>
						<div class="act_profile">
						   	 <div class="act_profile_inr">
						   	 	<div class="act_pic_dv">
							   	 	<div class="act_pic">
							   	 		<?php if($like_details[0]->profile_image == ''){ ?>
											<img src="<?php echo e($blank_image); ?>" alt="" class="img-responsive">
										<?php }elseif($provider_name == 'google'){ ?>
											<img src="<?php echo e($like_details[0]->profile_image); ?>" alt="" class="img-responsive">
										<?php }else{ ?>
											<img src="<?php echo e($visited_image); ?>" alt="" class="img-responsive">
										<?php } ?>
							   	 	</div>
						   	 	</div>
						   	 	<div class="act_pic_content">
						   	 		<h3>
										<a href="<?php echo e(url('profile')); ?>/<?php echo e($like_id); ?>"><?php echo e($like_details[0]->name); ?>,<span><?php echo e($like_details[0]->age); ?></span></a>
										<span class="active_status <?php echo e(($like_details[0]->is_login) ? 'online' : 'offline'); ?>">
											<i class="fa fa-circle"></i>
										</span>
									</h3>
						   	 		<span class="act_time"><?php echo e($like_details[0]->state); ?></span>
						   	 	</div>
						   	 </div>
					   	 </div>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <div class="flirt_pagination"><?php echo $like->render(); ?></div>
						<?php }else{ ?>												
									<h5>No likes yet. Visit our <a href="<?php echo e(url('/search')); ?>">members</a> page and find someone you like.</h5>															
						<?php } ?>
					   </div>
				   </div>
				   <!-- tab End -->
				    <!-- tab Start -->
			   	   <div class="tab_content <?php echo e(($segment == 'likeme') ? 'active' : ''); ?>" id="likedMe_tab">
			   	   	  <div class="tab_heading">Liked Me<span class="tab_btn"></span></div>
					   <div class="tab_content_inner">
                        <?php if(( isset($wallet->premiums) && ($wallet->premiums > 0) && ($wallet->premiums > time() ) )): ?>
					   	<?php if($like_me && !$like_me->isEmpty()){ ?>
						<?php $__currentLoopData = $like_me; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $liked_me): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						<?php  //type = 3 that Visited by Me	
							$liked_id 		= $liked_me->sender_id;	
							$liked_details	= DB::table('users')->select('users.*')->where('users.id',$liked_id)->get();
                            if($liked_details[0]->fake){
                                $liked_image	= env('PROFILE_MANAGER_ADDR').('/public/thumbnail/'.$liked_details[0]->profile_image);
                            }else{
                                $liked_image	= url('/public/thumbnail/'.$liked_details[0]->profile_image);
                            }
                            
							$provider_name	= $liked_details[0]->provider_name;
							$gender			= $liked_details[0]->gender;// 1=male   2=female
							if($gender == 1){	  $blank_image = url('/public/images/male_icon.png'); }
							else{				  $blank_image = url('/public/images/lady_icon.png'); }
						?>
						<div class="act_profile">
						   	 <div class="act_profile_inr">
						   	 	<div class="act_pic_dv">
							   	 	<div class="act_pic">
							   	 		<?php if($liked_details[0]->profile_image == ''){ ?>
											<img src="<?php echo e($blank_image); ?>" alt="" class="img-responsive">
										<?php }elseif($provider_name == 'google'){ ?>
											<img src="<?php echo e($liked_details[0]->profile_image); ?>" alt="" class="img-responsive">
										<?php }else{ ?>
											<img src="<?php echo e($liked_image); ?>" alt="" class="img-responsive">
										<?php } ?>
							   	 	</div>
						   	 	</div>
						   	 	<div class="act_pic_content">
						   	 		<h3>
										<a href="<?php echo e(url('profile')); ?>/<?php echo e($liked_id); ?>"><?php echo e($liked_details[0]->name); ?>,
											<span><?php echo e($liked_details[0]->age); ?></span>
										</a>
										<span class="active_status <?php echo e(($liked_details[0]->is_login) ? 'online' : 'offline'); ?>">
											<i class="fa fa-circle"></i>
										</span>
									</h3>
						   	 		<span class="act_time"><?php echo e($liked_details[0]->state); ?></span>
						   	 	</div>
						   	 </div>
					   	 </div>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <div class="flirt_pagination"><?php echo $like_me->render(); ?></div>
						<?php }else{ ?>						
								<h5>( you are not so popular yet. Engage with our hot <a href="<?php echo e(url('/search')); ?>">members</a> and increase your chances.</h5>									
						<?php } ?>
                        <?php else: ?>
                            <h5> You are not a premium member. Get a <a >premium </a> plan to show these records. </a></h5>
                            <?php if((Request::segment(2)) == 'likeme'): ?>
                            	<?php echo $__env->make('premiumslider', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>;
                            <?php endif; ?>	
                        <?php endif; ?>
					   </div>
				   </div>
				   <!-- tab End -->
				   <!-- tab Start -->
			   	   <div class="tab_content <?php echo e(($segment == 'favorite') ? 'active' : ''); ?>" id="favorite_tab">
			   	   	    <div class="tab_heading">favorite<span class="tab_btn"></span></div>
					    <div class="tab_content_inner">					
							<?php if($favourite && !$favourite->isEmpty()){ ?>
							<?php $__currentLoopData = $favourite; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $favourites): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<?php  //type = 3 that favourited by Me	
								$favourite_id 		= $favourites->to_id;	
								$favourite_details	= DB::table('users')->select('users.*')->where('users.id',$favourite_id)->get();
                                if($favourite_details[0]->fake){
                                    $favourite_image	= env('PROFILE_MANAGER_ADDR').('/public/thumbnail/'.$favourite_details[0]->profile_image);
                                }else{
                                    $favourite_image	= url('/public/thumbnail/'.$favourite_details[0]->profile_image);
                                }
								
								$provider_name		= $favourite_details[0]->provider_name;
								$gender				= $favourite_details[0]->gender;// 1=male   2=female
								if($gender == 1){	  $blank_image = url('/public/images/male_icon.png'); }
								else{				  $blank_image = url('/public/images/lady_icon.png'); }
							?>
							<div class="act_profile">
								 <div class="act_profile_inr">
									<div class="act_pic_dv">
										<div class="act_pic">
											<?php if($favourite_details[0]->profile_image == ''){ ?>
												<img src="<?php echo e($blank_image); ?>" alt="" class="img-responsive">
											<?php }elseif($provider_name == 'google'){ ?>
												<img src="<?php echo e($favourite_details[0]->profile_image); ?>" alt="" class="img-responsive">
											<?php }else{ ?>
												<img src="<?php echo e($favourite_image); ?>" alt="" class="img-responsive">
											<?php } ?>
										</div>
									</div>
									<div class="act_pic_content">
										<h3>
											<a href="<?php echo e(url('profile')); ?>/<?php echo e($favourite_id); ?>"><?php echo e($favourite_details[0]->name); ?>, <span><?php echo e($favourite_details[0]->age); ?></span></a>
											<span class="active_status <?php echo e(($favourite_details[0]->is_login) ? 'online' : 'offline'); ?>">
												<i class="fa fa-circle"></i>
											</span>
										</h3>
										<span class="act_time"><?php echo e($favourite_details[0]->state); ?></span>
									</div>
								 </div>
							 </div>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <div class="flirt_pagination"><?php echo $favourite->render(); ?></div>
						<?php }else{ ?>						
										<h5>No records to be shown. Browse our <a href="<?php echo e(url('/search')); ?>">members</a> and add them to your favorite list. </h5>								
						<?php } ?>
					   	</div>
				   </div>
				   <!-- tab End -->
				   <!-- tab Start -->
			   	   <div class="tab_content <?php echo e(($segment == 'favoriteme') ? 'active' : ''); ?>" id="favoritedMe_tab">
			   	   	    <div class="tab_heading">favorited Me<span class="tab_btn"></span></div>
					    <div class="tab_content_inner">
                        <?php if(( isset($wallet->premiums) && ($wallet->premiums > 0) && ($wallet->premiums > time() ) )): ?>
						<?php if($favourite_me && !$favourite_me->isEmpty()){ ?>
						<?php $__currentLoopData = $favourite_me; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $favourited): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						<?php  //type = 3 that Visited Me	
							$favourited_id 		= $favourited->from_id;	
							$favourited_details	= DB::table('users')->select('users.*')->where('users.id',$favourited_id)->get();
							if($favourited_details[0]->fake){
                                $favourited_image 	= env('PROFILE_MANAGER_ADDR').('/public/thumbnail/'.$favourited_details[0]->profile_image);
                            }else{
                                $favourited_image 	= url('/public/thumbnail/'.$favourited_details[0]->profile_image);
                            }
                            
							$provider_name		= $favourited_details[0]->provider_name;
							$gender				= $favourited_details[0]->gender;// 1=male   2=female
							if($gender == 1){	  $blank_image = url('/public/images/male_icon.png'); }
							else{				  $blank_image = url('/public/images/lady_icon.png'); }
						?>
						<div class="act_profile">
						   	 <div class="act_profile_inr">
						   	 	<div class="act_pic_dv">
							   	 	<div class="act_pic">
							   	 		<?php if($favourited_details[0]->profile_image == ''){ ?>
											<img src="<?php echo e($blank_image); ?>" alt="" class="img-responsive">
										<?php }elseif($provider_name == 'google'){ ?>
											<img src="<?php echo e($favourited_details[0]->profile_image); ?>" alt="" class="img-responsive">
										<?php }else{ ?>
											<img src="<?php echo e($favourited_image); ?>" alt="" class="img-responsive">
										<?php } ?>
							   	 	</div>
							   	 </div>
						   	 	<div class="act_pic_content">
						   	 		<h3>
										<a href="<?php echo e(url('profile')); ?>/<?php echo e($favourited_id); ?>"><?php echo e($favourited_details[0]->name); ?>, <span><?php echo e($favourited_details[0]->age); ?></span></a>
										<span class="active_status <?php echo e(($favourited_details[0]->is_login) ? 'online' : 'offline'); ?>">
											<i class="fa fa-circle"></i>
										</span>
									</h3>
						   	 		<span class="act_time"><?php echo e($favourited_details[0]->state); ?></span>
						   	 	</div>
						   	 </div>
					   	 </div>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <div class="flirt_pagination"><?php echo $favourite_me->render(); ?></div>
						<?php }else{ ?>						
										<h5>:( you are not so popular yet. Engage with our hot <a href="<?php echo e(url('/search')); ?>">members</a> and increase your chances. </h5>						
						<?php } ?>
                        <?php else: ?>
                            <h5> You are not a premium member. Get a <a >premium </a> plan to show these records. </a></h5>
                           <?php if((Request::segment(2)) == 'favoriteme'): ?>
                            	<?php echo $__env->make('premiumslider', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>;
                            <?php endif; ?>  
                        <?php endif; ?>
					  </div>
				   </div>
				   <!-- tab End -->
				   <!-- tab Start -->
			   	   <div class="tab_content <?php echo e(($segment == 'match') ? 'active' : ''); ?>" id="match_tab">
			   	   	    <div class="tab_heading">match<span class="tab_btn"></span></div>
					   	<div class="tab_content_inner">	
						<?php if($like && !$like->isEmpty()){ ?>
						<?php $__currentLoopData = $like; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $likes): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						<?php  //type = 3 that are matched
							$like_id = $likes->receiver_id;	
							
							$matched_ids = DB::table('likes')
										->select('sender_id')
										->where('sender_id',$like_id)
										->where('receiver_id',Auth::id())
										->where('type',1)
										->get();
										
							if(!$matched_ids->isEmpty())
							{
								$matched_id 	= $matched_ids[0]->sender_id;	
								$matched_details= DB::table('users')->select('users.*')->where('users.id',$matched_id)->get();
								if($matched_details[0]->fake){
                                    $matched_image 	= env('PROFILE_MANAGER_ADDR').('/public/thumbnail/'.$matched_details[0]->profile_image);
                                }else{
                                    $matched_image 	= url('/public/thumbnail/'.$matched_details[0]->profile_image);
                                }
                                
								$provider_name	= $matched_details[0]->provider_name;
								$gender			= $matched_details[0]->gender;// 1=male   2=female
								if($gender == 1){	  $blank_image = url('/public/images/male_icon.png'); }
								else{				  $blank_image = url('/public/images/lady_icon.png'); }
						?>	
						<div class="act_profile">
						   	 <div class="act_profile_inr">
						   	 	<div class="act_pic_dv">
							   	 	<div class="act_pic">
							   	 		<?php if($matched_details[0]->profile_image == ''){ ?>
											<img src="<?php echo e($blank_image); ?>" alt="" class="img-responsive">
										<?php }elseif($provider_name == 'google'){ ?>
											<img src="<?php echo e($matched_details[0]->profile_image); ?>" alt="" class="img-responsive">
										<?php }else{ ?>
											<img src="<?php echo e($matched_image); ?>" alt="" class="img-responsive">
										<?php } ?>
							   	 	</div>
							   	 </div>
						   	 	<div class="act_pic_content">
						   	 		<h3>
										<a href="<?php echo e(url('profile')); ?>/<?php echo e($matched_id); ?>"><?php echo e($matched_details[0]->name); ?>,<span><?php echo e($matched_details[0]->age); ?></span></a>
										<span class="active_status <?php echo e(($matched_details[0]->is_login) ? 'online' : 'offline'); ?>">
											<i class="fa fa-circle"></i>
										</span>
									</h3>
						   	 		<span class="act_time"><?php echo e($matched_details[0]->state); ?></span>
						   	 	</div>
						   	 </div>
					   	 </div>	
						<?php }	?>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <div class="flirt_pagination"></div>
						<?php }else{ ?>						
										<h5>( you are not so popular yet. Engage with our hot <a href="<?php echo e(url('/search')); ?>">members</a> and increase your chances. </h5>									
						<?php } ?>
					   	</div>
				   </div>
				   <!-- tab End -->
				   <!-- tab Start -->
			   	   <div class="tab_content <?php echo e(($segment == 'visit') ? 'active' : ''); ?>" id="visit_tab">
			   	   	    <div class="tab_heading">visit<span class="tab_btn"></span></div>
				  		<div class="tab_content_inner">
                        
						<?php if(!empty($visit)){ ?>
						<?php $__currentLoopData = $visit; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $visits): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						<?php  //type = 3 that Visited by Me	
							$visit_id 			= $visits;	
							$visited_details	= DB::table('users')->select('users.*')->where('users.id',$visit_id)->get();
                            if($visited_details[0]->fake){
                                $visited_image		= env('PROFILE_MANAGER_ADDR').('/public/thumbnail/'.$visited_details[0]->profile_image);
                            }else{
                                $visited_image		= url('/public/thumbnail/'.$visited_details[0]->profile_image);
                            }
							
							$provider_name		= $visited_details[0]->provider_name;
							$gender				= $visited_details[0]->gender;// 1=male   2=female
							if($gender == 1){	  $blank_image = url('/public/images/male_icon.png'); }
							else{				  $blank_image = url('/public/images/lady_icon.png'); }
						?>
						<div class="act_profile">
						   	 <div class="act_profile_inr">
						   	 	<div class="act_pic_dv">
							   	 	<div class="act_pic">
							   	 		<?php if($visited_details[0]->profile_image == ''){ ?>
											<img src="<?php echo e($blank_image); ?>" alt="" class="img-responsive">
										<?php }elseif($provider_name == 'google'){ ?>
											<img src="<?php echo e($visited_details[0]->profile_image); ?>" alt="" class="img-responsive">
										<?php }else{ ?>
											<img src="<?php echo e($visited_image); ?>" alt="" class="img-responsive">
										<?php } ?>
							   	 	</div>
						   	 	</div>
						   	 	<div class="act_pic_content">
						   	 		<h3>
										<a href="<?php echo e(url('profile')); ?>/<?php echo e($visit_id); ?>">
											<?php echo e($visited_details[0]->name); ?>, <span><?php echo e($visited_details[0]->age); ?></span>
										</a>
										<span class="active_status <?php echo e(($visited_details[0]->is_login) ? 'online' : 'offline'); ?>">
											<i class="fa fa-circle"></i>
										</span>
									</h3>
						   	 		<span class="act_time"><?php echo e($visited_details[0]->state); ?></span>,
						   	 		<div class="visit_count">
										<i class="fa fa-eye" aria-hidden="true"></i>
										(<?php echo e($visit_count[$visits]); ?>)
									</div>
						   	 	</div>
						   	 </div>
					   	 </div>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <div class="flirt_pagination"><?php echo $visit_pagination; ?></div>
						<?php }else{ ?>						
								<h5>No records to be shown. Browse our <a href="<?php echo e(url('/search')); ?>">members</a> and they will be added here.</h5>									
						<?php } ?>
					   	</div>
				   	</div>
				   <!-- tab End -->
				   <!-- tab Start -->
			   	   <div class="tab_content <?php echo e(($segment == 'visitme') ? 'active' : ''); ?>" id="visitedMe_tab">
			   	   	    <div class="tab_heading">visit Me<span class="tab_btn"></span></div>
				  		<div class="tab_content_inner">
                        <?php if(( isset($wallet->premiums) && ($wallet->premiums > 0) && ($wallet->premiums > time() ) )): ?>
					   	<?php if(!empty($visited_me)){ ?>
						<?php $__currentLoopData = $visited_me; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $visited): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						<?php  //type = 3 that Visited Me	
							$visitor_id 		= $visited;	
							$visitor_details	= DB::table('users')->select('users.*')->where('users.id',$visitor_id)->get();
                            if($visitor_details[0]->fake){
                                $visitor_image 		= env('PROFILE_MANAGER_ADDR').('/public/thumbnail/'.$visitor_details[0]->profile_image);
                            }else{
                                $visitor_image 		= url('/public/thumbnail/'.$visitor_details[0]->profile_image);
                            }
							
							$provider_name		= $visitor_details[0]->provider_name;
							$gender				= $visitor_details[0]->gender;// 1=male   2=female
							if($gender == 1){	  $blank_image = url('/public/images/male_icon.png'); }
							else{				  $blank_image = url('/public/images/lady_icon.png'); }
						?>
						<div class="act_profile">
						   	 <div class="act_profile_inr">
						   	 	<div class="act_pic_dv">
							   	 	<div class="act_pic">
							   	 		<?php if($visitor_details[0]->profile_image == ''){ ?>
											<img src="<?php echo e($blank_image); ?>" alt="" class="img-responsive">
										<?php }elseif($provider_name == 'google'){ ?>
											<img src="<?php echo e($visitor_details[0]->profile_image); ?>" alt="" class="img-responsive">
										<?php }else{ ?>
											<img src="<?php echo e($visitor_image); ?>" alt="" class="img-responsive">
										<?php } ?>
							   	 	</div>
							   	 </div>
						   	 	<div class="act_pic_content">
						   	 		<h3>
										<a href="<?php echo e(url('profile')); ?>/<?php echo e($visitor_id); ?>">
											<?php echo e($visitor_details[0]->name); ?>,<span><?php echo e($visitor_details[0]->age); ?></span>
										</a>
                                        <span class="active_status <?php echo e(($visitor_details[0]->is_login) ? 'online' : 'offline'); ?>">
											<i class="fa fa-circle"></i>
										</span>    
									</h3>
						   	 		<span class="act_time"><?php echo e($visitor_details[0]->state); ?></span>,
									<div class="visit_count">
										<i class="fa fa-eye" aria-hidden="true"></i>						
										(<?php echo e($visited_me_count[$visited]); ?>)
									</div>
						   	 	</div>
						   	 </div>
					   	 </div>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <div class="flirt_pagination"><?php echo $visited_pagination; ?></div>
						<?php }else{ ?>						
								<h5>( you are not so popular yet. Engage with our hot <a href="<?php echo e(url('/search')); ?>">members</a> and increase your chances.</h5>									
						<?php } ?>
                        <?php else: ?>
                            <h5> You are not a premium member. Get a <a >premium </a> plan to show these records. </a></h5>
                            <?php if((Request::segment(2)) == 'visitme'): ?>
                            	<?php echo $__env->make('premiumslider', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>;
                            <?php endif; ?>    
                        <?php endif; ?>
					   	</div>
				   	</div>
				   <!-- tab End -->
				   <!-- tab Start -->
			   	   <div class="tab_content" id="payment_tab">
			   	   	  <div class="tab_heading">Payment<span class="tab_btn"></span></div>
					   <div class="tab_content_inner">
					   	 <div class="payment_table table-responsive">
					   	 	<table class="table payment_tables">
								<thead>
									<tr>
                                        <td>Sr. No.</td>
										<td>Date</td>
										<td>Type</td>
										<td>Amount</td>
									</tr>
								</thead>
					   	 		<tbody>
									<?php 
										if(!empty($payments))
										{                                        
											//type = 4 payment
                                            array_reverse(array_sort($payments));
											foreach($payments as $key=>$payment)
											{
												$time 	= $payment->created_at;
												$date 	= date("d-m-Y",strtotime($time));		
												$type 	= $payment->plan_type;		
												$amount = $payment->amount;		
									?>
									<tr>
                                        <td><?php echo e(($key + 1)); ?></td>
										<td><?php echo e($date); ?></td>
										<td><?php echo e($type); ?></td>
										<td><?php echo e($amount); ?></td>
									</tr>
									<?php 	}
										} 
										else 
										{
										?>
									<tr>
										<td colspan="3"><center>No records to be shown.</center></td>
									</tr>
										<?php } ?>
								</tbody>
					   	 	</table>
					   	 </div>
					   </div>
				   </div>
				   <!-- tab End -->
			   </div>
			</div>
		</div>
	</div>
</div>

<!-- chat notification -->
  <div class="chat_notifi_box_wrap">
    <div class="chat_notifi_box">
      <div class="img_wrapper">
        <img src="https://moneoangels.com/public/images/profile_image/crop_1556187992.png" class="noti_image" alt="profile_img">
      </div>
      <div class="ct_info_wrapper">
        <div class="title_head">
          <h4 id="noti_name">MarinaBeauty, <span class="age">27</span></h4>
          <span class="active_status online">
            <i class="fa fa-circle"></i>
          </span>
        </div>
        <p>is online ! You may get lucky with her! </p>
        <a href="#" class="noti_chat_btn"><i class="fa fa-commenting-o"></i>Chat</a>
      </div>
    </div>
  </div>
  <!-- chat notification -->

<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css"/>
<script type="text/javascript" src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script>
	//tab menu css
	$(document).ready(function(){
	  //tab menu js script
	  $('.tab_menu li').on('click',function(){
	    $('.tab_menu li').removeClass("active");
	    $(this).addClass("active");
	    var content= $(this).attr('data-show');
	    $(".tab_content").removeClass("active");
	    $("#"+content).addClass("active");
	  });
	  //tab accordion
	  $(".tab_content:first-child .tab_content_inner").addClass("c_active").slideDown();
	  $(".tab_heading").on("click", function(){
	  	$(".tab_heading").removeClass("active");
	  	//$(".tab_heading").not(this).next(".tab_content_inner").slideUp(300).removeClass("c_active");
	  	 if($(this).next('.tab_content_inner').css('display') != 'block'){
			$('.c_active').removeClass('c_active').slideUp(500);
			$(this).next('.tab_content_inner').addClass('c_active').slideDown(500);
			$(this).addClass("active");
		} else {
			$('.c_active').removeClass('c_active').slideUp(500);
		}

	  });
	  //payment table
	  $('.payment_tables').DataTable({
        
      });
	});
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout.default', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>