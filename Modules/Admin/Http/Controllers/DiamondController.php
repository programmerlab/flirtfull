<?php

declare(strict_types=1);

namespace Modules\Admin\Http\Controllers;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Redirect;
use Input;
use Modules\Admin\Http\Requests\DiamondRequest;
use Modules\Admin\Models\Diamond;
use Route;
use View;
use Session;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Datatables;


/**
 * Class AdminController
 */
class DiamondController extends Controller
{
    /**
     * @var  Repository
     */

    /**
     * Displays all admin.
     *
     * @return \Illuminate\View\View
     */
    public function __construct()
    {
        $this->middleware('admin');
        View::share('viewPage', 'Plan');
        View::share('helper', new Helper);
        View::share('heading', 'Diamonds');
        View::share('route_url', route('diamond'));

        $this->record_per_page = Config::get('app.record_per_page');
    }

    protected $diamonds;

    /*
     * Dashboard
     * */

    public function index(Diamond $diamond, Request $request)
    {        
        $page_title  = 'Diamonds';
        $page_action = 'Diamonds';
        

        if ($request->ajax()) {
            $id           = $request->get('id');
            $status       = $request->get('status');
            $diamond         = Diamond::find($id);
            $s            = ($status == 1) ? $status=0:$status=1;
            $diamond->status = $s;
            $diamond->save();
            echo $s;

            exit();
        }
        
        $diamonds = Diamond::orderBy('id', 'desc')->where('status', '1')->where('is_deleted', '0')->Paginate($this->record_per_page);
        //print_r($diamonds); die;        

        $js_file = ['common.js','bootbox.js','formValidate.js'];

        return view('admin::diamonds.diamond.index', compact('js_file', 'status', 'diamonds', 'page_title', 'page_action'));
    }


    public function anyData()
    {
        return Datatables::of(Diamond::query())->make(true);
    }

    public function test_table(Diamond $diamond, Request $request)
    {        
        $page_title  = 'Admin Diamond';
        $page_action = 'Admin Diamond';
        

        if ($request->ajax()) {
            $id           = $request->get('id');
            $status       = $request->get('status');
            $diamond         = Diamond::find($id);
            $s            = ($status == 1) ? $status=0:$status=1;
            $diamond->status = $s;
            $diamond->save();
            echo $s;

            exit();
        }
        // Search by name ,email and group
        $search    = Input::get('search');
        $status    = Input::get('status');
        $role_type = Input::get('role_type');

        if ((isset($search) && !empty($search)) or  (isset($status) && !empty($status)) or !empty($role_type)) {
            $search = isset($search) ? Input::get('search') : '';

            $diamonds = Diamond::where(function ($query) use ($search,$status,$role_type) {
                if (!empty($search)) {
                    $query->Where('first_name', 'LIKE', "%$search%")
                        ->OrWhere('last_name', 'LIKE', "%$search%")
                        ->OrWhere('email', 'LIKE', "%$search%");
                }

                if (!empty($status)) {
                    
                    $status =  ($status == 'active')?1:0;
                    $query->Where('status', $status);
                }

                if ($role_type) {
                    $query->Where('role_type', $role_type);
                }
            })->where('role_type', 3)->Paginate($this->record_per_page);
        } else {
            $diamonds = Diamond::orderBy('id', 'desc')->where('role_type', 3)->where('is_deleted', 0)->Paginate($this->record_per_page);
        }
        $roles = Roles::all();

        $js_file = ['common.js','bootbox.js','formValidate.js'];

        return view('admin::diamonds.diamond.test_table', compact('js_file', 'roles', 'status', 'page_title', 'page_action', 'roles' ,'role_type'));
    }

    /*
     * create Group method
     * */

    public function create(Diamond $diamond)
    {
        $page_title  = 'Diamond';
        $page_action = 'Create Diamond';
        
        $js_file     = ['common.js','bootbox.js','formValidate.js'];


        return view('admin::diamonds.diamond.create', compact('js_file', 'diamond', 'page_title', 'page_action'));
    }

    /*
     * Save Group method
     * */

    public function store(DiamondRequest $request, Diamond $diamond)
    {
        //echo "<pre>"; print_r(Input::all()); die;
        $diamond->fill(Input::all());
        
        
        $action = $request->get('submit');        
        
        
        $diamond->save();
        
        $js_file = ['common.js','bootbox.js','formValidate.js'];

        return Redirect::to(route('diamond'))
            ->with('flash_alert_notice', 'New record successfully created.');
    }

    /*
     * Edit Group method
     * @param
     * object : $diamond
     * */

    public function edit(Diamond $diamond)
    {
        //echo "<pre>"; print_r($diamond); die;
        $page_title  = 'Diamond';
        $page_action = 'Show Diamond';                
        
        $js_file     = ['common.js','bootbox.js','formValidate.js'];
                
        //echo "<pre>"; print_r($diamond); die;
        return view('admin::diamonds.diamond.edit', compact('js_file', 'diamond', 'page_title', 'page_action'));
    }

    public function update(Request $request, Diamond $diamond, DiamondRequest $diamondrequest)
    {        
        //echo "<pre>"; print_r($diamond->password); die;        
        $diamond->fill(Input::all());
        $action          = $request->get('submit');
        $diamond->save();
                
        if ($request->get('role') == 3) {
            $Redirect = 'clientdiamond';
        } else {
            $Redirect = 'diamond';
        }

        return Redirect::to(route($Redirect))
            ->with('flash_alert_notice', 'Record successfully updated.');
    }
    /*
     *Delete Diamond
     * @param ID
     *
     */
    public function destroy(Request $request, $diamond)
    { //print_r($diamond);
        //echo $diamond->id;die;
        $diamond->is_deleted = 1;
        $diamond->save();
        //$diamond->delete();
        return Redirect::to(route('diamond'))
            ->with('flash_alert_notice', 'Record  successfully deleted.');
    }

    public function show(Diamond $diamond)
    {
    }
    
    public function image_delete(Diamond $diamond, Request $request, Gallery_image $gallery_image)
    {
        //echo $request->ajax('id');        
            $id           = $request->get('id');            
            $status       = $request->get('is_deleted');
            $diamond         = Gallery_image::find($id);
            $s            = ($status == 1) ? $status=0:$status=1;
            $diamond->is_deleted = $s;
            $diamond->save();
            echo $s;

            exit();        
    }
    
    public function delete_image(Diamond $diamond, Request $request, Gallery_image $gallery_image)
    {
        //echo $request->ajax('id');        
            $id           = $request->get('id');
            $diamondid           = $request->get('diamondid');
            DB::table('gallery_images')->where('id', '=', $id)->delete();
            return Redirect::to(url('admin/diamond/'.$diamondid.'/edit'))
                ->with('flash_alert_notice', 'Image successfully deleted.');
            exit();        
    }
    
    public function search1(Diamond $diamond, Request $request)
    {
        //echo $request->ajax('id');        
            $country      = $request->get('country');
            $age      = $request->get('age');
            $gender      = $request->get('gender');
            $status      = $request->get('status');
            
            
            $diamonds = Diamond::where(function ($query) use ($country,$age,$gender,$status) {
                if (!empty($country)) {
                    $query->Where('country', 'LIKE', "%$country%");                        
                }

                if (!empty($age)) {
                    $age = explode('_',$age);
                    $query->Where('age', '>=', $age[0]);
                    $query->Where('age', '<=', $age[1]);
                }

                if ($gender) {
                    $query->Where('gender', $gender);
                }
                
                if (!empty($status)) {
                    $status = ($status == 'yes') ? '1' : '0';
                    $query->Where('status', $status);
                }
            })->where('role_type', 3)->Paginate(18);
            
            //echo "<pre>"; print_r($diamonds); die;            
            $roles = Roles::all();

        $js_file = [];

        return view('admin::diamonds.diamond.index', compact('js_file', 'roles', 'country', 'diamonds', 'page_title', 'page_action', 'roles', 'role_type')); 
    }
    
    public function search(Diamond $diamond, Request $request)
    {      $last_part = $request->get('last_part'); 
        //echo $request->ajax('id');        
            $country      = $request->get('country');
            $age      = $request->get('age');
            $gender      = $request->get('gender');
            $status      = $request->get('status');
            $relationship      = $request->get('relationship');
            $photo      = $request->get('photo');
            $appearance      = $request->get('appearance');
            $profile_type      = $request->get('profile_type');
            $adult_level      = $request->get('adult_level');
            
            //DB::enableQueryLog();
            
            $reslt = DB::table('diamonds')
                    ->join('diamond_details', 'diamonds.id', '=', 'diamond_details.diamond_id')            
                    ->select('diamonds.*', 'diamond_details.relationship');
            
            if (!empty($country)) {
                $reslt->Where('diamonds.country', 'LIKE', "%$country%");                        
            }
                
            if (!empty($age)) {
                $age = explode('_',$age);
                $reslt->Where('diamonds.age', '>=', $age[0]);
                $reslt->Where('diamonds.age', '<=', $age[1]);
            }
            
            if ($gender) {
                $reslt->Where('diamonds.gender', $gender);
            }
            
            if ($profile_type) {
                $reslt->Where('diamonds.profile_type', $profile_type);
            }
            
            if ($adult_level) {
                $reslt->Where('diamonds.adult_level', $adult_level);
            }
            
            if ($appearance) {
                $reslt->Where('diamonds.appearance', $appearance);
            }
            
            if (!empty($status)) {
                $status = ($status == 'yes') ? '1' : '0';
                $reslt->Where('diamonds.status', $status);
            }
            
            if (isset($photo)) {
                if($photo == '1'){
                    $reslt->Where('diamonds.profile_image', '!=',  '');
                }
                if($photo == '0'){
                    $reslt->Where('diamonds.profile_image','');
                    //$reslt->or_Where('diamonds.profile_image', '=',  'default.png');
                }
            }
            
            if ($relationship) {
                $reslt->Where('diamond_details.relationship', $relationship);
            }

            if($last_part == 'deleted') {
                $page_title  = 'Player';
                $page_action = 'Deleted Players';
                $heading = 'Deleted Players';
                $reslt->Where('diamonds.is_deleted', '=',  '1');
            }
            else if($last_part == 'real') {
                $page_title  = 'Player';
                $page_action = 'Real Players';
                $heading = 'Real Players';
                $reslt->Where('diamonds.fake', '=',  '0');
                $reslt->Where('diamonds.is_deleted', '=',  '0');
            }
            else if($last_part == 'fake') {
                $page_title  = 'Player';
                $page_action = 'Fake Players';
                $heading = 'Fake Players';
                $reslt->Where('diamonds.fake', '=',  '1');
                $reslt->Where('diamonds.is_deleted', '=',  '0');
            }
            else if($last_part == 'unconfirmed') {
                $page_title  = 'Player';
                $page_action = 'Unconfirmed Players';
                $heading = 'Unconfirmed Players';
                $reslt->Where('diamonds.status', '=',  '0');
                $reslt->Where('diamonds.is_deleted', '=',  '0');
            }
                
            $diamonds = $reslt->where('diamonds.role_type', 3)->Paginate(30);
            
            //echo "<pre>"; print_r($diamonds); die;            
            $roles = Roles::all();

        $js_file = [];

        $search_slug = 'list-search';

        return view('admin::diamonds.diamond.index', compact('js_file', 'roles', 'country', 'diamonds', 'page_title', 'page_action','heading',  'roles', 'role_type', 'search_slug')); 
    }
    
    public function setProfileImage(Diamond $diamond, Request $request, Gallery_image $gallery_image)
    {        
            $id           = $request->get('id');
            $diamondid           = $request->get('diamondid');
            
            $diamond = DB::table('diamonds')->where('id', $diamondid)->first();            
            $old_image = $diamond->profile_image;
            
            $gallery = DB::table('gallery_images')->where('id', $id)->first();            
            $new_image = $gallery->gallery_image;
            
            DB::table('diamonds')
            ->where('id', $diamondid)
            ->update(array('profile_image' => $new_image));
            
            DB::table('gallery_images')
            ->where('id', $id)
            ->update(array('gallery_image' => $old_image));
                        
            return Redirect::to(url('admin/diamond/'.$diamondid.'/edit'))
                ->with('flash_alert_notice', 'Image Set as Profile successfully ');
            exit();        
    }


    public function deleted(Diamond $diamond, Request $request)
    {        
        $page_title  = 'Player';
        $page_action = 'Deleted Players';
        $heading = 'Deleted Players';
        

        if ($request->ajax()) {
            $id           = $request->get('id');
            $status       = $request->get('status');
            $diamond         = Diamond::find($id);
            $s            = ($status == 1) ? $status=0:$status=1;
            $diamond->status = $s;
            $diamond->save();
            echo $s;

            exit();
        }
        // Search by name ,email and group
        $search    = Input::get('search');
        $status    = Input::get('status');
        $role_type = Input::get('role_type');

        if ((isset($search) && !empty($search)) or  (isset($status) && !empty($status)) or !empty($role_type)) {
            $search = isset($search) ? Input::get('search') : '';

            $diamonds = Diamond::where(function ($query) use ($search,$status,$role_type) {
                if (!empty($search)) {
                    $query->Where('first_name', 'LIKE', "%$search%")
                        ->OrWhere('last_name', 'LIKE', "%$search%")
                        ->OrWhere('email', 'LIKE', "%$search%");
                }

                if (!empty($status)) {
                    
                    $status =  ($status == 'active')?1:0;
                    $query->Where('status', $status);
                }

                if ($role_type) {
                    $query->Where('role_type', $role_type);
                }
            })->where('role_type', 3)->Paginate($this->record_per_page);
        } else {
            $diamonds = Diamond::orderBy('id', 'desc')->where('role_type', 3)->where('is_deleted', 1)->Paginate($this->record_per_page);
        }
        $roles = Roles::all();

        $js_file = ['common.js','bootbox.js','formValidate.js'];

        return view('admin::diamonds.diamond.index', compact('js_file', 'roles', 'status', 'diamonds', 'page_title', 'page_action','heading', 'roles', 'role_type'));
    }

    public function fake(Diamond $diamond, Request $request)
    {        
        $page_title  = 'Player';
        $page_action = 'Deleted Players';
        $heading = 'Fake Players';
        

        if ($request->ajax()) {
            $id           = $request->get('id');
            $status       = $request->get('status');
            $diamond         = Diamond::find($id);
            $s            = ($status == 1) ? $status=0:$status=1;
            $diamond->status = $s;
            $diamond->save();
            echo $s;

            exit();
        }
        // Search by name ,email and group
        $search    = Input::get('search');
        $status    = Input::get('status');
        $role_type = Input::get('role_type');

        if ((isset($search) && !empty($search)) or  (isset($status) && !empty($status)) or !empty($role_type)) {
            $search = isset($search) ? Input::get('search') : '';

            $diamonds = Diamond::where(function ($query) use ($search,$status,$role_type) {
                if (!empty($search)) {
                    $query->Where('first_name', 'LIKE', "%$search%")
                        ->OrWhere('last_name', 'LIKE', "%$search%")
                        ->OrWhere('email', 'LIKE', "%$search%");
                }

                if (!empty($status)) {
                    
                    $status =  ($status == 'active')?1:0;
                    $query->Where('status', $status);
                }

                if ($role_type) {
                    $query->Where('role_type', $role_type);
                }
            })->where('role_type', 3)->Paginate($this->record_per_page);
        } else {
            $diamonds = Diamond::orderBy('id', 'desc')->where('role_type', 3)->where('fake', 1)->where('is_deleted', 0)->Paginate($this->record_per_page);
        }
        $roles = Roles::all();

        $js_file = ['common.js','bootbox.js','formValidate.js'];

        return view('admin::diamonds.diamond.index', compact('js_file', 'roles', 'status', 'diamonds', 'page_title', 'page_action','heading', 'roles', 'role_type'));
    }

    public function real(Diamond $diamond, Request $request)
    {        
        $page_title  = 'Player';
        $page_action = 'Real Players';
        $heading = 'Real Players';
        

        if ($request->ajax()) {
            $id           = $request->get('id');
            $status       = $request->get('status');
            $diamond         = Diamond::find($id);
            $s            = ($status == 1) ? $status=0:$status=1;
            $diamond->status = $s;
            $diamond->save();
            echo $s;

            exit();
        }
        // Search by name ,email and group
        $search    = Input::get('search');
        $status    = Input::get('status');
        $role_type = Input::get('role_type');

        if ((isset($search) && !empty($search)) or  (isset($status) && !empty($status)) or !empty($role_type)) {
            $search = isset($search) ? Input::get('search') : '';

            $diamonds = Diamond::where(function ($query) use ($search,$status,$role_type) {
                if (!empty($search)) {
                    $query->Where('first_name', 'LIKE', "%$search%")
                        ->OrWhere('last_name', 'LIKE', "%$search%")
                        ->OrWhere('email', 'LIKE', "%$search%");
                }

                if (!empty($status)) {
                    
                    $status =  ($status == 'active')?1:0;
                    $query->Where('status', $status);
                }

                if ($role_type) {
                    $query->Where('role_type', $role_type);
                }
            })->where('role_type', 3)->Paginate($this->record_per_page);
        } else {
            $diamonds = Diamond::orderBy('id', 'desc')->where('role_type', 3)->where('fake', 0)->where('is_deleted', 0)->Paginate($this->record_per_page);
        }
        $roles = Roles::all();

        $js_file = ['common.js','bootbox.js','formValidate.js'];

        return view('admin::diamonds.diamond.index', compact('js_file', 'roles', 'status', 'diamonds', 'page_title', 'page_action','heading', 'roles', 'role_type'));
    }

    public function unconfirmed(Diamond $diamond, Request $request)
    {        
        $page_title  = 'Player';
        $page_action = 'Unconfirmed Players';
        $heading = 'Unconfirmed Players';
        

        if ($request->ajax()) {
            $id           = $request->get('id');
            $status       = $request->get('status');
            $diamond         = Diamond::find($id);
            $s            = ($status == 1) ? $status=0:$status=1;
            $diamond->status = $s;
            $diamond->save();
            echo $s;

            exit();
        }
        // Search by name ,email and group
        $search    = Input::get('search');
        $status    = Input::get('status');
        $role_type = Input::get('role_type');

        if ((isset($search) && !empty($search)) or  (isset($status) && !empty($status)) or !empty($role_type)) {
            $search = isset($search) ? Input::get('search') : '';

            $diamonds = Diamond::where(function ($query) use ($search,$status,$role_type) {
                if (!empty($search)) {
                    $query->Where('first_name', 'LIKE', "%$search%")
                        ->OrWhere('last_name', 'LIKE', "%$search%")
                        ->OrWhere('email', 'LIKE', "%$search%");
                }

                if (!empty($status)) {
                    
                    $status =  ($status == 'active')?1:0;
                    $query->Where('status', $status);
                }

                if ($role_type) {
                    $query->Where('role_type', $role_type);
                }
            })->where('role_type', 3)->Paginate($this->record_per_page);
        } else {
            $diamonds = Diamond::orderBy('id', 'desc')->where('role_type', 3)->where('status', 0)->where('is_deleted', 0)->Paginate($this->record_per_page);
        }
        $roles = Roles::all();

        $js_file = ['common.js','bootbox.js','formValidate.js'];

        return view('admin::diamonds.diamond.index', compact('js_file', 'roles', 'status', 'diamonds', 'page_title', 'page_action','heading', 'roles', 'role_type'));
    }
    
    
}
