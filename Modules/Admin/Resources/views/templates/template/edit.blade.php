@extends('admin::layouts.master')
 
    @section('content') 
      @include('admin::partials.navigation')
      @include('admin::partials.breadcrumb')   

       @include('admin::partials.sidebar')  
                        <div class="panel panel-white"> 

 
                                <div class="panel panel-flat">
                                              <div class="panel-heading">
                                            <h6 class="panel-title"><b> {{$heading ?? ''}}</b><a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
                                            <div class="heading-elements">
                                              <ul class="icons-list">
                                                <li> <a type="button" href="{{route('template')}}" class="btn btn-primary text-white   btn-rounded "> View Templates<span class="legitRipple-ripple" ></span></a></li> 
                                              </ul>
                                            </div>
                                          </div> 
                                    </div>
                                    <div class="panel-body">
                                    {!! Form::model($template, ['method' => 'PATCH', 'route' => ['template.update', $template->id],'enctype'=>'multipart/form-data','class'=>
                                                  'form-basic ui-formwizard template-form']) !!}
                                                  
                                           <div class="form-group {{ $errors->first('template_name', ' has-error') }}">
                                                <label class="control-label col-md-3">Template Name <span class="required"> * </span></label>
                                                <div class="col-md-12"> 
                                                    {!! Form::text('template_name',null, ['class' => 'form-control','data-required'=>1])  !!} 
                                                    
                                                    <span class="help-block">{{ $errors->first('template_name', ':message') }}</span>
                                                </div>
                                            </div>
                                            
                                            <div class="form-group {{ $errors->first('email_subject', ' has-error') }}">
                                                <label class="control-label col-md-3">Email Subject<span class="required"> * </span></label>
                                                <div class="col-md-12"> 
                                                    {!! Form::text('email_subject',null, ['class' => 'form-control','data-required'=>1])  !!} 
                                                    
                                                    <span class="help-block">{{ $errors->first('email_subject', ':message') }}</span>
                                                </div>
                                            </div>
                                                
                                                
                                            <div class="col-md-12">
                                                <div class="form-group {{ $errors->first('email_message', ' has-error') }}">
                                                    <label>Email Message: <span class="text-danger">*</span></label> 
                                                     {!! Form::textarea('email_message',null, ['class' => 'form-control required summernote' ,'data-required'=>1])  !!} 
                                                      <span class="help-block">{{ $errors->first('email_message', ':message') }}</span>
                                                </div>

                                                <label>Available Variables: </label><p> [site_url] [verification_link] [user_name]</p>
                                                <p style="color:red">Note : Please do not change these variables.</p>
                                            </div>
                              
                                           <div class="col-md-12">
                                                  <div class="margin-top-10"> 
                                                       <button type="submit" class="btn green btn-primary" value="avtar" name="submit"> Save Changes </button>
                                                      <button type="reset" class="btn default "> Cancel </button>
                                                  </div> 
                                                 
                                           </div>
                                    </form>        
                                    </div>                                                                                          
                        </div>
                              
                           
                     
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
            
            
            <!-- END QUICK SIDEBAR -->
        </div>
        

        
@stop