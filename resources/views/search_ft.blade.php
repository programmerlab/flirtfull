@extends('layout.default')
@section('content')

<section class="search_list_outer">
  <div class="container-fluid">
    <div class="row">
            <div class="col-md-9 col-sm-8 pdng main-content search_content_left">
            <!--search bar inline-->
            <div class="button_wrapp">
                <button class="search-bar-btn"><i class="fa fa-search"></i> Find Your Matches</button>
            </div>
            <div class="profile_search_bar">
            		<form method="get" action="{{ url('search') }}">
            			<!--<div class="form_group">
            				<label class="label">Name</label>
            				<div class="form_input">            					
                                <input type="text" id="player" placeholder="Profile Name" id="profile_name" name="profile_name">              
            				</div>
            			</div>-->
                        <?php
                        if(isset($users)){
                            if($users->seeking_val == 1) { $v1 = 1; $op1 = 'Men'; $v2 = 2; $op2 = 'Women'; }else{ $v2 = 1; $op2 = 'Men'; $v1 = 2; $op1 = 'Women'; }        
                        }else{
                            $v1 = 1; $op1 = 'Men'; $v2 = 2; $op2 = 'Women';
                        }        
                        ?>    
            			<div class="form_group">
            				<label class="label">find Me</label>
            				<div class="form_input">
            					<select name="seeking">
            						<option value="<?=$v1?>" {{ ($_GET['seeking'] && $_GET['seeking']==$v1)?'selected':'' }}><?=$op1?></option>
                                    <option value="<?=$v2?>" {{ ($_GET['seeking'] && $_GET['seeking']==$v2)?'selected':'' }}><?=$op2?></option>
                                    <option value="" {{ ($_GET['seeking']=='')?'selected':'' }}>All</option>
            					</select>
            				</div>
            			</div>
            			<div class="form_group age_group">
            				<label class="label">Age</label>
            				<div class="form_input">
            					<select name="min_age">
		              			  <option value="18">all</option>
		              			  <option value="18">18</option>
			                      <option value="20" {{ ($_GET['min_age'] && $_GET['min_age']==20)?'selected':'' }}>20</option>
			                      <option value="25" {{ ($_GET['min_age'] && $_GET['min_age']==25)?'selected':'' }}>25</option>
			                      <option value="30" {{ ($_GET['min_age'] && $_GET['min_age']==30)?'selected':'' }}>30</option>
			                      <option value="35" {{ ($_GET['min_age'] && $_GET['min_age']==35)?'selected':'' }}>35</option>
			                      <option value="40" {{ ($_GET['min_age'] && $_GET['min_age']==40)?'selected':'' }}>40</option>
			                      <option value="45" {{ ($_GET['min_age'] && $_GET['min_age']==45)?'selected':'' }}>45</option>
			                      <option value="50" {{ ($_GET['min_age'] && $_GET['min_age']==50)?'selected':'' }}>50</option>
			                      <option value="55" {{ ($_GET['min_age'] && $_GET['min_age']==55)?'selected':'' }}>55</option>
			                      <option value="60" {{ ($_GET['min_age'] && $_GET['min_age']==60)?'selected':'' }}>60</option>
			                      <option value="65" {{ ($_GET['min_age'] && $_GET['min_age']==65)?'selected':'' }}>65</option>
			                      <option value="70" {{ ($_GET['min_age'] && $_GET['min_age']==70)?'selected':'' }}>70</option>
			                      <option value="75" {{ ($_GET['min_age'] && $_GET['min_age']==75)?'selected':'' }}>75</option>
			                    </select>
			                    <select name="max_age">
			                      <option value="99">all</option>
			                      <option value="20" {{ ($_GET['max_age'] && $_GET['max_age']==20)?'selected':'' }}>20</option>
			                      <option value="25" {{ ($_GET['max_age'] && $_GET['max_age']==25)?'selected':'' }}>25</option>
			                      <option value="30" {{ ($_GET['max_age'] && $_GET['max_age']==30)?'selected':'' }}>30</option>
			                      <option value="35" {{ ($_GET['max_age'] && $_GET['max_age']==35)?'selected':'' }}>35</option>
			                      <option value="40" {{ ($_GET['max_age'] && $_GET['max_age']==40)?'selected':'' }}>40</option>
			                      <option value="45" {{ ($_GET['max_age'] && $_GET['max_age']==45)?'selected':'' }}>45</option>
			                      <option value="50" {{ ($_GET['max_age'] && $_GET['max_age']==50)?'selected':'' }}>50</option>
			                      <option value="55" {{ ($_GET['max_age'] && $_GET['max_age']==55)?'selected':'' }}>55</option>
			                      <option value="60" {{ ($_GET['max_age'] && $_GET['max_age']==60)?'selected':'' }}>60</option>
			                      <option value="65" {{ ($_GET['max_age'] && $_GET['max_age']==65)?'selected':'' }}>65</option>
			                      <option value="70" {{ ($_GET['max_age'] && $_GET['max_age']==70)?'selected':'' }}>70</option>
			                      <option value="75" {{ ($_GET['max_age'] && $_GET['max_age']==75)?'selected':'' }}>75</option>
			                      <option value="80" {{ ($_GET['max_age'] && $_GET['max_age']==80)?'selected':'' }}>80+</option>
			                    </select>
            				</div>
            			</div>
            			<div class="form_group region_group">
            				<label class="label">In</label>
            				<div class="form_input">
            					<select name="state" id="prof_region">
                                 <option selected="" value="">Region</option>             
                               </select>        
            				</div>
            			</div>
            			<div class="form_group online_group">
            				<div class="form_input check_box">
            					<label>
            						<input type="checkbox" name="online" value="1" {{ ($_GET['online'] && $_GET['online']==1)?'checked':'' }}>
            						<span class="c_box"></span>
            						<span class="c_text">Online Now</span>
            					</label>
            				</div>
            			</div>
            			<div class="form_group button_group">
            				<div class="form_input">
                              <button type="submit" value="Search" class="pro_search_btn">Search</button>
                            </div>
                        </div>
            		</form>
                </div>
            <!-- <div class="user_profile_popup all-users" id="profile_popup"> -->
              <div class="all-users" id="profile_popup">
                       @include('partial/all_users')
                
            </div>
            <textarea id="chat_file_hidden" style="display:none"></textarea>     
           
            <!--  chat popup  --> 
               <div id="fake_chat" class="profile_fake_chat">
               </div>
            <!--  chat popup  -->
            
                
          </div>
        <div class="clearfix"></div>
        @include('chat_sidebar')
        </div>
    </div>
</section>
<!-- notice popup -->
  <div class="notice_popup">
    <div class="notice_dialog">
      <div class="notice_popup_content">
        <div class="popup_header">
          <h1>IMPORTANT NOTICE:</h1>
        </div>
        <div class="center_content">
          <h4>You state that the following facts are accurate</h4>
          <ul>
            <li>I am at least 18-years old.</li> 
            <li>I will not redistribute any material from the website.</li>
            <li>I will not allow any minors to access the website or any material found in it</li>
            <li>Any material I view or download from the website is for my own personal use and I will not show it to a minor.</li>
            <li>
              Sexually explicit material depicting kinky and other fetish activities is allowed by the local law governing my jurisdiction.
            </li>
            <li>
              I was not contacted by the suppliers of this material, and I willingly choose to view or download it
            </li>
            <li>
                I am entering this website because it has adult content, and pictures depicting men or women in various sexual situations is not obscene or offensive in any way. In addition, I do not believe that this material could be considered obscene or offensive.
            </li>
             <li>
                I acknowledge that the website includes fictitious angel profiles created and operated by the website that may communicate with me for entertainment and other purposes. I also understand that physical encounters with this angel profiles are not possible. 
            </li>
             <li>
                I acknowledge that individuals appearing in photos on the landing page or in angel profiles might not be actual members of the website and that certain data is provided for illustration and entertainment purposes only.
            </li>
             <li>
                I acknowledge that the website does not inquire into the background of its members and the website does not otherwise attempt to verify the accuracy of statements made by its members.
            </li>
              <li>
                I acknowledge that the website does not guarantee that I will find a date or that I will meet any of its members in person or that any given person or profile manifested on the website is available or interested in dating or communicating with me or anyone else.
            </li> 
            <li>
                I acknowledge that my use of the website is governed by the website’s <a target="_blank" href="{{ url('terms-conditions') }}">Terms and condition</a> and the website’s <a target="_blank" href="{{ url('privacy-policy') }}">Privacy Policy</a>, which I have carefully reviewed and accepted, and I am legally bound by the <a target="_blank" href="{{ url('terms-conditions') }}">Terms and condition</a> and the <a target="_blank" href="{{ url('privacy-policy') }}">Privacy Policy.</a>
            </li>
            <li>
                By entering the website, I am subjecting myself to the exclusive personal jurisdiction of the Netherlands should any dispute arise between the owner or operator of the website and myself in accordance with the <a href="{{ url('terms-conditions') }}">Terms and condition</a>.
            </li>
            <li>
                By entering the website, I am subjecting myself to binding arbitration in the Netherlands should any dispute arise at any time between the owner or operator of the website and myself in accordance with the <a href="{{ url('terms-conditions') }}">Terms and condition</a>.
            </li>
             <li>
                By entering the website, I will have released and discharged the providers, owners, and creators of the website from all liability that might arise.
            </li>
            <li>
              By entering the website, my personal data will be stored and processed, in accordance with the <a target="_blank" href="{{ url('privacy-policy') }}">Privacy Policy.</a>
            </li>
          </ul>
        </div>
        <div class="popup_footer_content">
          <p>
            By clicking Agree and continue, you state that all the above is true, that you want to enter the Website, and that you agree to the <a target="_blank" href="{{ url('terms-conditions') }}">Terms and conditions</a> &amp; <a target="_blank" href="{{ url('privacy-policy') }}">Privacy Policy</a>
          </p>
          <div class="popup_confirm_buttons">
            <button class="leave_btn buttons">Disagree and leave</button>
            <button class="confirm_btn buttons">Agree and continue</button>
          </div>
        </div>
      </div>
    </div>
  </div>
<!-- notice popup -->

<!-- confirm popup -->
<div class="n_conmfirm_popup" id="n_confrim_popup">
  <div class="modal_dialog_big">
    <div class="confirm_p_content">
      <div class="close_popup"><i class="fa fa-times"></i></div>  
      <div class="confirm_content_wrap">
        <div class="cnfrm_right_img">
          <div class="cnfrm_img">
            <img src="public/images/confrim_img.png" alt="" class="img-responsive" />
          </div>
        </div>
        <div class="cnfrm_left_content">
          <div class="cnfrm_inner_cover">
            <h1 class="cong_heading">Congratulations!</h1>
            <h3 class="success_heading">You've successfully created your profile on <span>Flirtfull</span></h3>
            <div class="email_text">Open the email from us and click on the <span>" Activate my account "</span> button. </div>
            <div class="email_text_2">Your email is <span>{{ Auth::user()->email }}</span></div>
            <div class="email_button">
              <a href="#">Go to your e-mail</a>
            </div>
            <div class="resend_mail_link" id="resend_email1">
              <a href="#" onclick="resend_verification('{{ Auth::id() }}')">Resend activation e-mail</a>
            </div>
        
            <div class="bottom_p_notes">
              <h4>Important:</h4>
              <ul>
                <li>it can take up to 5 minutes for the activation email to arrive</li>
                <li>Check your email account's 'Spam/Junk' folder to make sure our email hasn't landed there</li>
                <li>Mark us as 'Not Spam/Junk' to ensure that you receive all important e-mail from <span>Flirtfull</span></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
  function updateemail(){    
    $.ajax(
      {
        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
        url: '<?php echo url("/")?>/update-profile',
        data: { email: $('#updateemail').val() },
        type: "POST",       
        success: function( data ) 
        {
          console.log(data);
          $('.loader').hide();
          if(data == 1)
          {
            $('.successemail').show();            
            $('.erroremail').hide();
            //location.reload();
          }else{
            $('.successemail').hide();
            $('.erroremail').show();
          }
        },
        error: function( xhr, status, errorThrown ) 
        {          
          alert( "Sorry, there was a problem!" );
        }
      });
  }
</script>
<!-- confirm popup -->
<script>    
    <?php if((Auth::check()) && Auth::user()->status == 0 ) { ?>
        setTimeout(function()
			{
				$(".n_conmfirm_popup").addClass("open_popup");
			}, 2000);
        setInterval(function()
			{
				$(".n_conmfirm_popup").addClass("open_popup");
			}, 120000);
                    
      
    <?php   } ?>
</script>
<script>
    //confirm popup js
    $(".close_popup").on('click', function(){
    $(this).parents(".n_conmfirm_popup").removeClass("open_popup");
    });
    
    $(".n_conmfirm_popup").on('click', function(){
    $(this).removeClass("open_popup");
    });
    $(".confirm_p_content").on('click', function(event){
    event.stopPropagation();
    });
</script>    


<!-- Region modal Start -->
      <div id="region_popup" class="splash_screen_modal outer_close_modal">
         <div class="popup_modal_dialog">
            <div class="splash_content_bg">
                <!-- close button -->
              <span class="splash_close hide_model">
                <i class="fa fa-times"></i>
              </span>
               <!-- close button -->
               <div class="s_overlay"></div>
               <div class="splash_content">
                  <div class="splash_form">
                     <div class="s_form_heading">
                       <h2>hurry up select your Region and 
                        <span>get 1 Diamond Free</span>
                       </h2>
                     </div>
                     <form class="" id="region_form">
                        <div class="form_group">
                           <label class="label">Select your region</label>
                           <div class="input_group">
								<select name="prof_region" id="prof_region2" class="form-control">
									<option selected="" value="">Region</option>
								</select>
							  <p class="error-message region"></p>
                           </div>
                        </div>
						 <p class="region-process text-center">Processing...</p>
                         <p class="text-center" id="succmess" style="display:none; color:red;">Congrats! You won 1 diamond free.</p>   
                         <div class="button_group">
                           <input type="submit" name="submit" value="Submit" class="form_btn">
                        </div>
                     </form>
                  </div>
               </div>
            </div>
         </div>
      </div>
<!--Region modal End -->

<!-- profile modal Start -->
    <div id="name_popup" class="splash_screen_modal outer_close_modal">
       <div class="popup_modal_dialog">
          <div class="splash_content_bg">
            <!-- close button -->
            <span class="splash_close hide_model">
              <i class="fa fa-times"></i>
            </span>
             <!-- close button -->
             <div class="s_overlay"></div>
             <div class="splash_content">
                <div class="splash_form">
                  <div class="s_form_heading">
                   <h2>Hey Enter Your Profile Name 
                    <span>you Will get 1 Diamond free</span>
                   </h2>
                   </div>
                   <form class="" id="profilename_form">
                      <div class="form_group">
                         <label class="label">Your Profile name</label>
                         <div class="input_group">
                           <input type="text" id="profilename" name="profilename" placeholder="Enter Your Name">
                           <p class="error-message profilename"></p>
                         </div>
                      </div>
                       <p class="profilename-process text-center">Processing...</p>
                         <p class="text-center" id="succmess1" style="display:none; color:red;">Congrats! You won 1 diamond free.</p>    
                       <div class="button_group">
                         <input type="submit" name="submit" value="Submit" class="form_btn">
                      </div>
                   </form>
                </div>
             </div>
          </div>
       </div>
    </div>
<!--profile modal End -->




<!-- namepassword modal Start -->
      <div id="namepassword_popup" class="splash_screen_modal outer_close_modal">
         <div class="popup_modal_dialog">
            <div class="splash_content_bg">
                <!-- close button -->
              <span class="splash_close hide_model">
                <i class="fa fa-times"></i>
              </span>
               <!-- close button -->
               <div class="s_overlay"></div>
               <div class="splash_content">
                  <div class="splash_form">
                     <div class="s_form_heading">
                       <h2>hurry up submit this form and 
                        <span>get 1 Diamond Free</span>
                       </h2>
                     </div>
                     <form class="" id="namepassword_form">
                        <div class="form_group">
                           <label class="label">Enter your name</label>
                           <div class="input_group">
								<input type="text" name="name" id="ooname" placeholder="Enter Your Profile Name">
                                <span id="ooname_error" style="display:none; color:red;">This field is required</span>
                                <span id="oouniquename_error" style="display:none; color:red;">This name is already exists, please choose different one.</span>  
                           </div>
                        </div>
                        <div class="form_group">
                           <label class="label">Enter your Password</label>
                           <div class="input_group">
								<input type="password" name="password" id="oopass" placeholder="Enter Your Password">
                                <span id="oopass_error" style="display:none; color:red;">This field is required</span>
                                <span id="oominpass_error" style="display:none; color:red;">This field should be minimum of 4 characters</span>  
                                <span id="ooconfpass_error" style="display:none; color:red;">Confirm Password doesn't match</span>                                  
                           </div>
                        </div>
                        <!--<div class="form_group">
                           <label class="label">Confirm your password</label>
                           <div class="input_group">
								<input type="password" id="oorepass" placeholder="confirm Password">
                                <span id="oorepass_error" style="display:none; color:red;">This field is required</span>
                           </div>
                        </div> -->   
						 <p class="region-process text-center">Processing...</p>
                         <p class="text-center" id="succmess12" style="display:none; color:red;">Congrats! You won 1 diamond free.</p>   
                         <div class="button_group">
                           <input type="submit" name="submit" value="Submit" class="form_btn">
                        </div>
                     </form>
                  </div>
               </div>
            </div>
         </div>
      </div>
<!--Region modal End -->

<!-- Region modal Start -->
      <div id="ageregion_popup" class="splash_screen_modal outer_close_modal">
         <div class="popup_modal_dialog">
            <div class="splash_content_bg">
                <!-- close button -->
              <span class="splash_close hide_model">
                <i class="fa fa-times"></i>
              </span>
               <!-- close button -->
               <div class="s_overlay"></div>
               <div class="splash_content">
                  <div class="splash_form">
                     <div class="s_form_heading">
                       <h2>hurry up select Region and Birthyear
                        <span>get 1 Diamond Free</span>
                       </h2>
                     </div>
                     <form class="" id="ageregion_form">
                         <div class="form_group">
                           <label class="label">Select your year of birth</label>
                           <div class="input_group">
								<select id="ooyear" name="year" class="" >                                    
                                    <?php 
                                    for($i=2001;$i>=1930;$i--)
                                    {
                                      echo '<option value="'.$i.'">'.$i.'</option>';
                                    }
                                    ?>
                                  </select>
                                  <span id="ooyear_error" style="display:none; color:red;">This field is required</span>
                           </div>
                        </div>
                        <div class="form_group">
                           <label class="label">Select your region</label>
                           <div class="input_group">
								<select name="prof_region" id="prof_region3" class="form-control">
									<option selected="" value="">Region</option>
								</select>
							  <span id="ooregion_error" style="display:none; color:red;">This field is required</span>
                           </div>
                        </div>
						 <p class="region-process text-center">Processing...</p>
                         <p class="text-center" id="succmess11" style="display:none; color:red;">Congrats! You won 1 diamond free.</p>   
                         <div class="button_group">
                           <input type="submit" name="submit" value="Submit" class="form_btn">
                        </div>
                     </form>
                  </div>
               </div>
            </div>
         </div>
      </div>
<!--Region modal End -->

<!-- chat notification -->
  <div class="chat_notifi_box_wrap">
    <span class="close_box">
      <i class="fa fa-times"></i>
    </span>
    <div class="chat_notifi_box">
      <div class="img_wrapper">
        <img src="https://moneoangels.com/public/images/profile_image/crop_1556187992.png" class="noti_image" alt="profile_img">
      </div>
      <div class="ct_info_wrapper">
        <div class="title_head">
          <h4 id="noti_name">MarinaBeauty, <span class="age">27</span></h4>
          <span class="active_status online">
            <i class="fa fa-circle"></i>
          </span>
        </div>
        <p>is online ! You may get lucky with her! </p>
        <a href="#" class="noti_chat_btn"><i class="fa fa-commenting-o"></i>Chat</a>
      </div>
    </div>
  </div>
  <!-- chat notification -->

<script>
<?php if(isset($users)  && $users->name == '' && $users->campaign_id == '2' && Session::has('namepasswordstatus') && (Session::get('namepasswordstatus') == 1)){  ?>
    setTimeout(function(){        
        $("#namepassword_popup").addClass("open_popup");
    }, 10000);
    
    
<?php }else  ?>

<?php if(isset($users)  && $users->name != '' && $users->state == '' && $users->campaign_id == '2' && Session::has('ageregionstatus') && (Session::get('ageregionstatus') == 1)){  ?>
    setTimeout(function(){        
        $("#ageregion_popup").addClass("open_popup");
    }, 12000);
        
<?php } ?>


<?php if(isset($users) && $users->state == '' && $users->campaign_id != '2' && Session::has('regionstatus') && (Session::get('regionstatus') == 1)){  ?>
    setTimeout(function(){        
        $("#region_popup").addClass("open_popup");
    }, 10000);
    
    
<?php }else  ?>

<?php if(isset($users) && $users->state != '' && $users->name == '' && $users->campaign_id != '2' && Session::has('namestatus') && (Session::get('namestatus') == 1)){  ?>
    setTimeout(function(){        
        $("#name_popup").addClass("open_popup");
    }, 12000);
        
<?php } ?>

//hide popup on outer click js
$(".outer_close_modal").on('click',function(){
   $(this).removeClass("open_popup");
});
//Stop Event on inner div
 $(".splash_content_bg").on('click',function(e){
   e.stopPropagation();
});

//region form submit
$(document).ready(function() 
{	
	$('.region-process').hide();
	
	$('#region_form').submit(function(e) 
	{
		e.preventDefault();
	
		var region = $('#prof_region2').val();
		var error =0;
	
		if(region == '')
		{
		  $('.region').show();
		  $('.region').text('Please select region');
		  error++;
		}
		else{
		  $('.region').hide();
		  $('.region').text('');
		}
	
		if(error == 0)
		{
			$('.region-process').show();
			$.ajax(
			{
				headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
				url: '<?php echo url("/")?>/update-profile',
				data: { region: region },
				type: "POST",				
				success: function( data ) 
				{
					$('.loader').hide();
					if(data)
					{
						$('.region-process').hide();
                        $('#succmess').show();
                        setTimeout(function(){        
                            $("#region_popup").removeClass("open_popup");
                        }, 2000);
                        setTimeout(function(){        
                            $("#name_popup").addClass("open_popup");
                        }, 3000);
						//location.reload();
					}
				},
				error: function( xhr, status, errorThrown ) 
				{
					$('.region-process').hide();
					alert( "Sorry, there was a problem!" );
				}
			});
		}
	
	});
	
});


//profilename form submit
$(document).ready(function() 
{	
	$('.profilename-process').hide();
	
	$('#profilename_form').submit(function(e) 
	{
		e.preventDefault();
	
		var profilename = $('#profilename').val();
		var error =0;
	
		if(profilename == '')
		{
		  $('.profilename').show();
		  $('.profilename').text('Please enter name');
		  error++;
		}
		else{
		  $('.profilename').hide();
		  $('.profilename').text('');
		}
	
		if(error == 0)
		{
			$('.profilename-process').show();
			$.ajax(
			{
				headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
				url: '<?php echo url("/")?>/update-profile',
				data: { profilename: profilename },
				type: "POST",				
				success: function( data ) 
				{
					$('.loader').hide();
					if(data)
					{
						$('.profilename-process').hide();
                        $('#succmess1').show();
                        setTimeout(function(){        
                            $("#name_popup").removeClass("open_popup");
                        }, 2000);
						//location.reload();
					}
				},
				error: function( xhr, status, errorThrown ) 
				{
					$('.profilename-process').hide();
					alert( "Sorry, there was a problem!" );
				}
			});
		}
	
	});
	
});




//namepassword form submit
$(document).ready(function() 
{	
	$('.region-process').hide();
	
	$('#namepassword_form').submit(function(e) 
	{
		e.preventDefault();
	
		var name = $('#ooname').val();
        var password = $('#oopass').val();
		var error =0;
	
		if($('#ooname').val() == ''){
            $('#ooname_error').show();
            return false;
        }
        $('#ooname_error').hide();
        
        if($('#oopass').val() == ''){
            $('#oopass_error').show();
            return false;
        }
        $('#oopass_error').hide();
        
        if($('#oopass').val().length <= 3){
            $('#oominpass_error').show();
            return false;
        }
        $('#oominpass_error').hide();
        
        //if($('#oorepass').val() == ''){
        //    $('#oorepass_error').show();
        //    return false;
        //}
        //$('#oorepass_error').hide();
        //
        //if($('#oopass').val() != $('#oorepass').val()){
        //    $('#ooconfpass_error').show();
        //    return false;
        //}
        //$('#ooconfpass_error').hide();
                
	
		if(error == 0)
		{
			$('.region-process').show();
			$.ajax(
			{
				headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
				url: '<?php echo url("/")?>/update-profile',
				data: { password: password, name : name },
				type: "POST",				
				success: function( data ) 
				{
					$('.loader').hide();
					if(data)
					{
						$('.region-process').hide();
                        $('#succmess12').show();
                        setTimeout(function(){        
                            $("#namepassword_popup").removeClass("open_popup");
                        }, 2000);
                        setTimeout(function(){        
                            $("#ageregion_popup").addClass("open_popup");
                        }, 3000);
						//location.reload();
					}
				},
				error: function( xhr, status, errorThrown ) 
				{
					$('.region-process').hide();
					alert( "Sorry, there was a problem!" );
				}
			});
		}
	
	});
	
});


//profilename form submit
$(document).ready(function() 
{	
	$('.region-process').hide();
	
	$('#ageregion_form').submit(function(e) 
	{
		e.preventDefault();
	
		var region = $('#prof_region3').val();
        var age = $('#ooyear').val();
		var error =0;
		
        if($('#prof_region3').val() == ''){
            $('#ooregion_error').show();
            return false;
        }
        $('#ooregion_error').hide();	
	
		if(error == 0)
		{
			$('.region-process').show();
			$.ajax(
			{
				headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
				url: '<?php echo url("/")?>/update-profile',
				data: { age: age,region:region },
				type: "POST",				
				success: function( data ) 
				{
					$('.loader').hide();
					if(data)
					{
						$('.region-process').hide();
                        $('#succmess11').show();
                        setTimeout(function(){        
                            $("#ageregion_popup").removeClass("open_popup");
                        }, 2000);
						//location.reload();
					}
				},
				error: function( xhr, status, errorThrown ) 
				{
					$('.profilename-process').hide();
					alert( "Sorry, there was a problem!" );
				}
			});
		}
	
	});
	
});


</script>      
<script>  
  //notice popup open js
  $(window).on('load', function(){
	
	if(localStorage.getItem('popState') != 'shown')
	{
         <?php if($rand != 0) {?>
			setTimeout(function()
			{
				$(".notice_popup").addClass("open_popup");
			}, 2000);
         <?php } ?>   
        localStorage.setItem('popState','shown')
    } 
   
  });
  $(".confirm_btn").on('click', function(){
    $(this).parents(".notice_popup").removeClass("open_popup");
    //$('#passwordModal1').modal('show');
    $('#passwordModal1').modal({backdrop: 'static', keyboard: false});
  });
   $(".leave_btn").on('click', function(){
   window.location.href='https://www.google.com/';
  });
  
</script>    
<script type="text/javascript">
  //var page = 1;
  var is_login = '<?php echo (Auth::id()); ?>';
  /*$(window).scroll(function() {  
      if($(window).scrollTop() + $(window).height() >= $(document).height()- 0.5) {
          page++;
          if(is_login){
            loadMoreData(page);
          }
      }
  });*/

  /*$(window).on('hashchange', function() {
        if (window.location.hash) {
            var page = window.location.hash.replace('#', '');
            if (page == Number.NaN || page <= 0) {
                return false;
            }else{
                if(is_login){
                  loadMoreData(page);
                }
            }
        }
    });*/

  $(document).on('click', '.pagination a',function(event){
      event.preventDefault();

      $('li').removeClass('active');
      $(this).parent('li').addClass('active');

      var myurl = $(this).attr('href');
      var page=$(this).attr('href').split('page=')[1];

      if(is_login){
        loadMoreData(page);
      }
  });


  function loadMoreData(page){
    var currentURL = $(location).attr("href");
    var n = currentURL.includes("?");
    var url;
    if(n){
      currentURL = currentURL.split("?");
      url = '?page=' + page + '&' + currentURL[1];
    }
    else{
      url = '?page=' + page
    }
    $.ajax(
          {
              headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              },
              url: url,
              type: "get",
              beforeSend: function()
              {
                  $('.load-more').show();
              }
          })
          .done(function(data)
          { //console.log(data);
              if(data.html == ""){
                  $('.load-more').text("");
                  return;
              }
              $('.load-more').hide();
              //$("#all-users").append(data.html);
              $(".all-users").empty().html(data.html);
              //location.hash = page;
          })
          .fail(function(jqXHR, ajaxOptions, thrownError)
          {
                alert('server not responding...');
          });
  }
</script>

<script>
$(document).ready(function(){     
        $('#myScrollchat').hide();
		$("#profile_popup").removeClass("user_profile_popup");
});
</script>    

@endsection
