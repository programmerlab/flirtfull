<?php
namespace Modules\Admin\Http\Controllers;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests;
use Illuminate\Http\Request;
use Modules\Admin\Http\Requests\PageRequest;
use Modules\Admin\Models\User;
use Modules\Admin\Models\Page;
use Input;
use Validator;
use Auth;
use Paginate;
use Grids;
use HTML;
use Form;
use Hash;
use View;
use URL;
use Lang;
use Session;
use Route;
use Crypt;
use App\Http\Controllers\Controller;
use Illuminate\Http\Dispatcher; 
use Modules\Admin\Helpers\Helper as Helper;
use Response;
use Illuminate\Support\Facades\DB;

/**
 * Class AdminController
 */
class PageController extends Controller {
    /**
     * @var  Repository
     */

    /**
     * Displays all admin.
     *
     * @return \Illuminate\View\View
     */
    public function __construct() {        
        $this->middleware('admin');
        View::share('viewPage', 'page');
        View::share('heading', 'Pages');
        View::share('route_url', route('page'));
        View::share('helper',new Helper);
        $this->record_per_page = Config::get('app.record_per_page');
    }

    protected $categories;

    /*
     * Dashboard
     * */

    public function index(Page $page, Request $request) 
    { 
        $page_title = 'Pages';
        $page_action = 'View Pages'; 
        if ($request->ajax()) {
            $id = $request->get('id'); 
            $category = Page::find($id); 
            $category->status = $s;
            $category->save();
            echo $s;
            exit();
        }
         
        $vendor_id = 1;
        
        // Search by name ,email and group
        $search = Input::get('search');
        $status = Input::get('status');

        if ((isset($search) && !empty($search))) {
            $search = isset($search) ? Input::get('search') : '';

            $pages = Page::with('pages')->where(function ($query) use ($search,$status) {
                if (!empty($search)) {
                    $query->Where('title', 'LIKE', "%$search%");
                }
            });
        }
        else{ 
            $pages = $page->orderBy('id','desc')->Paginate($this->record_per_page);
        }
        
        //echo "<pre>"; print_r($pages); die;

        return view('admin::page.index', compact('pages', 'page_title', 'page_action','helper'));
   
    }

    /*
     * create  method
     * */

    public function create(Page $page, Request $request) 
    {        
        $page_title = 'Pages';
        $page_action = 'Create Page';
         
        return view('admin::page.create', compact('categories','cat','category','page','sub_category_name', 'page_title', 'page_action'));
     }

    /*
     * Save Group method
     * */


   

    public function store(PageRequest $request, Page $page) 
    {                       
        $vendor_id = 1;
        //echo "<pre>"; print_r($page); die;

        if ($request->file('image')) { 
            $photo = $request->file('image');
            $destinationPath = storage_path('uploads/pages');
            $photo->move($destinationPath, time().$photo->getClientOriginalName());
            $photo_name = time().$photo->getClientOriginalName();
            $request->merge(['photo'=>$photo_name]);
           
            $page->title      =   $request->get('title');
            if($request->get('slugs') && !empty($request->get('slug'))){
                $page->slug              =   strtolower(str_replace(" ", "-", $request->get('slug')));  
                $pro_slug   = strtolower(str_replace(" ", "-", $request->get('slug')));
                $url        = $cat_url.$pro_slug;  
            }else{
                $page->slug              =   strtolower(str_replace(" ", "-", $request->get('title')));
                $pro_slug   = strtolower(str_replace(" ", "-", $request->get('title')));
                $url        = $pro_slug;
            }
            //echo $photo_name; die;
                                    
            $page->description        =   $request->get('description');            
            $page->image              =   $photo_name;            
            $page->user_id            =   $vendor_id;
            $page->slug               =   $url;  

            //echo "<pre>"; print_r($page); die;
            DB::table('pages')->insert(
                ['title' => $request->get('title'),
                 'description' => $request->get('description'),
                 'image' => $photo_name,
                 'tags' => $request->get('tags'),
                 'user_id' => 1,
                 'slug' => $url,
                 'meta_key' => $request->get('meta_key'),
                 'meta_description' => $request->get('meta_description'),
                 'page_title' => $request->get('page_title')
                 ]
            );
            //$page->save(); 

        } 
        else{
            $page->title      =   $request->get('title');
            if($request->get('slugs') && !empty($request->get('slug'))){
                $page->slug              =   strtolower(str_replace(" ", "-", $request->get('slug')));  
                $pro_slug   = strtolower(str_replace(" ", "-", $request->get('slug')));
                $url        = $cat_url.$pro_slug;  
            }else{
                $page->slug              =   strtolower(str_replace(" ", "-", $request->get('title')));
                $pro_slug   = strtolower(str_replace(" ", "-", $request->get('title')));
                $url        = $pro_slug;
            }
            //echo $photo_name; die;
                                    
            $page->description        =   $request->get('description');            
                     
            $page->user_id            =   $vendor_id;
            $page->slug               =   $url;  

            
            DB::table('pages')->insert(
                ['title' => $request->get('title'),
                 'description' => $request->get('description'),
                 'tags' => $request->get('tags'),
                 'user_id' => 1,
                 'slug' => $url,
                 'meta_key' => $request->get('meta_key'),
                 'meta_description' => $request->get('meta_description'),
                 'page_title' => $request->get('page_title')
                 ]
            );
        }
       
        return Redirect::to(route('page'))
                            ->with('flash_alert_notice', 'New Page was successfully created !');
    }
    /*
     * Edit Group method
     * @param 
     * object : $category
     * */

    public function edit(Page $page) {

        $page_title = 'Pages';
        $page_action = 'Show Page';                         

        return view('admin::page.edit', compact( 'page', 'page_title', 'page_action'));
    }

    public function update(PageRequest $request, Page $page) 
    {        
        
         if ($request->file('image')) { 

            $photo = $request->file('image');
            //$destinationPath = base_path() . '/public/uploads/pages/';
            $destinationPath = storage_path('uploads/pages');
            $photo->move($destinationPath, time().$photo->getClientOriginalName());
            $photo_name = time().$photo->getClientOriginalName();
            $request->merge(['photo'=>$photo_name]);
           
            $page->title      =   $request->get('title');
            if($request->get('slug') && !empty($request->get('slug'))){
                $page->slug              =   strtolower(str_replace(" ", "-", $request->get('slug')));  
                $pro_slug   = strtolower(str_replace(" ", "-", $request->get('slug')));
                $url        = $pro_slug;  
            }else{
                $page->slug              =   strtolower(str_replace(" ", "-", $request->get('page_title')));
                $pro_slug   = strtolower(str_replace(" ", "-", $request->get('page_title')));
                $url        = $pro_slug;
            }
            
            $page->description        =   $request->get('description');
            $page->image              =   $photo_name;            
            
            if($request->get('title')){
                $page->title  = $request->get('title');
            }
            
            DB::table('pages')
                    ->where('id', $page->id)
                    ->update(['title' => $request->get('title'),
                                'description' => $request->get('description'),
                                'tags' => $request->get('tags'),
                                'meta_key' => $request->get('meta_key'),
                                'meta_description' => $request->get('meta_description'),
                                'page_title' => $request->get('page_title'),
                                'image' => $photo_name]);
            //$page->save(); 
        }else{            
            $page->title      =   $request->get('title');
            if($request->get('slug') && !empty($request->get('slug'))){
                $page->slug              =   strtolower(str_replace(" ", "-", $request->get('slug')));  
                $pro_slug   = strtolower(str_replace(" ", "-", $request->get('slug')));
                $url        = $pro_slug;  
            }else{
                $page->slug              =   strtolower(str_replace(" ", "-", $request->get('page_title')));
                $pro_slug   = strtolower(str_replace(" ", "-", $request->get('page_title')));
                $url        = $pro_slug;
            }
            
            $page->description        =   $request->get('description');
            $page->image              =   $request->get('images');
            
            //echo "<pre>"; print_r($page); die;
            if($request->get('title')){
                $page->title  = $request->get('title');
            }
            
            DB::table('pages')
                    ->where('id', $page->id)
                    ->update(['title' => $request->get('title'),
                                'description' => $request->get('description'),
                                'tags' => $request->get('tags'),
                                'meta_key' => $request->get('meta_key'),
                                'meta_description' => $request->get('meta_description'),
                                'page_title' => $request->get('page_title'),
                                'image' => $request->get('images')]);
            //$page->save(); 
        }
        return Redirect::to(route('page'))
                        ->with('flash_alert_notice', 'Page was  successfully updated !');
    }
    /*
     *Delete User
     * @param ID
     * 
     */
    public function destroy(Page $page) {
        
        Page::where('id',$page->id)->delete();

        return Redirect::to(route('page'))
                        ->with('flash_alert_notice', 'Page was successfully deleted!');
    }

    public function show(Page $page) {
        
    }

}
