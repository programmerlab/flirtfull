<!DOCTYPE html>
<html lang="en">
   <head>
      <?php
        $helper = new Helper();
        $settings = $helper->settings();
        ?>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="title" content="{{ $settings['meta_title'] }}">
        <meta name="description" content="{{ $settings['meta_description'] }}">
        <meta name="keywords" content="{{ $settings['meta_key'] }}">
      <!-- CSRF Token -->
      <meta name="csrf-token" content="{{ csrf_token() }}">
      <title> {{ env('APP_NAME') }} </title>
    <link rel="shortcut icon" type="image/png" href="{{ asset('public/images/fevicon.png') }}"/>
      <link href="{{ asset('public/css/font-awesome.min.css') }}" rel="stylesheet">
      <link href="{{ asset('public/css/bootstrap.min.css') }}" rel="stylesheet">
      <link rel="stylesheet" href="{{ asset('public/css/owl.carousel.min.css') }}">
      <link href="{{ asset('public/css/style.css') }}" rel="stylesheet">
      <link href="{{ asset('public/css/responsive.css') }}" rel="stylesheet">
      <script src="{{ asset('public/js/jquery.min.js') }}"></script>
      <script src="{{ asset('public/js/bootstrap.min.js') }}"></script>
      <script src="{{ asset('public/js/owl.carousel.js') }}"></script>
      <script src="https://maps.googleapis.com/maps/api/js?key={{ env('GOOGLE_KEY') }}&libraries=places" type="text/javascript"></script>
      <style type="text/css">
        .success-message{
          color: green;
        }
      </style>
      
      <!-- Hotjar Tracking Code for www.flirtfull.com -->
        <script>
            (function(h,o,t,j,a,r){
                h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
                h._hjSettings={hjid:1384835,hjsv:6};
                a=o.getElementsByTagName('head')[0];
                r=o.createElement('script');r.async=1;
                r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
                a.appendChild(r);
            })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
        </script>
      <!-- Hotjar Tracking Code for www.flirtfull.com -->
   </head>
   <body>
<section id="main-slider">
 <div class="container">
  @if( Session::has( 'success' ))
  <div class="row">
   <div class="col-md-12">
    <div class="alert alert-success">
      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
      {{ Session::get( 'success' ) }}
    </div>
   </div>
  </div>
  @endif

  <div class="row">
   <div class="col-md-12">
    <div class="mobile_logo"><a href="#" class="logo"><img src="{{ asset('public/images/logo.png') }}" alt="True Love Logo"></a></div>
    <ul class="signin_btn text-center"> 
      <li>
        <a><button class="action default medium default action"  id="log_click">Login</button></a> 
      </li>
      <li>
       <a><button class="action default medium default action"  id="sign_click">SignUp</button></a> 
      </li>
    </ul>

    <!-- popup wrappers -->
    <div class="login_popup_wrappers">
        <!-- signup Start -->
        <div class="login_popup fl_login_popup" id="sign_popup">
            <div class="fl_login_bg">
              <div class="s_overlay"></div>
                <div class="fl_login_form">
                    <h2>Please create an account</h2>
                    <form id="signup">
                        <div class="form-group text-center">
                            <div class="radio_group">
                              <label class="control-label">I Am A</label>
                              <div>
                                <span class="radio-btn">
                                    <input type="radio" name="gender" id="malee" value="1" class="gender_classs" checked=""> 
                                    <label for="malee"><img src="{{ asset('public/images/male.png') }}"></label>
                                  </span>
                                <span class="radio-btn">
                                    <input type="radio" name="gender" id="femalee" value="2" class="gender_classs" > 
                                    <label for="femalee"><img src="{{ asset('public/images/female.png') }}"></label>
                                </span>
                                <p class="error-message gender"></p>
                              </div>
                            </div>
                            <div class="radio_group">
                              <label class="control-label">Seeking A</label>
                              <div>
                                <span class="radio-btn">
                                  <input type="radio" id="s-malee" name="seeking" value="1" class="seeking_classs" > 
                                  <label for="s-malee"><img src="{{ asset('public/images/male.png') }}"></label>
                                </span>
                                <span class="radio-btn">
                                  <input type="radio" name="seeking" id="s-femalee" value="2" class="seeking_classs" checked=""> 
                                  <label for="s-femalee"><img src="{{ asset('public/images/female.png') }}"></label>
                                </span>
                                <p class="error-message seeking"></p>
                              </div>
                            </div>
                        </div>
                        <div class="form-group birthday_select">
                            <label class="control-label">Age</label>
                            <div class="">
                              <select id="year" class="selectpicker form-control">
                                <option value="" class="label" selected="selected" >year</option>
                                <?php 
                                for($i=2001;$i>=1930;$i--)
                                {
                                  echo '<option value="'.$i.'">'.$i.'</option>';
                                }
                                ?>
                              </select>
                            </div>
                            <p class="error-message birth_date"></p>
                        </div>
                        <div class="form-group">
                          <input type="email" id="email" class="form-control" placeholder="Your Email" >
                          <p class="email-wrong error-message"></p>
                        </div>
                        <div class="form-group">
                          <input type="password" id="password" class="form-control" placeholder="Your Password" >
                          <p class="password-wrong error-message"></p>
                        </div>
                        <input type="hidden" id="partner_id" name="partner_id" value="{{ isset($_GET['partner']) ? $_GET['partner'] : '' }}">
                        <!------------------------------------->
                        <!--------WORK BY ARJUN---------------->
                        <div class="form-group terms_groups">
                          <div class="check_box">
                            <label>
                            <input type="checkbox" id="accept_cond" value="1">
                            <span class="c_box"></span>
                            <span class="c_text">I accept the <a target="_blank" href="{{ url('terms-conditions') }}">Terms &amp; Conditions</a> &amp; <a target="_blank" href="{{ url('privacy-policy') }}">Privacy Policy</a>
                            </span>
                            </label>
                          </div>
                            <div>
                              
                              <p class="error-message accept"></p>
                            </div>
                        </div>

                        <!--------WORK BY ARJUN END------------>
                        <!------------------------------------->
                        <p class="signup-process text-center">Processing...</p>
                        <div class="form-group">
                          <button type="submit" id="submit-signup" class="btn-bg btn-primary"><span>Sign Up</span></button>
                        </div>
                    </form>

                  <!--<div class="form-group">
                      <a id="glink" href="{{ route('auth.social') }}"><button class="btn-bg btn-primary login-google"><img width="20px" src="{{ asset('public/images/glogo.png') }}"> <span>Sign in with Google</span></button>              
                  </div>-->
                </div>
            </div>
        </div>
        <!-- signup End -->
        <!-- Login Start -->
        <div class="login_popup fl_login_popup" id="log_popup">
            <div class="fl_login_bg">
              <div class="s_overlay"></div>
                <div class="fl_login_form">
                    <h2>Login To Your Account</h2>
                    <form id="login">
                        <div class="form-group">
                          <input type="email" id="login_email" class="form-control" placeholder="Your Email" >
                          <p class="login-email-wrong error-message"></p>
                        </div>
                        <div class="form-group">
                          <input type="password" id="login_password" class="form-control" placeholder="Your Password" >
                          <p class="login-pass-wrong error-message"></p>
                          <p class="both-wrong error-message"></p>
                        </div>
                        <div class="form-group">
                          <button type="submit" class="btn-bg btn-primary"><span>Sign In </span></button>
                        </div>
                    </form>
                  
                  <!--<div class="form-group">
                      <a id="glink" href="{{ route('auth.social') }}"><button class="btn-bg btn-primary login-google"><img width="20px" src="{{ asset('public/images/glogo.png') }}"> <span>Sign in with Google</span></button>
                    <!--<p><small>By Google Login, you are agreeing with our <a href="{{ url('/terms-conditions') }}"><u>terms & conditions</u></a></small></p> --
                  </div>-->
  
                    <div class="form-group forgot_group">
                      <a id="forgot-link" href="javascript:void(0)"><span>Forgot Password</span></a>
                    </div>
                </div>
            </div>
        </div>
        <!-- login End -->
        <!-- Forget Start -->
        <div class="login_popup fl_login_popup" id="forgot_popup">
            <div class="fl_login_bg">
              <div class="s_overlay"></div>
                <div class="fl_login_form">
                   <h2>Enter Your Registered Email Id</h2>
                   <form id="forgot">
                    <div class="form-group">
                      <input type="email" id="forgot_email" class="form-control" placeholder="Your Email" >
                      <p class="forgot-email-wrong error-message"></p>
                      <p class="forgot-email-success success-message"></p>
                    </div>
                    <p style="display: none" class="forgot-process text-center">Processing...</p>
                    <div class="form-group">
                      <button type="submit" class="btn-bg btn-primary"><span>Reset Password</span></button>
                    </div>
                   </form>
               </div>
            </div>
        </div>
        <!-- Forget End -->
    </div>
     <!-- popup wrappers -->
</div>
</div>
<div class="row">
    <div class="col-md-7 col-sm-5 text-right pull-right">
     <a href="#" class="desktop_logo"><img src="{{ asset('public/images/logo.png') }}" alt="True Love Logo"></a>
     <h1>Making the world a <span>hotter</span> place </br>
      <span>one flirt</span> at a time </h1>
    </div>
    <div class="col-md-5 col-sm-7">
       <div class="banner-form">
       <div class="banner_form_inner">
          <div class="row">
           <div class="col-md-12">
            <div class="date_bg">
             <h2>Jump into the action and find your flirt</h2>
           </div>
         </div>
       </div>
       <form class="form-inner" onSubmit="return onSearch()" method="get" action="{{ url('search-test') }}">
        <div class="row">
         <div class="col-md-6 col-sm-6  col-xs-6">
          <div class="form-group">
            <div class="row">
             <div class="col-md-6 col-sm-5  col-xs-5">
              <label class="control-label">I am a</label>
            </div>
            <div class="col-md-6 col-sm-6  col-xs-7">

             <span class="radio-btn">
              <input type="radio" name="gender" id="male" value ="1" class="gender_class" checked="">
              <label for="male"><img src="{{ asset('public/images/male.png') }}"></label>
             </span>
              <span class="radio-btn">
                <input type="radio" name="gender" id="female" value ="2" class="gender_class">
                <label for="female"><img src="{{ asset('public/images/female.png') }}"></label>
             </span>

           </div>

           <div class="col-md-12 text-right gender-wrong error-message"></div>

         </div>
       </div>
      </div>
      <div class="col-md-6 col-sm-6  col-xs-6">
        <div class="form-group">
          <div class="row">
            <div class="col-md-6 text-right col-sm-6  col-xs-5">
              <label class="control-label">Seeking a</label>
            </div>
            <div class="col-md-6 col-sm-6  col-xs-7">
             <span class="radio-btn">
              <input type="radio" id="s-male" name="seeking" value ="1" class="seeking_class">
              <label for="s-male"><img src="{{ asset('public/images/male.png') }}"></label>
             </span>
             <span class="radio-btn">
              <input type="radio" name="seeking" id="s-female" value ="2" class="seeking_class" checked="">
              <label for="s-female"><img src="{{ asset('public/images/female.png') }}"></label>
             </span>
           </div>

           <div class="col-md-12 text-right seeking-wrong error-message"></div>
         </div>
       </div>
      </div>
      </div>
      <div class="form-group">
        <div class="row">
          <label class="col-md-3 col-sm-3 control-label  col-xs-3">Age</label>
          <div class="col-md-9 col-sm-9  col-xs-9">
           <div class="select-left">
            <select name="min_age" class="selectpicker">
              <option value="18">18</option>
              <option value="20">20</option>
              <option value="25">25</option>
              <option value="30">30</option>
              <option value="35">35</option>
              <option value="40">40</option>
              <option value="45">45</option>
              <option value="50">50</option>
              <option value="55">55</option>
              <option value="60">60</option>
              <option value="65">65</option>
              <option value="70">70</option>
              <option value="75">75</option>
            </select>
          </div>
          <div class="select-divider">-</div>
          <div class="select-left">
            <select name="max_age" class="selectpicker">
              <option value="20">20</option>
              <option value="25">25</option>
              <option value="30" selected>30</option>
              <option value="35">35</option>
              <option value="40">40</option>
              <option value="45">45</option>
              <option value="50">50</option>
              <option value="55">55</option>
              <option value="60">60</option>
              <option value="65">65</option>
              <option value="70">70</option>
              <option value="75">75</option>
              <option value="80">80+</option>        
            </select>
          </div>
        </div>
      </div>
      </div>

      <!--<div class="form-group">
      <div class="row">
         <div class="">
           <label class="col-md-3 col-sm-3 control-label  col-xs-3">Country</label>
           <div class="col-md-9 col-sm-9  col-xs-9">
            <select name="country" id="country_home" class="home_hometown" onChange="getCity(this.value);">
             <option selected="" value="">Country</option>
                  @foreach($countries as $row)
                  <option value="{{ $row->name }}">{{ $row->name }}</option>
                  @endforeach 
            </select>
           </div>
           <!-- <input type="text" placeholder="Enter your Hometown" id="country_search_home" class="home_hometown">
           <input type="hidden" value="" name="country" id="country_home"> --
         </div>
       </div>
        </div>-->

        <div class="form-group">
      <div class="row">
         <div class="">
           <div class="col-md-12 col-sm-12  col-xs-12">
            <select name="state" id="region_home" class="home_hometown" onchange="getCityByState(this.value);">
             <option selected="" value="">Region</option>   
            </select>
           </div>
         <!--<div class="col-md-6 col-sm-6  col-xs-6">
            <select name="city" id="city_home" class="home_hometown">
             <option selected="" value="">City</option>   
            </select>
           </div>-->
           <!-- <input type="text" placeholder="Enter your Hometown" id="country_search_home" class="home_hometown">
           <input type="hidden" value="" name="country" id="country_home"> -->
         </div>
       </div>
        </div>
      <!-- <div class="form-group">
        <div class="row">
          <label class="col-md-3 control-label">City</label>
          <div class="col-md-9">
            <select class="selectpicker">
              <option>Alexander</option>
              <option>Andalusia</option>
              <option> Anniston</option>
              <option>Athens</option>
              <option>Atmore</option>
              <option> Auburn</option>
              <option>Bessemer</option>
              <option>Birmingham</option>
              <option>Chickasaw</option>
              <option>Clanton</option>
            </select>
          </div>
        </div>
      </div> -->
      <div class="row">
        <div class="col-md-12">
          <div class="form-group text-center"><button type="submit" class="btn-bg btn-primary"><span>Find My Matches <i class="fa fa-heart-o heart1"></i><i class="fa fa-heart-o heart2"></i><i class="fa fa-heart-o heart3"></i></span></button></div>
        </div>
      </div>
      </form>
      </div>
    </div>
    </div>

    </div>
  </div>
</section>
<section class="page-section-ptb text-center">
 <div class="container">
  <div class="section-title row">
    <div class="col-md-12">
     <h2>Start flirting in 3 simple steps</h2>
 <!--     <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna</p> -->
   </div>
   </div>
   <div class="row">
     <div class="col-md-4 col-sm-4">
      <div class="img-icon"><img class="img-center" src="{{ asset('public/images/createprofile.png') }}" alt="Create Profile"></div>
      <h4>Create Profile</h4>
      <p>Our one page registration form makes it very easy to create a profile. After activation you can personalise your profile to increase your chances on a match.</p>
    </div>
    <div class="col-md-4 col-sm-4">
      <div class="img-icon"><img class="img-center" src="{{ asset('public/images/findmatch.png') }}" alt="Find Match"></div>
      <h4>Find Match</h4>
      <p>What ever you are looking for, if he/she is in the UK we will find him/her for you! Use our search function to browse 1000s of members and find your ultimate match.</p>
    </div>
    <div class="col-md-4 col-sm-4">
      <div class="img-icon"><img class="img-center" src="{{ asset('public/images/flirting.png') }}" alt="Start Dating"></div>
      <h4>Start Flirting</h4>
      <p>Let the action begin. Engage in hot conversations with hot members who are looking for just the same. Unleash your fantasies.</p>
    </div>
  </div>
</div>
</section>

@if(!empty($most_popular) && count($most_popular)>0)
<section class="page-section-profile text-center gray-bg">
 <div class="container">
  <div class="section-title row">
    <div class="col-md-12">
     <h2>Meet some of our hot members</h2>
   </div>
 </div>
 <div class="row">
  @foreach($most_popular as $single)
  <?php $name = strlen($single->name) > 8 ? substr($single->name,0,8)."..." : $single->name;?>
  <?php
        if($single->fake){
            $profileimg = env('PROFILE_MANAGER_ADDR').('/public/images/profile_image/'.$single->profile_image);        
        }else{
            $profileimg = asset('/public/images/profile_image/'.$single->profile_image);
        }
    if (@GetImageSize($profileimg) && ($single->profile_image_status == 1)) 
    { 
      $profile_img = $profileimg ;
    }
    else{  
      $profile_img = asset('public/images/user-profile.jpg');
    }
  ?>
  <div class="col-lg-2 col-md-3 col-sm-4 col-xs-6 home_prof_box">
     <div class="item popular-profile" style="margin: 15px 0">
      <div class="user_img">
        <div class="profile_img" style="background-image:url({{ $profile_img }});"></div>
      </div>
      <h3>{{ ucwords($name) }}, {{ $single->age }}</h3>
      <p>{{ ucwords($single->state) }}<!--, {{ ucwords($single->country) }} --></p>
      <a href="{{ url('profile') }}/{{ $single->user_id }}">Chat</a>
    </div>
  </div>
  @endforeach
  </div>
 </div>
</section>
@endif

<!--<section class="page-section-testimonial text-center">
 <div class="container">
  <div class="section-title row">
    <div class="col-md-12">
     <h2>Testimonials</h2></div>
   </div>
   <div class="row">
     <div class="owl-carousel owl-theme" id="owl-carousel1">
      <div class="item">
       <div class="tuser_img"><img src="{{ asset('public/images/thum-2.jpg') }}" alt="Testimonials imaage"></div>
       <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>
       <h3>Francisco Pierce - <span>USA</span></h3>
     </div>
     <div class="item">
       <div class="tuser_img"><img src="{{ asset('public/images/thum-2.jpg') }}" alt="Testimonials imaage"></div>
       <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>
       <h3>Glen Bell - <span>USA</span></h3>
     </div>
     <div class="item">
       <div class="tuser_img"><img src="{{ asset('public/images/thum-2.jpg') }}" alt="Testimonials imaage"></div>
       <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>
       <h3>Bill Nelson - <span>USA</span></h3>
     </div>
   </div>
 </div>
</div>
</section>-->
<!-- <section class="page-section-subscribe text-center">
 <div class="container">
  <div class="section-title row">
    <div class="col-md-12">
     <h2>Get The Best Dating Team Now</h2>
     <p>Want to hear more story, join the secure & easy way</p>
  </div>
   </div>
   <div class="row">
    <div class="col-md-12">
     <a href="{{ url('/') }}"><span>create an account <i class="fa fa-heart-o heart3"></i></span></a></div>
   </div>
 </div>
</section> -->
<!-- signup section start -->
  <div class="signup_banner">
    <div class="container">
      <div class="signup_banner_text">
        <h3>3 Reasons why flirtfull.com</h3>
        <ul>
          <li><i class="fa fa-heart"></i> Flirtfull.com is 100% discrete and anonymous</li>
          <li><i class="fa fa-heart"></i> We have a matching forluma that works. Hot chats guaranteed!</li>
          <li><i class="fa fa-heart"></i> 
            Flirtfull.com is a adult platform with thousands of men and women with the same mind set and looking for just the same like you: <span>HOT ONLINE ACTION</span>
          </li>
        </ul>
        <div class="banner_btn_wrap">
         <a href="#" class="signup_button_n">Singup now</a>
        </div>
      </div>
    </div>
  </div>
<!-- signup section End -->
<!-- skills section start -->
  <div class="skills_section">
    <div class="container">
      <div class="row">
        <div class="col-sm-4 col-xs-6">
          <div class="skills_box">
            <span class="skill_icon">
              <i class="fa fa-shield"></i>
            </span>
        <h4>100%</h4>
        <p>Verified Profiles</p>
           </div>
         </div>
          <div class="col-sm-4 col-xs-6">
          <div class="skills_box">
            <span class="skill_icon">
              <i class="fa fa-users"></i>
            </span>
        <h4>100%</h4>
        <p>Matching Guaranteed</p>
           </div>
         </div>
        <div class="col-sm-4 col-xs-6">
          <div class="skills_box">
            <span class="skill_icon">
              <i class="fa fa-lock"></i>
            </span>
        <h4>100%</h4>
        <p>Discrete & Anonymous</p>
           </div>
         </div>
       </div>
    </div>
  </div>
<!-- skills section End -->
<script type="text/javascript">
$(document).ready(function() {
  //for login
  $('#login').submit(function(e) {
    e.preventDefault();
    var login_email = $('#login_email').val();
    var login_password = $('#login_password').val();
    var err =0;
    var mail_validation = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if(login_email.trim() == "")
    {
      $('.login-email-wrong').text('Please enter email');
      err++;
    }
    else if(!mail_validation.test(login_email)){
      $('.login-email-wrong').text('Please enter valid email');
      err++;
    }
    else{
     $('.login-email-wrong').text('');
    }
    
    if(login_password.trim() == "")
    {
      $('.both-wrong').text('');
      $('.login-pass-wrong').text('Please enter password');
      err++;
    }
    else{
     $('.login-pass-wrong').text('');
    }

    if(err == 0)
    { 
      $('.loader').show();
      $.ajax({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: '<?php echo url("/")?>/login',
        data: {
            email: login_email ,
            password: login_password
        },
        type: "POST",
        dataType : "json",
        success: function( data ) {
            //alert(JSON.stringify(data));
            //alert(data.errors.password);
            console.log(data);
            $('.loader').hide();
            if(data.success)
            {
              window.location.href = "<?php echo url('/')?>/search";
            }
            else if(data.errors.email){
              $('.login-email-wrong').text(data.errors.email);
            }
            else if(data.errors.password){
              $('.both-wrong').text('');
              $('.login-pass-wrong').text(data.errors.password);
            }
            else if((data.errors.email) && (data.errors.password)){
              $('.login-email-wrong').text(data.errors.email);
              $('.login-pass-wrong').text(data.errors.password);
            }
            else{
              $('.login-pass-wrong').text('');
              $('.both-wrong').text(data.errors);
            }
          },
          error: function( xhr, status, errorThrown ) {
            console.log( "Sorry, there was a problem!" );
          }
      });
    }
    else{
      //$('.both-wrong').text('Please fill all the details');
    }
    
  });

  //for signup
  $('#signup').submit(function(e){
    e.preventDefault();
    var uname = $('#uname').val();
    var email = $('#email').val();
    var password = $('#password').val();
  
  //work by arjun 
    var month = $('#month').val();
    var day = $('#day').val();
    var year = $('#year').val();
    var country = $('#country').val();
    var region = $('#prof_region1').val();
    var city = $('#prof_city').val();
    var gender = $("input:radio.gender_classs:checked").val();
    var seeking = $("input:radio.seeking_classs:checked").val();
    var partner_id = $('#partner_id').val();
    //alert(partner_id);
    var mail_validation = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
   
    var error =0;
    if(uname.trim() == "")
    {
      $('.name-wrong').text('Please enter username');
      $("#uname").css("border-color", "red");
      error++;
    }
    else{
      $('.name-wrong').text('');
      $("#uname").css("border-color", "#eee");
    }
    if(email.trim() == "")
    {
      $('.email-wrong').text('Please enter email');
      $("#email").css("border-color", "red");
      error++;
    }
    else if(!mail_validation.test(email)){
      $('.email-wrong').text('Please enter valid email');
      $("#email").css("border-color", "red");
      error++;
    }
    else{
      $('.email-wrong').text('');
      $("#email").css("border-color", "#eee");
    }
    if(password.trim() == "")
    {
      $('.password-wrong').text('Please enter password');
      $("#password").css("border-color", "red");
      error++;
    }
    else{
      $('.password-wrong').text('');
      $("#password").css("border-color", "#eee");
    }

    if(region == '')
    {
      $('.region').show();
      $('.region').text('Please select region');
      error++;
    }
    else{
      $('.region').hide();
      $('.region').text('');
    }

    if(gender == '' || typeof gender === "undefined")
    {
      $('.gender').show();
      $('.gender').text('Please select gender');
      error++;
    }
    else{
      $('.gender').hide();
      $('.gender').text('');
    }

    if(seeking == '' || typeof seeking === "undefined")
    {
      $('.seeking').show();
      $('.seeking').text('Please select seeking');
      error++;
    }
    else{
      $('.seeking').hide();
      $('.seeking').text('');
    }

    if(year == '')
    {
      $('.bith_date').show();
      $('.birth_date').text('Select your date of birth');
      error++;
    }
    

    if(month == '')
    {
      $('.bith_date').show();
      $('.birth_date').text('Please select birth date');
      error++;
    }
    
    
    if(day == '')
    {
      $('.bith_date').show();
      $('.birth_date').text('Please select birth date');
      error++;
    }
    

    if(day != '' && month != '' && year != '')
    {
      $('.bith_date').hide();
      $('.birth_date').text('');
    }

    if($('#accept_cond').is(':not(:checked)'))
    {
      $('.accept').show();
      $('.accept').text('Please accept conditions');
      error++;
    }
    else{
      $('.accept').hide();
      $('.accept').text('');
    }

    if(error == 0)
    {
      $('.signup-process').show();
      $.ajax({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: '<?php echo url("/")?>/signup',
        data: {
            name: uname,
            email: email,
            password: password,
      
      month: month,
            day: day,
            year: year,
            country: country,
            region: region,
            city: city,
            gender: gender,
            seeking: seeking,
            partner_id: partner_id
        },
        type: "POST",
        dataType : "json",
        success: function( data ) {
            //alert(JSON.stringify(data));
            //alert(data.errors.password);
            $('.signup-process').hide();
            if(data.success)
            {
              $('.signup-process').hide();
              window.location.href = "<?php echo url("/")?>";
             // alert(data);
            }
            if(data.errors.name){
              //clear_signup_errors();
              $('.name-wrong').text(data.errors.name);
              $("#uname").css("border-color", "red");
            }
            else{
              $("#uname").css("border-color", "#eee");
            }
           
            
            if(data.errors.email){
              //clear_signup_errors();
              $.each( data.errors.email, function( i, val ) {
                  $('.email-wrong').append('<p class="error-message">'+val+'</p>');
              });
              $("#email").css("border-color", "red");
              //$('.signup-email-wrong').text(data.errors.email);
            }
            else{
              $("#email").css("border-color", "#eee");
            }
            if(data.errors.password){
              //clear_signup_errors();
              $.each( data.errors.password, function( i, val ) {
                  $('.password-wrong').append('<p class="error-message">'+val+'</p>');
              });
              $("#password").css("border-color", "red");
              //$('.password-wrong').text(data.errors.password);
            }
            else
            {
              $("#password").css("border-color", "#eee");
            }
          },
          error: function( xhr, status, errorThrown ) {
            $('.signup-process').hide();
            alert( "Sorry, there was a problem!" );
          }
      });
    }
    else{
      //$('.all-wrong').text('Please fill all the details');
    }

  });


  //forgot password

  $('#forgot').submit(function(e) {
    e.preventDefault();
    var forgot_email = $('#forgot_email').val();
    
    var err =0;
    var mail_validation = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if(forgot_email.trim() == "")
    {
      $('.forgot-email-wrong').text('Please enter email');
      err++;
    }
    else if(!mail_validation.test(forgot_email)){
      $('.forgot-email-wrong').text('Please enter valid email');
      err++;
    }
    else{
     $('.forgot-email-wrong').text('');
    }
    

    if(err == 0)
    { 
      $('.forgot-process').show();
      $.ajax({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: '<?php echo url("/")?>/forgetPassword',
        data: {
            email: forgot_email ,
        },
        type: "POST",
        dataType : "json",
        success: function( data ) {
            //alert(JSON.stringify(data));
            //alert(data.errors.password);
            $('.forgot-process').hide();
            if(data.status)
            {
              $('.forgot-email-success').text(data.message);
              $('.forgot-email-wrong').text('');
              //window.location.href = "<?php echo url('/')?>/search";
            }
            else if(data.message){
              $('.forgot-email-wrong').text(data.message);
              $('.forgot-email-success').text('');
            }
            
          },
          error: function( xhr, status, errorThrown ) {
            $('.forgot-process').hide();
            console.log( "Sorry, there was a problem!" );
          }
      });
    }
    else{
      //$('.both-wrong').text('Please fill all the details');
    }
    
  });
});
</script>
<footer class="fl_footer_section text-center footer_home">
         <div class="container">
           <!--  <div class="footer-logo"><a href="#"><img src="{{ asset('public/images/logo.png') }}" alt="Logo"></a>
            </div> -->
            <div class="footer_menu">
              <ul>
                 <li><a href="{{ url('about') }}">About Us</a></li>
                 <li><a href="{{ url('privacy-policy') }}">Privacy Policy</a></li>
                 <li><a href="{{ url('terms-conditions') }}">Terms &amp; Conditions</a></li>
              </ul>
            </div>
            <div class="footer_text">
                <p>
                  <span class="full_width">The minimum age for participation on flirtfull.com is 18 years.</span>
                  Flirtfull.com is an adult social flirting platform for men and women who are looking for a fun, flirty or exciting contact. Every day, hundreds of new members sign up. Based on your profile settings, you will receive match suggestions. You can also use our search functionality and browse for profiles yourself. Flirtfull.com is designed for adult pleasure and entertainmentand optimised for desktops, mobile phones and tablets.Profiles are partly fictional, physical arrangements with these angel profiles are not possible. We strongly advise you to read our Terms and Conditions before using our Service.
              </p>
            </div>
           <!--  <ul>
               <li><a href="#"><i class="fa fa-facebook"></i></a></li>
               <li><a href="#"><i class="fa fa-twitter"></i></a></li>
               <li><a href="#"><i class="fa fa-google"></i></a></li>
               <li><a href="#"><i class="fa fa-youtube"></i></a></li>
            </ul> -->
            <p>© {{ date('Y') }} - {{ env('APP_NAME') }} All Right Reserved </p>
         </div>
      </footer>
  <!-- notice popup -->
  
  
<?php if($reslt !=0){ ?>
<div id="notice_popup2" class="notice_popup">
    <div class="notice_dialog">
      <div class="notice_popup_content">
        <div class="popup_header">
          <h1>IMPORTANT NOTICE:</h1>
        </div>
        <div class="center_content">
          <h4>YOU Are Successfully Registered On Flirtfull Now</h4>
          <ul>
            <li>You will receive two emails</li> 
            <li>one is for verify your email address</li>
            <li>another one is contains your login detils</li>
          </ul>
        </div>
        <div class="popup_footer_content">
          <div class="popup_confirm_buttons">
            <button class="confirm_btn buttons">Close</button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <script>
  //notice popup open js
  $(window).on('load', function(){
    $("#notice_popup2").addClass("open_popup");
  });
  $(".confirm_btn").on('click', function(){
   // $(this).parents("#notice_popup2").removeClass("open_popup");
   window.location.href='https://flirtfull.com/';
  });
</script>
<?php } ?>
  
  <div class="notice_popup">
    <div class="notice_dialog">
      <div class="notice_popup_content">
        <div class="popup_header">
          <h1>IMPORTANT NOTICE:</h1>
        </div>
        <div class="center_content">
          <h4>You state that the following facts are accurate</h4>
          <ul>
            <li>I am at least 18-years old.</li> 
            <li>I will not redistribute any material from the website.</li>
            <li>I will not allow any minors to access the website or any material found in it</li>
            <li>Any material I view or download from the website is for my own personal use and I will not show it to a minor.</li>
            <li>
              Sexually explicit material depicting kinky and other fetish activities is allowed by the local law governing my jurisdiction.
            </li>
            <li>
              I was not contacted by the suppliers of this material, and I willingly choose to view or download it
            </li>
            <li>
                I am entering this website because it has adult content, and pictures depicting men or women in various sexual situations is not obscene or offensive in any way. In addition, I do not believe that this material could be considered obscene or offensive.
            </li>
             <li>
                I acknowledge that the website includes fictitious angel profiles created and operated by the website that may communicate with me for entertainment and other purposes. I also understand that physical encounters with this angel profiles are not possible. 
            </li>
             <li>
                I acknowledge that individuals appearing in photos on the landing page or in angel profiles might not be actual members of the website and that certain data is provided for illustration and entertainment purposes only.
            </li>
             <li>
                I acknowledge that the website does not inquire into the background of its members and the website does not otherwise attempt to verify the accuracy of statements made by its members.
            </li>
              <li>
                I acknowledge that the website does not guarantee that I will find a date or that I will meet any of its members in person or that any given person or profile manifested on the website is available or interested in dating or communicating with me or anyone else.
            </li> 
            <li>
                I acknowledge that my use of the website is governed by the website’s <a target="_blank" href="{{ url('terms-conditions') }}">Terms and condition</a> and the website’s <a target="_blank" href="{{ url('privacy-policy') }}">Privacy Policy</a>, which I have carefully reviewed and accepted, and I am legally bound by the <a target="_blank" href="{{ url('terms-conditions') }}">Terms and condition</a> and the <a target="_blank" href="{{ url('privacy-policy') }}">Privacy Policy.</a>
            </li>
            <li>
                By entering the website, I am subjecting myself to the exclusive personal jurisdiction of the Netherlands should any dispute arise between the owner or operator of the website and myself in accordance with the <a href="{{ url('terms-conditions') }}">Terms and condition</a>.
            </li>
            <li>
                By entering the website, I am subjecting myself to binding arbitration in the Netherlands should any dispute arise at any time between the owner or operator of the website and myself in accordance with the <a href="{{ url('terms-conditions') }}">Terms and condition</a>.
            </li>
             <li>
                By entering the website, I will have released and discharged the providers, owners, and creators of the website from all liability that might arise.
            </li>
            <li>
              By entering the website, my personal data will be stored and processed, in accordance with the <a target="_blank" href="{{ url('privacy-policy') }}">Privacy Policy.</a>
            </li>
          </ul>
        </div>
        <div class="popup_footer_content">
          <p>
            By clicking Agree and continue, you state that all the above is true, that you want to enter the Website, and that you agree to the <a target="_blank" href="{{ url('terms-conditions') }}">Terms and conditions</a> &amp; <a target="_blank" href="{{ url('privacy-policy') }}">Privacy Policy</a>
          </p>
          <div class="popup_confirm_buttons">
            <button class="leave_btn buttons">Disagree and leave</button>
            <button class="confirm_btn buttons">Agree and continue</button>
          </div>
        </div>
      </div>
    </div>
  </div>
<!-- notice popup -->
<!-- confirm popup -->
<div class="n_conmfirm_popup open_popup" id="n_confrim_popup">
  <div class="modal_dialog_big">
    <div class="confirm_p_content">
      <div class="close_popup"><i class="fa fa-times"></i></div>
      <div class="confirm_content_wrap">
        <div class="cnfrm_right_img">
          <div class="cnfrm_img">
            <img src="public/images/confrim_img.png" alt="" class="img-responsive" />
          </div>
        </div>
        <div class="cnfrm_left_content">
          <div class="cnfrm_inner_cover">
            <h1 class="cong_heading">Congratulations!</h1>
            <h3 class="success_heading">You've successfully created your profile on <span>Flirtfull</span></h3>
            <div class="email_text">Open the email from us and click on the <span>Activate my account</span> button. </div>
            <div class="email_text_2">Your email is <span>eyesmfi@gmail.com</span></div>
            <div class="email_button">
              <a href="#">Go to your e-mail</a>
            </div>
            <div class="resend_mail_link">
              <a href="#">Resend activation e-mail</a>
            </div>
            <div class="bottom_p_notes">
              <h4>Important:</h4>
              <ul>
                <li>it can take up to 5 minutes for the activation email to drive</li>
                <li>Check your email account's 'Spam/Junk' folder to make sure our email hasn't landed there</li>
                <li>Make us as 'Not Spam/Junk' to ensure that you receive all important e-mail from <span>Flirtfull</span></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- confirm popup -->
      <script>
         $(document).ready(function() {
           var owl = $('#owl-carousel');
           owl.owlCarousel({
           loop:false,
           margin:10,
           nav:false,
           autoplay: true,
           autoplayTimeout: 1000,
           autoplayHoverPause: true,
           responsive:{
               0:{
                   items:1
               },
               600:{
                   items:3
               },
               1000:{
                   items:5
               }
           }    
         
           });
           $('.play').on('click', function() {
             owl.trigger('play.owl.autoplay', [1000])
           })
           $('.stop').on('click', function() {
             owl.trigger('stop.owl.autoplay')
           })
         })
      </script>
      <script>
         $('#owl-carousel1').owlCarousel({
         items:1,
         margin:10,
         autoHeight:true,
         autoplay: true,
         dots: true,
         });
      </script>
      <script>
    $(document).ready(function() {
      $("#log_click").click(function(){
        $("#log_popup").slideToggle();
        $("#sign_popup").slideUp();
        $("#forgot_popup").slideUp();
        
      });

      $("#forgot-link").click(function(){
        $("#forgot_popup").slideToggle();
        $("#sign_popup").slideUp();
        $("#log_popup").slideUp();
        
      });
        
        $("#sign_click").click(function(){
        $("#sign_popup").slideToggle();
        $("#log_popup").slideUp();
        $("#forgot_popup").slideUp();
        
      });
        
      });
       
    </script>
<script type="text/javascript">
  //for login
  function onSearch(){
    
    var gender = $("input:radio.gender_class:checked").val();
    var seeking = $("input:radio.seeking_class:checked").val();
    var err =0;

   
    if(gender == '' || typeof gender === "undefined")
    {
      $('.gender-wrong').show();
      $('.gender-wrong').text('Select gender');
      err++;
    }
    else{
      $('.gender-wrong').hide();
      $('.gender-wrong').text('');
    }
    
    if(seeking == '' || typeof seeking === "undefined")
    {
      $('.seeking-wrong').show();
      $('.seeking-wrong').text('Select seeking');
      err++;
    }
    else{
      $('.seeking-wrong').hide();
      $('.seeking-wrong').text('');
    }

    if(err == 0)
    { 
      return true;
    }
    else{
      return false;
    }
    
  }

</script>

<script type="text/javascript">
   /*var search_box = 'country_search_home';
   function initialize() {
      var input = document.getElementById(search_box);
      
      var componentForm = {
        street_number: 'short_name',
        route: 'long_name',
        locality: 'long_name',
        administrative_area_level_1: 'short_name',
        country: 'long_name',
        postal_code: 'short_name'
      };

      var options = {
        types: ['geocode'], //this should work !
        componentRestrictions: {country: "uk"}
      };
      var autocomplete = new google.maps.places.Autocomplete(input, options);

        google.maps.event.addListener(autocomplete, 'place_changed', function() {
            var place = autocomplete.getPlace();
    
    
            //alert("'"+place.formatted_address+"'");
            document.getElementById(search_box).value= place.formatted_address;


            //code for remove other locality
            for (var i = 0; i < place.address_components.length; i++) {
              var addressType = place.address_components[i].types[0];
              //console.log(place.address_components);
              if (componentForm[addressType]) { 
                var val = place.address_components[i][componentForm[addressType]];
                if(addressType == 'country'){
                //alert(addressType +'==='+val);
                  document.getElementById('country_home').value= val;
                }
              }
            }
            //code for remove other locality
        });
      
   }
   google.maps.event.addDomListener(window, 'load', initialize);*/

   
</script>
<script>
    getCity('United Kingdom'); 
    getRegion('United Kingdom'); 
    function getCity(name){
        //alert(name);
        $.ajax({
                headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '<?php echo url("/")?>/getCity',
                type: 'POST',
                cache : false,
                processData: false,
                data: 'name='+name, // serializes the form's elements.
                success: function(data)
                {
                 console.log(data);
                 $('#city_home').html(data);
                 $('#prof_city').html(data);
                }
            });
    }
  function getRegion(name){
        //alert(name);
        $.ajax({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      url: '<?php echo url("/")?>/getRegion',
      type: 'POST',
      cache : false,
      processData: false,
      data: 'name='+name, // serializes the form's elements.
      success: function(data)
      {
       console.log(data);
       $('#region_home').html(data);
       $('#prof_region1').html(data);
      }
    });
    }
function getCityByState(name){
        //alert(name);
        $.ajax({
                headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '<?php echo url("/")?>/getCityByState',
                type: 'POST',
                cache : false,
                processData: false,
                data: 'name='+name, // serializes the form's elements.
                success: function(data)
                {
                 console.log(data);
                 $('#city').html(data);
                 $('#prof_city').html(data);
         $('#city_home').html(data);
                }
            });
    }   
</script>
<script>
  $('.signup_button_n').click(function(e) {
    $("html, body").animate({
        scrollTop:0 
    }, 1000)
    e.preventDefault();
    $("#sign_popup").show();
});
  //notice popup open js
  $(window).on('load', function(){
  
  if(localStorage.getItem('popState') != 'shown')
  {
      setTimeout(function()
      {
        $(".notice_popup").addClass("open_popup");
      }, 3000);
        localStorage.setItem('popState','shown')
    } 
   
  });
  $(".confirm_btn").on('click', function(){
    $(this).parents(".notice_popup").removeClass("open_popup");
  });
   $(".leave_btn").on('click', function(){
   window.location.href='https://www.google.com/';
  });
  
</script>
    
<!-- Happy -- Mautic -->
  
  <script>
  function getUrlParameter(name) {
    name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
    var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
    var results = regex.exec(location.search);
    return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
  };
  
    (function(w,d,t,u,n,a,m){w['MauticTrackingObject']=n;
        w[n]=w[n]||function(){(w[n].q=w[n].q||[]).push(arguments)},a=d.createElement(t),
        m=d.getElementsByTagName(t)[0];a.async=1;a.src=u;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://emm.oddigits.com/mtc.js','mt');

  var email  = getUrlParameter('email');

  if(email !== ''){
    mt('send', 'pageview', {email: email});
  } else {
    mt('send', 'pageview');
  }
</script>

    
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-138205075-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-138205075-1');
</script>
    <script>
    //confirm popup js
    $(".close_popup").on('click', function(){
    $(this).parents(".n_conmfirm_popup").removeClass("open_popup");
    });

    $(".n_conmfirm_popup").on('click', function(){
    $(this).removeClass("open_popup");
    });
    $(".confirm_p_content").on('click', function(event){
    event.stopPropagation();
    });
    </script>
</body>
</html>