<div class="tab-pane" id="tab_1_2"> 
<div class="portlet light bordered">

	<div class="row">
    	<div class="form-group">
        	<label>Language</label> &nbsp;&nbsp;&nbsp;&nbsp;
        	<select>
            	<option>English</option>
                <option>Hindi</option>
            </select>
        </div>
    </div>
 
 	<div class="form-group {{ $errors->first('relationship', ' has-error') }}">
        <label class="control-label">Profile Type</label>        
        <select name="profile_type" class="form-control">
            <option value="Generic" {{ ($user->profile_type == 'Generic') ? 'selected' : ''}} >Generic</option>
            <option value="Mature" {{ ($user->profile_type == 'Mature') ? 'selected' : ''}}>Mature</option>
            <option value="Kinky" {{ ($user->profile_type == 'Kinky') ? 'selected' : ''}}>Kinky</option>
            <option value="Shemale" {{ ($user->profile_type == 'Shemale') ? 'selected' : ''}}>Shemale</option>            
        </select> 
    </div>
   
    <div class="form-group {{ $errors->first('about_me', ' has-error') }}">
        <label class="control-label">About Me</label>
        <textarea  placeholder="About Me" class="form-control" name="about_me">
        {{ ($user->about_me)?$user->about_me:old('about_me')}}</textarea>
   </div>
    
    <div class="form-group {{ $errors->first('seeking', ' has-error') }}">
        <label class="control-label ">Seeking</label>
        <input type="text" placeholder="Seeking" class="form-control" name="seeking" value="{{ ($user->seeking)?$user->seeking:old('seeking')}}"> 
    </div>
        
    <div class="form-group {{ $errors->first('about_partner', ' has-error') }}">
        <label class="control-label">About Partner</label>
        <textarea  placeholder="About Partner" class="form-control" name="about_partner">
        {{ ($user->about_partner)?$user->about_partner:old('about_partner')}}</textarea>
   </div>
    
    <div class="form-group {{ $errors->first('work_as', ' has-error') }}">
        <label class="control-label">Work As</label>
        <input type="text" placeholder="Work As" class="form-control" name="work_as" value="{{ ($user->work_as)?$user->work_as:old('work_as')}}"> 
    </div>

     <input type="hidden" value="3" name="role_type">
    
    <div class="form-group {{ $errors->first('education', ' has-error') }}">
        <label class="control-label">Education</label>
        <input type="text" placeholder="Education" class="form-control" name="education"  value="{{ ($user->education)?$user->education:old('education')}}">
    </div>
        
    <div class="form-group {{ $errors->first('relationship', ' has-error') }}">
        <label class="control-label">Relationship</label>        
        <select name="relationship" class="form-control">
            <option value="1" {{ ($user->relationship == '1') ? 'selected' : ''}} >Never Married</option>
            <option value="2" {{ ($user->relationship == '2') ? 'selected' : ''}}>Separated</option>
            <option value="3" {{ ($user->relationship == '3') ? 'selected' : ''}}>Divorced</option>    
        </select>
    </div>
        
    <div class="form-group {{ $errors->first('kids', ' has-error') }}">
        <label class="control-label">Kids</label>        
        <select name="kids" class="form-control">
            <option value="1" {{ ($user->kids == '1') ? 'selected' : ''}} >Yes</option>
            <option value="2" {{ ($user->kids == '2') ? 'selected' : ''}}>No</option>            
        </select>
    </div>    
    
    <div class="form-group {{ $errors->first('smoke', ' has-error') }}">
        <label class="control-label">Smoke</label>
        <select name="smoke" class="form-control">
            <option value="1" {{ ($user->smoke == '1') ? 'selected' : ''}} >Never</option>
            <option value="2" {{ ($user->smoke == '2') ? 'selected' : ''}}>Regularly</option>
            <option value="3" {{ ($user->smoke == '3') ? 'selected' : ''}}>Occassionally</option>    
        </select>
    </div>
        
    <div class="form-group {{ $errors->first('drink', ' has-error') }}">
        <label class="control-label">Drink</label>        
        <select name="drink" class="form-control">
            <option value="1" {{ ($user->drink == '1') ? 'selected' : ''}} >Never</option>
            <option value="2" {{ ($user->drink == '2') ? 'selected' : ''}}>Regularly</option>
            <option value="3" {{ ($user->drink == '3') ? 'selected' : ''}}>Occassionally</option>    
        </select> 
    </div>    
    
    <div class="form-group {{ $errors->first('height', ' has-error') }}">
        <label class="control-label">Height</label>
        <input type="text" placeholder="Height" class="form-control" name="height"  value="{{ ($user->height)?$user->height:old('height')}}">
    </div>
    
          
        
    <div class="form-group {{ $errors->first('languages', ' has-error') }}">
        <label class="control-label">Languages</label>
            <div class="row grey-box ">
            @foreach($languages as $row)
            <div class="check_box">
            <?php $checked = ''; ?>
            @if(in_array($row->id,explode(',',$user->languages)))
                <?php $checked = 'checked'; ?>
            @endif    
            <input type="checkbox" name="languages[]" {{ $checked }} value="{{ $row->id }}"> {{ $row->name }}
            </div>    
            @endforeach
            </div>
    </div>
    
    <div class="form-group {{ $errors->first('interests', ' has-error') }}">
        <label class="control-label">Interests</label>
            <div class="row grey-box">
            @foreach($interests as $row)
            <div class="check_box">
            <?php $checked1 = ''; ?>
            @if(in_array($row->id,explode(',',$user->interests)))
                <?php $checked1 = 'checked'; ?>
            @endif    
            <input type="checkbox" name="interests[]" {{ $checked1 }} value="{{ $row->id }}"> {{ $row->name }}
            </div>    
            @endforeach
            </div>
    </div>        
          
    <div class="row"> 
        <div class="col-md-6"> <button type="submit" class="btn green btn-primary" value="avtar" name="submit"> Save Changes </button></div>
         <div class="col-md-6"><button type="reset" class="btn default"> Cancel </button></div>
    </div>      
             
</div>
</div>
 