@extends('admin::layouts.master')
 
    @section('content') 
      @include('admin::partials.navigation')
      @include('admin::partials.breadcrumb')   

       @include('admin::partials.sidebar')  
            <div class="panel panel-white"> 
                <div class="panel panel-flat">
                  <div class="panel-heading">
                    <h6 class="panel-title"><b> {{$heading }} </b><a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
                    <div class="heading-elements">
                      <ul class="icons-list">
                        <li> <a type="button" href="{{route('faq.create')}}" class="btn btn-primary text-white btn-labeled btn-rounded "><b><i class="icon-plus3"></i></b> Add Faq<span class="legitRipple-ripple" ></span></a></li> 
                      </ul>
                    </div>
                  </div> 
                </div>  
                <div class="panel-body"> 
                 
              </div> 
                 @if(Session::has('flash_alert_notice'))
                     <div class="alert alert-success alert-dismissable" style="margin:10px">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                      <i class="icon fa fa-check"></i>  
                     {{ Session::get('flash_alert_notice') }} 
                     </div>
                @endif
                 <div class="table-responsive">
                  <table class="table datatable-basic table-bordered table-hover table-responsive" id="roles_list">
                                   
                                        <thead>
                                            <tr>
                                                <th> S. no. </th>
                                                <th> Question </th>
                                                <th> Answer</th>
                                                <th>Action</th> 
                                            </tr>
                                        </thead>
                                        <tbody>

                                    
                                        @foreach($faqs as $key => $result)
                                            <tr>
                                                 <td> {{ (($faqs->currentpage()-1)*15)+(++$key) }}</td>
                                                <td> {{  $result->question }} </td>
                                                <td> {{ substr($result->answer,0,50) }} </td>
                                                
                                                <td> 
                                                    <a href="{{ route('faq.edit',$result->id)}}" class="btn btn-primary btn-xs" style="margin: 3px">
                            <i class="icon-pencil7" title="edit"></i>  
                            </a> 

                            {!! Form::open(array('class' => 'form-inline pull-left deletion-form', 'method' => 'DELETE',  'id'=>'deleteForm_'.$result->id, 'style'=>'margin:3px', 'route' => array('faq.destroy', $result->id))) !!}

                            <button class='delbtn btn btn-danger btn-xs' type="submit" name="remove_levels" value="delete" id="{{$result->id}}" onclick=" var c = confirm('Are you sure want to delete?'); if(!c) return false;"><i class="icon-trash" title="Delete"></i> 
                            </button> 
                            {!! Form::close() !!} 

                                                </td>
                                               
                                            </tr>
                                           @endforeach
                                        
                                        </tbody>
                                    </table>
                                    Showing {{($faqs->currentpage()-1)*$faqs->perpage()+1}} to {{$faqs->currentpage()*$faqs->perpage()}}
					                of  {{$faqs->total()}} entries
					                 <div class="center" align="center">  {!! $faqs->appends(['gender' => isset($_GET['gender'])?$_GET['gender']:'', 'country' => isset($_GET['country'])?$_GET['country']:'' ,'age' => isset($_GET['age'])?$_GET['age']:'', 'photo' => isset($_GET['photo'])?$_GET['photo']:'', 'relationship' => isset($_GET['relationship'])?$_GET['relationship']:'' , 'profile_type' => isset($_GET['profile_type'])?$_GET['profile_type']:'' , 'adult_level' => isset($_GET['adult_level'])?$_GET['adult_level']:'' , 'appearance' => isset($_GET['appearance'])?$_GET['appearance']:'' ,
                                     'last_part' => isset($_GET['last_part'])?$_GET['last_part']:'' , 'no_of_pictures' => isset($_GET['no_of_pictures'])?$_GET['no_of_pictures']:'' , 'status' => isset($_GET['status'])?$_GET['status']:''])->render() !!}</div>    
                                
                 </div> 
               </div>
         </div> 
   @stop
   
   
   