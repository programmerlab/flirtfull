<?php

namespace App\Http\Controllers\Auth;

use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Validator;
use Response;
use DB;
use Session;
use Hash;
use DateTime;
use App\User;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/search';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request)
    {   //print_r($request->all());die;
        // validate the info, create rules for the inputs
        $rules = array(
            'email'    => 'required|email', // make sure the email is an actual email
            'password' => 'required' // password can only be alphanumeric and has to be greater than 3 characters
        );

        // run the validation rules on the inputs from the form
        $validator = Validator::make(Input::all(), $rules);

        // if the validator fails, redirect back to the form
        if ($validator->fails()) {
            echo json_encode(array(
                'success' => false,
                'errors' => $validator->getMessageBag()->toArray()

            )); // 400 being the HTTP code for an invalid request.
        } else {

            // create our user data for the authentication
            $userdata = array(
                'email'     => Input::post('email'),
                'password'  => Input::post('password')
                
            );

            $user_id = Auth::id();

            //$user_status = $user_id = Auth::user()->status;
           
            // attempt to do the login
            $last_login = date('Y-m-d H:i:s');
	    $session_id = Session::getId();
            DB::enableQueryLog();
            if (Auth::attempt($userdata,$request->has('remember'))) {
                if(Auth::check() && Auth::user()->is_deleted == 0)
                {
                    User::where('id',Auth::id())->update(['last_login'=>$last_login,'is_login' => 1,'session_id'=>$session_id,'login_time'=>strtotime($last_login)]);
                    $user_id = Auth::id();
                    return response()->json(['redirect' => true, 'success' => true,'user_id' => $user_id], 200);die;
                }
                else{
                    echo json_encode((array('errors' => 'Please Confirm Your Email Address.',400)));die;
                }

            } 
           
            else {   
                //dd(DB::getQueryLog() );
      
                // validation not successful, send back to form 
                echo json_encode((array('errors' => 'You entered wrong credentials.',400)));

                //echo "validation not successful";;

            }

        }
    }


    public function logout(Request $request){
        
        User::where('id',Auth::id())->update(['is_login' => 0]);
        $user_id = User::where('id',Auth::id())->first();
        /*if($user_id->is_deleted == 1)
        {
            session()->flash('success', 'Your account has been successfully deleted !!');
        }*/
        Auth::logout();
        Session::flush();
        return redirect('/');
    }
}
