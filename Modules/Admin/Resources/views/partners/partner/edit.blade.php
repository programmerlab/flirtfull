@extends('admin::layouts.master')
 
    @section('content') 
      @include('admin::partials.navigation')
      @include('admin::partials.breadcrumb')   

       @include('admin::partials.sidebar')  
                        <div class="panel panel-white"> 

 
                                <div class="panel panel-flat">
                                              <div class="panel-heading">
                                            <h6 class="panel-title"><b> {{$heading ?? ''}}</b><a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
                                            <div class="heading-elements">
                                              <ul class="icons-list">
                                                <li> <a type="button" href="{{route('partner')}}" class="btn btn-primary text-white   btn-rounded "> View Partners<span class="legitRipple-ripple" ></span></a></li> 
                                              </ul>
                                            </div>
                                          </div> 
                                    </div>
                                    <div class="panel-body">
                                    {!! Form::model($partner, ['method' => 'PATCH', 'route' => ['partner.update', $partner->id],'enctype'=>'multipart/form-data','class'=>
                                                  'form-basic ui-formwizard partner-form']) !!}
                                                  
                                           <div class="form-group {{ $errors->first('name', ' has-error') }}">
                                                <label class="control-label col-md-3">Name <span class="required"> * </span></label>
                                                <div class="col-md-12"> 
                                                    {!! Form::text('name',null, ['class' => 'form-control','data-required'=>1])  !!} 
                                                    
                                                    <span class="help-block">{{ $errors->first('name', ':message') }}</span>
                                                </div>
                                            </div>
                                            
                                            <div class="form-group {{ $errors->first('contact', ' has-error') }}">
                                                <label class="control-label col-md-3">Contact<span class="required"> * </span></label>
                                                <div class="col-md-12"> 
                                                    {!! Form::text('contact',null, ['class' => 'form-control','data-required'=>1])  !!} 
                                                    
                                                    <span class="help-block">{{ $errors->first('contact', ':message') }}</span>
                                                </div>
                                            </div>
                                                
                                                
                                             <div class="form-group {{ $errors->first('commission', ' has-error') }}">
                                                <label class="control-label col-md-3">Commission (in %)<span class="required"> * </span></label>
                                                <div class="col-md-12"> 
                                                    {!! Form::text('commission',null, ['class' => 'form-control','data-required'=>1])  !!} 
                                                    
                                                    <span class="help-block">{{ $errors->first('commission', ':message') }}</span>
                                                </div>
                                            </div>
                                            
                                            <div class="form-group {{ $errors->first('partnerid', ' has-error') }}">
                                                <label class="control-label col-md-3">Partner Id (unique partner Id)<span class="required"> * </span></label>
                                                <div class="col-md-12"> 
                                                    {!! Form::text('partnerid',null, ['class' => 'form-control','data-required'=>1,'disabled'=>'true'])  !!} 
                                                    
                                                    <span class="help-block">{{ $errors->first('partnerid', ':message') }}</span>
                                                </div>
                                            </div>
                                            
                                           <div class="col-md-12">
                                                  <div class="margin-top-10"> 
                                                       <button type="submit" class="btn green btn-primary" value="avtar" name="submit"> Save Changes </button>
                                                      <button type="reset" class="btn default "> Cancel </button>
                                                  </div> 
                                                 
                                           </div>
                                    </form>        
                                    </div>                                                                                          
                        </div>
                              
                           
                     
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
            
            
            <!-- END QUICK SIDEBAR -->
        </div>
        

        
@stop