<?php

declare(strict_types=1);

namespace Modules\Admin\Http\Controllers;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Redirect;
use Input;
use Modules\Admin\Models\Template;
use Route;
use View;
use Session;
use Validator;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Datatables;


/**
 * Class AdminController
 */
class TemplateController extends Controller
{
    /**
     * @var  Repository
     */

    /**
     * Displays all admin.
     *
     * @return \Illuminate\View\View
     */
    public function __construct()
    {
        $this->middleware('admin');
        View::share('viewPage', 'Template');
        View::share('helper', new Helper);
        View::share('heading', 'Email Template');
        View::share('route_url', route('template'));

        $this->record_per_page = Config::get('app.record_per_page');
    }

    protected $template;

    /*
     * Dashboard
     * */

    public function index(Template $template, Request $request)
    {        
        $page_title  = 'Email Template';
        $page_action = 'Email Template';
        

        $template = Template::orderBy('id', 'desc')->get();
        //print_r($credits); die;        

        $js_file = ['common.js','bootbox.js','formValidate.js'];

        return view('admin::templates.template.index', compact('js_file', 'status', 'template', 'page_title', 'page_action'));
    }


    public function anyData()
    {
        return Datatables::of(Credit::query())->make(true);
    }

    public function test_table(Credit $credit, Request $request)
    {        
        $page_title  = 'Admin Credit';
        $page_action = 'Admin Credit';
        

        if ($request->ajax()) {
            $id           = $request->get('id');
            $status       = $request->get('status');
            $credit         = Credit::find($id);
            $s            = ($status == 1) ? $status=0:$status=1;
            $credit->status = $s;
            $credit->save();
            echo $s;

            exit();
        }
        // Search by name ,email and group
        $search    = Input::get('search');
        $status    = Input::get('status');
        $role_type = Input::get('role_type');

        if ((isset($search) && !empty($search)) or  (isset($status) && !empty($status)) or !empty($role_type)) {
            $search = isset($search) ? Input::get('search') : '';

            $credits = Credit::where(function ($query) use ($search,$status,$role_type) {
                if (!empty($search)) {
                    $query->Where('first_name', 'LIKE', "%$search%")
                        ->OrWhere('last_name', 'LIKE', "%$search%")
                        ->OrWhere('email', 'LIKE', "%$search%");
                }

                if (!empty($status)) {
                    
                    $status =  ($status == 'active')?1:0;
                    $query->Where('status', $status);
                }

                if ($role_type) {
                    $query->Where('role_type', $role_type);
                }
            })->where('role_type', 3)->Paginate($this->record_per_page);
        } else {
            $credits = Credit::orderBy('id', 'desc')->where('role_type', 3)->where('is_deleted', 0)->Paginate($this->record_per_page);
        }
        $roles = Roles::all();

        $js_file = ['common.js','bootbox.js','formValidate.js'];

        return view('admin::credits.credit.test_table', compact('js_file', 'roles', 'status', 'page_title', 'page_action', 'roles' ,'role_type'));
    }

    /*
     * create Group method
     * */

    public function create(Credit $credit)
    {
        $page_title  = 'Credit';
        $page_action = 'Create Credit';
        
        $js_file     = ['common.js','bootbox.js','formValidate.js'];


        return view('admin::credits.credit.create', compact('js_file', 'credit', 'page_title', 'page_action'));
    }

    /*
     * Save Group method
     * */

    public function store(CreditRequest $request, Credit $credit)
    {
        //echo "<pre>"; print_r(Input::all()); die;
        $credit->fill(Input::all());
        
        
        $action = $request->get('submit');        
        
        
        $credit->save();
        
        $js_file = ['common.js','bootbox.js','formValidate.js'];

        return Redirect::to(route('credit'))
            ->with('flash_alert_notice', 'New record successfully created.');
    }

    /*
     * Edit Group method
     * @param
     * object : $credit
     * */

    public function edit(Template $template)
    {
        //echo "<pre>"; print_r($credit); die;
        $page_title  = 'Template';
        $page_action = 'Show Template';                
        
        $js_file     = ['common.js','bootbox.js','formValidate.js'];
                
        //echo "<pre>"; print_r($template); die;
        return view('admin::templates.template.edit', compact('js_file', 'template', 'page_title', 'page_action'));
    }

    public function update(Request $request, Template $template)
    {        
        $rules = [
                'template_name' => 'required',
                'email_subject' => 'required',
                'email_message' => 'required',
            ];

        $messages = [
            'template_name.required' => 'Please Enter Template Name.',
            'email_subject.required' => 'Please Enter Email Subject.',
            'email_message.required' => 'Please Enter Email Message.',
            
        ];
        $validator = Validator::make($request->all(),$rules,$messages);

        if ($validator->fails()) {
          return redirect()->back()->withErrors($validator)->withInput();
        }        
        
        $template->fill(Input::all());
        
        $template->save();

        return Redirect::to(route('template'))
            ->with('flash_alert_notice', 'Template was successfully updated.');
    }
    
    
    
}
