<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

class BlogController extends Controller{
	public function index(){
		return view('blogview');
	}
	
	public function uploadfile(Request $request){
		  $request->validate([
            'fileToUpload' => 'required|file|max:1024',
        ]);

        $fileName = "fileName".time().'.'.request()->fileToUpload->getClientOriginalExtension();

        $request->fileToUpload->storeAs('logos',$fileName);

        return back()
            ->with('success','You have successfully upload image.');
	}
}