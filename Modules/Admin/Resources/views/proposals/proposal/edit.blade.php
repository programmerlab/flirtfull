@extends('admin::layouts.master')
 
    @section('content') 
      @include('admin::partials.navigation')
      @include('admin::partials.breadcrumb')   

       @include('admin::partials.sidebar')  
                        <div class="panel panel-white"> 

 
                                <div class="panel panel-flat">
                                              <div class="panel-heading">
                                            <h6 class="panel-title"><b> {{$heading ?? ''}}</b><a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
                                            <div class="heading-elements">
                                              <ul class="icons-list">
                                                <li> <a type="button" href="{{route('proposal')}}" class="btn btn-primary text-white   btn-rounded "> View Proposal category<span class="legitRipple-ripple" ></span></a></li> 
                                              </ul>
                                            </div>
                                          </div> 
                                    </div>
                                    <div class="panel-body">
                                    {!! Form::model($proposal, ['method' => 'PATCH', 'route' => ['proposal.update', $proposal->id],'enctype'=>'multipart/form-data','class'=>
                                                  'form-basic ui-formwizard proposal-form']) !!}
                                                  
                                           <div class="form-group {{ $errors->first('category_id', ' has-error') }}">
                                                <label class="control-label col-md-3">Category <span class="required"> * </span></label>
                                                <div class="col-md-12">                                                     
                                                    <select name="category_id" class="form-control">
                                                        @foreach($category as $row)
                                                        <option value="{{$row->id}}" {{ ($proposal->category_id == $row->id) ? 'selected' : '' }}>{{ $row->title }}</option>
                                                        @endforeach    
                                                   </select>
                                                    <span class="help-block">{{ $errors->first('category_id', ':message') }}</span>
                                                </div>
                                            </div>
                                                
                                            <div class="form-group {{ $errors->first('image', ' has-error') }}">
                                                <label class="control-label col-md-3">Image <span class="required"> * </span></label>
                                                <div class="col-md-12"> 
                                                    {!! Form::file('image',null, ['class' => 'form-control','data-required'=>1])  !!} 
                                                    
                                                    <span class="help-block">{{ $errors->first('image', ':message') }}</span>
                                                    <img src="http://35.242.157.120/public/images/profile_image/{{$proposal->image}}" width="100">    
                                                </div>
                                            </div>
                                                
                                            <div class="form-group {{ $errors->first('status', ' has-error') }}">
                                                <label class="control-label col-md-3">Status <span class="required"> * </span></label>
                                                <div class="col-md-12">                                                     
                                                    <select name="status" class="form-control">
                                                       <option value="1" {{ ($proposal->status == 1) ? 'selected' : '' }}>Active</option>
                                                       <option value="0" {{ ($proposal->status == 0) ? 'selected' : '' }}>Inactive</option>	            
                                                   </select>
                                                    <span class="help-block">{{ $errors->first('status', ':message') }}</span>
                                                </div>
                                            </div>                                                         
                          
                              
                                           <div class="col-md-12">
                                                  <div class="margin-top-10"> 
                                                       <button type="submit" class="btn green btn-primary" value="avtar" name="submit"> Save Changes </button>
                                                      <button type="reset" class="btn default "> Cancel </button>
                                                  </div> 
                                                 
                                           </div>
                                    </form>        
                                    </div>                                                                                          
                        </div>
                              
                           
                     
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
            
            
            <!-- END QUICK SIDEBAR -->
        </div>
        

        
@stop