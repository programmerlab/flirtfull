<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">

<head>
    <title></title>
    <!--[if !mso]>-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!--<![endif]-->
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style type="text/css">
        #outlook a {
            padding: 0;
        }
        
        .ReadMsgBody {
            width: 100%;
        }
        
        .ExternalClass {
            width: 100%;
        }
        
        .ExternalClass * {
            line-height: 100%;
        }
        
        body {
            margin: 0;
            padding: 0;
            -webkit-text-size-adjust: 100%;
            -ms-text-size-adjust: 100%;
        }
        
        table,
        td {
            border-collapse: collapse;
            mso-table-lspace: 0pt;
            mso-table-rspace: 0pt;
        }
        
        img {
            border: 0;
            height: auto;
            line-height: 100%;
            outline: none;
            text-decoration: none;
            -ms-interpolation-mode: bicubic;
        }
        
        p {
            display: block;
            margin: 13px 0;
        }
    </style>
    <!--[if !mso]><!-->
    <style type="text/css">
        @media  only screen and (max-width:480px) {
            @-ms-viewport {
                width: 320px;
            }
            @viewport  {
                width: 320px;
            }
        }
    </style>
    <!--<![endif]-->
    <!--[if mso]><xml>  <o:OfficeDocumentSettings>    <o:AllowPNG/>    <o:PixelsPerInch>96</o:PixelsPerInch>  </o:OfficeDocumentSettings></xml><![endif]-->
    <!--[if lte mso 11]><style type="text/css">  .outlook-group-fix {    width:100% !important;  }</style><![endif]-->
    <!--[if !mso]><!-->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,700" rel="stylesheet" type="text/css">
    <style type="text/css">
        @import  url(https://fonts.googleapis.com/css?family=Baloo|Montserrat:300,400,500,700);
    </style>
    <!--<![endif]-->
    <style type="text/css">
        @media  only screen and (min-width:480px) {
            .mj-column-per-100 {
                width: 100%!important;
            }
        }
    </style>
</head>

<body style="background: #DF2358;">
    <div class="mj-container" style="background-color:#DF2358;">
      <table width="100%" cellpadding="0" cellspacing="0">
        <tbody>
          <tr>
            <td height="20"></td>
          </tr>
        </tbody>
        </table>
        <div style="margin: 0px auto; max-width:600px; background:#FFFFFF; font-family: 'Montserrat', sans-serif;">
            <div style="max-width: 600px; margin: 0px auto; background:#fff; position:relative;">
                <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                  <td>
                  <table align="center">
                    <tr>
                      <td height="10"></td>
                    </tr>
                    <tr>
                      <td></td>
                       <td style="width:204px;">
                            <img alt height="auto" src="https://www.flirtfull.com/public/images/logo.png" style="border:none;border-radius:0px;display:block;font-size:13px;outline:none;text-decoration:none;width:100%;height:auto;" width="204">
                        </td>
                    </tr>
                    <tr>
                      <td height="10"></td>
                    </tr>
                  </table>
                  </td>
                </tr>
                </table>
            </div>
        </div>
        <div style="max-width: 600px; margin: 0 auto; position:relative; background-image:url('https://flirtfull.com/public/images/tablebanner.jpg'); background-repeat:no-repeat; background-size:cover; background-position: center;">
            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td>
                      <table width="100%">
                        <tr><td height="28"></td></tr>
                        <tr>
                          <td>
                            <p style="color:#ffffff; font-family:Ubuntu, Helvetica, Arial, sans-serif; font-size:20px; text-align:right; margin-bottom: 0; margin-top: 0; ">Dear [user_name],</p>
                          </td>
                        </tr>
                        <tr><td height="28"></td></tr>
                      </table>
                    </td>
                </tr>
            </table>
        </div>
        <div style="max-width: 600px; margin: 0 auto; background:#fff; position:relative; word-break: break-word;">
          <table align="center" style="max-width:550px; margin:0 auto; word-break: break-word;">
        <tr>
          <td height="50"></td>
        </tr>
        <tr>
          <td>
            <h4 style=" text-align:center; font-weight:300; font-size:18px; margin:0px; color: #000000; font-family: Ubuntu, Helvetica, Arial, sans-serif; ">Thank you for your registration</h4>
          </td>
        </tr>
        <tr>
          <td height="10"></td>
        </tr>
        <tr>
          <td>
          <h4  style="text-align:center; font-weight:300; font-size: 15px; line-height: 23px; margin:0 0 15px; 
          color: #555; font-family: Ubuntu, Helvetica, Arial, sans-serif;">
          To activate your account, please click the button below to confirm your email address and get started. 
          </h4> 
          </td>
        </tr>
        <tr>
          <td align="center">
          <a href="[verification_link]" style="text-decoration: none; display:inline-block; padding: 12px 36px;background: #EC155A; color: #fff; border-radius: 2em; font-family: Ubuntu, Helvetica, Arial, sans-serif, Helvetica, Arial, sans-serif;font-size: 16px; text-transform: capitalize;">Activate my account now</a>
          </td>
        </tr>
        <tr>
          <td height="30"></td>
        </tr>
        <tr>
          <td style="border-top: 1px solid #000; height: 1px;">
          </td>
        </tr>
        <tr>
          <td height="20"></td>
        </tr>
        <tr>
          <td>
            <h4  style="text-align:center; font-weight:300; font-size: 15px; line-height: 23px; margin:0 0 15px; 
            color: #555; font-family: Ubuntu, Helvetica, Arial, sans-serif;">
            if you have trouble to click on button to activate your account then please copy this url to your browser.
            </h4> 
            </td>
        </tr>
        <tr>
          <td align="center">
            <a href="#" style="font-size: 10px; color: #EC155A; display: block; text-align: center; line-height: 24px; font-family: Ubuntu, Helvetica, Arial, sans-serif; word-break: break-word;">
              [verification_link]
            </a>
          </td>
        </tr>
        <tr>
          <td height="15"></td>
        </tr>
        <tr>
          <td align="center">
            <h4 style=" margin:0; color: #000000; font-weight: 600; font-size: 16px; font-family: Ubuntu, Helvetica, Arial, sans-serif;">Regards</h4>
          </td>
        </tr>
        <tr>
          <td height="5"></td>
        </tr>
        <tr>
          <td align="center">
            <p style="margin:0; color: #000000; font-weight: 500; font-size: 14px; font-family: Ubuntu, Helvetica, Arial, sans-serif;">Team 
              <a style="color: #000000; font-weight: 500; text-decoration: none;" href="[site_url]">flirtfull.com</a></p>
          </td>
        </tr>
        <tr>
          <td height="40"></td>
        </tr>
          </table>
        </div>
        <div style="max-width: 600px; margin: 0 auto; position:relative; font-family: 'Montserrat', sans-serif; word-break: break-word;">
           <table style="width:100%;">
              <tr>
                <td height="10"></td>
              </tr>
              <tr>
                <td>
                  <p style="margin:0; color: #ffffff; font-size: 13px; line-height: 22px; word-wrap:break-word;">Copyright &#xA9; 2019 <a style="color:#ffffff;"href="">Flirtfull.com</a>, All rights reserved.</p>
                 </td>
              </tr>
              <tr>
                 <td>
                  <p style="margin:0; color:#ffffff; font-size: 13px; line-height: 22px; word-wrap:break-word;">You subscribed to our newsletter via our website, <a href="#" style="color:#ffffff; text-decoration: none;">flirtfull.com</a></p>
                 </td>
              </tr>
               <tr>
                 <td>
                  <a draggable="false" href="" style="margin:0; color: #ffffff; font-size: 13px; line-height: 22px; word-wrap:break-word;">Unsubscribe from this list</a>
                 </td>
              </tr>
              <tr>
                 <td height="10"></td>
              </tr>
            </table>
        </div>
        <!--[if mso | IE]></td></tr></table><![endif]-->
    </div>
</body>
</html>