<h3>chat api</h3>
    <script src="{{ asset('public/js/jquery.min.js') }}"></script>
    <script src="{{ asset('public/js/bootstrap.min.js') }}"></script>
    
    <script src="https://cdn.socket.io/socket.io-1.0.0.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/socket.io/1.7.4/socket.io.min.js"></script>
    
    <script>
        
        //console.log(postval);
        var socket = io.connect("http://35.242.157.120:9090");

        

        socket.on("connect", function() {
            // var params = jQuery.deparam(window.location.search);
            var params = { name: jQuery('#sender_name').val(), room: jQuery('#room_id').val() };

            socket.emit("join", params, function(err) {
                console.log("emited");
            });
                        
            console.log(params);
            console.log("Connected to Server");
                        
        });

        socket.on("updateUserList", function(users) {
            var ol = jQuery("<ol></ol>");
            users.forEach(function(user) {
                ol.append(jQuery("<li></li>").text(user));
            });

            jQuery("#users").html(ol);
        });
        

        socket.on("disconnect", function() {
            console.log("Disconnected from Server");
        });
        
        socket.emit(
            "createMessage",
            {
                text: 'how are you today?',
                senderName: 'Luci Lovett',
                receiverName: 'Brielle Woods',
                senderId: '3',
                receiverId: '14',
                chatID: '144831563',
                type : "text", 
                readStatus: false,
                answereStatus: false,
                senderImageUrl: '',
                receiverImageUrl: ''
            },
            function() {
                
            }
        );
</script>
    
    
    