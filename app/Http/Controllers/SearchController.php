<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use Validator;
use Session;
use Hash;
use Auth;
use Illuminate\Support\Facades\Input;
use Excel;
use App\Models\Search;
use Carbon\Carbon;
use DateTime;
use Image;
use Helper;
use App\User;

class SearchController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $search;


    public function __construct(Request $request,Search $search)
    {
        error_reporting(env('ERROR_SHOW'));
        ini_set('display_errors', env('ERROR_SHOW'));
        $this->middleware('web');
        $this->search = $search;
        header("cache-Control: no-store, no-cache, must-revalidate");
        header("cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");
    
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
   
    public function index(Request $request)
    {
		$reslt = '0';
        if(isset($_GET['partner']) && ($_GET['partner'] != '')){
            $reslt = $this->signup();
        }
		$adminid = DB::table('users')->where('id',3077)->first();
        
        $min_age = $request->min_age;
        $max_age = $request->max_age;        
        $state = trim($request->state);        
        $seeking = $request->seeking;        
        $online = $request->get('online');        
        
        $flag = 0;
        
        //print_r($request->all());die;
        DB::enableQueryLog();
        $seeking_val = (isset(Auth::user()->seeking_val)) ? Auth::user()->seeking_val : 2;
        
        if(Auth::check() && Auth::user()->status == 1){
            
            $users = DB::table('users')->select('users.*')
                    ->where('users.status','=',1)->where('users.is_deleted','=',0)->where('users.id','!=',Auth::id());
            
            $perpage = 30;
        }else{
            
            $users = DB::table('users')->select('users.*')
                    ->where('users.status','=',1)->where('users.is_deleted','=',0);
            
            $perpage = 29;
        }
                

        if (!empty($seeking)) {
            $users->where('users.gender','=',$seeking);
            $flag = 1;
        }

        if (!empty($min_age) && !empty($max_age)) {
            $users->whereBetween('users.age',[$min_age, $max_age]);
            $flag = 1;
        }
        
        
        if (!empty($state)) {
            $users->where('users.state','LIKE','%'.$state.'%');
            $flag = 1;
        }
                

        if (!empty($online)) {
            $users->where('users.is_login','=',$online);
            $flag = 1;
        }

        if (Auth::check() && $seeking == '') {                
            $login = DB::table('users')->select('users.*','user_details.*')
                ->leftJoin('user_details', 'user_details.user_id', '=', 'users.id')->where('users.id','=',Auth::id())->first();        
            $users->where('users.gender','=',$login->seeking_val);
        }            
        $all_users = $users->orderByRaw("RAND()")->paginate($perpage)->onEachSide(4);                    
        
        $users = DB::table('users')->select('users.*')->where('users.id', '=',Auth::id())->first();                                
        
        if ($request->ajax()) {			
            $view = view('partial/all_users',compact('all_users','seeking','min_age','max_age','state','online'))->render();
            return response()->json(['html'=>$view]);
        }
        
        
        if(Session::has('resendEmail')){
            Session::put('resendEmail','2');
        }else{
            Session::put('resendEmail','1');
        }
        
        if(Session::has('regionstatus')){
            Session::put('regionstatus','2');
        }else{
            Session::put('regionstatus','1');
        }
        
        
        if(Session::has('namestatus')){
            Session::put('namestatus','2');
        }else{
            Session::put('namestatus','1');
        }
        
        if(Session::has('namepasswordstatus')){
            Session::put('namepasswordstatus','2');
        }else{
            Session::put('namepasswordstatus','1');
        }
        
        if(Session::has('ageregionstatus')){
            Session::put('ageregionstatus','2');
        }else{
            Session::put('ageregionstatus','1');
        }
        
        return view('search',['all_users' => $all_users,'users'=>$users, 'rand'=>$reslt,'adminid'=>$adminid]);
    }
    
    public function update_profile(Request $request){
        if($request->get('region')){
            $userid = Auth::id();
            DB::table('users')->where('id',$userid)->update(array("state"=>$request->get('region')));
            
            $userwallet = DB::table('user_wallet')->where('user_id',$userid)->first();
            if($userwallet){
               $diamonds =  $userwallet->diamonds + 1;
               DB::table('user_wallet')->where('user_id',$userid)->update(array("diamonds"=>$diamonds)); 
            }else{
               $wallet = ["user_id"=>$userid, "diamonds"=>1]; 
               DB::table('user_wallet')->insertGetId($wallet);
            }
            echo 1;
        }
        
        if($request->get('profilename')){
            $userid = Auth::id();
            DB::table('users')->where('id',$userid)->update(array("name"=>$request->get('profilename')));
                    
                    $user = DB::table('users')->where('id',$userid)->first();
                    //register user to third party chat
                    $dataaa = [
                                 "username"=>"Moneo",
                                 "password"=>"Moneo2019@",
                                 "service_code"=>"cd9271fa",
                                 "name"=> $user->name.'__'.$user->id,
                                 "group"=> "subscriber",
                                 "profile"=> [
                                                "avatar"=> "avatar.jpg",
                                                "age"=> $user->age,
                                                "location"=> $user->state,
                                                "images"=> [$user->profile_image]
                                             ],
                                  "additional_info"=> [
                                                "membership"=> "free",
                                                "link"=> "http://xyz.com",
                                                "about"=> "about"
                                             ],                                            
                                 ];
                        $ch = curl_init();
                        curl_setopt($ch, CURLOPT_URL,"https://im.operatorplatform.com/api/users/add");
                        curl_setopt($ch, CURLOPT_POST, 1);
                        curl_setopt($ch, CURLOPT_POSTFIELDS,
                                    "data=".json_encode($dataaa));
                        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
                        
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                
                        $server_output = curl_exec ($ch);
                        //echo "<pre>"; print_r($dataaa); print_r($server_output);
                        curl_close ($ch);
                        
                    $dataaa1 = [
                                 "username"=>"Moneo",
                                 "password"=>"Moneo2019@",
                                 "service_code"=>"cd9271fa",
                                 "name"=> $user->name.'__'.$user->id,
                                 "group"=> "persona",
                                 "profile"=> [
                                                "avatar"=> "avatar.jpg",
                                                "age"=> $user->age,
                                                "location"=> $user->state,
                                                "images"=> [$user->profile_image]
                                             ],
                                  "additional_info"=> [
                                                "membership"=> "free",
                                                "link"=> "http://xyz.com",
                                                "about"=> "about"
                                             ],                                            
                                 ];
                        $ch = curl_init();
                        curl_setopt($ch, CURLOPT_URL,"https://im.operatorplatform.com/api/users/add");
                        curl_setopt($ch, CURLOPT_POST, 1);
                        curl_setopt($ch, CURLOPT_POSTFIELDS,
                                    "data=".json_encode($dataaa1));
                        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
                        
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                
                        $server_output = curl_exec ($ch);
                        //print_r($dataaa1); print_r($server_output);
                        curl_close ($ch);    
                    //register user to third party chat                    
            
            $userwallet = DB::table('user_wallet')->where('user_id',$userid)->first();
            if($userwallet){
               $diamonds =  $userwallet->diamonds + 1;
               DB::table('user_wallet')->where('user_id',$userid)->update(array("diamonds"=>$diamonds)); 
            }else{
               $wallet = ["user_id"=>$userid, "diamonds"=>1]; 
               DB::table('user_wallet')->insertGetId($wallet);
            }
            echo 1;
        }
        
        if($request->get('name') && $request->get('password')){
            $userid = Auth::id();
            DB::table('users')->where('id',$userid)->update(array("name"=>$request->get('name'),"password"=>bcrypt($request->get('password'))));                        
                    
                    $user = DB::table('users')->where('id',$userid)->first();
                    //register user to third party chat
                    $dataaa = [
                                 "username"=>"Moneo",
                                 "password"=>"Moneo2019@",
                                 "service_code"=>"cd9271fa",
                                 "name"=> $user->name.'__'.$user->id,
                                 "group"=> "subscriber",
                                 "profile"=> [
                                                "avatar"=> "avatar.jpg",
                                                "age"=> $user->age,
                                                "location"=> $user->state,
                                                "images"=> [$user->profile_image]
                                             ],
                                  "additional_info"=> [
                                                "membership"=> "free",
                                                "link"=> "http://xyz.com",
                                                "about"=> "about"
                                             ],                                            
                                 ];
                        $ch = curl_init();
                        curl_setopt($ch, CURLOPT_URL,"https://im.operatorplatform.com/api/users/add");
                        curl_setopt($ch, CURLOPT_POST, 1);
                        curl_setopt($ch, CURLOPT_POSTFIELDS,
                                    "data=".json_encode($dataaa));
                        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
                        
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                
                        $server_output = curl_exec ($ch);
                        //echo "<pre>"; print_r($dataaa); print_r($server_output);
                        curl_close ($ch);
                        
                    $dataaa1 = [
                                 "username"=>"Moneo",
                                 "password"=>"Moneo2019@",
                                 "service_code"=>"cd9271fa",
                                 "name"=> $user->name.'__'.$user->id,
                                 "group"=> "persona",
                                 "profile"=> [
                                                "avatar"=> "avatar.jpg",
                                                "age"=> $user->age,
                                                "location"=> $user->state,
                                                "images"=> [$user->profile_image]
                                             ],
                                  "additional_info"=> [
                                                "membership"=> "free",
                                                "link"=> "http://xyz.com",
                                                "about"=> "about"
                                             ],                                            
                                 ];
                        $ch = curl_init();
                        curl_setopt($ch, CURLOPT_URL,"https://im.operatorplatform.com/api/users/add");
                        curl_setopt($ch, CURLOPT_POST, 1);
                        curl_setopt($ch, CURLOPT_POSTFIELDS,
                                    "data=".json_encode($dataaa1));
                        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
                        
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                
                        $server_output = curl_exec ($ch);
                        //print_r($dataaa1); print_r($server_output);
                        curl_close ($ch);    
                    //register user to third party chat 
            
            $userwallet = DB::table('user_wallet')->where('user_id',$userid)->first();
            if($userwallet){
               $diamonds =  $userwallet->diamonds + 1;
               DB::table('user_wallet')->where('user_id',$userid)->update(array("diamonds"=>$diamonds)); 
            }else{
               $wallet = ["user_id"=>$userid, "diamonds"=>1]; 
               DB::table('user_wallet')->insertGetId($wallet);
            }
            echo 1;
        }
        
        if($request->get('age') && $request->get('region')){
            $userid = Auth::id();
            $age = date('Y') - $request->get('age');
            DB::table('users')->where('id',$userid)->update(array("state"=>$request->get('region'),"age"=>$age));
            
            $userwallet = DB::table('user_wallet')->where('user_id',$userid)->first();
            if($userwallet){
               $diamonds =  $userwallet->diamonds + 1;
               DB::table('user_wallet')->where('user_id',$userid)->update(array("diamonds"=>$diamonds)); 
            }else{
               $wallet = ["user_id"=>$userid, "diamonds"=>1]; 
               DB::table('user_wallet')->insertGetId($wallet);
            }
            echo 1;
        }

        if($request->get('email')){
            $userid = Auth::id();

            $emailexist = DB::table('users')->where('email',$request->get('email'))->count();
            
            if($emailexist){
                echo 0;
            }else{
                DB::table('users')->where('id',$userid)->update(array("email"=>$request->get('email')));

                $helper = new Helper;
                $subject = "Welcome to Flirting Sins! Verify your email address to get started";
                $email_content = [
                        'receipent_email'=> $request->get('email'),
                        'subject'=>$subject,
                        'greeting'=> 'Flirting Sins',
                        'first_name'=> explode('@',$request->get('email'))[0],
                        'template_id' => 1
                        ];
                //$verification_email = $helper->sendMailFrontEnd($email_content,'verification_link');

                $verification_email = $helper->sendMailFrontEnd_1($email_content,'register');

                echo 1;
            }                                                
        }
    }

	public function search_test(Request $request)
    {
	
		$users = DB::table('users')->where('country','Ireland')->inRandomOrder()->limit(500)->get();
		foreach($users as $row){
			DB::table('users')->where('id',$row->id)->update(array("state"=>"South West"));	
		}
		print_r($users); die;
		
		
		
		$reslt = '0';
        if(isset($_GET['partner']) && ($_GET['partner'] != '')){
            $reslt = $this->signup();
        }
		$adminid = DB::table('users')->where('id',3077)->first();
        
        //print_r($adminid);die;
        $profile_name = $request->profile_name;
        $gender = $request->gender;
        $min_age = $request->min_age;
        $max_age = $request->max_age;
        $country = trim($request->country);
        $state = trim($request->state);
        $city = trim($request->city);
        $seeking = $request->seeking;
        $s_interest = $request->get('s_interest');
        $kids = $request->s_kids;
        $education = $request->s_education;
        $s_languages = $request->get('s_languages');
        $online = $request->get('online');
        $relationship = $request->s_relationship;
        
        $flag = 0;
        if(!empty($s_interest))
        {
            $interest = implode(',',$s_interest);
            $flag = 1;
        }

        if(!empty($s_languages))
        {
            $languages = implode(',',$s_languages);
            $flag = 1;
        }

        //print_r($request->all());die;
        DB::enableQueryLog();
        $seeking_val = (isset(Auth::user()->seeking_val)) ? Auth::user()->seeking_val : 2;
        //$all_users = $this->search->get_users();
        if(Auth::check() && Auth::user()->status == 1){
            if($_GET['profile_name']){
                $users = DB::table('users')->select('users.*','user_details.*')
                        ->leftJoin('user_details', 'user_details.user_id', '=', 'users.id')->leftJoin('user_interests', 'user_interests.user_id', '=', 'user_details.user_id')->leftJoin('user_languages', 'user_languages.user_id', '=', 'user_interests.user_id')->where('users.status','=',1)->where('users.is_deleted','=',0)->where('users.id','!=',Auth::id());
            }else{
                $users = DB::table('users')->select('users.*')
                        ->where('users.status','=',1)->where('users.is_deleted','=',0)->where('users.id','!=',Auth::id());
            }
        }else{
            if($_GET['profile_name']){
                $users = DB::table('users')->select('users.*','user_details.*')
                        ->leftJoin('user_details', 'user_details.user_id', '=', 'users.id')->leftJoin('user_interests', 'user_interests.user_id', '=', 'user_details.user_id')->leftJoin('user_languages', 'user_languages.user_id', '=', 'user_interests.user_id')->where('users.status','=',1)->where('users.is_deleted','=',0);
            }else{
                $users = DB::table('users')->select('users.*')
                        ->where('users.status','=',1)->where('users.is_deleted','=',0);
            }
        }

        /*if (isset($profile_name)) {
           
        }
        else{
            $users->where('users.gender','=',$seeking_val);
        }*/

        if (!empty($profile_name)) {
            $users->where('users.name','LIKE','%'.$profile_name.'%');
            $flag = 1;
        }
        
        if (!empty($gender)) {
            //$users->where('users.gender','=',$gender);
            $flag = 1;
        }

        if (!empty($seeking)) {
            $users->where('users.gender','=',$seeking);
            $flag = 1;
        }

        if (!empty($min_age) && !empty($max_age)) {
            $users->whereBetween('users.age',[$min_age, $max_age]);
            $flag = 1;
        }

        if (!empty($country)) {
            $users->where('users.country','LIKE','%'.$country.'%');
            $flag = 1;
        }
        
        if (!empty($state)) {
            $users->where('users.state','LIKE','%'.$state.'%');
            $flag = 1;
        }
        
        if (!empty($city)) {
            $users->where('users.city','LIKE','%'.$city.'%');
            $flag = 1;
        }

        if (!empty($interest)) {
            $output_array = explode(',',$interest);

            foreach($output_array as $val)
            {
               $users->whereRaw('FIND_IN_SET("'.$val.'",user_interests.interests)');
            }
        }

        if (!empty($languages)) {
            $output_array = explode(',',$languages);

            foreach($output_array as $val)
            {
               $users->whereRaw('FIND_IN_SET("'.$val.'",user_languages.languages)');
            }
            $flag = 1;
        }

        if (!empty($kids)) {
            $users->where('user_details.kids','=',$kids);
            $flag = 1;
        }

        if (!empty($online)) {
            $users->where('users.is_login','=',$online);
            $flag = 1;
        }

        if (!empty($relationship)) {
            $users->where('user_details.relationship','=',$relationship);
            $flag = 1;
        }

        if (!empty($education)) {
            $users->where('user_details.education','Like','%'.$education.'%');
            $flag = 1;
        }
        
        if($flag){
            $all_users = $users->orderBy('users.id', 'desc')->paginate(30);
            //$all_users = $users->orderByRaw("RAND()")->paginate(30);
        }else{
            if (Auth::check()) {                
                $login = DB::table('users')->select('users.*','user_details.*')
                    ->leftJoin('user_details', 'user_details.user_id', '=', 'users.id')->where('users.id','=',Auth::id())->first();
                //echo "<pre>"; print_r($login); die;
                $min_age = ($login->min_age) ? $login->min_age : '18';
                $max_age = ($login->max_age) ? $login->max_age : '100';
                $users->whereBetween('users.age',[$min_age, $max_age]);          
                $users->where('users.gender','=',$login->seeking_val);
            }
            //$all_users = $users->orderBy('users.id', 'desc')->paginate(30);
            $all_users = $users->orderByRaw("RAND()")->paginate(30);
        }
        //$all_users = $users->orderBy('users.id', 'asc')->paginate(30);        
        //print_r($all_users);die;             
        $users = DB::table('users')->select('users.*')->where('users.id', '=',Auth::id())->first();                
        
        $chat_request = [];//DB::table('users')->select('users.*')->where('users.id', '!=',Auth::id())->paginate(5);
        //echo '<pre>';
        //print_r(DB::getQueryLog());die;
        //print_r($all_users);die;
        if ($request->ajax()) {
            $view = view('partial/all_users',compact('all_users'))->render();
            return response()->json(['html'=>$view]);
        }

        $all_interests = Helper::all_data('interests',['status'=>1]);

        $all_languages = Helper::all_data('languages',['status'=>1]);

        $countries = DB::table('countries')->get();
        
        if(Session::has('resendEmail')){
            Session::put('resendEmail','2');
        }else{
            Session::put('resendEmail','1');
        }
        
        return view('search_test',['all_users' => $all_users,'users'=>$users,'chat_request'=>$chat_request, 'all_interests' => $all_interests, 'all_languages' => $all_languages, 'countries' => $countries,'rand'=>$reslt,'adminid'=>$adminid]);
    }





    public function upload_csv()
    {
        //print_r(phpinfo()); die;
        /*if (file_exists(public_path('images\profile_image').'\\'.'Alexia2d.jpg')) {
            echo 'yes';
        }
        else{
            echo 'no';
        }*/
        return view('upload_csv');
    }

    public function importExcel(Request $request)
    {
        $request->validate([
            'import_file' => 'required'
        ]);
 
        $path = $request->file('import_file')->getRealPath();
        $data = Excel::load($path)->get();
 
        if($data->count()){
            foreach ($data as $key => $value) {
                echo '<pre>';
                //print_r($value);
            
                /*for users*/
                $gender = $value->gender;
                if(!empty($gender))
                {
                    if($gender == 'male')
                    {
                        $gender_val = '1';
                    }
                    else if($gender == 'female')
                    {
                        $gender_val = '2';
                    }
                }

                if(!empty($value->dob) && $value->dob != 'NULL')
                {
                    $from = new DateTime($value->dob);
                    $to   = new DateTime('today');
                    $age = $from->diff($to)->y;
                }
                else{
                    $age = '';
                }

                $users = array(
                    'name' => $value->f_name.' '.$value->l_name, //$value->name,
                    'email' => $value->email,
                    'password' => bcrypt($value->password),
                    'phone' => (!empty($value->phone) && $value->phone != 'NULL') ? $value->phone : '',
                    'gender' => $gender_val,
                    'dob' => date('Y-m-d', strtotime($value->dob)),
                    'age' => $age,  
                    'country' => $value->country,
                    'city' => $value->city,
                    'address' => (!empty($value->address) && $value->address != 'NULL') ? $value->address : '',
                    'profile_image' => $value->profile_image,
                    'cover_image' => $value->cover_image,
                    'profile_type' => $value->profile_type,
                    'appearance' => $value->appearance,
                    'adult_level' => $value->adult_level,
                    'is_update' => 0,//$value->is_update,
                    'is_login' => 0,//$value->is_login,
                    'last_login' => date('Y-m-d H:i:s'),//$value->last_login,
                    'status' => 1,//$value->status,
                    //'created_at' => date('Y-m-d H:i:s', strtotime($value->created_at)),
                    'updated_at' => date('Y-m-d H:i:s')

                );

                $user_id = DB::table('users')->insertGetId($users);
                //$user_id = 1;
                /*for users*/
                
                /*for user details*/
                if(!empty($value->kids))
                {
                    $kids = ($value->kids == 'Yes') ? '1' : '0';
                }

                $smoke = $value->smoke;
                if(!empty($smoke))
                {
                    if($smoke == 'Never')
                    {
                        $smoke_val = '1';
                    }
                    else if($smoke == 'Regularly')
                    {
                        $smoke_val = '2';
                    }
                    else if($smoke == 'Occasionally')
                    {
                        $smoke_val = '3';
                    }
                }

                $drink = $value->drink;
                if(!empty($drink))
                {
                    if($drink == 'Never')
                    {
                        $drink_val = '1';
                    }
                    else if($drink == 'Regularly')
                    {
                        $drink_val = '2';
                    }
                    else if($drink == 'Occasionally')
                    {
                        $drink_val = '3';
                    }
                }

                $relationship = $value->relationship;
                if(!empty($relationship))
                {
                    if($relationship == 'Never married')
                    {
                        $relationship_val = '1';
                    }
                    else if($relationship == 'Separated')
                    {
                        $relationship_val = '2';
                    }
                    else if($relationship == 'Divorced')
                    {
                        $relationship_val = '3';
                    }
                }

                $user_details = array(
                    'user_id' => $user_id, 
                    'seeking' => $value->seeking,
                    'about_me' => $value->about_me,
                    'about_partner' => $value->about_partner,
                    'work_as' => $value->work_as,
                    'education' => $value->education,
                    'relationship' => $relationship_val,
                    'kids' => $kids,
                    'smoke' => $smoke_val,
                    'drink' => $drink_val,
                    'height' => $value->height,
                    'updated_at' => date('Y-m-d H:i:s')
                );

                DB::table('user_details')->insertGetId($user_details);
                /*for user details*/

                /*for interests*/
                $interests = strtolower($value->interests);
                if(!empty($interests))
                {
                    if (strpos($interests, ';') === false) {
                        $interests_id = DB::table('interests')->where('name', '=', $interests)->first();
                        $interests_val = (!empty($interests_id)) ? $interests_id->id : '';
                    }
                    else{ 
                        $interests_arr = explode(';',$interests);
                        $interests_id_arr = array();
                        foreach($interests_arr as $val)
                        {
                            $interests_id = DB::table('interests')->where('name', '=', trim($val))->first();
                            $interests_id_arr[] = (!empty($interests_id)) ? $interests_id->id : '';
                        }
                        
                        $interests_val = (!empty($interests_id_arr)) ? implode(',',$interests_id_arr) : '';
                    }
                    
                }
                
                $user_interests = array(
                    'user_id' => $user_id, 
                    'interests' => $interests_val,
                    'updated_at' => date('Y-m-d H:i:s')
                );

                DB::table('user_interests')->insertGetId($user_interests);

                /*for interests*/


                /*for languages*/
                $languages = strtolower($value->languages);
                if(!empty($languages))
                {
                    if (strpos($languages, ';') === false) {
                        $languages_id = DB::table('languages')->where('name', '=', $languages)->first();
                        $languages_val = (!empty($languages_id)) ? $languages_id->id : '2';
                    }
                    else{ 
                        $languages_arr = explode(';',$languages);
                        $languages_id_arr = array();
                        foreach($languages_arr as $val)
                        {
                            $languages_id = DB::table('languages')->where('name', '=', trim($val))->first();

                            $languages_id_arr[] = (!empty($languages_id)) ? $languages_id->id : '2';

                        }
                        
                        $languages_val = (!empty($languages_id)) ? implode(',',$languages_id_arr) : '';
                    }
                    
                }
                
                $user_languages = array(
                    'user_id' => $user_id, 
                    'languages' => $languages_val,
                    'updated_at' => date('Y-m-d H:i:s')
                );
                

                DB::table('user_languages')->insertGetId($user_languages);
                /*for languages*/

                /*for gallery images*/
                $images = $value->gallery_image;
                
                if(!empty($images))
                {
                    if (strpos($images, ';') === false) {
                        $images_val = (!empty($images)) ? $images : '';
                            
                        $gallery_images = array(
                                'user_id' => $user_id, 
                                'gallery_image' => $images_val,
                                'updated_at' => date('Y-m-d H:i:s')
                            );

                        DB::table('gallery_images')->insertGetId($gallery_images);
                    }
                    else
                    { 
                        $images_arr = explode(';',$images);
                       
                        foreach($images_arr as $val)
                        {
                            $images_val = (!empty($val)) ? $val : '';
                            
                            $gallery_images = array(
                                'user_id' => $user_id, 
                                'gallery_image' => $images_val,
                                'updated_at' => date('Y-m-d H:i:s')
                            );

                            DB::table('gallery_images')->insertGetId($gallery_images);
                        }
                    }
                    
                }


                $private_photos = $value->private_photos;
                if(!empty($private_photos) && ($private_photos != 'NULL'))
                {
                    if (strpos($private_photos, ';') === false) {
                    
                        $private_images_val = (!empty($private_photos) && ($private_photos != 'NULL')) ? $private_photos : ''; 

                        if($private_images_val != '')
                        {
                            $gallery_images_private = array(
                                'user_id' => $user_id, 
                                'gallery_image' => $private_images_val,
                                'updated_at' => date('Y-m-d H:i:s')
                            );

                            DB::table('gallery_images')->insertGetId($gallery_images_private);
                        }
                    }
                    else
                    { 
                        $private_photos_arr = explode(';',$private_photos);
                            foreach($private_photos_arr as $val)
                            {
                                $private_images_val = (!empty($val) && ($val != 'NULL')) ? $val : ''; 

                                $gallery_images_private = array(
                                    'user_id' => $user_id, 
                                    'gallery_image' => $private_images_val,
                                    'is_private' => '1',
                                    'updated_at' => date('Y-m-d H:i:s')
                                );
                                
                                DB::table('gallery_images')->insertGetId($gallery_images_private);
                            }
                    
                    }
                    
                }
                
                /*for gallery images*/

                /*for gallery videos*/
                $videos = $value->gallery_video;
                if(!empty($videos))
                {
                    if (strpos($videos, ';') === false) {
                        
                        $videos_val = (!empty($videos)) ? $videos : '';

                        $gallery_videos = array(
                            'user_id' => $user_id, 
                            'gallery_video' => $videos_val,
                            'updated_at' => date('Y-m-d H:i:s')
                        );

                        DB::table('gallery_videos')->insertGetId($gallery_videos);
                    }
                    else{ 
                        $videos_arr = explode(';',$videos);
                        
                        foreach($videos_arr as $val)
                        {
                            
                            $videos_val = (!empty($val)) ? $val : '';

                            $gallery_videos = array(
                                'user_id' => $user_id, 
                                'gallery_video' => $videos_val,
                                'updated_at' => date('Y-m-d H:i:s')
                            );

                            DB::table('gallery_videos')->insertGetId($gallery_videos);

                        }
                        
                    }
                    
                }

                echo '<pre>';
                //print_r($gallery_videos);

                /*for gallery videos*/

                if(file_exists(public_path('images/profile_image').'/'.$value->profile_image)) 
                {
                    if(!file_exists(public_path('thumbnail').'/'.$value->profile_image)) 
                    {
                        $destinationPath = public_path('/thumbnail');
                        $image = public_path('images/profile_image').'/'.$value->profile_image;
                        $img = Image::make($image);
                        $img->resize(300, 300, function ($constraint) {
                            $constraint->aspectRatio();
                        })->save($destinationPath.'/'.$value->profile_image);
                    }
                }
                else{
                    
                }

                if(file_exists(public_path('images\profile_image').'\\'.$value->profile_image)) 
                {
                    if(!file_exists(public_path('thumbnail').'\\'.$value->profile_image)) 
                    {
                        $destinationPath = public_path('\thumbnail');
                        $image = public_path('images\profile_image').'\\'.$value->profile_image;
                        $img = Image::make($image);
                        $img->resize(300, 300, function ($constraint) {
                            $constraint->aspectRatio();
                        })->save($destinationPath.'/'.$value->profile_image);
                    }
                }
                else{
                    
                }
                
            }
            echo 'Insert Record successfully.';
            die;
 
           
        }
        
            
        return back()->with('success', 'Insert Record successfully.');
    }
    
    
    public function signup()
    {				
            $profile_image = ($gender == 1) ? 'male_icon.png' : 'male_icon.png';
            
			$cover_img = DB::table('cover_images')->select('images')->inRandomOrder()->take(1)->first();
            
            $partnerid = Null;
            if($_GET['partner']){
                $p_exist = DB::table('partner')->where('partnerid',$_GET['partner'])->first();
                //print_r($p_exist); die;
                if($p_exist){
                    $partnerid = $p_exist->id;
                }
            }
            
            if($partnerid && ($_GET['email'] != '')){                
                $email_exist = DB::table('users')->where('email',$_GET['email'])->first();
                
                if($email_exist){
                    Auth::loginUsingId($email_exist->id);
                    return 0;                
                }else{
                    $rand = rand(111111,999999);//uniqid();
                    $user_data = array(
                        'name' => '',//explode('@',$_GET['email'])[0],
                        'email' => $_GET['email'],
                        'password' => bcrypt($rand),		
                        'gender' => '1',
                        'seeking_val' => '',
                        'country' => 'United Kingdom',
                        'state' => '',
                        'city' => '',
                        'dob' => '2000-01-01',
                        'age'  => '18',
                        'is_update' => '1',
                        'profile_image' => $profile_image,
                        'cover_image' => $cover_img->images,
                        'status' => '1',
                        'fake' => '0',
                        'partner_id' => $partnerid,
                        'profile_created_at'=>date('Y-m-d'),
                        'profile_activated_at'=>date('Y-m-d'),
                        'activation_time'=>date('Y-m-d h:i:s'),
                        'campaign_id' => 2
                    );
        
                    //print_r($user_data);die;
        
                    $user = User::create($user_data);
        
                    $user_detail = array(
                        'user_id' => $user->id
                    );
                    
                    Auth::loginUsingId($user->id);
                    //print_r($user);die;
                    
                    $detail_id = Helper::insert_data('user_details',$user_detail);
                    
                    $wallet_detail = array(
                        'user_id' => $user->id,
                        'credits' => '1',
                        'diamonds' => '3'
                    );
                    Helper::insert_data('user_wallet',$wallet_detail);
                    
                    $helper = new Helper;
                    $subject = "Welcome to flirtfull! You are successfully registered with us.";
                    $email_content = [
                            'receipent_email'=> $_GET['email'],
                            'subject'=>$subject,
                            'greeting'=> 'flirtfull',
                            'first_name'=> $_GET['email'],
                            'template_id' => 3
                            ];
                    $verification_email = $helper->sendMailSuccess($email_content,$rand);
                    
                    //register user to third party chat
                    $dataaa = [
                                 "username"=>"Moneo",
                                 "password"=>"Moneo2019@",
                                 "service_code"=>"cd9271fa",
                                 "name"=> $user->name.'__'.$user->id,
                                 "group"=> "subscriber",
                                 "profile"=> [
                                                "avatar"=> "avatar.jpg",
                                                "age"=> $user->age,
                                                "location"=> $user->state,
                                                "images"=> [$user->profile_image]
                                             ],
                                  "additional_info"=> [
                                                "membership"=> "free",
                                                "link"=> "http://xyz.com",
                                                "about"=> "about"
                                             ],                                            
                                 ];
                        $ch = curl_init();
                        curl_setopt($ch, CURLOPT_URL,"https://im.operatorplatform.com/api/users/add");
                        curl_setopt($ch, CURLOPT_POST, 1);
                        curl_setopt($ch, CURLOPT_POSTFIELDS,
                                    "data=".json_encode($dataaa));
                        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
                        
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                
                        $server_output = curl_exec ($ch);                
                        curl_close ($ch);
                        
                    $dataaa1 = [
                                 "username"=>"Moneo",
                                 "password"=>"Moneo2019@",
                                 "service_code"=>"cd9271fa",
                                 "name"=> $user->name.'__'.$user->id,
                                 "group"=> "persona",
                                 "profile"=> [
                                                "avatar"=> "avatar.jpg",
                                                "age"=> $user->age,
                                                "location"=> $user->state,
                                                "images"=> [$user->profile_image]
                                             ],
                                  "additional_info"=> [
                                                "membership"=> "free",
                                                "link"=> "http://xyz.com",
                                                "about"=> "about"
                                             ],                                            
                                 ];
                        $ch = curl_init();
                        curl_setopt($ch, CURLOPT_URL,"https://im.operatorplatform.com/api/users/add");
                        curl_setopt($ch, CURLOPT_POST, 1);
                        curl_setopt($ch, CURLOPT_POSTFIELDS,
                                    "data=".json_encode($dataaa1));
                        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
                        
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                
                        $server_output = curl_exec ($ch);                
                        curl_close ($ch);    
                    //register user to third party chat                    
                    
                    return $rand;   
                }
            }else{
                return 0;
            }
        
    }

//saif changes start

public function resend_verification(Request $request)
{
$userid = Auth::id();
//$userid =$request->user_id;

$user_details = DB::table('users')->select('users.*')->where('users.id',$userid)->get();

//print_r($user_details); die;
//echo $user_details[0]->email; die;
$user_email = $user_details[0]->email;
$user_name = $user_details[0]->name;

$helper = new Helper;

$subject = "Welcome to Flirting Sins! Verify your email address to get started";

$email_content = [
'receipent_email'=> $user_email,
'subject'=>$subject,
'greeting'=> 'Flirting Sins',
'first_name'=> $user_name,
'template_id' => 1
];

$verification_email = $helper->sendMailFrontEnd($email_content,'verification_link');

//session()->flash('success', 'Resend Success..!!');

echo json_encode(array('success' => true));
}

//saif changes end


}