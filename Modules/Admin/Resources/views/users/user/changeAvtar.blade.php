<div class="tab-pane" id="tab_1_3">  
<div class="portlet light bordered">
 
 
<input type="hidden" name="tab" value="avtar">
    <div class="form-group">
         
        <div class="col-md-12">
        <div class="form-group  {{ $errors->first('profile_image', ' has-error') }}">
            <label class="control-label col-md-2"> Profile Image <span class="required"> * </span></label>
            <div class="col-lg-6">
                @if(isset($user->profile_image))
                <div class="" >
                <img src=" {{ (URL::asset('public/images/profile_image/'.$user->profile_image)) ?? 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image'}}" alt="" height="200px" width="150px">
                </div>
                @endif                
                <input type="file" class="file-input" name="profile_image">
                <span class="help-block" style="color:#e73d4a">{{ $errors->first('profile_image', ':message') }}</span>
            
            </div>
        </div>
    </div>
     <div class="form-group">
         
        <div class="col-md-12">
        <div class="form-group  {{ $errors->first('cover_image', ' has-error') }}">
            <label class="control-label col-md-2"> Cover Image <span class="required"> * </span></label>
            <div class="col-lg-6">
                @if(isset($user->cover_image))
                <div class="" >
                <img src=" {{ (URL::asset('public/images/cover_img/'.$user->cover_image)) ?? 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image'}}" alt="" height="200px" width="150px">
                </div>
                @endif                
                <input type="file" class="file-input" name="cover_image">
                <span class="help-block" style="color:#e73d4a">{{ $errors->first('cover_image', ':message') }}</span>
            
            </div>
        </div>
    </div>    
    <div class="form-group">
         
        <div class="col-md-12">
        <div class="form-group  {{ $errors->first('gallery_image', ' has-error') }}">
            <label class="control-label col-md-2"> Gallery Images <span class="required"> * </span></label>
            <div class="col-lg-6">
            
                @if(isset($user->gallery_images))
                    <div class="row">
                    @foreach($user->gallery_images as $row)
                        
                    <div class="col-md-4" >
                    <!--<img src=" {{ (URL::asset('public/images/profile_image/'.$row->gallery_image)) ?? 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image'}}" alt="" height="200px" width="150px">-->
                    <img src=" {{ (URL::asset('public/images/profile_image/'.$row->gallery_image)) ?? 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image'}}" alt="" height="200px" width="150px" onerror="this.src='{{ asset("public/images/profile_image/no_image.jpg") }}'">
                    <!--<span class="label label-primary status" id="{{$row->id}}"  data="{{$row->is_deleted}}"   >-->
                       <a href="{{ url('admin/delete_image?id='.$row->id.'&userid='.$user->id) }}"> Delete </a>
                    <!--</span>-->
                    </div>                    
                    @endforeach
                    </div>        
                @endif
                
                <input type="file" class="file-input" name="gallery_image[]" multiple>                            
                 <span class="help-block" style="color:#e73d4a">{{ $errors->first('gallery_image', ':message') }}</span>
            
            </div>
        </div>
    </div>    
     <div class="col-md-12">
            <div class="margin-top-10"> 
                 <button type="submit" class="btn green btn-primary" value="avtar" name="submit"> Save Changes </button>
                <button type="reset" class="btn default "> Cancel </button>
            </div> 
           
            </div>
    </div>
          
</div>