<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('welcome');
});

Route::get('/test', function () {
    return view('emails.verification_link');
});

Route::get('resizeImage', 'ImageController@resizeImage');
Route::post('resizeImagePost',['as'=>'resizeImagePost','uses'=>'ImageController@resizeImagePost']);



Route::post('signup', 'Auth\RegisterController@signup' );

Route::post('login', 'Auth\LoginController@login' );

Route::get('logout', 'Auth\LoginController@logout' );

Route::match(['post','get'],'forgetPassword','Auth\ForgotPasswordController@forgetPassword');

//Route::match(['post','get'],'resetPassword','Auth\ForgotPasswordController@resetPassword');

Route::get('/social-auth', 'Auth\SocialController@redirectToProvider')->name('auth.social');

Route::get('/social-auth/{provider}/callback', 'Auth\SocialController@handleProviderCallback')->name('auth.social.callback');

Route::group(['middleware' => 'web'], function () {

Route::get('/clear-cache', function() {
Artisan::call('cache:clear');
Artisan::call('config:clear');
Artisan::call('view:clear');
Artisan::call('cache:clear');
    return "Cache is cleared";
die;
});


	Route::get('/', 'HomeController@index');
	Route::get('home-test', 'HomeController@home_test');

    Route::post('getCity', 'HomeController@getCity');
    
	Route::post('getRegion', 'HomeController@getRegion');
	
	Route::post('getCityByState', 'HomeController@getCityByState');

	Route::get('search', 'SearchController@index' );
	Route::get('search-test', 'SearchController@search_test' );

	Route::get('inbox', 'InboxController@index' );
    Route::get('inbox_ajax/{id}', 'InboxController@ajax_message' );
    Route::get('inbox_ajax/{id}/{term}', 'InboxController@ajax_message' );

    Route::get('inbox-chat/{id}', 'InboxController@chat' );
    Route::get('message/{email}/{sender_id}', 'InboxController@message' );

    Route::get('profile/{user_id}', 'ProfileController@index');
    Route::get('profile-test/{user_id}', 'ProfileController@index_test');
    
    Route::post('fake_chat', 'ProfileController@fake_chat');
	
    Route::post('profile-like', 'ProfileController@profile_like');
    Route::post('profile-liking', 'HomeController@profile_liking');
    Route::post('likes', 'ProfileController@likes');
	Route::post('send-like-message', 'ProfileController@send_like_message');
    Route::post('add-diamonds', 'ProfileController@add_diamonds');
	Route::get('our-plans', 'HomeController@our_plans');
    
	Route::get('upload_csv', 'SearchController@upload_csv' );

	Route::post('importExcel', 'SearchController@importExcel');
	
	Route::post('user-profile-update', 'ProfileController@user_profile_update');

	Route::post('about-me-update', 'ProfileController@about_me_update');

	Route::post('looking-for-update', 'ProfileController@looking_for_update');

	Route::post('interests-update', 'ProfileController@interests_update');
	
	Route::post('about-detail-update', 'ProfileController@about_detail_update');

	Route::post('ajax-image-upload', 'ProfileController@ajax_profile_image');
    
    Route::post('cover-image-upload', 'ProfileController@cover_image_upload');
    
    Route::post('gallery-image-upload', 'ProfileController@gallery_image_upload');
    
    Route::post('gallery-video-upload', 'ProfileController@gallery_video_upload');

    Route::post('gallery-image-delete', 'ProfileController@gallery_image_delete');
    
    Route::post('gallery-video-delete', 'ProfileController@gallery_video_delete');
    
    Route::post('chat-file', 'ProfileController@chat_file');
	
	Route::post('purchase-credit', 'HomeController@purchase_credit');
	
	Route::get('payment-success/{order_id}', 'HomeController@payment_success');
	
    Route::get('payment-callback', 'HomeController@payment_callback');
	Route::post('payment-callback', 'HomeController@payment_callback');
	
	Route::get('post-back', 'HomeController@post_back');
	Route::get('post-back-doc', 'HomeController@post_back_doc');
	
	Route::post('purchase-credit-paynl', 'HomeController@purchase_credit_paynl');
	Route::get('payment-callback-paynl', 'HomeController@payment_callback_paynl');
	Route::get('getPaynl', 'HomeController@getPaynl');
    
    Route::post('get_chat_id', 'ProfileController@get_chat_id');
    Route::post('update_conversation', 'ProfileController@update_conversation');
    Route::post('update_diamonds', 'ProfileController@update_conversation1');
    Route::get('conversation', 'InboxController@conversation');
    
    Route::get('get_chat_contacts', 'ProfileController@get_chat_contacts');
    
    Route::post('chat-request', 'ProfileController@chat_request');
    Route::post('chat-proposal', 'ProfileController@chat_proposal');
    Route::post('get-proposal', 'ProfileController@get_proposal');
    Route::get('get-emoji', 'ProfileController@get_emoji');
    Route::post('getProposal', 'InboxController@get_proposal');
    
    Route::post('change-email', 'HomeController@change_email');

    Route::post('change-phone', 'HomeController@change_phone');

    Route::post('change-password', 'HomeController@change_password');

    Route::post('delete-account', 'HomeController@delete_my_account' );

    Route::get('setting', 'HomeController@setting');
   
    Route::get('about', 'HomeController@about');
    Route::get('test', 'HomeController@test');

    Route::get('help-center', 'HomeController@help_center');
    
    Route::post('post-help-center', 'HomeController@post_help_center');

    Route::get('faq', 'HomeController@faq');
    Route::get('terms-conditions', 'HomeController@terms_conditions');
    Route::get('privacy-policy', 'HomeController@privacy_policy');
    
    //for activities
    Route::get('activities', 'HomeController@activities');
    Route::get('activities/likeme', 'HomeController@activities');
    Route::get('activities/favorite', 'HomeController@activities');
    Route::get('activities/favoriteme', 'HomeController@activities');
    Route::get('activities/match', 'HomeController@activities');
    Route::get('activities/visit', 'HomeController@activities');
    Route::get('activities/visitme', 'HomeController@activities');
    
    Route::get('matches', 'HomeController@like_page');
    Route::get('resend-verification', 'HomeController@resend_verification' );
	
    Route::post('updateCredits', 'HomeController@updateCredits' );
    
    Route::post('insert-chat-api', 'HomeController@insert_chat_api' );
    Route::get('insert-chat-api', 'HomeController@insert_chat_api' );
    Route::post('insert_activity_api', 'HomeController@insert_activity_api' );
    Route::post('insert_like_activity_api', 'HomeController@insert_like_activity_api' );
    
    Route::get('getMessagesCron', 'HomeController@get_messages_cron');
    Route::get('getMessagesCron_test', 'HomeController@get_messages_cron_test');
    Route::get('newchatroom', 'HomeController@newchatroom');
    Route::get('getRandomFakeUser', 'HomeController@getRandomFakeUser');
    
    Route::get('insertFirebase', 'InboxController@insertFirebase');
    Route::get('registerThirdparty', 'InboxController@registerThirdparty');
    Route::post('/update-outer-user', 'HomeController@update_outer_user');
    
    Route::get('update-user-list', 'HomeController@updateUserList');
	Route::get('update-chatRoom-list', 'HomeController@updateChatRoomList');
    Route::get('update-partner-list', 'HomeController@updatePartnerList');
	
	Route::get('/callBack', 'InboxController@callBackFunFirebase');
    Route::post('/callBack', 'InboxController@callBackFunFirebase');
    
    Route::post('/get_user', 'HomeController@get_user');
    
    
    Route::get('/makeUserLogin', 'HomeController@makeUserLogin');
    Route::get('/check-online-status', 'HomeController@check_online');
    Route::get('/check-online-status-real', 'HomeController@check_online_real');
    
    Route::get('/release_credits', 'HomeController@release_credits');
        
    Route::post('/update-profile', 'SearchController@update_profile');

    Route::post('/get_myPhoto', 'ProfileController@get_myPhoto');
    Route::post('/get_myPrivatePhoto', 'ProfileController@get_myPrivatePhoto');
    Route::post('/get_similar', 'ProfileController@get_similar');

    Route::get('/credits', 'HomeController@credits');
    Route::get('/wallet', 'HomeController@wallet');
    Route::get('/premiums', 'HomeController@premiums');
});

Route::post('getProfiles','HomeController@get_profiles');
Route::get('getRealProfiles','HomeController@getRealProfiles');
Route::get('GetRealUsersMY','HomeTestController@GetRealUsersMY');
// Route::get('GetRealUsersMY','HomeController@GetRealUsersMY');

Route::get('mailtemplate','HomeController@mailtemplate');
Route::get('mailtemplate1','HomeController@mailtemplate1');

Route::post('fetch_users','HomeController@fetch_users');

Route::get('send-activation-reminder','HomeController@send_activation_reminder');

Route::get('BlogController','BlogController@index');
Route::post('uploadfile','BlogController@uploadFilePost');

