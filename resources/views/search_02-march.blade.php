@extends('layout.default')
@section('content')

<!-- Chat script -->
<script src="{{ URL::asset('assets/js/emojis/config.js')}}"></script>
<script src="{{ URL::asset('assets/js/emojis/util.js')}}"></script>
<script src="{{ URL::asset('assets/js/emojis/jquery.emojiarea.js')}}"></script>
<script src="{{ URL::asset('assets/js/emojis/emoji-picker.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.11/lodash.core.js"></script>
<script src="https://cdn.socket.io/socket.io-1.0.0.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/socket.io/1.7.4/socket.io.min.js"></script>
<script src="{{ URL::asset('assets/js/libs/moment.js')}}"></script>
<script src="https://rawgit.com/thielicious/selectFile.js/master/selectFile.js"></script>    
<!--Chat script -->   
<?php
            $helper1 = new Helper();
            if (Auth::check()){
            $chatrequest = $helper1->getChatRequest(Auth::user()->id);
            $confirmchatrequest = $helper1->getConfirmChatRequest(Auth::user()->id);
            $fakechatrequest = $helper1->getFakeChatRequest(Auth::user()->id);
            }else{
            $chatrequest = [];
            $confirmchatrequest = [];
            $fakechatrequest = [];
            }
            
            ?>    
<section class="search_list_outer">
  <div class="container-fluid">
    <div class="row">
            <div class="col-md-9 col-sm-8 pdng main-content">
            <div class="user_profile_popup" id="profile_popup">
                <div class="nw-profiles">
                    <div class="nw-profiles-otr">
                     @if(!empty($all_users) && count($all_users)>0)
                     <ul id="all-users">
                       @include('partial/all_users')
                     </ul>
                     @else
                       <div class="text-center load-more">Oops, your search criteria does not match any members yet.</div>
                     @endif
                   </div>
                 </div>
                <div style="display: none" class="text-center load-more"><img width="100px" src="{{ asset('public/images/loader.gif') }}"></div>
            </div>
            <textarea id="chat_file_hidden" style="display:none"></textarea>     
           <!--  chat popup  --> 
           @foreach($fakechatrequest as $simlr)
           <div class="user_chat_popup" id="myScrollchat{{$simlr->id}}" data-spy="affix" data-offset-top="20" data-offset-bottom="0">
            	<div class="chat-window-panel">
                <div class="chat-window-panel-inner">
            	<div class="chat-window-title">
                	<img id="chat_image{{$simlr->id}}" src="http://democarol.com/DatingApp/assets/front/images/user4.jpg"><span></span> <span id="chat_name{{$simlr->id}}"></span>
                    <span id="close{{$simlr->id}}" class="pull-right"><i class="fa fa-close"></i></span>
                </div>
                <div class="chat-window-box">                                
                  <ul id="messages{{$simlr->id}}" class="chat__messages">
                    <script id="message-template{{$simlr->id}}" type="text/template">

                    </script>
                  </ul>
                  
                    <!--<script id="location-message-template" type="text/template">
                        <li class="message">
                            <div class="message__title">
                                <h4>from1</h4>
                                <span>20-02-19</span>
                            </div>
                            <div class="message__body">
                                <a href="" target="_blank">My current location</a>
                            </div>
                        </li>
                    </script>-->    
                    
                    
                                    	
                </div>   
                <div class="chat_icon_outer">
                    	<div class="emoji-tape">
                        <span class="top-arrow"><i class="fa fa-angle-up"></i></span>
                        	<ul>
                            	<li>
                                	<div class="zoomed">
                                        <div class=""><img src="http://35.242.157.120/public/thumbnail/chat1.png" onclick="set_smiley('chat1.png');">
                                        <span class="amount for-devaluation-disabled">40 credits</span>
                                    </div></div>
                                </li>
                                <li>
                                	<div class="zoomed">
                                        <div class=""><img src="http://35.242.157.120/public/thumbnail/chat2.png" onclick="set_smiley('chat2.png');">
                                        <span class="amount for-devaluation-disabled">10 credits</span>
                                    </div></div>
                                </li>
                                <li>
                                	<div class="zoomed">
                                        <div class=""><img src="http://35.242.157.120/public/thumbnail/chat3.png" onclick="set_smiley('chat3.png');">
                                        <span class="amount for-devaluation-disabled">40 credits</span>
                                    </div></div>
                                </li>
                                <li>
                                	<div class="zoomed">
                                        <div class=""><img src="http://35.242.157.120/public/thumbnail/chat4.png" onclick="set_smiley('chat4.png');">
                                        <span class="amount for-devaluation-disabled">20 credits</span>
                                    </div></div>
                                </li>
                                <li>
                                	<div class="zoomed">
                                        <div class=""><img src="http://35.242.157.120/public/thumbnail/chat5.png" onclick="set_smiley('chat5.png');">
                                        <span class="amount for-devaluation-disabled">40 credits</span>
                                    </div></div>
                                </li>
                            
                            
                            	
                                
                            </ul>
                        </div>
                    	<div class="emoji-list">
                        	<ul>
                            	<li><img src="http://35.242.157.120/public/thumbnail/chat1.png" onclick="set_smiley('chat1.png');"><span>10 Credits</span></li>
                                <li><img src="http://35.242.157.120/public/thumbnail/chat2.png" onclick="set_smiley('chat2.png');"><span>40 Credits</span></li>
                                <li><img src="http://35.242.157.120/public/thumbnail/chat3.png" onclick="set_smiley('chat3.png');"><span>20 Credits</span></li>
                                <li><img src="http://35.242.157.120/public/thumbnail/chat4.png" onclick="set_smiley('chat4.png');"><span>40 Credits</span></li>
                                <li><img src="http://35.242.157.120/public/thumbnail/chat5.png" onclick="set_smiley('chat5.png');"><span>25 Credits</span></li>
                                <li><img src="http://35.242.157.120/public/thumbnail/chat6.png" onclick="set_smiley('chat6.png');"><span>40 Credits</span></li>
                                <li><img src="http://35.242.157.120/public/thumbnail/chat7.png" onclick="set_smiley('chat7.png');"><span>30 Credits</span></li>
                                <li><img src="http://35.242.157.120/public/thumbnail/chat8.png" onclick="set_smiley('chat8.png');"><span>40 Credits</span></li>
                                <li><img src="http://35.242.157.120/public/thumbnail/chat9.png" onclick="set_smiley('chat9.png');"><span>10 Credits</span></li>
                                <li><img src="http://35.242.157.120/public/thumbnail/chat10.png" onclick="set_smiley('chat10.png');"><span>40 Credits</span></li>
                            </ul>
                        </div>
                    </div>
                
                                                     
                </div>
                
                <div class="chat-sender-box"> 
                
                	                   
                        <div class="chat__main">
                            
                
                            <div class="chat__footer">
                                <form id="message-form{{$simlr->id}}" method="POST" enctype="multipart/form-data">
                                    <input
                                        class="form-control"
                                        type="text"
                                        name="message{{$simlr->id}}"
                                        id="message{{$simlr->id}}"
                                        placeholder="type your message"
                                        autofocus
                                        autocomplete="false"
                                    />
                                    <div class="chat_submit_box">
                                    <button type="submit" class="btn btn-primary chat-btn-text"><span class="chat-btn-text">send</span><span class="chat-btn-icon"><i class="fa fa-arrow-right"></i></span></button>
                                   <div class="chat_submit_box_inner"> 
                                   	<input type="file" hidden id="file{{$simlr->id}}" name="file{{$simlr->id}}">
									<button id="upload{{$simlr->id}}"><i class="fa fa-paperclip "></i></button>
									<label id="selected{{$simlr->id}}">Nothing selected</label>
                                   
                                  <!-- <input type="file" hidden id="choose" name="file{{$simlr->id}}" id="file{{$simlr->id}}">-->
                                   
                                   </div>
                                    
                                    </div>
                                    <img class="load-more" src="http://35.242.157.120/public/images/chat/sending.gif" height="100px" width="200px" style="display:none">
                                    <!--<a href="#"><i class="fa fa-paperclip"></i>
                                    </a>                                                                        -->
                                </form>
                
                                <!--<button id="send-location">Send Location</button>-->
                            </div>
                        </div>                    
                </div>
            </div>
           	
            </div> 
           <!-- chat popup-->
           
           <script>
		   	
		   $(document).ready(function(){ 
		   
		   $('#upload{{$simlr->id}}').click(function(){
                $("#file{{$simlr->id}}").click();
            })
                        
                            $("#file{{$simlr->id}}").change(function(e) {
                                     var fileName = e.target.files[0].name;
                                     $("#selected{{$simlr->id}}").html(fileName);
                                //alert('The file "' + fileName +  '" has been selected.');
                            }); 
                        });
                       
            var getFile = new selectFile;
            getFile.targets('file{{$simlr->id}}','selected{{$simlr->id}}');
            </script>
           <script>
            function set_smiley(smiley){
                $('#message{{$simlr->id}}').val(smiley);
                $('#message-form{{$simlr->id}}').submit();
            }
           </script>
           <script>
            $(document).ready(function(){ 
                $("#close{{$simlr->id}}").click(function() {
                    $('#myScrollchat{{$simlr->id}}').hide();
                    $("#profile_popup").removeClass("user_profile_popup");
                    $('#messages{{$simlr->id}}').html('');
                }); 
            });
            </script>
            <script>
            $(document).ready(function(){     
                    $('#myScrollchat{{$simlr->id}}').hide();
                    $("#profile_popup").removeClass("user_profile_popup");
            });
            </script>    
            <script>
            $(document).ready(function(){ 
                $("#chat-user-btn{{$simlr->id}}").click(function() {
                    $('.user_chat_popup').hide();
                    $('.chat__messages').html('');                    
                    $('#myScrollchat{{$simlr->id}}').show();
                    $("#profile_popup").addClass("user_profile_popup");
                }); 
            });    
            </script>
            <script>
            $(document).ready(function(){ 
                $("#chat-user-btnn{{$simlr->id}}").click(function() {
                    $('.user_chat_popup').hide();
                    $('.chat__messages').html('');                    
                    $('#myScrollchat{{$simlr->id}}').show();
                    $("#profile_popup").addClass("user_profile_popup");
                }); 
            });    
            </script>                
            @endforeach
      
            
           @foreach($chatrequest as $simlr)
           <div class="user_chat_popup" id="myScrollchat{{$simlr->id}}" data-spy="affix" data-offset-top="20" data-offset-bottom="0">
            	<div class="chat-window-panel">
                <div class="chat-window-panel-inner">
            	<div class="chat-window-title">
                	<img id="chat_image{{$simlr->id}}" src="http://democarol.com/DatingApp/assets/front/images/user4.jpg"><span></span> <span id="chat_name{{$simlr->id}}"></span>
                    <span id="close{{$simlr->id}}" class="pull-right"><i class="fa fa-close"></i></span>
                </div>
                <div class="chat-window-box">                                
                  <ul id="messages{{$simlr->id}}" class="chat__messages">
                    <script id="message-template{{$simlr->id}}" type="text/template">

                    </script>
                  </ul>
                  
                    <!--<script id="location-message-template" type="text/template">
                        <li class="message">
                            <div class="message__title">
                                <h4>from1</h4>
                                <span>20-02-19</span>
                            </div>
                            <div class="message__body">
                                <a href="" target="_blank">My current location</a>
                            </div>
                        </li>
                    </script>-->    
                    
                    
                                    	
                </div>   
                <div class="chat_icon_outer">
                    	<div class="emoji-tape">
                        <span class="top-arrow"><i class="fa fa-angle-up"></i></span>
                        	<ul>
                            	<li>
                                	<div class="zoomed">
                                        <div class=""><img src="http://35.242.157.120/public/thumbnail/chat1.png" onclick="set_smiley('chat1.png');">
                                        <span class="amount for-devaluation-disabled">40 credits</span>
                                    </div></div>
                                </li>
                                <li>
                                	<div class="zoomed">
                                        <div class=""><img src="http://35.242.157.120/public/thumbnail/chat2.png" onclick="set_smiley('chat2.png');">
                                        <span class="amount for-devaluation-disabled">10 credits</span>
                                    </div></div>
                                </li>
                                <li>
                                	<div class="zoomed">
                                        <div class=""><img src="http://35.242.157.120/public/thumbnail/chat3.png" onclick="set_smiley('chat3.png');">
                                        <span class="amount for-devaluation-disabled">40 credits</span>
                                    </div></div>
                                </li>
                                <li>
                                	<div class="zoomed">
                                        <div class=""><img src="http://35.242.157.120/public/thumbnail/chat4.png" onclick="set_smiley('chat4.png');">
                                        <span class="amount for-devaluation-disabled">20 credits</span>
                                    </div></div>
                                </li>
                                <li>
                                	<div class="zoomed">
                                        <div class=""><img src="http://35.242.157.120/public/thumbnail/chat5.png" onclick="set_smiley('chat5.png');">
                                        <span class="amount for-devaluation-disabled">40 credits</span>
                                    </div></div>
                                </li>
                            
                            
                            	
                                
                            </ul>
                        </div>
                    	<div class="emoji-list">
                        	<ul>
                            	<li><img src="http://35.242.157.120/public/thumbnail/chat1.png" onclick="set_smiley('chat1.png');"><span>10 Credits</span></li>
                                <li><img src="http://35.242.157.120/public/thumbnail/chat2.png" onclick="set_smiley('chat2.png');"><span>40 Credits</span></li>
                                <li><img src="http://35.242.157.120/public/thumbnail/chat3.png" onclick="set_smiley('chat3.png');"><span>20 Credits</span></li>
                                <li><img src="http://35.242.157.120/public/thumbnail/chat4.png" onclick="set_smiley('chat4.png');"><span>40 Credits</span></li>
                                <li><img src="http://35.242.157.120/public/thumbnail/chat5.png" onclick="set_smiley('chat5.png');"><span>25 Credits</span></li>
                                <li><img src="http://35.242.157.120/public/thumbnail/chat6.png" onclick="set_smiley('chat6.png');"><span>40 Credits</span></li>
                                <li><img src="http://35.242.157.120/public/thumbnail/chat7.png" onclick="set_smiley('chat7.png');"><span>30 Credits</span></li>
                                <li><img src="http://35.242.157.120/public/thumbnail/chat8.png" onclick="set_smiley('chat8.png');"><span>40 Credits</span></li>
                                <li><img src="http://35.242.157.120/public/thumbnail/chat9.png" onclick="set_smiley('chat9.png');"><span>10 Credits</span></li>
                                <li><img src="http://35.242.157.120/public/thumbnail/chat10.png" onclick="set_smiley('chat10.png');"><span>40 Credits</span></li>
                            </ul>
                        </div>
                    </div>
                
                                                     
                </div>
                
                <div class="chat-sender-box"> 
                
                	                   
                        <div class="chat__main">
                            
                
                            <div class="chat__footer">
                                <form id="message-form{{$simlr->id}}" method="POST" enctype="multipart/form-data">
                                    <input
                                        class="form-control"
                                        type="text"
                                        name="message{{$simlr->id}}"
                                        id="message{{$simlr->id}}"
                                        placeholder="type your message"
                                        autofocus
                                        autocomplete="false"
                                    />
                                    <div class="chat_submit_box">
                                    <button type="submit" class="btn btn-primary chat-btn-text"><span class="chat-btn-text">send</span><span class="chat-btn-icon"><i class="fa fa-arrow-right"></i></span></button>
                                   <div class="chat_submit_box_inner"> 
                                   	<input type="file" hidden id="file{{$simlr->id}}" name="file{{$simlr->id}}">
									<button id="upload{{$simlr->id}}"><i class="fa fa-paperclip "></i></button>
									<label id="selected{{$simlr->id}}">Nothing selected</label>
                                   
                                  <!-- <input type="file" hidden id="choose" name="file{{$simlr->id}}" id="file{{$simlr->id}}">-->
                                   
                                   </div>
                                    
                                    </div>
                                    <img class="load-more" src="http://35.242.157.120/public/images/chat/sending.gif" height="100px" width="200px" style="display:none">
                                    <!--<a href="#"><i class="fa fa-paperclip"></i>
                                    </a>                                                                        -->
                                </form>
                
                                <!--<button id="send-location">Send Location</button>-->
                            </div>
                        </div>                    
                </div>
            </div>
           	
            </div> 
           <!-- chat popup-->
           
           <script>
		   	
		   $(document).ready(function(){ 
		   
		   $('#upload{{$simlr->id}}').click(function(){
                $("#file{{$simlr->id}}").click();
            })
                        
                            $("#file{{$simlr->id}}").change(function(e) {
                                     var fileName = e.target.files[0].name;
                                     $("#selected{{$simlr->id}}").html(fileName);
                                //alert('The file "' + fileName +  '" has been selected.');
                            }); 
                        });
                       
            var getFile = new selectFile;
            getFile.targets('file{{$simlr->id}}','selected{{$simlr->id}}');
            </script>
           <script>
            function set_smiley(smiley){
                $('#message{{$simlr->id}}').val(smiley);
                $('#message-form{{$simlr->id}}').submit();
            }
           </script>
           <script>
            $(document).ready(function(){ 
                $("#close{{$simlr->id}}").click(function() {
                    $('#myScrollchat{{$simlr->id}}').hide();
                    $("#profile_popup").removeClass("user_profile_popup");
                    $('#messages{{$simlr->id}}').html('');
                }); 
            });
            </script>
            <script>
            $(document).ready(function(){     
                    $('#myScrollchat{{$simlr->id}}').hide();
                    $("#profile_popup").removeClass("user_profile_popup");
            });
            </script>    
            <script>
            $(document).ready(function(){ 
                $("#chat-user-btn{{$simlr->id}}").click(function() {
                    $('.user_chat_popup').hide();
                    $('.chat__messages').html('');                    
                    $('#myScrollchat{{$simlr->id}}').show();
                    $("#profile_popup").addClass("user_profile_popup");
                }); 
            });    
            </script>
            <script>
            $(document).ready(function(){ 
                $("#chat-user-btnn{{$simlr->id}}").click(function() {
                    $('.user_chat_popup').hide();
                    $('.chat__messages').html('');                    
                    $('#myScrollchat{{$simlr->id}}').show();
                    $("#profile_popup").addClass("user_profile_popup");
                }); 
            });    
            </script>    
            @endforeach 
            
            
           @foreach($confirmchatrequest as $reqst)
           @foreach($reqst as $simlr)         
           <div class="user_chat_popup" id="myScrollchat{{$simlr->id}}" data-spy="affix" data-offset-top="20" data-offset-bottom="0">
            	<div class="chat-window-panel">
                <div class="chat-window-panel-inner">
            	<div class="chat-window-title">
                	<img id="chat_image{{$simlr->id}}" src="http://democarol.com/DatingApp/assets/front/images/user4.jpg"><span></span> <span id="chat_name{{$simlr->id}}"></span>
                    <span id="close{{$simlr->id}}" class="pull-right"><i class="fa fa-close"></i></span>
                </div>
                <div class="chat-window-box">                                
                  <ul id="messages{{$simlr->id}}" class="chat__messages">
                    <script id="message-template{{$simlr->id}}" type="text/template">

                    </script>
                  </ul>
                  
                    <!--<script id="location-message-template" type="text/template">
                        <li class="message">
                            <div class="message__title">
                                <h4>from1</h4>
                                <span>20-02-19</span>
                            </div>
                            <div class="message__body">
                                <a href="" target="_blank">My current location</a>
                            </div>
                        </li>
                    </script>-->    
                    
                    
                                    	
                </div>   
                <div class="chat_icon_outer">
                    	<div class="emoji-tape">
                        <span class="top-arrow"><i class="fa fa-angle-up"></i></span>
                        	<ul>
                            	<li>
                                	<div class="zoomed">
                                        <div class=""><img src="http://35.242.157.120/public/thumbnail/chat1.png" onclick="set_smiley('chat1.png','credits','50');">
                                        <span class="amount for-devaluation-disabled">50 credits</span>
                                    </div></div>
                                </li>
                                <li>
                                	<div class="zoomed">
                                        <div class=""><img src="http://35.242.157.120/public/thumbnail/chat2.png" onclick="set_smiley('chat2.png','coins','10');">
                                        <span class="amount for-devaluation-disabled">10 coins</span>
                                    </div></div>
                                </li>
                                <li>
                                	<div class="zoomed">
                                        <div class=""><img src="http://35.242.157.120/public/thumbnail/chat3.png" onclick="set_smiley('chat3.png','diamonds','40');">
                                        <span class="amount for-devaluation-disabled">40 diamonds</span>
                                    </div></div>
                                </li>
                                <li>
                                	<div class="zoomed">
                                        <div class=""><img src="http://35.242.157.120/public/thumbnail/chat4.png" onclick="set_smiley('chat4.png','credits','20');">
                                        <span class="amount for-devaluation-disabled">20 credits</span>
                                    </div></div>
                                </li>
                                <li>
                                	<div class="zoomed">
                                        <div class=""><img src="http://35.242.157.120/public/thumbnail/chat5.png" onclick="set_smiley('chat5.png','credits','40');">
                                        <span class="amount for-devaluation-disabled">40 credits</span>
                                    </div></div>
                                </li>
                            
                            
                            	
                                
                            </ul>
                        </div>
                    	<div class="emoji-list">
                        	<ul>
                            	<li><img src="http://35.242.157.120/public/thumbnail/chat1.png" onclick="set_smiley('chat1.png','credits','10');"><span>10 Credits</span></li>
                                <li><img src="http://35.242.157.120/public/thumbnail/chat2.png" onclick="set_smiley('chat2.png','credits','40');"><span>40 Credits</span></li>
                                <li><img src="http://35.242.157.120/public/thumbnail/chat3.png" onclick="set_smiley('chat3.png','credits','20');"><span>20 Credits</span></li>
                                <li><img src="http://35.242.157.120/public/thumbnail/chat4.png" onclick="set_smiley('chat4.png','credits','40');"><span>40 Credits</span></li>
                                <li><img src="http://35.242.157.120/public/thumbnail/chat5.png" onclick="set_smiley('chat5.png','credits','25');"><span>25 Credits</span></li>
                                <li><img src="http://35.242.157.120/public/thumbnail/chat6.png" onclick="set_smiley('chat6.png','credits','40');"><span>40 Credits</span></li>
                                <li><img src="http://35.242.157.120/public/thumbnail/chat7.png" onclick="set_smiley('chat7.png','credits','30');"><span>30 Credits</span></li>
                                <li><img src="http://35.242.157.120/public/thumbnail/chat8.png" onclick="set_smiley('chat8.png','credits','40');"><span>40 Credits</span></li>
                                <li><img src="http://35.242.157.120/public/thumbnail/chat9.png" onclick="set_smiley('chat9.png','credits','10');"><span>10 Credits</span></li>
                                <li><img src="http://35.242.157.120/public/thumbnail/chat10.png" onclick="set_smiley('chat10.png','credits','40');"><span>40 Credits</span></li>
                            </ul>
                        </div>
                    </div>
                
                                                     
                </div>
                
                <div class="chat-sender-box"> 
                
                	                   
                        <div class="chat__main">
                            
                
                            <div class="chat__footer">
                                <form id="message-form{{$simlr->id}}" method="POST" enctype="multipart/form-data">
                                    <input
                                        class="form-control"
                                        type="text"
                                        name="message{{$simlr->id}}"
                                        id="message{{$simlr->id}}"
                                        placeholder="type your message"
                                        autofocus
                                        autocomplete="false"
                                    />
                                    <input type="hidden" id="points{{$simlr->id}}">
                                    <input type="hidden" id="planType{{$simlr->id}}">
                                    <div class="chat_submit_box">
                                    <button type="submit" class="btn btn-primary chat-btn-text"><span class="chat-btn-text">send</span><span class="chat-btn-icon"><i class="fa fa-arrow-right"></i></span></button>
                                   <div class="chat_submit_box_inner"> 
                                   	<input type="file" hidden id="file{{$simlr->id}}" name="file{{$simlr->id}}">
									<button id="upload{{$simlr->id}}"><i class="fa fa-paperclip "></i></button>
									<label id="selected{{$simlr->id}}">Nothing selected</label>
                                   
                                  <!-- <input type="file" hidden id="choose" name="file{{$simlr->id}}" id="file{{$simlr->id}}">-->
                                   
                                   </div>
                                    
                                    </div>
                                    <img class="load-more" src="http://35.242.157.120/public/images/chat/sending.gif" height="100px" width="200px" style="display:none">
                                    <!--<a href="#"><i class="fa fa-paperclip"></i>
                                    </a>                                                                        -->
                                </form>
                
                                <!--<button id="send-location">Send Location</button>-->
                            </div>
                        </div>                    
                </div>
            </div>
           	
            </div> 
           <!-- chat popup-->
           
           <script>
		   	
		   $(document).ready(function(){ 
		   
		   $('#upload{{$simlr->id}}').click(function(){
                $("#file{{$simlr->id}}").click();
            })
                        
                            $("#file{{$simlr->id}}").change(function(e) {
                                     var fileName = e.target.files[0].name;
                                     $("#selected{{$simlr->id}}").html(fileName);
                                //alert('The file "' + fileName +  '" has been selected.');
                            }); 
                        });
                       
            var getFile = new selectFile;
            getFile.targets('file{{$simlr->id}}','selected{{$simlr->id}}');
            </script>
           <script>
            function set_smiley(smiley,planType,points){
                $('#planType{{$simlr->id}}').val(planType);
                $('#points{{$simlr->id}}').val(points);
                $('#message{{$simlr->id}}').val(smiley);
                $('#message-form{{$simlr->id}}').submit();
            }
           </script>
           <script>
            $(document).ready(function(){ 
                $("#close{{$simlr->id}}").click(function() {
                    $('#myScrollchat{{$simlr->id}}').hide();
                    $("#profile_popup").removeClass("user_profile_popup");
                    $('#messages{{$simlr->id}}').html('');
                }); 
            });
            </script>
            <script>
            $(document).ready(function(){     
                    $('#myScrollchat{{$simlr->id}}').hide();
                    $("#profile_popup").removeClass("user_profile_popup");
            });
            </script>    
            <script>
            $(document).ready(function(){ 
                $("#chat-user-btn{{$simlr->id}}").click(function() {
                    $('.user_chat_popup').hide();
                    $('.chat__messages').html('');                    
                    $('#myScrollchat{{$simlr->id}}').show();
                    $("#profile_popup").addClass("user_profile_popup");
                }); 
            });    
            </script>
            <script>
            $(document).ready(function(){ 
                $("#chat-user-btnn{{$simlr->id}}").click(function() {
                    $('.user_chat_popup').hide();
                    $('.chat__messages').html('');                    
                    $('#myScrollchat{{$simlr->id}}').show();
                    $("#profile_popup").addClass("user_profile_popup");
                }); 
            });    
            </script>    
            @endforeach
            @endforeach
            
            
            
            <!--- -->
            
            
                
          </div>
        <div class="clearfix"></div>
        @include('chat_sidebar');
        </div>
    </div>
</section>
<script type="text/javascript">
  var page = 1;
  var is_login = '<?php echo (Auth::id()); ?>';
  $(window).scroll(function() {  
      if($(window).scrollTop() + $(window).height() >= $(document).height()- 0.5) {
          page++;
          if(is_login){
            loadMoreData(page);
          }
      }
  });


  function loadMoreData(page){
    var currentURL = $(location).attr("href");
    var n = currentURL.includes("?");
    var url;
    if(n){
      currentURL = currentURL.split("?");
      url = '?page=' + page + '&' + currentURL[1];
    }
    else{
      url = '?page=' + page
    }
    $.ajax(
          {
              headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              },
              url: url,
              type: "get",
              beforeSend: function()
              {
                  $('.load-more').show();
              }
          })
          .done(function(data)
          { //console.log(data);
              if(data.html == ""){
                  $('.load-more').text("");
                  return;
              }
              $('.load-more').hide();
              $("#all-users").append(data.html);
          })
          .fail(function(jqXHR, ajaxOptions, thrownError)
          {
                alert('server not responding...');
          });
  }
</script>
<script>
$(document).ready(function(){ 
    $("#close").click(function() {
        $('#myScrollchat').hide();
		$("#profile_popup").removeClass("user_profile_popup");
    }); 
});
</script>
<script>
$(document).ready(function(){     
        $('#myScrollchat').hide();
		$("#profile_popup").removeClass("user_profile_popup");
});
</script>    
<script>
$(document).ready(function(){ 
    $(".chat-user-btn").click(function() {
        $('#myScrollchat').show();
		$("#profile_popup").addClass("user_profile_popup");
    }); 
});    
</script>
    

<input type="hidden"  name="sender_id" id="sender_id" value="{{ isset($users->id) ? $users->id : '' }}">
<input type="hidden"  name="sender_name" id="sender_name" value="{{ isset($users->name) ? $users->name : ''}}">
<input type="hidden"  name="sender_image" id="sender_image" value="{{ isset($users->profile_image) ? $users->profile_image : '' }}">

<input type="hidden"  name="receiver_id" id="receiver_id" value="">
<input type="hidden"  name="receiver_name" id="receiver_name" value="">
<input type="hidden"  name="receiver_image" id="receiver_image" value="">
<input type="hidden"  name="room_id" id="room_id" value="">
<input type="hidden"  name="chat_image" id="chat_image" value="">

<script>
    function set_receiver(id, name, image){
        //alert(name);
        //gethistory();
        $('#receiver_id').val(id);
        $('#receiver_name').val(name);
        $('#chat_name'+id).text(name);
        $('#chat_image'+id).attr('src','http://35.242.157.120/public/thumbnail/'+image);        
        $('#receiver_image').val(image);
        
        var sender_id = '{{ isset($users->id) ? $users->id : '' }}';
        var receiver_id = id;                        
        //alert(sender_id);
        //alert(receiver_id);
        $.ajax({
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '<?php echo url("/")?>/get_chat_id',
            type: 'POST',
            cache : false,
            processData: false,
            data: 'sender_id='+sender_id+'&receiver_id='+receiver_id, // serializes the form's elements.
            success: function(data)
            {
             console.log(data);
             $('#room_id').val(data);
                    var URL2 = "http://35.242.157.120:9090/history?chatId="+data;
                    $.ajax ( {
                          //headers: {
                          //  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                          //},
                          crossdomain:true,
                          async:false,
                        type: "GET",
                        url: URL2,
                        dataType: "json",
                        success: response => {
                            console.log(response);
                            $.each(response, function(index, element) {
                                var pos   = (element.senderName == '{{ isset($users->name) ? $users->name : '' }}') ? 'right' : 'left';
                                var image   = (element.senderName == '{{ isset($users->name) ? $users->name : '' }}') ? '{{ isset($users->profile_image) ? $users->profile_image : '' }}' : $('#receiver_image').val();
                                var timestamp = element._id.toString().substring(0, 8);
                                var options = {
                                    year: "numeric",
                                    month: "numeric",
                                    day: "numeric",
                                    hour: "numeric",
                                    minute: "numeric",
                                    second: "numeric"
                                };
                                var cdate = new Date(
                                                parseInt(timestamp, 16) * 1000
                                            ).toLocaleDateString("en-US", options);
                                            
                                var ndate = new Date(cdate);
                                var days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
                                var d = new Date();
                                if((d.toDateString() === ndate.toDateString())){
                                    var mdate = (ndate.getHours()+':'+ndate.getMinutes());
                                }else{
                                    var mdate = (days[ndate.getDay()]);
                                }
                                
                                if(element.type === 'text'){
                                    if(element.message){
                                        var extens = element.message.substr( (element.message.lastIndexOf('.') +1) );
                                        
                                        if(extens == 'png'){
                                            $('#messages'+id).append('<li class="message">'+                    
                                                '<div class="chat-user-'+pos+'">'+
                                                    '<div class="chat-user-box">'+
                                                        '<img src="http://35.242.157.120/public/thumbnail/'+element.senderImageUrl+'">'+
                                                        '<span>'+element.senderName+'</span>'+
                                                    '</div>'+
                                                    '<div class="chat-user-message">'+
                                                        '<div class="chat-line"><span><img src="http://35.242.157.120/public/thumbnail/'+element.message+'"></span></div>'+                                   
                                                        '<div class="meta-info"><span class="date">'+mdate+'</span></div>'+
                                                    '</div>'+
                                                '</div>'+
                                            '</li>');
                                        }else{
                                            $('#messages'+id).append('<li class="message">'+                    
                                                '<div class="chat-user-'+pos+'">'+
                                                    '<div class="chat-user-box">'+
                                                        '<img src="http://35.242.157.120/public/thumbnail/'+element.senderImageUrl+'">'+
                                                        '<span>'+element.senderName+'</span>'+
                                                    '</div>'+
                                                    '<div class="chat-user-message">'+
                                                        '<div class="chat-line"><span>'+element.message+'</span></div>'+                                   
                                                        '<div class="meta-info"><span class="date">'+mdate+'</span></div>'+
                                                    '</div>'+
                                                '</div>'+
                                            '</li>');   
                                        }                                        
                                    }
                                }else if(element.type === 'file') {
                                    var extension = element.fileName.substr( (element.fileName.lastIndexOf('.') +1) );
                                    console.log(extension);
                                    if(extension === 'jpeg' || extension === 'jpg' || extension === 'png'){
                                        if(element.fileName){
                                        $('#messages'+id).append('<li class="message">'+                    
                                                '<div class="chat-user-'+pos+'">'+
                                                    '<div class="chat-user-box">'+
                                                        '<img src="http://35.242.157.120/public/thumbnail/'+element.senderImageUrl+'">'+
                                                        '<span>'+element.senderName+'</span>'+
                                                    '</div>'+
                                                    '<div class="chat-user-message">'+
                                                        '<div class="chat-line"><span><img src="http://35.242.157.120/public/images/profile_image/'+element.fileName+'" width="200px" height="200px"><br>'+
                                                        ''+element.fileText+'</span></div>'+                                   
                                                        '<div class="meta-info"><span class="date">'+mdate+'</span></div>'+
                                                    '</div>'+                                                    
                                                '</div>'+
                                            '</li>');
                                        }
                                    }else if(extension === 'mp3'){
                                        if(element.fileName){
                                        $('#messages'+id).append('<li class="message">'+                    
                                                '<div class="chat-user-'+pos+'">'+
                                                    '<div class="chat-user-box">'+
                                                        '<img src="http://35.242.157.120/public/thumbnail/'+element.senderImageUrl+'">'+
                                                        '<span>'+element.senderName+'</span>'+
                                                    '</div>'+
                                                    '<div class="chat-user-message">'+
                                                        '<div class="chat-line"><span>'+
                                                        '<audio controls><source src="http://35.242.157.120/public/images/profile_image/'+element.fileName+'" type="audio/mpeg"></audio><br>'+                                            
                                                        ''+element.fileText+'</span></div>'+                                   
                                                        '<div class="meta-info"><span class="date">'+mdate+'</span></div>'+
                                                    '</div>'+
                                                '</div>'+
                                            '</li>');
                                        }
                                    }else if(extension === 'mp4'){
                                        if(element.fileName){
                                        $('#messages'+id).append('<li class="message">'+                    
                                                '<div class="chat-user-'+pos+'">'+
                                                    '<div class="chat-user-box">'+
                                                        '<img src="http://35.242.157.120/public/thumbnail/'+element.senderImageUrl+'">'+
                                                        '<span>'+element.senderName+'</span>'+
                                                    '</div>'+
                                                    '<div class="chat-user-message">'+
                                                        '<div class="chat-line"><span>'+
                                                        '<video width="200" height="180" controls><source src="http://35.242.157.120/public/images/profile_image/'+element.fileName+'" type="video/mp4"></video><br>'+                                            
                                                        ''+element.fileText+'</span></div>'+                                   
                                                        '<div class="meta-info"><span class="date">'+mdate+'</span></div>'+
                                                    '</div>'+
                                                '</div>'+
                                            '</li>');
                                        }
                                    }else{
                                        if(element.fileName){
                                        $('#messages'+id).append('<li class="message">'+                    
                                                '<div class="chat-user-'+pos+'">'+
                                                    '<div class="chat-user-box">'+
                                                        '<img src="http://35.242.157.120/public/thumbnail/'+element.senderImageUrl+'">'+
                                                        '<span>'+element.senderName+'</span>'+
                                                    '</div>'+
                                                    '<div class="chat-user-message">'+
                                                        '<div class="chat-line"><span><img src="http://35.242.157.120/public/images/profile_image/'+element.fileName+'" width="200px" height="200px"><br>'+
                                                        ''+element.text+'</span></div>'+                                   
                                                        '<div class="meta-info"><span class="date">'+mdate+'</span></div>'+
                                                    '</div>'+                                                    
                                                '</div>'+
                                            '</li>');
                                        } 
                                    }
                                }
                            });                                
                        }
                    });
             
            }
        });
        
        var roomid = $('#room_id').val();
        //alert(roomid);
        
    }
</script>
<!--- chat scripts  -->
<script>
        var socket = io.connect("http://35.242.157.120:9090");

        function scrollToBottom() {
            var messages = jQuery("#messages");
            var newMessage = messages.children("li:last-child");
            var clientHeight = messages.prop("clientHeight");
            var scrollTop = messages.prop("scrollTop");
            var scrollHeight = messages.prop("scrollHeight");

            var newMessageHeight = newMessage.innerHeight();
            var lastMessageHeight = newMessage.prev().innerHeight();
            if (
                clientHeight +
                    scrollTop +
                    lastMessageHeight +
                    newMessageHeight >=
                scrollHeight
            ) {
                messages.scrollTop(scrollHeight);
            }
        }

        socket.on("connect", function() {
            // var params = jQuery.deparam(window.location.search);
            var params = { name: jQuery('#sender_name').val(), room: jQuery('#room_id').val() };

            socket.emit("join", params, function(err) {
                console.log("emited");
            });
            
            //ON READ
            //socket.emit(
            //    "onread",
            //    {
            //        messageId: "1234567"
            //    },
            //    () => {}
            //);
            
            console.log(params);
            console.log("Connected to Server");
            
            socket.on("newMessage", function(message) {
                console.log(message);
                if (message.text !== "" || message.from !== "") {
                    
                    var formattedTime = moment(message.createdAt).format(
                        "h:mm"
                    );
                    //var template = jQuery("#message-template"+jQuery('#receiver_id').val()).html();
                    //var html = Mustache.render(template, {
                    //    text: message.text,
                    //    from: message.from,
                    //    createdAt: formattedTime
                    //});
                    //console.log(html);
                
                
                var pos   = (message.from == '{{ isset($users->name) ? $users->name : '' }}') ? 'right' : 'left';
                var image   = (message.from == '{{ isset($users->name) ? $users->name : '' }}') ? '{{ isset($users->profile_image) ? $users->profile_image : '' }}' : $('#receiver_image').val();
                
                if(message.type == 'text'){
                    var ext = message.text.substr( (message.text.lastIndexOf('.') +1) );
                    if(ext == 'png'){
                        var html1 = '<li class="message">'+                    
                                    '<div class="chat-user-'+pos+'">'+
                                        '<div class="chat-user-box">'+
                                            '<img src="http://35.242.157.120/public/thumbnail/'+message.senderImageUrl+'">'+
                                            '<span>'+message.from+'</span>'+
                                        '</div>'+
                                        '<div class="chat-user-message">'+
                                            '<div class="chat-line"><span><img src="http://35.242.157.120/public/thumbnail/'+message.text+'"></span></div>'+                                   
                                            '<div class="meta-info"><span class="date">'+formattedTime+'</span></div>'+
                                        '</div>'+
                                    '</div>'+
                                '</li>';    
                    }else{
                        var html1 = '<li class="message">'+                    
                                    '<div class="chat-user-'+pos+'">'+
                                        '<div class="chat-user-box">'+
                                            '<img src="http://35.242.157.120/public/thumbnail/'+message.senderImageUrl+'">'+
                                            '<span>'+message.from+'</span>'+
                                        '</div>'+
                                        '<div class="chat-user-message">'+
                                            '<div class="chat-line"><span>'+message.text+'</span></div>'+                                   
                                            '<div class="meta-info"><span class="date">'+formattedTime+'</span></div>'+
                                        '</div>'+
                                    '</div>'+
                                '</li>';    
                    }
                            
                }else if(message.type == 'file') {
                            var extension = message.fileName.substr( (message.fileName.lastIndexOf('.') +1) );
                            console.log(extension);
                            if(extension === 'jpeg' || extension === 'jpg' || extension === 'png'){
                                var html1 = '<li class="message">'+                    
                                    '<div class="chat-user-'+pos+'">'+
                                        '<div class="chat-user-box">'+
                                            '<img src="http://35.242.157.120/public/thumbnail/'+message.senderImageUrl+'">'+
                                            '<span>'+message.from+'</span>'+
                                        '</div>'+
                                        '<div class="chat-user-message">'+
                                            '<div class="chat-line"><span><img src="http://35.242.157.120/public/images/profile_image/'+message.fileName+'" width="200px" height="200px"><br>'+
                                            ''+message.text+'</span></div>'+                                   
                                            '<div class="meta-info"><span class="date">'+formattedTime+'</span></div>'+
                                        '</div>'+
                                    '</div>'+
                                '</li>';   
                            }else if(extension === 'mp3'){
                                var html1 = '<li class="message">'+                    
                                    '<div class="chat-user-'+pos+'">'+
                                        '<div class="chat-user-box">'+
                                            '<img src="http://35.242.157.120/public/thumbnail/'+message.senderImageUrl+'">'+
                                            '<span>'+message.from+'</span>'+
                                        '</div>'+
                                        '<div class="chat-user-message">'+
                                            '<div class="chat-line"><span>'+
                                            '<audio controls><source src="http://35.242.157.120/public/images/profile_image/'+message.fileName+'" type="audio/mpeg"></audio><br>'+                                            
                                            ''+message.text+'</span></div>'+                                   
                                            '<div class="meta-info"><span class="date">'+formattedTime+'</span></div>'+
                                        '</div>'+
                                    '</div>'+
                                '</li>';
                            }else if(extension === 'mp4'){
                                var html1 = '<li class="message">'+                    
                                    '<div class="chat-user-'+pos+'">'+
                                        '<div class="chat-user-box">'+
                                            '<img src="http://35.242.157.120/public/thumbnail/'+message.senderImageUrl+'">'+
                                            '<span>'+message.from+'</span>'+
                                        '</div>'+
                                        '<div class="chat-user-message">'+
                                            '<div class="chat-line"><span>'+
                                            '<video width="200" height="100" controls><source src="http://35.242.157.120/public/images/profile_image/'+message.fileName+'" type="video/mp4"></video><br>'+                                            
                                            ''+message.text+'</span></div>'+                                   
                                            '<div class="meta-info"><span class="date">'+formattedTime+'</span></div>'+
                                        '</div>'+
                                    '</div>'+
                                '</li>';
                            }else{
                                var html1 = '<li class="message">'+                    
                                    '<div class="chat-user-'+pos+'">'+
                                        '<div class="chat-user-box">'+
                                            '<img src="http://35.242.157.120/public/thumbnail/'+message.senderImageUrl+'">'+
                                            '<span>'+message.from+'</span>'+
                                        '</div>'+
                                        '<div class="chat-user-message">'+
                                            '<div class="chat-line"><span><img src="http://35.242.157.120/public/images/profile_image/'+message.fileName+'" width="200px" height="200px"><br>'+
                                            ''+message.text+'</span></div>'+                                   
                                            '<div class="meta-info"><span class="date">'+formattedTime+'</span></div>'+
                                        '</div>'+
                                    '</div>'+
                                '</li>'; 
                            }
                    
                }
                
                
                console.log("#messages"+jQuery('#receiver_id').val());
                jQuery("#messages"+jQuery('#receiver_id').val()).append(html1);
                }
                scrollToBottom();
            });
        });

        socket.on("updateUserList", function(users) {
            var ol = jQuery("<ol></ol>");
            users.forEach(function(user) {
                ol.append(jQuery("<li></li>").text(user));
            });

            jQuery("#users").html(ol);
        });

        socket.on("newLocationMessage", function(message) {
            var formattedTime = moment(message.createdAt).format("h:mm:a");
            var template = jQuery("#location-message-template").html();
            var html = Mustache.render(template, {
                text: message.url,
                from: message.from,
                createdAt: formattedTime
            });

            jQuery("#messages").append(html);

            scrollToBottom();

            /*var li = jQuery('<li></li>');
        var a = jQuery('<a target="_blank">My current Location</a>');

        var formattedTime = moment(message.createdAt).format('h:mm:a');
        li.text(`${message.from} ${formattedTime}:`);
        a.attr('href', message.url);
        li.append(a);
        jQuery('#messages').append(li);*/
        });

        socket.on("disconnect", function() {
            console.log("Disconnected from Server");
        });

        
        
        
        
        // var locationButton = jQuery("#send-location");
        // locationButton.on("click", function() {
        //     if (!navigator.geolocation) {
        //         return alert("Geolocation not supported by your browser");
        //     }

        //     locationButton.attr("disabled", "disabled").text("Sending location...");

        //     navigator.geolocation.getCurrentPosition(
        //         function(position) {
        //             locationButton.removeAttr("disabled").text("Send Location");
        //             socket.emit("createLocationMessage", {
        //                 latitude: position.coords.latitude,
        //                 longitude: position.coords.longitude
        //             });
        //         },
        //         function() {
        //             locationButton.attr("disabled", "disabled").text("Second Location");
        //             alert("Unable to fetch location");
        //         }
        //     );
        // });
    </script>
    
    
    @foreach($chatrequest as $simlr)
    <script>
    $('#file{{$simlr->id}}').on('change', function(){
        var reader = new FileReader();
        reader.onload = function (event) {
          console.log(event.target.result);
          $('#chat_file_hidden').val(reader.result);
        }
        reader.readAsDataURL(this.files[0]);
        console.log('readed '+file{{$simlr->id}});
        console.log(reader.result);
      });
    </script>    
    <script>
        jQuery("#message-form{{$simlr->id}}").on("submit", function(e) {            
            //var fd = new FormData();
            //fd.append("file", $("#file").file[0])
            
            $.ajax({
                headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '<?php echo url("/")?>/update_conversation',
                type: 'POST',
                cache : false,
                processData: false,
                data: 'room_id='+jQuery('#room_id').val(), // serializes the form's elements.
                success: function(data)
                {
                 console.log(data);                 
                }
            });
            
            e.preventDefault();
            var messageTextbox = jQuery("[name=message]");
            console.log('new message');
            console.log(jQuery('#receiver_id').val());
            console.log(jQuery('#receiver_name').val());
            console.log(jQuery('#room_id').val());
            console.log(jQuery('#sender_image').val());
            console.log(jQuery('#receiver_image').val());
            console.log(jQuery("[name=message{{$simlr->id}}]").val());
            
            var f = jQuery('#file{{$simlr->id}}').val();
            var extension = f.substr( (f.lastIndexOf('.') +1) );
            //if(cradits < msgcount){}
            //else{
            //    
            //}
            var type = (jQuery("#file{{$simlr->id}}").val() == '') ? 'text' : 'file';
            if(type==="text"){
                console.log('type : text');
                 var mydiamonds = localStorage.getItem("diamonds");
                 var mycoins = localStorage.getItem("coins");
                 var mycredits = localStorage.getItem("credits");
                 var mypremiums = localStorage.getItem("premiums");
                 
                 if(mydiamonds == 'null'){ mydiamonds = 0;}
                 if(mycoins == 'null'){ mycoins = 0;}
                 if(mycredits == 'null'){ mycredits = 0;}
                 if(mypremiums  == 'null'){ mypremiums = 0;}
                 
                 var requiredDiamonds = 0;
                 var requiredCoins = 0;
                 var requiredCredits = 0;
                 var requiredPremiums = 0;
                 
                 var planType = jQuery("#planType{{$simlr->id}}").val();
                 if(planType === 'diamonds'){ requiredDiamonds = jQuery("#points{{$simlr->id}}").val(); }
                 if(planType === 'coins'){ requiredCoins = jQuery("#points{{$simlr->id}}").val(); }
                 if(planType === 'credits'){ requiredCredits = jQuery("#points{{$simlr->id}}").val(); }
                 if(planType === 'premiums'){ requiredPremiums = jQuery("#points{{$simlr->id}}").val(); }
                 //alert(mycredits); alert(requiredCredits);
                 if(parseInt(mydiamonds) >= parseInt(requiredDiamonds) && parseInt(mycoins) >= parseInt(requiredCoins) && parseInt(mycredits) >= parseInt(requiredCredits) && parseInt(mypremiums) >= parseInt(requiredPremiums)){                 
                        var leftdiamonds = (mydiamonds - requiredDiamonds);
                        var leftcoins = (mycoins - requiredCoins);
                        var leftcredits = (mycredits - requiredCredits);
                        var leftpremiums = (mypremiums - requiredPremiums);
                        
                        localStorage.setItem("diamonds",leftdiamonds);
                        localStorage.setItem("coins",leftcoins);
                        localStorage.setItem("credits",leftcredits);
                        localStorage.setItem("premiums",leftpremiums);
                        socket.emit(
                            "createMessage",
                            {
                                text: jQuery("[name=message{{$simlr->id}}]").val(),
                                senderName: jQuery('#sender_name').val(),
                                receiverName: jQuery('#receiver_name').val(),
                                senderId: jQuery('#sender_id').val(),
                                receiverId: jQuery('#receiver_id').val(),
                                chatID: jQuery('#room_id').val(),
                                type : "text", 
                                readStatus: false,
                                answereStatus: false,
                                senderImageUrl: jQuery('#sender_image').val(),
                                receiverImageUrl: jQuery('#receiver_image').val()
                            },
                            function() {
                                jQuery("[name=message{{$simlr->id}}]").val("");
                            }
                        );
                 }else{                    
                    jQuery("[name=message{{$simlr->id}}]").val("");
                        console.log(' msg '+msgs);
                        var htm = '<li class="message">'+                    
                                    '<div class="chat-user-right">'+
                                        '<div class="chat-user-box">'+
                                            '<img src="http://35.242.157.120/public/thumbnail/'+jQuery('#sender_image').val()+'">'+
                                            '<span>'+jQuery('#sender_name').val()+'</span>'+
                                        '</div>'+
                                        '<div class="chat-user-message">'+
                                            '<div class="chat-line"><span style="color:red">Fail!</span> <span><img src="http://35.242.157.120/public/thumbnail/'+msgs+'"></span></div>'+                                   
                                            '<div class="meta-info"><span class="date"></span></div>'+
                                        '</div>'+
                                    '</div>'+
                                '</li>';
                       jQuery("#messages"+jQuery('#receiver_id').val()).append(htm);
                       setTimeout(function(){ jQuery('#upgrade_myModal').modal('show'); }, 3000);
                       
                 }
                 
                 
            }
            else{
                console.log('type : file');
                console.log('extension : '+extension);
                $('.load-more').show();
                $('.chat-btn-text').attr('disabled',true);
                
                $.ajax({
                    headers: {
                          'X-CSRF-TOKEN': '{{csrf_token()}}'
                    },
                    url: "{{url('chat-file')}}",
                    type: "POST",
                    //data:{file : $("#file{{$simlr->id}}").val()},
                    data:{
                        base64: ($("#chat_file_hidden").val()),
                        extension : extension
                      },
                    //data: $("#message-form{{$simlr->id}}").serialize(),
                    success:function(dataa)
                    {
                        console.log(dataa);
                        $('.load-more').hide();
                        $('.chat-btn-text').attr('disabled',false);
                        //$('#uploaded_image').html(data);
                        jQuery('#chat_image').val(dataa);
                        
                        socket.emit(
                            "createMessage",
                            {
                                fileText: jQuery("[name=message{{$simlr->id}}]").val(),
                                fileName : dataa,
                                senderName: jQuery('#sender_name').val(),
                                receiverName: jQuery('#receiver_name').val(),
                                senderId: jQuery('#sender_id').val(),
                                receiverId: jQuery('#receiver_id').val(),
                                chatID: jQuery('#room_id').val(),
                                type : "file", 
                                readStatus: false,
                                answereStatus: false,
                                senderImageUrl: jQuery('#sender_image').val(),
                                receiverImageUrl: jQuery('#receiver_image').val()
                            },
                            function() {
                                jQuery("[name=message{{$simlr->id}}]").val("");
                                jQuery('#file{{$simlr->id}}').val('');
                                jQuery('#selected{{$simlr->id}}').val('');                                
                            }
                        );
                    }
                  });
                  
            }
           
        });
    </script>
    @endforeach
    
    
    @foreach($confirmchatrequest as $rqstt)
    @foreach($rqstt as $simlr)        
    <script>
    $('#file{{$simlr->id}}').on('change', function(){
        var reader = new FileReader();
        reader.onload = function (event) {
          console.log(event.target.result);
          $('#chat_file_hidden').val(reader.result);
        }
        reader.readAsDataURL(this.files[0]);
        console.log('readed '+file{{$simlr->id}});
        console.log(reader.result);
      });
    </script>    
    <script>
        jQuery("#message-form{{$simlr->id}}").on("submit", function(e) {            
            //var fd = new FormData();
            //fd.append("file", $("#file").file[0])            
            
            e.preventDefault();
            var messageTextbox = jQuery("[name=message]");
            console.log('new message');
            console.log(jQuery('#receiver_id').val());
            console.log(jQuery('#receiver_name').val());
            console.log(jQuery('#room_id').val());
            console.log(jQuery('#sender_image').val());
            console.log(jQuery('#receiver_image').val());
            console.log(jQuery("[name=message{{$simlr->id}}]").val());
            
            var f = jQuery('#file{{$simlr->id}}').val();
            var extension = f.substr( (f.lastIndexOf('.') +1) );
            var msgs = jQuery("[name=message{{$simlr->id}}]").val();
            //if(cradits < msgcount){}
            //else{
            //    
            //}
            var type = (jQuery("#file{{$simlr->id}}").val() == '') ? 'text' : 'file';
            if(type==="text"){
                console.log('type : text');
                 var mydiamonds = localStorage.getItem("diamonds");
                 var mycoins = localStorage.getItem("coins");
                 var mycredits = localStorage.getItem("credits");
                 var mypremiums = localStorage.getItem("premiums");
                 
                 if(mydiamonds == 'null'){ mydiamonds = 0;}
                 if(mycoins == 'null'){ mycoins = 0;}
                 if(mycredits == 'null'){ mycredits = 0;}
                 if(mypremiums  == 'null'){ mypremiums = 0;}
                 
                 var requiredDiamonds = 0;
                 var requiredCoins = 0;
                 var requiredCredits = 0;
                 var requiredPremiums = 0;
                 
                 var planType = jQuery("#planType{{$simlr->id}}").val();
                 if(planType === 'diamonds'){ requiredDiamonds = jQuery("#points{{$simlr->id}}").val(); }
                 if(planType === 'coins'){ requiredCoins = jQuery("#points{{$simlr->id}}").val(); }
                 if(planType === 'credits'){ requiredCredits = jQuery("#points{{$simlr->id}}").val(); }
                 if(planType === 'premiums'){ requiredPremiums = jQuery("#points{{$simlr->id}}").val(); }
                 //alert(mycredits); alert(requiredCredits);
                 if(parseInt(mydiamonds) >= parseInt(requiredDiamonds) && parseInt(mycoins) >= parseInt(requiredCoins) && parseInt(mycredits) >= parseInt(requiredCredits) && parseInt(mypremiums) >= parseInt(requiredPremiums)){                 
                        var leftdiamonds = (mydiamonds - requiredDiamonds);
                        var leftcoins = (mycoins - requiredCoins);
                        var leftcredits = (mycredits - requiredCredits);
                        var leftpremiums = (mypremiums - requiredPremiums);
                        
                        localStorage.setItem("diamonds",leftdiamonds);
                        localStorage.setItem("coins",leftcoins);
                        localStorage.setItem("credits",leftcredits);
                        localStorage.setItem("premiums",leftpremiums);
                        socket.emit(
                            "createMessage",
                            {
                                text: jQuery("[name=message{{$simlr->id}}]").val(),
                                senderName: jQuery('#sender_name').val(),
                                receiverName: jQuery('#receiver_name').val(),
                                senderId: jQuery('#sender_id').val(),
                                receiverId: jQuery('#receiver_id').val(),
                                chatID: jQuery('#room_id').val(),
                                type : "text", 
                                readStatus: false,
                                answereStatus: false,
                                senderImageUrl: jQuery('#sender_image').val(),
                                receiverImageUrl: jQuery('#receiver_image').val()
                            },
                            function() {
                                jQuery("[name=message{{$simlr->id}}]").val("");
                            }
                        );
                 }else{                    
                    jQuery("[name=message{{$simlr->id}}]").val("");
                        console.log(' msg '+msgs);
                        var htm = '<li class="message">'+                    
                                    '<div class="chat-user-right">'+
                                        '<div class="chat-user-box">'+
                                            '<img src="http://35.242.157.120/public/thumbnail/'+jQuery('#sender_image').val()+'">'+
                                            '<span>'+jQuery('#sender_name').val()+'</span>'+
                                        '</div>'+
                                        '<div class="chat-user-message">'+
                                            '<div class="chat-line"><span style="color:red">Fail!</span> <span><img src="http://35.242.157.120/public/thumbnail/'+msgs+'"></span></div>'+                                   
                                            '<div class="meta-info"><span class="date"></span></div>'+
                                        '</div>'+
                                    '</div>'+
                                '</li>';
                       jQuery("#messages"+jQuery('#receiver_id').val()).append(htm);
                       setTimeout(function(){ jQuery('#upgrade_myModal').modal('show'); }, 3000);
                       
                 }
                 
                 
            }
            else{
                console.log('type : file');
                console.log('extension : '+extension);
                $('.load-more').show();
                $('.chat-btn-text').attr('disabled',true);
                
                $.ajax({
                    headers: {
                          'X-CSRF-TOKEN': '{{csrf_token()}}'
                    },
                    url: "{{url('chat-file')}}",
                    type: "POST",
                    //data:{file : $("#file{{$simlr->id}}").val()},
                    data:{
                        base64: ($("#chat_file_hidden").val()),
                        extension : extension
                      },
                    //data: $("#message-form{{$simlr->id}}").serialize(),
                    success:function(dataa)
                    {
                        console.log(dataa);
                        $('.load-more').hide();
                        $('.chat-btn-text').attr('disabled',false);
                        //$('#uploaded_image').html(data);
                        jQuery('#chat_image').val(dataa);
                        
                        socket.emit(
                            "createMessage",
                            {
                                fileText: jQuery("[name=message{{$simlr->id}}]").val(),
                                fileName : dataa,
                                senderName: jQuery('#sender_name').val(),
                                receiverName: jQuery('#receiver_name').val(),
                                senderId: jQuery('#sender_id').val(),
                                receiverId: jQuery('#receiver_id').val(),
                                chatID: jQuery('#room_id').val(),
                                type : "file", 
                                readStatus: false,
                                answereStatus: false,
                                senderImageUrl: jQuery('#sender_image').val(),
                                receiverImageUrl: jQuery('#receiver_image').val()
                            },
                            function() {
                                jQuery("[name=message{{$simlr->id}}]").val("");
                                jQuery('#file{{$simlr->id}}').val('');
                                jQuery('#selected{{$simlr->id}}').val('');                                
                            }
                        );
                    }
                  });
                  
            }
           
        });
    </script>
    @endforeach
    @endforeach
    
    @foreach($fakechatrequest as $simlr)
    <script>
    $('#file{{$simlr->id}}').on('change', function(){
        var reader = new FileReader();
        reader.onload = function (event) {
          console.log(event.target.result);
          $('#chat_file_hidden').val(reader.result);
        }
        reader.readAsDataURL(this.files[0]);
        console.log('readed '+file{{$simlr->id}});
        console.log(reader.result);
      });
    </script>    
    <script>
        jQuery("#message-form{{$simlr->id}}").on("submit", function(e) {            
            //var fd = new FormData();
            //fd.append("file", $("#file").file[0])
            $.ajax({
                headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '<?php echo url("/")?>/update_conversation',
                type: 'POST',
                cache : false,
                processData: false,
                data: 'room_id='+jQuery('#room_id').val(), // serializes the form's elements.
                success: function(data)
                {
                 console.log(data);                 
                }
            });
            
            e.preventDefault();
            var messageTextbox = jQuery("[name=message]");
            console.log('new message');
            console.log(jQuery('#receiver_id').val());
            console.log(jQuery('#receiver_name').val());
            console.log(jQuery('#room_id').val());
            console.log(jQuery('#sender_image').val());
            console.log(jQuery('#receiver_image').val());
            console.log(jQuery("[name=message{{$simlr->id}}]").val());
            
            var f = jQuery('#file{{$simlr->id}}').val();
            var extension = f.substr( (f.lastIndexOf('.') +1) );
            //if(cradits < msgcount){}
            //else{
            //    
            //}
            var type = (jQuery("#file{{$simlr->id}}").val() == '') ? 'text' : 'file';
            if(type==="text"){
                console.log('type : text');
                 var mydiamonds = localStorage.getItem("diamonds");
                 var mycoins = localStorage.getItem("coins");
                 var mycredits = localStorage.getItem("credits");
                 var mypremiums = localStorage.getItem("premiums");
                 
                 if(mydiamonds == 'null'){ mydiamonds = 0;}
                 if(mycoins == 'null'){ mycoins = 0;}
                 if(mycredits == 'null'){ mycredits = 0;}
                 if(mypremiums  == 'null'){ mypremiums = 0;}
                 
                 var requiredDiamonds = 0;
                 var requiredCoins = 0;
                 var requiredCredits = 0;
                 var requiredPremiums = 0;
                 
                 var planType = jQuery("#planType{{$simlr->id}}").val();
                 console.log(planType);
                 if(planType === 'diamonds'){ requiredDiamonds = jQuery("#points{{$simlr->id}}").val(); }
                 if(planType === 'coins'){ requiredCoins = jQuery("#points{{$simlr->id}}").val(); }
                 if(planType === 'credits'){ requiredCredits = jQuery("#points{{$simlr->id}}").val(); }
                 if(planType === 'premiums'){ requiredPremiums = jQuery("#points{{$simlr->id}}").val(); }
                 //alert(mycredits); alert(requiredCredits);
                 if(parseInt(mydiamonds) >= parseInt(requiredDiamonds) && parseInt(mycoins) >= parseInt(requiredCoins) && parseInt(mycredits) >= parseInt(requiredCredits) && parseInt(mypremiums) >= parseInt(requiredPremiums)){                 
                        var leftdiamonds = (mydiamonds - requiredDiamonds);
                        var leftcoins = (mycoins - requiredCoins);
                        var leftcredits = (mycredits - requiredCredits);
                        var leftpremiums = (mypremiums - requiredPremiums);
                        
                        localStorage.setItem("diamonds",leftdiamonds);
                        localStorage.setItem("coins",leftcoins);
                        localStorage.setItem("credits",leftcredits);
                        localStorage.setItem("premiums",leftpremiums);
                        socket.emit(
                            "createMessage",
                            {
                                text: jQuery("[name=message{{$simlr->id}}]").val(),
                                senderName: jQuery('#sender_name').val(),
                                receiverName: jQuery('#receiver_name').val(),
                                senderId: jQuery('#sender_id').val(),
                                receiverId: jQuery('#receiver_id').val(),
                                chatID: jQuery('#room_id').val(),
                                type : "text", 
                                readStatus: false,
                                answereStatus: false,
                                senderImageUrl: jQuery('#sender_image').val(),
                                receiverImageUrl: jQuery('#receiver_image').val()
                            },
                            function() {
                                jQuery("[name=message{{$simlr->id}}]").val("");
                            }
                        );
                 }else{                    
                    jQuery("[name=message{{$simlr->id}}]").val("");
                        console.log(' msg '+msgs);
                        var htm = '<li class="message">'+                    
                                    '<div class="chat-user-right">'+
                                        '<div class="chat-user-box">'+
                                            '<img src="http://35.242.157.120/public/thumbnail/'+jQuery('#sender_image').val()+'">'+
                                            '<span>'+jQuery('#sender_name').val()+'</span>'+
                                        '</div>'+
                                        '<div class="chat-user-message">'+
                                            '<div class="chat-line"><span style="color:red">Fail!</span> <span><img src="http://35.242.157.120/public/thumbnail/'+msgs+'"></span></div>'+                                   
                                            '<div class="meta-info"><span class="date"></span></div>'+
                                        '</div>'+
                                    '</div>'+
                                '</li>';
                       jQuery("#messages"+jQuery('#receiver_id').val()).append(htm);
                       setTimeout(function(){ jQuery('#upgrade_myModal').modal('show'); }, 3000);
                       
                 }
                 
                 
            }
            else{
                console.log('type : file');
                console.log('extension : '+extension);
                $('.load-more').show();
                $('.chat-btn-text').attr('disabled',true);
                
                $.ajax({
                    headers: {
                          'X-CSRF-TOKEN': '{{csrf_token()}}'
                    },
                    url: "{{url('chat-file')}}",
                    type: "POST",
                    //data:{file : $("#file{{$simlr->id}}").val()},
                    data:{
                        base64: ($("#chat_file_hidden").val()),
                        extension : extension
                      },
                    //data: $("#message-form{{$simlr->id}}").serialize(),
                    success:function(dataa)
                    {
                        console.log(dataa);
                        $('.load-more').hide();
                        $('.chat-btn-text').attr('disabled',false);
                        //$('#uploaded_image').html(data);
                        jQuery('#chat_image').val(dataa);
                        
                        socket.emit(
                            "createMessage",
                            {
                                fileText: jQuery("[name=message{{$simlr->id}}]").val(),
                                fileName : dataa,
                                senderName: jQuery('#sender_name').val(),
                                receiverName: jQuery('#receiver_name').val(),
                                senderId: jQuery('#sender_id').val(),
                                receiverId: jQuery('#receiver_id').val(),
                                chatID: jQuery('#room_id').val(),
                                type : "file", 
                                readStatus: false,
                                answereStatus: false,
                                senderImageUrl: jQuery('#sender_image').val(),
                                receiverImageUrl: jQuery('#receiver_image').val()
                            },
                            function() {
                                jQuery("[name=message{{$simlr->id}}]").val("");
                                jQuery('#file{{$simlr->id}}').val('');
                                jQuery('#selected{{$simlr->id}}').val('');                                
                            }
                        );
                    }
                  });
                  
            }
           
        });
    </script>
    @endforeach
    
    
    <script src="{{ URL::asset('assets/js/libs/moustache.js')}}"></script>
    <script src="{{ URL::asset('assets/js/libs/deparam.js')}}"></script>
<!-- chat scripts --->


<script> 
$(document).ready(function(){
  $(".top-arrow").click(function(){
    $(".emoji-list").slideDown("slow");
	$(".emoji-tape").hide();
  });
  
   $(".chat__messages").click(function(){
    $(".emoji-list").slideUp("slow");
	$(".emoji-tape").show();
  });
});
</script>
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">    
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>    
<script>
    $(function() {
        $("#chat_input").autocomplete({            
            source: '<?php echo url("/")?>/get_chat_contacts',
            select: function( event, ui ) {
                event.preventDefault();
                //$("#chat_input").val('');              
                    $('.no-chat-scrl').hide();
                    $("#chat_input_btn").html('<li><div class="chat-user-img"><img alt="" src="http://35.242.157.120/public/thumbnail/'+ ui.item.image + '" height="55px"></div><div class="chat-user-detail"><h6>'+ ui.item.value + '</h6><p></p></div><div class="chat-user-btn" id="chat-user-btn"><button class="btn-bg" onclick="set_receiver('+ui.item.id+',"'+ui.item.value+'","'+ui.item.image+'")">chat</button></div></li>');
                    //$("#chat_input_btnnn").html('<div class="col-md-4"><div class=""><img src="http://35.242.157.120/public/thumbnail/'+ ui.item.image + '"></div></div><div class="col-md-4">'+ ui.item.value + '</div><div class="col-md-4"><button class="btn-bg" onclick="set_receiver('+ui.item.id+',"'+ui.item.value+'","'+ui.item.image+'")">Chat</button></div>');                    
              
                set_receiver(ui.item.id, ui.item.value, ui.item.image);
            }
        });
        $("#chat_input").on('keyup',function(){            
            if($("#chat_input").val() == ''){
                $('.no-chat-scrl').show();
                $('#chat_input_btn').hide();                
            }
        });
        
    });
</script>    

@endsection
