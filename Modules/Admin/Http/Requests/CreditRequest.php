<?php

declare(strict_types=1);

namespace Modules\Admin\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreditRequest extends FormRequest
{
    /**
     * The metric validation rules.
     *
     * @return array
     */
    public function rules()
    {        
        //if ( $metrics = $this->metrics ) {
        switch ($this->method()) {
                case 'GET':
                case 'DELETE': {
                        return [];
                    }
                case 'POST': {
                        return [
                            'qty'      => 'required',
                            'discount'       => 'required',
                            'price'           => 'required',
                            'txn_fee'        => 'required'                            
                        ];
                    }
                case 'PUT':
                case 'PATCH': {
//                    if ($user = $this->user) {
                        return [
                            'qty'      => 'required',
                            'discount'       => 'required',
                            'price'           => 'required',
                            'txn_fee'        => 'required'
                        ];
                    //}
                }
                default:break;
            }
        //}
    }

    /**
     * The
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
