<?php

declare(strict_types=1);

namespace Modules\Admin\Http\Controllers;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Redirect;
use Input;
use Modules\Admin\Http\Requests\UserRequest;
use Modules\Admin\Models\Roles;
use Modules\Admin\Models\Language;
use Modules\Admin\Models\Interest;
use Modules\Admin\Models\User;
use Modules\Admin\Models\User_detail;
use Modules\Admin\Models\User_language;
use Modules\Admin\Models\User_interest;
use Modules\Admin\Models\Gallery_image;
use Modules\Admin\Models\Gallery_video;
use Route;
use View;
use Session;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Datatables;


/**
 * Class AdminController
 */
class UsersController extends Controller
{
    /**
     * @var  Repository
     */

    /**
     * Displays all admin.
     *
     * @return \Illuminate\View\View
     */
    public function __construct()
    {
        $this->middleware('admin');
        View::share('viewPage', 'Users');
        View::share('helper', new Helper);
        View::share('heading', 'App Players');
        View::share('route_url', route('user'));

        $this->record_per_page = 30;//Config::get('app.record_per_page');
    }

    protected $users;

    /*
     * Dashboard
     * */

    public function index(User $user, Request $request)
    {        
        $page_title  = 'Admin User';
        $page_action = 'Admin Users';
        

        if ($request->ajax()) {
            $id           = $request->get('id');
            $status       = $request->get('status');
            $user         = User::find($id);
            $s            = ($status == 1) ? $status=0:$status=1;
            $user->status = $s;
            $user->save();
            echo $s;

            exit();
        }
        
        $users = User::orderBy('id', 'desc')->where('role_type', 3)->where('is_deleted', 0)->Paginate($this->record_per_page);
        
        $roles = Roles::all();

        $js_file = ['common.js','bootbox.js','formValidate.js'];
         $states = DB::table('states')->get();
        return view('admin::users.user.index', compact('js_file', 'roles', 'status', 'users', 'page_title', 'page_action', 'roles' ,'role_type','states'));
    }


    public function anyData()
    {
        return Datatables::of(User::query())->make(true);
    }

    public function test_table(User $user, Request $request)
    {        
        $page_title  = 'Admin User';
        $page_action = 'Admin Users';
        

        if ($request->ajax()) {
            $id           = $request->get('id');
            $status       = $request->get('status');
            $user         = User::find($id);
            $s            = ($status == 1) ? $status=0:$status=1;
            $user->status = $s;
            $user->save();
            echo $s;

            exit();
        }
        // Search by name ,email and group
        $search    = Input::get('search');
        $status    = Input::get('status');
        $role_type = Input::get('role_type');

        if ((isset($search) && !empty($search)) or  (isset($status) && !empty($status)) or !empty($role_type)) {
            $search = isset($search) ? Input::get('search') : '';

            $users = User::where(function ($query) use ($search,$status,$role_type) {
                if (!empty($search)) {
                    $query->Where('first_name', 'LIKE', "%$search%")
                        ->OrWhere('last_name', 'LIKE', "%$search%")
                        ->OrWhere('email', 'LIKE', "%$search%");
                }

                if (!empty($status)) {
                    
                    $status =  ($status == 'active')?1:0;
                    $query->Where('status', $status);
                }

                if ($role_type) {
                    $query->Where('role_type', $role_type);
                }
            })->where('role_type', 3)->Paginate($this->record_per_page);
        } else {
            $users = User::orderBy('id', 'desc')->where('role_type', 3)->where('is_deleted', 0)->Paginate($this->record_per_page);
        }
        $roles = Roles::all();

        $js_file = ['common.js','bootbox.js','formValidate.js'];

        return view('admin::users.user.test_table', compact('js_file', 'roles', 'status', 'page_title', 'page_action', 'roles' ,'role_type'));
    }

    /*
     * create Group method
     * */

    public function create(User $user)
    {
        $page_title  = 'Player';
        $page_action = 'Create Player';
        $roles       = Roles::all();
        $languages       = Language::all();
        $interests       = Interest::all();
        $role_id     = 3;
        $js_file     = ['common.js','bootbox.js','formValidate.js'];


        return view('admin::users.user.create', compact('js_file', 'role_id', 'roles', 'user', 'page_title', 'page_action', 'groups','languages','interests'));
    }

    /*
     * Save Group method
     * */

    public function store(UserRequest $request, User $user, User_detail $user_detail, User_language $user_language, User_interest $user_interest, Gallery_image $gallery_image)
    {
        //echo "<pre>"; print_r(Input::all()); die;
        $user->fill(Input::all());
        $user->password = Hash::make($request->get('password'));
        
        $action = $request->get('submit');        
        
        $request->file('profile_image');
        if ($action == 'avtar') {
            if ($request->file('profile_image')) {
                $profile_image = User::createImage($request, 'profile_image');
                $request->merge(['profilePic' => $profile_image]);
                $user->profile_image = $request->get('profilePic');
            }
            
            if ($request->file('cover_image')) {
                $profile_image = User::createCoverImage($request, 'cover_image');
                $request->merge(['profilePic' => $profile_image]);
                $user->cover_image = $request->get('profilePic');
            }
        }
        $user->save();
        if($user->id){
            $user_detail->user_id = $user->id;
            $user_detail->fill(Input::all());
            $user_detail->save();
                    
            $user_language->user_id = $user->id;                
            $user_language->languages = (!empty($request->get('languages')) ) ? implode(',',$request->get('languages')) : '' ;
            $user_language->save();
            
            $user_interest->user_id = $user->id;                
            $user_language->interests = (!empty($request->get('interests')) ) ? implode(',',$request->get('interests')) : '' ;
            $user_interest->save();
            
            
            
            if($request->file('gallery_image')){
                //print_r($request->file('gallery_image'));                            
                    $galleryimg = User::createGalleryImage($request, 'gallery_image');
                    $request->merge(['profilePic' => $galleryimg]);
                    $galleryimage = $request->get('profilePic');
                    
                    foreach($galleryimage as $imgs){
                        $gallery_image->user_id = $user->id;
                        $gallery_image->gallery_image = $imgs;                        
                        DB::table('gallery_images')->insert(
                            ['user_id' => $user->id, 'gallery_image' => $imgs]
                        );
                        //echo "<pre>"; print_r($gallery_image);
                    }
                    
            }
        }
        //echo "<pre>"; print_r($user_language); die;
        
        $js_file = ['common.js','bootbox.js','formValidate.js'];

        return Redirect::to(route('user'))
            ->with('flash_alert_notice', 'New record successfully created.');
    }

    /*
     * Edit Group method
     * @param
     * object : $user
     * */

    public function edit(User $user, User_detail $user_detail)
    {
        //echo "<pre>"; print_r($user); die;
        $page_title  = 'Admin User';
        $page_action = 'Show Users';
        $role_id     = $user->role_type;
        $roles       = Roles::all();
        $languages       = Language::all();
        $interests       = Interest::all();
        $js_file     = ['common.js','bootbox.js','formValidate.js'];
        $userid = $user->id;
        $user = User::leftJoin('user_details', function($join) {
            $join->on('users.id', '=', 'user_details.user_id');
          })->leftJoin('user_languages', function($join1) {
            $join1->on('users.id', '=', 'user_languages.user_id');
          })->leftJoin('user_interests', function($join2) {
            $join2->on('users.id', '=', 'user_interests.user_id');
          })
          ->where('users.id','=',$userid)
          ->first();
       
        $user['gallery_images'] = Gallery_image::where('user_id',$userid)->get();
        $user['gallery_videos'] = Gallery_video::where('user_id',$userid)->get();
        $user->id = $userid;
        //echo "<pre>"; print_r($user); die;
        return view('admin::users.user.edit', compact('js_file', 'role_id', 'roles', 'user', 'page_title', 'page_action','languages','interests'));
    }

    public function update(Request $request, User $user)
    {        
        //echo "<pre>"; print_r($user->password); die;
        $userid = $user->id;
        $password = $user->password;
        $user->fill(Input::all());

        if (!empty($request->get('password'))) {            
            $user->password = Hash::make($request->get('password'));
        }else{
            $user->password = $password;
        }

        $action          = $request->get('submit');
        $user->role_type = $request->get('role_type');

        if ($action == 'avtar') {
            if ($request->file('profile_image')) {                
                $profile_image = User::createImage($request, 'profile_image');
                $request->merge(['profilePic' => $profile_image]);
                $user->profile_image = $request->get('profilePic');
            }                        
            
        } elseif ($action == 'businessInfo') {
        } elseif ($action == 'paymentInfo') {
        } else {
        }
                        
        //echo "<pre>"; print_r($user); die;

        $validator_email = User::where('email', $request->get('email'))
            ->where('id', '!=', $user->id)->first();

        if ($validator_email) {
            if ($validator_email->id == $user->id) {
                $user->save();
            } else {
                return  Redirect::back()->withInput()->with(
                    'field_errors',
                      'The Email already been taken!'
                 );
            }
        }
        //echo "<pre>"; print_r($user); die;

        $user->save();
        
        if ($request->file('cover_image')) {
            $profile_image = User::createCoverImage($request, 'cover_image');
            $request->merge(['profilePic' => $profile_image]);
            $user->cover_image = $request->get('profilePic');
            
            DB::table('users')
            ->where('id',$userid)
            ->update(["cover_image" => $profile_image, 'updated_at' => date('Y-m-d h:i:s') ]);
        }                
                
        $userdetail['about_me'] = $request->get('about_me');        
        $userdetail['seeking'] = $request->get('seeking');
        $userdetail['about_partner'] = $request->get('about_partner');
        $userdetail['work_as'] = $request->get('work_as');
        $userdetail['education'] = $request->get('education');
        $userdetail['relationship'] = $request->get('relationship');
        $userdetail['kids'] = $request->get('kids');
        $userdetail['smoke'] = $request->get('smoke');
        $userdetail['drink'] = $request->get('drink');
        $userdetail['height'] = $request->get('height');        
        $userdetail['updated_at'] = date('Y-m-d h:i:s');
        DB::table('user_details')
            ->where('user_id', $userid)
            ->update($userdetail);
        //echo "<pre>"; print_r($user_detail); die;
                                    
        $userlanguage['languages'] = (!empty($request->get('languages')) ) ? implode(',',$request->get('languages')) : '' ;
        $userlanguage['updated_at'] = date('Y-m-d h:i:s');
        DB::table('user_languages')
            ->where('user_id', $userid)
            ->update($userlanguage);
                            
        $userinterests['interests'] = (!empty($request->get('interests')) ) ? implode(',',$request->get('interests')) : '' ;
        $userinterests['updated_at'] = date('Y-m-d h:i:s');
        DB::table('user_interests')
            ->where('user_id', $userid)
            ->update($userinterests);
            
        if($request->file('gallery_image')){
            //print_r($request->file('gallery_image'));                            
                $galleryimg = User::createGalleryImage($request, 'gallery_image');
                $request->merge(['profilePic' => $galleryimg]);
                $galleryimage = $request->get('profilePic');
                
                
                foreach($galleryimage as $imgs){                                        
                    DB::table('gallery_images')->insert(
                        ['user_id' => $userid, 'gallery_image' => $imgs, 'updated_at' => date('Y-m-d h:i:s')]
                    );
                    //echo "<pre>"; print_r($gallery_image);
                }
                
        }    
        

        if ($request->get('role') == 3) {
            $Redirect = 'clientuser';
        } else {
            $Redirect = 'user';
        }

        return Redirect::to(route($Redirect))
            ->with('flash_alert_notice', 'Record successfully updated.');
    }
    /*
     *Delete User
     * @param ID
     *
     */
    public function destroy(Request $request, $user)
    { //print_r($user);
        //echo $user->id;die;
        $user->is_deleted = 1;
        $user->profile_deleted_at = date('Y-m-d');
        $user->deleted_time = date('Y-m-d h:i:s');
        $user->save();
        //$user->delete();
        return Redirect::back()
            ->with('flash_alert_notice', 'Record  successfully deleted.');
    }

    public function show(User $user)
    {
    }
    
    public function image_delete(User $user, Request $request, Gallery_image $gallery_image)
    {
        //echo $request->ajax('id');        
            $id           = $request->get('id');            
            $status       = $request->get('is_deleted');
            $user         = Gallery_image::find($id);
            $s            = ($status == 1) ? $status=0:$status=1;
            $user->is_deleted = $s;
            $user->save();
            echo $s;

            exit();        
    }
    
    public function delete_image(User $user, Request $request, Gallery_image $gallery_image)
    {
        //echo $request->ajax('id');        
            $id           = $request->get('id');
            $userid           = $request->get('userid');
            DB::table('gallery_images')->where('id', '=', $id)->delete();
            return Redirect::to(url('admin/user/'.$userid.'/edit'))
                ->with('flash_alert_notice', 'Image successfully deleted.');
            exit();        
    }
    
    public function search1(User $user, Request $request)
    {
        //echo $request->ajax('id');        
            $country      = $request->get('country');
            $age      = $request->get('age');
            $gender      = $request->get('gender');
            $status      = $request->get('status');
            
            
            $users = User::where(function ($query) use ($country,$age,$gender,$status) {
                if (!empty($country)) {
                    $query->Where('country', 'LIKE', "%$country%");                        
                }

                if (!empty($age)) {
                    $age = explode('_',$age);
                    $query->Where('age', '>=', $age[0]);
                    $query->Where('age', '<=', $age[1]);
                }

                if ($gender) {
                    $query->Where('gender', $gender);
                }
                
                if (!empty($status)) {
                    $status = ($status == 'yes') ? '1' : '0';
                    $query->Where('status', $status);
                }
            })->where('role_type', 3)->Paginate(18);
            
            //echo "<pre>"; print_r($users); die;            
            $roles = Roles::all();

        $js_file = [];

        return view('admin::users.user.index', compact('js_file', 'roles', 'country', 'users', 'page_title', 'page_action', 'roles', 'role_type')); 
    }
    
    public function search(User $user, Request $request)
    {      $last_part = $request->get('last_part'); 
        //echo $request->ajax('id');        
            $name      = $request->get('name');
            $state      = $request->get('region');
            $age      = $request->get('age');
            $gender      = $request->get('gender');
            $status      = $request->get('status');
            $relationship      = $request->get('relationship');
            $photo      = $request->get('photo');
            $appearance      = $request->get('appearance');
            $profile_type      = $request->get('profile_type');
            $adult_level      = $request->get('adult_level');
            $no_of_pictures      = $request->get('no_of_pictures');
            //DB::enableQueryLog();
                        
            if ($no_of_pictures != '' && $relationship == '') {
                $reslt = DB::table('users')                    
                    ->select(array('users.*', DB::raw('COUNT(gallery_images.id) as photo')))
                    ->join('gallery_images', 'users.id', '=', 'gallery_images.user_id');
            }else{                
                $reslt = DB::table('users')
                    ->join('user_details', 'users.id', '=', 'user_details.user_id')
                    ->select('users.*', 'user_details.relationship' );                    
            }       
            
            if (!empty($state)) {
                $reslt->Where('users.state', 'LIKE', "%$state%");                        
            }

            if (!empty($name)) {
                $reslt->Where('users.name', 'LIKE', "%$name%");                        
            }
                
            if (!empty($age)) {
                $age = explode('_',$age);
                $reslt->Where('users.age', '>=', $age[0]);
                $reslt->Where('users.age', '<=', $age[1]);
            }
            
            if ($gender) {
                $reslt->Where('users.gender', $gender);
            }
            
            if ($profile_type) {
                $reslt->Where('users.profile_type', $profile_type);
            }
            
            if ($adult_level) {
                $reslt->Where('users.adult_level', $adult_level);
            }
            
            if ($appearance) {
                $reslt->Where('users.appearance', $appearance);
            }
            
            if (!empty($status)) {
                $status = ($status == 'yes') ? '1' : '0';
                $reslt->Where('users.status', $status);
            }
            
            if (isset($photo)) {
                if($photo == '1'){
                    $reslt->where(function ($query) {                
                        $query->Where('users.profile_image', '!=', 'male_icon.png')
                            ->Where('users.profile_image', '!=', 'lady_icon.png');                       
                    });
                    //$reslt->Where('users.profile_image', '!=',  'male_icon.png');
                    //$reslt->or_Where('users.profile_image', '!=',  'lady_icon.png');
                }
                if($photo == '0'){
                    $reslt->where(function ($query) {                
                        $query->Where('users.profile_image', 'male_icon.png')
                            ->OrWhere('users.profile_image', 'lady_icon.png');
                            //->OrWhere('users.profile_image', '');                       
                    });
                    //$reslt->Where('users.profile_image','male_icon.png');
                    //$reslt->or_Where('users.profile_image', '=',  'lady_icon.png');
                }
            }
            
            if ($relationship) {
                $reslt->Where('user_details.relationship', $relationship);
            }
            
            

            if($last_part == 'deleted') {
                $page_title  = 'Player';
                $page_action = 'Deleted Players';
                $heading = 'Deleted Players';
                $reslt->Where('users.is_deleted', '=',  '1');
            }
            else if($last_part == 'real') {
                $page_title  = 'Player';
                $page_action = 'Real Players';
                $heading = 'Real Players';
                $reslt->Where('users.fake', '=',  '0');
                $reslt->Where('users.is_deleted', '=',  '0');
            }
            else if($last_part == 'fake') {
                $page_title  = 'Player';
                $page_action = 'Fake Players';
                $heading = 'Fake Players';
                $reslt->Where('users.fake', '=',  '1');
                $reslt->Where('users.is_deleted', '=',  '0');
            }
            else if($last_part == 'unconfirmed') {
                $page_title  = 'Player';
                $page_action = 'Unconfirmed Players';
                $heading = 'Unconfirmed Players';
                $reslt->Where('users.status', '=',  '0');
                $reslt->Where('users.is_deleted', '=',  '0');
            }
                
            //$users = $reslt->where('users.role_type', 3)->Paginate(30);
            if ($no_of_pictures != '' && $relationship == '') {
                if($no_of_pictures == 6){
                    $users = $reslt->where('users.role_type', 3)->where('gallery_images.status', 1)->groupBy('users.id')->havingRaw('COUNT(gallery_images.id) >= '.$no_of_pictures)->Paginate(30);
                }else{
                    $users = $reslt->where('users.role_type', 3)->where('gallery_images.status', 1)->groupBy('users.id')->havingRaw('COUNT(gallery_images.id) = '.$no_of_pictures)->Paginate(30);   
                }                
            }else{
                $users = $reslt->where('users.role_type', 3)->Paginate(30);    
            }
            
            //echo "<pre>"; print_r($users); die;            
            $roles = Roles::all();

        $js_file = [];

        $search_slug = 'list-search';
          $states = DB::table('states')->get();
        return view('admin::users.user.index', compact('js_file', 'roles', 'state','states',  'users', 'page_title', 'page_action','heading',  'roles', 'role_type', 'search_slug')); 
    }
    
    public function setProfileImage(User $user, Request $request, Gallery_image $gallery_image)
    {        
            $id           = $request->get('id');
            $userid           = $request->get('userid');
            
            $user = DB::table('users')->where('id', $userid)->first();            
            $old_image = $user->profile_image;
            
            $gallery = DB::table('gallery_images')->where('id', $id)->first();            
            $new_image = $gallery->gallery_image;
            
            DB::table('users')
            ->where('id', $userid)
            ->update(array('profile_image' => $new_image));
            
            DB::table('gallery_images')
            ->where('id', $id)
            ->update(array('gallery_image' => $old_image));
                        
            return Redirect::to(url('admin/user/'.$userid.'/edit'))
                ->with('flash_alert_notice', 'Image Set as Profile successfully ');
            exit();        
    }


    public function deleted(User $user, Request $request)
    {        
        $page_title  = 'Player';
        $page_action = 'Deleted Players';
        $heading = 'Deleted Players';
        

        if ($request->ajax()) {
            $id           = $request->get('id');
            $status       = $request->get('status');
            $user         = User::find($id);
            $s            = ($status == 1) ? $status=0:$status=1;
            $user->status = $s;
            $user->save();
            echo $s;

            exit();
        }
        // Search by name ,email and group
        $search    = Input::get('search');
        $status    = Input::get('status');
        $role_type = Input::get('role_type');

        if ((isset($search) && !empty($search)) or  (isset($status) && !empty($status)) or !empty($role_type)) {
            $search = isset($search) ? Input::get('search') : '';

            $users = User::where(function ($query) use ($search,$status,$role_type) {
                if (!empty($search)) {
                    $query->Where('first_name', 'LIKE', "%$search%")
                        ->OrWhere('last_name', 'LIKE', "%$search%")
                        ->OrWhere('email', 'LIKE', "%$search%");
                }

                if (!empty($status)) {
                    
                    $status =  ($status == 'active')?1:0;
                    $query->Where('status', $status);
                }

                if ($role_type) {
                    $query->Where('role_type', $role_type);
                }
            })->where('role_type', 3)->Paginate($this->record_per_page);
        } else {
            $users = User::orderBy('id', 'desc')->where('role_type', 3)->where('is_deleted', 1)->Paginate($this->record_per_page);
        }
        $roles = Roles::all();
        $states = DB::table('states')->get();
        $js_file = ['common.js','bootbox.js','formValidate.js'];
        $del = 1;
        return view('admin::users.user.index', compact('js_file', 'roles', 'status', 'users', 'page_title', 'page_action','heading', 'roles', 'role_type','del','states'));
    }

    public function fake(User $user, Request $request)
    {      

        $page_title  = 'Player';
        $page_action = 'Deleted Players';
        $heading = 'Fake Players';
       
        $states = DB::table('states')->get();
          // print_r($states);die;
        if ($request->ajax()) {
            $id           = $request->get('id');
            $status       = $request->get('status');
            $user         = User::find($id);
            $s            = ($status == 1) ? $status=0:$status=1;
            $user->status = $s;
            $user->save();
            echo $s;

            exit();
        }
        // Search by name ,email and group
        $search    = Input::get('search');
        $status    = Input::get('status');
        $role_type = Input::get('role_type');

        if ((isset($search) && !empty($search)) or  (isset($status) && !empty($status)) or !empty($role_type)) {
            $search = isset($search) ? Input::get('search') : '';

            $users = User::where(function ($query) use ($search,$status,$role_type) {
                if (!empty($search)) {
                    $query->Where('first_name', 'LIKE', "%$search%")
                        ->OrWhere('last_name', 'LIKE', "%$search%")
                        ->OrWhere('email', 'LIKE', "%$search%");
                }

                if (!empty($status)) {
                    
                    $status =  ($status == 'active')?1:0;
                    $query->Where('status', $status);
                }

                if ($role_type) {
                    $query->Where('role_type', $role_type);
                }
            })->where('role_type', 3)->Paginate($this->record_per_page);
        } else {
            $users = User::orderBy('id', 'desc')->where('role_type', 3)->where('fake', 1)->where('is_deleted', 0)->Paginate($this->record_per_page);
        }
        $roles = Roles::all();

        $js_file = ['common.js','bootbox.js','formValidate.js'];

        return view('admin::users.user.index', compact('js_file', 'roles', 'status', 'users', 'page_title', 'page_action','heading', 'roles', 'role_type','states'));
    }

    public function real(User $user, Request $request)
    {        
        $page_title  = 'Player';
        $page_action = 'Real Players';
        $heading = 'Real Players';
        

        if ($request->ajax()) {
            $id           = $request->get('id');
            $status       = $request->get('status');
            $user         = User::find($id);
            $s            = ($status == 1) ? $status=0:$status=1;
            $user->status = $s;
            $user->save();
            echo $s;

            exit();
        }
        // Search by name ,email and group
        $search    = Input::get('search');
        $status    = Input::get('status');
        $role_type = Input::get('role_type');

        if ((isset($search) && !empty($search)) or  (isset($status) && !empty($status)) or !empty($role_type)) {
            $search = isset($search) ? Input::get('search') : '';

            $users = User::where(function ($query) use ($search,$status,$role_type) {
                if (!empty($search)) {
                    $query->Where('first_name', 'LIKE', "%$search%")
                        ->OrWhere('last_name', 'LIKE', "%$search%")
                        ->OrWhere('email', 'LIKE', "%$search%");
                }

                if (!empty($status)) {
                    
                    $status =  ($status == 'active')?1:0;
                    $query->Where('status', $status);
                }

                if ($role_type) {
                    $query->Where('role_type', $role_type);
                }
            })->where('role_type', 3)->Paginate($this->record_per_page);
        } else {
            $users = User::orderBy('id', 'desc')->where('role_type', 3)->where('fake', 0)->where('is_deleted', 0)->Paginate($this->record_per_page);
        }
        $roles = Roles::all();

        $js_file = ['common.js','bootbox.js','formValidate.js'];
         $states = DB::table('states')->get();
        return view('admin::users.user.index', compact('js_file', 'roles', 'status', 'users', 'page_title', 'page_action','heading', 'roles', 'role_type','states'));
    }

    public function unconfirmed(User $user, Request $request)
    {        
        $page_title  = 'Player';
        $page_action = 'Unconfirmed Players';
        $heading = 'Unconfirmed Players';
        

        if ($request->ajax()) {
            $id           = $request->get('id');
            $status       = $request->get('status');
            $user         = User::find($id);
            $s            = ($status == 1) ? $status=0:$status=1;
            $user->status = $s;
            $user->save();
            echo $s;

            exit();
        }
        // Search by name ,email and group
        $search    = Input::get('search');
        $status    = Input::get('status');
        $role_type = Input::get('role_type');

        if ((isset($search) && !empty($search)) or  (isset($status) && !empty($status)) or !empty($role_type)) {
            $search = isset($search) ? Input::get('search') : '';

            $users = User::where(function ($query) use ($search,$status,$role_type) {
                if (!empty($search)) {
                    $query->Where('first_name', 'LIKE', "%$search%")
                        ->OrWhere('last_name', 'LIKE', "%$search%")
                        ->OrWhere('email', 'LIKE', "%$search%");
                }

                if (!empty($status)) {
                    
                    $status =  ($status == 'active')?1:0;
                    $query->Where('status', $status);
                }

                if ($role_type) {
                    $query->Where('role_type', $role_type);
                }
            })->where('role_type', 3)->Paginate($this->record_per_page);
        } else {
            $users = User::orderBy('id', 'desc')->where('role_type', 3)->where('status', 0)->where('is_deleted', 0)->Paginate($this->record_per_page);
        }
        $roles = Roles::all();

        $js_file = ['common.js','bootbox.js','formValidate.js'];
          $states = DB::table('states')->get();
        return view('admin::users.user.index', compact('js_file', 'roles', 'status', 'users', 'page_title', 'page_action','heading', 'roles', 'role_type','states'));
    }
    
    
}
