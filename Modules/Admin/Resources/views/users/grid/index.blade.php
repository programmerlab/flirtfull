@extends('admin::layouts.master')
 
    @section('content') 
      @include('admin::partials.navigation')
      @include('admin::partials.breadcrumb')   

       @include('admin::partials.sidebar')  
			
            <div class="advanced-search">            	
            	<form method="get" action="{{ url('admin/grid-search') }}">
            	<div class="row">

                     <div class="col-md-2"><div class="form-group">
                    <input class="custom-select" placeholder="Profile Name" type="text" name="name" value="{{ (isset($_GET['name']) && ($_GET['name']))  ? $_GET['name'] : '' }}">
              </div></div>

                	<div class="col-md-2">
                    <div class="form-group">
                    <select name="age" class="custom-select">
                      <option value="" selected="">Age</option>
                      <option value="18_25" {{ (isset($_GET['age']) && ($_GET['age'] == '18_25') ) ? "selected" : '' }}>18 year to 25 year</option>
                      <option value="25_35" {{ (isset($_GET['age']) && ($_GET['age'] == '25_35') ) ? "selected" : '' }}>25 year to 35 year</option>
                      <option value="35_50" {{ (isset($_GET['age']) && ($_GET['age'] == '35_50') ) ? "selected" : '' }}>35 year to 50 year</option>
                      <option value="50_100" {{ (isset($_GET['age']) && ($_GET['age'] == '50_100') ) ? "selected" : '' }}>Above 50 year</option>
                    </select>
                    </div>
					</div>
                    <div class="col-md-2"><div class="form-group">
                    <select name="gender" class="custom-select">
                      <option value="" selected="">Gender</option>
                      <option value="1" {{ (isset($_GET['gender']) && ($_GET['gender'] == '1') ) ? "selected" : '' }}>Male</option>
                      <option value="2" {{ (isset($_GET['gender']) && ($_GET['gender'] == '2') ) ? "selected" : '' }}>Female</option>
                      <option value="3" {{ (isset($_GET['gender']) && ($_GET['gender'] == '3') ) ? "selected" : '' }}>Transsexual </option>
                    </select>
					</div></div>
                    
                      <div class="col-md-2">
                        <div class="form-group">
                        	<!--<input name="country" type="text" class="form-control" value="{{ (isset($_GET['country']) && ($_GET['country'] != '') ) ? $_GET['country'] : '' }}">-->
                        <select name="country" class="custom-select">
                            <option selected="" name="country" value="">Country</option>
                            <option value="Ukraine" {{ (isset($_GET['country']) && ($_GET['country'] == 'Ukraine') ) ? "selected" : '' }}>Ukraine</option>
                            <option value="India" {{ (isset($_GET['country']) && ($_GET['country'] == 'India') ) ? "selected" : '' }}>India</option>
                            <option value="Morocco" {{ (isset($_GET['country']) && ($_GET['country'] == 'Morocco') ) ? "selected" : '' }}>Morocco</option>
                            <option value="China" {{ (isset($_GET['country']) && ($_GET['country'] == 'China') ) ? "selected" : '' }}>China</option>
                            <option value="Colombia" {{ (isset($_GET['country']) && ($_GET['country'] == 'Colombia') ) ? "selected" : '' }}>Colombia</option>                                
                        </select>
                        </div>
                        </div>  
                      
                        <div class="col-md-2"><div class="form-group">
                        	<button class="advance-btn" type="button"><i class="glyphicon glyphicon-cog"></i> More Filter</button>
                        </div></div>
                         <div class="col-md-2"><div class="form-group">
                        	<button type="submit" class="btn btn-primary form-control">Search</button>
                        </div></div>
                </div>
                
                <div class="advance-fields">
                	<div class="row">

                        <div class="col-md-2"><div class="form-group">
                        <select name="status" class="custom-select">
                            <option selected="" value="">Profile Status</option>
                            <option value="yes" {{ (isset($_GET['status']) && ($_GET['status'] == 'yes') ) ? "selected" : '' }}>Active</option>
                            <option value="no" {{ (isset($_GET['status']) && ($_GET['status'] == 'no') ) ? "selected" : '' }}>Inactive</option>
                           
                        </select>
                        </div></div>

                    	<div class="col-md-2"><div class="form-group">
                        	<select name="adult_level" class="custom-select">
                              <option value="" selected="">Adult level </option>
                              <option value="Sexy" {{ (isset($_GET['adult_level']) && ($_GET['adult_level'] == 'Sexy') ) ? "selected" : '' }}>Sexy</option>
                              <option value="Adult" {{ (isset($_GET['adult_level']) && ($_GET['adult_level'] == 'Adult') ) ? "selected" : '' }}>Adult</option>
                              <option value="Non-adult" {{ (isset($_GET['adult_level']) && ($_GET['adult_level'] == 'Non-adult') ) ? "selected" : '' }}>Non-adult</option>
                              <option value="Explicit" {{ (isset($_GET['adult_level']) && ($_GET['adult_level'] == 'Explicit') ) ? "selected" : '' }}>Explicit</option>
                            </select>
                        </div></div>
                        <div class="col-md-2"><div class="form-group">
                        	<select name="profile_type" class="custom-select">
                                <option value="" selected="">Profile Type</option>
                                <option value="Generic" {{ (isset($_GET['profile_type']) && ($_GET['profile_type'] == 'Generic') ) ? "selected" : '' }}>Generic</option>
                                <option value="Mature" {{ (isset($_GET['profile_type']) && ($_GET['profile_type'] == 'Mature') ) ? "selected" : '' }}>Mature</option>
                                <option value="Kinky" {{ (isset($_GET['profile_type']) && ($_GET['profile_type'] == 'Kinky') ) ? "selected" : '' }}>Kinky</option>
                                <option value="Shemale" {{ (isset($_GET['profile_type']) && ($_GET['profile_type'] == 'Shemale') ) ? "selected" : '' }}>Shemale</option>
                            </select>
                        </div></div>
                        <div class="col-md-3"><div class="form-group">
                        	<select name="photo" class="custom-select">
                                <option value="" selected="">Photo setting</option>
                                <option value="0" {{ (isset($_GET['photo']) && ($_GET['photo'] == '0') ) ? "selected" : '' }}>	Without picture</option>
                                <option value="1" {{ (isset($_GET['photo']) && ($_GET['photo'] == '1') ) ? "selected" : '' }}>With pictures</option>
                                <!--<option value="3">Only attachements</option>
                                <option value="4">Picture + attachement</option>-->
                            </select>
                        </div></div>
                           <div class="col-md-3"><div class="form-group">
                        	<select name="no_of_pictures" class="custom-select">
                                <option selected="">Number of pictures</option>
                                <option value="1" {{ (isset($_GET['no_of_pictures']) && ($_GET['no_of_pictures'] == '0_1') ) ? "selected" : '' }}>	1</option>
                                <option value="2" {{ (isset($_GET['no_of_pictures']) && ($_GET['no_of_pictures'] == '2_3') ) ? "selected" : '' }}>2-3</option>
                                <option value="3" {{ (isset($_GET['no_of_pictures']) && ($_GET['no_of_pictures'] == '4_5') ) ? "selected" : '' }}>4-5</option>
                                <option value="4" {{ (isset($_GET['no_of_pictures']) && ($_GET['no_of_pictures'] == '5_6') ) ? "selected" : '' }}>5-6</option>
                                <option value="5" {{ (isset($_GET['no_of_pictures']) && ($_GET['no_of_pictures'] == '6_7') ) ? "selected" : '' }}>6-7</option>
                                <option value="6" {{ (isset($_GET['no_of_pictures']) && ($_GET['no_of_pictures'] == '8_100') ) ? "selected" : '' }}>+8</option>
                            </select>
                        </div></div>
                    </div>
                    <div class="row">
                    	<div class="col-md-2"><div class="form-group">
                        <select name="appearance" class="custom-select">
                            <option value="" selected="">Appearance</option>
                            <option value="Arab" {{ (isset($_GET['appearance']) && ($_GET['appearance'] == 'Arab') ) ? "selected" : '' }}>Arab</option>
                            <option value="Asian" {{ (isset($_GET['appearance']) && ($_GET['appearance'] == 'Asian') ) ? "selected" : '' }}>Asian</option>
                            <option value="Ebony" {{ (isset($_GET['appearance']) && ($_GET['appearance'] == 'Ebony') ) ? "selected" : '' }}>Ebony</option>
                            <option value="Latin" {{ (isset($_GET['appearance']) && ($_GET['appearance'] == 'Latin') ) ? "selected" : '' }}>Latin</option>
                            <option value="White" {{ (isset($_GET['appearance']) && ($_GET['appearance'] == 'White') ) ? "selected" : '' }}>White </option>
                        </select>
                        </div></div>
                        <div class="col-md-2"><div class="form-group">
                        	<select name="relationship" class="custom-select">
                                <option value="" selected="">Civil status</option>
                                <option value="1" {{ (isset($_GET['relationship']) && ($_GET['relationship'] == '1') ) ? "selected" : '' }}>Never Married</option>
                                <option value="2" {{ (isset($_GET['relationship']) && ($_GET['relationship'] == '2') ) ? "selected" : '' }}>Separated</option>
                                <option value="3" {{ (isset($_GET['relationship']) && ($_GET['relationship'] == '3') ) ? "selected" : '' }}>Divorced</option>
                                <option value="4" {{ (isset($_GET['relationship']) && ($_GET['relationship'] == '4') ) ? "selected" : '' }}>Marrioed</option>
                                <option value="5" {{ (isset($_GET['relationship']) && ($_GET['relationship'] == '5') ) ? "selected" : '' }}>LAT relationship</option>
                                <option value="6" {{ (isset($_GET['relationship']) && ($_GET['relationship'] == '6') ) ? "selected" : '' }}>Living together</option>
                                <option value="7" {{ (isset($_GET['relationship']) && ($_GET['relationship'] == '7') ) ? "selected" : '' }}>In a relationship</option>
                            </select>
                        </div></div>
                    </div>
                </div>
            	</form>
            </div>
            <div class="panel panel-white">
            @if(Session::has('flash_alert_notice'))
                     <div class="alert alert-success alert-dismissable" style="margin:10px">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                      <i class="icon fa fa-check"></i>  
                     {{ Session::get('flash_alert_notice') }} 
                     </div>
                @endif
                <div class="panel panel-flat">
                  <div class="panel-heading">
                    <h6 class="panel-title"><b> App Players </b><a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
                    <div class="heading-elements">
                      <ul class="icons-list">
                        <li> <a type="button" href="{{route('user.create')}}" class="btn btn-primary text-white btn-labeled btn-rounded "><b><i class="icon-plus3"></i></b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Add Player<span class="legitRipple-ripple" ></span></a></li> 
                      </ul>
                    </div>
                  </div> 
                </div>  
                <div class="panel-body">
                	<div class="row">
                    	
                        
                      @if($users->count() > 0)  
                        @foreach($users as $key => $result)
                            <div class="col-md-2">
                            <div class="profile-grid-outer">
                                <div class="profile-grid">
                                    
                                    <section class="regular slider" data-sizes="50vw">
                                        
                                        @if($result->profile_image)
                                            <div>
                                            <img src="{{ URL::asset('public/images/profile_image/'.$result->profile_image) }}" onerror="this.src='{{ asset("public/images/profile_image/no_image.jpg") }}'">
                                            <div class="profile-grid-edit col-md-12">
                                            <div class="row">
                                                <div class="col-md-4 view_pro" style="{{ ($result->profile_image_status == 1) ? 'background:green' : '' }}">
                                                    <a href="{{ route('grid.show',$result->id)}}"><button type="submit" name="view_levels" class="" value="view"><i class="glyphicon glyphicon-user" title="View"></i></button></a>
                                                </div>
                                                <div class="col-md-4 edit_pro" style="{{ ($result->status == 1) ? 'background:green' : '' }}">
                                                    <a href="{{ route('grid.edit',$result->id)}}"><button type="submit" name="edit_levels" class="" value="edit"><i class="icon-pencil7" title="edit"></i></button></a>
                                                </div>
                                                <div class="col-md-4 delete_pro">
                                                    {!! Form::open(array('class' => 'form-inline pull-left deletion-form', 'method' => 'DELETE',  'id'=>'deleteForm_'.$result->id,  'route' => array('grid.destroy', $result->id))) !!}
                                                    <button type="submit" id="{{$result->id}}" onclick=" var c = confirm('Are you sure want to delete?'); if(!c) return false;" name="remove_levels" class="" value="delete"><i class="icon-trash" title="Delete"></i></button>
                                                    {!! Form::close() !!}    
                                                </div>
                                                </div>
                                           </div>
                                            </div>
                                        @endif
										
                                        @if($result->gallery_images)
                                            @foreach($result->gallery_images as $img)
                                            <div>
                                            <img src="{{ URL::asset('public/images/profile_image/'.$img[0]) }}" onerror="this.src='{{ asset("public/images/profile_image/no_image.jpg") }}'">
                                            <div class="profile-grid-edit col-md-12">
                                            <div class="row">
                                                <div class="col-md-4 view_pro" style="{{ ($img[1] == 1) ? 'background:green' : '' }}">
                                                    <a href="{{ route('grid.show',$result->id)}}"><button type="submit" name="view_levels" class="" value="view"><i class="glyphicon glyphicon-user" title="View"></i></button></a>
                                                </div>
                                                <div class="col-md-4 edit_pro" style="{{ ($result->status == 1) ? 'background:green' : '' }}">
                                                    <a href="{{ route('grid.edit',$result->id)}}"><button type="submit" name="edit_levels" class="" value="edit"><i class="icon-pencil7" title="edit"></i></button></a>
                                                </div>
                                                <div class="col-md-4 delete_pro">
                                                    {!! Form::open(array('class' => 'form-inline pull-left deletion-form', 'method' => 'DELETE',  'id'=>'deleteForm_'.$result->id, 'route' => array('grid.destroy', $result->id))) !!}
                                                    <button type="submit" id="{{$result->id}}" onclick=" var c = confirm('Are you sure want to delete?'); if(!c) return false;" name="remove_levels" class="" value="delete"><i class="icon-trash" title="Delete"></i></button>
                                                    {!! Form::close() !!}    
                                                </div>
                                                </div>
                                           </div>
                                            </div>
                                            @endforeach    
                                        @endif
										
                                    </section>
                                   
                                 
                                 </div>
                                   <h3>{{$result->name}} {{ date_diff(date_create($result->dob), date_create('today'))->y }} </h3>
                                   <p>  {{$result->address}}</p>
                                </div>
                            </div>
                        @endforeach
                      @else
                          <h3>No Records Found</h3>
                      @endif  
                                                                                                                                                    
                    </div>
                </div>
                Showing {{($users->currentpage()-1)*$users->perpage()+1}} to {{$users->currentpage()*$users->perpage()}}
                of  {{$users->total()}} entries
                 <div class="center" align="center">  {!! $users->appends(['gender' => isset($_GET['gender'])?$_GET['gender']:'', 'country' => isset($_GET['country'])?$_GET['country']:'' ,'age' => isset($_GET['age'])?$_GET['age']:'', 'photo' => isset($_GET['photo'])?$_GET['photo']:'', 'relationship' => isset($_GET['relationship'])?$_GET['relationship']:'' , 'profile_type' => isset($_GET['profile_type'])?$_GET['profile_type']:'' , 'adult_level' => isset($_GET['adult_level'])?$_GET['adult_level']:'' , 'appearance' => isset($_GET['appearance'])?$_GET['appearance']:'' , 'no_of_pictures' => isset($_GET['no_of_pictures'])?$_GET['no_of_pictures']:'' , 'status' => isset($_GET['status'])?$_GET['status']:''])->render() !!}</div>    
                
               </div>
         </div> 
   @stop