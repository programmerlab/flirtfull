<?php

declare(strict_types=1);

namespace Modules\Admin\Http\Controllers;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Redirect;
use Input;
use Route;
use View;
use Session;
use Validator;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Datatables;


/**
 * Class AdminController
 */
class RewardController extends Controller
{
    /**
     * @var  Repository
     */

    /**
     * Displays all admin.
     *
     * @return \Illuminate\View\View
     */
    public function __construct()
    {
        $this->middleware('admin');
        View::share('viewPage', 'Reward');
        View::share('helper', new Helper);
        View::share('heading', 'Rewards');
        View::share('route_url', route('reward'));

        $this->record_per_page = Config::get('app.record_per_page');
    }

    

    /*
     * Dashboard
     * */

    /*public function index(Credit $credit, Request $request)
    {        
        $page_title  = 'Credits';
        $page_action = 'Credits';
        

        if ($request->ajax()) {
            $id           = $request->get('id');
            $status       = $request->get('status');
            $credit         = Credit::find($id);
            $s            = ($status == 1) ? $status=0:$status=1;
            $credit->status = $s;
            $credit->save();
            echo $s;

            exit();
        }
        
        $credits = Credit::orderBy('id', 'desc')->where('status', '1')->where('is_deleted', '0')->Paginate($this->record_per_page);
        //print_r($credits); die;        

        $js_file = ['common.js','bootbox.js','formValidate.js'];

        return view('admin::credits.credit.index', compact('js_file', 'status', 'credits', 'page_title', 'page_action'));
    }*/



    /*
     * create Group method
     * */

    public function index()
    {
        $page_title  = 'Reward Credit';
        $page_action = 'Reward Credit';
        
        $js_file     = ['common.js','bootbox.js','formValidate.js'];

        $users = DB::table('users')->where('status',1)->where('is_deleted',0)->get();
        return view('admin::rewards.reward.create', compact('js_file', 'credit', 'page_title', 'page_action','users'));
    }

    /*
     * Save Group method
     * */

    public function store(Request $request)
    {
        //echo "<pre>"; print_r(Input::all()); die;
        
        $rules = [
                'user_id' => 'required',
                'credit' => 'required|numeric',
            ];

        $messages = [
            'user_id.required' => 'Please Select User.',
            'credit.required' => 'Please Enter Credit.',
            'credit.numeric' => 'Credit should be numeric',
            
        ];
        $validator = Validator::make($request->all(),$rules,$messages);

        if ($validator->fails()) {
          return redirect()->back()->withErrors($validator)->withInput();
        }

        $user_id = $request->get('user_id');
        $credit = $request->get('credit');
        
        $get_credit = DB::table('user_wallet')->where('user_id',$user_id)->count();

        if($get_credit > 0)
        {
            DB::table('user_wallet')->where('user_id',$user_id)->increment('credits',$credit);
        }
        else{
            DB::table('user_wallet')->insert(['user_id'=>$user_id,'credits'=>$credit]);
        }

        $js_file = ['common.js','bootbox.js','formValidate.js'];

        return Redirect::to(route('reward'))
            ->with('flash_alert_notice', 'Credit reward has given successfully.');
    }

}
