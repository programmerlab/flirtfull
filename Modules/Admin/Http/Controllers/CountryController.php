<?php

declare(strict_types=1);

namespace Modules\Admin\Http\Controllers;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Redirect;
use Input;

use Modules\Admin\Models\Country;
use Route;
use View;
use Session;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Datatables;


/**
 * Class AdminController
 */
class CountryController extends Controller
{
    /**
     * @var  Repository
     */

    /**
     * Displays all admin.
     *
     * @return \Illuminate\View\View
     */
    public function __construct()
    {
        $this->middleware('admin');
        View::share('viewPage', 'Countries');
        View::share('helper', new Helper);
        View::share('heading', 'Countries List');
        View::share('route_url', route('country'));

        $this->record_per_page = 10;
    }

    protected $country;

    /*
     * Dashboard
     * */

    public function index(Country $country, Request $request)
    {        
        $page_title  = 'Countries';
        $page_action = 'Countries';
        

        if ($request->ajax()) {
            $id           = $request->get('id');
            $status       = $request->get('status');
            $payment         = Country::find($id);
            $s            = ($status == 1) ? $status=0:$status=1;
            $payment->status = $s;
            $payment->save();
            echo $s;

            exit();
        }

        // Search by name ,email and group
        $search = Input::get('search');
        $status = Input::get('status');

        if ((isset($search) && !empty($search))) {
            $search = isset($search) ? Input::get('search') : '';

            $countries = Country::where(function ($query) use ($search,$status) {
                if (!empty($search)) {
                    $query->Where('name', 'LIKE', "%$search%");
                }
            })->orderBy('id', 'asc')->Paginate($this->record_per_page);

              
        } else {
        
            $countries = Country::orderBy('id', 'asc')->Paginate($this->record_per_page);
        }
        //print_r($payments); die;  

        $js_file = ['common.js','bootbox.js','formValidate.js'];

        return view('admin::countries.country.index', compact('js_file', 'status', 'countries', 'page_title', 'page_action'));
    }


    
    
    
}
