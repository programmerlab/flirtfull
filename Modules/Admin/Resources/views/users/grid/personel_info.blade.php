<div class="portlet light bordered">
 		<input type="hidden" value="3" name="role_type">
        <div class="profile-usertitle">
                                        <span class="profile-usertitle-name"> {{$user->name}} </span>
                                        <span class="profile-usertitle-job"> {{$user->address}}</span>
                                    </div>
                                    
                                     <div class="row">
      <div class="form-group {{ $errors->first('adultlevel', ' has-error') }}">
            <div class="col-md-4">  <label class="control-label">Adult level</label></div>
            <div class="col-md-8">
                <div class="radio">
                    <input name="adult_level" id="adult" value="Adult" type="radio" {{ ($user->adult_level == 'Adult') ? 'checked' : ''}}>
                    <label for="adult">Adult</label>
                </div>
                <div class="radio">
                  <input name="adult_level" id="sexy" value="Sexy" type="radio" {{ ($user->adult_level == 'Sexy') ? 'checked' : ''}}>
                  <label for="sexy">Sexy</label>
                </div>
                 <div class="radio">
                    <input name="adult_level" id="nonadult" value="Non-adult" type="radio" {{ ($user->adult_level == 'Non-adult') ? 'checked' : ''}}>
                    <label for="nonadult">Non Adult</label>
                </div>
                <div class="radio">
                  <input name="adult_level" id="explicit" value="Explicit" type="radio" {{ ($user->adult_level == 'Explicit') ? 'checked' : ''}}>
                  <label for="explicit">Explicit</label>
                </div>
                
             </div>
        </div>
    </div>
                                    
   <div class="row">
        <div class="form-group {{ $errors->first('name', ' has-error') }}">
            <div class="col-md-4"><label class="control-label">Display Name</label></div>
            <div class="col-md-8">  <input type="text" placeholder="Display Name" class="form-control" name="name" 
                value="{{ ($user->name)?$user->name:old('name')}}"> </div>
        </div>
    </div>
    
     <div class="row">
       <div class="form-group {{ $errors->first('email', ' has-error') }}">
            <div class="col-md-4"> <label class="control-label ">Email</label></div>
            <div class="col-md-8"> <input type="email" placeholder="Email" class="form-control" name="email" value="{{ ($user->email)?$user->email:old('email')}}"> </div>
        </div>
    </div>
    
    <div class="row">
      <div class="form-group {{ $errors->first('password', ' has-error') }}">
            <div class="col-md-4">   <label class="control-label">Password</label></div>
            <div class="col-md-8">  <input type="password" placeholder="******" class="form-control" name="password"> </div>
        </div>
    </div>
    
     <div class="row">
      <div class="form-group {{ $errors->first('phone', ' has-error') }}">
            <div class="col-md-4"> <label class="control-label">Mobile Number</label></div>
            <div class="col-md-8">  <input type="text" placeholder="Mobile or Phone" class="form-control phone" name="phone"  value="{{ ($user->phone)?$user->phone:old('phone')}}"></div>
        </div>
    </div>
    
     <div class="row">
     <div class="form-group {{ $errors->first('gender', ' has-error') }}">
            <div class="col-md-4">    <label class="control-label">Gender</label></div>
            <div class="col-md-8">  <select name="gender" class="form-control">
            <option value="1" {{ ($user->gender == '1') ? 'selected' : ''}} >Male</option>
            <option value="2" {{ ($user->gender == '2') ? 'selected' : ''}}>Female</option>
        </select>    </div>
        </div>
    </div>
    
     <div class="row">
     <div class="form-group {{ $errors->first('dob', ' has-error') }}">
            <div class="col-md-4"> <label class="control-label">Date of Birth</label></div>
            <div class="col-md-8"> <input type="text" placeholder="Date of Birth" class="form-control" name="dob"  value="{{ ($user->dob)?$user->dob:old('dob')}}"></div>
        </div>
    </div>
    
     <div class="row">
      <div class="form-group {{ $errors->first('address', ' has-error') }}">
            <div class="col-md-4"> <label class="control-label">Address</label></div>
            <div class="col-md-8">  <input type="text" placeholder="Address" class="form-control" name="address"  value="{{ ($user->address)?$user->address:old('address')}}"> </div>
        </div>
    </div>
    
    <div class="row">
    <div class="form-group {{ $errors->first('profile_language', ' has-error') }}">
            <div class="col-md-4">    <label class="control-label">Language</label></div>
            <div class="col-md-8">               
               <select name="profile_language" class="form-control">
	<option value="English" {{ ($user->profile_language == 'English') ? 'selected' : ''}}>English</option>
	<option value="French" {{ ($user->profile_language == 'French') ? 'selected' : ''}}>French</option>
	<option value="Arabic" {{ ($user->profile_language == 'Arabic') ? 'selected' : ''}}>Arabic</option>                
                </select>
            </div>
        </div>
    </div>
    
     <div class="row">
    <div class="form-group {{ $errors->first('country', ' has-error') }}">
            <div class="col-md-4">    <label class="control-label">Country</label></div>
            <div class="col-md-8">
	<!--<input type="text" placeholder="Country" class="form-control" name="country"  value="{{ ($user->country)?$user->country:old('country')}}">-->
	<select name="country" class="form-control">
	<option selected="" name="country" value="">Country</option>
	<option value="Ukraine" {{ (isset($user->country) && ($user->country == 'Ukraine') ) ? "selected" : '' }}>Ukraine</option>
	<option value="India" {{ (isset($user->country) && ($user->country == 'India') ) ? "selected" : '' }}>India</option>
	<option value="Morocco" {{ (isset($user->country) && ($user->country == 'Morocco') ) ? "selected" : '' }}>Morocco</option>
	<option value="China" {{ (isset($user->country) && ($user->country == 'China') ) ? "selected" : '' }}>China</option>
	<option value="Colombia" {{ (isset($user->country) && ($user->country == 'Colombia') ) ? "selected" : '' }}>Colombia</option>                                
                            </select>
            </div>
        </div>
    </div>
    
    <div class="row">
      <div class="form-group {{ $errors->first('city', ' has-error') }}">
            <div class="col-md-4">  <label class="control-label">City</label></div>
            <div class="col-md-8">     <input type="text" placeholder="City" class="form-control phone" name="city"  value="{{ ($user->city)?$user->city:old('city')}}"> </div>
        </div>
    </div>
    
    
    
     <div class="row">
    <div class="form-group {{ $errors->first('status', ' has-error') }}">
            <div class="col-md-4">   <label class="control-label">Status</label></div>
            <div class="col-md-8">  <select name="status" class="form-control">
            <option value="1" {{ ($user->status == '1') ? 'selected' : ''}} >Active</option>
            <option value="0" {{ ($user->status == '0') ? 'selected' : ''}}>Deactive</option>
        </select>  </div>
        </div>
    </div>
     <div class="row">
    <div class="form-group {{ $errors->first('appearance', ' has-error') }}">
            <div class="col-md-4">   <label class="control-label">Appearance</label></div>
            <div class="col-md-8">  <select name="appearance" class="form-control">
            <option value="Arab" {{ ($user->appearance == 'Arab') ? 'selected' : ''}} >Arab</option>
            <option value="Asian" {{ ($user->appearance == 'Asian') ? 'selected' : ''}}>Asian</option>
            <option value="Ebony" {{ ($user->appearance == 'Ebony') ? 'selected' : ''}}>Ebony</option>
            <option value="Latin" {{ ($user->appearance == 'Latin') ? 'selected' : ''}}>Latin</option>
            <option value="White" {{ ($user->appearance == 'White') ? 'selected' : ''}}>White</option>        
        </select>  </div>
        </div>
    </div>
  

    
    
<!-- <div class="row">
    <div class="form-group">
           <button type="submit" class="btn green btn-primary" value="avtar" name="submit"> Save Changes </button>        
        <button type="reset" class="btn default"> Cancel </button>
        </div>
    </div>  --> 
          
             
</div>
 