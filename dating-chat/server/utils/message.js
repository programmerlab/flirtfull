var moment = require('moment');

var generateTextMessage = (from, text, senderImageUrl, receiverImageUrl, type) => {

	return {

		from,
		text,
        senderImageUrl,
        receiverImageUrl,
        type,
		createdAt: moment.valueOf()
	};
};

var generateFileMessage = (from, text, senderImageUrl, receiverImageUrl, fileName, type) => {

	return {

		from,
		text,
        senderImageUrl,
        receiverImageUrl,
        fileName,
        type,
		createdAt: moment.valueOf()
	};
};
var generateLocationMessage = (from, lat, long) => {

		return {

			from,
			url: `https://google.com/maps?q=${lat},${long}`,
			createdAt: moment.valueOf()
		};
};

module.exports = {

	generateTextMessage,
    generateFileMessage,
	generateLocationMessage
};