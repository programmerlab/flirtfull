<?php

declare(strict_types=1);

namespace Modules\Admin\Http\Controllers;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Modules\Admin\Models\User;
use View;
use Modules\Admin\Models\Roles;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Redirect;
use Input;
use DB;
/**
 * Class AdminController
 */
class HomeController extends Controller
{
    /**
     * @var  Repository
     */
    /**
     * Displays all admin.
     *
     * @return \Illuminate\View\View
     */
    /*
     * Dashboard
     */
    public function index(User $user)
    {
        return view('packages::auth.create', compact('user'));
    }

    public function deleteAll(Request $request)
    {
        $ids    = $request->get('ids');
        $table  = $request->get('table');
        $status = 0;

        if (count($ids)) {
            foreach ($ids as $key => $value) {
                \DB::table($table)->where('id', $value)->delete();
                $status = 1;
            }
        }


        if ($status == 1) {
            echo 'true';
        } else {
            echo 'false';
        }

        exit();
    }

    public function csvImport(Request $request)
    {
        try {
            $file = $request->file('importCsv');

            if ($file == null) {
                echo json_encode(['status' => 0,'message' => 'Please select  csv file!']);

                exit();
            }
            $ext = $file->getClientOriginalExtension();

            if ($file == null || $ext != 'csv') {
                echo json_encode(['status' => 0,'message' => 'Please select valid csv file!']);

                exit();
            }
            $mime = $file->getMimeType();

            $upload = $this->uploadFile($file);

            $rs =    \Excel::load($upload, function ($reader) use ($request) {
                $data = $reader->all();

                $table_cname = \Schema::getColumnListing('reports');

                $except = ['id','create_at','updated_at'];

                $input = $request->all();

                $contact =  new Report;
                foreach ($data  as $key => $result) {
                    foreach ($table_cname as $key => $value) {
                        if (in_array($value, $except)) {
                            continue;
                        }

                        if (isset($result->$value)) {
                            $contact->$value = $result->$value;
                            $status = 1;
                        }
                    }

                    if (isset($status)) {
                        $contact->save();
                    }
                }

                if (isset($status)) {
                    echo json_encode(['status' => 1,'message' => ' Data imported successfully!']);
                } else {
                    echo json_encode(['status' => 0,'message' => 'Invalid file type or content.Please upload csv file only.']);
                }
            });
        } catch (\Exception $e) {
            echo json_encode(['status' => 0,'message' => 'Please select csv file!']);

            exit();
        }
    }
    
    public function track(User $user, Request $request)
    {
        View::share('viewPage', 'Users');
        View::share('helper', new Helper);
        View::share('heading', 'Total Turnover');
        View::share('route_url', route('user'));

        $this->record_per_page = Config::get('app.record_per_page');
        $page_title  = 'Admin User';
        $page_action = 'Admin Users';                        
        
        //print_r($revenue); die;
        
        $get = '';
        $partnerid = "";
        if($request->get('type') == 'partnerid'){
            $partnerid = $request->get('value');
        }
        $age_group = '';
        if($request->get('type') == 'agegroup'){
            $age_group = $request->get('value');
        }
        $paymenttype = "";
        if($request->get('type') == 'paymenttype'){
            $paymenttype = $request->get('value');
        }
            //print_r($_GET);
            //**************for turnover************************************            
            
            $query = DB::table('credit_payment as p')->leftJoin('users as u','p.user_id','=','u.id')
                    ->select('p.user_id','p.amount','p.created_at','u.partner_id')                                                            
                    ->where(function ($query) {
                        $query->where('p.status', 'PAID')
                              ->orWhere('p.status', 'success');
                    });
            if($partnerid){
                $query->where('u.partner_id', $partnerid);
                $get = '&last_part=track&type=partnerid&option=0&value='.$partnerid;
            }
            if($age_group){
                $age = explode('-',$age_group);                
                $query->where('u.age', '>=' , $age[0]);
                $query->where('u.age', '<=' , $age[1]);
                $get = '&last_part=track&type=agegroup&option=0&value='.$age_group;
            }
            if($paymenttype){
                $query->where('p.plan_type', $paymenttype);
                $get = '&last_part=track&type=paymenttype&option=0&value='.$paymenttype;
            }
            $credits = $query->orderBy('p.id', 'desc')->get();
            //print_r($credits); die;        
                       
        
        foreach($credits as $key=>$row){
            $credits[$key]->date = explode(' ',$row->created_at)[0];            
        }
        
        $arr = array();

        foreach ($credits as $key => $item) {
           $arr[$item->date][$key] = $item;
        }
        
        ksort($arr, SORT_NUMERIC);
        $credits = $arr;
        //echo "<pre>"; print_r($credits); die;
        $userdata = []; $turnover = []; $total_turnover = 0; $total_usercount = 0;
        foreach($credits as $d => $credit){
            $amt = 0;
            $arrr = array();
            foreach($credit as $key=>$item){
                $arrr[$item->user_id][$key] = $item;
                $amt += $item->amount;
            }
            $userdata['date'] = $d;
            $userdata['amount'] = $amt;
            $total_turnover += $amt;
            $userdata['usercount'] = count($arrr);
            $total_usercount += count($arrr);
            $turnover[] = $userdata;
        }
        //print_r($total_usercount); die;        
        $pagination1 = $this->custom_page($turnover,'track',$get);
		$turnover = $pagination1[0];
		$pagination = $pagination1[1];
        //echo "<pre>"; print_r($turnover); die;
        //**************for turnover End************************************
        
        
        //**************for revenue start************************************
        $revenue = [];   $turnover = [];             
        for($i=0; $i<10;$i++){
            $todate = date('Y-m-d');
            $fromdate = date('Y-m-d', strtotime('-'.$i.' days', strtotime($todate)));
            
            $query = DB::table('credit_payment as p')->leftJoin('users as u','p.user_id','=','u.id')
                    ->select('p.user_id','p.amount','p.created_at','u.partner_id')                                                            
                    ->where(function ($query) {
                        $query->where('p.status', 'PAID')
                              ->orWhere('p.status', 'success');
                    });
            if($partnerid){
                $query->where('u.partner_id', $partnerid);
                $get = '&last_part=track&type=partnerid&option=0&value='.$partnerid;
            }
            if($age_group){
                $age = explode('-',$age_group);                
                $query->where('u.age', '>=' , $age[0]);
                $query->where('u.age', '<=' , $age[1]);
                $get = '&last_part=track&type=agegroup&option=0&value='.$age_group;
            }
            if($paymenttype){
                $query->where('p.plan_type', $paymenttype);
                $get = '&last_part=track&type=paymenttype&option=0&value='.$paymenttype;
            }
            $credits = $query->orderBy('p.id', 'desc')->where('p.created_at','>=',$fromdate.' 00:00:00')->where('p.created_at','<=',$fromdate.' 23:59:59')->get();

            $arr = array();
            foreach ($credits as $key => $item) {
               $arr[$item->user_id][$key] = $item;               
            }
            
            //ksort($arr, SORT_NUMERIC);
            $credits = $arr;
            $f_amt = 0; $usercount = 0; $payout = 0;
            foreach($credits as $d => $credit){
                $amtt = 0; $commission = 0;
                $arrr = array();
                foreach($credit as $key=>$item){
                    $arrr[$item->user_id][$key] = $item;
                    $amtt += $item->amount;
                        if($item->partner_id){
                            $partner = DB::table('partner')->where('id', $item->partner_id)->first();
                            $commission += (($partner->commission/100)*($item->amount));
                        }
                }
                $f_amt = $amtt;
                $usercount = count($arrr);
                $payout = $commission;
            }
                                
            //echo count($arrr);
            
            $users_created = User::where('role_type', 3)->where('is_deleted', 0)->where('profile_created_at', $fromdate);
            if($partnerid){
                $users_created->where('partner_id', $partnerid);                
            }
            if($age_group){
                $age = explode('-',$age_group);                
                $users_created->where('age', '>=' , $age[0]);
                $users_created->where('age', '<=' , $age[1]);                
            }            
            $users_created = $users_created->count();
            
            
            $users_activated = User::where('role_type', 3)->where('status', 1)->where('is_deleted', 0)->where('profile_activated_at', $fromdate);
            if($partnerid){
                $users_activated->where('partner_id', $partnerid);                
            }
            if($age_group){
                $age = explode('-',$age_group);                
                $users_activated->where('age', '>=' , $age[0]);
                $users_activated->where('age', '<=' , $age[1]);                
            }
            $users_activated = $users_activated->count();
            
            $users_deleted = User::where('role_type', 3)->where('is_deleted', 1)->where('profile_deleted_at', $fromdate);
            if($partnerid){
                $users_deleted->where('partner_id', $partnerid);                
            }
            if($age_group){
                $age = explode('-',$age_group);                
                $users_deleted->where('age', '>=' , $age[0]);
                $users_deleted->where('age', '<=' , $age[1]);                
            }
            $users_deleted = $users_deleted->count();
            
            $cr = ($users_created) ? round(($users_activated / $users_created),2) : '0';
            $del = ($users_activated) ? round(($users_deleted / $users_activated),2) : '0';
            $revenue[] = [$fromdate,$users_created,$users_activated,$users_deleted,$cr,$del];
            $turnover[] = [$fromdate,$usercount,$f_amt,$payout]; 
        }
        //**************for revenue End************************************
        //print_r($turnover); die;
        
        $partner = DB::table('partner')                    
                    ->where('status', '1')->where('is_deleted', '0')                                        
                    ->orderBy('id', 'desc')->get();
        
        $roles = Roles::all();

        $js_file = ['common.js','bootbox.js','formValidate.js'];

        return view('admin::users.track.index', compact('js_file', 'roles', 'status', 'users', 'page_title', 'page_action', 'roles' ,'role_type','turnover','pagination','partner','total_turnover','total_usercount','revenue'));
    }
    
    public function get_value(){
        $type = $_POST['type'];
        if($type == 'partnerid'){
            $partner = DB::table('partner')                    
                    ->where('status', '1')->where('is_deleted', '0')                                        
                    ->orderBy('id', 'desc')->get();
            foreach($partner as $row){
                echo '<option value="'.$row->id.'">'.$row->name.'</option>';
            }                    
        }else if($type == 'affiliateid'){
            echo '<option>1</option>';
        }else if($type == 'domain'){
            echo '<option>flirtfull.com</option>';
        }else if($type == 'agegroup'){
            echo '<option value="18-25">18-25</option>
                  <option value="26-35">26-35</option>
                  <option value="36-45">36-45</option>
                  <option value="46-55">46-55</option>
                  <option value="56-99">56-99</option>';
        }else if($type == 'paymenttype'){
            echo '<option value="credits">credits</option>                  
                  <option value="premiums">premiums</option>';
        }
    }
    
    protected function custom_page($arr,$pagelink,$get){
		$page = ! empty( $_GET['page'] ) ? (int) $_GET['page'] : 1;
		$total = count( $arr ); //total items in array    
		$limit = 10; //per page    
		$totalPages = ceil( $total/ $limit ); //calculate total pages
		$page = max($page, 1); //get 1 page when $_GET['page'] <= 0
		$page = min($page, $totalPages); //get last page when $_GET['page'] > $totalPages        
		$offset = ($page - 1) * $limit;
		if( $offset < 0 ) $offset = 0;		
		$arr = array_slice( $arr, $offset, $limit );
		
		$link = $pagelink.'?page=%d';
		$pagerContainer = '<ul class="pagination" role="navigation">';   
		if( $totalPages != 0 ) 
		{
		  if( $page == 1 ) 
		  { 
			$pagerContainer .= ''; 
		  } 
		  else 
		  { 
			$pagerContainer .= sprintf( '<li class="page-item">
						<a class="page-link" href="'.$link.$get.'" rel="prev" aria-label="« Previous">‹</a>
					</li>', $page - 1 ); 
		  }
		  for($i=1;$i<=$totalPages;$i++){
			if($page == $i){
				$pagerContainer .= '<li class="page-item active" aria-current="page"><span class="page-link">'.$i.'</span></li>';
			}else{
				$pagerContainer .= '<li class="page-item"><a class="page-link" href="http://35.242.157.120/admin/'.$pagelink.'?page='.$i.$get.'">'.$i.'</a></li>';	
			}	
		  }
		  if( $page == $totalPages ) 
		  { 
			$pagerContainer .= ''; 
		  }
		  else 
		  { 
			$pagerContainer .= sprintf( '<li class="page-item">
						<a class="page-link" href="'.$link.$get.'" rel="next" aria-label="Next »">›</a>
					</li>', $page + 1 ); 
		  }           
		}                   
		$pagerContainer .= '</ul>';
		return array($arr,$pagerContainer);
	}
    
    function group_by($key, $data) {
    $result = array();
        
        foreach($data as $val) {
            if(array_key_exists($key, $val)){
                $result[$val[$key]][] = $val;
            }else{
                $result[""][] = $val;
            }
        }
    
        return $result;
    }
    
}
