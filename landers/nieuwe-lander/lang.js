window.lang_js = {
    required_field: "Dit veld is verplicht",
    name_sym: "Cijfers, tekens en onderstrepingsteken zijn toegestaan.",
    name_min: "Minimaal 3 karakters",
    name_max: "Maximaal 20 karakters",
    name_exists: "Gebruikersnaam bestaat reeds",
    age_min: "Je moet minimaal 18 jaar of ouder zijn.",
    pwd_min: "Minimaal 6 karakters",
    pwd_max: "Maximaal 20 karakters",
    email_wrong: "E-mailadres niet geldig"
};