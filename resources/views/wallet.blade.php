@extends('layout.default')
@section('content')
<style type="text/css">
</style>
	<?php            
            if (Auth::check()){               
                //$wallet = $helper->getWallet(Auth::user()->id);
            }else{         
                $wallet = [];
            }
            ?>
	<?php //print_r($wallet); ?>
	<section class="search_list_outer search_detail wallets_page">
        <div class="container">
	        <div class="wallet_outer_center"> 
	            <!-- wallet outer -->
	            <div class="wallet_outer">
		            <div class="wallet_col_full">
		              	<div class="wallet_p_box">
			              	<div class="wallet_lft">
			              		<div class="wallet_img">
		                  			<img src="{{ asset('public')  }}/images/wallet_premium.png">
			                  		<h2>Premium</h2>
			              		</div>
			              		<div class="wallet_count">
				                  	<h3>{{ (isset($wallet->premiums) && ($wallet->premiums > 0) ) ? date('d M Y',($wallet->premiums)): 'No' }}</h3>
			                  	</div>
							</div>
		                  	<div class="wallet_rgt">
			                  <!--<a class="wallet_btn btn btn-info" data-plan="premium" data-toggle="modal" data-target="#upgrade_myModal">Buy Premium</a>-->
							  <a class="wallet_btn btn btn-info" href="{{ url('/premiums') }}">Buy Premium</a>
			                  <div>
		              			<span class="w_info_btn">
		              				<i class="fa fa-question"></i>
		              			</span>
		              			 <div class="wallet_info_text">
				                  	<h4>Get Instant Access To:</h4>
			                  		<ul>
	                  				  <li>Private Pictures.</li>
					                  <li>See Who Liked & Visited You</li>
					                  <li>Free Monthly Chat Credits</li>
					                  <li>Send Proposals</li>
					                  </ul>
					              </div>
		              		 </div>
			                </div>
		             	</div>
		            </div>
		            <div class="wallet_col_full">
		             	<div class="wallet_p_box">
              				<div class="wallet_lft">
              					<div class="wallet_img">
			                      <img src="{{ asset('public')  }}/images/wallet_credit.png">
			                      <h2>credit</h2>
			             		</div>
			                    <div class="wallet_count">
		                  		   <h3 id="updatedwallet">{{ isset($wallet->credits) ? $wallet->credits: '0' }}</h3>
		              		    </div>
	             			</div>
	                    	<div class="wallet_rgt">
			                  <!--<a class="wallet_btn btn btn-info credit_btn" data-plan="credit" data-toggle="modal" data-target="#upgrade_myModal">Add Credits</a>-->
							  <a class="wallet_btn btn btn-info credit_btn" href="{{ url('/credits') }}">Add Credits</a>
			                  <div>
		              			<span class="w_info_btn">
		              				<i class="fa fa-question"></i>
		              			</span>
		              			<div class="wallet_info_text">
				                  	<ul>
				                  	  <li>For Chatting with Your Flirts</li>
					                  <li>Swap Credits for Diamonds</li>
				                    </ul>
			              		</div>
		              		 </div>
			              	</div>
		              	</div>
		            </div>
		            <div class="wallet_col_full">
		              <div class="wallet_p_box">
					  <form action="{{ url('/add-diamonds') }}" method="post">
		              	<div class="wallet_lft">
		              		<div class="wallet_img">
			                  <img src="{{ asset('public')  }}/images/wallet_diamond.png">
			                  <h2>diamond</h2>
		                  	</div> 
		                  	<div class="wallet_count">                 
			                  <div class="add-diamonds">
			                    <h3 id="updateddiamonds">{{ isset($wallet->diamonds) ? $wallet->diamonds: '0' }}</h3>
			                  </div>
		              		</div>
		              	</div>
		              	<div class="wallet_rgt">
	              		 @if( isset($wallet->credits) && ($wallet->credits > '0') )
		                    <div class="diamond-select">
		                    	<select name="diamonds" class="form-control">
		                          <option value="3">3 Diamonds for 1 Credit</option>
		                          <option value="6">6 Diamonds for 2 Credit</option>
		                          <option value="9">9 Diamonds for 3 Credit</option>
		                          <option value="12">12 Diamonds for 4 Credit</option>
		                          <option value="15">15 Diamonds for 5 Credit</option>
		                    	</select>
		                    </div>
		                    @endif    
		              		<button type="submit" class="wallet_btn btn btn-info">ADD Diamonds</button>
		              		<div>
		              			<span class="w_info_btn">
		              				<i class="fa fa-question"></i>
		              			</span>
	              			 	<div class="wallet_info_text">
		                  		  <ul>
				                  	<li>Send Hot Proposals to Your Flirts..</li>
			                   	  </ul>
		              	 		</div>  
		              		</div>
		              	</div>
						</form>	
		              </div>
		            </div>
		        </div>	
	            <!-- wallet outer -->
	        </div>
        </div>
    </section>
      
@endsection	  