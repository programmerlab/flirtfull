<div class="modal-content">
                                <div class="modal-header">                                  
                                  <h4 class="modal-title text-center" id="premium_title">Premium: Become a premium member and get access to all website features. </h4>                                  
                                  <p></p>
                                    
                                </div>
                                <?php
                                        $helper = new Helper();
                                        $diamonds = $helper->getAllDiamonds();
                                        $credits = $helper->getAllCredits();
                                        $coins = $helper->getAllCoins();
                                        $premiums = $helper->getAllPremiums();                
                                        ?>  
                                
                                <div class="modal-body">
                                  <div class="row">
                                      <div class="col-md-12">
                                        <div class="flipster_tab menulist">
                                          <ul id="active_plan">
                                            <!--<li data-show="premium_tab">Premium</li>-->
                                            <!--<li data-show="credit_tab">Credit</li>-->
                                          </ul>
                                        </div>                                        
                                        <!-- tab content-->
                                        
                                          <div class="wallet_text">
                                              <h4>See private pictures</h4>
                                              <h5>See who visited / liked your profile</h5>
                                            </div>
                                            <div class="flipster">
                                             <ul>
                                                <?php $i = 1; ?>
                                                <?php $__currentLoopData = $premiums; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $premium): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <li>
                                                  <div class="upgrade_inner">
                                                      <div class="upgrade-img">
                                                        <!-- <img src="<?php echo e(asset('public')); ?>/thumbnail/nolemoniquep1.jpg"> -->
                                                        <img src="<?php echo e(asset('public/thumbnail/prem_'.$i.'.jpg')); ?>" onerror="this.src="<?php echo e(asset('public')); ?>/thumbnail/nolemoniquep1.jpg">
                                                        <!-- offer banner -->
                                                        <div class="credit_bnr">
                                                          <img src="<?php echo e(asset('public')); ?>/images/icons/offer_banner.png">
                                                          <div class="credit_bnr_txt">
                                                            <span class="bnr_txt_1"><?php echo e(($i == 1) ? 10 : (($i ==2) ? 15 : 25)); ?>  </span>
                                                            <span class="bnr_txt_2">Free credits <br/>per month</span>
                                                          </div>
                                                        </div>
                                                        <!-- offer banner -->
                                                      </div>
                                                      <div class="upgrade_inner_info">
                                                        <h4><span class="amount"><span class="currency">£</span><?php echo e($premium->price); ?></span></h4>
                                                        <h2><?php echo e($premium->month); ?> <span>Month</span></h2>
                                                        <h3><span class="currency">£</span><?php echo e($premium->qty); ?>/month</h3>
                                                        <a  class="upgrade-btn" id="premium<?php echo e($premium->id); ?>" onclick="select_plan('premiums',<?php echo e($premium->id); ?>);">select <i class="fa fa-check"></i></a>
                                                      </div>
                                                    </div>
                                                  </li>
                                                  <?php $i++; ?>    
                                                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                              </ul>
                                            </div>
                                        
                                        <!-- tab content-->
                                      </div>
                                    </div>
                                        
                                    <div class="pymnt-form">
                                        <div class="row">
                                            <div class="col-md-5 col-sm-12 payment_right">
                                                <?php if(Auth::check() && Auth::user()->status == 1): ?>  
                                                <form id="purchase_credit" action="">
                                                <?php else: ?>
                                                <form id="" action="">              
                                                    <?php endif; ?>              
                                                   <!--<input type="hidden" id="currency" value="GBP">
                                                   <input type="hidden" id="amount" value="10.00">-->
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                          <!-- <div class="form-group">
                                                            <input type="text" class="form-control" id="" placeholder="Card number">
                                                          </div> -->
                                                         </div>
                                                       </div>                       
                                                    <div class="pay_radio">
                                                       <!--<input type="hidden" name="plan_type" id="plan_type">    
                                                       <input type="hidden" name="plan" id="plan" >
                                                       <input type="hidden" id="payment_method" name="payment_method" value="flexpay">-->
                                                       <!--<div class="selectbox">
                                                         <select id="paynl_id" name="paynl_id"></select>
                                                       </div>-->
                                                    </div>
                                                      <span id="plan_error" style="display:none; color:red;">Please Select Plan</span>
                                                     <?php if(Auth::check() && Auth::user()->status == 1): ?>  
                                                    <button type="submit" class="btn btn-default">Purchase</button>
                                                    <?php else: ?>
                                                    <button class="btn btn-default" data-toggle="modal" data-target="#notverifyModal">Purchase</button>    
                                                    <?php endif; ?>  
                                                </form>
                                                    </div>
                                            <div class="col-md-7 col-sm-12">
                                              <ul>
                                                  <li><a href="#"><img src="<?php echo e(asset('public')); ?>/images/visa.png"></a></li>                            
                                                    <li><a href="#"><img src="<?php echo e(asset('public')); ?>/images/master_card.png"></a></li>
                                                    <li><a href="#"><img src="<?php echo e(asset('public')); ?>/images/amex.png"></a></li>
                                                    <li><a href="#"><img src="<?php echo e(asset('public')); ?>/images/paypal.png"></a></li>    
                                                </ul>
                                                <p>All payments are done to and processed by Oddigits BV. To protect your privacy, your bank statement will say: "DTN Oddigits". All prices include 25% VAT.</p>
                                                <p id="premium_footer">We will not renew your subscription or rebill your card.</p>
                                                
                                            </div>
                                        </div>
                                      
                                        </div>
                                    <div class="row text-center">                                      
                                        <div class="pypl-btn" id="paypal">
                                            <button type="button"><span class="pay">Pay</span><span class="pal">Pal</span></button>
                                        </div>
                                      </div>
                                        </div>                                                                                        
                                    </div>