<?php

declare(strict_types=1);

namespace Modules\Admin\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use URL;

class Diamond extends Authenticatable
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'diamonds';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    /**
     * The primary key used by the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'qty',                
        'discount',
        'price',
        'txn_fee'        
    ];  // All field of user table h


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $guarded = ['created_at', 'updated_at', 'id'];

    // Return user record
    public function getDiamondDetail($id = null)
    {
        if ($id) {
            return Diamond::find($id);
        }

        return Diamond::all();
    }

   
    
    
    public function role()
    {
        return $this->belongsTo('Modules\Admin\Models\Role', 'id','role_type');
    }

}
