	<title>{{ env('APP_NAME') }}</title>
	<link rel="shortcut icon" type="image/png" href="{{ asset('public/images/fevicon.png') }}"/>
	<link href="{{ asset('public/css/font-awesome.min.css') }}" rel="stylesheet">
	<link href="{{ asset('public/css/bootstrap.min.css') }}" rel="stylesheet">
	<link rel="stylesheet" href="{{ asset('public/css/owl.carousel.min.css') }}">
  <!-- <link href="{{ asset('public/css/ninja-slider.css') }}" rel="stylesheet"> -->

  <link href="{{ asset('public/css/fotorama.css') }}" rel="stylesheet">
  <link rel="stylesheet" href="{{ asset('public/css/croppie.css') }}" />
  
  <link href="{{ asset('public/css/style.css') }}" rel="stylesheet">
  <link href="{{ asset('public/css/responsive.css') }}" rel="stylesheet">
  
  <script src="{{ asset('public/js/jquery.min.js') }}"></script>
  <script src="{{ asset('public/js/bootstrap.min.js') }}"></script>
  <script src="{{ asset('public/js/owl.carousel.js') }}"></script>
  <!-- <script src="{{ asset('public/js/ninja-slider.js') }}"></script> -->
  <script src="{{ asset('public/js/fotorama.js') }}"></script>
  <script src="https://maps.googleapis.com/maps/api/js?key={{ env('GOOGLE_KEY') }}&sensor=false&libraries=places" type="text/javascript"></script>
  <script src="{{ asset('public/js/croppie.js') }}"></script>
  <script src="https://cdn.ckeditor.com/4.11.4/standard/ckeditor.js"></script>
  <!-- Latest compiled and minified CSS -->

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.2/css/bootstrap-select.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>  
<!-- Latest compiled and minified JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.2/js/bootstrap-select.min.js"></script>
    
    
<!--Firebase chat conf.-->
<script src="https://www.gstatic.com/firebasejs/5.0.4/firebase-app.js"></script>
<script src="https://www.gstatic.com/firebasejs/5.0.4/firebase-auth.js"></script>
<script src="https://www.gstatic.com/firebasejs/5.0.4/firebase-database.js"></script>
<script src="https://www.gstatic.com/firebasejs/5.0.4/firebase-storage.js"></script>
<script src="https://www.gstatic.com/firebasejs/5.0.4/firebase-messaging.js"></script>
<script src="https://www.gstatic.com/firebasejs/5.0.4/firebase-functions.js"></script>

<script>
  // Your web app's Firebase configuration
  var firebaseConfig = {
    apiKey: "AIzaSyCh56LGkwDZaSlqs5t0LNZBB-ppT0UQm4c",
    authDomain: "flirtengine.firebaseapp.com",
    databaseURL: "https://flirtengine.firebaseio.com",
    projectId: "flirtengine",
    storageBucket: "flirtengine.appspot.com",
    messagingSenderId: "765124051377",
    appId: "1:765124051377:web:63bf28a0cae41e6a"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);
</script>
<!--Firebase chat conf.-->
<script type="text/javascript">
	var interval = null;
    
	$(document).on('ready',function(){		
			sendMsgCron();
		
	});
	
	
	function sendMsgCron(){ //1 :text,2:proposal,3:likes,4:favorite
		$.ajax({
			url: "https://triggersengine.com/api/v1/getMessagesCron", 
			type: 'GET',
			crossDomain: true,
            crossOrigin: true,
			success: function(result){
				if(result.status == 1){
					
					if(result.data.length){
                        var time = 1000;
						$.each(result.data, function( index, value ) {                                                    
                        setTimeout(function () { 
							var msgText = value.message;
							var messageType =  'text';
							var imageText =  '';

							if(value.message_type == 1){
								if(value.attachment != 0){
									messageType = 'file';
									imageText = msgText;
									msgText = value.attachment;
								}
							}else if(value.message_type == 2){
								messageType = 'proposal';
                                msgText = '<img src="https://www.flirtfull.com/public/images/profile_image/'+msgText+'" style="margin:2px">';
							}else if(value.message_type == 3){
								messageType = 'likes';
							}else if(value.message_type == 4){
								messageType = 'favorite';
							}

							//console.log(msgText);
							//console.log(messageType);
							//return true;

							firebase.database().ref('/users/'+value.sender_id).once('value').then(function(snapshot1) {

								var snapshot1 = snapshot1.val();
                                if(snapshot1 != null){
                                    var senderId = value.sender_id;
                                    var senderName = snapshot1.userName;
                                    var senderImage = snapshot1.userImage;
                                    //console.log(value.receiver_id);
                                    firebase.database().ref('/users/'+value.receiver_id).once('value').then(function(snapshot2) {
    
                                        var snapshot2 = snapshot2.val();
                                        console.log(snapshot2);
                                        if(snapshot2 != null){
                                            var receiverId = value.receiver_id;
                                            var receiverName = snapshot2.userName;
                                            var receiverImage = snapshot2.userImage;
        
        
                                            var chatRoom = senderId + '_' + receiverId;
                                            if(receiverId < senderId){
                                                chatRoom = receiverId + '_' + senderId;
                                            }
        
                                            firebase.database().ref().child('chat_history/'+senderId).child(receiverId).once('value').then(function(snapshot3) {
                                                var snapshot3 = snapshot3.val();
                                                var msgCount = 0;
                                                var hisType = 1;
                                                
                                                if (snapshot3 != null){
                                                    var msgCount = Number(snapshot3.messageCount)+1;
                                                    if(snapshot3.historyType == 1){
                                                        if(snapshot3.lastSenderId != senderId){
                                                            hisType = 2;
                                                        }
                                                    }else{
                                                        hisType = 2;
                                                    }
                                                }
        
                                                var insertData = {
                                                    deletteby : '',
                                                    message : msgText,
                                                    messageCount : msgCount,
                                                    name : receiverName,
                                                    profilePic : receiverImage,
                                                    timestamp : $.now(),
                                                    uid : receiverId.toString(),
                                                    historyType : hisType,
                                                    lastSenderId : senderId.toString(),
                                                    messageType : messageType,
                                                    historyLoad : '0',
                                                };
                                                firebase.database().ref().child('chat_history/'+senderId).child(receiverId).set(insertData);
                                            });
        
        
        
                                            firebase.database().ref().child('chat_history/'+receiverId).child(senderId).once('value').then(function(snapshot4) {
                                                var snapshot4 = snapshot4.val();
                                                var msgCount = 0;
                                                var hisType = 1;
                                                if (snapshot4 != null){
                                                    var msgCount = Number(snapshot4.messageCount)+1;
                                                    if(snapshot4.historyType == 1){
                                                        if(snapshot4.lastSenderId != senderId){
                                                            hisType = 2;
                                                        }
                                                    }else{
                                                        hisType = 2;
                                                    }
                                                }
        
        
                                                var insertData = {
                                                    deletteby : '',
                                                    message : msgText,
                                                    messageCount : msgCount,
                                                    name : senderName,
                                                    profilePic : senderImage,
                                                    timestamp : $.now(),
                                                    uid : senderId.toString(),
                                                    historyType : hisType,
                                                    lastSenderId : senderId.toString(),
                                                    messageType : messageType,
                                                    historyLoad : '0',
                                                };
                                                firebase.database().ref().child('chat_history/'+receiverId).child(senderId).set(insertData);
                                            });
        
        
                                            var insertData = {
                                                deleteby:'',
                                                message : msgText,
                                                messageCount: 1,
                                                name : senderName,
                                                profilePic : senderImage,
                                                timestamp : $.now(),
                                                uid : senderId.toString(),
                                                messageType : messageType,
                                                imageText : imageText,
                                            };
                                            firebase.database().ref().child('chat_rooms/').child(chatRoom).push(insertData);
        
                                            $.ajax({
                                                url: "https://triggersengine.com/api/v1/updateMessagesCron", 
                                                type: 'POST',
                                                //data:'{"id:"'+value.id+'"}',
                                                data:JSON.stringify({id:value.id}),
                                                contentType: "application/json",
                                                dataType: "application/json",
                                                //processData :false,
                                                success: function(result){}
                                            });
    
                                        
                                        } //if snapshot2 is not null
    
    
                                    });
                                } //if snapshot2 is not null
                                
							});
                        }, time);
                        time += 2000;
						});                        
					}else{
						clearInterval(interval); // stop the interval
					}
				}else{
					clearInterval(interval); // stop the interval
				}
			}
		});
	}

</script>