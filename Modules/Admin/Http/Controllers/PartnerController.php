<?php

declare(strict_types=1);

namespace Modules\Admin\Http\Controllers;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Redirect;
use Input;
use Modules\Admin\Http\Requests\PartnerRequest;
use Modules\Admin\Models\Partner;
use Route;
use View;
use Session;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Datatables;


/**
 * Class AdminController
 */
class PartnerController extends Controller
{
    /**
     * @var  Repository
     */

    /**
     * Displays all admin.
     *
     * @return \Illuminate\View\View
     */
    public function __construct()
    {
        $this->middleware('admin');
        View::share('viewPage', 'Plan');
        View::share('helper', new Helper);
        View::share('heading', 'Partners');
        View::share('route_url', route('partner'));

        $this->record_per_page = Config::get('app.record_per_page');
    }

    protected $partners;

    /*
     * Dashboard
     * */

    public function index(Partner $partner, Request $request)
    {        
        $page_title  = 'Partners';
        $page_action = 'Partners';
        

        if ($request->ajax()) {
            $id           = $request->get('id');
            $status       = $request->get('status');
            $partner         = Partner::find($id);
            $s            = ($status == 1) ? $status=0:$status=1;
            $partner->status = $s;
            $partner->save();
            echo $s;

            exit();
        }
        
        $partners = Partner::orderBy('id', 'desc')->where('status', '1')->where('is_deleted', '0')->Paginate($this->record_per_page);
        //print_r($partners); die;        

        $js_file = ['common.js','bootbox.js','formValidate.js'];

        return view('admin::partners.partner.index', compact('js_file', 'status', 'partners', 'page_title', 'page_action'));
    }


    public function anyData()
    {
        return Datatables::of(Partner::query())->make(true);
    }

    public function test_table(Partner $partner, Request $request)
    {        
        $page_title  = 'Admin Partner';
        $page_action = 'Admin Partner';
        

        if ($request->ajax()) {
            $id           = $request->get('id');
            $status       = $request->get('status');
            $partner         = Partner::find($id);
            $s            = ($status == 1) ? $status=0:$status=1;
            $partner->status = $s;
            $partner->save();
            echo $s;

            exit();
        }
        // Search by name ,email and group
        $search    = Input::get('search');
        $status    = Input::get('status');
        $role_type = Input::get('role_type');

        if ((isset($search) && !empty($search)) or  (isset($status) && !empty($status)) or !empty($role_type)) {
            $search = isset($search) ? Input::get('search') : '';

            $partners = Partner::where(function ($query) use ($search,$status,$role_type) {
                if (!empty($search)) {
                    $query->Where('first_name', 'LIKE', "%$search%")
                        ->OrWhere('last_name', 'LIKE', "%$search%")
                        ->OrWhere('email', 'LIKE', "%$search%");
                }

                if (!empty($status)) {
                    
                    $status =  ($status == 'active')?1:0;
                    $query->Where('status', $status);
                }

                if ($role_type) {
                    $query->Where('role_type', $role_type);
                }
            })->where('role_type', 3)->Paginate($this->record_per_page);
        } else {
            $partners = Partner::orderBy('id', 'desc')->where('role_type', 3)->where('is_deleted', 0)->Paginate($this->record_per_page);
        }
        $roles = Roles::all();

        $js_file = ['common.js','bootbox.js','formValidate.js'];

        return view('admin::partners.partner.test_table', compact('js_file', 'roles', 'status', 'page_title', 'page_action', 'roles' ,'role_type'));
    }

    /*
     * create Group method
     * */

    public function create(Partner $partner)
    {
        $page_title  = 'Partner';
        $page_action = 'Create Partner';
        
        $js_file     = ['common.js','bootbox.js','formValidate.js'];


        return view('admin::partners.partner.create', compact('js_file', 'partner', 'page_title', 'page_action'));
    }

    /*
     * Save Group method
     * */

    public function store(PartnerRequest $request, Partner $partner)
    {
        //echo "<pre>"; print_r(Input::all()); die;
        $partner->partnerid = substr($request->get('name'),0,3).rand(10000000,99999999);
        $partner->status = 1;
        //print_r($partner); die;
        $partner->fill(Input::all());
        
        
        $action = $request->get('submit');        
        
        
        $partner->save();
        
        $js_file = ['common.js','bootbox.js','formValidate.js'];

        return Redirect::to(route('partner'))
            ->with('flash_alert_notice', 'New record successfully created.');
    }

    /*
     * Edit Group method
     * @param
     * object : $partner
     * */

    public function edit(Partner $partner)
    {
        //echo "<pre>"; print_r($partner); die;
        $page_title  = 'Partner';
        $page_action = 'Show Partner';                
        
        $js_file     = ['common.js','bootbox.js','formValidate.js'];
                
        //echo "<pre>"; print_r($partner); die;
        return view('admin::partners.partner.edit', compact('js_file', 'partner', 'page_title', 'page_action'));
    }

    public function update(Request $request, Partner $partner, PartnerRequest $partnerrequest)
    {        
        //echo "<pre>"; print_r(Input::all()); die;        
        $partner->fill(Input::all());
        $action          = $request->get('submit');
        $partner->save();
                
        if ($request->get('role') == 3) {
            $Redirect = 'clientpartner';
        } else {
            $Redirect = 'partner';
        }

        return Redirect::to(route($Redirect))
            ->with('flash_alert_notice', 'Record successfully updated.');
    }
    /*
     *Delete Partner
     * @param ID
     *
     */
    public function destroy(Request $request, $partner)
    { //print_r($partner);
        //echo $partner->id;die;
        $partner->is_deleted = 1;
        $partner->save();
        //$partner->delete();
        return Redirect::to(route('partner'))
            ->with('flash_alert_notice', 'Record  successfully deleted.');
    }

    public function show(Partner $partner)
    {
    }
    
    public function image_delete(Partner $partner, Request $request, Gallery_image $gallery_image)
    {
        //echo $request->ajax('id');        
            $id           = $request->get('id');            
            $status       = $request->get('is_deleted');
            $partner         = Gallery_image::find($id);
            $s            = ($status == 1) ? $status=0:$status=1;
            $partner->is_deleted = $s;
            $partner->save();
            echo $s;

            exit();        
    }
    
    public function delete_image(Partner $partner, Request $request, Gallery_image $gallery_image)
    {
        //echo $request->ajax('id');        
            $id           = $request->get('id');
            $partnerid           = $request->get('partnerid');
            DB::table('gallery_images')->where('id', '=', $id)->delete();
            return Redirect::to(url('admin/partner/'.$partnerid.'/edit'))
                ->with('flash_alert_notice', 'Image successfully deleted.');
            exit();        
    }
    
    public function search1(Partner $partner, Request $request)
    {
        //echo $request->ajax('id');        
            $country      = $request->get('country');
            $age      = $request->get('age');
            $gender      = $request->get('gender');
            $status      = $request->get('status');
            
            
            $partners = Partner::where(function ($query) use ($country,$age,$gender,$status) {
                if (!empty($country)) {
                    $query->Where('country', 'LIKE', "%$country%");                        
                }

                if (!empty($age)) {
                    $age = explode('_',$age);
                    $query->Where('age', '>=', $age[0]);
                    $query->Where('age', '<=', $age[1]);
                }

                if ($gender) {
                    $query->Where('gender', $gender);
                }
                
                if (!empty($status)) {
                    $status = ($status == 'yes') ? '1' : '0';
                    $query->Where('status', $status);
                }
            })->where('role_type', 3)->Paginate(18);
            
            //echo "<pre>"; print_r($partners); die;            
            $roles = Roles::all();

        $js_file = [];

        return view('admin::partners.partner.index', compact('js_file', 'roles', 'country', 'partners', 'page_title', 'page_action', 'roles', 'role_type')); 
    }
    
    public function search(Partner $partner, Request $request)
    {      $last_part = $request->get('last_part'); 
        //echo $request->ajax('id');        
            $country      = $request->get('country');
            $age      = $request->get('age');
            $gender      = $request->get('gender');
            $status      = $request->get('status');
            $relationship      = $request->get('relationship');
            $photo      = $request->get('photo');
            $appearance      = $request->get('appearance');
            $profile_type      = $request->get('profile_type');
            $adult_level      = $request->get('adult_level');
            
            //DB::enableQueryLog();
            
            $reslt = DB::table('partners')
                    ->join('partner_details', 'partners.id', '=', 'partner_details.partner_id')            
                    ->select('partners.*', 'partner_details.relationship');
            
            if (!empty($country)) {
                $reslt->Where('partners.country', 'LIKE', "%$country%");                        
            }
                
            if (!empty($age)) {
                $age = explode('_',$age);
                $reslt->Where('partners.age', '>=', $age[0]);
                $reslt->Where('partners.age', '<=', $age[1]);
            }
            
            if ($gender) {
                $reslt->Where('partners.gender', $gender);
            }
            
            if ($profile_type) {
                $reslt->Where('partners.profile_type', $profile_type);
            }
            
            if ($adult_level) {
                $reslt->Where('partners.adult_level', $adult_level);
            }
            
            if ($appearance) {
                $reslt->Where('partners.appearance', $appearance);
            }
            
            if (!empty($status)) {
                $status = ($status == 'yes') ? '1' : '0';
                $reslt->Where('partners.status', $status);
            }
            
            if (isset($photo)) {
                if($photo == '1'){
                    $reslt->Where('partners.profile_image', '!=',  '');
                }
                if($photo == '0'){
                    $reslt->Where('partners.profile_image','');
                    //$reslt->or_Where('partners.profile_image', '=',  'default.png');
                }
            }
            
            if ($relationship) {
                $reslt->Where('partner_details.relationship', $relationship);
            }

            if($last_part == 'deleted') {
                $page_title  = 'Player';
                $page_action = 'Deleted Players';
                $heading = 'Deleted Players';
                $reslt->Where('partners.is_deleted', '=',  '1');
            }
            else if($last_part == 'real') {
                $page_title  = 'Player';
                $page_action = 'Real Players';
                $heading = 'Real Players';
                $reslt->Where('partners.fake', '=',  '0');
                $reslt->Where('partners.is_deleted', '=',  '0');
            }
            else if($last_part == 'fake') {
                $page_title  = 'Player';
                $page_action = 'Fake Players';
                $heading = 'Fake Players';
                $reslt->Where('partners.fake', '=',  '1');
                $reslt->Where('partners.is_deleted', '=',  '0');
            }
            else if($last_part == 'unconfirmed') {
                $page_title  = 'Player';
                $page_action = 'Unconfirmed Players';
                $heading = 'Unconfirmed Players';
                $reslt->Where('partners.status', '=',  '0');
                $reslt->Where('partners.is_deleted', '=',  '0');
            }
                
            $partners = $reslt->where('partners.role_type', 3)->Paginate(30);
            
            //echo "<pre>"; print_r($partners); die;            
            $roles = Roles::all();

        $js_file = [];

        $search_slug = 'list-search';

        return view('admin::partners.partner.index', compact('js_file', 'roles', 'country', 'partners', 'page_title', 'page_action','heading',  'roles', 'role_type', 'search_slug')); 
    }
    
    public function setProfileImage(Partner $partner, Request $request, Gallery_image $gallery_image)
    {        
            $id           = $request->get('id');
            $partnerid           = $request->get('partnerid');
            
            $partner = DB::table('partners')->where('id', $partnerid)->first();            
            $old_image = $partner->profile_image;
            
            $gallery = DB::table('gallery_images')->where('id', $id)->first();            
            $new_image = $gallery->gallery_image;
            
            DB::table('partners')
            ->where('id', $partnerid)
            ->update(array('profile_image' => $new_image));
            
            DB::table('gallery_images')
            ->where('id', $id)
            ->update(array('gallery_image' => $old_image));
                        
            return Redirect::to(url('admin/partner/'.$partnerid.'/edit'))
                ->with('flash_alert_notice', 'Image Set as Profile successfully ');
            exit();        
    }


    public function deleted(Partner $partner, Request $request)
    {        
        $page_title  = 'Player';
        $page_action = 'Deleted Players';
        $heading = 'Deleted Players';
        

        if ($request->ajax()) {
            $id           = $request->get('id');
            $status       = $request->get('status');
            $partner         = Partner::find($id);
            $s            = ($status == 1) ? $status=0:$status=1;
            $partner->status = $s;
            $partner->save();
            echo $s;

            exit();
        }
        // Search by name ,email and group
        $search    = Input::get('search');
        $status    = Input::get('status');
        $role_type = Input::get('role_type');

        if ((isset($search) && !empty($search)) or  (isset($status) && !empty($status)) or !empty($role_type)) {
            $search = isset($search) ? Input::get('search') : '';

            $partners = Partner::where(function ($query) use ($search,$status,$role_type) {
                if (!empty($search)) {
                    $query->Where('first_name', 'LIKE', "%$search%")
                        ->OrWhere('last_name', 'LIKE', "%$search%")
                        ->OrWhere('email', 'LIKE', "%$search%");
                }

                if (!empty($status)) {
                    
                    $status =  ($status == 'active')?1:0;
                    $query->Where('status', $status);
                }

                if ($role_type) {
                    $query->Where('role_type', $role_type);
                }
            })->where('role_type', 3)->Paginate($this->record_per_page);
        } else {
            $partners = Partner::orderBy('id', 'desc')->where('role_type', 3)->where('is_deleted', 1)->Paginate($this->record_per_page);
        }
        $roles = Roles::all();

        $js_file = ['common.js','bootbox.js','formValidate.js'];

        return view('admin::partners.partner.index', compact('js_file', 'roles', 'status', 'partners', 'page_title', 'page_action','heading', 'roles', 'role_type'));
    }

    public function fake(Partner $partner, Request $request)
    {        
        $page_title  = 'Player';
        $page_action = 'Deleted Players';
        $heading = 'Fake Players';
        

        if ($request->ajax()) {
            $id           = $request->get('id');
            $status       = $request->get('status');
            $partner         = Partner::find($id);
            $s            = ($status == 1) ? $status=0:$status=1;
            $partner->status = $s;
            $partner->save();
            echo $s;

            exit();
        }
        // Search by name ,email and group
        $search    = Input::get('search');
        $status    = Input::get('status');
        $role_type = Input::get('role_type');

        if ((isset($search) && !empty($search)) or  (isset($status) && !empty($status)) or !empty($role_type)) {
            $search = isset($search) ? Input::get('search') : '';

            $partners = Partner::where(function ($query) use ($search,$status,$role_type) {
                if (!empty($search)) {
                    $query->Where('first_name', 'LIKE', "%$search%")
                        ->OrWhere('last_name', 'LIKE', "%$search%")
                        ->OrWhere('email', 'LIKE', "%$search%");
                }

                if (!empty($status)) {
                    
                    $status =  ($status == 'active')?1:0;
                    $query->Where('status', $status);
                }

                if ($role_type) {
                    $query->Where('role_type', $role_type);
                }
            })->where('role_type', 3)->Paginate($this->record_per_page);
        } else {
            $partners = Partner::orderBy('id', 'desc')->where('role_type', 3)->where('fake', 1)->where('is_deleted', 0)->Paginate($this->record_per_page);
        }
        $roles = Roles::all();

        $js_file = ['common.js','bootbox.js','formValidate.js'];

        return view('admin::partners.partner.index', compact('js_file', 'roles', 'status', 'partners', 'page_title', 'page_action','heading', 'roles', 'role_type'));
    }

    public function real(Partner $partner, Request $request)
    {        
        $page_title  = 'Player';
        $page_action = 'Real Players';
        $heading = 'Real Players';
        

        if ($request->ajax()) {
            $id           = $request->get('id');
            $status       = $request->get('status');
            $partner         = Partner::find($id);
            $s            = ($status == 1) ? $status=0:$status=1;
            $partner->status = $s;
            $partner->save();
            echo $s;

            exit();
        }
        // Search by name ,email and group
        $search    = Input::get('search');
        $status    = Input::get('status');
        $role_type = Input::get('role_type');

        if ((isset($search) && !empty($search)) or  (isset($status) && !empty($status)) or !empty($role_type)) {
            $search = isset($search) ? Input::get('search') : '';

            $partners = Partner::where(function ($query) use ($search,$status,$role_type) {
                if (!empty($search)) {
                    $query->Where('first_name', 'LIKE', "%$search%")
                        ->OrWhere('last_name', 'LIKE', "%$search%")
                        ->OrWhere('email', 'LIKE', "%$search%");
                }

                if (!empty($status)) {
                    
                    $status =  ($status == 'active')?1:0;
                    $query->Where('status', $status);
                }

                if ($role_type) {
                    $query->Where('role_type', $role_type);
                }
            })->where('role_type', 3)->Paginate($this->record_per_page);
        } else {
            $partners = Partner::orderBy('id', 'desc')->where('role_type', 3)->where('fake', 0)->where('is_deleted', 0)->Paginate($this->record_per_page);
        }
        $roles = Roles::all();

        $js_file = ['common.js','bootbox.js','formValidate.js'];

        return view('admin::partners.partner.index', compact('js_file', 'roles', 'status', 'partners', 'page_title', 'page_action','heading', 'roles', 'role_type'));
    }

    public function unconfirmed(Partner $partner, Request $request)
    {        
        $page_title  = 'Player';
        $page_action = 'Unconfirmed Players';
        $heading = 'Unconfirmed Players';
        

        if ($request->ajax()) {
            $id           = $request->get('id');
            $status       = $request->get('status');
            $partner         = Partner::find($id);
            $s            = ($status == 1) ? $status=0:$status=1;
            $partner->status = $s;
            $partner->save();
            echo $s;

            exit();
        }
        // Search by name ,email and group
        $search    = Input::get('search');
        $status    = Input::get('status');
        $role_type = Input::get('role_type');

        if ((isset($search) && !empty($search)) or  (isset($status) && !empty($status)) or !empty($role_type)) {
            $search = isset($search) ? Input::get('search') : '';

            $partners = Partner::where(function ($query) use ($search,$status,$role_type) {
                if (!empty($search)) {
                    $query->Where('first_name', 'LIKE', "%$search%")
                        ->OrWhere('last_name', 'LIKE', "%$search%")
                        ->OrWhere('email', 'LIKE', "%$search%");
                }

                if (!empty($status)) {
                    
                    $status =  ($status == 'active')?1:0;
                    $query->Where('status', $status);
                }

                if ($role_type) {
                    $query->Where('role_type', $role_type);
                }
            })->where('role_type', 3)->Paginate($this->record_per_page);
        } else {
            $partners = Partner::orderBy('id', 'desc')->where('role_type', 3)->where('status', 0)->where('is_deleted', 0)->Paginate($this->record_per_page);
        }
        $roles = Roles::all();

        $js_file = ['common.js','bootbox.js','formValidate.js'];

        return view('admin::partners.partner.index', compact('js_file', 'roles', 'status', 'partners', 'page_title', 'page_action','heading', 'roles', 'role_type'));
    }
    
    
}
