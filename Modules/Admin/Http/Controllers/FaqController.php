<?php

declare(strict_types=1);

namespace Modules\Admin\Http\Controllers;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Redirect;
use Input;
use Modules\Admin\Http\Requests\FaqRequest;
use Modules\Admin\Models\Faq;
use Route;
use View;
use Session;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Datatables;


/**
 * Class AdminController
 */
class FaqController extends Controller
{
    /**
     * @var  Repository
     */

    /**
     * Displays all admin.
     *
     * @return \Illuminate\View\View
     */
    public function __construct()
    {
        $this->middleware('admin');
        View::share('viewPage', 'Faq');
        View::share('helper', new Helper);
        View::share('heading', 'Faq');
        View::share('route_url', route('faq'));

        $this->record_per_page = Config::get('app.record_per_page');
    }

    protected $faq;

    /*
     * Dashboard
     * */

    public function index(Faq $faq, Request $request)
    {        
        $page_title  = 'Faq';
        $page_action = 'Faq';
        

        if ($request->ajax()) {
            $id           = $request->get('id');
            $status       = $request->get('status');
            $faq         = Faq::find($id);
            $s            = ($status == 1) ? $status=0:$status=1;
            $faq->status = $s;
            $faq->save();
            echo $s;

            exit();
        }
        
        $faqs = Faq::orderBy('id', 'desc')->where('status', '1')->where('is_deleted', '0')->Paginate($this->record_per_page);
        //print_r($faqs); die;        

        $js_file = ['common.js','bootbox.js','formValidate.js'];

        return view('admin::faqs.faq.index', compact('js_file', 'status', 'faqs', 'page_title', 'page_action'));
    }


    public function anyData()
    {
        return Datatables::of(Credit::query())->make(true);
    }

    public function test_table(Credit $credit, Request $request)
    {        
        $page_title  = 'Admin Credit';
        $page_action = 'Admin Credit';
        

        if ($request->ajax()) {
            $id           = $request->get('id');
            $status       = $request->get('status');
            $credit         = Credit::find($id);
            $s            = ($status == 1) ? $status=0:$status=1;
            $credit->status = $s;
            $credit->save();
            echo $s;

            exit();
        }
        // Search by name ,email and group
        $search    = Input::get('search');
        $status    = Input::get('status');
        $role_type = Input::get('role_type');

        if ((isset($search) && !empty($search)) or  (isset($status) && !empty($status)) or !empty($role_type)) {
            $search = isset($search) ? Input::get('search') : '';

            $credits = Credit::where(function ($query) use ($search,$status,$role_type) {
                if (!empty($search)) {
                    $query->Where('first_name', 'LIKE', "%$search%")
                        ->OrWhere('last_name', 'LIKE', "%$search%")
                        ->OrWhere('email', 'LIKE', "%$search%");
                }

                if (!empty($status)) {
                    
                    $status =  ($status == 'active')?1:0;
                    $query->Where('status', $status);
                }

                if ($role_type) {
                    $query->Where('role_type', $role_type);
                }
            })->where('role_type', 3)->Paginate($this->record_per_page);
        } else {
            $credits = Credit::orderBy('id', 'desc')->where('role_type', 3)->where('is_deleted', 0)->Paginate($this->record_per_page);
        }
        $roles = Roles::all();

        $js_file = ['common.js','bootbox.js','formValidate.js'];

        return view('admin::credits.credit.test_table', compact('js_file', 'roles', 'status', 'page_title', 'page_action', 'roles' ,'role_type'));
    }

    /*
     * create Group method
     * */

    public function create(Faq $faq)
    {
        $page_title  = 'Faq';
        $page_action = 'Create Faq';
        
        $js_file     = ['common.js','bootbox.js','formValidate.js'];


        return view('admin::faqs.faq.create', compact('js_file', 'faq', 'page_title', 'page_action'));
    }

    /*
     * Save Group method
     * */

    public function store(FaqRequest $request, Faq $faq)
    {
        //echo "<pre>"; print_r(Input::all()); die;
        $faq->fill(Input::all());
        
        
        $action = $request->get('submit');        
        
        
        $faq->save();
        
        $js_file = ['common.js','bootbox.js','formValidate.js'];

        return Redirect::to(route('faq'))
            ->with('flash_alert_notice', 'New record successfully created.');
    }

    /*
     * Edit Group method
     * @param
     * object : $credit
     * */

    public function edit(Faq $faq)
    {
        //echo "<pre>"; print_r($credit); die;
        $page_title  = 'Edit Faq';
        $page_action = 'Edit Faq';                
        
        $js_file     = ['common.js','bootbox.js','formValidate.js'];
                
        //echo "<pre>"; print_r($credit); die;
        return view('admin::faqs.faq.edit', compact('js_file', 'faq', 'page_title', 'page_action'));
    }

    public function update(Request $request, Faq $faq, FaqRequest $faqrequest)
    {        
        //echo "<pre>"; print_r($credit->password); die;        
        $faq->fill(Input::all());
        $action          = $request->get('submit');
        $faq->save();
                
        if ($request->get('role') == 3) {
            $Redirect = 'faq';
        } else {
            $Redirect = 'faq';
        }

        return Redirect::to(route($Redirect))
            ->with('flash_alert_notice', 'Record successfully updated.');
    }
    /*
     *Delete Credit
     * @param ID
     *
     */
    public function destroy(Request $request, $faq)
    { //print_r($credit);
        //echo $credit->id;die;
        $faq->is_deleted = 1;
        $faq->save();
        //$credit->delete();
        return Redirect::to(route('faq'))
            ->with('flash_alert_notice', 'Record  successfully deleted.');
    }

    public function show(Credit $credit)
    {
    }
    
    public function image_delete(Credit $credit, Request $request, Gallery_image $gallery_image)
    {
        //echo $request->ajax('id');        
            $id           = $request->get('id');            
            $status       = $request->get('is_deleted');
            $credit         = Gallery_image::find($id);
            $s            = ($status == 1) ? $status=0:$status=1;
            $credit->is_deleted = $s;
            $credit->save();
            echo $s;

            exit();        
    }
    
    public function delete_image(Credit $credit, Request $request, Gallery_image $gallery_image)
    {
        //echo $request->ajax('id');        
            $id           = $request->get('id');
            $creditid           = $request->get('creditid');
            DB::table('gallery_images')->where('id', '=', $id)->delete();
            return Redirect::to(url('admin/credit/'.$creditid.'/edit'))
                ->with('flash_alert_notice', 'Image successfully deleted.');
            exit();        
    }
    
    public function search1(Credit $credit, Request $request)
    {
        //echo $request->ajax('id');        
            $country      = $request->get('country');
            $age      = $request->get('age');
            $gender      = $request->get('gender');
            $status      = $request->get('status');
            
            
            $credits = Credit::where(function ($query) use ($country,$age,$gender,$status) {
                if (!empty($country)) {
                    $query->Where('country', 'LIKE', "%$country%");                        
                }

                if (!empty($age)) {
                    $age = explode('_',$age);
                    $query->Where('age', '>=', $age[0]);
                    $query->Where('age', '<=', $age[1]);
                }

                if ($gender) {
                    $query->Where('gender', $gender);
                }
                
                if (!empty($status)) {
                    $status = ($status == 'yes') ? '1' : '0';
                    $query->Where('status', $status);
                }
            })->where('role_type', 3)->Paginate(18);
            
            //echo "<pre>"; print_r($credits); die;            
            $roles = Roles::all();

        $js_file = [];

        return view('admin::credits.credit.index', compact('js_file', 'roles', 'country', 'credits', 'page_title', 'page_action', 'roles', 'role_type')); 
    }
    
    public function search(Credit $credit, Request $request)
    {      $last_part = $request->get('last_part'); 
        //echo $request->ajax('id');        
            $country      = $request->get('country');
            $age      = $request->get('age');
            $gender      = $request->get('gender');
            $status      = $request->get('status');
            $relationship      = $request->get('relationship');
            $photo      = $request->get('photo');
            $appearance      = $request->get('appearance');
            $profile_type      = $request->get('profile_type');
            $adult_level      = $request->get('adult_level');
            
            //DB::enableQueryLog();
            
            $reslt = DB::table('credits')
                    ->join('credit_details', 'credits.id', '=', 'credit_details.credit_id')            
                    ->select('credits.*', 'credit_details.relationship');
            
            if (!empty($country)) {
                $reslt->Where('credits.country', 'LIKE', "%$country%");                        
            }
                
            if (!empty($age)) {
                $age = explode('_',$age);
                $reslt->Where('credits.age', '>=', $age[0]);
                $reslt->Where('credits.age', '<=', $age[1]);
            }
            
            if ($gender) {
                $reslt->Where('credits.gender', $gender);
            }
            
            if ($profile_type) {
                $reslt->Where('credits.profile_type', $profile_type);
            }
            
            if ($adult_level) {
                $reslt->Where('credits.adult_level', $adult_level);
            }
            
            if ($appearance) {
                $reslt->Where('credits.appearance', $appearance);
            }
            
            if (!empty($status)) {
                $status = ($status == 'yes') ? '1' : '0';
                $reslt->Where('credits.status', $status);
            }
            
            if (isset($photo)) {
                if($photo == '1'){
                    $reslt->Where('credits.profile_image', '!=',  '');
                }
                if($photo == '0'){
                    $reslt->Where('credits.profile_image','');
                    //$reslt->or_Where('credits.profile_image', '=',  'default.png');
                }
            }
            
            if ($relationship) {
                $reslt->Where('credit_details.relationship', $relationship);
            }

            if($last_part == 'deleted') {
                $page_title  = 'Player';
                $page_action = 'Deleted Players';
                $heading = 'Deleted Players';
                $reslt->Where('credits.is_deleted', '=',  '1');
            }
            else if($last_part == 'real') {
                $page_title  = 'Player';
                $page_action = 'Real Players';
                $heading = 'Real Players';
                $reslt->Where('credits.fake', '=',  '0');
                $reslt->Where('credits.is_deleted', '=',  '0');
            }
            else if($last_part == 'fake') {
                $page_title  = 'Player';
                $page_action = 'Fake Players';
                $heading = 'Fake Players';
                $reslt->Where('credits.fake', '=',  '1');
                $reslt->Where('credits.is_deleted', '=',  '0');
            }
            else if($last_part == 'unconfirmed') {
                $page_title  = 'Player';
                $page_action = 'Unconfirmed Players';
                $heading = 'Unconfirmed Players';
                $reslt->Where('credits.status', '=',  '0');
                $reslt->Where('credits.is_deleted', '=',  '0');
            }
                
            $credits = $reslt->where('credits.role_type', 3)->Paginate(30);
            
            //echo "<pre>"; print_r($credits); die;            
            $roles = Roles::all();

        $js_file = [];

        $search_slug = 'list-search';

        return view('admin::credits.credit.index', compact('js_file', 'roles', 'country', 'credits', 'page_title', 'page_action','heading',  'roles', 'role_type', 'search_slug')); 
    }
    
    public function setProfileImage(Credit $credit, Request $request, Gallery_image $gallery_image)
    {        
            $id           = $request->get('id');
            $creditid           = $request->get('creditid');
            
            $credit = DB::table('credits')->where('id', $creditid)->first();            
            $old_image = $credit->profile_image;
            
            $gallery = DB::table('gallery_images')->where('id', $id)->first();            
            $new_image = $gallery->gallery_image;
            
            DB::table('credits')
            ->where('id', $creditid)
            ->update(array('profile_image' => $new_image));
            
            DB::table('gallery_images')
            ->where('id', $id)
            ->update(array('gallery_image' => $old_image));
                        
            return Redirect::to(url('admin/credit/'.$creditid.'/edit'))
                ->with('flash_alert_notice', 'Image Set as Profile successfully ');
            exit();        
    }


    public function deleted(Credit $credit, Request $request)
    {        
        $page_title  = 'Player';
        $page_action = 'Deleted Players';
        $heading = 'Deleted Players';
        

        if ($request->ajax()) {
            $id           = $request->get('id');
            $status       = $request->get('status');
            $credit         = Credit::find($id);
            $s            = ($status == 1) ? $status=0:$status=1;
            $credit->status = $s;
            $credit->save();
            echo $s;

            exit();
        }
        // Search by name ,email and group
        $search    = Input::get('search');
        $status    = Input::get('status');
        $role_type = Input::get('role_type');

        if ((isset($search) && !empty($search)) or  (isset($status) && !empty($status)) or !empty($role_type)) {
            $search = isset($search) ? Input::get('search') : '';

            $credits = Credit::where(function ($query) use ($search,$status,$role_type) {
                if (!empty($search)) {
                    $query->Where('first_name', 'LIKE', "%$search%")
                        ->OrWhere('last_name', 'LIKE', "%$search%")
                        ->OrWhere('email', 'LIKE', "%$search%");
                }

                if (!empty($status)) {
                    
                    $status =  ($status == 'active')?1:0;
                    $query->Where('status', $status);
                }

                if ($role_type) {
                    $query->Where('role_type', $role_type);
                }
            })->where('role_type', 3)->Paginate($this->record_per_page);
        } else {
            $credits = Credit::orderBy('id', 'desc')->where('role_type', 3)->where('is_deleted', 1)->Paginate($this->record_per_page);
        }
        $roles = Roles::all();

        $js_file = ['common.js','bootbox.js','formValidate.js'];

        return view('admin::credits.credit.index', compact('js_file', 'roles', 'status', 'credits', 'page_title', 'page_action','heading', 'roles', 'role_type'));
    }

    public function fake(Credit $credit, Request $request)
    {        
        $page_title  = 'Player';
        $page_action = 'Deleted Players';
        $heading = 'Fake Players';
        

        if ($request->ajax()) {
            $id           = $request->get('id');
            $status       = $request->get('status');
            $credit         = Credit::find($id);
            $s            = ($status == 1) ? $status=0:$status=1;
            $credit->status = $s;
            $credit->save();
            echo $s;

            exit();
        }
        // Search by name ,email and group
        $search    = Input::get('search');
        $status    = Input::get('status');
        $role_type = Input::get('role_type');

        if ((isset($search) && !empty($search)) or  (isset($status) && !empty($status)) or !empty($role_type)) {
            $search = isset($search) ? Input::get('search') : '';

            $credits = Credit::where(function ($query) use ($search,$status,$role_type) {
                if (!empty($search)) {
                    $query->Where('first_name', 'LIKE', "%$search%")
                        ->OrWhere('last_name', 'LIKE', "%$search%")
                        ->OrWhere('email', 'LIKE', "%$search%");
                }

                if (!empty($status)) {
                    
                    $status =  ($status == 'active')?1:0;
                    $query->Where('status', $status);
                }

                if ($role_type) {
                    $query->Where('role_type', $role_type);
                }
            })->where('role_type', 3)->Paginate($this->record_per_page);
        } else {
            $credits = Credit::orderBy('id', 'desc')->where('role_type', 3)->where('fake', 1)->where('is_deleted', 0)->Paginate($this->record_per_page);
        }
        $roles = Roles::all();

        $js_file = ['common.js','bootbox.js','formValidate.js'];

        return view('admin::credits.credit.index', compact('js_file', 'roles', 'status', 'credits', 'page_title', 'page_action','heading', 'roles', 'role_type'));
    }

    public function real(Credit $credit, Request $request)
    {        
        $page_title  = 'Player';
        $page_action = 'Real Players';
        $heading = 'Real Players';
        

        if ($request->ajax()) {
            $id           = $request->get('id');
            $status       = $request->get('status');
            $credit         = Credit::find($id);
            $s            = ($status == 1) ? $status=0:$status=1;
            $credit->status = $s;
            $credit->save();
            echo $s;

            exit();
        }
        // Search by name ,email and group
        $search    = Input::get('search');
        $status    = Input::get('status');
        $role_type = Input::get('role_type');

        if ((isset($search) && !empty($search)) or  (isset($status) && !empty($status)) or !empty($role_type)) {
            $search = isset($search) ? Input::get('search') : '';

            $credits = Credit::where(function ($query) use ($search,$status,$role_type) {
                if (!empty($search)) {
                    $query->Where('first_name', 'LIKE', "%$search%")
                        ->OrWhere('last_name', 'LIKE', "%$search%")
                        ->OrWhere('email', 'LIKE', "%$search%");
                }

                if (!empty($status)) {
                    
                    $status =  ($status == 'active')?1:0;
                    $query->Where('status', $status);
                }

                if ($role_type) {
                    $query->Where('role_type', $role_type);
                }
            })->where('role_type', 3)->Paginate($this->record_per_page);
        } else {
            $credits = Credit::orderBy('id', 'desc')->where('role_type', 3)->where('fake', 0)->where('is_deleted', 0)->Paginate($this->record_per_page);
        }
        $roles = Roles::all();

        $js_file = ['common.js','bootbox.js','formValidate.js'];

        return view('admin::credits.credit.index', compact('js_file', 'roles', 'status', 'credits', 'page_title', 'page_action','heading', 'roles', 'role_type'));
    }

    public function unconfirmed(Credit $credit, Request $request)
    {        
        $page_title  = 'Player';
        $page_action = 'Unconfirmed Players';
        $heading = 'Unconfirmed Players';
        

        if ($request->ajax()) {
            $id           = $request->get('id');
            $status       = $request->get('status');
            $credit         = Credit::find($id);
            $s            = ($status == 1) ? $status=0:$status=1;
            $credit->status = $s;
            $credit->save();
            echo $s;

            exit();
        }
        // Search by name ,email and group
        $search    = Input::get('search');
        $status    = Input::get('status');
        $role_type = Input::get('role_type');

        if ((isset($search) && !empty($search)) or  (isset($status) && !empty($status)) or !empty($role_type)) {
            $search = isset($search) ? Input::get('search') : '';

            $credits = Credit::where(function ($query) use ($search,$status,$role_type) {
                if (!empty($search)) {
                    $query->Where('first_name', 'LIKE', "%$search%")
                        ->OrWhere('last_name', 'LIKE', "%$search%")
                        ->OrWhere('email', 'LIKE', "%$search%");
                }

                if (!empty($status)) {
                    
                    $status =  ($status == 'active')?1:0;
                    $query->Where('status', $status);
                }

                if ($role_type) {
                    $query->Where('role_type', $role_type);
                }
            })->where('role_type', 3)->Paginate($this->record_per_page);
        } else {
            $credits = Credit::orderBy('id', 'desc')->where('role_type', 3)->where('status', 0)->where('is_deleted', 0)->Paginate($this->record_per_page);
        }
        $roles = Roles::all();

        $js_file = ['common.js','bootbox.js','formValidate.js'];

        return view('admin::credits.credit.index', compact('js_file', 'roles', 'status', 'credits', 'page_title', 'page_action','heading', 'roles', 'role_type'));
    }
    
    
}
