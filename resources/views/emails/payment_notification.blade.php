<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="image/jpeg; charset=utf-8" />
<title>Your Payment Notification</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
<link href="https://fonts.googleapis.com/css?family=Arizonia|Cookie|Montserrat:100,200,300,400,500,600,700,800,900|Poppins:100,200,300,400,500,600,700,800,900" rel="stylesheet"> 
<link href="https://fonts.googleapis.com/css?family=Oswald" rel="stylesheet">   
<style type="text/css">
  body{font-family: 'Montserrat', sans-serif; margin:0; font-size:19px;}
  p{ line-height:34px; color:#888}
  p span{color:#000;}
  table{ width:100%;}
  h6{ margin:15px 0}
</style>
</head>

<body>
<div style="width:700px;margin:0 auto;background:url({{ url('public/images/bg1.jpg') }}) no-repeat; background-size:cover; padding:50px ">
<div style="width:90%; background:#fff; padding:5%; position:relative">
<table>
  <tr><td>
      <h4 style=" text-align:center; font-weight:300; font-size:25px; margin:0 0 15px;">You have received a  payment notification </h4>
      <h1 style="text-align:center; font-family: 'Arizonia', cursive; font-size:80px; color:#EC155A; margin:0; text-transform:capitalize;"><img src="{{ url('public/images/logo.png') }}"> </h1>
        <p>Dear {{$content['first_name']??'User'}},</p>
        <p>Thank you for payment.</p>
        <p>Your payment status was {{$content['payment_status']}}</p>
        
        
        <p>Regards</p>
        <p>Team <a href="{{ url('/') }}">Flirtfull.com</a></p>
    </td></tr>
</table>
</div>
</div>

      
        

</body>
</html>
