<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use Validator;
use Session;
use Hash;
use Auth;
use Illuminate\Support\Facades\Input;
use App\Models\Profile;
use Excel;
use App\Models\Search;
use Carbon\Carbon;
use DateTime;
use Helper;
use Image;
use Kreait\Firebase;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Kreait\Firebase\Database;
 
class InboxController extends Controller
{
    /**    
     * Create a new controller instance.
     *
     * @return void
     */
    protected $search;

    public function __construct(Request $request,Search $search, Profile $profile)
    {
        error_reporting(env('ERROR_SHOW'));
        ini_set('display_errors', env('ERROR_SHOW'));
        $this->middleware('web');
        $this->search = $search;
        $this->profile = $profile;
        date_default_timezone_set('Asia/Calcutta');        
        
        if(Auth::check()){           
            DB::table('users')->where("id",Auth::user()->id)->update(array("is_login"=>"1","login_time"=>time()));
        }
    }            
    
    public function index()
    {
        $user_id = Auth::id();
        if(!Auth::check())
        {
            return redirect('search');
        }        

        $users = DB::table('users')->select('users.*')->where('users.id', '=',Auth::id())->first();
        $user_chats = $this->get_user_chat($users->id);
        
        if($user_id == 4890){
            //echo "<pre>"; print_r($user_chats); die;
        }
        $receive = array();
        $send = array();
        $all = array();
        if(!empty($user_chats)){
                foreach ($user_chats as $key => $value) {
                    $chatTabType = "all";
                    $readStatus = '<p class="read_msg"></p>';
                    $unreadClass = '';
                    $icon = "";
                   if($value['messageType'] == 'favorite'){
                        $icon = 'fa fa-star-o';
                    }else if($value['messageType'] == 'likes'){
                        $icon = 'fa fa-heart-o';
                    }else if($value['messageType'] == 'file'){

                        
                        if($value['message']){
                                                        
                            if(strpos($value['message'],'firebasestorage') != false){                                   
                              $value['message'] = '<img src="'.$value['message'].'" width="80" height="80">';
                            }else if(strpos($value['message'],'<img') != false){
                              $value['message'] = $value['message'];
                            }else{
                              $value['message'] = '<img src="https://moneoangels.com/public/images/profile_image/'.$value['message'].'" width="80" height="80">';
                            }                                
                            
                            // if(strpos($value['message'],'firebasestorage') != -1){                            
                            //   $value['message'] = '<img src="'+$value['message']+'" width="80" height="80">';
                            // }else if(strpos($value['message'],'<img') != -1){
                            //   $value['message'] = $value['message'];
                            // }else{
                            //   $value['message'] = '<img src="https://moneoangels.com/public/images/profile_image/'+$value['message']+'" width="80" height="80">';
                            // }


                        }

                        
                    }else if($value['messageType'] == 'text'){
                        
                    }else if($value['messageType'] == 'proposal'){
                        
                    }

                    if($value['historyType'] == 1){
                        if($value['lastSenderId'] == $users->id){
                          $chatTabType = 'send';
                        }else{
                          $chatTabType = 'receive';
                        }
                    }

                    if($value['historyLoad'] == '0'){
                        $readStatus = '<i class="fa fa-circle"></i>';
                        $unreadClass = "unreadClass";
                    }
                                 
                    if($chatTabType == 'send'){
                        $readStatus = '';
                    }

                    if($value['name']){

                      $str1 = strtolower($value['name']);
                      $str2 = "";
                      $str3 = ($value['message']) ? strtolower($value['message']) : '';
                     
                      if(strpos($value['profilePic'],'crop_') != -1){
                        $imgurll = env('PROFILE_MANAGER_ADDR').('/public/thumbnail/');
                      }else{
                        $imgurll = asset('/public/thumbnail').'/';
                      }
                      
                      if($value['message']){
                            if(strpos($str1,$str2) != -1 || strpos($str3,$str2) != -1){
                                $value['chatTabType'] = $chatTabType;
                                $value['unreadClass'] = $unreadClass;
                                $value['readStatus'] = $readStatus;
                                $value['imgurll'] = $imgurll;
                                $value['icon'] = $icon;
                                $value['message'] = $this->limit_words($value['message']);
                                if($chatTabType == 'receive'){
                                    array_push($receive, $value);
                                }else if($chatTabType == 'send'){
                                    array_push($send, $value);
                                }else if($chatTabType == 'all'){
                                	array_push($receive, $value);
                                    array_push($all, $value);
                                }
                                /*$chat_one = "";

                                $chat_one = '<tr id="class'.$value['uid'].'" class="chating_list '.$chatTabType.' '.$unreadClass.' ">'.            
                                    '<td class="chat_thumb">'.
                                      '<a href="https://thebuzztok.net/inbox-chat/'.$value['uid'].'">'.
                                        '<span id="status'.$value['uid'].'" class="unread_msg">'.$readStatus.'</span></div>'.
                                        '<div class="inbox_img">'.
                                            '<img src="'.$imgurll.$value['profilePic'].'" alt="">'.
                                        '</div>'.
                                      '</a>'.
                                    '</td>'.
                                    '<td class="chat_name_wrap">'.
                                       '<a href="https://thebuzztok.net/inbox-chat/'.$value['uid'].'">'.
                                        '<div class="chat_name">'.
                                            '<div class="inbox-name">'.$value['name'].'</div>'.
                                            '<div class="chat_time">'.
                                                '<div  class="inbox-date">'.
                                                          '<h5 id="date'.$value['uid'].'" >'.$value['timestamp'].'<h5>'.
                                                '<span class="inbox_icons like">'.
                                                    '<i class="'.$icon.'"></i>'.
                                                '</span>'.                        
                                            '</div>'.
                                        '</div>'.
                                        '<div id="msg'.$value['uid'].'" class="chat_msg">'.(($value['message'])? $value['message']:"").'</div>'.
                                       '</a>'.
                                    '</td>'.            
                                '</tr>';*/

                            }
                            //$chats = $chats.$chat_one;
                        }
                }
                
            }
        }

        // Sort inbox
        foreach ($receive as $key => $node) {
		   $timestamps_r[$key]    = $node['timestamp'];
		}
		array_multisort($timestamps_r, SORT_DESC, $receive);

		// Sort outbox
        foreach ($send as $key => $node) {
		   $timestamps_s[$key]    = $node['timestamp'];
		}
		array_multisort($timestamps_s, SORT_DESC, $send);

		// Sort all
        foreach ($all as $key => $node) {
		   $timestamps_a[$key]    = $node['timestamp'];
		}
		array_multisort($timestamps_a, SORT_DESC, $all);
        
        $per_page = 10;

        return view('inbox',['receive' => $receive,'send' => $send,'all' => $all,'receive_count' => count($receive),'send_count' => count($send),'all_count' => count($all),'user_chats' =>  $user_chats,'chat_count' =>  count($user_chats), 'users' => $users, 'per_page'=>$per_page]);
    }
    
    public function ajax_message($user_id,$term=''){

        $user_chats = $this->get_user_chat($user_id);
        
        $receive = array();
        $send = array();
        $all = array();
        if(!empty($user_chats)){
                foreach ($user_chats as $key => $value) {
                    $chatTabType = "all";
                    $readStatus = '<p class="read_msg"></p>';
                    $unreadClass = '';
                    $icon = "";
                   if($value['messageType'] == 'favorite'){
                        $icon = 'fa fa-star-o';
                    }else if($value['messageType'] == 'likes'){
                        $icon = 'fa fa-heart-o';
                    }else if($value['messageType'] == 'file'){
                        if($value['message']){
                            // if(strpos($value['message'],'firebasestorage') != -1){
                            //   $value['message'] = '<img src="'+$value['message']+'" width="80" height="80">';
                            // }else if(strpos($value['message'],'<img') != -1){
                            //   $value['message'] = $value['message'];
                            // }else{
                            //   $value['message'] = '<img src="https://moneoangels.com/public/images/profile_image/'+$value['message']+'" width="80" height="80">';
                            // }
                            if(strpos($value['message'],'firebasestorage') != false){                                   
                              $value['message'] = '<img src="'.$value['message'].'" width="80" height="80">';
                            }else if(strpos($value['message'],'<img') != false){
                              $value['message'] = $value['message'];
                            }else{
                              $value['message'] = '<img src="https://moneoangels.com/public/images/profile_image/'.$value['message'].'" width="80" height="80">';
                            }
                        }
                    }else if($value['messageType'] == 'text'){
                        
                    }else if($value['messageType'] == 'proposal'){
                        
                    }

                    if($value['historyType'] == 1){
                        if($value['lastSenderId'] == $user_id){
                          $chatTabType = 'send';
                        }else{
                          $chatTabType = 'receive';
                        }
                    }

                    if($value['historyLoad'] == '0'){
                        $readStatus = '<i class="fa fa-circle"></i>';
                        $unreadClass = "unreadClass";
                    }
                                 
                    if($chatTabType == 'send'){
                        $readStatus = '';
                    }

                    if($value['name']){

                      $str1 = strtolower($value['name']);
                      $str2 = "";
                      $str3 = ($value['message']) ? strtolower($value['message']) : '';
                     
                      if(strpos($value['profilePic'],'crop_') != -1){
                        $imgurll = env('PROFILE_MANAGER_ADDR').('/public/thumbnail/');
                      }else{
                        $imgurll = asset('/public/thumbnail').'/';
                      }
                      
                      if($value['message']){
                            if(strpos($str1,$str2) != -1 || strpos($str3,$str2) != -1){
                                $value['chatTabType'] = $chatTabType;
                                $value['unreadClass'] = $unreadClass;
                                $value['readStatus'] = $readStatus;
                                $value['imgurll'] = $imgurll;
                                $value['icon'] = $icon;
                                $value['message'] = $this->limit_words($value['message']);

                                if($term != ''){

                                    if(strpos(strtolower($str1), strtolower($term)) !== false){
                                        if($chatTabType == 'receive'){
                                        array_push($receive, $value);
                                        }else if($chatTabType == 'send'){
                                            array_push($send, $value);
                                        }else if($chatTabType == 'all'){
                                            array_push($receive, $value);
                                            array_push($all, $value);
                                        }
                                    }                                    
                                }else{
                                    if($chatTabType == 'receive'){
                                        array_push($receive, $value);
                                    }else if($chatTabType == 'send'){
                                        array_push($send, $value);
                                    }else if($chatTabType == 'all'){
                                        array_push($receive, $value);
                                        array_push($all, $value);
                                    }
                                }
                                    
                                /*$chat_one = "";

                                $chat_one = '<tr id="class'.$value['uid'].'" class="chating_list '.$chatTabType.' '.$unreadClass.' ">'.            
                                    '<td class="chat_thumb">'.
                                      '<a href="https://thebuzztok.net/inbox-chat/'.$value['uid'].'">'.
                                        '<span id="status'.$value['uid'].'" class="unread_msg">'.$readStatus.'</span></div>'.
                                        '<div class="inbox_img">'.
                                            '<img src="'.$imgurll.$value['profilePic'].'" alt="">'.
                                        '</div>'.
                                      '</a>'.
                                    '</td>'.
                                    '<td class="chat_name_wrap">'.
                                       '<a href="https://thebuzztok.net/inbox-chat/'.$value['uid'].'">'.
                                        '<div class="chat_name">'.
                                            '<div class="inbox-name">'.$value['name'].'</div>'.
                                            '<div class="chat_time">'.
                                                '<div  class="inbox-date">'.
                                                          '<h5 id="date'.$value['uid'].'" >'.$value['timestamp'].'<h5>'.
                                                '<span class="inbox_icons like">'.
                                                    '<i class="'.$icon.'"></i>'.
                                                '</span>'.                        
                                            '</div>'.
                                        '</div>'.
                                        '<div id="msg'.$value['uid'].'" class="chat_msg">'.(($value['message'])? $value['message']:"").'</div>'.
                                       '</a>'.
                                    '</td>'.            
                                '</tr>';*/

                            }
                            //$chats = $chats.$chat_one;
                        }
                }
                
            }
        }

        // Sort inbox
        foreach ($receive as $key => $node) {
           $timestamps_r[$key]    = $node['timestamp'];
        }
        array_multisort($timestamps_r, SORT_DESC, $receive);

        // Sort outbox
        foreach ($send as $key => $node) {
           $timestamps_s[$key]    = $node['timestamp'];
        }
        array_multisort($timestamps_s, SORT_DESC, $send);

        // Sort all
        foreach ($all as $key => $node) {
           $timestamps_a[$key]    = $node['timestamp'];
        }
        array_multisort($timestamps_a, SORT_DESC, $all);

        $data = ['receive' => $receive,'send' => $send,'all' => $all,'receive_count' => count($receive),'send_count' => count($send),'all_count' => count($all),'user_chats' =>  $user_chats,'chat_count' =>  count($user_chats) ];
        
        return json_encode($data);
    }

    public function limit_words($post_content, $word_limit=20)
    {
      $words = explode(" ",$post_content);
      return implode(" ",array_splice($words,0,$word_limit));
    }

    public function get_user_chat($user_id){
        $acc = ServiceAccount::fromJsonFile('/var/www/html/flirtengine-firebase-adminsdk-94hsi-4dac5384f1.json');
        $firebase = (new Factory)->withServiceAccount($acc)->create();
        $database = $firebase->getDatabase();
        $newPost = $database
        ->getReference('chat_history/'.$user_id)
        ->getSnapshot();
        return $newPost->getValue();
    }
    
    public function chat($uid)
    {
        $user_id = Auth::id();
        if(!Auth::check())
        {
            return redirect('search');
        }        
        $users = DB::table('users')->select('users.*')->where('users.id', '=',Auth::id())->first();        
        $highliteuser = DB::table('users as u')->join('user_details as ud','u.id','=','ud.user_id')->select('u.*','ud.*')->where('u.id', '=',$uid)->first();        
        $highlite = ["id"=>$uid,"name"=>$highliteuser->name,"profile_image"=>$highliteuser->profile_image,"fake"=>$highliteuser->fake];
        
        return view('inbox_chat',compact('highlite','users','highliteuser'));
    }
    
    
    public function insertFirebase(){
        
        
        $users = DB::table('users')->select('users.*')->get();
        echo count($users);
        //foreach($users as $row){                
        //        $data = [
        //                 "username"=>"Moneo",
        //                 "password"=>"Moneo2019@",
        //                 "service_code"=>"cd9271fa",
        //                 "name"=> $row->name.'__'.$row->id,
        //                 "group"=> "subscriber",
        //                 "profile"=> [
        //                                "avatar"=> "avatar.jpg",
        //                                "age"=> "23",
        //                                "location"=> "indore",
        //                                "images"=> [$row->profile_image]
        //                             ],
        //                  "additional_info"=> [
        //                                "membership"=> "free",
        //                                "link"=> "http://xyz.com",
        //                                "about"=> "about"
        //                             ],                                            
        //                 ];
        //        $ch = curl_init();
        //        curl_setopt($ch, CURLOPT_URL,"https://im.operatorplatform.com/api/users/add");
        //        curl_setopt($ch, CURLOPT_POST, 1);
        //        curl_setopt($ch, CURLOPT_POSTFIELDS,
        //                    "data=".json_encode($data));
        //        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
        //        
        //        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        //        echo $row->name.'__'.$row->id;
        //        $server_output = curl_exec ($ch);
        //        print_r($server_output);
        //        curl_close ($ch);
        //}
        //print_r($users); die;
                            
        return view('insertFirebasess',compact('users'));
    }
    
    public function registerThirdparty(){
        
        
        $users = DB::table('users')->select('users.*')->where('fake','0')->where('status','1')->get();
        
        foreach($users as $row){                
                $data = [
                         "username"=>"Moneo",
                         "password"=>"Moneo2019@",
                         "service_code"=>"cd9271fa",
                         "name"=> $row->name.'__'.$row->id,
                         "group"=> "subscriber",
                         "profile"=> [
                                        "avatar"=> "avatar.jpg",
                                        "age"=> "23",
                                        "location"=> "indore",
                                        "images"=> [$row->profile_image]
                                     ],
                          "additional_info"=> [
                                        "membership"=> "free",
                                        "link"=> "http://xyz.com",
                                        "about"=> "about"
                                     ],                                            
                         ];
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL,"https://im.operatorplatform.com/api/users/add");
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS,
                            "data=".json_encode($data));
                curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
                
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                echo $row->name.'__'.$row->id;
                $server_output = curl_exec ($ch);
                print_r($server_output);
                curl_close ($ch);
                
                $data1 = [
                         "username"=>"Moneo",
                         "password"=>"Moneo2019@",
                         "service_code"=>"cd9271fa",
                         "name"=> $row->name.'__'.$row->id,
                         "group"=> "persona",
                         "profile"=> [
                                        "avatar"=> "avatar.jpg",
                                        "age"=> "23",
                                        "location"=> "indore",
                                        "images"=> [$row->profile_image]
                                     ],
                          "additional_info"=> [
                                        "membership"=> "free",
                                        "link"=> "http://xyz.com",
                                        "about"=> "about"
                                     ],                                            
                         ];
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL,"https://im.operatorplatform.com/api/users/add");
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS,
                            "data=".json_encode($data1));
                curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
                
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                echo $row->name.'__'.$row->id;
                $server_output = curl_exec ($ch);
                print_r($server_output);
                curl_close ($ch);
        }
                                        
    }
    
    public function callBackFunFirebase(Request $request){
        //echo 'Hello this is test';
        //$data = json_encode($request->all());
        //$mdata = '{"data":"{\"username\":\"Moneo\",\"password\":\"Moneo2019@\",\"service_code\":\"cd9271fa\",\"service_name\":\"CL - MNS\",\"from\":\"macto222\",\"to\":\"carol111\",\"message\":\"Hi there! May I get to know your name,  please? I am doing good\",\"attachment\":null,\"additional_info\":{\"operatorId\":\"RKFL4718\"}}"}';
        $mdata = json_encode($_POST);
        //$mdata = '{"data":"{\"username\":\"Moneo\",\"password\":\"Moneo2019@\",\"service_code\":\"cd9271fa\",\"service_name\":\"CL - MNS\",\"from\":\"Sensuous_karol_324\",\"to\":\"carol111\",\"message\":\"Hi! have you been busy? I hope you got a moment to chat with me?\",\"attachment\":null,\"additional_info\":{\"operatorId\":\"LSD745\"}}"}';
        if($mdata){            
            $detail = (json_decode($mdata)->data);
            $detail = json_decode($detail);
            //print_r($detail); die;
            if($detail){
                $udata['from'] = explode('__',$detail->from)[1];
                $udata['to'] = explode('__',$detail->to)[1];
                $udata['message'] = $detail->message;
                $udata['attachment'] = json_encode($detail->attachment);
                $udata['additional_info'] = json_encode($detail->additional_info);
                
                DB::table('users')->where('id',$udata['from'])->update(array('is_login'=>'1'));                
            }
            //print_r($udata); die;
        }
        
        //print_r($detail->from); die;
        $udata['insertdata'] = $mdata;
        
        
        //print_r($udata); die;
        DB::table('thirdparty')->insert(
            $udata
        );
    }
    
    
    public function get_proposal(Request $request){
        //print_r($request->all());
        $proposal_images = DB::table('proposals_images')->select('image')->where('category_id',$request->get('Id'))->where('status',1)->where('is_deleted',0)->get();
        $htm = "";
        foreach($proposal_images as $row){
            $htm .= '<div class="send_pro_img">'.
                '<img src="'.asset("public/images/profile_image/".$row->image).'" class="img-responsive" onclick="setproposal(\''.$row->image.'\',\'diamonds\',1);">'.
            '</div>';
        }
        return $htm;
    }

    public function message($email,$sender_id)
    {
        $email = base64_decode($email);
        $sender_id = base64_decode($sender_id);
        $users = DB::table('users')->select('users.id')->where('users.email', '=',$email)->first(); 

        //print_r($users->id); die;
        if($users->is_deleted == 0){
            Auth::loginUsingId($users->id);    
        }
        
        return redirect('inbox-chat/'.$sender_id);

    }
}