<?php $__env->startSection('content'); ?>

<div class="like_page_wrapper">
	<div class="container">
	   	<div class="like_box_wrapper">
		   	  <div class="like_box_center">
			   	  <div class="like_img_wrapper">
			   	  	  <!-- box start -->
				   	  <div class="like_img_box">
					   	  <div class="like_img">
                            <?php if($user->provider_name == 'google'): ?>
                                <a href="<?php echo e(url('/profile/'.$user->id )); ?>"><img src="<?php echo e($user->profile_image); ?>" alt="" class="img-responsive"></a>
                            <?php else: ?>
                                <?php if($user->fake): ?>
                                    <a href="<?php echo e(url('/profile/'.$user->id )); ?>"><img src="<?php echo e(env('PROFILE_MANAGER_ADDR')); ?>/public/images/profile_image/<?php echo e($user->profile_image); ?>" alt="" class="img-responsive"></a>
                                <?php else: ?>
                                    <a href="<?php echo e(url('/profile/'.$user->id )); ?>"><img src="<?php echo e(asset('public/images/profile_image/'.$user->profile_image)); ?>" alt="" class="img-responsive"></a>
                                <?php endif; ?>                                    
                            <?php endif; ?>    
					   	  </div>
					   	  <div class="like_detail">
					   	  	<h3><a href="<?php echo e(url('/profile/'.$user->id )); ?>"><?php echo e($user->name); ?></a></h3>
					   	  	<span class="like_age"><?php echo e($user->age); ?></span>
					   	  	<span class="like_desig"><i class="fa fa-id-badge"></i><?php echo e($user->state); ?></span>
					   	  </div>
					   	  <!-- like dislike buttons -->
					   	  <div class="like_dis_btns">
					   	  	  <?php if(Auth::check()): ?>    
                    <div class="btns i_dis_btn" onclick="is_matched('<?php echo e(Auth::id()); ?>','<?php echo e($user->id); ?>','0');">
                      <i class="fa fa-thumbs-down" aria-hidden="true"></i>
                      <span  id="not-hot-link">Not</span>
                    </div>
                    <?php endif; ?>

                  <?php if(Auth::check()): ?>
					   	  	  <a onclick="is_matched('<?php echo e(Auth::id()); ?>','<?php echo e($user->id); ?>','1');" id="hot-link" href="#" data-toggle="modal" data-target="#like_popupp" class="btns i_like_btn">
					   	  	  	<i class="fa fa-heart"></i> 
					   	  		 <span>Hot</span>
                    <?php endif; ?>    
					   	  	</a>
					   	  </div>
					   	  <!-- like dislike buttons -->
				   	  </div>
				   	  <!-- box End -->
			   	  </div>
		   	  </div>
		</div>
	</div>
</div>

<!-- Like popup Start -->
<div id="like_popup" class="modal fade" role="dialog">  
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <div class="like_popup_box">
        	<div class="like_popup_img_wrap">
            <div class="like_popup_img first_img">
          		<img src="<?php echo e(asset('public/images/profile_image/'.Auth::user()->profile_image)); ?>" alt="" class="img-responsive">
          	</div>
            <div class="like_popup_img second_img">
            <?php if($user->provider_name == 'google'): ?>
                <img id="match_img" src="<?php echo e($user->profile_image); ?>" alt="" class="img-responsive">
            <?php else: ?>
                <?php if($user->fake): ?>
                    <img id="match_img" src="<?php echo e(env('PROFILE_MANAGER_ADDR')); ?>/public/images/profile_image/<?php echo e($user->profile_image); ?>" alt="" class="img-responsive">
                <?php else: ?>
                    <img id="match_img" src="<?php echo e(asset('public/images/profile_image/'.$user->profile_image)); ?>" alt="" class="img-responsive">          
                <?php endif; ?>
            <?php endif; ?>    
            </div>
            <span class="img_icon"><i class="fa fa-heart"></i></span>
          </div>
        	<div class="like_popup_text">
        		<h3>it's a match!</h3>
        		<p id="matched">You and <?php echo e($user->name); ?> liked each other!</p>
        	</div>
        	<div class="like_form">
        		<!--<form id="like_message" method="post">-->
                    <div id="successdiv" style="display:none; color:red;">Your Message has been sent successfully.</div>
                    <div id="faileddiv" style="display:none; color:red;">You haven't enough credit to send this message. Please purchase credits.</div>    
        			<div class="form_group">
	        			<textarea name="message" id="message" placeholder="write a message"></textarea>
                        <input type="hidden" name="receiver_id" id="matched_receiver_id" value="<?php echo e($user->id); ?>">
                        <input type="hidden" name="receiver_name" id="matched_receiver_name" value="<?php echo e($user->name); ?>">
                        <input type="hidden" name="receiver_image" id="matched_receiver_image" value="<?php echo e($user->profile_image); ?>">
	        		</div>
	        		<div class="form_group">
	        			<button class="lk_send_btn" onclick="send_message();">Send</button>
	        		</div>
        		<!--</form>-->
        		<div class="keep_text">
        			<span class="keep_btn">Keep Playing</span>
        		</div>
        	</div>
        </div>
	   </div>
    </div>
  </div>
</div>
<!-- Like popup End -->
<input type="hidden"  name="sender_id" id="sender_id" value="<?php echo e(isset(Auth::user()->id) ? Auth::user()->id : ''); ?>">
<input type="hidden"  name="sender_name" id="sender_name" value="<?php echo e(isset(Auth::user()->name) ? Auth::user()->name : ''); ?>">
<input type="hidden"  name="sender_image" id="sender_image" value="<?php echo e(isset(Auth::user()->profile_image) ? Auth::user()->profile_image : ''); ?>">

<input type="hidden"  name="receiver_id" id="receiver_id" value="<?php echo e(isset($user->id) ? $user->id : ''); ?>">
<input type="hidden"  name="receiver_name" id="receiver_name" value="<?php echo e(isset($user->name) ? $user->name : ''); ?>">
<input type="hidden"  name="receiver_image" id="receiver_image" value="<?php echo e(isset($user->profile_image) ? $user->profile_image : ''); ?>">
<input type="hidden"  name="room_id" id="room_id" value="">
<input type="hidden"  name="chat_image" id="chat_image" value="">

<input type="hidden"  name="chat_send_image" id="chat_send_image" value="">
<input type="hidden" id="free_message" value="<?php echo e(($userwallet) ? $userwallet->free_message : '0'); ?>">
<script>
	//tab menu css
	$(document).ready(function(){
	  //tab menu js script
	  $('.keep_btn').on('click',function(){
	    $('#like_popup').modal('hide');
	  });
	});
</script>
<script>

var adminId = $('#sender_id').val();
     function sendMsg1(proposal=null){                
            
            var mycredits = jQuery("#initialcredit").val();//localStorage.getItem("credits");
            var mypremiums = jQuery("#initialpremium").val();
            if(mycredits == 'null'){ mycredits = 0;}
            var requiredCredits = 0;
            
            // for text required credit will be 1
            var type = (jQuery("#file").val() == '') ? 'text' : 'file';
            if(type === "text"){
                requiredCredits = 1;
            }
            
            var planType = jQuery("#planType").val();
            if(planType === 'credits'){ requiredCredits = jQuery("#points").val(); }
            
            if(planType === 'diamonds'){
                    requiredCredits = 1;//jQuery("#points").val();
            }
            
            //chatNowtext chatNowuserId
            if(proposal){
                var msgText1 = '<img src="'+proposal+'" width="100">';
                var planType = 'diamonds';
                if(proposal == 'likes'){
                    planType = '';
                }
                if(proposal == 'favorites'){
                    planType = '';
                }
            }else{
                var msgText1 = $.trim($('#message').val());
            }
            //alert(msgText1);
                if(msgText1 == ''){
                    return true;
                }

            
            
            
    
        localStorage.setItem("chatTab", "Outbox");
    
    
    
        var chat_send_image = $('#chat_send_image').val();
        var messageType = 'text';
        if(chat_send_image != ''){
            messageType = 'text';
           // CKEDITOR.instances.message.setData(chat_send_image);
        }
    
        var userId    = $('#matched_receiver_id').val();
        var userName  = $('#matched_receiver_name').val();
        var userImage = $('#matched_receiver_image').val();
        
        if(proposal){
            var msgText = '<img src="'+proposal+'">';
            if(proposal === 'likes'){                
                var msgText = 'Hi '+userName+', i like your profile. Check mine ans see if we are a match! Lets connect';
            }
            if(proposal === 'favorites'){            
                var msgText = 'Hi '+userName+' I have added you to my favorite list. I hope you like my profile to! Lets get this rolling';
            }
            var free_message = 0;
        }else{
            var msgText = $('#message').val();
            requiredCredits = 1;                        
            //alert(msgText);
            if($('#free_message').val() > 0) { 
                requiredCredits = 0;
                var free_message = 1;
            }else{
                var free_message = 0;
            }
        }
        
        
        //alert(msgText);
       
        
        $('#message').val('');
        
        
        
        var room_id = $('#room_id').val();
        if(room_id != ''){
            firebase.database().ref('chat_rooms/' + room_id).off("child_added");
        }
        
        var uid = userId; //$('#chatNowuserId').val();
        var chatRoom = adminId + '_' + uid;
        if(uid < adminId){
          chatRoom = uid + '_' + adminId;
        }
        
        
        firebase.database().ref('chat_rooms/' + chatRoom).off("child_added");
        
        $('#room_id').val(chatRoom);
        
        
        ////////////*******************888
         //if(planType === 'diamonds'){
            //requiredCredits = 1;
            //    $.ajax({
            //        headers: {
            //          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            //        },
            //        url: '<?php echo url("/")?>/update_diamonds',
            //        type: 'POST',
            //        cache : false,
            //        processData: false,
            //        data: 'room_id='+jQuery('#room_id').val()+'&amt=1&receiver_id='+userId, // serializes the form's elements.
            //        success: function(data)
            //        {
            //         console.log(data);
            //         if(data){
            //            var d = JSON.parse(data);
            //            if(d.credits){
            //                jQuery("#initialcredit").val(d.credits);
            //                jQuery("#updatedwallet").html(d.credits);
            //                jQuery("#updateddiamonds").html(d.diamonds);   
            //            }else{                            
            //                jQuery("#updateddiamonds").html(d.diamonds);   
            //            }                        
            //          }                      
            //        }
            //    });
            //}else{
                $.ajax({
                    headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '<?php echo url("/")?>/update_conversation',
                    type: 'POST',
                    cache : false,
                    processData: false,
                    data: 'room_id='+jQuery('#room_id').val()+'&amt='+requiredCredits+'&receiver_id='+userId+'&free_message='+free_message, // serializes the form's elements.
                    success: function(data)
                    {
                     console.log(data);
                      if(data){
                        jQuery("#initialcredit").val(data);
                        jQuery("#updatedwallet").html(data);
                      }
                      if(free_message){
                        $('#free_message').val(0);
                      }
                    }
                });    
          //  }
        //*******************************
    
        var chatRoom = $('#room_id').val();
        $('.alreadyChat').hide();
        firebase.database().ref('/users/'+userId).once('value').then(function(snapshot) {
          var childData = snapshot.val();
          
          firebase.database().ref().child('chat_history/'+adminId).child(userId).once('value').then(function(snapshot1) {
    
            var snapshot1Data = snapshot1.val();
    
            var msgCount = 0;
            var hisType = 1;
            if (snapshot1Data != null){
              var msgCount = Number(snapshot1Data.messageCount)+1;
              if(snapshot1Data.historyType == 1){
                if(snapshot1Data.lastSenderId != adminId){
                  hisType = 2;
                }
              }else{
                hisType = 2;
              }
            }
    
            var insertData = {
              deletteby : '',
              message : msgText,
              messageCount : msgCount,
              name : userName,
              profilePic : userImage,
              timestamp : $.now(),
              uid : userId.toString(),
              historyType : hisType,
              lastSenderId : adminId.toString(),
              messageType : messageType,
              historyLoad : '0'
            };
          if(parseInt(mycredits) >= parseInt(requiredCredits) || (mypremiums >= '<?php echo e(time()); ?>')){
            firebase.database().ref().child('chat_history/'+adminId).child(userId).set(insertData);
          }
          });
        });
    
        firebase.database().ref('/users/'+adminId).once('value').then(function(snapshot) {
          var childData = snapshot.val();
    
          firebase.database().ref().child('chat_history/'+userId).child(adminId).once('value').then(function(snapshot2) {
    
            var snapshot2Data = snapshot2.val();
    
            var msgCount = 0;
            var hisType = 1;
            if (snapshot2Data != null){
              var msgCount = Number(snapshot2Data.messageCount)+1;
              if(snapshot2Data.historyType == 1){
                if(snapshot2Data.lastSenderId != adminId){
                  hisType = 2;
                }
              }else{
                hisType = 2;
              }
            }
    
            var insertData = {
              deletteby : '',
              message : msgText,
              messageCount : msgCount,
              name : childData.userName,
              profilePic : childData.userImage,
              timestamp : $.now(),
              uid : adminId.toString(),
              historyType : hisType,
              lastSenderId : adminId.toString(),
              messageType : messageType,
              historyLoad : '0'
            };
        if(parseInt(mycredits) >= parseInt(requiredCredits) || (mypremiums >= '<?php echo e(time()); ?>')){
              firebase.database().ref().child('chat_history/'+userId).child(adminId).set(insertData);
        }
          });
        });
    
        firebase.database().ref('/users/'+adminId).once('value').then(function(snapshot) {  
          var childData = snapshot.val();
          var insertData = {
            deleteby:'',
            message : msgText,
            messageCount: 1,
            name : childData.userName,
            profilePic : childData.userImage,
            timestamp : $.now(),
            uid : adminId.toString(),
            messageType : messageType,
          };
          //firebase.database().ref().child('chat_rooms/').child(chatRoom).push(insertData);
          if(parseInt(mycredits) >= parseInt(requiredCredits) || (mypremiums >= '<?php echo e(time()); ?>')){
			firebase.database().ref().child('chat_rooms/').child(chatRoom).push(insertData);
                if(proposal){
                    if(proposal != 'likes' && proposal != 'favorites') {
                     $('#proposal_message').show();
                    }
               }else{
                     $('#chat_message').show();
               }
               $('#mypriphoto').modal('hide');
               if(requiredCredits || free_message){
                    $('#successdiv').show();
               }               
		  }else{
            $('#faileddiv').show();
			//message failed
    			$('#messages').append(''+ 
                        '<li class="message">'+                    
                            '<div class="chat-user-right">'+
                            '<div class="chat-user-box">'+
                            '<img src="<?php echo e(asset('/public/thumbnail/')); ?>/'+jQuery('#sender_image').val()+'">'+
                            '<span>'+jQuery('#sender_name').val()+'</span>'+
                            '</div>'+
                            '<div class="chat-user-message">'+
                            '<div class="chat-line"><span>'+msgText+'</span></div>'+                                   
                            '<div class="meta-info"><span class="date">'+$.now()+'</span></div>'+
    						'<div class="meta-info red"><span class="date">Message Sent Failed!!</span></div>'+
                            '</div>'+
                            '</div>'+
                        '</li>'+
                    '');
    			//setTimeout(function(){ jQuery('.credit_btn').click(); }, 3000);	
          $('#msgDiv').animate({
                  scrollTop: $('#messages').prop("scrollHeight")
              }, 0);
          setTimeout(function(){ window.location.replace("<?php echo e(url('/credits')); ?>"); }, 5000); 

		  }
          
                    
        });
               
      }

       function createChatRoom(){
        
        var room_id = $('#room_id').val();
        if(room_id != ''){
            firebase.database().ref('chat_rooms/' + room_id).off("child_added");
        }
        
        var uid = $('#receiver_id').val();
        var chatRoom = adminId + '_' + uid;
        if(uid < adminId){
          chatRoom = uid + '_' + adminId;
        }
        
        
        firebase.database().ref('chat_rooms/' + chatRoom).off("child_added");
        
        $('#room_id').val(chatRoom);
        
        
        <?php if(isset($users) && ($users->fake)){ ?>
            var sendrimgurl = '<?php echo e(env('PROFILE_MANAGER_ADDR').('/public/thumbnail/')); ?>/';
        <?php }else{ ?>
            var sendrimgurl = '<?php echo e(asset('/public/thumbnail/')); ?>/';
        <?php } ?>
        
        firebase.database().ref('chat_rooms/' + chatRoom).on('child_added', function(chat){
            var oneMsgVal = chat.val();
            console.log(oneMsgVal);
            if(oneMsgVal.uid == adminId){
                var liposi = 'right';
            }else{
                var liposi = 'left';
            }
            
            $('#removeLi').remove();
            
            if(oneMsgVal.messageType != 'file'){
                $('#messages').append(''+ 
                    '<li class="message">'+                    
                        '<div class="chat-user-'+liposi+'">'+
                        '<div class="chat-user-box">'+
                        '<img src="'+sendrimgurl+oneMsgVal.profilePic+'">'+
                        '<span>'+oneMsgVal.name+'</span>'+
                        '</div>'+
                        '<div class="chat-user-message">'+
                        '<div class="chat-line"><span>'+oneMsgVal.message+'</span></div>'+                                   
                        '<div class="meta-info"><span class="date">'+oneMsgVal.timestamp+'</span></div>'+
                        '</div>'+
                        '</div>'+
                    '</li>'+
                '');
            }else{
                $('#messages').append(''+
                    '<li class="message">'+                    
                    '<div class="chat-user-'+liposi+'">'+
                    '<div class="chat-user-box">'+
                    '<img src="'+sendrimgurl+oneMsgVal.profilePic+'">'+
                    '<span>'+oneMsgVal.name+'</span>'+
                    '</div>'+
                    '<div class="chat-user-message">'+
                    '<div class="chat-line"><span>'+
                    '<img src=' + oneMsgVal.message + ' alt="Ovengo..." height="100" width="100">'+
                    '</span></div>'+                                   
                    '<div class="meta-info"><span class="date">'+oneMsgVal.timestamp+'</span></div>'+
                    '</div>'+
                    '</div>'+
                '</li>'+
                '');
            }
            
            var msgDiv = document.getElementById('msgDiv');
        msgDiv.scrollTop = msgDiv.scrollHeight;
            
            //scrollToBottom();
        });
    }

    function is_matched(sender_id,receiver_id,type){
        if(type == 1){
         sendMsg1('likes');   
        }        
        //alert(sender_id);
        //alert(receiver_id);
        $('#hot-link').attr('onclick','return false');
        $('#not-hot-link').attr('onclick','return false');        
        $.ajax({
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '<?php echo url("/");?>/likes',
            type: 'POST',
            data: 'receiver_id='+receiver_id+'&sender_id='+sender_id+'&type='+type,
            cache : false,
            dataType: 'json',
            async: false,
            processData: false,
            success: function(data)
            {
              console.log(data);  
              console.log(data[0]);  
              console.log(data[1].name);
              
              if(data[0] == 1){
                $('#successdiv').hide();
                $('#faileddiv').hide();
                $('#like_popup').modal('show');
              }else{
                if(data[1].provider_name == 'google'){
                    $('#match_img').attr('src',data[1].profile_image);       
                }else{
                      if(data[1].fake){
                        var matchimgurl = '<?php echo e(env('PROFILE_MANAGER_ADDR')); ?>/public/images/profile_image/'+data[1].profile_image;
                      }else{
                        var matchimgurl = '<?php echo e(asset('public/images/profile_image/')); ?>/'+data[1].profile_image;
                      }
                    $('#match_img').attr('src',matchimgurl);    
                }                
                $('#matched_receiver_id').val(data[1].id);
                $('#matched_receiver_name').val(data[1].name);
                $('#matched_receiver_image').val(data[1].profile_image);  
              }
              
              $('#matched').html('You and '+$('#matched_receiver_name').val()+' liked each other!');
              
              if(data[1].fake){
                var imgurl = '<?php echo e(env('PROFILE_MANAGER_ADDR')); ?>/public/images/profile_image/'+data[1].profile_image;
              }else{
                var imgurl = '<?php echo e(asset('public/images/profile_image/')); ?>/'+data[1].profile_image;
              }
              var html = '<div class="like_box_center">'+
                            '<div class="like_img_wrapper">'+			   	  	  
                                '<div class="like_img_box">'+
                                  '<div class="like_img">'+
                                    '<a href="<?php echo e(url('/profile/')); ?>/'+data[1].id+'"><img src="'+imgurl+'" alt="" class="img-responsive"></a>'+
                                  '</div>'+
                                  '<div class="like_detail">'+
                                    '<h3><a href="<?php echo e(url('/profile/')); ?>/'+data[1].id+'">'+data[1].name+'</a></h3>'+
                                    '<span class="like_age">'+data[1].age+'</span>'+
                                    '<span class="like_desig"><i class="fa fa-id-badge"></i>'+data[1].state+'</span>'+
                                  '</div>'+                                  
                                  '<div class="like_dis_btns">'+
                                    '<div class="btns i_dis_btn" onclick="is_matched(<?php echo e(Auth::id()); ?>,'+data[1].id+',0)">'+
                                        '<i class="fa fa-thumbs-down" aria-hidden="true"></i>'+
                                        '<span>Not</span>'+                                        
                                    '</div>'+
                                    '<a onclick="is_matched(<?php echo e(Auth::id()); ?>,'+data[1].id+',1)" href="#" id="hot-link" data-toggle="modal" data-target="#like_popupp" class="btns i_like_btn">'+
                                        '<i class="fa fa-heart"></i>'+                                        
                                        '<span>Hot</span>'+                                        
                                    '</a>'+
                                  '</div>'+                                  
                              '</div>'+
                          '</div>'+
                      '</div>';
                $('.like_box_wrapper').html(html);
                
            }
          }); 
    }
    
    
    function send_message(){        
        var receiver_id = $('#matched_receiver_id').val();
        var message = $('#message').val();
        if(message == ''){ return false; }
        //alert(receiver_id);        
        $.ajax({
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '<?php echo url("/");?>/send-like-message',
            type: 'POST',
            data: 'receiver_id='+receiver_id+'&message='+message,
            cache : false,
            dataType: 'json',
            async: false,
            processData: false,
            success: function(data)
            {
              console.log(data);
              sendMsg1();
              //setInterval(function(){ location.reload(); }, 2000);              
              return false;
            }
          });
          
          //$('#like_popup').modal('hide');
          return false;
    }
</script>
   
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout.default', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>