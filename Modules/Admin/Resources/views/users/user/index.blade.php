@extends('admin::layouts.master')
 
    @section('content') 
      @include('admin::partials.navigation')
      @include('admin::partials.breadcrumb')   

       @include('admin::partials.sidebar')  
           <?php 
           $last = basename(request()->path());
           if($last == 'list-search')
           {
            $last_part = $_GET['last_part'];
           }
           else{
            $last_part = $last;
           }
           ?> 
            <div class="advanced-search">
            	<form method="get" action="{{ url('admin/list-search') }}">
                <input type="hidden" name="last_part" value="{{ $last_part }}">    
            	<div class="row">

                <div class="col-md-2"><div class="form-group">
                    <input class="custom-select" placeholder="Profile Name" type="text" name="name" value="{{ (isset($_GET['name']) && ($_GET['name']))  ? $_GET['name'] : '' }}">
              </div></div>

                	<div class="col-md-2">
                    <div class="form-group">
                    <select name="age" class="custom-select">
                      <option value="" selected="">Age</option>
                      <option value="18_25" {{ (isset($_GET['age']) && ($_GET['age'] == '18_25') ) ? "selected" : '' }}>18 year to 25 year</option>
                      <option value="25_35" {{ (isset($_GET['age']) && ($_GET['age'] == '25_35') ) ? "selected" : '' }}>25 year to 35 year</option>
                      <option value="35_50" {{ (isset($_GET['age']) && ($_GET['age'] == '35_50') ) ? "selected" : '' }}>35 year to 50 year</option>
                      <option value="50_100" {{ (isset($_GET['age']) && ($_GET['age'] == '50_100') ) ? "selected" : '' }}>Above 50 year</option>
                    </select>
                    </div>
					</div>
                    <div class="col-md-2"><div class="form-group">
                    <select name="gender" class="custom-select">
                      <option value="" selected="">Gender</option>
                      <option value="1" {{ (isset($_GET['gender']) && ($_GET['gender'] == '1') ) ? "selected" : '' }}>Male</option>
                      <option value="2" {{ (isset($_GET['gender']) && ($_GET['gender'] == '2') ) ? "selected" : '' }}>Female</option>
                      <option value="3" {{ (isset($_GET['gender']) && ($_GET['gender'] == '3') ) ? "selected" : '' }}>Transsexual </option>
                    </select>
					</div></div>
                    <!-- saif changes starts -->
                   <div class="col-md-2">
                      <div class="form-group">
                         <select name="region" class="custom-select">
                          @if(empty($state))
                            <option selected="" name="states" value="">States</option>
                           @foreach($states as $row)
                           <option value="{{$row->name}}">{{$row->name}}</option>
                            @endforeach
                          @else
                           @foreach($states as $row)
  <option value="{{$row->name}}" {{($row->name==$state) ? "selected" : ''}}>   {{$row->name}}</option>
                            @endforeach

                            @endif
                         </select>
                      </div>
                   </div>  
                    <!-- saif changes end -->
                      <!-- <div class="col-md-2">
                        <div class="form-group"> -->
                        	<!--<input name="country" type="text" class="form-control" value="{{ (isset($_GET['country']) && ($_GET['country'] != '') ) ? $_GET['country'] : '' }}">-->
                        <!-- <select name="country" class="custom-select">
                            <option selected="" name="country" value="">Country</option>
                            <option value="Ukraine" {{ (isset($_GET['country']) && ($_GET['country'] == 'Ukraine') ) ? "selected" : '' }}>Ukraine</option>
                            <option value="India" {{ (isset($_GET['country']) && ($_GET['country'] == 'India') ) ? "selected" : '' }}>India</option>
                            <option value="Morocco" {{ (isset($_GET['country']) && ($_GET['country'] == 'Morocco') ) ? "selected" : '' }}>Morocco</option>
                            <option value="China" {{ (isset($_GET['country']) && ($_GET['country'] == 'China') ) ? "selected" : '' }}>China</option>
                            <option value="Colombia" {{ (isset($_GET['country']) && ($_GET['country'] == 'Colombia') ) ? "selected" : '' }}>Colombia</option>                                
                        </select>
                        </div>
                        </div>   -->
                     
                        <div class="col-md-2"><div class="form-group">
                        	<button class="advance-btn" type="button"><i class="glyphicon glyphicon-cog"></i> More Filter</button>
                        </div></div>
                         <div class="col-md-2"><div class="form-group">
                        	<button type="submit" class="btn btn-primary form-control">Search</button>
                        </div></div>
                </div>
                
                <div class="advance-fields">
                	<div class="row">

                     <div class="col-md-2"><div class="form-group">
                        <select name="status" class="custom-select">
                            <option selected="" value="">Profile Status</option>
                            <option value="yes" {{ (isset($_GET['status']) && ($_GET['status'] == 'yes') ) ? "selected" : '' }}>Active</option>
                            <option value="no" {{ (isset($_GET['status']) && ($_GET['status'] == 'no') ) ? "selected" : '' }}>Inactive</option>
                           
                        </select>
                        </div></div>
                    	<div class="col-md-2"><div class="form-group">
                        	<select name="adult_level" class="custom-select">
                              <option value="" selected="">Adult level </option>
                              <option value="Sexy" {{ (isset($_GET['adult_level']) && ($_GET['adult_level'] == 'Sexy') ) ? "selected" : '' }}>Sexy</option>
                              <option value="Adult" {{ (isset($_GET['adult_level']) && ($_GET['adult_level'] == 'Adult') ) ? "selected" : '' }}>Adult</option>
                              <option value="Non-adult" {{ (isset($_GET['adult_level']) && ($_GET['adult_level'] == 'Non-adult') ) ? "selected" : '' }}>Non-adult</option>
                              <option value="Explicit" {{ (isset($_GET['adult_level']) && ($_GET['adult_level'] == 'Explicit') ) ? "selected" : '' }}>Explicit</option>
                            </select>
                        </div></div>
                        <div class="col-md-2"><div class="form-group">
                        	<select name="profile_type" class="custom-select">
                                <option value="" selected="">Profile Type</option>
                                <option value="Generic" {{ (isset($_GET['profile_type']) && ($_GET['profile_type'] == 'Generic') ) ? "selected" : '' }}>Generic</option>
                                <option value="Mature" {{ (isset($_GET['profile_type']) && ($_GET['profile_type'] == 'Mature') ) ? "selected" : '' }}>Mature</option>
                                <option value="Kinky" {{ (isset($_GET['profile_type']) && ($_GET['profile_type'] == 'Kinky') ) ? "selected" : '' }}>Kinky</option>
                                <option value="Shemale" {{ (isset($_GET['profile_type']) && ($_GET['profile_type'] == 'Shemale') ) ? "selected" : '' }}>Shemale</option>
                            </select>
                        </div></div>
                        <div class="col-md-3"><div class="form-group">
                        	<select name="photo" class="custom-select">
                                <option value="" selected="">Photo setting</option>
                                <option value="0" {{ (isset($_GET['photo']) && ($_GET['photo'] == '0') ) ? "selected" : '' }}>	Without picture</option>
                                <option value="1" {{ (isset($_GET['photo']) && ($_GET['photo'] == '1') ) ? "selected" : '' }}>With pictures</option>
                                <!--<option value="3">Only attachements</option>
                                <option value="4">Picture + attachement</option>-->
                            </select>
                        </div></div>
                           <div class="col-md-3"><div class="form-group">
                        	<select name="no_of_pictures" class="custom-select">
                                <option selected="" value="">Number of pictures</option>
                                <option value="0" {{ (isset($_GET['no_of_pictures']) && ($_GET['no_of_pictures'] == '0') ) ? "selected" : '' }}>0</option>
                                <option value="1" {{ (isset($_GET['no_of_pictures']) && ($_GET['no_of_pictures'] == '1') ) ? "selected" : '' }}>1</option>
                                <option value="2" {{ (isset($_GET['no_of_pictures']) && ($_GET['no_of_pictures'] == '2') ) ? "selected" : '' }}>2</option>
                                <option value="3" {{ (isset($_GET['no_of_pictures']) && ($_GET['no_of_pictures'] == '3') ) ? "selected" : '' }}>3</option>
                                <option value="4" {{ (isset($_GET['no_of_pictures']) && ($_GET['no_of_pictures'] == '4') ) ? "selected" : '' }}>4</option>
                                <option value="5" {{ (isset($_GET['no_of_pictures']) && ($_GET['no_of_pictures'] == '5') ) ? "selected" : '' }}>5</option>
                                <option value="6" {{ (isset($_GET['no_of_pictures']) && ($_GET['no_of_pictures'] == '6') ) ? "selected" : '' }}>6+</option>
                            </select>
                        </div></div>
                    </div>
                    <div class="row">
                    	<div class="col-md-3"><div class="form-group">
                        <select name="appearance" class="custom-select">
                            <option value="" selected="">Appearance</option>
                            <option value="Arab" {{ (isset($_GET['appearance']) && ($_GET['appearance'] == 'Arab') ) ? "selected" : '' }}>Arab</option>
                            <option value="Asian" {{ (isset($_GET['appearance']) && ($_GET['appearance'] == 'Asian') ) ? "selected" : '' }}>Asian</option>
                            <option value="Ebony" {{ (isset($_GET['appearance']) && ($_GET['appearance'] == 'Ebony') ) ? "selected" : '' }}>Ebony</option>
                            <option value="Latin" {{ (isset($_GET['appearance']) && ($_GET['appearance'] == 'Latin') ) ? "selected" : '' }}>Latin</option>
                            <option value="White" {{ (isset($_GET['appearance']) && ($_GET['appearance'] == 'White') ) ? "selected" : '' }}>White </option>
                        </select>
                        </div></div>
                        <div class="col-md-3"><div class="form-group">
                        	<select name="relationship" class="custom-select">
                                <option value="" selected="">Civil status</option>
                                <option value="1" {{ (isset($_GET['relationship']) && ($_GET['relationship'] == '1') ) ? "selected" : '' }}>Never Married</option>
                                <option value="2" {{ (isset($_GET['relationship']) && ($_GET['relationship'] == '2') ) ? "selected" : '' }}>Separated</option>
                                <option value="3" {{ (isset($_GET['relationship']) && ($_GET['relationship'] == '3') ) ? "selected" : '' }}>Divorced</option>
                                <option value="4" {{ (isset($_GET['relationship']) && ($_GET['relationship'] == '4') ) ? "selected" : '' }}>Marrioed</option>
                                <option value="5" {{ (isset($_GET['relationship']) && ($_GET['relationship'] == '5') ) ? "selected" : '' }}>LAT relationship</option>
                                <option value="6" {{ (isset($_GET['relationship']) && ($_GET['relationship'] == '6') ) ? "selected" : '' }}>Living together</option>
                                <option value="7" {{ (isset($_GET['relationship']) && ($_GET['relationship'] == '7') ) ? "selected" : '' }}>In a relationship</option>
                            </select>
                        </div></div>
                    </div>
                </div>
            	</form>
            </div>
       

            <div class="panel panel-white"> 
                <div class="panel panel-flat">
                  <div class="panel-heading">
                    <h6 class="panel-title"><b> {{$heading }} </b><a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
                    <div class="heading-elements">
                      <ul class="icons-list">
                        <li> <a type="button" href="{{route('user.create')}}" class="btn btn-primary text-white btn-labeled btn-rounded "><b><i class="icon-plus3"></i></b> Add Player<span class="legitRipple-ripple" ></span></a></li> 
                      </ul>
                    </div>
                  </div> 
                </div>  
                <div class="panel-body"> 
                 
              </div> 
                 @if(Session::has('flash_alert_notice'))
                     <div class="alert alert-success alert-dismissable" style="margin:10px">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                      <i class="icon fa fa-check"></i>  
                     {{ Session::get('flash_alert_notice') }} 
                     </div>
                @endif
                 <div class="table-responsive">
                  <table class="table datatable-basic table-bordered table-hover table-responsive" id="roles_list">
                                   
                                        <thead>
                                            <tr>
                                                <th> Sno. </th>
                                                <th> Full Name </th>
                                                <th> Email </th>
                                                <th> Gender </th>
                                                <th> Age </th>
                                                <th> Region </th>
                                                <!-- <th> City </th> -->
                                                <th> Profile Image </th>
                                                <th> Messages </th>
                                                <th> Poke </th>
                                                <th>Activation Date</th>    
                                                <th>Signup Date</th>
                                                <th>Last Login</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                                
                                            </tr>
                                        </thead>
                                        <tbody>

                                    
                                        @foreach($users as $key => $result)
                                            <tr>
                                                 <td> {{ (($users->currentpage()-1)*30)+(++$key) }}</td>
                                                <td> {{$result->name}} <br> {{ ($result->campaign_id == 1) ? '( Landers Page )' : '' }} {{ ($result->campaign_id == 2) ? '( One Click )' : '' }}</td>
                                                <td> {{$result->email}} </td>
                                                <td> 
                                                    <?php 
                                                    if($result->gender == 1) 
                                                    {
                                                        echo 'Male';
                                                    }else if($result->gender == 2)
                                                    {
                                                        echo 'Female';
                                                    }
                                                    ?>
                                                </td>
                                                <td> {{$result->age}} </td>
                                                <td> {{$result->state}} </td>
                                                <!-- <td> {{$result->city}} </td> -->
                                                <td class="center"> 
                                               
                                                    @if($result->role_type==5)
                                                    <a href="{{url('admin/mytask/'.$result->id)}}">
                                                        View Details

                                                    </a>
                                                    @else
                                                        @if($result->profile_image)
                                                            @if($result->fake == 0)
                                                                <img src="{{ URL::asset('public/images/profile_image/'.$result->profile_image) }}" width="100px" height="100px" onerror="this.src='{{ asset("public/images/profile_image/no_image.jpg") }}'">
                                                            @endif
                                                            @if($result->fake == 1)
                                                                <img src="{{ env('PROFILE_MANAGER_ADDR').('/public/thumbnail/'.$result->profile_image) }}" width="100px" height="100px" onerror="this.src='{{ asset("public/images/profile_image/no_image.jpg") }}'">
                                                            @endif
                                                        @endif
                                                    @endif
                                                </td>
                                                <td>  </td>
                                                <td> </td>
                                                <td>
                                                @if($result->activation_time)
                                                    {!! Carbon\Carbon::parse($result->activation_time)->format('Y-m-d h:i:s'); !!}
                                                @else
                                                    --
                                                @endif    
                                                </td>     
                                                <td>
                                                    {!! Carbon\Carbon::parse($result->created_at)->format('Y-m-d h:i:s'); !!}
                                                </td>
                                                <td>
                                                @if($result->fake == 0 && $result->last_login != Null)
                                                    {!! Carbon\Carbon::parse($result->last_login)->format('Y-m-d h:i:s'); !!}
                                                @else
                                                    --
                                                @endif    
                                                </td>    
                                                <td>
                                                    @if(($result->is_deleted == 0))
                                                    <span class="label label-{{ ($result->status==1)?'success':'warning'}} status" id="{{$result->id}}"  data="{{$result->status}}"  onclick="changeStatus({{$result->id}},'user')" >
                                                            {{ ($result->status==1)?'Active':'Inactive'}}
                                                        </span>
                                                     @endif   
                                                </td>
                                                <td>
                                                    @if(($result->is_deleted == 0))
														@if(($result->fake == 0))
													<a href="{{ route('user.edit',$result->id)}}" class="btn btn-primary btn-xs" style="margin: 3px">
                                                    <i class="icon-pencil7" title="edit"></i>  
                                                    </a> 
													@endif
                                                    {!! Form::open(array('class' => 'form-inline pull-left deletion-form', 'method' => 'DELETE',  'id'=>'deleteForm_'.$result->id, 'style'=>'margin:3px', 'route' => array('user.destroy', $result->id))) !!}
                        
                                                    <button class='delbtn btn btn-danger btn-xs' type="submit" name="remove_levels" value="delete" id="{{$result->id}}" onclick=" var c = confirm('Are you sure want to delete?'); if(!c) return false;"><i class="icon-trash" title="Delete"></i> 
                                                    </button> 
                                                    {!! Form::close() !!} 
                                                    @endif
                                                </td>
                                                
                                            </tr>
                                           @endforeach
                                        
                                        </tbody>
                                    </table>
                                    Showing {{($users->currentpage()-1)*$users->perpage()+1}} to {{$users->currentpage()*$users->perpage()}}
					                of  {{$users->total()}} entries
					                 <div class="center" align="center">  {!! $users->appends(['gender' => isset($_GET['gender'])?$_GET['gender']:'', 'country' => isset($_GET['country'])?$_GET['country']:'' ,'age' => isset($_GET['age'])?$_GET['age']:'', 'photo' => isset($_GET['photo'])?$_GET['photo']:'', 'relationship' => isset($_GET['relationship'])?$_GET['relationship']:'' , 'profile_type' => isset($_GET['profile_type'])?$_GET['profile_type']:'' , 'adult_level' => isset($_GET['adult_level'])?$_GET['adult_level']:'' , 'appearance' => isset($_GET['appearance'])?$_GET['appearance']:'' ,
                                     'last_part' => isset($_GET['last_part'])?$_GET['last_part']:'' , 'no_of_pictures' => isset($_GET['no_of_pictures'])?$_GET['no_of_pictures']:'' , 'status' => isset($_GET['status'])?$_GET['status']:''])->render() !!}</div>    
                                
                 </div> 
               </div>
         </div> 
   @stop
   
   
   