<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Socialite;
use Illuminate\Http\Request;
use App\SocialAccount;
use App\User;
use Helper;
use DateTime;
use DB;

class SocialController extends Controller
{
    public function redirectToProvider(Request $request)
    {
        $provider = 'google';
        $gender =  $request->gender;
        $min_age = $request->min_age;
        $max_age = $request->max_age;
        $seeking = $request->seeking;
        return Socialite::driver($provider)->with(['state' => $gender.';'.$seeking.';'.$min_age.';'.$max_age])->redirect();
    }

    public function handleProviderCallback(Request $request,$provider)
	{
	    $state = $request->state;
	    $explode = explode(';',$request->state);

	    $socialiteUser = Socialite::driver($provider)->stateless()->user();

	    //print_r($socialiteUser);//die;

	    $user = $this->findOrCreateUser($provider, $socialiteUser);

	    auth()->login($user, true);

	    $last_login = date('Y-m-d H:i:s');
	    User::where('id',$user->id)->update(['last_login'=>$last_login]);

	    $gender =  $explode[0];
        $seeking = $explode[1];
        $min_age = $explode[2];
        $max_age = $explode[3];

	    if($gender == '' && $seeking == '' && $min_age== '' && $max_age == '')
	    {
	    	return redirect('search');
	    }
	    else{
	    	$url = 'search?gender='.$gender.'&seeking='.$seeking.'&min_age='.$min_age.'&max_age='.$max_age;

	    	return redirect($url);
	    }
	}

	public function findOrCreateUser($provider, $socialiteUser)
	{
        
	    if ($user = $this->findUserBySocialId($provider, $socialiteUser->getId())) {
	        return $user;
	    }
        
	    if ($user = $this->findUserByEmail($provider, $socialiteUser->getEmail())) {
	        $this->addSocialAccount($provider, $user, $socialiteUser);

	        return $user;
	    }

	    $last_login = date('Y-m-d H:i:s');
        $cover_img = DB::table('cover_images')->select('images')->inRandomOrder()->take(1)->first();
        //echo "<pre>"; print_r($socialiteUser); 
	    $user = User::create([
	        'name' => $socialiteUser->getName(),
	        'email' => $socialiteUser->getEmail(),
	        'password' => bcrypt(str_random(25)),
	        'provider_name' => 'google',
	        'profile_image' => $socialiteUser->getAvatar(),
            'cover_image' => $cover_img->images,
	        'status' 	=> 0,
	        'fake'		=> 0,
	        'last_login' => $last_login
	    ]);
        
        //print_r($user); die;
	    $user_detail = array(
                'user_id' => $user->id
            );

        
        $inser_detail = Helper::insert_data('user_details',$user_detail);
        
        $helper = new Helper;
            $subject = "Welcome to Flirtfull.com! To get started, please activate your account by clicking the link below.";
            $email_content = [
                    'receipent_email'=> $socialiteUser->getEmail(),
                    'subject'=>$subject,
                    'greeting'=> 'Flirtfull.com',
                    'first_name'=> $socialiteUser->getName(),
                    'template_id' => 1
                    ];
            $verification_email = $helper->sendMailFrontEnd($email_content,'verification_link');

	    $this->addSocialAccount($provider, $user, $socialiteUser);

	    return $user;
	}

	public function findUserBySocialId($provider, $id)
	{        
	    $socialAccount = SocialAccount::where('provider', $provider)
	        ->where('provider_id', $id)
	        ->first();

	    return $socialAccount ? $socialAccount->user : false;
	}

	public function findUserByEmail($provider, $email)
	{
	    return User::where('email', $email)->first();
	}

	public function addSocialAccount($provider, $user, $socialiteUser)
	{        	                
        $insert['user_id'] = $user->id;
        $insert['provider'] = $provider;
        $insert['provider_id'] = $socialiteUser->getId();
        $insert['token'] = $socialiteUser->token;
        //print_r($insert); die;
        //DB::table('social_accounts')->insert($insert);
	    //SocialAccount::create([
	    //    'user_id' => $user->id,
	    //    'provider' => $provider,
	    //    'provider_id' => $socialiteUser->getId(),
	    //    'token' => $socialiteUser->token,
	    //]);
        
	}
}