<?php

declare(strict_types=1);

namespace Modules\Admin\Http\Controllers;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Redirect;
use Input;
use Modules\Admin\Http\Requests\ProposalRequest;
use Modules\Admin\Models\Proposal;
use Route;
use View;
use Session;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Datatables;


/**
 * Class AdminController
 */
class ProposalController extends Controller
{
    /**
     * @var  Repository
     */

    /**
     * Displays all admin.
     *
     * @return \Illuminate\View\View
     */
    public function __construct()
    {
        $this->middleware('admin');
        View::share('viewPage', 'Proposal');
        View::share('helper', new Helper);
        View::share('heading', 'Proposals');
        View::share('route_url', route('proposal'));

        $this->record_per_page = Config::get('app.record_per_page');
    }

    protected $proposals;

    /*
     * Dashboard
     * */

    public function index(Proposal $proposal, Request $request)
    {        
        $page_title  = 'Proposals';
        $page_action = 'Proposals';
        

        if ($request->ajax()) {
            $id           = $request->get('id');
            $status       = $request->get('status');
            $proposal         = Proposal::find($id);
            $s            = ($status == 1) ? $status=0:$status=1;
            $proposal->status = $s;
            $proposal->save();
            echo $s;

            exit();
        }
        
        //$proposals = Proposal::orderBy('id', 'desc')->where('status', '1')->where('is_deleted', '0')->Paginate($this->record_per_page);
        //print_r($proposals); die;
        $proposals = Proposal::leftJoin('proposals_category', function($join) {
                            $join->on('proposals_images.category_id', '=', 'proposals_category.id');
                          })
                        ->select('proposals_images.*','proposals_category.title')
                        ->orderBy('proposals_images.id', 'desc')->where('proposals_images.is_deleted', '0')
                        ->Paginate($this->record_per_page);
        //print_r($proposals); die;                

        $js_file = ['common.js','bootbox.js','formValidate.js'];

        return view('admin::proposals.proposal.index', compact('js_file', 'status', 'proposals', 'page_title', 'page_action'));
    }


    public function anyData()
    {
        return Datatables::of(Proposal::query())->make(true);
    }

    public function test_table(Proposal $proposal, Request $request)
    {        
        $page_title  = 'Admin Proposal';
        $page_action = 'Admin Proposal';
        

        if ($request->ajax()) {
            $id           = $request->get('id');
            $status       = $request->get('status');
            $proposal         = Proposal::find($id);
            $s            = ($status == 1) ? $status=0:$status=1;
            $proposal->status = $s;
            $proposal->save();
            echo $s;

            exit();
        }
        // Search by name ,email and group
        $search    = Input::get('search');
        $status    = Input::get('status');
        $role_type = Input::get('role_type');

        if ((isset($search) && !empty($search)) or  (isset($status) && !empty($status)) or !empty($role_type)) {
            $search = isset($search) ? Input::get('search') : '';

            $proposals = Proposal::where(function ($query) use ($search,$status,$role_type) {
                if (!empty($search)) {
                    $query->Where('first_name', 'LIKE', "%$search%")
                        ->OrWhere('last_name', 'LIKE', "%$search%")
                        ->OrWhere('email', 'LIKE', "%$search%");
                }

                if (!empty($status)) {
                    
                    $status =  ($status == 'active')?1:0;
                    $query->Where('status', $status);
                }

                if ($role_type) {
                    $query->Where('role_type', $role_type);
                }
            })->where('role_type', 3)->Paginate($this->record_per_page);
        } else {
            $proposals = Proposal::orderBy('id', 'desc')->where('role_type', 3)->where('is_deleted', 0)->Paginate($this->record_per_page);
            $proposals = Proposal::leftJoin('proposals_category', function($join) {
                            $join->on('proposals_images.category_id', '=', 'proposals_category.id');
                          })                        
                        ->get();
        }
        
        print_r($proposals); die;
        $roles = Roles::all();

        $js_file = ['common.js','bootbox.js','formValidate.js'];

        return view('admin::proposals.proposal.test_table', compact('js_file', 'roles', 'status', 'page_title', 'page_action', 'roles' ,'role_type'));
    }

    /*
     * create Group method
     * */

    public function create(Proposal $proposal)
    {
        $page_title  = 'Proposal';
        $page_action = 'Create Proposal';
        
        $js_file     = ['common.js','bootbox.js','formValidate.js'];
        
        $category = DB::table('proposals_category')->select('*')->get();
        
        return view('admin::proposals.proposal.create', compact('js_file', 'proposal','category', 'page_title', 'page_action'));
    }

    /*
     * Save Group method
     * */

    public function store(ProposalRequest $request, Proposal $proposal)
    {
        //echo "<pre>"; print_r(Input::all()); die;
        $proposal->fill(Input::all());
        
        
        $action = $request->get('submit');        
        
        $request->file('profile_image');
        if ($action == 'avtar') {
            if ($request->file('image')) {
                $profile_image = Proposal::createImage($request, 'image');
                $request->merge(['profilePic' => $profile_image]);
                $proposal->image = $request->get('profilePic');
            }            
        }
        
        $proposal->save();
        
        $js_file = ['common.js','bootbox.js','formValidate.js'];

        return Redirect::to(route('proposal'))
            ->with('flash_alert_notice', 'New record successfully created.');
    }

    /*
     * Edit Group method
     * @param
     * object : $proposal
     * */

    public function edit(Proposal $proposal)
    {
        //echo "<pre>"; print_r($proposal); die;
        $page_title  = 'Proposal';
        $page_action = 'Show Proposal';                
        
        $js_file     = ['common.js','bootbox.js','formValidate.js'];
        $category = DB::table('proposals_category')->select('*')->get();                
        //echo "<pre>"; print_r($proposal); die;
        return view('admin::proposals.proposal.edit', compact('js_file', 'proposal', 'page_title','category', 'page_action'));
    }

    public function update(Request $request, Proposal $proposal, ProposalRequest $proposalrequest)
    {        
        //echo "<pre>"; print_r($proposal->password); die;        
        $proposal->fill(Input::all());
        $action          = $request->get('submit');
        $request->file('profile_image');
        if ($action == 'avtar') {
            if ($request->file('image')) {
                $profile_image = Proposal::createImage($request, 'image');
                $request->merge(['profilePic' => $profile_image]);
                $proposal->image = $request->get('profilePic');
            }            
        }
        $proposal->save();
                
        if ($request->get('role') == 3) {
            $Redirect = 'clientproposal';
        } else {
            $Redirect = 'proposal';
        }

        return Redirect::to(route($Redirect))
            ->with('flash_alert_notice', 'Record successfully updated.');
    }
    /*
     *Delete Proposal
     * @param ID
     *
     */
    public function destroy(Request $request, $proposal)
    { //print_r($proposal);
        //echo $proposal->id;die;
        $proposal->is_deleted = 1;
        $proposal->save();
        //$proposal->delete();
        return Redirect::to(route('proposal'))
            ->with('flash_alert_notice', 'Record  successfully deleted.');
    }

    public function show(Proposal $proposal)
    {
    }
    
    public function image_delete(Proposal $proposal, Request $request, Gallery_image $gallery_image)
    {
        //echo $request->ajax('id');        
            $id           = $request->get('id');            
            $status       = $request->get('is_deleted');
            $proposal         = Gallery_image::find($id);
            $s            = ($status == 1) ? $status=0:$status=1;
            $proposal->is_deleted = $s;
            $proposal->save();
            echo $s;

            exit();        
    }
    
    public function delete_image(Proposal $proposal, Request $request, Gallery_image $gallery_image)
    {
        //echo $request->ajax('id');        
            $id           = $request->get('id');
            $proposalid           = $request->get('proposalid');
            DB::table('gallery_images')->where('id', '=', $id)->delete();
            return Redirect::to(url('admin/proposal/'.$proposalid.'/edit'))
                ->with('flash_alert_notice', 'Image successfully deleted.');
            exit();        
    }
    
    public function search1(Proposal $proposal, Request $request)
    {
        //echo $request->ajax('id');        
            $country      = $request->get('country');
            $age      = $request->get('age');
            $gender      = $request->get('gender');
            $status      = $request->get('status');
            
            
            $proposals = Proposal::where(function ($query) use ($country,$age,$gender,$status) {
                if (!empty($country)) {
                    $query->Where('country', 'LIKE', "%$country%");                        
                }

                if (!empty($age)) {
                    $age = explode('_',$age);
                    $query->Where('age', '>=', $age[0]);
                    $query->Where('age', '<=', $age[1]);
                }

                if ($gender) {
                    $query->Where('gender', $gender);
                }
                
                if (!empty($status)) {
                    $status = ($status == 'yes') ? '1' : '0';
                    $query->Where('status', $status);
                }
            })->where('role_type', 3)->Paginate(18);
            
            //echo "<pre>"; print_r($proposals); die;            
            $roles = Roles::all();

        $js_file = [];

        return view('admin::proposals.proposal.index', compact('js_file', 'roles', 'country', 'proposals', 'page_title', 'page_action', 'roles', 'role_type')); 
    }
    
    public function search(Proposal $proposal, Request $request)
    {      $last_part = $request->get('last_part'); 
        //echo $request->ajax('id');        
            $country      = $request->get('country');
            $age      = $request->get('age');
            $gender      = $request->get('gender');
            $status      = $request->get('status');
            $relationship      = $request->get('relationship');
            $photo      = $request->get('photo');
            $appearance      = $request->get('appearance');
            $profile_type      = $request->get('profile_type');
            $adult_level      = $request->get('adult_level');
            
            //DB::enableQueryLog();
            
            $reslt = DB::table('proposals')
                    ->join('proposal_details', 'proposals.id', '=', 'proposal_details.proposal_id')            
                    ->select('proposals.*', 'proposal_details.relationship');
            
            if (!empty($country)) {
                $reslt->Where('proposals.country', 'LIKE', "%$country%");                        
            }
                
            if (!empty($age)) {
                $age = explode('_',$age);
                $reslt->Where('proposals.age', '>=', $age[0]);
                $reslt->Where('proposals.age', '<=', $age[1]);
            }
            
            if ($gender) {
                $reslt->Where('proposals.gender', $gender);
            }
            
            if ($profile_type) {
                $reslt->Where('proposals.profile_type', $profile_type);
            }
            
            if ($adult_level) {
                $reslt->Where('proposals.adult_level', $adult_level);
            }
            
            if ($appearance) {
                $reslt->Where('proposals.appearance', $appearance);
            }
            
            if (!empty($status)) {
                $status = ($status == 'yes') ? '1' : '0';
                $reslt->Where('proposals.status', $status);
            }
            
            if (isset($photo)) {
                if($photo == '1'){
                    $reslt->Where('proposals.profile_image', '!=',  '');
                }
                if($photo == '0'){
                    $reslt->Where('proposals.profile_image','');
                    //$reslt->or_Where('proposals.profile_image', '=',  'default.png');
                }
            }
            
            if ($relationship) {
                $reslt->Where('proposal_details.relationship', $relationship);
            }

            if($last_part == 'deleted') {
                $page_title  = 'Player';
                $page_action = 'Deleted Players';
                $heading = 'Deleted Players';
                $reslt->Where('proposals.is_deleted', '=',  '1');
            }
            else if($last_part == 'real') {
                $page_title  = 'Player';
                $page_action = 'Real Players';
                $heading = 'Real Players';
                $reslt->Where('proposals.fake', '=',  '0');
                $reslt->Where('proposals.is_deleted', '=',  '0');
            }
            else if($last_part == 'fake') {
                $page_title  = 'Player';
                $page_action = 'Fake Players';
                $heading = 'Fake Players';
                $reslt->Where('proposals.fake', '=',  '1');
                $reslt->Where('proposals.is_deleted', '=',  '0');
            }
            else if($last_part == 'unconfirmed') {
                $page_title  = 'Player';
                $page_action = 'Unconfirmed Players';
                $heading = 'Unconfirmed Players';
                $reslt->Where('proposals.status', '=',  '0');
                $reslt->Where('proposals.is_deleted', '=',  '0');
            }
                
            $proposals = $reslt->where('proposals.role_type', 3)->Paginate(30);
            
            //echo "<pre>"; print_r($proposals); die;            
            $roles = Roles::all();

        $js_file = [];

        $search_slug = 'list-search';

        return view('admin::proposals.proposal.index', compact('js_file', 'roles', 'country', 'proposals', 'page_title', 'page_action','heading',  'roles', 'role_type', 'search_slug')); 
    }
    
    public function setProfileImage(Proposal $proposal, Request $request, Gallery_image $gallery_image)
    {        
            $id           = $request->get('id');
            $proposalid           = $request->get('proposalid');
            
            $proposal = DB::table('proposals')->where('id', $proposalid)->first();            
            $old_image = $proposal->profile_image;
            
            $gallery = DB::table('gallery_images')->where('id', $id)->first();            
            $new_image = $gallery->gallery_image;
            
            DB::table('proposals')
            ->where('id', $proposalid)
            ->update(array('profile_image' => $new_image));
            
            DB::table('gallery_images')
            ->where('id', $id)
            ->update(array('gallery_image' => $old_image));
                        
            return Redirect::to(url('admin/proposal/'.$proposalid.'/edit'))
                ->with('flash_alert_notice', 'Image Set as Profile successfully ');
            exit();        
    }


    public function deleted(Proposal $proposal, Request $request)
    {        
        $page_title  = 'Player';
        $page_action = 'Deleted Players';
        $heading = 'Deleted Players';
        

        if ($request->ajax()) {
            $id           = $request->get('id');
            $status       = $request->get('status');
            $proposal         = Proposal::find($id);
            $s            = ($status == 1) ? $status=0:$status=1;
            $proposal->status = $s;
            $proposal->save();
            echo $s;

            exit();
        }
        // Search by name ,email and group
        $search    = Input::get('search');
        $status    = Input::get('status');
        $role_type = Input::get('role_type');

        if ((isset($search) && !empty($search)) or  (isset($status) && !empty($status)) or !empty($role_type)) {
            $search = isset($search) ? Input::get('search') : '';

            $proposals = Proposal::where(function ($query) use ($search,$status,$role_type) {
                if (!empty($search)) {
                    $query->Where('first_name', 'LIKE', "%$search%")
                        ->OrWhere('last_name', 'LIKE', "%$search%")
                        ->OrWhere('email', 'LIKE', "%$search%");
                }

                if (!empty($status)) {
                    
                    $status =  ($status == 'active')?1:0;
                    $query->Where('status', $status);
                }

                if ($role_type) {
                    $query->Where('role_type', $role_type);
                }
            })->where('role_type', 3)->Paginate($this->record_per_page);
        } else {
            $proposals = Proposal::orderBy('id', 'desc')->where('role_type', 3)->where('is_deleted', 1)->Paginate($this->record_per_page);
        }
        $roles = Roles::all();

        $js_file = ['common.js','bootbox.js','formValidate.js'];

        return view('admin::proposals.proposal.index', compact('js_file', 'roles', 'status', 'proposals', 'page_title', 'page_action','heading', 'roles', 'role_type'));
    }

    public function fake(Proposal $proposal, Request $request)
    {        
        $page_title  = 'Player';
        $page_action = 'Deleted Players';
        $heading = 'Fake Players';
        

        if ($request->ajax()) {
            $id           = $request->get('id');
            $status       = $request->get('status');
            $proposal         = Proposal::find($id);
            $s            = ($status == 1) ? $status=0:$status=1;
            $proposal->status = $s;
            $proposal->save();
            echo $s;

            exit();
        }
        // Search by name ,email and group
        $search    = Input::get('search');
        $status    = Input::get('status');
        $role_type = Input::get('role_type');

        if ((isset($search) && !empty($search)) or  (isset($status) && !empty($status)) or !empty($role_type)) {
            $search = isset($search) ? Input::get('search') : '';

            $proposals = Proposal::where(function ($query) use ($search,$status,$role_type) {
                if (!empty($search)) {
                    $query->Where('first_name', 'LIKE', "%$search%")
                        ->OrWhere('last_name', 'LIKE', "%$search%")
                        ->OrWhere('email', 'LIKE', "%$search%");
                }

                if (!empty($status)) {
                    
                    $status =  ($status == 'active')?1:0;
                    $query->Where('status', $status);
                }

                if ($role_type) {
                    $query->Where('role_type', $role_type);
                }
            })->where('role_type', 3)->Paginate($this->record_per_page);
        } else {
            $proposals = Proposal::orderBy('id', 'desc')->where('role_type', 3)->where('fake', 1)->where('is_deleted', 0)->Paginate($this->record_per_page);
        }
        $roles = Roles::all();

        $js_file = ['common.js','bootbox.js','formValidate.js'];

        return view('admin::proposals.proposal.index', compact('js_file', 'roles', 'status', 'proposals', 'page_title', 'page_action','heading', 'roles', 'role_type'));
    }

    public function real(Proposal $proposal, Request $request)
    {        
        $page_title  = 'Player';
        $page_action = 'Real Players';
        $heading = 'Real Players';
        

        if ($request->ajax()) {
            $id           = $request->get('id');
            $status       = $request->get('status');
            $proposal         = Proposal::find($id);
            $s            = ($status == 1) ? $status=0:$status=1;
            $proposal->status = $s;
            $proposal->save();
            echo $s;

            exit();
        }
        // Search by name ,email and group
        $search    = Input::get('search');
        $status    = Input::get('status');
        $role_type = Input::get('role_type');

        if ((isset($search) && !empty($search)) or  (isset($status) && !empty($status)) or !empty($role_type)) {
            $search = isset($search) ? Input::get('search') : '';

            $proposals = Proposal::where(function ($query) use ($search,$status,$role_type) {
                if (!empty($search)) {
                    $query->Where('first_name', 'LIKE', "%$search%")
                        ->OrWhere('last_name', 'LIKE', "%$search%")
                        ->OrWhere('email', 'LIKE', "%$search%");
                }

                if (!empty($status)) {
                    
                    $status =  ($status == 'active')?1:0;
                    $query->Where('status', $status);
                }

                if ($role_type) {
                    $query->Where('role_type', $role_type);
                }
            })->where('role_type', 3)->Paginate($this->record_per_page);
        } else {
            $proposals = Proposal::orderBy('id', 'desc')->where('role_type', 3)->where('fake', 0)->where('is_deleted', 0)->Paginate($this->record_per_page);
        }
        $roles = Roles::all();

        $js_file = ['common.js','bootbox.js','formValidate.js'];

        return view('admin::proposals.proposal.index', compact('js_file', 'roles', 'status', 'proposals', 'page_title', 'page_action','heading', 'roles', 'role_type'));
    }

    public function unconfirmed(Proposal $proposal, Request $request)
    {        
        $page_title  = 'Player';
        $page_action = 'Unconfirmed Players';
        $heading = 'Unconfirmed Players';
        

        if ($request->ajax()) {
            $id           = $request->get('id');
            $status       = $request->get('status');
            $proposal         = Proposal::find($id);
            $s            = ($status == 1) ? $status=0:$status=1;
            $proposal->status = $s;
            $proposal->save();
            echo $s;

            exit();
        }
        // Search by name ,email and group
        $search    = Input::get('search');
        $status    = Input::get('status');
        $role_type = Input::get('role_type');

        if ((isset($search) && !empty($search)) or  (isset($status) && !empty($status)) or !empty($role_type)) {
            $search = isset($search) ? Input::get('search') : '';

            $proposals = Proposal::where(function ($query) use ($search,$status,$role_type) {
                if (!empty($search)) {
                    $query->Where('first_name', 'LIKE', "%$search%")
                        ->OrWhere('last_name', 'LIKE', "%$search%")
                        ->OrWhere('email', 'LIKE', "%$search%");
                }

                if (!empty($status)) {
                    
                    $status =  ($status == 'active')?1:0;
                    $query->Where('status', $status);
                }

                if ($role_type) {
                    $query->Where('role_type', $role_type);
                }
            })->where('role_type', 3)->Paginate($this->record_per_page);
        } else {
            $proposals = Proposal::orderBy('id', 'desc')->where('role_type', 3)->where('status', 0)->where('is_deleted', 0)->Paginate($this->record_per_page);
        }
        $roles = Roles::all();

        $js_file = ['common.js','bootbox.js','formValidate.js'];

        return view('admin::proposals.proposal.index', compact('js_file', 'roles', 'status', 'proposals', 'page_title', 'page_action','heading', 'roles', 'role_type'));
    }
    
    
}
