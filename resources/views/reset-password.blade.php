<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="">
	<title>{{ env('APP_NAME') }}</title>
	<link rel="shortcut icon" type="image/png" href="{{ asset('public/images/fevicon.png') }}"/>
	<link href="{{ asset('public/css/font-awesome.min.css') }}" rel="stylesheet">
	<link href="{{ asset('public/css/bootstrap.min.css') }}" rel="stylesheet">
	<link rel="stylesheet" href="{{ asset('public/css/owl.carousel.min.css') }}">
  <!-- <link href="{{ asset('public/css/ninja-slider.css') }}" rel="stylesheet"> -->

  <link href="{{ asset('public/css/fotorama.css') }}" rel="stylesheet">
  <link rel="stylesheet" href="{{ asset('public/css/croppie.css') }}" />
  <!-- magnific popup -->
  <link href="{{ asset('public/css/magnific-popup.css') }}" rel="stylesheet">
  <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css"> 
 <!-- magnific popup -->
  <link href="{{ asset('public/css/style.css') }}" rel="stylesheet">
  <link href="{{ asset('public/css/responsive.css') }}" rel="stylesheet">
  
  <script src="{{ asset('public/js/jquery.min.js') }}"></script>
  <script src="{{ asset('public/js/bootstrap.min.js') }}"></script>
  <script src="{{ asset('public/js/owl.carousel.js') }}"></script>
  <!-- <script src="{{ asset('public/js/ninja-slider.js') }}"></script> -->
  <script src="{{ asset('public/js/fotorama.js') }}"></script>
  <script src="https://maps.googleapis.com/maps/api/js?key={{ env('GOOGLE_KEY') }}&libraries=places" type="text/javascript"></script>
  <script src="{{ asset('public/js/croppie.js') }}"></script>
  <script src="https://cdn.ckeditor.com/4.11.4/standard/ckeditor.js"></script>
  <!-- Latest compiled and minified CSS -->

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.2/css/bootstrap-select.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>  
<!-- Latest compiled and minified JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.2/js/bootstrap-select.min.js"></script>
    
    
<!--Firebase chat conf.-->
<script src="https://www.gstatic.com/firebasejs/5.0.4/firebase-app.js"></script>
<script src="https://www.gstatic.com/firebasejs/5.0.4/firebase-auth.js"></script>
<script src="https://www.gstatic.com/firebasejs/5.0.4/firebase-database.js"></script>
<script src="https://www.gstatic.com/firebasejs/5.0.4/firebase-storage.js"></script>
<script src="https://www.gstatic.com/firebasejs/5.0.4/firebase-messaging.js"></script>
<script src="https://www.gstatic.com/firebasejs/5.0.4/firebase-functions.js"></script>

<script>
  // Initialize Firebase
  var config = {
    apiKey: "AIzaSyCmPrmf8-I9it76PmVKDzEOooQ34i3XAm0",
    authDomain: "chat-cc446.firebaseapp.com",
    databaseURL: "https://chat-cc446.firebaseio.com",
    projectId: "chat-cc446",
    storageBucket: "chat-cc446.appspot.com",
    messagingSenderId: "686864650438"
  };
  firebase.initializeApp(config);
</script> 
<!--Firebase chat conf.--> 
    
    
    
    
<style type="text/css">
    .dropdown-menu{}
    .wlt-drpwn ul li{padding: 5px 10px;}
    .wlt-drpwn ul li:hover{background-color: #ec155a;padding: 5px 10px;color: #fff;}
    .wlt-drpwn button.btn.btn-primary.dropdown-toggle{background-color: #EC155A;border:1px solid #EC155A;}
    .wlt-drpwn button.btn.btn-primary.dropdown-toggle:hover{background-color: #EC155A;border:1px solid #EC155A;}
    header .dropdown.wlt-drpwn .dropdown-toggle{margin: 0px;}
    .wlt-drpwn button.btn.btn-primary.dropdown-toggle{font-size: 14px;font-weight: 600;color: #fff;}
    button.btn.dropdown-toggle.bs-placeholder.btn-default{background-color: #eee;}
    .bootstrap-select .dropdown-toggle .filter-option-inner-inner {
    overflow: hidden;
    color: #000;
}
  </style>
</style>
</head>

<body>
  <header>
   <div class="container-fluid">
    <div class="row">
      <div class="col-md-3 col-xs-3"><a href="{{ url('search') }}" class="logo"><img alt="" src="{{ asset('public/images/logo.png') }}"></a></div>
      <div class="col-md-9 col-xs-9">
        <div class="dropdown pull-right custom tbtwodrop">
          <button class="dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fa fa-bars"></i>
          </button>
            
                           
            
          <ul class="dropdown-menu" role="menu" >
            <li role="presentation" ><a role="menuitem" tabindex="-1" href="{{ url('/') }}">Home</a></li>
            @if (Auth::check())
            <li role="presentation"><a role="menuitem" tabindex="-1" href="{{ url('profile') }}/{{ Auth::id() }}">My Profile</a></li>
            <li role="presentation"><a role="menuitem" tabindex="-1" data-toggle="modal" data-target="#wallet">Wallet</a></li>
            @endif
            @if (Auth::check() && Auth::user()->status == 1)
            <li role="presentation"><a role="menuitem" tabindex="-1" href="{{ url('setting') }}">Setting</a></li>
            @else
            <li role="presentation"><a role="menuitem" tabindex="-1" data-toggle="modal" data-target="#notverifyModal">Setting</a></li>    
            @endif    
            <li role="presentation"><a role="menuitem" tabindex="-1" href="{{ url('help-center') }}">Help Center</a>
            </li>
            <!-- <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Promotional Coder</a></li> -->
            <li role="presentation" class="divider"></li>
           
          </ul>
        </div>
       

</div><!--end of col -->

</div>
</div>



<div class="main_wrapper"><!-- main_wrapper -->

    
	<section class="search_list_outer search_detail">
         <div class="container">
             <div class="row">
              <div class="col-md-8 col-md-offset-2">
                  <div class="panel panel-default">
                      <div class="panel-heading">Reset Password</div>
                      <div align="center" style="color:red">
                          @if($errors->first('message'))
                          <div class="alert alert-danger">
                              {{$errors->first('message')}}
                          </div>    
                          @endif  
                      </div> 
      
                      <div class="panel-body">
                          <form id="reset-password" class="form-horizontal" role="form" method="POST" action="{{ url('api/v1/password/reset') }}">
                              {{ csrf_field() }}
      
                              <input type="hidden" name="token" value="{{ $token }}">
      
                              <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                 
                                  <div class="col-md-6">
                                      <input id="email" type="hidden" class="form-control" name="email" value="{{ $email}}">
       
                                  </div>
                              </div>
      
                              <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                  <label for="password" class="col-md-4 control-label">Password</label>
      
                                  <div class="col-md-6">
                                      <input id="password" type="password" class="form-control" name="password">
                                      <p class="error-message password-wrong"></p>
                                      @if ($errors->has('password'))
                                          <span class="help-block">
                                              <strong>{{ $errors->first('password') }}</strong>
                                          </span>
                                      @endif
                                  </div>
                              </div>
      
                              <div class="form-group{{ $errors->has('confirm_password') ? ' has-error' : '' }}">
                                  <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>
                                  <div class="col-md-6">
                                      <input id="password-confirm" type="password" class="form-control" name="confirm_password">
                                      <p class="error-message cpassword-wrong"></p>
                                      @if ($errors->has('confirm_password'))
                                          <span class="help-block">
                                              <strong>{{ $errors->first('confirm_password') }}</strong>
                                          </span>
                                      @endif
                                  </div>
                              </div>
      
                              <div class="form-group">
                                  <div class="col-md-6 col-md-offset-4">
                                      <button type="submit" class="btn btn-primary">
                                          <i class="fa fa-btn fa-refresh"></i> Reset Password
                                      </button>
                                  </div>
                              </div>
                          </form>
                      </div>
                  </div>
              </div>
          </div>
         </div>
      </section>
<script type="text/javascript">
$(document).ready(function(){
    $('#reset-password').submit(function(e){
        //e.preventDefault();
        var error = 0;
        var password = $('#password').val();
        var cpassword = $('#password-confirm').val();

        if(password.trim() == "")
        {
          $('.password-wrong').text('Please enter password');
          $("#password").css("border-color", "red");
          error++;
        }
        else{
          $('.password-wrong').text('');
          $("#password").css("border-color", "#eee");
        }

        if(cpassword.trim() == "")
        {
          $('.cpassword-wrong').text('Please enter confirm password');
          $("#password-confirm").css("border-color", "red");
          error++;
        }
        else if(cpassword != password)
        {
          $('.cpassword-wrong').text('Both password should be same');
          error++;
        }
        else{
          $('.cpassword-wrong').text('');
        }

        if(error == 0)
        { 
            return true;
        }
        else{
            return false;
        }
    });
});
</script>
</div><!-- main_wrapper -->
<footer class="fl_footer_section footer_second">
  <div class="container">
    <div class="row">
     <div class="col-sm-12">
      <div class="footer_menu">
        <ul>
           <li><a href="{{ url('about') }}">About Us</a></li>
           <li><a href="{{ url('our-plans') }}">Pricing</a></li>
           <li><a href="{{ url('faq') }}">FAQ</a></li>
           <li><a href="{{ url('privacy-policy') }}">Privacy Policy</a></li>
           <li><a href="{{ url('terms-conditions') }}">Terms &amp; Conditions</a></li>
        </ul>
      </div>
      <div class="footer_text">
        <p>
            <span class="full_width">The minimum age for participation on flirtfull.com is 18 years.</span>
            Flirtfull.com is an adult social flirting platform for men and women who are looking for a fun, flirty or exciting contact. Every day, hundreds of new members sign up. Based on your profile settings, you will receive match suggestions. You can also use our search functionality and browse for profiles yourself. Flirtfull.com is designed for adult pleasure and entertainmentand optimised for desktops, mobile phones and tablets.Profiles are partly fictional, physical arrangements with these angel profiles are not possible. We strongly advise you to read our Terms and Conditions before using our Service.
        </p>
      </div>
      <div class="footer_copyright">
        <p><a href="https://www.flirtfull.com/">Flirtfull.com</a> © {{ date('Y') }} . All Rights Reserved</p>
      </div>
     </div>
    </div>
  </div>
</footer> 