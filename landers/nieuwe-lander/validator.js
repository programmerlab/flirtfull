$.validator.addMethod("loginRegex", function (value, element) {
    return this.optional(element) || /^[a-z0-9_]+$/i.test(value);
}, lang_js.name_sym);

$.validator.addMethod("synchronousRemote", function (value, element, param) {
    if (this.optional(element)) {
        return "dependency-mismatch";
    }

    var previous = this.previousValue(element);
    if (!this.settings.messages[element.name]) {
        this.settings.messages[element.name] = {};
    }
    previous.originalMessage = this.settings.messages[element.name].remote;
    this.settings.messages[element.name].remote = previous.message;

    param = typeof param === "string" && {url: param} || param;

    if (previous.old === value) {
        return previous.valid;
    }

    previous.old = value;
    var validator = this;
    this.startRequest(element);
    var data = {};
    data[element.name] = value;
    var valid = "pending";
    $.ajax($.extend(true, {
        url: param,
        async: false,
        mode: "abort",
        port: "validate" + element.name,
        dataType: "json",
        data: data,
        success: function (response) {
            validator.settings.messages[element.name].remote = previous.originalMessage;
            valid = response === true || response === "true";
            if (valid) {
                var submitted = validator.formSubmitted;
                validator.prepareElement(element);
                validator.formSubmitted = submitted;
                validator.successList.push(element);
                delete validator.invalid[element.name];
                validator.showErrors();
            } else {
                var errors = {};
                var message = response || validator.defaultMessage(element, "remote");
                errors[element.name] = previous.message = $.isFunction(message) ? message(value) : message;
                validator.invalid[element.name] = true;
                validator.showErrors(errors);
            }
            previous.valid = valid;
            validator.stopRequest(element, valid);
        }
    }, param));
    return valid;
}, function (params, element) {

});

$.validator.addMethod("check_date_of_birth", function (value, element) {

    var day = $("#day").val();
    var month = $("#month").val();
    var year = $("#year").val();
    var age = 18;

    var mydate = new Date();
    mydate.setFullYear(year, month - 1, day);

    var currdate = new Date();
    currdate.setFullYear(currdate.getFullYear() - age);

    return currdate > mydate;

}, lang_js.age_min);

$.validator.addMethod("check_email", function (value, element) {
    return this.optional(element) || /^([a-zA-Z0-9_.\-\+])+@(([a-zA-Z0-9\-])+\.)+([a-zA-Z]{2,4})+$/.test(value);
}, lang_js.email_wrong);

var validator = $('#register').validate({
    onkeyup: false,
    onclick: false,

    submitHandler: function (form) {
        return false;
    },
    errorElement: "span",
    errorClass: "help-inline",
    rules: {
        province: {
            required: true,
        },
        day: {
            required: true,
        },
        month: {
            required: true,
        },
        year: {
            required: true,
            check_date_of_birth: true,
        },
        uname: {
            required: true,
            loginRegex: true,
            minlength: 3,
            maxlength: 20,
            synchronousRemote: {
                url: "/controller/__checkuser.php",
                type: "post",
                data: {
                    uname: function () {
                        return $("#uname").val();
                    }
                }
            },
        },
        pwd1: {
            required: true,
            minlength: 6,
            maxlength: 20,
        },
        email: {
            required: true,
            email: true,
            check_email: true,
            synchronousRemote: {
                url: "/controller/__checkuser.php",
                type: "post",
                data: {
                    email: function () {
                        return $("#email").val();
                    }
                }
            },
        },
        agreements: {
            required: true,
        }
    },
    messages: {
        province: {
            required: lang_js.required_field
        },
        day: {
            required: lang_js.required_field
        },
        month: {
            required: lang_js.required_field
        },
        year: {
            required: lang_js.required_field
        },
        uname: {
            required: lang_js.required_field,
            minlength: lang_js.name_min,
            maxlength: lang_js.name_max,
            synchronousRemote: lang_js.name_exists
        },
        pwd1: {
            required: lang_js.required_field,
            minlength: lang_js.pwd_min,
            maxlength: lang_js.pwd_max
        },
        email: {
            required: lang_js.required_field,
            email: lang_js.email_wrong
        },
        agreements: {
            required: lang_js.required_field
        }
    }
});

$('#register input').on('keyup', function () {
    $(this).next("span.help-inline").hide();
    $(this).next(".help-inline").removeClass("help-inline");
});

$('#register select').on('change', function () {
    $(this).next("span.help-inline").hide();
    $(this).next(".help-inline").removeClass("help-inline");
});

function tryRegister(re) {
    if (re['status'] == 'success') {
        window.location.replace("/my-profile");
    }
}

$(document).ready(function () {
    $('img').on('dragstart', function (event) {
        event.preventDefault();
    });
    $("body").on("contextmenu", "img", function (e) {
        return false;
    });
});