<?php

namespace App\Http\Controllers\Auth;

use Auth;
use App\User;
use Mail;
use App\Mail\VerifyMail;
use App\VerifyUser;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use DB;
use Illuminate\Http\Request;
use Session;
use Hash;
use Response;
use Helper;
use DateTime;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255|unique:users|without_spaces',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
        
        Validator::extend('without_spaces', function($attr, $value){
            return preg_match('/^\S*$/u', $value);
        });
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
    }

    public function signup(Request $request)
    {
		//echo "<pre>"; print_r($request->gender); die;
        //echo "<pre>"; print_r($_POST); die;
		$rules = [
                //'name' => 'required|unique:users|without_spaces',
                'email' => 'required|email|unique:users',
                'password' => 'required',
            ];

        $messages = [
            //'name.required' => 'Please enter username.',
            'email.required' => 'Please enter email.',
            'email.email' => 'Please enter valid email.',
            'email.unique' => 'This email is already exists.',
            //'name.unique' => 'This username is already exists.',
            //'name.without_spaces' => 'Please enter username without space.',
            'password.required' => 'Please enter password',
        ];
        $validator = Validator::make($request->all(),$rules,$messages);
        
        

        if ($validator->fails()) {
            echo json_encode(array(
                'success' => false,
                'errors' => $validator->getMessageBag()->toArray()

            ));
        }
        else
        {
			$month = '01';
            if($request->month){
                $month = trim($request->month);
            }
			
			$day = '01';
            if($request->day){
                $day = trim($request->day);
            }
			
			
			$year = trim($request->year);
						

            $name =  $this->clean(explode('@',$request->email)[0]);
            if($request->name){
                $name = $request->name;
            }
			
			$country = 'United Kingdom';
            if($request->country){
                $country = trim($request->country);
            }
			
			$region = Null;
            if($request->region){
                $region = trim($request->region);
            }
			
			$city = Null;
            if($request->city){
                $city = trim($request->city);
            }
            
			$gender = trim($request->gender);
			$seeking = trim($request->seeking);

			$dob = $year.'-'.$month.'-'.$day;

            $from = new DateTime($dob);
            $to   = new DateTime('today');
            $age = $from->diff($to)->y;
			
            $profile_image = ($gender == 1) ? 'male_icon.png' : 'lady_icon.png';
            
			$cover_img = DB::table('cover_images')->select('images')->inRandomOrder()->take(1)->first();
            
            $partnerid = Null;
            if($request->partner_id){
                $p_exist = DB::table('partner')->where('partnerid',$request->partner_id)->first();
                if($p_exist){
                    $partnerid = $p_exist->id;
                }
            }
			
			$user_data = array(
                'name' => $name,
                'email' => $request->email,
				'password' => bcrypt($request->password),		
                'gender' => $gender,
                'seeking_val' => $seeking,
                'country' => $country,
                'state' => $region,
                'city' => $city,
                'dob' => $dob,
                'age'  => $age,
                'is_update' => '1',
				'profile_image' => $profile_image,
				'cover_image' => $cover_img->images,
                'status' => '0',
                'fake' => '0',
                'partner_id' => $partnerid,
                'profile_created_at'=>date('Y-m-d')
            );

            //print_r($user_data);die;

            $user = User::create($user_data);

            $user_detail = array(
                'user_id' => $user->id
            );
            
            Auth::loginUsingId($user->id);
            //print_r($user);die;
            
            $detail_id = Helper::insert_data('user_details',$user_detail);
            
            $wallet_detail = array(
                'user_id' => $user->id,
                'credits' => '1',
                'diamonds' => '3'
            );
            Helper::insert_data('user_wallet',$wallet_detail);
            $helper = new Helper;
            $subject = "Welcome to Flirting Sins! Verify your email address to get started";
            $email_content = [
                    'receipent_email'=> $request->email,
                    'subject'=>$subject,
                    'greeting'=> 'Flirting Sins',
                    'first_name'=> $name,
                    'template_id' => 1
                    ];
            //$verification_email = $helper->sendMailFrontEnd($email_content,'verification_link');

            $verification_email = $helper->sendMailFrontEnd_1($email_content,'register');
            
            //register user to third party chat
            $dataaa = [
                         "username"=>"Moneo",
                         "password"=>"Moneo2019@",
                         "service_code"=>"cd9271fa",
                         "name"=> $user->name.'__'.$user->id,
                         "group"=> "subscriber",
                         "profile"=> [
                                        "avatar"=> "avatar.jpg",
                                        "age"=> $user->age,
                                        "location"=> $user->state,
                                        "images"=> [$user->profile_image]
                                     ],
                          "additional_info"=> [
                                        "membership"=> "free",
                                        "link"=> "http://xyz.com",
                                        "about"=> "about"
                                     ],                                            
                         ];
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL,"https://im.operatorplatform.com/api/users/add");
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS,
                            "data=".json_encode($dataaa));
                curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
                
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                
                $server_output = curl_exec ($ch);                
                curl_close ($ch);
                
            $dataaa1 = [
                         "username"=>"Moneo",
                         "password"=>"Moneo2019@",
                         "service_code"=>"cd9271fa",
                         "name"=> $user->name.'__'.$user->id,
                         "group"=> "persona",
                         "profile"=> [
                                        "avatar"=> "avatar.jpg",
                                        "age"=> $user->age,
                                        "location"=> $user->state,
                                        "images"=> [$user->profile_image]
                                     ],
                          "additional_info"=> [
                                        "membership"=> "free",
                                        "link"=> "http://xyz.com",
                                        "about"=> "about"
                                     ],                                            
                         ];
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL,"https://im.operatorplatform.com/api/users/add");
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS,
                            "data=".json_encode($dataaa1));
                curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
                
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                
                $server_output = curl_exec ($ch);                
                curl_close ($ch);    
            //register user to third party chat
            
            session()->flash('success', 'Thank you for registration.Verify your email address to get started !!');
            
            echo json_encode(array('success' => true));
        }
    }

    function clean($string) {
       $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.

       return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
    }
}
