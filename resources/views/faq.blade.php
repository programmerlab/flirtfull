@extends('layout.default')
@section('content')

<section class="search_list_outer search_detail faqs_nw">
  <div class="container">
    
      <div class="row">
        <div class="col-md-12">
          <div class="profile_cover_pic">
              <!-- <div class="profile_cover_pic_outer" style="background-image:url({{ asset('public/images/cover-pic.jpg') }})"></div> -->
              <!--<div class="profile_cover_panel">
                 <div class="profile_img"><img  src="{{ asset('public/images/logo.png') }}" alt="Profile Image"></div>
                 <div class="profile_user_info">
                    <div class="user_info_inner">
                       <h3>Flirtfull</h3>
                       <p><a href="mailto:support@oddigits.com ">support@oddigits.com </a></p>
                    </div>
                 </div>
              </div>-->
           </div>

         <div class="about_panel">
            <h3>Faq</h3>
            <div class="panel_group_wrapper">
        <!-- Group Start -->
                @if(!empty($faq))
                @foreach($faq as $res)
                <div class="panel_group">
                    <div class="panel_heading">
                        <h4 class="panel_title">
                            {{ ucfirst($res->question) }}
                        </h4>
                    </div>
                    <div class="panel_content" style="display: none;">
                        <div class="panel_body">
                            <p>{{ $res->answer }}</p><p> </p>
                        </div>
                    </div>
                </div>
        <!-- Group End -->
              @endforeach
              @else
                Faq not found
              @endif
            </div>
         </div>
        </div>
      </div>
  </div>
</section>

 

<script>
$(document).ready(function(){
	$(".panel_heading").on('click', function(){
		$(this).next(".panel_content").slideToggle(300);
		$(this).toggleClass("active");
	});
});
</script>

@endsection	