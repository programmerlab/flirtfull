@extends('admin::layouts.master')
 
    @section('content') 
      @include('admin::partials.navigation')
      @include('admin::partials.breadcrumb')   

       @include('admin::partials.sidebar')  
      <div class="panel panel-white"> 
        
 
        <div class="panel panel-flat">
                      <div class="panel-heading">
                    <h6 class="panel-title"><b>{{$page_action ?? ''}}</b><a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
                    <div class="heading-elements">
                      <ul class="icons-list">
                        <li> <a type="button" href="{{route('proposalCategory')}}" class="btn btn-primary text-white   btn-rounded "> View {{$page_title}}<span class="legitRipple-ripple" ></span></a></li> 
                      </ul>
                    </div>
                  </div> 
            </div>
                      
                     <div class="panel-body">
                           
                            <!-- END BEGIN PROFILE SIDEBAR -->
                            <!-- BEGIN PROFILE CONTENT -->
                            <div class="profile-content col-md-10">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="portlet light bordered">
                                            
                                    
                                    
                                    {!! Form::model($proposalCategory, ['route' => ['proposalCategory.store'],'class'=>'form-basic ui-formwizard proposalCategory-form','id'=>'proposalCategorys_form','enctype'=>'multipart/form-data']) !!}
                                    <div class="portlet-body">
                                        <div class="tab-content">
                                            <!-- PERSONAL INFO TAB --> 
                                                <div class="margin-top-10">
                                                    @if (count($errors) > 1000)
                                                      <div class="alert alert-danger">
                                                          <ul>
                                                              @foreach ($errors->all() as $error)
                                                                  <li>{!! $error !!}</li>
                                                              @endforeach
                                                          </ul>
                                                      </div>
                                                    @endif
                                                </div>

                                            <div class="form-group {{ $errors->first('title', ' has-error') }}">
                                                <label class="control-label col-md-3">Title <span class="required"> * </span></label>
                                                <div class="col-md-12"> 
                                                    {!! Form::text('title',null, ['class' => 'form-control','data-required'=>1])  !!} 
                                                    
                                                    <span class="help-block">{{ $errors->first('title', ':message') }}</span>
                                                </div>
                                            </div>
                                                
                                            <div class="form-group {{ $errors->first('status', ' has-error') }}">
                                                <label class="control-label col-md-3">Status <span class="required"> * </span></label>
                                                <div class="col-md-12">                                                     
                                                    <select name="status" class="form-control">
                                                       <option value="1">Active</option>
                                                       <option value="0">Inactive</option>	            
                                                   </select>
                                                    <span class="help-block">{{ $errors->first('status', ':message') }}</span>
                                                </div>
                                            </div>    
                                                                                        
                                            
                                           <div class="col-md-12">
                                                  <div class="margin-top-10"> 
                                                       <button type="submit" class="btn green btn-primary" value="avtar" name="submit"> Save Changes </button>
                                                      <button type="reset" class="btn default "> Cancel </button>
                                                  </div> 
                                                 
                                           </div>

                                            {!! Form::close() !!} 
                                            
                                             
                                        </div>

                                    </div>
                                    </form>
                                </div>
                            </div>
                        
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
        
@stop