@if ($paginator->hasPages())
    <nav class="pagination is-centered">
        <ul class="pagination-list pagination">
            @if ($paginator->onFirstPage())
            <a class="prev_btn disable-btn" disabled>Prev</a>
            @else
            <a href="{{ $paginator->previousPageUrl() }}" rel="prev" class="prev_btn">Prev</a>
            @endif
            {{-- Pagination Elements --}}
            @foreach ($elements as $element)
                {{-- "Three Dots" Separator --}}
                @if (is_string($element))
                    <!-- <li><span class="pagination-ellipsis"><span>{{ $element }}</span></span></li> -->
                @endif

                {{-- Array Of Links --}}
                @if (is_array($element))
                    @foreach ($element as $page => $url)

                        @if($paginator->currentPage() >= 9)
                            @if($page != 1 && $page != 2 && $page != $paginator->lastPage() && $page != ($paginator->lastPage() -1) )
                                @if ($page == $paginator->currentPage())
                                    <li><a class="pagination-link is-current active">{{ $page }}</a></li>
                                @else
                                    <li><a href="{{ $url }}" class="pagination-link">{{ $page }}</a></li>
                                @endif
                            @endif    
                        @else
                            @if($page != $paginator->lastPage() && $page != ($paginator->lastPage() -1) )
                                @if ($page == $paginator->currentPage())
                                    <li><a class="pagination-link is-current avtive">{{ $page }}</a></li>
                                @else
                                    <li><a href="{{ $url }}" class="pagination-link">{{ $page }}</a></li>
                                @endif
                            @endif        
                        @endif
                    @endforeach
                @endif
            @endforeach
            @if ($paginator->hasMorePages())
            <a class="next_btn" href="{{ $paginator->nextPageUrl() }}" rel="next">Next</a>
            @else
            <a class="next_btn disable-btn" disabled>Next</a>
            @endif
        </ul>
    </nav>
@endif