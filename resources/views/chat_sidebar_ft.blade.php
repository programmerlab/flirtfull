<div class="col-md-3 col-sm-4 chat_sidebar">
  <span class="chat_toggle">
    <i class="first fa fa-bars"></i>
    <i class='second fa fa-comment-o'></i>
  </span>
  <div class="sidebar">
    <div class="sidebar_panel">
      <h4 class="chat_title">My Contacts</h4>
      <div id="confirmchat" class="chat_box no-chat custom-scrl">
      <!--<span id="chat_input_btn"></span>-->
        <ul id="chatHis" class="no-chat-scrl">                  
        </ul>    
      </div>
      <div class="chat_search">
        <!--<input type="text" placeholder="Search Chat" id="chat_input" class="chat_input">-->
        <input id="myInput" type="text" placeholder="Search.." class="chat_input">
        <script>
        $(document).ready(function(){
          $("#myInput1").on("keyup", function() {
            var value = $(this).val().toLowerCase();
           getChatHistory('chatHis','inbox',value,'chat'); // for show My Contacts          
          });
        });
        </script>
        <div class="chat-user-btn">
            
        </div>    
      </div>
    </div>
    
    <div class="sidebar_panel">
      <h4 class="chat_title">Chat Request</h4>
      <div class="chat_box custom-scrl">
      
        <ul id="chatRequest1" >        
       
        </ul>
      </div>
      
    </div>
    
    
  </div>
</div>

<input type="hidden"  name="sender_id" id="sender_ids" value="{{ $users->id }}">

<script type="text/javascript">


    //setup before functions
    var typingTimer;                //timer identifier
    var doneTypingInterval = 2000;  //time in ms, 5 second for example
    var $input = $('#myInput');

    //on keyup, start the countdown
    $input.on('keyup', function () {
      clearTimeout(typingTimer);
      typingTimer = setTimeout(doneTyping, doneTypingInterval);
    });

    //on keydown, clear the countdown 
    $input.on('keydown', function () {
      clearTimeout(typingTimer);
    });

    //user is "finished typing," do something
    function doneTyping () {
      var value = $('#myInput').val().toLowerCase();
      getChatHistory('chatHis','inbox',value,'chat');
    }


    var adminId = $('#sender_ids').val();
   
    function getChatHistory(type, show, search, btnText){

      var userId    = $('#chatNowuserId').val();

      var showHideBtn = showNoMsg = false;

      $('#'+type).html(''+
       '<li id="noMsgText1" >'+
          '<div class="no_chat_requests">'+
            '<h5>You have currently no chat.</h5>'+
          '</div>'+
        '</li>'+
      '');


      firebase.database().ref('/chat_history/'+adminId).once('value').then(function(snapshot) {
      $('.alreadyChat').show();
      snapshot.forEach(function(oneUser) {
        var childData = oneUser.val();
        if(childData.message && childData.name){
            if(childData.message.length > 3)
                  childData.message = childData.message.substring(0,3) + '...';
    
            var chatTabType = 'all';
            if(childData.historyType == 1){
              if(childData.lastSenderId == adminId){
                chatTabType = 'send';
              }else{
                chatTabType = 'receive';
              }
            }
    
            
            
    
           
            if(childData.uid == userId){
               //$('.alreadyChat').attr('disabled',true);
               showHideBtn = true;
            }else{
              if(showHideBtn == false){
                 $('.alreadyChat').show();
              }
            }
    
    
    
            var str1 = childData.name.toLowerCase();
            var str2 = search.toLowerCase();
            if(str1.indexOf(str2) != -1){
          
              if(chatTabType == 'all' || chatTabType == 'send'){
                $('#noMsgText1').remove();
              }
              
             if(childData.profilePic.indexOf('crop_') != -1){
                var imgurll = '{{ env('PROFILE_MANAGER_ADDR').('/public/thumbnail/') }}';
              }else{
                var imgurll = '{{ asset('/public/thumbnail').'/' }}';
              }
    
              $('#'+type).append(''+
                '<li class="confirmchat '+chatTabType+'">'+
                  '<div class="chat-user-img">'+
                    '<a href="{{ url('inbox-chat') }}/'+childData.uid+'">'+
                      '<img alt="" src="'+imgurll+childData.profilePic+'" height="55px" onerror="https://www.moneoangels.com/public/thumbnail/'+childData.profilePic+'">'+
                      '<img alt="" class="zoomed" src="'+imgurll+childData.profilePic+'"  height="55px" onerror="https://www.moneoangels.com/public/thumbnail/'+childData.profilePic+'">'+
                    '</a>'+
                  '</div>'+              
                '</li>'+
              '')
            }
    
            if(chatTabType == 'receive'){
             
              showNoMsg = true;
              $('#chatRequest1').append(''+
                '<li class="confirmchat">'+
                  '<div class="chat-user-img">'+
                    '<a href="{{ url('inbox-chat') }}/'+childData.uid+'">'+
                      '<img alt="" src="'+imgurll+childData.profilePic+'" height="55px">'+
                      '<img alt="" class="zoomed" src="'+imgurll+childData.profilePic+'"  height="55px">'+
                    '</a>'+
                  '</div>'+            
                '</li>'+
              '')
            }else{
              if(showNoMsg == false){
                $('#chatRequest1').html(''+
                 '<li id="noMsgText" >'+
                    '<div class="no_chat_requests">'+
                      '<h5>You have currently no chat request.</h5>'+
                    '</div>'+
                  '</li>'+
                '');
              }else{
                $('#noMsgText').remove();
              }
            }
    
    
            showHideChat(show);
        }
      });
    });
  }
  getChatHistory('chatHis','inbox','','chat'); // for show My Contacts

  function getCaht(userId){
    firebase.database().ref('/users/'+userId).once('value').then(function(snapshot) {
      var childData = snapshot.val();
      var id = childData.userId;
      var name = childData.userName;
      var image = childData.userImage;
      set_receiver(id,name,image,0);;
    });
  }
  
  function showHideChat(type){
   
    $('.chating_list').hide();
    if(type == 'inbox'){
        //$('.receive').show();
        $('.all').show();
        $('.send').hide();
        $('.receive').hide();
    }else if(type == 'Outbox'){
        $('.send').show();
    }else if(type == 'Conversation'){
        $('.all').show();
    }
  }
</script>