@extends('admin::layouts.master')
 
    @section('content') 
      @include('admin::partials.navigation')
      @include('admin::partials.breadcrumb')   

       @include('admin::partials.sidebar')  
      <div class="panel panel-white"> 
        
 
        <div class="panel panel-flat">
                      <div class="panel-heading">
                    <h6 class="panel-title"><b>{{$page_action ?? ''}}</b><a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
                    <!-- <div class="heading-elements">
                      <ul class="icons-list">
                        <li> <a type="button" href="{{route('credit')}}" class="btn btn-primary text-white   btn-rounded "> View {{$page_title}}<span class="legitRipple-ripple" ></span></a></li> 
                      </ul>
                    </div> -->
                  </div> 
            </div>
                      
                     <div class="panel-body">
                       @if(Session::has('flash_alert_notice'))
                           <div class="alert alert-success alert-dismissable" style="margin:10px">
                              <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                            <i class="icon fa fa-check"></i>  
                           {{ Session::get('flash_alert_notice') }} 
                           </div>
                      @endif     
                            <!-- END BEGIN PROFILE SIDEBAR -->
                            <!-- BEGIN PROFILE CONTENT -->
                            <div class="profile-content col-md-10">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="portlet light bordered">
                                            
                                    
                                    
                                    <form method="post" enctype="multipart/form-data" class="form-basic ui-formwizard" action="{{route('reward.store')}}">
                                    <div class="portlet-body">
                                        <div class="tab-content">
                                            <!-- PERSONAL INFO TAB --> 
                                                <div class="margin-top-10">
                                                    @if (count($errors) > 1000)
                                                      <div class="alert alert-danger">
                                                          <ul>
                                                              @foreach ($errors->all() as $error)
                                                                  <li>{!! $error !!}</li>
                                                              @endforeach
                                                          </ul>
                                                      </div>
                                                    @endif
                                                </div>

                                                <div class="form-group {{ $errors->first('user_id', ' has-error') }}">
                                                 <label class="control-label col-md-3">Select Users
                                                          <span class="required"> * </span>
                                                      </label>
                                                <div class="col-md-12"> 
                                                   <select name="user_id" class="form-control select2me">
                                                   <option value="">Select Users</option>
                                                    @foreach($users as $key=>$value)
                                                    
                                                    <option value="{{$value->id}}">{{ $value->name }}</option>
                                                    @endforeach
                                                    </select>
                                                    <span class="help-block">{{ $errors->first('user_id', ':message') }}</span>
                                                </div>
                                            </div> 
                                            
                                            <div class="form-group {{ $errors->first('credit', ' has-error') }}">
                                                <label class="control-label col-md-3">Credit <span class="required"> * </span></label>
                                                <div class="col-md-12"> 
                                                    {!! Form::text('credit',null, ['class' => 'form-control','data-required'=>1])  !!} 
                                                    
                                                    <span class="help-block">{{ $errors->first('credit', ':message') }}</span>
                                                </div>
                                            </div>
                                                
                                                
                                                              
                          
                              
                                           <div class="col-md-12">
                                                  <div class="margin-top-10"> 
                                                       <button type="submit" class="btn green btn-primary" value="avtar" name="submit"> Save Changes </button>
                                                      <button type="reset" class="btn default "> Cancel </button>
                                                  </div> 
                                                 
                                           </div>

                                            {!! Form::close() !!} 
                                            
                                             
                                        </div>

                                    </div>
                                    </form>
                                </div>
                            </div>
                        
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
        
@stop