<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Hash;
use Response;
use Helper;
use Illuminate\Support\Facades\Validator;
use Auth,Crypt;
use App\User;
use DB;
use Illuminate\Http\Request;
use Session;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /* @method : forget password
    * @param : token,email
    * Response : json
    * Return : json response
    */
    public function forgetPassword(Request $request)
    {
        $email = $request->input('email');
        //Server side valiation
        $validator = Validator::make($request->all(), [
            'email' => 'required|email'
        ]);
        $helper = new Helper;
        if ($validator->fails()) {
            $error_msg  =   [];
            foreach ( $validator->messages()->all() as $key => $value) {
                        array_push($error_msg, $value);
                    }
            return Response::json(array(
                'status' => 0,
                'message' => $error_msg[0],
                'data'  =>  ''
                )
            );
        }
        $user =   User::where('email',$email)->first();
        if($user==null){
            return Response::json(array(
                'status' => 0,
                'code' => 201,
                'message' => "The email you provided isn't in our system",
                'data'  =>  $request->all()
                )
            );
        }
        $user_data = User::find($user->id);
        $temp_password = Hash::make($email);
      // Send Mail after forget password
        $temp_password =  Hash::make($email);
        $email_content = array(
                        'receipent_email'   => $request->input('email'),
                        'subject'           => 'Your Flirtfull Account Password',
                        'first_name'        => $user->name,
                        'temp_password'     => $temp_password,
                        'encrypt_key'       => Crypt::encrypt($email),
                        'greeting'          => 'Flirtfull',
                        'message'           => 'Looks like you need to reset your password. Please click the link below on.',
                        'profile_image'     => $user->profile_image
                    );
        //print_r($email_content); die;
        $helper = new Helper;
        $email_response = $helper->sendMail(
                                $email_content,
                                'forgot_password_link'
                            );
       return   response()->json(
                    [
                        "status"=>1,
                        "code"=> 200,
                        "message"=>"Reset password link is sent. Please check your email.",
                        'data' => $request->all()
                    ]
                );
    }

    public function resetPassword(Request $request)
    { echo 123;die;
        $encryptedValue = $request->get('key')?$request->get('key'):'';
        $method_name = $request->method();
        $token = $request->get('token');
       // $email = ($request->get('email'))?$request->get('email'):'';
        if($method_name=='GET')
        {
            try {
                $email = Crypt::decrypt($encryptedValue);
                if (Hash::check($email, $token)) {
                    return view('admin.auth.passwords.reset',compact('token','email'));
                }else{
                    return Response::json(array(
                        'status' => 0,
                        'message' => "Invalid reset password link!",
                        'data'  =>  ''
                        )
                    );
                }
            } catch (DecryptException $e) {
            //   return view('admin.auth.passwords.reset',compact('token','email'))
              //              ->withErrors(['message'=>'Invalid reset password link!']);
                return Response::json(array(
                        'status' => 0,
                        'message' => "Invalid reset password link!",
                        'data'  =>  ''
                        )
                    );
            }
        }else
        {
            try {
                $email = Crypt::decrypt($encryptedValue);
                if (Hash::check($email, $token)) {
                        $password =  Hash::make($request->get('password'));
                        $user = User::where('email',$email)->update(['password'=>$password]);
                        return Response::json(array(
                                'status' => 1,
                                'message' => "Password reset successfully.",
                                'data'  =>  []
                                )
                            );
                }else{
                    return Response::json(array(
                        'status' => 0,
                        'message' => "Invalid reset password link!",
                        'data'  =>  ''
                        )
                    );
                }
            } catch (DecryptException $e) {
                return Response::json(array(
                        'status' => 0,
                        'message' => "Invalid reset password link!",
                        'data'  =>  []
                        )
                    );
            }
        }
    }

    /* @method : change password
    * @param : token,oldpassword, newpassword
    * Response : "message"
    * Return : json response
   */
    public function changePassword(Request $request)
    {
        $email = $request->input('email');
        //Server side valiation
        $validator = Validator::make($request->all(), [
            'email' => 'required|email'
        ]);
        $helper = new Helper;
        if ($validator->fails()) {
            $error_msg  =   [];
            foreach ( $validator->messages()->all() as $key => $value) {
                        array_push($error_msg, $value);
                    }
            return Response::json(array(
                'status' => 0,
                "code" => 201,
                'message' => $error_msg[0],
                'data'  =>  $request->all()
                )
            );
        }
        $user =   User::where('email',$email)->first();
        if($user==null){
            return Response::json(array(
                'status' => 0,
                'code' => 500,
                'message' => "The email address you provided isn't in our system",
                'data'  =>  $request->all()
                )
            );
        }
        
        $user = User::where('email',$request->get('email'))->first();
        $user_id = $user->id;
        $old_password = $user->password;
        $validator = Validator::make($request->all(), [
            'oldPassword' => 'required',
            'newPassword' => 'required|min:6'
        ]);
        // Return Error Message
        if ($validator->fails()) {
            $error_msg  =   [];
            foreach ( $validator->messages()->all() as $key => $value) {
                        array_push($error_msg, $value);
                    }
            return Response::json(array(
                'status' => 0,
                'message' => $error_msg[0],
                'data'  =>  ''
                )
            );
        }
        if (Hash::check($request->input('oldPassword'),$old_password)) {
           $user_data =  User::find($user_id);
           $user_data->password =  Hash::make($request->input('newPassword'));
           $user_data->save();
           return  response()->json([
                    "status"=>1,
                    "code"=> 200,
                    "message"=>"Password changed successfully.",
                    'data' => []
                    ]
                );
        }else
        {
            return Response::json(array(
                'status' => 0,
                "code"=> 500,
                'message' => "Old password mismatch!",
                'data'  =>  []
                )
            );
        }
    }
}
