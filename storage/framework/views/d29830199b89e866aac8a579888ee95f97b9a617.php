<!DOCTYPE html>

<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8" />
        <title><?php echo e($setting->website_title ?? 'Admin'); ?> </title>
        <?php if(isset($setting->website_logo)): ?>
        <link rel="shortcut icon" type="image/png" href="<?php echo e(URL::asset('storage/uploads/img/'.$setting->website_logo )); ?>" /> 
        <?php endif; ?>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta name="robots" content="noindex, nofollow">
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
      
  
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
        <link href="<?php echo e(URL::asset('assets/css/icons/icomoon/styles.css')); ?>" rel="stylesheet" type="text/css">
         <link href="<?php echo e(URL::asset('assets/css/bootstrap.css')); ?>" rel="stylesheet" type="text/css"> 
          <link href="<?php echo e(URL::asset('assets/css/components.css')); ?>" rel="stylesheet" type="text/css">
        <link href="<?php echo e(URL::asset('assets/css/core.css')); ?>" rel="stylesheet" type="text/css">
              
       
        <link href="<?php echo e(URL::asset('assets/css/colors.css')); ?>" rel="stylesheet" type="text/css">
        <link href="https://cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css">
        <!-- /global stylesheets -->
 
 
        <!-- Core JS files -->
  <script type="text/javascript" src="<?php echo e(URL::asset('assets/js/plugins/loaders/pace.min.js')); ?>"></script>
  <script type="text/javascript" src="<?php echo e(URL::asset('assets/js/core/libraries/jquery.min.js')); ?>"></script>
  <script type="text/javascript" src="<?php echo e(URL::asset('assets/js/core/libraries/bootstrap.min.js')); ?>"></script>

  <script src="https://cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>

        <script>
        var base_url = "<?php echo e(url('/')); ?>";
        </script>
    </head>

 