"use strict";
const path = require("path");
const http = require("http");
const publicPath = path.join(__dirname, "../public");
const express = require("express");
const socketIO = require("socket.io");
// const client = require("socket.io").listen(4000).sockets;
var mongoose = require("mongoose");
var app = express();
var server = http.createServer(app);
var io = socketIO(server);
const {
    generateMessage,
    generateLocationMessage
} = require("./utils/message.js");
var uri = "mongodb+srv://@crm-2laff.mongodb.net/test";
mongoose.connect(
    uri,
    {
        auth: {
            user: "torenia",
            password: "torenia@1mongo"
        }
    }
);
var Schema = {
    senderId: String,
    senderName: String,
    receiverId: String,
    receiverName: String,
    chatId: String,
    message: String
};
var modal = mongoose.model("chats", Schema);
console.log("MongoDB connected...");
const { isRealString } = require("./utils/validation.js");
const { Users } = require("./utils/users.js");

var users = new Users();

app.use(express.static(publicPath));

io.on("connection", socket => {
    console.log("New user connected");

    socket.on("join", (params, callback) => {
        console.log("asd", params);
        if (!isRealString(params.name) && !isRealString(params.room)) {
            return callback("Name and Room name are required");
        }

        socket.join(params.room);
        users.removeUser(socket.id);
        users.addUser(socket.id, params.name, params.room);

        io.to(params.room).emit(
            "updateUserList",
            users.getUserList(params.room)
        );

        socket.emit("newMessage", generateMessage("", ""));
        modal.find({ chatId: "#######" }, (err, data) => {
            socket.emit("history", data);
        });

        socket.broadcast
            .to(params.room)
            .emit("newMessage", generateMessage("", ``));

        callback();
    });

    socket.on("createMessage", (message, callback) => {
        var user = users.getUser(socket.id);

        if (user && isRealString(message.text)) {
            io.to(user.room).emit(
                "newMessage",
                generateMessage(user.name, message.text)
            );
        }
        var insertedData = {
            senderId: message.senderId,
            senderName: message.senderName,
            receiverId: message.receiverId,
            receiverName: message.receiverName,
            chatId: message.chatID,
            message: message.text
        };
        modal.create(insertedData, (err, data) => {
            if (err) {
                console.log(err);
            } else {
                console.log("inserted data successfully");
            }
        });
        console.log("New Message", message);
        callback();
    });

    // socket.on("createLocationMessage", coords => {
    //     var user = users.getUser(socket.id);

    //     if (user) {
    //         io.to(user.room).emit(
    //             "newLocationMessage",
    //             generateLocationMessage(
    //                 user.name,
    //                 coords.latitude,
    //                 coords.longitude
    //             )
    //         );
    //     }
    // });

    socket.on("disconnect", () => {
        var user = users.removeUser(socket.id);

        if (user) {
            io.to(user.room).emit(
                "updateUserList",
                users.getUserList(user.room)
            );
            io.to(user.room).emit("newMessage", generateMessage("", ``));
        }
        console.log("User Disconnected");
    });
});

server.listen(9090, () => {
    console.log("Started on port 9090");
});
