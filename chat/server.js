const mongo = require("mongodb").MongoClient;
const client = require("socket.io").listen(4000).sockets;
var mongoose = require("mongoose");
var express = require("express");
var app = express();

// Connect to mongo
// mongo.connect(
//     "mongodb://127.0.0.1/mongochat",
//     function(err, db) {
//         if (err) {
//             throw err;
//         }
var uri = "mongodb+srv://@crm-2laff.mongodb.net/CRM";
mongoose.connect(
    uri,
    {
        auth: {
            user: "torenia",
            password: "torenia@1mongo"
        }
    }
);
var Schema = {
    senderId: String,
    senderName: String,
    receiverId: String,
    receiverName: String,
    chatId: String,
    message: String
};
var modal = mongoose.model("chats", Schema);
console.log("MongoDB connected...");

// Connect to Socket.io
client.on("connection", function(socket) {
    // Create function to send status
    sendStatus = function(s) {
        socket.emit("status", s);
    };

    // Get chats from mongo collection
    modal.find({}, function(err, res) {
        if (err) {
            throw err;
        }

        // Emit the messages
        socket.emit("output", res);
    });

    // Handle input events
    socket.on("input", function(data) {
        let name = data.name;
        let message = data.message;

        // Check for name and message
        if (name == "" || message == "") {
            // Send error status
            sendStatus("Please enter a name and message");
        } else {
            // Insert message
            var msg = new modal({ senderName: name, message: message });
            modal.create(msg, function() {
                client.emit("output", [data]);

                // Send status object
                sendStatus({
                    message: "Message sent",
                    clear: true
                });
            });
        }
    });

    // Handle clear
    socket.on("clear", function(data) {
        // Remove all chats from collection
        modal.remove({}, function() {
            // Emit cleared
            socket.emit("cleared");
        });
    });
});
//     }
// );
