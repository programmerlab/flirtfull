<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use Validator;
use Illuminate\Support\Facades\File;
use Session;
use Hash;
use Auth;
use Illuminate\Support\Facades\Input;
use App\Models\Profile;
use Carbon\Carbon;
use Helper;
use DateTime;
use Image;

class ProfileController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    protected $profile;

    public function __construct(Request $request, Profile $profile)
    {
        error_reporting(env('ERROR_SHOW'));
        ini_set('display_errors', env('ERROR_SHOW'));
        $this->middleware('web');
        $this->profile = $profile;

        if(Auth::check()){           
            DB::table('users')->where("id",Auth::user()->id)->update(array("is_login"=>"1","login_time"=>time()));
        }
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
   
    public function index($user_id)
    {
        $user_detail = $this->profile->user_detail($user_id);

        if($user_detail && !empty($user_detail))
        {       
                $get_videos = $this->profile->get_videos($user_detail->user_id);

                $user_detail->my_videos = $get_videos;


                $get_public_images = $this->profile->get_public_images($user_detail->user_id);

                $user_detail->my_photos = $get_public_images;
            
                
                $get_private_images = $this->profile->get_private_images($user_detail->user_id);

                $user_detail->private_photos = $get_private_images;
            
                
                $get_interests = $this->profile->get_interests($user_detail->user_id);

                $user_detail->interests = $get_interests;
            
                
                $get_languages = $this->profile->get_languages($user_detail->user_id);

                $user_detail->languages = $get_languages;

        }

        $users = DB::table('users')->select('users.*')->where('users.id', '=',Auth::id())->first();

        $liked = DB::table('chat_room')->select('*')->where('sender_id', '=',Auth::id())->where('receiver_id',$user_id)->where('status','1')->get();
        
        //if($user_id == 4639){ echo "<pre>"; echo Auth::id(); echo $user_id; print_r($liked); die; }
        
        $liking = DB::table('likes')->where('sender_id', '=',Auth::id())->where('receiver_id',$user_id)->get();
        //if($user_id == 4634){ echo "<pre>"; echo Auth::id(); echo $user_id; print_r($liking); die; }
        //print_r($users);die;

        $get_interests = $this->profile->get_interests($user_detail->user_id);

        $similar_profile = DB::table('users')->leftJoin('user_details', 'user_details.user_id', '=', 'users.id')->leftJoin('user_interests', 'user_interests.user_id', '=', 'user_details.user_id')->select('users.*','user_details.*')->where('users.id', '!=',$user_id);
        $rand = rand(10,20);
        if(!empty($get_interests))
        {
            if (strpos($get_interests, ',') !== false) 
            { 
                
                $output_array = explode(',',$get_interests);

                foreach($output_array as $val)
                {
                   $similar_profile->whereRaw('FIND_IN_SET("'.$val.'",interests)');
                }
            }
            else
            { 
                $similar_profile->where('interests','=',$get_interests);
            }
            $similar = $similar_profile->skip($rand)->take(5)->get();
        }
        else{ 
            $similar = $similar_profile->skip($rand)->take(5)->get();
        }

        $all_interests = Helper::all_data('interests',['status'=>1]);

        $all_languages = Helper::all_data('languages',['status'=>1]);
        $chat_request = [];//DB::table('users')->select('users.*')->where('users.id', '!=',Auth::id())->paginate(5);
        //print_r($similar);die;

        $countries = DB::table('countries')->get();
        
        // insert in activity
        if (Auth::check()){
            if($user_id != Auth::id()){
                $activity['from_id'] = Auth::id();
                $activity['to_id'] = $user_id;
                $activity['message'] = Auth::user()->name.' has viewed your profile';
                $activity['type'] = 1;
                $activity['payment_type'] = NULL;
                DB::table('activity')->insertGetId($activity);
            }
        }
                
        //getting proposals
        $proposal_category = DB::table('proposals_category')->where('is_deleted',0)->get();
        //$proposals = DB::table('proposals_images')->leftJoin('proposals_category', function($join) {
        //                    $join->on('proposals_images.category_id', '=', 'proposals_category.id');
        //                  })
        //                ->select('proposals_images.*','proposals_category.title')
        //                ->orderBy('proposals_images.id', 'desc')->where('proposals_images.is_deleted', '0')->where('proposals_images.status', '1')
        //                ->get();
        //print_r($proposals); die;
        return view('profile',compact('user_detail','similar','all_interests','all_languages','chat_request','countries','users','liked','liking','proposals','proposal_category'));
    }
    
    public function index_test($user_id)
    {
        $user_detail = $this->profile->user_detail($user_id);

        if($user_detail && !empty($user_detail))
        {       
                $get_videos = $this->profile->get_videos($user_detail->user_id);

                $user_detail->my_videos = $get_videos;


                $get_public_images = $this->profile->get_public_images($user_detail->user_id);

                $user_detail->my_photos = $get_public_images;
            
                
                $get_private_images = $this->profile->get_private_images($user_detail->user_id);

                $user_detail->private_photos = $get_private_images;
            
                
                $get_interests = $this->profile->get_interests($user_detail->user_id);

                $user_detail->interests = $get_interests;
            
                
                $get_languages = $this->profile->get_languages($user_detail->user_id);

                $user_detail->languages = $get_languages;

        }

        $users = DB::table('users')->select('users.*')->where('users.id', '=',Auth::id())->first();

        $liked = DB::table('chat_room')->select('*')->where('sender_id', '=',Auth::id())->where('receiver_id',$user_id)->where('status','1')->get();
        
        //if($user_id == 4639){ echo "<pre>"; echo Auth::id(); echo $user_id; print_r($liked); die; }
        
        $liking = DB::table('likes')->where('sender_id', '=',Auth::id())->where('receiver_id',$user_id)->get();
        //if($user_id == 4634){ echo "<pre>"; echo Auth::id(); echo $user_id; print_r($liking); die; }
        //print_r($users);die;

        $get_interests = $this->profile->get_interests($user_detail->user_id);

        $similar_profile = DB::table('users')->leftJoin('user_details', 'user_details.user_id', '=', 'users.id')->leftJoin('user_interests', 'user_interests.user_id', '=', 'user_details.user_id')->select('users.*','user_details.*')->where('users.id', '!=',$user_id);
        if(!empty($get_interests))
        {
            if (strpos($get_interests, ',') !== false) 
            { 
                
                $output_array = explode(',',$get_interests);

                foreach($output_array as $val)
                {
                   $similar_profile->whereRaw('FIND_IN_SET("'.$val.'",interests)');
                }
            }
            else
            { 
                $similar_profile->where('interests','=',$get_interests);
            }
            $similar = $similar_profile->orderByRaw("RAND()")->take(5)->get();
        }
        else{ 
            $similar = $similar_profile->orderByRaw("RAND()")->take(5)->get();
        }

        $all_interests = Helper::all_data('interests',['status'=>1]);

        $all_languages = Helper::all_data('languages',['status'=>1]);
        $chat_request = [];//DB::table('users')->select('users.*')->where('users.id', '!=',Auth::id())->paginate(5);
        //print_r($similar);die;

        $countries = DB::table('countries')->get();
        
        // insert in activity
        if (Auth::check()){
            if($user_id != Auth::id()){
                $activity['from_id'] = Auth::id();
                $activity['to_id'] = $user_id;
                $activity['message'] = Auth::user()->name.' has viewed your profile';
                $activity['type'] = 1;
                $activity['payment_type'] = NULL;
                DB::table('activity')->insertGetId($activity);
            }
        }
                
        //getting proposals
        $proposal_category = DB::table('proposals_category')->where('is_deleted',0)->get();
        //$proposals = DB::table('proposals_images')->leftJoin('proposals_category', function($join) {
        //                    $join->on('proposals_images.category_id', '=', 'proposals_category.id');
        //                  })
        //                ->select('proposals_images.*','proposals_category.title')
        //                ->orderBy('proposals_images.id', 'desc')->where('proposals_images.is_deleted', '0')->where('proposals_images.status', '1')
        //                ->get();
        //print_r($proposals); die;
        return view('test',compact('user_detail','similar','all_interests','all_languages','chat_request','countries','users','liked','liking','proposals','proposal_category'));
    }
    
    public function user_profile_update(Request $request)
    {
        //print_r($request->all());die;
        $user_id = $request->user_id;
        $name = trim($request->name);
        $gender = trim($request->gender);
        $seeking = trim($request->seeking);
        $day = trim($request->day);
        $month = trim($request->month);
        $year = trim($request->year);
        $address = trim($request->address);
        $city = trim($request->city);
        $country = trim($request->country);
        
        $dob = $year.'-'.$month.'-'.$day;

        $from = new DateTime($dob);
        $to   = new DateTime('today');
        $age = $from->diff($to)->y;

        $update_profile = array(
            'name' => $name,
            'gender' => $gender,
            'seeking_val' => $seeking,
            'dob' => $dob,
            'age' => $age,
            'address' => $address,
            'country' => $country,
            'city' => $city,
            'is_update' => '1',
        );

        $update_user = DB::table('users')->where('id', $user_id)->update($update_profile);
        
        if($update_user)
        {
            echo 'success';
        }
        else{
            echo 'success';
        }

    }

    public function about_me_update(Request $request)
    {
        $user_id = $request->user_id;
        $about_me = $request->about_me;

        $update_about_me = array(
            'about_me' => $about_me,
            'updated_at' => date('Y-m-d H:i:s')
        );

        $update_user = DB::table('user_details')->where('user_id', $user_id)->update($update_about_me);
        
        if($update_user)
        {
            echo 'success';
        }
        else{
            echo 'error';
        }
    }

    public function looking_for_update(Request $request)
    {
        $user_id = $request->user_id;
        $seeking_val = $request->seeking_val;
        $min_age = $request->min_age;
        $max_age = $request->max_age;
        $about_partner = $request->about_partner;

        $update_seeking = array(
            'seeking_val' => $seeking_val
        );

        $update_user = DB::table('users')->where('id', $user_id)->update($update_seeking);

        $update_looking_for = array(
            'min_age' => $min_age,
            'max_age' => $max_age,
            'about_partner' => $about_partner,
            'updated_at' => date('Y-m-d H:i:s')
        );

        $update_user = DB::table('user_details')->where('user_id', $user_id)->update($update_looking_for);
        
        if($update_user)
        {
            echo 'success';
        }
        else{
            echo 'error';
        }
    }

    public function interests_update(Request $request)
    {
        $user_id = $request->user_id;
        $interests = $request->interests;

        $update_interests = array(
            'interests' => $interests,
            'updated_at' => date('Y-m-d H:i:s')
        );

        $insert_interests = array(
            'user_id' => $user_id,
            'interests' => $interests,
            'updated_at' => date('Y-m-d H:i:s')
        );

        $get_interests = DB::table('user_interests')->where('user_id','=',$user_id)->first();

        if(!empty($get_interests))
        {
            $update_user = DB::table('user_interests')->where('user_id', $user_id)->update($update_interests);
        }
        else{
            $update_user = DB::table('user_interests')->insertGetId($insert_interests);
        }
        
        if($update_user)
        {
            echo 'success';
        }
        else{
            echo 'error';
        }
    }
    
    public function get_chat_id(Request $request){  
    //print_r($_POST);die;      
        $res1 = DB::table('chat_room')
                    ->where('sender_id', $_POST['sender_id'])
                    ->where('receiver_id', $_POST['receiver_id'])
                    ->first();
                    
        $res2 = DB::table('chat_room')
                    ->where('sender_id', $_POST['receiver_id'])
                    ->where('receiver_id', $_POST['sender_id'])
                    ->first();
        if($res1 || $res2){
            $roomid = (isset($res1->room_id)) ? $res1->room_id : $res2->room_id;
            //DB::table('chat_room')->where('room_id', $roomid)->update(array("message"=>NULL));
            echo $roomid;
        }else{
            $insert['sender_id'] = $_POST['sender_id'];
            $insert['receiver_id'] = $_POST['receiver_id'];
            $roomid = $this->create_roomId($_POST['sender_id'],$_POST['receiver_id']);
            $insert['room_id'] = $roomid;
            $insert['subject'] = NULL;
            $insert['message'] = 'I wanna chat with you';
            $res = DB::table('chat_room')->insert($insert);
            echo $res->room_id;
        }            
    }
    
    public function update_conversation(Request $request){
            if(isset($request->free_message) && ($request->free_message > 0)){
                DB::table('users')->where('id', Auth::id())->update(array("free_message"=>0));
            }
            $roomid = $request->room_id;
            $amt = $request->amt;
            $mesageType = isset($request->mesageType) ? $request->mesageType : '1';
            $msg = isset($request->msg) ? $request->msg : '1';
            
            $res1 = DB::table('chat_room')
                    ->where('sender_id', Auth::id())
                    ->where('receiver_id', $_POST['receiver_id'])
                    ->first();
                    
            $res2 = DB::table('chat_room')
                        ->where('sender_id', $_POST['receiver_id'])
                        ->where('receiver_id', Auth::id())
                        ->first();
            if($res1 || $res2){
                $myroomid = (isset($res1->room_id)) ? $res1->room_id : $res2->room_id;
                    //insert into chat messages
                    if($mesageType && $mesageType != 4){
                        $insert['sender_id'] = Auth::id();
                        $insert['receiver_id'] = $_POST['receiver_id'];            
                        $insert['room_id'] = $myroomid;
                        $insert['message'] = $msg;
                        $insert['message_type'] = ($amt)? '1' : '3';                    
                        $res = DB::table('chat_messages')->insert($insert);
                    }
            }else{
                $myroomid = $this->create_roomId(Auth::id(),$_POST['receiver_id']);
                $insert['sender_id'] = Auth::id();
                $insert['receiver_id'] = $_POST['receiver_id'];            
                $insert['room_id'] = $myroomid;
                $insert['message'] = $msg;
                $insert['message_type'] = ($amt)? '1' : '3';
                $res = DB::table('chat_room')->insert($insert);
                    //insert into chat messages
                    if($mesageType && $mesageType != 4){
                        $res = DB::table('chat_messages')->insert($insert);
                    }
            }
            //print_r($myroomid); die;
            $roomdata = DB::table('chat_room')->where('room_id', $myroomid)->first();
            
            $userwallet = DB::table('user_wallet')->where('user_id', Auth::id())->first();
            
            if($roomdata->sender_id != Auth::id()){
                //echo Auth::id(); echo print_r($roomdata); die;
                if(isset($userwallet) && $userwallet->credits){
                    DB::table('chat_room')->where('room_id', $myroomid)->update(array("message"=>NULL));
                }
                //echo $roomid;                    
            }
            if($amt){                
                $updatdamt = ($userwallet->credits - $amt);
                if($updatdamt>=0 &&  ($userwallet->premiums <= time()) ){
                    DB::table('user_wallet')->where('user_id', Auth::id())->update(array("credits"=>$updatdamt));
                    echo $updatdamt;   
                }                
            }
            
            //send email
            $roomidarr = explode('_',$roomid);
            if($roomidarr[0] == Auth::id()){
                $recvid = $roomidarr[1];
            }else{
                $recvid = $roomidarr[0];
            }
            $sender = DB::table('users')                    
                    ->where('id', Auth::id())
                    ->first();
            $receiver = DB::table('users')                    
                    ->where('id', $recvid)
                    ->first();   

            $message = ucwords($sender->name).' send you a message.';
            $profile_image = (!empty($sender->profile_image)) ? $sender->profile_image : 'user-profile.jpg';
            $email_content = array(
                            //'receipent_email'   => $get_receiver->email,
                            'receipent_email'   => $receiver->email,
                            'subject'           => 'Your Flirtfull Chat Notification',
                            'first_name'        => $receiver->name,
                            'sender_id'        => $sender->id,
                            'sender_name'        => $sender->name,
                            'message'        => $message,
                            'greeting'          => 'Flirtfull',
                            'profile_image' => $profile_image
                        );
            $helper = new Helper;
            $email_response = $helper->sendMail(
                                    $email_content,
                                    'chat_notification'
                                );
            //send email
            
    }
    
    public function update_conversation1(Request $request){
        
            $roomid = $request->room_id;
            $amt = $request->amt;
            $msg = isset($request->msg) ? $request->msg : '1';
            
            $res1 = DB::table('chat_room')
                    ->where('sender_id', Auth::id())
                    ->where('receiver_id', $_POST['receiver_id'])
                    ->first();
                    
            $res2 = DB::table('chat_room')
                        ->where('sender_id', $_POST['receiver_id'])
                        ->where('receiver_id', Auth::id())
                        ->first();
            if($res1 || $res2){
                $myroomid = (isset($res1->room_id)) ? $res1->room_id : $res2->room_id;
                    //insert into chat message
                    $insert['sender_id'] = Auth::id();
                    $insert['receiver_id'] = $_POST['receiver_id'];            
                    $insert['room_id'] = $myroomid;
                    $insert['message'] = $msg;
                    $insert['message_type'] = 2;
                    $res = DB::table('chat_messages')->insert($insert);
            }else{
                $myroomid = $this->create_roomId(Auth::id(),$_POST['receiver_id']);
                $insert['sender_id'] = Auth::id();
                $insert['receiver_id'] = $_POST['receiver_id'];            
                $insert['room_id'] = $myroomid;
                $insert['message'] = $msg;
                $insert['message_type'] = 2;
                $res = DB::table('chat_room')->insert($insert);
                    //insert into chat message
                    $res = DB::table('chat_messages')->insert($insert);
            }
            //print_r($myroomid); die;
            $roomdata = DB::table('chat_room')->where('room_id', $myroomid)->first();
            
            $userwallet = DB::table('user_wallet')->where('user_id', Auth::id())->first();
            
            if($roomdata->sender_id != Auth::id()){
                //echo Auth::id(); echo print_r($roomdata); die;
                if(isset($userwallet) && $userwallet->credits){
                    DB::table('chat_room')->where('room_id', $myroomid)->update(array("message"=>NULL));
                }
                //echo $roomid;                    
            }
            
            if($amt){
                if($userwallet->diamonds){
                    $updatdamt = ($userwallet->diamonds - $amt);
                    if($updatdamt>=0 &&  ($userwallet->premiums <= time()) ){
                        DB::table('user_wallet')->where('user_id', Auth::id())->update(array("diamonds"=>$updatdamt));
                        echo json_encode(array('diamonds'=>$updatdamt));   
                    }
                }else{
                    $updatdamt = ($userwallet->credits - $amt);
                    if($updatdamt>=0 &&  ($userwallet->premiums <= time()) ){
                        DB::table('user_wallet')->where('user_id', Auth::id())->update(array("credits"=>$updatdamt));
                        $updatddiamonds = ($userwallet->diamonds + 2);
                        DB::table('user_wallet')->where('user_id', Auth::id())->update(array("diamonds"=>$updatddiamonds));
                        echo json_encode(array('credits'=>$updatdamt,'diamonds'=>$updatddiamonds));   
                    }
                }                                            
            }
            
            //send email
            $roomidarr = explode('_',$roomid);
            if($roomidarr[0] == Auth::id()){
                $recvid = $roomidarr[1];
            }else{
                $recvid = $roomidarr[0];
            }
            $sender = DB::table('users')                    
                    ->where('id', Auth::id())
                    ->first();
            $receiver = DB::table('users')                    
                    ->where('id', $recvid)
                    ->first();   

            $message = ucwords($sender->name).' send you a proposal.';
            $profile_image = (!empty($sender->profile_image)) ? $sender->profile_image : 'user-profile.jpg';
            $email_content = array(
                            //'receipent_email'   => $get_receiver->email,
                            'receipent_email'   => $receiver->email,
                            'subject'           => 'Your Flirtfull Chat Notification',
                            'first_name'        => $receiver->name,
                            'sender_id'        => $sender->id,
                            'sender_name'        => $sender->name,
                            'message'        => $message,
                            'greeting'          => 'Flirtfull',
                            'profile_image' => $profile_image
                        );
            $helper = new Helper;
            $email_response = $helper->sendMail(
                                    $email_content,
                                    'chat_notification_proposal'
                                );
            //send email
            
    }
    
    public function get_chat_contacts(Request $request){        
        $users = DB::table('users')
                    ->where('status', 1)
                    ->Where('name', 'like', '%' . Input::get('term') . '%')->get();
        $chatData = [];                    
        foreach($users as $row){
            $data['id'] = $row->id;
            $data['value'] = $row->name;
            $data['image'] = $row->profile_image;
            array_push($chatData, $data);
        }
        echo json_encode($chatData);
    }
    
    public function chat_request(Request $request){        
        $res1 = DB::table('chat_room')
                    ->where('sender_id', Auth::id())
                    ->where('receiver_id', $_POST['receiverId'])
                    ->first();
                    
        $res2 = DB::table('chat_room')
                    ->where('sender_id', $_POST['receiverId'])
                    ->where('receiver_id', Auth::id())
                    ->first();
        if($res1 || $res2){
            $roomid = (isset($res1->room_id)) ? $res1->room_id : $res2->room_id;
            return redirect()->back()->with('message', 'This member is already in your Chat box.');
        }else{
            $roomid = $this->create_roomId(Auth::id(),$_POST['receiverId']);
            $insert['sender_id'] = Auth::id();
            $insert['receiver_id'] = $_POST['receiverId'];            
            $insert['room_id'] = $roomid;
            $insert['message'] = $_POST['mesage'];
            $res = DB::table('chat_room')->insert($insert);
            
            $sender = DB::table('users')                    
                    ->where('id', Auth::id())
                    ->first();
            $receiver = DB::table('users')                    
                    ->where('id', $_POST['receiverId'])
                    ->first();        
            
                    $data1 = [
                    'senderId'=> Auth::id(),
                    'senderName'=> $sender->name,
                    'receiverId'=> $_POST['receiverId'],
                    'receiverName'=> $receiver->name,
                    'chatId'=> $roomid,
                    'fileName'=> '',
                    'type' => 'text',
                    'fileText' => '',
                    'message' => $_POST['mesage'],
                    'readStatus'=> false,
                    'answereStatus'=> false,
                    'senderImageUrl' => ($sender->profile_image) ? $sender->profile_image : 'user-profile.jpg',
                    'receiverImageUrl' => ($receiver->profile_image) ? $receiver->profile_image : 'user-profile.jpg'
                    ];
                
                

                $curl = curl_init();

                curl_setopt_array($curl, array(
                    CURLOPT_URL => "http://35.242.157.120:9090/insertChatRequest",
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => "",
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 30000,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => "POST",
                    CURLOPT_POSTFIELDS => json_encode($data1),
                    CURLOPT_HTTPHEADER => array(
                        // Set here requred headers
                        "content-type: application/json",
                        
                    ),
                ));

                $response = curl_exec($curl);

                $err = curl_error($curl);

                curl_close($curl);
            
            return redirect()->back()->with('message', 'Your request has been sent successfully');
        }                                    
    }
    
    
    public function chat_proposal(Request $request){        
        $userwallet = DB::table('user_wallet')->where('user_id', Auth::id())->first();
        
        if($userwallet->diamonds > 0 || $userwallet->credits > 0){
            if($userwallet->diamonds){
                DB::table('user_wallet')->where('user_id', Auth::id())->update(array('diamonds'=>($userwallet->diamonds-1))); 
            }else{
                DB::table('user_wallet')->where('user_id', Auth::id())->update(array('credits'=>($userwallet->credits-1),'diamonds'=>($userwallet->diamonds+2))); 
            }
            
            
                $res1 = DB::table('chat_room')
                            ->where('sender_id', Auth::id())
                            ->where('receiver_id', $_POST['receiverId'])
                            ->first();
                            
                $res2 = DB::table('chat_room')
                            ->where('sender_id', $_POST['receiverId'])
                            ->where('receiver_id', Auth::id())
                            ->first();
                if($res1 || $res2){
                    $roomid = (isset($res1->room_id)) ? $res1->room_id : $res2->room_id;
                                
                    $insert['message'] = 'Send a proposal';
                    $insert['attachment'] = $_POST['image'];
                    $insert['created_at'] = date('Y-m-d h:i:s');
                    $res = DB::table('chat_room')->where('room_id', $roomid)->update($insert);
                    
                    $sender = DB::table('users')                    
                            ->where('id', Auth::id())
                            ->first();
                    $receiver = DB::table('users')                    
                            ->where('id', $_POST['receiverId'])
                            ->first();        
                    
                            $data1 = [
                            'senderId'=> Auth::id(),
                            'senderName'=> $sender->name,
                            'receiverId'=> $_POST['receiverId'],
                            'receiverName'=> $receiver->name,
                            'chatId'=> $roomid,
                            'fileName'=> $_POST['image'],
                            'type' => 'file',
                            'fileText' => '',
                            'message' => 'send a proposal',
                            'readStatus'=> false,
                            'answereStatus'=> false,
                            'senderImageUrl' => ($sender->profile_image) ? $sender->profile_image : 'user-profile.jpg',
                            'receiverImageUrl' => ($receiver->profile_image) ? $receiver->profile_image : 'user-profile.jpg'
                            ];
                        
                        
        
                        $curl = curl_init();
        
                        curl_setopt_array($curl, array(
                            CURLOPT_URL => "http://35.242.157.120:9090/insertChatRequest",
                            CURLOPT_RETURNTRANSFER => true,
                            CURLOPT_ENCODING => "",
                            CURLOPT_MAXREDIRS => 10,
                            CURLOPT_TIMEOUT => 30000,
                            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                            CURLOPT_CUSTOMREQUEST => "POST",
                            CURLOPT_POSTFIELDS => json_encode($data1),
                            CURLOPT_HTTPHEADER => array(
                                // Set here requred headers
                                "content-type: application/json",
                                
                            ),
                        ));
        
                        $response = curl_exec($curl);
        
                        $err = curl_error($curl);
        
                        curl_close($curl);
                    
                    return redirect()->back()->with('message', 'Fantasy has been shared successfully');
                    //return redirect()->back()->with('message', 'This member is already in your Chat box.');
                }else{
                    $roomid = $this->create_roomId(Auth::id(),$_POST['receiverId']);
                    
                    $insert['sender_id'] = Auth::id();
                    $insert['receiver_id'] = $_POST['receiverId'];            
                    $insert['room_id'] = $roomid;
                    $insert['message'] = 'Send a proposal';
                    $insert['attachment'] = $_POST['image'];
                    $res = DB::table('chat_room')->insert($insert);
                    
                    $sender = DB::table('users')                    
                            ->where('id', Auth::id())
                            ->first();
                    $receiver = DB::table('users')                    
                            ->where('id', $_POST['receiverId'])
                            ->first();        
                    
                            $data1 = [
                            'senderId'=> Auth::id(),
                            'senderName'=> $sender->name,
                            'receiverId'=> $_POST['receiverId'],
                            'receiverName'=> $receiver->name,
                            'chatId'=> $roomid,
                            'fileName'=> $_POST['image'],
                            'type' => 'file',
                            'fileText' => '',
                            'message' => 'send a proposal',
                            'readStatus'=> false,
                            'answereStatus'=> false,
                            'senderImageUrl' => ($sender->profile_image) ? $sender->profile_image : 'user-profile.jpg',
                            'receiverImageUrl' => ($receiver->profile_image) ? $receiver->profile_image : 'user-profile.jpg'
                            ];
                        
                        
        
                        $curl = curl_init();
        
                        curl_setopt_array($curl, array(
                            CURLOPT_URL => "http://35.242.157.120:9090/insertChatRequest",
                            CURLOPT_RETURNTRANSFER => true,
                            CURLOPT_ENCODING => "",
                            CURLOPT_MAXREDIRS => 10,
                            CURLOPT_TIMEOUT => 30000,
                            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                            CURLOPT_CUSTOMREQUEST => "POST",
                            CURLOPT_POSTFIELDS => json_encode($data1),
                            CURLOPT_HTTPHEADER => array(
                                // Set here requred headers
                                "content-type: application/json",
                                
                            ),
                        ));
        
                        $response = curl_exec($curl);
        
                        $err = curl_error($curl);
        
                        curl_close($curl);
                    
                    return redirect()->back()->with('message', 'Fantasy has been shared successfully');
                }
        }else{
            return redirect()->back()->with('message', 'You have not enough crdit or diamond.');
        }
    }
    
    
    public function create_roomId($senderid,$receiverid){
        $roomid = $senderid.rand(100000,999999).$receiverid;
        $res2 = DB::table('chat_room')
                    ->where('room_id', $roomid)                    
                    ->get();
        if(!count($res2)){
            return $roomid;
        }else{
            $this->create_roomId($senderid,$receiverid);
        }
    }
    
    public function about_detail_update(Request $request)
    {
        $user_id = $request->user_id;
        $address = $request->address;
        $city = $request->city;
        $country = $request->country;
        
        $work_as = $request->work_as;
        $education = $request->education;
        $lang = $request->lang;
        $relationship = $request->relationship;
        $kids = $request->kids;
        $smoke = $request->smoke;
        $drink = $request->drink;
        $height = $request->height;

        /*address update*/
        $update_address = array(
            'address' => $address,
            'city' => $city,
            'country' => $country
        );

        $update_user_address = DB::table('users')->where('id', $user_id)->update($update_address);
        /*address update*/


        /*lang insert or update*/

        $languages = $request->lang;

        $update_languages = array(
            'languages' => $languages,
            'updated_at' => date('Y-m-d H:i:s')
        );

        $insert_languages = array(
            'user_id' => $user_id,
            'languages' => $languages,
            'updated_at' => date('Y-m-d H:i:s')
        );

        $get_languages = DB::table('user_languages')->where('user_id','=',$user_id)->first();

        if(!empty($get_languages))
        {
            $update_lang = DB::table('user_languages')->where('user_id', $user_id)->update($update_languages);
        }
        else{
            $update_lang = DB::table('user_languages')->insertGetId($insert_languages);
        }

        /*lang insert or update*/


        /*about detail update*/
        $update_about = array(
            'work_as' => $work_as,
            'education' => $education,
            'relationship' => $relationship,
            'kids' => $kids,
            'smoke' => $smoke,
            'drink' => $drink,
            'height' => $height,
            'updated_at' => date('Y-m-d H:i:s')
        );

        $update_user = DB::table('user_details')->where('user_id', $user_id)->update($update_about);
        /*about detail update*/
        
        if($update_user)
        {
            echo 'success';
        }
        else{
            echo 'error';
        }
    }

    /*public function ajax_profile_image(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'profile_img' => 'image',
            ],
            [
                'profile_img.image' => 'The file must be an image (jpeg, png, bmp, gif, or svg)'
            ]);
        if ($validator->fails())
            return array(
                'fail' => true,
                'errors' => $validator->errors()
            );

        $image = $request->file('profile_img');

        $extension = $image->getClientOriginalExtension();
       
        $filename = time() . '.' . $extension;

        if(!file_exists(public_path('images/profile_image').'/'.$filename)) 
        {
            if(!file_exists(public_path('thumbnail').'/'.$filename)) 
            {
                $destinationPath = public_path('thumbnail');
                $img = Image::make($image->getRealPath());
                //print_r($img);die;
                $img->resize(300, 300, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($destinationPath.'/'.$filename);
            }
            else{
                return array(
                    'fail' => true,
                    'errors' => $validator->errors()
                );
            }

            $destinationPath = public_path('images/profile_image');
            $image->move($destinationPath, $filename);

            $update_profile_image = array(
            'profile_image' => $filename,
            'profile_image_status' => 1,
            'updated_at' => date('Y-m-d H:i:s')
            );

            
            $update_user = DB::table('users')->where('id', $request->user_id)->update($update_profile_image);
            
        }
        else{
            return array(
                'fail' => true,
                'errors' => $validator->errors()
            );
        }

        return $filename;
    }*/

    public function ajax_profile_image(Request $request)
    {
        //for thumbnail
        $data_org = $request->original_profile;

        list($type_org, $data_org) = explode(';', $data_org);
        list(, $data_org)      = explode(',', $data_org);


        $data_org = base64_decode($data_org);
        $image_name_org= time().'.jpg';
        $path_org = public_path() . "/images/profile_image/" . $image_name_org;


        file_put_contents($path_org, $data_org);
        //for thumbnail


        //for thumbnail
        $data = $request->original_profile;
        

        list($type, $data) = explode(';', $data);
        list(, $data)      = explode(',', $data);


        $data = base64_decode($data);
        $image_name= time().'.jpg';
        $path = public_path() . "/thumbnail/" . $image_name;


        file_put_contents($path, $data);
        //for thumbnail

        $update_profile_image = array(
        'profile_image' => $image_name,
        'profile_image_status' => 1,
        'updated_at' => date('Y-m-d H:i:s')
        );

        
        $update_user = DB::table('users')->where('id', $request->user_id)->update($update_profile_image);

        return response()->json(['success'=>true,'img' => $image_name]);
    }
    
    public function cover_image_upload(Request $request)
    {
        $data = $request->cover_image;

        list($type, $data) = explode(';', $data);
        list(, $data)      = explode(',', $data);


        $data = base64_decode($data);
        $image_name= time().'.jpg';
        $path = public_path() . "/images/cover_img/" . $image_name;


        file_put_contents($path, $data);

        $update_cover_image = array(
            'cover_image' => $image_name,
            'cover_image_status' => 1,
            'updated_at' => date('Y-m-d H:i:s')
        );

        
        $update_user = DB::table('users')->where('id', $request->user_id)->update($update_cover_image);

        return response()->json(['success'=>true,'img' => $image_name]);

        // echo '<img src="'.$imageName.'" class="img-thumbnail" />';
    }

    public function gallery_image_upload(Request $request)
    {
        $data = $request->gallery_image;

        list($type, $data) = explode(';', $data);
        list(, $data)      = explode(',', $data);


        $data = base64_decode($data);
        $image_name= time().'.jpg';
        $path = public_path() . "/images/profile_image/" . $image_name;


        file_put_contents($path, $data);

        $gallery_image = array(
            'user_id' => $request->user_id,
            'gallery_image' => $image_name,
            'is_private' => $request->is_private,
            'status' => 1,
            'updated_at' => date('Y-m-d H:i:s')
        );

        
        $inser_images = DB::table('gallery_images')->insert($gallery_image);

        return response()->json(['success'=>true,'img' => $image_name]);

        // echo '<img src="'.$imageName.'" class="img-thumbnail" />';
    }

    public function gallery_video_upload(Request $request)
    {
        $image = $request->file('upload_videos');
        
        /*$validator = Validator::make($request->all(),
            [
                'video_file'  => 'mimes:mp4 | max:20000'
            ]
           );
        if ($validator->fails())
            return array(
                'fail' => true,
                'errors' => $validator->errors()
            );*/

            if (false !== mb_strpos($image->getMimeType(), "video")) {
               //echo('video');
            } else {
               return array(
                    'fail' => true,
                    'errors' => array('video_file'=> 'Please select valid .mp4 format !!')
                );
            }



        $extension = $image->getClientOriginalExtension();
       
        $filename = time() . '.' . $extension;

        if(!file_exists(public_path('gallery_videos').'/'.$filename)) 
        {
            /*if(!file_exists(public_path('thumbnail').'/'.$filename)) 
            {
                $destinationPath = public_path('thumbnail');
                $img = Image::make($image->getRealPath());
                //print_r($img);die;
                $img->resize(300, 300, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($destinationPath.'/'.$filename);
            }
            else{
                return array(
                    'fail' => true,
                    'errors' => $validator->errors()
                );
            }*/

            $destinationPath = public_path('gallery_videos/');
            $image->move($destinationPath, $filename);

            $insert_video = array(
            'user_id' => $request->user_id,
            'gallery_video' => $filename,
            'status' => 1,
            'updated_at' => date('Y-m-d H:i:s')
            );

            
            $gallery_video = DB::table('gallery_videos')->insert($insert_video);
            
        }
        else{
            return array(
                'fail' => true,
                'errors' => $validator->errors()
            );
        }

        return $filename;

    }
    
    public function chat_file(Request $request)
    {        
        $data = $request->get('base64');
        //print_r($data); die;

        list($type, $data) = explode(';', $data);
        list(, $data)      = explode(',', $data);


        $dataa = base64_decode($data);
        $image_name= 'chat_'.time().'.'.$request->get('extension');
        $path = public_path() . "/images/profile_image/" . $image_name;


        file_put_contents($path, $dataa);        
        echo $image_name;
        // echo '<img src="'.$imageName.'" class="img-thumbnail" />';
    }

    public function gallery_image_delete(Request $request)
    {
        $image_id = $request->image_id;

        $gallery_image_delete = DB::table('gallery_images')->where('id',$image_id)->delete();
        echo ($gallery_image_delete) ? true : false;

    }

    public function gallery_video_delete(Request $request)
    {
        $video_id = $request->video_id;

        $gallery_video_delete = DB::table('gallery_videos')->where('id',$video_id)->delete();
        echo ($gallery_video_delete) ? true : false;

    }

    public function profile_like(Request $request)
    {
        //print_r($_POST);die;
       $res1 = DB::table('chat_room')
                    ->where('sender_id', Auth::id())
                    ->where('receiver_id', $_POST['receiverId'])
                    ->first();
                    
        $res2 = DB::table('chat_room')
                    ->where('sender_id', $_POST['receiverId'])
                    ->where('receiver_id', Auth::id())
                    ->first();
        if($res1 || $res2){
			echo 'exist';
            $roomid = (isset($res1->room_id)) ? $res1->room_id : $res2->room_id;
            //return redirect()->back()->with('message', 'This member is already in your Chat box.');
            DB::table('chat_room')->where('room_id',$roomid)->update(array('status'=>'1','like_receiver'=>'1'));
            
            

            $sender = DB::table('users')                    
                    ->where('id', Auth::id())
                    ->first();
            $receiver = DB::table('users')                    
                    ->where('id', $_POST['receiverId'])
                    ->first();   

            $message = ucwords($sender->name).' also make favorited you.';
                // insert in chat message                           
                $insert['sender_id'] = Auth::id();
                $insert['receiver_id'] = $_POST['receiverId'];            
                $insert['room_id'] = $roomid;
                $insert['message'] = $message;
                $insert['message_type'] = 4;
                $insert['status'] = '1';
                $insert['like_receiver'] = '1';            
                $res = DB::table('chat_messages')->insert($insert);

            $profile_image = (!empty($sender->profile_image)) ? $sender->profile_image : 'user-profile.jpg';
            //send email
            $email_content = array(
                            //'receipent_email'   => $get_receiver->email,
                            'receipent_email'   => $receiver->email,
                            'subject'           => 'Your Flirtfull Chat Notification',
                            'first_name'        => $receiver->name,
                            'sender_id'        => $sender->id,
                            'sender_name'        => $sender->name,
                            'message'        => $message,
                            'greeting'          => 'Flirtfull',
                            'profile_image' => $profile_image
                            
                        );
            $helper = new Helper;
            $email_response = $helper->sendMail(
                                    $email_content,
                                    'chat_notification'
                                );
            //send email
            
            // insert in activity
                if (Auth::check()){                    
                        $activity['from_id'] = Auth::id();
                        $activity['to_id'] = $_POST['receiverId'];
                        $activity['message'] = Auth::user()->name.' has make favorited your profile';
                        $activity['type'] = 3;
                        $activity['payment_type'] = NULL;
                        DB::table('activity')->insertGetId($activity);                    
                }

            //echo 'exist';
        }else{
			echo 'success'; 
            $sender = DB::table('users')                    
                    ->where('id', Auth::id())
                    ->first();
            $receiver = DB::table('users')                    
                    ->where('id', $_POST['receiverId'])
                    ->first();   

            $message = ucwords($sender->name).' favorites you.';     

            $roomid = $this->create_roomId(Auth::id(),$_POST['receiverId']);
            $insert['sender_id'] = Auth::id();
            $insert['receiver_id'] = $_POST['receiverId'];            
            $insert['room_id'] = $roomid;
            $insert['message'] = $message;
            $insert['message_type'] = 4;
            $insert['status'] = '1';
            $insert['like_sender'] = '1';
            $res = DB::table('chat_room')->insert($insert);
                //insert into chat messages
                $res = DB::table('chat_messages')->insert($insert);

            $profile_image = (!empty($sender->profile_image)) ? $sender->profile_image : 'user-profile.jpg';
            //send email
            $email_content = array(
                            //'receipent_email'   => $get_receiver->email,
                            'receipent_email'   => $receiver->email,
                            'subject'           => 'Your Flirtfull Chat Notification',
                            'first_name'        => $receiver->name,
                            'sender_id'        => $sender->id,
                            'sender_name'        => $sender->name,
                            'message'        => $message,
                            'greeting'          => 'Flirtfull',
                            'profile_image' => $profile_image
                        );
            $helper = new Helper;
            $email_response = $helper->sendMail(
                                    $email_content,
                                    'chat_notification'
                                );
            //send email
            
            if(!empty($sender->profile_image))
            {
                $sender->profile_image = $sender->profile_image;
            }
            else{
                $sender->profile_image = 'user-profile.jpg';
            }

            if(!empty($receiver->profile_image))
            {
                $receiver->profile_image = $receiver->profile_image;
            }
            else{
                $receiver->profile_image = 'user-profile.jpg';
            }
            
                                                
                
                // insert in activity
                if (Auth::check()){                    
                        $activity['from_id'] = Auth::id();
                        $activity['to_id'] = $_POST['receiverId'];
                        $activity['message'] = Auth::user()->name.' has make favorited your profile';
                        $activity['type'] = 3;
                        $activity['payment_type'] = NULL;
                        DB::table('activity')->insertGetId($activity);                    
                }
            
            //echo 'success';
            //return redirect()->back()->with('message', 'Your request has been sent successfully');
        }  
    }
    
    public function likes(Request $request)
    {
       //print_r($_POST);die;
       $sender_id = $request->get('sender_id');
       $receiver_id = $request->get('receiver_id');
       $type = $request->get('type');
       
       
       $likes['sender_id'] = $sender_id;
       $likes['receiver_id'] = $receiver_id;
       $likes['status'] = 0;
       $likes['type'] = $type;
       DB::table('likes')->insert($likes);
       
            //send email
            $sender = DB::table('users')                    
                    ->where('id', $sender_id)
                    ->first();
            $receiver = DB::table('users')                    
                    ->where('id', $receiver_id)
                    ->first();   

            $message = ucwords($sender->name).' likes your profile.';
            $profile_image = (!empty($sender->profile_image)) ? $sender->profile_image : 'user-profile.jpg';
            $email_content = array(
                            //'receipent_email'   => $get_receiver->email,
                            'receipent_email'   => $receiver->email,
                            'subject'           => 'Your Flirtfull Chat Notification',
                            'first_name'        => $receiver->name,
                            'sender_id'        => $sender->id,
                            'sender_name'        => $sender->name,
                            'message'        => $message,
                            'greeting'          => 'Flirtfull',
                            'profile_image' => $profile_image
                        );
            $helper = new Helper;
            $email_response = $helper->sendMail(
                                    $email_content,
                                    'chat_notification'
                                );
            //send email
       
       $user = DB::table('users')->where('id', '=',$receiver_id)->first();
       
       if($user->fake){
            $rand = rand(1,4);
            
            $is_chat_like = DB::table('chat_room')->where('sender_id', '=',$receiver_id)->where('receiver_id', '=',$sender_id)->where('status','1')->count();
            if($is_chat_like){
                $is_already_like = 1;
            }else{
                if($rand == 3){
                    $likes['sender_id'] = $receiver_id;
                    $likes['receiver_id'] = $sender_id;
                    $likes['status'] = 0;
                    $likes['type'] = $type;
                    DB::table('likes')->insert($likes);
                    
                    $is_already_like = 1;  
                }else{
                    $is_already_like = 0;  
                }   
            }                        
       }else{
            $is_already_like = DB::table('likes')->where('sender_id', '=',$receiver_id)->where('receiver_id', '=',$sender_id)->count(); 
       }
       
       
       $random_user = $this->random_user();
       
       
       if($is_already_like && ($type == 1)){
            echo json_encode(array('1',$random_user));
       }else{
            echo json_encode(array('0',$random_user));
       }                     
    }
    
    //public function random_user(){
    //   $iam = DB::table('users')->where('id', '=',Auth::id())->where('status', '=',1)->where('is_deleted', '=',0)->first(); 
    //   $random_user = DB::table('users')->where('gender', '=',$iam->seeking_val)->where('id', '!=',Auth::id())->where('status', '=',1)->where('is_deleted', '=',0)->inRandomOrder()->first();
    //   $alreayliked = DB::table('likes')->where('sender_id', '=',Auth::id())->where('receiver_id', '=',$random_user->id)->count();       
    //   if($alreayliked){
    //        $this->random_user();
    //   }
    //   //if(!$random_user->profile_image){ $this->random_user(); }
    //   return $random_user;
    //}
    
    public function random_user($count=Null){    
       $iam = DB::table('users')->where('id', '=',Auth::id())->where('status', '=',1)->where('is_deleted', '=',0)->first(); 

       $seeking_val = (isset(Auth::user()->seeking_val)) ? Auth::user()->seeking_val : 2;
                          
       $random_user = DB::table('users')->where('id', '!=',Auth::id())->where('status', '=',1)->where('gender', '=',$seeking_val)->where('is_deleted', '=',0)->inRandomOrder()->first();
       
       $alreayliked = DB::table('likes')->where('sender_id', '=',Auth::id())->where('receiver_id', '=',$random_user->id)->count();
       //print_r($alreayliked); die;
       if($alreayliked > 0){                    
            $this->random_user();            
       }
       return $random_user;
    }
    
    public function send_like_message(){
        $iam = DB::table('users')->where('id', '=',Auth::id())->where('status', '=',1)->where('is_deleted', '=',0)->first();
        $user = DB::table('users')->where('id', '=',$_POST['receiver_id'])->where('status', '=',1)->where('is_deleted', '=',0)->inRandomOrder()->first();        
        //send email
            //$email_content = array(
            //                //'receipent_email'   => $get_receiver->email,
            //                'receipent_email'   => $user->email,
            //                'subject'           => 'Your Flirtfull Like Notification',
            //                'first_name'        => $user->name,
            //                'sender_name'        => $iam->name,
            //                'message'        => $_POST['message'],
            //                'greeting'          => 'Flirtfull',
            //                'profile_image' => $user->profile_image
            //                
            //            );
            //$helper = new Helper;
            //$email_response = $helper->sendMail(
            //                        $email_content,
            //                        'chat_notification'
            //                    );
            //print_r($_POST);
        
        //send notification in chat
        $res1 = DB::table('chat_room')
                    ->where('sender_id', Auth::id())
                    ->where('receiver_id', $_POST['receiver_id'])
                    ->first();
                    
        $res2 = DB::table('chat_room')
                    ->where('sender_id', $_POST['receiver_id'])
                    ->where('receiver_id', Auth::id())
                    ->first();
        if($res1 || $res2){
            $roomid = (isset($res1->room_id)) ? $res1->room_id : $res2->room_id;
            //return redirect()->back()->with('message', 'This member is already in your Chat box.');
            DB::table('chat_room')->where('room_id',$roomid)->update(array('message'=>'matched'));

            $sender = DB::table('users')                    
                    ->where('id', Auth::id())
                    ->first();
            $receiver = DB::table('users')                    
                    ->where('id', $_POST['receiver_id'])
                    ->first();   

            $message = ucwords($sender->name).' matched your profile.';

            $profile_image = (!empty($sender->profile_image)) ? $sender->profile_image : 'user-profile.jpg';
            //send email
            $email_content = array(
                            //'receipent_email'   => $get_receiver->email,
                            'receipent_email'   => $receiver->email,
                            'subject'           => 'Your Flirtfull Chat Notification',
                            'first_name'        => $receiver->name,
                            'sender_id'        => $sender->id,
                            'sender_name'        => $sender->name,
                            'message'        => $_POST['message'],
                            'greeting'          => 'Flirtfull',
                            'profile_image' => $profile_image
                            
                        );
            $helper = new Helper;
            $email_response = $helper->sendMail(
                                    $email_content,
                                    'chat_notification'
                                );
            //send email

            echo json_encode(array('exist'));
        }else{

            $sender = DB::table('users')                    
                    ->where('id', Auth::id())
                    ->first();
            $receiver = DB::table('users')                    
                    ->where('id', $_POST['receiver_id'])
                    ->first();   

            $message = ucwords($sender->name).' matched your profile.';     

            $roomid = $this->create_roomId(Auth::id(),$_POST['receiver_id']);
            $insert['sender_id'] = Auth::id();
            $insert['receiver_id'] = $_POST['receiver_id'];            
            $insert['room_id'] = $roomid;
            $insert['message'] = $_POST['message'];                    
            $res = DB::table('chat_room')->insert($insert);

            $profile_image = (!empty($sender->profile_image)) ? $sender->profile_image : 'user-profile.jpg';
            //send email
            $email_content = array(
                            //'receipent_email'   => $get_receiver->email,
                            'receipent_email'   => $receiver->email,
                            'subject'           => 'Your Flirtfull Chat Notification',
                            'first_name'        => $receiver->name,
                            'sender_id'        => $sender->id,
                            'sender_name'        => $sender->name,
                            'message'        => $_POST['message'],
                            'greeting'          => 'Flirtfull',
                            'profile_image' => $profile_image
                        );
            $helper = new Helper;
            $email_response = $helper->sendMail(
                                    $email_content,
                                    'chat_notification'
                                );
            //send email
            
            if(!empty($sender->profile_image))
            {
                $sender->profile_image = $sender->profile_image;
            }
            else{
                $sender->profile_image = 'user-profile.jpg';
            }

            if(!empty($receiver->profile_image))
            {
                $receiver->profile_image = $receiver->profile_image;
            }
            else{
                $receiver->profile_image = 'user-profile.jpg';
            }
            
                    $data1 = [
                    'senderId'=> Auth::id(),
                    'senderName'=> $sender->name,
                    'receiverId'=> $_POST['receiver_id'],
                    'receiverName'=> $receiver->name,
                    'chatId'=> $roomid,
                    'fileName'=> '',
                    'type' => 'text',
                    'fileText' => '',
                    'message' => $_POST['message'],
                    'readStatus'=> false,
                    'answereStatus'=> false,
                    'senderImageUrl' => $sender->profile_image,
                    'receiverImageUrl' => $receiver->profile_image
                    ];
                
                

                //$curl = curl_init();
                //
                //curl_setopt_array($curl, array(
                //    CURLOPT_URL => "http://35.242.157.120:9090/insertChatRequest",
                //    CURLOPT_RETURNTRANSFER => true,
                //    CURLOPT_ENCODING => "",
                //    CURLOPT_MAXREDIRS => 10,
                //    CURLOPT_TIMEOUT => 30000,
                //    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                //    CURLOPT_CUSTOMREQUEST => "POST",
                //    CURLOPT_POSTFIELDS => json_encode($data1),
                //    CURLOPT_HTTPHEADER => array(
                //        // Set here requred headers
                //        "content-type: application/json",
                //        
                //    ),
                //));
                //
                //$response = curl_exec($curl);
                //
                //$err = curl_error($curl);
                //
                //curl_close($curl);                                
            
            echo json_encode(array('success'));
            //return redirect()->back()->with('message', 'Your request has been sent successfully');
        }
    }
    
    
    public function add_diamonds(Request $request){
        $mywallet = DB::table('user_wallet')->where('user_id', '=',Auth::id())->first();
        if($mywallet->credits >= ($_POST['diamonds']/3)){
            $available_credits = ($mywallet->credits - ($_POST['diamonds']/3));
            $available_diamonds = ($_POST['diamonds']) + $mywallet->diamonds;
            $wallet['credits'] = $available_credits;
            $wallet['diamonds'] = $available_diamonds;
            DB::table('user_wallet')->where('user_id', Auth::id())->update($wallet);
            return redirect()->back()->with('add_diamonds', 'Diamonds added to your wallet.');            
        }else{            
            return redirect()->back()->with('add_diamonds', 'You have not sufficient credits to purchase these diamonds.');  
        }
        print_r($_POST);
        
    }
    
    public function fake_chat(Request $request)
    {                
        $receiver_id = $request->get('receiver_id');
        //DB::table('chat_room')->where('receiver_id', Auth::user()->id)->where('sender_id', $receiver_id)->update(array('notification_status'=>'0'));
        
        //getting proposals
        $proposal_category = DB::table('proposals_category')->where('is_deleted',0)->get();        
        
        return view('partial.fake_chat',compact('fakechatrequest','receiver_id','proposal_category',''));
    }
    
    public function get_proposal(Request $request){
        //print_r($request->all());
        $proposal_images = DB::table('proposals_images')->select('image')->where('category_id',$request->get('Id'))->where('status',1)->where('is_deleted',0)->get();
        $htm = "";
        foreach($proposal_images as $row){
            $htm .= '<div class="send_pro_img">'.
                '<img src="'.asset("public/images/profile_image/".$row->image).'" class="img-responsive" onClick="sendMsg1(\''.asset("public/images/profile_image/".$row->image).'\');">'.
            '</div>';
        }
        return $htm;
    }
    
    public function get_emoji(Request $request){
        $htm = '';
        for($i=1;$i<14;$i++){
            $htm .= '<li><img class="loading_emoji" src="'.asset('/public/thumbnail/').'/loading_spinner.gif" height="38px;" onclick="set_smiley(\'chat'.$i.'.png\',\'credits\',\'0\');" onload="this.src=\''.asset('/public/thumbnail/').'/chat'.$i.'.png\'"></li>';
        }
        
        for($i=1;$i<102;$i++){
			
            $htm .= '<li><img class="loading_emoji" src="'.asset('/public/thumbnail').'/loading_spinner.gif" height="38px;" onclick="set_smiley(\''.$i.'.png\',\'credits\',\'0\');" onload="this.src=\''.asset('/public/thumbnail/').'/'.$i.'.png\'"></li>';
        }                               
        
        return $htm;
    }

}