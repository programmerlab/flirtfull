<?php if($paginator->hasPages()): ?>
    <nav class="pagination is-centered">
        <ul class="pagination-list pagination">
            <?php if($paginator->onFirstPage()): ?>
            <a class="prev_btn disable-btn" disabled>Prev</a>
            <?php else: ?>
            <a href="<?php echo e($paginator->previousPageUrl()); ?>" rel="prev" class="prev_btn">Prev</a>
            <?php endif; ?>
            
            <?php $__currentLoopData = $elements; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $element): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                
                <?php if(is_string($element)): ?>
                    <!-- <li><span class="pagination-ellipsis"><span><?php echo e($element); ?></span></span></li> -->
                <?php endif; ?>

                
                <?php if(is_array($element)): ?>
                    <?php $__currentLoopData = $element; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $page => $url): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                        <?php if($paginator->currentPage() >= 9): ?>
                            <?php if($page != 1 && $page != 2 && $page != $paginator->lastPage() && $page != ($paginator->lastPage() -1) ): ?>
                                <?php if($page == $paginator->currentPage()): ?>
                                    <li><a class="pagination-link is-current active"><?php echo e($page); ?></a></li>
                                <?php else: ?>
                                    <li><a href="<?php echo e($url); ?>" class="pagination-link"><?php echo e($page); ?></a></li>
                                <?php endif; ?>
                            <?php endif; ?>    
                        <?php else: ?>
                            <?php if($page != $paginator->lastPage() && $page != ($paginator->lastPage() -1) ): ?>
                                <?php if($page == $paginator->currentPage()): ?>
                                    <li><a class="pagination-link is-current avtive"><?php echo e($page); ?></a></li>
                                <?php else: ?>
                                    <li><a href="<?php echo e($url); ?>" class="pagination-link"><?php echo e($page); ?></a></li>
                                <?php endif; ?>
                            <?php endif; ?>        
                        <?php endif; ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php endif; ?>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <?php if($paginator->hasMorePages()): ?>
            <a class="next_btn" href="<?php echo e($paginator->nextPageUrl()); ?>" rel="next">Next</a>
            <?php else: ?>
            <a class="next_btn disable-btn" disabled>Next</a>
            <?php endif; ?>
        </ul>
    </nav>
<?php endif; ?>