<?php

namespace App\Http\Controllers;

require_once './vendor/autoload.php';
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use Validator;
use Session;
use Hash;
use Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use App\Helpers\Helper as Helper;
use App\User;
use DateTime;

//use Mollie\Api\MollieApiClient;
class HomeTestController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct(Request $request)
    {        
        error_reporting(env('ERROR_SHOW'));
        ini_set('display_errors', env('ERROR_SHOW'));
        $this->middleware('web');
        header("cache-Control: no-store, no-cache, must-revalidate");
        header("cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");

        if(Auth::check()){           
            //DB::table('users')->where("id",Auth::user()->id)->update(array("is_login"=>"1","login_time"=>time()));
        }
    
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
   
    public function index(Request $request)
    {
        
		$reslt = '0';
        if(isset($_GET['partner']) && ($_GET['partner'] != '')){            
            $reslt = $this->signup();
		}
		
		
		$helper = new Helper();
        //$helper->test_mail();
        if(Auth::check())
        {                                    
            return redirect('search');
			//return redirect('search')->with( ['msg' => $reslt] );
        }
        else{ 
   //          $countries = DB::table('countries')->where('status',1)->get();            
			
			// $most_popular = DB::table('users')->select('users.*','user_details.*')->leftJoin('user_details', 'user_details.user_id', '=', 'users.id')->whereBetween('users.id',array(1,15))->get();

			// //echo "<pre>"; print_r($most_popular);die;
			
   //          $all_interests = Helper::all_data('interests',['status'=>1]);

   //          $all_languages = Helper::all_data('languages',['status'=>1]);

            return view('home',compact('most_popular','countries','all_interests','all_languages','reslt'));
        }
    }


	public function home_test(Request $request)
    {
        
		$reslt = '0';
        if(isset($_GET['partner']) && ($_GET['partner'] != '')){
            $reslt = $this->signup();
		}
		
		
		$helper = new Helper();
        //$helper->test_mail();
        if(Auth::check())
        {                                    
            return redirect('search');
			//return redirect('search')->with( ['msg' => $reslt] );
        }
        else{ 
            $countries = DB::table('countries')->where('status',1)->get();            
			
			$most_popular = DB::table('users')->select('users.*','user_details.*')->leftJoin('user_details', 'user_details.user_id', '=', 'users.id')->whereBetween('users.id',array(1,15))->get();

			//echo "<pre>"; print_r($most_popular);die;
			
            $all_interests = Helper::all_data('interests',['status'=>1]);

            $all_languages = Helper::all_data('languages',['status'=>1]);

            return view('home_test',compact('most_popular','countries','all_interests','all_languages','reslt'));
        }
    }
    
    
	public function post_back(Request $request)
    { 
		
		$name		= Input::get('name');
		$email		= Input::get('email');
		$password	= Input::get('password');
		$phone		= Input::get('phone');
		$gender		= Input::get('gender');
		$dob		= Input::get('dob');
		$country	= Input::get('country');
		$state		= Input::get('region');
		$city		= Input::get('city');
		$partner	= Input::get('partner_id');
		$address	= Input::get('address');
		$term_accept= Input::get('termsaccept');

		if(User::where('email', '=', Input::get('email'))->count() > 0) 
		{
			echo 'Email entered is already exist';
			exit();
		}
		else
		{		
			/*
			**CHECK  VALIDATION   //
			*/
			
			// **NAME
			if( empty($name) ){ echo "Name should not be empty"; exit(); }
			
			// **EMAIL
			if( empty($email) ){ echo "Email should not be empty"; exit(); }		
			if(filter_var($email, FILTER_VALIDATE_EMAIL) == false){ echo 'Email is wrong'; exit(); }	
	
			// **PASSWORD
			if( empty($password) ){ echo "Password should not be empty"; exit(); }
			if (strlen($password)<4){ echo "password length must be up to 4 character"; exit(); }
			
			// **DATE OF BIRTH
			if( empty($dob) ){ echo "Date of birth should not be empty"; exit(); }			
			
			// **GENDER		/* 1 - male   2 - female  */
			if( $gender == 1 ){ $seeking_val = 2; }
			elseif( $gender == 2 ){ $seeking_val = 1; }else{ echo 'gender should be 1 or 2 or not to be empty'; exit(); }
			
			// **COUNTRY
			if(empty($country)){ echo 'country should not be empty'; exit(); }
			$check_country = DB::table('countries')->select('countries.id')->where('countries.name','=',$country)->first();
			if(empty($check_country)){ echo 'country is not here'; exit(); }
			else{ $country_id = $check_country->id; }
			
			// **REGION
			//$check_region = DB::table('states')->select('states.name')->where('states.id','=',$country_id)->first();
			//if(empty($check_country)){ echo 'Region is not here'; exit(); }
			
			// **PARTNER ID
			$partners = DB::table('partner')->select('partner.id')->where('partner.partnerid','=',$partner)->first();
			if(empty($partners)){ echo 'partner id is wrong'; exit(); }
			
			// **TERMS AND CONDITIONS ACCEPT
			if(empty($term_accept)){ echo 'Terms And Conditions should be accept'; exit(); }

			//echo 'hurray!!!! ... all correct'; die;
			
			$from = new DateTime($dob);
            $to   = new DateTime('today');
            $age = $from->diff($to)->y;
		
			$users = array(
                    'name'			=> $name,
                    'email'			=> $email,
                    'password'		=> bcrypt($password),
                    'phone'			=> $phone,
                    'gender'		=> $gender,
                    'dob'			=> date('Y-m-d', strtotime($dob)),
                    'age'			=> $age,  
                    'seeking_val'	=> $seeking_val,  
                    'country'		=> $country,
                    'state'			=> $state,
                    'city'			=> $city,
                    'address'		=> $address,
                    'is_update'		=> 0,
                    'is_login'		=> 0,
                    'last_login'	=> date('Y-m-d H:i:s'),
                    'status' 		=> 1,
                    'fake'			=> 0,
                    'profile_created_at'	=> date('Y-m-d'),
                    'profile_activated_at'	=> date('Y-m-d'),
                    'updated_at'	=> date('Y-m-d H:i:s'),
                    'partner_id'	=> $partners->id
                );
            //print_r($users); die;
            //echo "success"; die;
			$user_id = DB::table('users')->insertGetId($users);
			
			//USER DETAILS
			//1 male 2 female
			if( $gender == 1 ){ $seeking = 'Women'; }
			if( $gender == 2 ){ $seeking = 'Man'; }
			
			$user_details = array(
                    'user_id' 		=> $user_id, 
                    'seeking' 		=> $seeking,
                    'about_me' 		=> NULL,
                    'about_partner' => NULL,
                    'work_as' 		=> NULL,
                    'education' 	=> NULL,
                    'relationship' 	=> 1,
                    'kids' 			=> 1,
                    'smoke' 		=> 1,
                    'drink' 		=> 1,
                    'height' 		=> NULL,
                    'updated_at' 	=> date('Y-m-d H:i:s')
                );
            DB::table('user_details')->insertGetId($user_details);
			
			//INTEREST
			$user_interests = array(
                    'user_id'		=> $user_id, 
                    'interests' 	=> NULL,
                    'updated_at' 	=> date('Y-m-d H:i:s')
                );
            DB::table('user_interests')->insertGetId($user_interests);

			//LANGUAGES
			$user_languages = array(
                    'user_id'		=> $user_id, 
                    'languages' 	=> NULL,
                    'updated_at' 	=> date('Y-m-d H:i:s')
                );
           DB::table('user_languages')->insertGetId($user_languages);
			
			if($user_id)
			{
				echo "success";
                //$user_id.' User created successfully :) ';
			}
		}
	}
	public function post_back_doc(Request $request)
    {
        $countries = DB::table('countries')->select('countries.*')->where('countries.status','=',1)->get();

		return view('post_back_doc',compact('countries'));
    }
	
	
	public function signup()
    {				
            $profile_image = ($_GET['gender'] == 2) ? 'lady_icon.png' : 'male_icon.png';
            
			$cover_img = DB::table('cover_images')->select('images')->inRandomOrder()->take(1)->first();
            
            $partnerid = Null;
            if($_GET['partner']){
                $p_exist = DB::table('partner')->where('partnerid',$_GET['partner'])->first();
                //print_r($p_exist); die;
                if($p_exist){
                    $partnerid = $p_exist->id;
                }
            }
            
            if($partnerid && ($_GET['email'] != '')){                
                $email_exist = DB::table('users')->where('email',$_GET['email'])->first();
                
                if($email_exist){
                    Auth::loginUsingId($email_exist->id);
                    return 0;                
                }else{
                    $rand = ($_GET['password']) ? $_GET['password'] : rand(111111,999999);//uniqid();
					
					if(isset($_GET['name'])){ $name= $_GET['name']; }
					else{ $name = explode('@',$_GET['email'])[0]; } 
					
					if(isset($_GET['birth_date']))
					{ 
						$newDate= $_GET['birth_date'];
						
						$dob = date("Y-m-d", strtotime($newDate));
						
						$from = new DateTime($dob);
						$to   = new DateTime('today');
						$age = $from->diff($to)->y;	
					}
					else{ $dob = '2000-01-01'; $age = '18'; }

					if(isset($_GET['city'])){ $city = $_GET['city']; }
					else{ $city = ''; }	
					
					if(isset($_GET['country'])){ $country = $_GET['country']; }
					else{ $country = 'United Kingdom'; }
	
                    $token = ($_GET['token']) ? $_GET['token'] : '';
                    $hash =  ($_GET['hash']) ? $_GET['hash'] : '';
                    $cid =  ($_GET['cid']) ? $_GET['cid'] : '';
                    $parameters =  ($_GET['parameters']) ? $_GET['parameters'] : '';
                    
					$user_data = array(
                        'name' => $name,
                        'email' => $_GET['email'],
                        'password' => bcrypt($rand),		
                        'gender' => $_GET['gender'],
                        'seeking_val' => $_GET['seeking'],
                        'country' => $country,
                        'state' => $_GET['state'],
                        'city' => $city,
                        'dob' => $dob,
                        'age'  => $age,
                        'is_update' => '1',
                        'profile_image' => $profile_image,
                        'cover_image' => $cover_img->images,
                        'status' => '0',
                        'fake' => '0',
                        'partner_id' => $partnerid,
                        'profile_created_at'=>date('Y-m-d'),
                        'campaign_id' => 1,
                        'token' => $token,
                        'hash' => $hash,
                        'cid' => $cid,
                        'parameters' => $parameters
                    );
        
                    //print_r($user_data);die;
        
                    $user = User::create($user_data);
        
                    $user_detail = array(
                        'user_id' => $user->id,
                        'min_age' => 18,//($_GET['min_age']) ? $_GET['min_age'] : '',
                        'max_age' => 55//($_GET['max_age']) ? $_GET['max_age'] : ''
                    );
                    
                    
                    
                    Auth::loginUsingId($user->id);
                    //print_r($user);die;
                    
                    $detail_id = Helper::insert_data('user_details',$user_detail);
                    $wallet_detail = array(
                        'user_id' => $user->id,
                        'credits' => '1',
                        'diamonds' => '3'
                    );
                    Helper::insert_data('user_wallet',$wallet_detail);
                    $helper = new Helper;
					$subject = "Welcome to Flirtfull! You are successfully registered with us.";
                    $email_content = [
                            'receipent_email'=> $_GET['email'],
                            'subject'=>$subject,
                            'greeting'=> 'Flirtfull',
                            'first_name'=> $name,
                            'template_id' => 3
                            ];
                    
                    if($_GET['password']){
                        
                    }else{
                        //$verification_email = $helper->sendMailSuccess($email_content,$rand);   
                    }                    
            
					$subject2 = "Welcome to Flirtfull! Verify your email address to get started";
					$email_content2 = [
							'receipent_email'=> $_GET['email'],
							'subject'=>$subject2,
							'greeting'=> 'Flirting Sins',
							'first_name'=> $name,
							'template_id' => 1
							];
					$verification_email2 = $helper->sendMailFrontEnd($email_content2,'verification_link');
                    
                    //register user to third party chat
                    $dataaa = [
                                 "username"=>"Moneo",
                                 "password"=>"Moneo2019@",
                                 "service_code"=>"cd9271fa",
                                 "name"=> $user->name.'__'.$user->id,
                                 "group"=> "subscriber",
                                 "profile"=> [
                                                "avatar"=> "avatar.jpg",
                                                "age"=> $user->age,
                                                "location"=> $user->state,
                                                "images"=> [$user->profile_image]
                                             ],
                                  "additional_info"=> [
                                                "membership"=> "free",
                                                "link"=> "http://xyz.com",
                                                "about"=> "about"
                                             ],                                            
                                 ];
                        $ch = curl_init();
                        curl_setopt($ch, CURLOPT_URL,"https://im.operatorplatform.com/api/users/add");
                        curl_setopt($ch, CURLOPT_POST, 1);
                        curl_setopt($ch, CURLOPT_POSTFIELDS,
                                    "data=".json_encode($dataaa));
                        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
                        
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                
                        $server_output = curl_exec ($ch);
                        //echo "<pre>"; print_r($dataaa); print_r($server_output);
                        curl_close ($ch);
                        
                    $dataaa1 = [
                                 "username"=>"Moneo",
                                 "password"=>"Moneo2019@",
                                 "service_code"=>"cd9271fa",
                                 "name"=> $user->name.'__'.$user->id,
                                 "group"=> "persona",
                                 "profile"=> [
                                                "avatar"=> "avatar.jpg",
                                                "age"=> $user->age,
                                                "location"=> $user->state,
                                                "images"=> [$user->profile_image]
                                             ],
                                  "additional_info"=> [
                                                "membership"=> "free",
                                                "link"=> "http://xyz.com",
                                                "about"=> "about"
                                             ],                                            
                                 ];
                        $ch = curl_init();
                        curl_setopt($ch, CURLOPT_URL,"https://im.operatorplatform.com/api/users/add");
                        curl_setopt($ch, CURLOPT_POST, 1);
                        curl_setopt($ch, CURLOPT_POSTFIELDS,
                                    "data=".json_encode($dataaa1));
                        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
                        
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                
                        $server_output = curl_exec ($ch);
                        //print_r($dataaa1); print_r($server_output);
                        curl_close ($ch);    
                    //register user to third party chat
                    
                    //if user trigger by soi then
                    $partner = DB::table('partner')->where('id',$user->partner_id)->first();
                    $callback_param = [];
                    if($partner->trigger1 && ($partner->trigger1 == 'soi')){
                        $urll = $partner->callback1_url;
                        if($partner->callback1_parameter){
                            $callback_param = explode(',',$partner->callback1_parameter);
                        }
                    }
                    if($partner->trigger2 && ($partner->trigger2 == 'soi')){
                        $urll = $partner->callback2_url;
                        if($partner->callback2_parameter){
                            $callback_param = explode(',',$partner->callback2_parameter);
                        }
                    }
                    if($partner->trigger3 && ($partner->trigger3 == 'soi')){
                        $urll = $partner->callback3_url;
                        if($partner->callback3_parameter){
                            $callback_param = explode(',',$partner->callback3_parameter);
                        }
                    }
                    if($user->parameters){
                        $parameters = json_decode($user->parameters);
                        $default_param = ['payout-amount','username','transaction-id','currency'];
                        $str = ''; $i = 0;                        
                        
                            //echo "<pre>"; print_r($parameters);
                            foreach($parameters as $key=>$param){
                                if(($param != 'partner='.$partner->partnerid) && ($param != 'program=affiliate')){
                                   $paramtr = explode('=',$param);
                                   //print_r($paramtr);
                                   $sign = ($i == 0) ? '?' : '&';
                                   if(in_array($paramtr[0],$callback_param)){
                                        $str .= $sign.$paramtr[0].'='.$paramtr[1];
                                   }
                                   $i++;
                                }                            
                            }
                                                        
                            foreach($default_param as $row){
                                $sign = ($i == 0) ? '?' : '&';
                                if(in_array($row,$callback_param)){
                                    if($row == 'username'){
                                        $str .= $sign.$row.'='.$user->name;
                                    }else{
                                        $str .= $sign.$row.'='; 
                                    }
                                }
                                $i++;
                            }
                            $urll = $urll.$str;
                        
                        
                        $curl = curl_init();            
                        curl_setopt_array($curl, array(
                            CURLOPT_RETURNTRANSFER => 1,
                            CURLOPT_URL => $urll                
                        ));
                        // Send the request & save response to $resp
                        $resp = curl_exec($curl);                    
                        curl_close($curl);
                        
                        $updadetail['partner_id'] = $partner->partnerid;
                        $updadetail['url'] = $urll;
                        //print_r($updadetail); die;
                        DB::table('callback_partner')->insertGetId($updadetail);
                        
                    }
                    
                    
                    //die;
                    return $rand;   
                }
            }else{
                return 0;
            }
        
    }
	
	
    public function remove_querystring_var($url, $key) { 
        $url = preg_replace('/(.*)(?|&)' . $key . '=[^&]+?(&)(.*)/i', '$1$2$4', $url . '&'); 
        $url = substr($url, 0, -1); 
        return $url; 
    }
	
	public function purchase_credit(Request $request)
    {                
        $userid = Auth::id();
        //$mollie = new \Mollie\Api\MollieApiClient();
        //$mollie->setApiKey("test_hbTuaptRcRF9S69dnm8QM4J5RUQTBS");
        //$mollie->setApiKey("live_VUFaAbx4zxp6DxPMFd99ncvT3efdqs");
        //require_once '/var/www/html/flexpay/src/Verotel/FlexPay/Client.php';
        require_once '/var/www/html/flexpay/examples/config.php';        
        require_once 'vendor/autoload.php';        
        
        $brand = \Verotel\FlexPay\Brand::create_from_merchant_id($FLEXPAYCONFIG['merchantId']);        
        $flexpayClient = new \Verotel\FlexPay\Client(
            $FLEXPAYCONFIG['shopId'],
            $FLEXPAYCONFIG['signatureKey'],
            $brand
        );
                
        //print_r($flexpayClient); die;
        $plan_type = $request->plan_type;
        $plan = $request->plan;
        //$description = $request->description;
        $res = DB::table($plan_type)->where('id',$plan)->first();
        //print_r($res); die;
        $price = $res->price + $res->txn_fee ;
        $price = number_format((float)$price, 2, '.', '');
        //echo $price; die;
        $order_id = time();
        
        $purchaseUrl = $flexpayClient->get_purchase_URL([
            "priceAmount" => $price,
            "priceCurrency" => "GBP",
            "description" => $plan_type,
            "custom1" => $order_id,
            "custom2" => $plan_type.'-'.$plan,
            "custom3" => $userid            
        ]);
        //print_r($purchaseUrl); die;
        return response()->json(['success'=>true,'redirect' => $purchaseUrl]);
        //header("Location: " . $payment->getCheckoutUrl(), true, 303);die;
    }
    
    public function payment_callback(Request $request)
    {                
        $userid = Auth::id();
        
        require_once '/var/www/html/flexpay/examples/config.php';        
        require_once 'vendor/autoload.php';        
        
        $brand = \Verotel\FlexPay\Brand::create_from_merchant_id($FLEXPAYCONFIG['merchantId']);        
        $flexpayClient = new \Verotel\FlexPay\Client(
            $FLEXPAYCONFIG['shopId'],
            $FLEXPAYCONFIG['signatureKey'],
            $brand
        );
        
        if (!$flexpayClient->validate_signature($_GET)){
            http_response_code(500);
            echo "ERROR - Invalid signature!";
            exit;
        }
        
        $order_id = $_GET['custom1'];
        $plan_type = explode('-',$_GET['custom2'])[0];
        $plan = explode('-',$_GET['custom2'])[1];
        $extid = $_GET['custom3'];
        $amount = $_GET['priceAmount'];
        $users = DB::table('users')->where('id',$extid)->first();
        $insert_detail = array(
            'order_id' => $order_id,
            'firstname' => '',
            'lastname' => '',
            'amount' => $amount,
            'user_id' => $extid,//Auth::id(),
            'plan_type' => $plan_type,
            'plan_id' => $plan,
            'payment_id' => $_GET['transactionID'],
            'status' => 'success'

        );
        
       // print_r($insert_detail); die;
        
        $get_insert = DB::table('credit_payment')->insertGetId($insert_detail);
        
        $is_exist = DB::table('user_wallet')->where('user_id',$extid)->get();
        
        $qty = 0;
        if($plan_type == 'credits'){
            if(count($is_exist)){
                $qty = $is_exist[0]->credits;
            }
            $p = DB::table($plan_type)->where('id',$plan)->first();
            
            $premium = 0;
            if(count($is_exist)){
                $premium = $is_exist[0]->premiums;
            }
            
            
            if($p->qty == 15){
                if($premium > time()){
                    $update_detail['premiums'] = $premium + (3600 * 24 * 7);   
                }else{
                    $update_detail['premiums'] = time() + (3600 * 24 * 7);
                }
            }
            if($p->qty == 35){
                if($premium > time()){
                    $update_detail['premiums'] = $premium + (3600 * 24 * 15);   
                }else{
                    $update_detail['premiums'] = time() + (3600 * 24 * 15);
                }
            }
            if($p->qty == 75){
                if($premium > time()){
                    $update_detail['premiums'] = $premium + (3600 * 24 * 30);   
                }else{
                    $update_detail['premiums'] = time() + (3600 * 24 * 30);
                }
            }
            if($p->qty == 120){
                if($premium > time()){
                    $update_detail['premiums'] = $premium + (3600 * 24 * 45);   
                }else{
                    $update_detail['premiums'] = time() + (3600 * 24 * 45);
                }
            }
            if($p->qty == 250){
                if($premium > time()){
                    $update_detail['premiums'] = $premium + (3600 * 24 * 60);   
                }else{
                    $update_detail['premiums'] = time() + (3600 * 24 * 60);
                }
            }
            
            
            $update_detail['credits'] = $qty + $p->qty;
            if( isset($users) && ($users->campaign_id == 2) && (time() <= (strtotime($users->profile_activated_at) + (3600 * 24 * 7))) ){
                $update_detail['credits'] = $qty + (($p->qty)*2);    
            }
            $txn_fee = $p->txn_fee;
        }
        if($plan_type == 'coins'){
            if(count($is_exist)){
                $qty = $is_exist[0]->coins;
            }
            $p = DB::table($plan_type)->where('id',$plan)->first();
            $update_detail['coins'] = $qty + $p->qty;
            $txn_fee = $p->txn_fee;
        }
        if($plan_type == 'diamonds'){
            if(count($is_exist)){
                $qty = $is_exist[0]->diamonds;
            }
            $p = DB::table($plan_type)->where('id',$plan)->first();
            $update_detail['diamonds'] = $qty + $p->qty;
            $txn_fee = $p->txn_fee;
        }
        if($plan_type == 'premiums'){
            $is_premium = DB::table('user_wallet')->where('user_id',$extid)->first();
            $premium = 0;
            if($is_premium){
                $premium = $is_premium->premiums;
            }
            $p = DB::table($plan_type)->where('id',$plan)->first();
            if($premium > time()){
                $update_detail['premiums'] = $premium + ((24*3600*30)*$p->month);
                $update_detail['next_premium_month'] = time() + ((24*3600*30));
                $update_detail['premium_plan'] = $is_premium->premium_plan + ($p->month - 1);
                $onhold = $is_premium->onhold_credits;
            }else{
                $update_detail['premiums'] = time() + ((24*3600*30)*$p->month);
                $update_detail['next_premium_month'] = time() + ((24*3600*30));
                $update_detail['premium_plan'] = ($p->month - 1);
                $onhold = 0;
            }
            
            
            $qty = 0;
            if(count($is_exist)){
                $qty = $is_exist[0]->credits;
            }
            if($p->month == 1){
                $update_detail['credits'] = $qty + 10;   
            }            
            if($p->month == 3){
                $update_detail['credits'] = $qty + 15;
                $update_detail['onhold_credits'] = 30 + $onhold; 
            }
            if($p->month == 6){
                $update_detail['credits'] = $qty + 25;
                $update_detail['onhold_credits'] = 125 + $onhold; 
            }
            $txn_fee = $p->txn_fee;
        }        
        
        
        //echo "<pre>"; print_r(count($is_exist)); die;
        if(count($is_exist)){            
            DB::table('user_wallet')
                ->where('user_id',$extid)
                ->update($update_detail);    
        }else{
            $update_detail['user_id'] = $extid;
            DB::table('user_wallet')->insertGetId($update_detail); 
        }
        
        //credit to special paertner        
        $partner = DB::table('partner')->where('id',$users->partner_id)->first();
        $amount = ($partner->commission/100)*($amount - $txn_fee);
        $url = 'https://callbacks.bitterstrawberry.org/?token='.$partner->token.'&hash='.$users->hash.'&transaction_id='.$_GET['transactionID'].'&amount='.$amount.'&payout_type=pps&currency=GBP';
        if($partner->partner_type == 2 && $partner->partnerid == 'BSB98867602'){
            $curl = curl_init();            
            curl_setopt_array($curl, array(
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => $url                
            ));
            // Send the request & save response to $resp
            $resp = curl_exec($curl);                    
            curl_close($curl);
            
            $updadetail['partner_id'] = $partner->partnerid;
            $updadetail['url'] = $url;
            //print_r($updadetail); die;
            DB::table('callback_partner')->insertGetId($updadetail);
        }

        //if user trigger by soi then            
            $callback_param = [];
            if($partner->trigger1 && ($partner->trigger1 == 'sale')){
                $urll = $partner->callback1_url;
                if($partner->callback1_parameter){
                    $callback_param = explode(',',$partner->callback1_parameter);
                }
            }
            if($partner->trigger2 && ($partner->trigger2 == 'sale')){
                $urll = $partner->callback2_url;
                if($partner->callback2_parameter){
                    $callback_param = explode(',',$partner->callback2_parameter);
                }
            }
            if($partner->trigger3 && ($partner->trigger3 == 'sale')){
                $urll = $partner->callback3_url;
                if($partner->callback3_parameter){
                    $callback_param = explode(',',$partner->callback3_parameter);
                }
            }
            if($users->parameters){
                $parameters = json_decode($users->parameters);
                $default_param = ['payout-amount','username','transaction-id','cur'];
                $str = ''; $i = 0;                        
                
                    //echo "<pre>"; print_r($parameters);
                    foreach($parameters as $key=>$param){
                        if(($param != 'partner='.$partner->partnerid) && ($param != 'program=affiliate')){
                           $paramtr = explode('=',$param);
                           //print_r($paramtr);
                           $sign = ($i == 0) ? '?' : '&';
                           if(in_array($paramtr[0],$callback_param)){
                                $str .= $sign.$paramtr[0].'='.$paramtr[1];
                           }
                           $i++;
                        }                            
                    }
                                                
                    foreach($default_param as $row){
                        $sign = ($i == 0) ? '?' : '&';
                        if(in_array($row,$callback_param)){
                            if($row == 'username'){
                                $str .= $sign.$row.'='.$users->name;
                            }
                            if($row == 'payout-amount'){
                                $str .= $sign.$row.'='.$amount;
                            }
                            if($row == 'transaction-id'){
                                $str .= $sign.$row.'='.$users->name;
                            }
                            if($row == 'cur'){
                                $str .= $sign.'cur=GBP';
                            }

                        }
                        $i++;
                    }
                    $urll = $urll.$str;
                
                
                $curl = curl_init();            
                curl_setopt_array($curl, array(
                    CURLOPT_RETURNTRANSFER => 1,
                    CURLOPT_URL => $urll                
                ));
                // Send the request & save response to $resp
                $resp = curl_exec($curl);                    
                curl_close($curl);
                
                $updadetail['partner_id'] = $partner->partnerid;
                $updadetail['url'] = $urll;
                //print_r($updadetail); die;
                DB::table('callback_partner')->insertGetId($updadetail);
                
            }
        
        
        echo "OK";
        /*if ($payment->isPaid())
        {
            echo "Payment received.";
        }*/
    }

    public function payment_success($order_id=Null)
    {
        require_once '/var/www/html/flexpay/examples/config.php';        
        require_once 'vendor/autoload.php';        
        
        $brand = \Verotel\FlexPay\Brand::create_from_merchant_id($FLEXPAYCONFIG['merchantId']);        
        $flexpayClient = new \Verotel\FlexPay\Client(
            $FLEXPAYCONFIG['shopId'],
            $FLEXPAYCONFIG['signatureKey'],
            $brand
        );
        
        if (!$flexpayClient->validate_signature($_GET)){
            http_response_code(500);
            echo "ERROR - Invalid signature!";
            exit;
        }
        
        $order_id = $_GET['custom1'];
                
        //print_r($_GET); die;
        $get_status = DB::table('credit_payment')->select('users.email','users.name','credit_payment.*')
            ->leftJoin('users', 'users.id', '=', 'credit_payment.user_id')->where('credit_payment.order_id', '=', $order_id)->first();        
        $payment_status = isset($get_status->status) ? $get_status->status : 'pending';
        //print_r($get_status); die;
        //return view('payment-success',compact('payment_status'));

        // send email
        if($payment_status == 'paid')
        {
            $email_content = array(
                            'receipent_email'   => $get_status->email,
                            'subject'           => 'Your Flirtfull Payment Status',
                            'first_name'        => $get_status->name,
                            'payment_status'        => $payment_status,
                            'greeting'          => 'Flirtfull'
                        );
            $helper = new Helper;
            $email_response = $helper->sendMail(
                                    $email_content,
                                    'payment_notification'
                                );
        }

        return redirect('search')->with('payment_status', $payment_status);
    }
		
	

    public function setting(Request $request)
    {
        if(Auth::check())
        {
            $get_user = DB::table('users')->select('users.*','user_details.*')
            ->leftJoin('user_details', 'user_details.user_id', '=', 'users.id')->where('users.id','=',Auth::id())->first();
            //print_r($get_user);die;
            return view('setting',compact('get_user'));
        }
        else{ 

            $countries = DB::table('countries')->where('status',1)->get();

            $most_popular = DB::table('users')->select('users.*','user_details.*')
            ->leftJoin('user_details', 'user_details.user_id', '=', 'users.id')->where('users.status','=',1)->skip(100)->take(12)->get();
            //print_r($most_popular);die;

            return view('home',compact('most_popular','countries'));
        }

    }
	
	public function about(Request $request)
    {
        $page = DB::table('pages')->where('id',19)->first();
        return view('about',compact('page'));
    }

    public function help_center(Request $request)
    {
        return view('help_center');
    }

	public function faq(Request $request)
    {
        $faq = DB::table('faq')->where('is_deleted',0)->get();
        return view('faq',compact('faq'));
    }
	public function terms_conditions(Request $request)
    {
        $page = DB::table('pages')->where('id',18)->first();

        $all_interests = Helper::all_data('interests',['status'=>1]);

        $all_languages = Helper::all_data('languages',['status'=>1]);

        return view('terms_conditions',compact('page','all_interests','all_languages'));
    }
	public function privacy_policy(Request $request)
    {
        $page = DB::table('pages')->where('id',21)->first();

        $all_interests = Helper::all_data('interests',['status'=>1]);

        $all_languages = Helper::all_data('languages',['status'=>1]);

        return view('privacy_policy',compact('page','all_interests','all_languages'));
    }
	public function our_plans(Request $request)
    {
        
		$credits		= DB::table('credits')
						->where('status','1')
						->where('is_deleted','0')
						->get();
						
		$diamonds	= DB::table('diamonds')
						->where('status','1')
						->where('is_deleted','0')
						->get();
		
		
		$premiums		= DB::table('premiums')
						->where('status','1')
						->where('is_deleted','0')
						->get();
		
		return view('our_plans',compact('credits','diamonds','premiums'));
    }
	public function activities(Request $request)
    {
        $segment = (request()->segment(2)) ? request()->segment(2) : 'like';
        
        $like = [];
        if($segment == 'like' || $segment == 'match'){
            $like       = DB::table('likes')
                        ->select('receiver_id')
                        ->where('sender_id',Auth::id())
                        ->where('type',1)
                        ->orderBy('id','desc')
                        ->paginate(15);    
        }
		
        $like_me = [];
		if($segment == 'likeme'){
		    $like_me	= DB::table('likes')
						->select('sender_id')
						->where('receiver_id',Auth::id())
						->where('type',1)
                        ->orderBy('id','desc')
						->paginate(15);
        }
		
        $favourite = [];
		if($segment == 'favorite'){
		    $favourite		= DB::table('activity')
    						->select(array('to_id'))
    						->where('from_id',Auth::id())
    						->where('type',3)
    						//->groupBy('to_id','id')
                            ->orderBy('id','desc')
    						->paginate(15);
        }                        
		
        $favourite_me = [];
        if($segment == 'favoriteme'){
    		$favourite_me	= DB::table('activity')
    						->select(array('from_id'))
    						->where('to_id',Auth::id())
    						->where('type',3)
    						//->groupBy('from_id','id')
                            ->orderBy('id','desc')
    						->paginate(15);
        }
		
        $visit = [];
        if($segment == 'visit'){
    		$visit			= DB::table('activity')    						
    						->where('from_id',Auth::id())
    						->where('type',1)    						
                            ->orderBy('id','desc')    						
                            ->get();

            $toids = []; $to_ids = [];
            foreach($visit as $row){
                $to_ids[] = $row->to_id;
                if(!in_array($row->to_id,$toids)){
                    $toids[] = $row->to_id;                
                }
            }
            
            $visit = $toids;
            $visit_count = array_count_values($to_ids);
            
            
            $pagination = $this->custom_page($visit,'activities/visit');
            $visit = $pagination[0];
            $visit_pagination = $pagination[1];                
        }
        
        $visited_me = [];
        if($segment == 'visit'){						
    		$visited_me		= DB::table('activity')    						
    						->where('to_id',Auth::id())
    						->where('type',1)    						
                            ->orderBy('id','desc')    						
                            ->get();

            $fromids = []; $from_ids = [];
            foreach($visited_me as $row){
                $from_ids[] = $row->from_id;
                if(!in_array($row->from_id,$fromids)){
                    $fromids[] = $row->from_id;                
                }
            }
            
            $visited_me = $fromids;
            $visited_me_count = array_count_values($from_ids);
            
            $pagination1 = $this->custom_page($visited_me,'activities/visitme');
            $visited_me = $pagination1[0];
            $visited_pagination = $pagination1[1];                
        }
		
		$payments	= DB::table('credit_payment')->where('user_id',Auth::id())->orderBy('id','desc')->get();
                        
		$wallet = DB::table('user_wallet')
                    ->where('user_id', Auth::id())->first();  


		return view('activities',compact('like','like_me','favourite','favourite_me','match','visit','visit_count','visited_me','visited_me_count','payments','visit_pagination','visited_pagination','segment','wallet'));
    }
	
	protected function custom_page($arr,$pagelink){
        $page = ! empty( $_GET['page'] ) ? (int) $_GET['page'] : 1;
        $total = count( $arr ); //total items in array    
        $limit = 15; //per page    
        $totalPages = ceil( $total/ $limit ); //calculate total pages
        $page = max($page, 1); //get 1 page when $_GET['page'] <= 0
        $page = min($page, $totalPages); //get last page when $_GET['page'] > $totalPages
        $offset = ($page - 1) * $limit;
        if( $offset < 0 ) $offset = 0;
        
        $arr = array_slice( $arr, $offset, $limit );
        
        $link = $pagelink.'?page=%d';
        $pagerContainer = '<ul class="pagination" role="navigation">';   
        if( $totalPages != 0 ) 
        {
          if( $page == 1 ) 
          { 
            $pagerContainer .= ''; 
          } 
          else 
          { 
            $pagerContainer .= sprintf( '<li class="page-item">
                        <a class="page-link" href="'.$link.'" rel="prev" aria-label="« Previous">‹</a>
                    </li>', $page - 1 ); 
          }
          
          if($page < 5){ $cont = 10 - $page; }else{ $cont = 5; }

          for($i=1;$i<=$totalPages;$i++){
            if($page == $i){
                $pagerContainer .= '<li class="page-item active" aria-current="page"><span class="page-link">'.$i.'</span></li>';
            }else{
                if($i >= $page-$cont && $i <= $page+$cont){
                    $pagerContainer .= '<li class="page-item"><a class="page-link" href="'.url('/').'/'.$pagelink.'?page='.$i.'">'.$i.'</a></li>';  
                }          
            }   
          }
          if( $page == $totalPages ) 
          { 
            $pagerContainer .= ''; 
          }
          else 
          { 
            $pagerContainer .= sprintf( '<li class="page-item">
                        <a class="page-link" href="'.$link.'" rel="next" aria-label="Next »">›</a>
                    </li>', $page + 1 ); 
          }           
        }                   
        $pagerContainer .= '</ul>';
        return array($arr,$pagerContainer);
    }


    public function get_user_chat($user_id){
        $acc = ServiceAccount::fromJsonFile('/var/www/html/flirtengine-firebase-adminsdk-94hsi-4dac5384f1.json');
        $firebase = (new Factory)->withServiceAccount($acc)->create();
        $database = $firebase->getDatabase();
        $newPost = $database
        ->getReference('chat_history/'.$user_id)
        ->getSnapshot();
        return $newPost->getValue();
    }
	
	public function like_page(Request $request)
    {                    
        $user = $this->random_user();//DB::table('users')->inRandomOrder()->first();
        $userwallet = DB::table('users')->where('id', Auth::id())->first();
		return view('like_page',compact('user','userwallet'));
    }
    
    public function get_user(Request $request)
    {                    
        $user = DB::table('users')->where('id',$_POST['userid'])->first();
        echo $user->fake;
    }
    
    public function mailtemplate(Request $request)
    {
        $email_content = array(
                            //'receipent_email'   => $get_receiver->email,
                            'receipent_email'   => $receiver->email,
                            'subject'           => 'Your Flirtfull Chat Notification',
                            'first_name'        => $receiver->name,
                            'sender_name'        => $sender->name,
                            'message'        => $message,
                            'greeting'          => 'Flirtfull',
                            'profile_image' => $profile_image
                            );
        
		return view('emails.chat_notification',compact('email_content'));
    }
    
    public function mailtemplate1(Request $request)
    {
        $email_content = array(
                            //'receipent_email'   => $get_receiver->email,
                            'receipent_email'   => $receiver->email,
                            'subject'           => 'Your Flirtfull Chat Notification',
                            'first_name'        => $receiver->name,
                            'sender_name'        => $sender->name,
                            'message'        => $message,
                            'greeting'          => 'Flirtfull',
                            'profile_image' => $profile_image
                            );
        
		return view('emails.verification_link',compact('email_content'));
    }

	public function random_user($count=Null){    
       $iam = DB::table('users')->where('id', '=',Auth::id())->where('status', '=',1)->where('is_deleted', '=',0)->first();  

       $seeking_val = (isset(Auth::user()->seeking_val)) ? Auth::user()->seeking_val : 2;

       $random_user = DB::table('users')->where('id', '!=',Auth::id())->where('status', '=',1)->where('gender', '=',$seeking_val)->where('is_deleted', '=',0)->inRandomOrder()->first();
       
       $alreayliked = DB::table('likes')->where('sender_id', '=',Auth::id())->where('receiver_id', '=',$random_user->id)->count();
       //print_r($alreayliked); die;
       if($alreayliked > 0){                    
            $this->random_user();            
       }
       return $random_user;
    }
    
    public function get_random_user($count=Null){
       $iam = DB::table('users')->where('id', '=',Auth::id())->where('status', '=',1)->where('is_deleted', '=',0)->first();
       if($count > 5){        
            $random_user = DB::table('users')->where('id', '!=',Auth::id())->where('status', '=',1)->where('is_deleted', '=',0)->inRandomOrder()->first();
       }else{
            $random_user = DB::table('users')->where('gender', '=',$iam->seeking_val)->where('id', '!=',Auth::id())->where('status', '=',1)->where('is_deleted', '=',0)->inRandomOrder()->first();
       }
       $alreayliked = DB::table('likes')->where('sender_id', '=',Auth::id())->where('receiver_id', '=',$random_user->id)->count();
       //print_r($alreayliked); die;
       if($alreayliked > 0){
            $count++;            
            $this->random_user($count);            
       }else{
        return '$random_user';
        //echo $alreayliked;    
       }
    }
	
    public function change_email(Request $request)
    {
        $user_id = $request->user_id;

        $input = array(
            'email' => trim($request->email),
            'name' => trim($request->name)
        );

        $update_account = DB::table('users')->where('id',$user_id)->update($input);

        if($update_account)
        {
            session()->flash('success', 'Your account was successfully updated. !!'); 
            echo 'success';
        }
        else{
            session()->flash('success', 'Your account was successfully updated. !!'); 
            echo 'success';
        }
    }

    public function profile_liking(Request $request)
    {                
		
        $receiver_id = $_POST['receiverId'];
        $sender_id = Auth::id();;
        $user = DB::table('users')->where('id', '=',$receiver_id)->first();
                
            $rand = rand(1,4);
            
			$insert['sender_id'] = Auth::id();
			$insert['receiver_id'] = $_POST['receiverId'];            
            $insert['type'] = 1;
            $insert['status'] = 0;
            $insert['message'] = NULL;
			
            $res = DB::table('likes')->insert($insert);
            
            if($user->fake){
                if($rand == 3){
                    $insert['sender_id'] = $_POST['receiverId'];
                    $insert['receiver_id'] = Auth::id();            
                    $insert['type'] = 1;
                    $insert['status'] = 0;
                    $insert['message'] = NULL;
                    
                    $res = DB::table('likes')->insert($insert);
                   echo 1; 
                }else{
                   echo 0;
                }
            }else{
                echo 0;    
            }
			
        
	}	
	
	public function change_phone(Request $request)
    {
        $user_id = $request->user_id;

        $input = array(
            'phone' => $request->phone,
        );

        $update_account = DB::table('users')->where('id',$user_id)->update($input);

        if($update_account)
        {
            session()->flash('success', 'Your account was successfully updated. !!'); 
            echo 'success';
        }
        else{
            session()->flash('success', 'Your account was successfully updated. !!'); 
            echo 'success';
        }
    }

    public function change_password(Request $request)
    {
        $user_id = $request->user_id;

        $user = User::where('id',$user_id)->first();
        $current_password = $user->password;
        
        if (Hash::check($request->get('current_password'),$current_password)) 
        {
            $input = array(
                'password' => bcrypt($request->password),
            );

            $update_account = DB::table('users')->where('id',$user_id)->update($input);

            if($update_account)
            {
                session()->flash('success', 'Your password was successfully changed. !!'); 
                echo 'success';
            }
            else{
                session()->flash('success', 'Your password was successfully changed. !!'); 
                echo 'success';
            }
        }
        else{
            echo 'error';
        }
    }
    
    public function updateCredits(Request $request){
        if(Auth::check())
        {
            $user_id = Auth::user()->id;
            $input = array(
                'credits' => $request->credits,
            );
            DB::table('user_wallet')
            ->where('user_id', $user_id)
            ->update(['credits' => $request->credits]);
            
            DB::table('user_wallet')
            ->where('user_id', $user_id)
            ->update(['coins' => $request->coins]);
            
            DB::table('user_wallet')
            ->where('user_id', $user_id)
            ->update(['diamonds' => $request->diamonds]);
            
            DB::table('user_wallet')
            ->where('user_id', $user_id)
            ->update(['premiums' => $request->premiums]);
            
            $wallet = DB::table('user_wallet')->where('user_id',$user_id)->first();
            print_r($wallet->credits);
        }        
    }
    
    public function insert_chat_api(Request $request){
        $post = '';    
        $post = $request->all();
        if($post){
            DB::table('test')->insertGetId(array("insertdata"=>json_encode($post)));   
        }        
                
        return view('chat_api',compact('post'));        
    }
    
    public function insert_activity_api(Request $request){
        $post = '';    
        $post = $request->all();
        //echo "<pre>"; print_r($post); die;
        if($post){
            foreach ($post as $key => $value) {
                DB::table('activity')->insert($value);   
            }
            
        }        
                            
    }

    public function insert_like_activity_api(Request $request){
        $post = '';    
        $post = $request->all();
        //echo "<pre>"; print_r($post); die;
        if($post){
            foreach ($post as $key => $value) {
                DB::table('likes')->insert($value);   
            }
            
        }        
                            
    }
    
    //RESEND VERIFICATION LINK
	public function resend_verification(Request $request)
    {
		$userid = Auth::id();
		//$userid =$request->user_id;
		
		$user_details = DB::table('users')->select('users.*')->where('users.id',$userid)->get();
			
		//print_r($user_details); die;
        //echo $user_details[0]->email; die;
		$user_email = $user_details[0]->email;
        $user_name = $user_details[0]->name;
		
		$helper = new Helper;
        
		$subject = "Welcome to Flirting Sins! Verify your email address to get started";
            
		$email_content = [
                'receipent_email'=> $user_email,
				'subject'=>$subject,
				'greeting'=> 'Flirting Sins',
				'first_name'=> $user_name,
				'template_id' => 1
				];
        
		//$verification_email = $helper->sendMailFrontEnd($email_content,'verification_link');

        //if($userid == 4890){
            $verification_email = $helper->sendMailFrontEnd_1($email_content,'register');
        //}

		//session()->flash('success', 'Resend Success..!!');
            
        echo json_encode(array('success' => true));
	}	
    
    public function updateUserList(){
        // Get cURL resource
        $curl = curl_init();
        //print_r($curl);die;
        // Set some options - we are passing in a useragent too here
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => 'https://moneoangels.com/api/v1/getUpdatedUserList',
            //CURLOPT_USERAGENT => 'Codular Sample cURL Request'
        ));
        // Send the request & save response to $resp
        $resp = curl_exec($curl);
        //print_r($resp); die;
        // Close request to clear up some resources
        curl_close($curl);
        $list = json_decode($resp);
        if($list->status == 1){
            $data = $list->data;
            // print_r($data); die;
            foreach($data as $row){
                $user_exists = DB::table('users')->where('email', $row->email)->first();
                //print_r($user_exists); die;
                if($user_exists){
					
					$from = new DateTime($row->dob);
					$to   = new DateTime('today');
					$age = $from->diff($to)->y;
					
                    $update['name'] = $row->name;
                    $update['email'] = $row->email;
                    //$update['password'] = $row->password;
                    $update['gender'] = $row->gender;
                    $update['dob'] = $row->dob;
                    $update['age'] = $age;
                    $update['seeking_val'] = $row->seeking_val;
                    $update['country'] = $row->country;
                    $update['city'] = $row->city;
                    $update['state'] = $row->state;
                    $update['address'] = $row->address;
                    $update['profile_image'] = $row->profile_image;
                    $update['source_img'] = $row->source_img;
                    $update['profile_image_status'] = $row->profile_image_status;
                    $update['cover_image'] = $row->cover_image;
                    $update['cover_image_status'] = $row->cover_image_status;
                    $update['profile_type'] = $row->profile_type;
                    $update['appearance'] = $row->appearance;
                    $update['adult_level'] = $row->adult_level;                                
                    $update['status'] = $row->status;
                    $update['is_deleted'] = $row->is_deleted;
                    $update['profile_language'] = $row->profile_language;
                    //echo $row->id;
                    DB::table('users')
                        ->where('email', $row->email)
                        ->update($update);
                        
                    if($row->user_details){    
                        $user_details = array(                                
                                    'seeking' => $row->user_details->seeking,
                                    'about_me' => $row->user_details->about_me,
                                    'about_partner' => $row->user_details->about_partner,
                                    'work_as' => $row->user_details->work_as,
                                    'education' => $row->user_details->education,
                                    'relationship' => $row->user_details->relationship,
                                    'kids' => $row->user_details->kids,
                                    'smoke' => $row->user_details->smoke,
                                    'drink' => $row->user_details->drink,
                                    'height' => $row->user_details->height,
                                    'updated_at' => date('Y-m-d H:i:s')
                                );
                        DB::table('user_details')
                            ->where('user_id', $user_exists->id)
                            ->update($user_details);
                    }
                    /*for interests*/
                    if($row->user_interests){
                        $user_interests = array(                            
                            'interests' => $row->user_interests->interests,
                            'updated_at' => date('Y-m-d H:i:s')
                        );
                        DB::table('user_interests')
                            ->where('user_id', $user_exists->id)
                            ->update($user_interests);
                    }
                    /*for languages*/
                    if($row->user_languages){
                        $user_languages = array(                            
                            'languages' => $row->user_languages->languages,
                            'updated_at' => date('Y-m-d H:i:s')
                        );                                
                        DB::table('user_languages')
                            ->where('user_id', $user_exists->id)
                            ->update($user_languages);
                    }
                    /*for gallery images*/
                    
                    $images = $row->gallery_images;
                    
                    if(!empty($images))
                    {
                            DB::table('gallery_images')->where('user_id', $user_exists->id)->delete();
                            foreach($images as $val)
                            {
                                $image_exists = DB::table('gallery_images')->where('user_id', $user_exists->id)->where('gallery_image', $val->gallery_image)->first();
                                $gallery_images = [];                                                                                                                
                                    $gallery_images['is_private'] = $val->is_private;
                                    $gallery_images['status'] = $val->status;
                                    $gallery_images['is_crop'] = $val->is_crop;
                                    $gallery_images['source_img'] = $val->source_img;
                                    $gallery_images['updated_at'] = date('Y-m-d H:i:s');
                                
                                     
                                if($image_exists){
                                    DB::table('gallery_images')
                                    ->where('gallery_image', $val->gallery_image)
                                    ->where('user_id', $user_exists->id)
                                    ->update($gallery_images);    
                                }else{
                                    $gallery_images['gallery_image'] = $val->gallery_image;
                                    $gallery_images['user_id'] = $user_exists->id; 
                                    DB::table('gallery_images')->insert($gallery_images);
                                }
                                
                            }
                    }    
                }
            }
            echo "Updated successfully";
        }
    }
    
    public function release_credits(){
        $users = DB::table('user_wallet')                            
                            ->where('premium_plan','!=', 0)
                            ->get();
        
        foreach($users as $row){                            
            if($row->next_premium_month < time()){                                    
                DB::table('user_wallet')                                    
                                    ->where('user_id', $row->user_id)
                                    ->update(['credits'=>($row->credits + ($row->onhold_credits/$row->premium_plan)),
                                              'onhold_credits'=>($row->onhold_credits - ($row->onhold_credits/$row->premium_plan)),
                                              'premium_plan'=> ($row->premium_plan - 1),
                                              'next_premium_month' => ($row->next_premium_month + (24*3600*30) )
                                            ]);   
            }
        }
    }
    
    public function updatePartnerList(){        
        // Get cURL resource
        $curl = curl_init();
        //print_r($curl);die;
        // Set some options - we are passing in a useragent too here
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => 'https://www.moneotracks.com/api/v1/getPartnerList',
            //CURLOPT_USERAGENT => 'Codular Sample cURL Request'
        ));
        // Send the request & save response to $resp
        $resp = curl_exec($curl);
        //print_r($resp); die;
        // Close request to clear up some resources
        curl_close($curl);
        $list = json_decode($resp);
        if($list->status == 1){
            $data = $list->data;
            // print_r($data); die;                    
                                    
            foreach($data as $row){
                $exist = DB::table('partner')                        
                            ->where('partnerid',$row->partnerid)->first();
                //print_r($exist); die;            
                	//$update['id'] = $row->id;									
                    $update['name'] = $row->name;
                    $update['contact'] = $row->contact;
                    $update['companyname'] = $row->companyname;
                    $update['email'] = $row->email;
                    $update['address'] = $row->address;
                    $update['country'] = $row->country;
                    $update['city'] = $row->city;
                    $update['coc'] = $row->coc;
                    $update['vat'] = $row->vat;
                    $update['options'] = $row->options;
                    $update['account_status'] = $row->account_status;
                    $update['account_type'] = $row->account_type;
                    $update['provision_type'] = $row->provision_type;
                    $update['bank_name'] = $row->bank_name;
                    $update['bank_location'] = $row->bank_location;
                    $update['account_number'] = $row->account_number;
                    $update['swift'] = $row->swift;
                    $update['partner_type'] = $row->partner_type;
                    $update['commission'] = $row->commission;                    
                    $update['status'] = $row->status;
                    $update['is_deleted'] = $row->is_deleted;
                    $update['token'] = $row->token;
                    $update['parameter1_name'] = $row->parameter1_name;
                    $update['parameter1_value'] = $row->parameter1_value;
                    $update['parameter2_name'] = $row->parameter2_name;
                    $update['parameter2_value'] = $row->parameter2_value;
                    $update['parameter3_name'] = $row->parameter3_name;
                    $update['parameter3_value'] = $row->parameter3_value;
                    $update['parameter4_name'] = $row->parameter4_name;
                    $update['parameter4_value'] = $row->parameter4_value;
                    $update['parameter5_name'] = $row->parameter5_name;
                    $update['parameter5_value'] = $row->parameter5_value;
                    $update['callback1_url'] = $row->callback1_url;
                    $update['callback1_parameter'] = $row->callback1_parameter;
                    $update['trigger1'] = $row->trigger1;
                    $update['callback2_url'] = $row->callback2_url;
                    $update['callback2_parameter'] = $row->callback2_parameter;
                    $update['trigger2'] = $row->trigger2;
                    $update['callback3_url'] = $row->callback3_url;
                    $update['callback3_parameter'] = $row->callback3_parameter;
                    $update['trigger3'] = $row->trigger3;
                    $update['domain_id'] = 1;
                    //echo $row->id;
                    if($exist){
                        DB::table('partner')
                        ->where('partnerid', $row->partnerid)
                        ->update($update);
                    }else{
                        $update['partnerid'] = $row->partnerid;
                        DB::table('partner')                        
                            ->insert($update);
                    }
                
            }
            echo "Updated successfully";
        }
    }
	
	public function updateChatRoomList(){ 
        // Get cURL resource
        $curl = curl_init();
        //print_r($curl);die;
        // Set some options - we are passing in a useragent too here
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => 'https://www.triggersengine.com/api/v1/getUpdatedChatRoomList',
            //CURLOPT_USERAGENT => 'Codular Sample cURL Request'
        ));
        // Send the request & save response to $resp
        $resp = curl_exec($curl);
        //print_r($resp); die;
        // Close request to clear up some resources
        curl_close($curl);
        $list = json_decode($resp);
         //print_r($list); die;
        if($list->status == 1){
            $data = $list->data;
             //print_r($data); die;                        
            foreach($data as $row){
                $exists = DB::table('chat_room')->where('room_id', $row->room_id)->first();
                //print_r($exists); die;                
                $insert['sender_id'] = $row->sender_id;
                $insert['receiver_id'] = $row->receiver_id;                
                $insert['subject'] = $row->subject;
                $insert['message'] = $row->message;
                $insert['attachment'] = $row->attachment;
                $insert['status'] = $row->status;
                $insert['notification_status'] = $row->notification_status;
                $insert['like_sender'] = $row->like_receiver;
                $insert['source'] = $row->source;
                $insert['created_at'] = $row->created_at;
                $insert['updated_at'] = $row->updated_at;
                
                if($exists){										
                    DB::table('chat_room')
                                    ->where('room_id', $row->room_id)
                                    ->update($insert);
                }else{
                    $insert['room_id'] = $row->room_id;
                    DB::table('chat_room')                        
                        ->insert($insert);
                }
            }
            
            echo "Updated successfully";
        }
    }
    
    public function get_profiles(Request $request)
    {
        $result = $request->all();
        //echo json_encode($result);die;

        //print_r($result);die;
        //$get_all = json_decode($result);
        if(!empty($result))
        {
            foreach($result as $key => $value)
            {
            	$user_exist = DB::table('users')->where('email', $value['email'])->count();

                if($user_exist == 0)
                {
					              $users_array = array(
					                    'role_type' => $value['role_type'],
					                    'name' => $value['name'],
					                    'email' => $value['email'],
					                    'password' => $value['password'],
					                    'phone' => $value['phone'],
					                    'gender' => $value['gender'],
					                    'dob' => $value['dob'],
					                    'age' => $value['age'],
					                    'seeking_val' => $value['seeking_val'],
					                    'country' => $value['country'],
					                    'state' => $value['state'],
					                    'city' => $value['city'],
					                    'address' => $value['address'],
					                    'profile_image' => $value['profile_image'],
					                    'source_img' => $value['source_img'],
					                    'profile_image_status' => $value['profile_image_status'],
					                    'cover_image' => $value['cover_image'],
					                    'cover_image_status' => $value['cover_image_status'],
					                    'profile_type' => $value['profile_type'],
					                    'appearance' => $value['appearance'],
					                    'adult_level' => $value['adult_level'],
					                    'is_update' => $value['is_update'],
					                    'is_login' => $value['is_login'],
					                    'last_login' => $value['last_login'],
					                    'provider_name' => $value['provider_name'],
					                    'status' => $value['status'],
					                    'fake' => $value['fake'],
					                    'remember_token' => $value['remember_token'],
					                    'is_deleted' => $value['is_deleted'],
					                    'profile_language' => $value['profile_language']
					                ); 

					                $user_id = DB::table('users')->insertGetId($users_array);

					                $profile_image = $value['profile_image'];
					                $profile_image_status = $value['profile_image_status'];

					                // if($profile_image_status == 1)
					                // {
					                //     $image = 'https://moneoangels.com/public/thumbnail/'.$profile_image;
					                //     $imageData = file_get_contents($image);

					                //     $imageProfile = 'https://moneoangels.com/public/images/profile_image/'.$profile_image;
					                //     $imageDataProfile = file_get_contents($imageProfile);

					                //     if(!file_exists(public_path('/images/profile_image').'/'.$profile_image)) 
					                //     {
					                        

					                //         $destinationProfilePath = public_path('/images/profile_image');
					                //         file_put_contents($destinationProfilePath.'/'.$profile_image, $imageDataProfile);

					                //         if(!file_exists(public_path('/thumbnail').'/'.$profile_image)) 
					                //         {
					                //             $destinationPath = public_path('/thumbnail');
					                //             file_put_contents($destinationPath.'/'.$profile_image, $imageData);

					                    
					                //         }
					                //     }
					                //     else{
					                        
					                //     }
					                // }

					                /*user details*/
					                $user_details = array(
					                    'user_id' => $user_id, 
					                    'seeking' => $value['seeking'],
					                    'about_me' => $value['about_me'],
					                    'about_partner' => $value['about_partner'],
					                    'work_as' => $value['work_as'],
					                    'education' => $value['education'],
					                    'relationship' => $value['relationship'],
					                    'kids' => $value['kids'],
					                    'smoke' => $value['smoke'],
					                    'drink' => $value['drink'],
					                    'height' => $value['height'],
					                    'updated_at' => date('Y-m-d H:i:s')
					                );

					                DB::table('user_details')->insertGetId($user_details);
					                /*user details*/

					                /*for interests*/
					                $user_interests = array(
					                    'user_id' => $user_id, 
					                    'interests' => $value['interests'],
					                    'updated_at' => date('Y-m-d H:i:s')
					                );

					                DB::table('user_interests')->insertGetId($user_interests);

					                /*for interests*/

					                /*for languages*/
					                $user_languages = array(
					                    'user_id' => $user_id, 
					                    'languages' => $value['languages'],
					                    'updated_at' => date('Y-m-d H:i:s')
					                );
					                

					                DB::table('user_languages')->insertGetId($user_languages);
					                /*for languages*/

					                /*for gallery images*/
					                $images = $value['gallery_images'];
					                
					                if(!empty($images))
					                {
					                        foreach($images as $val)
					                        {
					                            $gallery_images = array(
					                                'user_id' => $user_id, 
					                                'gallery_image' => $val['gallery_image'],
					                                'is_private' => $val['is_private'],
					                                'status' => $val['status'],
					                                'is_crop' => $val['is_crop'],
					                                'source_img' => $val['source_img'],
					                                'updated_at' => date('Y-m-d H:i:s')
					                            );

					                            $gallery_image = $val['gallery_image'];
					                           

					                            
					                                // $get_gallery_image = 'https://moneoangels.com/public/images/profile_image/'.$gallery_image;
					                                // $galleryImageData = file_get_contents($get_gallery_image);

					                                

					                                // if(!file_exists(public_path('/images/profile_image').'/'.$gallery_image)) 
					                                // {
					                                    
					                                //         $destinationProfilePath = public_path('/images/profile_image');
					                                //         file_put_contents($destinationProfilePath.'/'.$gallery_image, $galleryImageData);
					                                // }
					                                
					                            

					                            DB::table('gallery_images')->insertGetId($gallery_images);
					                        }
					                }
					                /*for gallery images*/

					                /*for gallery images*/
					                $videos = $value['gallery_videos'];
					                
					                if(!empty($videos))
					                {
					                        foreach($videos as $val)
					                        {
					                            $gallery_videos = array(
					                                'user_id' => $user_id, 
					                                'gallery_video' => $val['gallery_video'],
					                                'is_private' => $val['is_private'],
					                                'status' => $val['status'],
					                                'updated_at' => date('Y-m-d H:i:s')
					                            );

					                            $gallery_video = $val['gallery_video'];
					                           

					                            

					                                

					                                /*if(!file_exists(public_path('/gallery_videos').'/'.$gallery_video)) 
					                                {
					                                    
					                                    $get_gallery_video = 'https://moneoangels.com/public/gallery_videos/'.$gallery_video;
					                                    $galleryVideoData = file_get_contents($get_gallery_video);

					                                        $destinationPath = public_path('/gallery_videos');
					                                        file_put_contents($destinationPath.'/'.$gallery_video, $galleryVideoData);
					                                }*/

					                            DB::table('gallery_videos')->insertGetId($gallery_videos);
					                        }
					                }
					                /*for gallery images*/
					                
					                //register user to third party chat
					                // $dataaa = [
					                //              "username"=>"Moneo",
					                //              "password"=>"Moneo2019@",
					                //              "service_code"=>"cd9271fa",
					                //              "name"=> $value['name'].'__'.$user_id,
					                //              "group"=> "subscriber",
					                //              "profile"=> [
					                //                             "avatar"=> "avatar.jpg",
					                //                             "age"=> $value['age'],
					                //                             "location"=> $value['state'],
					                //                             "images"=> [$value['profile_image']]
					                //                          ],
					                //               "additional_info"=> [
					                //                             "membership"=> "free",
					                //                             "link"=> "http://xyz.com",
					                //                             "about"=> "about"
					                //                          ],                                            
					                //              ];
					                //     $chh = curl_init();
					                //     curl_setopt($chh, CURLOPT_URL,"https://im.operatorplatform.com/api/users/add");
					                //     curl_setopt($chh, CURLOPT_POST, 1);
					                //     curl_setopt($chh, CURLOPT_POSTFIELDS,
					                //                 "data=".json_encode($dataaa));
					                //     curl_setopt($chh, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
					                    
					                //     curl_setopt($chh, CURLOPT_RETURNTRANSFER, true);                
					                //     $server_output = curl_exec ($chh);                
					                //     curl_close ($chh);
					                    
					                // $dataaa1 = [
					                //              "username"=>"Moneo",
					                //              "password"=>"Moneo2019@",
					                //              "service_code"=>"cd9271fa",
					                //              "name"=> $value['name'].'__'.$user_id,
					                //              "group"=> "persona",
					                //              "profile"=> [
					                //                             "avatar"=> "avatar.jpg",
					                //                             "age"=> $value['age'],
					                //                             "location"=> $value['state'],
					                //                             "images"=> [$value['profile_image']]
					                //                          ],
					                //               "additional_info"=> [
					                //                             "membership"=> "free",
					                //                             "link"=> "http://xyz.com",
					                //                             "about"=> "about"
					                //                          ],                                            
					                //              ];
					                //     $chh = curl_init();
					                //     curl_setopt($chh, CURLOPT_URL,"https://im.operatorplatform.com/api/users/add");
					                //     curl_setopt($chh, CURLOPT_POST, 1);
					                //     curl_setopt($chh, CURLOPT_POSTFIELDS,
					                //                 "data=".json_encode($dataaa1));
					                //     curl_setopt($chh, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
					                    
					                //     curl_setopt($chh, CURLOPT_RETURNTRANSFER, true);                
					                //     $server_output = curl_exec ($chh);                
					                //     curl_close ($chh);    
					                //register user to third party chat
				}
                
            }
            return json_encode(array('status'=>'true','message'=>'Profiles send successfully'));
        }
        else
        {
            return json_encode(array('status'=>'false','message'=>'Already sent')); 
        }

    }
    
    public function getRealProfiles(Request $request)
    {
            $allcities =  DB::table('users')->select('state')->groupBy('state')->get();
            foreach($allcities as $row){
                if($row->state){
                    $cities[] = $row->state;


                }
            }
             $cities[] = NULL;
             $cities[] ='';

             // print_r($cities); die;
            
            $type = $request->get('type');
            $sum ='';
            $real = [];
            if($type == 'number'){                
                foreach($cities as $row){                    
                    $count1 = DB::table('users')->where('state',$row)->where('status','1')->where('fake','0')->where('is_deleted','0')->whereBetween('age', array(18, 25))->count();                    
                    $count2 = DB::table('users')->where('state',$row)->where('status','1')->where('fake','0')->where('is_deleted','0')->whereBetween('age', array(26, 35))->count();
                    $count3 = DB::table('users')->where('state',$row)->where('status','1')->where('fake','0')->where('is_deleted','0')->whereBetween('age', array(36, 45))->count();
                    $count4 = DB::table('users')->where('state',$row)->where('status','1')->where('fake','0')->where('is_deleted','0')->whereBetween('age', array(46, 55))->count();
                    $count5 = DB::table('users')->where('state',$row)->where('status','1')->where('fake','0')->where('is_deleted','0')->whereBetween('age', array(56, 99))->count();
                  
                    $real[] = [$row, $count1, $count2,$count3,$count4,$count5]; 

                      $sum+= $count1+$count2+$count3+$count4+$count5;
                      "<br>";
                                                             
                }   

                 // print_r($sum);die;
                    
             }
            elseif($type == 'perc_state'){                
                foreach($cities as $row){
                    $totalstate  = DB::table('users')->where('state',$row)->where('status','1')->where('fake','0')->count();
                    $count1 = DB::table('users')->where('state',$row)->where('status','1')->where('fake','0')->whereBetween('age', array(18, 25))->count();
                    $perc1  = ($totalstate) ? round(($count1 / $totalstate)*100) : '0';
                    
                    $count2 = DB::table('users')->where('state',$row)->where('status','1')->where('fake','0')->whereBetween('age', array(26, 35))->count();
                    $perc2  = ($totalstate) ? round(($count2 / $totalstate)*100) : '0';
                    
                    $count3 = DB::table('users')->where('state',$row)->where('status','1')->where('fake','0')->whereBetween('age', array(36, 45))->count();
                    $perc3  = ($totalstate) ? round(($count3 / $totalstate)*100) : '0';
                    
                    $count4 = DB::table('users')->where('state',$row)->where('status','1')->where('fake','0')->whereBetween('age', array(46, 55))->count();
                    $perc4  = ($totalstate) ? round(($count4 / $totalstate)*100) : '0';
                    
                    $count5 = DB::table('users')->where('state',$row)->where('status','1')->where('fake','0')->whereBetween('age', array(56, 99))->count();
                    $perc5  = ($totalstate) ? round(($count5 / $totalstate)*100) : '0';
                    
                    $real[] = [$row, $perc1.'%', $perc2.'%' , $perc3.'%' , $perc4.'%' , $perc5.'%'];                                        
                }                
            }elseif($type == 'perc_age'){                
                $total_18_25  = DB::table('users')->whereBetween('age', array(18, 25))->where('status','1')->where('fake','0')->count();
                $total_26_35  = DB::table('users')->whereBetween('age', array(26, 35))->where('status','1')->where('fake','0')->count();
                $total_36_45  = DB::table('users')->whereBetween('age', array(36, 45))->where('status','1')->where('fake','0')->count();
                $total_46_55  = DB::table('users')->whereBetween('age', array(46, 55))->where('status','1')->where('fake','0')->count();
                $total_56_99  = DB::table('users')->whereBetween('age', array(56, 99))->where('status','1')->where('fake','0')->count();
                                
                foreach($cities as $row){                    
                    $count1 = DB::table('users')->where('state',$row)->where('status','1')->where('fake','0')->whereBetween('age', array(18, 25))->count();
                    $perc1  = ($total_18_25) ? round(($count1 / $total_18_25)*100) : '0';
                                        
                    $count2 = DB::table('users')->where('state',$row)->where('status','1')->where('fake','0')->whereBetween('age', array(26, 35))->count();
                    $perc2  = ($total_26_35) ? round(($count2 / $total_26_35)*100) : '0';
                                        
                    $count3 = DB::table('users')->where('state',$row)->where('status','1')->where('fake','0')->whereBetween('age', array(36, 45))->count();
                    $perc3  = ($total_36_45) ? round(($count3 / $total_36_45)*100) : '0';
                    
                    $count4 = DB::table('users')->where('state',$row)->where('status','1')->where('fake','0')->whereBetween('age', array(46, 55))->count();
                    $perc4  = ($total_46_55) ? round(($count4 / $total_46_55)*100) : '0';
                    
                    $count5 = DB::table('users')->where('state',$row)->where('status','1')->where('fake','0')->whereBetween('age', array(56, 99))->count();
                    $perc5  = ($total_56_99) ? round(($count5 / $total_56_99)*100) : '0';
                    
                    $real[] = [$row, $perc1.'%', $perc2.'%' , $perc3.'%' , $perc4.'%' , $perc5.'%'];                                                                                
                }
            }
         $male = DB::table('users')->where('gender','1')->where('status','1')->where('fake','0')->where('is_deleted','0')->count();
          $female = DB::table('users')->where('gender','2')->where('status','1')->where('fake','0')->where('is_deleted','0')->count();
           $activeReal = DB::table('users')->where('status','1')->where('fake','0')->where('is_deleted','0')->count();
                                    
            
return json_encode(array('status'=>'true','message'=>json_encode($real),'totalflirtfull'=>json_encode($sum),'activeReal'=>json_encode($activeReal)));

    }
    
    public function getRegion(){
        //print_r($_POST); die;
        if(!empty($_POST['name']))
        {
            $country = DB::table('countries')->where('name', '=', $_POST['name'])->first();        
            $state = DB::table('states')->where('country_id', '=', $country->id)->get();
            
            //foreach($state as $row){
            //    $cities[] = DB::table('cities')->where('state_id', '=', $row->id)->get();
            //}
        //echo "<pre>"; print_r($cities);die;
        
             echo '<option value="" >Region</option>';
            //foreach($state as $value){
                foreach($state as $row){
                    echo '<option value="'.$row->name.'" '.(($row->name == $_POST['selected'])? 'selected' : '').' >'.$row->name.'</option>';
                }
            //}   
        }
        else{
             echo '<option value="" >Region</option>';
        }
        
    }
    
    public function getCity(){
        //print_r($_POST); die;
        if(!empty($_POST['name']))
        {
            $country = DB::table('countries')->where('name', '=', $_POST['name'])->first();        
            $state = DB::table('states')->where('country_id', '=', $country->id)->get();
            
            foreach($state as $row){
                $cities[] = DB::table('cities')->where('state_id', '=', $row->id)->get();
            }
        //echo "<pre>"; print_r($cities);die;
        
             echo '<option value="" >City</option>';
            foreach($cities as $value){
                foreach($value as $row){
                    echo '<option value="'.$row->name.'" >'.$row->name.'</option>';
                }
            }   
        }
        else{
             echo '<option value="" >City</option>';
        }
        
    }
    
    public function getCityByState(){
        //print_r($_POST); die;
        if(!empty($_POST['name']))
        {
            $state = DB::table('states')->where('name', '=', $_POST['name'])->first();                    
                        
            $cities = DB::table('cities')->where('state_id', '=', $state->id)->get();
            
        //echo "<pre>"; print_r($cities);die;
        
             echo '<option value="" >City</option>';            
                foreach($cities as $row){
                    echo '<option value="'.$row->name.'" >'.$row->name.'</option>';
                }            
        }
        else{
             echo '<option value="" >City</option>';
        }
        
    }

    public function delete_my_account(Request $request)
    {
        $user_id = Auth::id();

        DB::table('users')->where('id', '=', $user_id)->update(array('is_deleted' => 1,'profile_deleted_at'=>date('Y-m-d'),'deleted_time'=>date('Y-m-d h:i:s')));

        return redirect('logout');
    }


    public function post_help_center(Request $request)
    {
        
        $rules = [
                'h_name' => 'required',
                'h_email' => 'required|email',
                // 'h_phone' => 'required',
                // 'h_address' => 'required',
                'h_message' => 'required',
            ];

        $messages = [
            'h_name.required' => 'Please enter name.',
            'h_email.required' => 'Please enter email.',
            'h_email.email' => 'Please enter valid email.',
            // 'h_phone.required' => 'Please enter phone no',
            // 'h_address.required' => 'Please enter address',
            'h_message.required' => 'Please enter message',
        ];
        $validator = Validator::make($request->all(),$rules,$messages);

        if ($validator->fails()) {
            return redirect('help-center')->withErrors($validator)->withInput();
        }
        else
        {
            $h_name = trim($request->h_name);
            $h_email = trim($request->h_email);
            // $h_phone = trim($request->h_phone);
            // $h_address = trim($request->h_address);
            $h_message = trim($request->h_message);
            
            $user_data = array(
                'name' => $h_name,
                 'email' => $h_email,
                'phone' => 0,//$h_phone,
                'address' => '', //$h_address,
                'message' => $h_message
            );

            //print_r($user_data);die;

            $help_us = DB::table('help_us')->insert($user_data);

            $email_content = array(
                            'receipent_email'   => 'admin@flirtfull.com',
                            'subject'           => 'Help Center Enquiry',
                            'email'        => $h_email,
                            'first_name'        => '',
                            'name'        => $h_name,
                            // 'address'        => $h_address,
                            // 'phone'        => $h_phone,
                            'message'        => $h_message,
                            'greeting'          => 'Flirtfull'
                        );
            $helper = new Helper;
            $email_response = $helper->sendMail(
                                    $email_content,
                                    'help_us'
                                );

            return redirect()->back()->with('success', 'Thank you for contact us. Will be reply soon you on your Email address.');
        }
    }
    
    public function fetch_users(Request $request)
    {
        $users = DB::table('users')->where('name', 'LIKE', '%'.$request->get('search').'%')->where('role_type', 3)->where('status', 1)->where('is_deleted', 0)->get();
        
        //echo "<pre>"; 
        $users_arr = [];
        foreach($users as $row)
        {
            $users_arr[] =  array("value"=>$row->id,"label"=>$row->name);
        }
        echo json_encode($users_arr);
    }
    
    public function random_users(Request $request)
    {
        $users = DB::table('users')->where('fake', 1)->where('is_deleted', 0)->inRandomOrder()->paginate(30);
        //print_r($users); die;
        $random_time = array(200,250,300,350,400);
        
        foreach($users as $row)
        {
            $userdata['user_id'] = $row->id;
            $userdata['time'] = time() + array_rand($random_time);
            DB::table('random_online_users')->insert($userdata);
        }
        
    }
    
    public static function makeUserLogin(Request $request)
    {
       
        $users = DB::table('users')->select('id')->where('is_login','=','0')->where('status', 1)->where('fake', 1)->where('is_deleted', 0)->inRandomOrder()->limit(1000)->get();

        //print_r($users); die;
        foreach ($users as $key => $value) {                    
            $random_time = rand(500,800);
            $random_time = time() + ($random_time);
            DB::table('users')->where('id', $value->id)->update(array("is_login"=>'1',"login_time"=>$random_time));
        }
    }

    public function check_online(Request $request)
    {
        $users = DB::table('users')->where('login_time','<=', time())->where('status', 1)->where('fake', 1)->where('is_deleted', 0)->get();
        
        //echo "<pre>"; print_r($users); die;
        foreach($users as $row)
        {            
            DB::table('users')->where("id",$row->id)->update(array("is_login"=>"0","login_time"=>"0"));
        }
        
    }

    public function check_online_real(Request $request)
    {
        $time = (time() - 1800); 
        $users = DB::table('users')->where('login_time','<=', $time)->where('is_login', 1)->where('is_deleted', 0)->get();
        //echo "<pre>"; print_r($users); die;
        foreach($users as $row)
        {            
            DB::table('users')->where("id",$row->id)->update(array("is_login"=>"0","login_time"=>"0"));
        }
        
    }
    
    
    public function update_outer_user(Request $request){
        
        //print_r($_POST); die;
        $name = $request->name;
        $userid = Auth::id();
        $users = DB::table('users')->where('name', $name)->where('id','!=', $userid)->count();
        
        if($users){
            echo json_encode(array('success' => false));
        }else{
            $dob = $request->year.'-'.$request->month.'-'.$request->day;
        
            $from = new DateTime($dob);
            $to   = new DateTime('today');
            $age = $from->diff($to)->y;
            
            $password = bcrypt($request->password);
            $state = $request->region;
            
            DB::table('users')->where("id",$userid)->update(array("name"=>$name,"dob"=>$dob,"age"=>$age,"password"=>$password,"state"=>$state));
            echo json_encode(array('success' => true));
        }            
    }
    
    
    public function send_activation_reminder(){
        $users = DB::table('users')->where('fake', 0)->where('activation_time','=', Null)->get();
        $helper = new Helper; 
        foreach($users as $row){
            if($row->created_at){            
                $t1 = strtotime( $row->created_at );
                $t2 = strtotime( date('Y-m-d h:i:s') );
                $diff = $t2 - $t1;
                $hours = round($diff / ( 60 * 60 ));
                //echo $row->id;echo "<br>";
                if($hours >= 24 && $hours < 72){
                    $is_sent = DB::table('email_reminder')->where('user_id', $row->id)->count();
                    if($is_sent){
                        
                    }else{
                        //send mail                                       
                        $subject2 = "Welcome to Flirting Sins! Verify your email address to get started";
                        $email_content2 = [
                                'receipent_email'=> $row->email,
                                'subject'=>$subject2,
                                'greeting'=> 'Flirting Sins',
                                'first_name'=> $row->name,
                                'template_id' => 1
                                ];
                        //$verification_email2 = $helper->sendMailFrontEnd($email_content2,'verification_link');
                        $verification_email2 = $helper->sendMailFrontEnd_1($email_content2,'register');
                        
                        $details['user_id'] = $row->id;
                        $details['count'] = 1;
                        DB::table('email_reminder')->insertGetId($details);
                        
                    }                    
                }
                
                if($hours >= 72){
                    $is_sent = DB::table('email_reminder')->where('user_id', $row->id)->where('count','=', 2)->count();
                    if($is_sent){
                        
                    }else{
                        //send mail                                       
                        $subject2 = "Welcome to Flirting Sins! Verify your email address to get started";
                        $email_content2 = [
                                'receipent_email'=> $row->email,
                                'subject'=>$subject2,
                                'greeting'=> 'Flirting Sins',
                                'first_name'=> $row->name,
                                'template_id' => 1
                                ];
                        //print_r($email_content2);
                        //$verification_email2 = $helper->sendMailFrontEnd($email_content2,'verification_link');
                        $verification_email2 = $helper->sendMailFrontEnd_1($email_content2,'register');
                        
                        $details['count'] = 2;
                        DB::table('email_reminder')
                            ->where('user_id',$row->id)
                            ->update($details); 
                    }                    
                }
            }
        }
         echo "success"; die;
    }
    
    public function get_messages_cron(){
        $acc = ServiceAccount::fromJsonFile('/var/www/html/flirtengine-firebase-adminsdk-94hsi-4dac5384f1.json');
        $firebase = (new Factory)->withServiceAccount($acc)->create();
        $this->database = $firebase->getDatabase();

        $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL,"https://triggersengine.com/api/v1/getMessagesCron");
            curl_setopt($ch, CURLOPT_POST, 0);
                            
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                
            $server_output = curl_exec ($ch);
            //print_r($server_output);
            curl_close ($ch);        
            //$arr = '[{"id":6122,"sender_id":4639,"receiver_id":4890,"room_id":"46399658904890","subject":"hot picture","message":"Why dont you have a hotter","attachment":"null","message_type":1,"message_time":"1563529523","is_send":0,"is_mail_send":1,"status":1,"notification_status":1,"like_sender":null,"like_receiver":null,"created_at":"2019-07-19 09:45:37","updated_at":"2019-07-19 09:45:37","source":1}]';        
            $arr = json_decode($server_output);
            //print_r($arr->data);                die;
            if($arr->status){
                foreach($arr->data as $row){
                                                        
                    
                            $msgText = $row->message;
                            $messageType =  'text';
                            $imageText =  '';
                            
                            
                            
                            if($row->message_type == 1){                                
                                if($row->attachment != '0'){                                    
                                    $messageType = 'file';
                                    $imageText = $msgText;
                                    $msgText = $row->attachment;
                                }
                            }else if($row->message_type == 2){
                                $messageType = 'proposal';
                                $msgText = '<img src="https://www.flirtfull.com/public/images/profile_image/'.$msgText.'" style="margin:2px">';
                            }else if($row->message_type == 3){
                                $messageType = 'likes';
                            }else if($row->message_type == 4){
                                $messageType = 'favorite';
                            }
                            
                            $chatRoom = $row->sender_id.'_'.$row->receiver_id;
                            if($row->receiver_id < $row->sender_id){
                                $chatRoom = $row->receiver_id.'_'.$row->sender_id;
                            }
                            
                            $snapshot1 = $this->get1($row->sender_id);
                            //print_r($snapshot1); die;
                            $snap1 = $this->get($row->receiver_id,$row->sender_id);            
                            $insertData = [
                                "deletteby" => '',
                                "message" => $msgText,
                                "messageCount" => $snap1['messageCount'] + 1,
                                "name" => $snapshot1['userName'],
                                "profilePic" => $snapshot1['userImage'],
                                "timestamp" => time() * 1000,
                                "uid" => $row->sender_id,
                                "historyType" => 2,
                                "lastSenderId" => $row->sender_id,
                                "messageType" => $messageType,
                                "historyLoad" => '0',
                            ];
                                                        
                            $this->insert($insertData,$row->receiver_id,$row->sender_id);
                            
                            $snapshot2 = $this->get1($row->receiver_id);
                            $snap2 = $this->get($row->sender_id,$row->receiver_id);
                            $insertData1 = [
                                "deletteby" => '',
                                "message" => $msgText,
                                "messageCount" => $snap2['messageCount'] + 1,
                                "name" => $snapshot2['userName'],
                                "profilePic" => $snapshot2['userImage'],
                                "timestamp" => time() * 1000,
                                "uid" => $row->receiver_id,
                                "historyType" => 2,
                                "lastSenderId" => $row->sender_id,
                                "messageType" => $messageType,
                                "historyLoad" => '0',
                            ];
                            $this->insert($insertData1,$row->sender_id,$row->receiver_id);
                            
                            $insertData3 = [
                                            "deletteby" => '',
                                            "message" => $msgText,
                                            "messageCount"=> 0,
                                            "name" => $snapshot1['userName'],
                                            "profilePic" => $snapshot1['userImage'],
                                            "timestamp" => time() * 1000,
                                            "uid" => $row->sender_id,
                                            "messageType" => $messageType,
                                            "imageText" => $imageText,
                                        ];
                            $this->insert1($insertData3,$chatRoom);
                    
                    //print_r($row);   die;   
                    sleep(7); // this should halt for 3 seconds for every loop
                }
            }
        }
        
        public function insert(array $data,$one,$two) {
            //echo "<pre>";print_r($data); die;
            if (empty($data) || !isset($data)) { return FALSE; }
            foreach ($data as $key => $value){
                $this->database->getReference()->getChild('chat_history/'.$one)->getChild($two)->getChild($key)->set($value);
            }
            return TRUE;
        }
        
        public function insert1(array $data,$chatRoom) {            
            //echo "<pre>";print_r($data); die;
            if (empty($data) || !isset($data)) { return FALSE; }
            //foreach ($data as $key => $value){
                $this->database->getReference()->getChild('chat_rooms/')->getChild($chatRoom)->push($data);
            //}
            return TRUE;
        }
        
        public function get($one, $two){
           
           if (empty($one) || !isset($one)) { return FALSE; }
           if ($this->database->getReference('chat_history/'.$one)->getSnapshot()->hasChild($two)){
               return $this->database->getReference('chat_history/'.$one)->getChild($two)->getValue();
           } else {
               return FALSE;
           }
       }
       
       public function get1($one){
           if (empty($one) || !isset($one)) { return FALSE; }
           if ($this->database->getReference('users/'.$one)->getValue()){
               return $this->database->getReference('users/'.$one)->getValue();
           } else {
               return FALSE;
           }
       }
       
       
       public function get_messages_cron_test(){
        $acc = ServiceAccount::fromJsonFile('/var/www/html/flirtengine-firebase-adminsdk-94hsi-4dac5384f1.json');
        $firebase = (new Factory)->withServiceAccount($acc)->create();
        $this->database = $firebase->getDatabase();

        $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL,"https://triggersengine.com/api/v1/getMessagesCron");
            curl_setopt($ch, CURLOPT_POST, 0);
                            
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                
            $server_output = curl_exec ($ch);
            //print_r($server_output);
            curl_close ($ch);        
            //$arr = '[{"id":6122,"sender_id":4639,"receiver_id":4890,"room_id":"46399658904890","subject":"hot picture","message":"Why dont you have a hotter","attachment":"null","message_type":1,"message_time":"1563529523","is_send":0,"is_mail_send":1,"status":1,"notification_status":1,"like_sender":null,"like_receiver":null,"created_at":"2019-07-19 09:45:37","updated_at":"2019-07-19 09:45:37","source":1}]';        
            $arr = json_decode($server_output);
            //print_r($arr->data);                die;
            if($arr->status){
                foreach($arr->data as $row){
                                                        
                    
                            $msgText = $row->message;
                            $messageType =  'text';
                            $imageText =  '';
                            
                            
                            
                            if($row->message_type == 1){                                
                                if($row->attachment != '0'){                                    
                                    $messageType = 'file';
                                    $imageText = $msgText;
                                    $msgText = $row->attachment;
                                }
                            }else if($row->message_type == 2){
                                $messageType = 'proposal';
                                $msgText = '<img src="https://www.flirtfull.com/public/images/profile_image/'.$msgText.'" style="margin:2px">';
                            }else if($row->message_type == 3){
                                $messageType = 'likes';
                            }else if($row->message_type == 4){
                                $messageType = 'favorite';
                            }
                            
                            $chatRoom = $row->sender_id.'_'.$row->receiver_id;
                            if($row->receiver_id < $row->sender_id){
                                $chatRoom = $row->receiver_id.'_'.$row->sender_id;
                            }
                            
                            $snapshot1 = $this->get1($row->sender_id);
                            //print_r($snapshot1); die;
                            $snap1 = $this->get($row->receiver_id,$row->sender_id);            
                            $insertData = [
                                "deletteby" => '',
                                "message" => $msgText,
                                "messageCount" => $snap1['messageCount'] + 1,
                                "name" => $snapshot1['userName'],
                                "profilePic" => $snapshot1['userImage'],
                                "timestamp" => time() * 1000,
                                "uid" => $row->sender_id,
                                "historyType" => 2,
                                "lastSenderId" => $row->sender_id,
                                "messageType" => $messageType,
                                "historyLoad" => '0',
                            ];
                                                        
                            $this->insert($insertData,$row->receiver_id,$row->sender_id);
                            
                            $snapshot2 = $this->get1($row->receiver_id);
                            $snap2 = $this->get($row->sender_id,$row->receiver_id);
                            $insertData1 = [
                                "deletteby" => '',
                                "message" => $msgText,
                                "messageCount" => $snap2['messageCount'] + 1,
                                "name" => $snapshot2['userName'],
                                "profilePic" => $snapshot2['userImage'],
                                "timestamp" => time() * 1000,
                                "uid" => $row->receiver_id,
                                "historyType" => 2,
                                "lastSenderId" => $row->sender_id,
                                "messageType" => $messageType,
                                "historyLoad" => '0',
                            ];
                            $this->insert($insertData1,$row->sender_id,$row->receiver_id);
                            
                            $insertData3 = [
                                            "deletteby" => '',
                                            "message" => $msgText,
                                            "messageCount"=> 0,
                                            "name" => $snapshot1['userName'],
                                            "profilePic" => $snapshot1['userImage'],
                                            "timestamp" => time() * 1000,
                                            "uid" => $row->sender_id,
                                            "messageType" => $messageType,
                                            "imageText" => $imageText,
                                        ];
                            $this->insert1($insertData3,$chatRoom);
                    
                    //print_r($row);   die;   
                    sleep(2); // this should halt for 3 seconds for every loop
                }
            }
        }
    
    public function test(Request $request)
    {
        
            $users = DB::table('users')->where('fake','1')->get();
            //print_r($users); die;
            //foreach($users as $row){
            //    $name = explode('@',$row->email);
            //    echo $name[0];
            //    DB::table('users')->where('id',$row->id)->update(array('name'=>$name[0]));
            //}
            
            //foreach($users as $row){
            //    $country = 'United Kingdom';                
            //    DB::table('users')->where('id',$row->id)->update(array('country'=>$country));
            //}
            
            //foreach($users as $row){
            //    $region = DB::table('states')->inRandomOrder()->limit(1)->get();
            //    $state = ($region[0]->name);
            //    DB::table('users')->where('id',$row->id)->update(array('state'=>$state));
            //}
            //die;
            
            return view('test',compact());
        

    }

    public function newchatroom(){  
        $acc = ServiceAccount::fromJsonFile('/var/www/html/flirtengine-firebase-adminsdk-94hsi-4dac5384f1.json');
        $firebase = (new Factory)->withServiceAccount($acc)->create();
        $this->database = $firebase->getDatabase();
        for($i=10; $i<=20;$i++){
        		$id = $i;
        		//$dataa = [];
        		$data[$id] =  [
                            "deletteby" => '',
                            "message" => 'How are you',
                            "messageCount"=> 0,
                            "name" => 'test',
                            "profilePic" => 'img',
                            "timestamp" => time() * 1000,
                            "uid" => '123',                            
                            "chat_room" => $i
                        ];
                
              }          
               // echo "<pre>"; print_r($data); die;     
                //$res = $this->database->getReference('new_chat_rooms')->orderByChild('chat_room')->getValue();    
                //echo "<pre>"; print_r($res); die;
        return view('newchatroom',compact());            
                           
                $this->database->getReference()->getChild('new_chat_rooms')->push($data);     
                    
    }

    public function getRandomFakeUser(){
        $gender = 2;
        // if (Auth::check()) {                
        //     $login = DB::table('users')->where('users.id','=',Auth::id())->first();        
        //     $gender = $login->seeking_val;
        // } 

        $user = DB::table('users')->select('id','name','profile_image')->where('is_login',1)->where('gender',$gender)->where('fake',1)->where('status',1)->inRandomOrder()->first();
        
            echo json_encode(["id"=>$user->id,"name"=>$user->name,"image"=>$user->profile_image]);            
    }

    public function wallet(Request $request)
    {
        $wallet = [];
        if(Auth::check()){
            $userid = Auth::id();
            $wallet = DB::table('user_wallet')->where('user_id',$userid)->first();      
        }
        //print_r($wallet); die;
        return view('wallet',compact('wallet'));        
    }

    public function credits(Request $request)
    {
        
            return view('credits',compact());        
    }

    public function premiums(Request $request)
    {
        
            return view('premiums',compact());        
    }

    public function GetRealUsersMY(Request $request)
    {
     
            
            if(Input::get("type")){
                 $type = Input::get("type");   
            }else{
               $type = 'number';
             }
        
            
            //   $allcities =  DB::table('users')->select('state')->where('country',$country)->groupBy('state')->get();
            // foreach($allcities as $row){
            //     if($row->state){
            //         $cities[] = $row->state;
            //     }
            // }
             
$real = [];
            if($type == 'number'){      

           $noreg1 = DB::table('users')
            ->where('fake','0')
            ->where('status','1')
            // ->where('country',$country)
            ->whereBetween('age', array(18, 25))
            ->where(function ($query) {
                $query->where('state', '=', NULL)
                      ->orWhere('state', '=', '');
            })
            ->count();

            $noreg2 = DB::table('users')
            ->where('fake','0')
            ->where('status','1')
            // ->where('country',$country)
            ->whereBetween('age', array(26, 35))
            ->where(function ($query) {
                $query->where('state', '=', NULL)
                      ->orWhere('state', '=', '');
            })
            ->count();
            $noreg3 = DB::table('users')
            ->where('fake','0')
            ->where('status','1')
            // ->where('country',$country)
            ->whereBetween('age', array(36, 45))
            ->where(function ($query) {
                $query->where('state', '=', NULL)
                      ->orWhere('state', '=', '');
            })
            ->count();
            $noreg4 = DB::table('users')
            ->where('fake','0')
            ->where('status','1')
            // ->where('country',$country)
            ->whereBetween('age', array(46, 55))
            ->where(function ($query) {
                $query->where('state', '=', NULL)
                      ->orWhere('state', '=', '');
            })
            ->count();

             $noreg5 = DB::table('users')
            ->where('fake','0')
            ->where('status','1')
            // ->where('country',$country)
            ->whereBetween('age', array(56, 99))
            ->where(function ($query) {
                $query->where('state', '=', NULL)
                      ->orWhere('state', '=', '');
            })
            ->count();

           $fulldata = [$noreg1,$noreg2,$noreg3,$noreg4,$noreg5];
    // print_r($fulldata); die;
        
      $count1 = DB::table('users')->select(DB::raw('count(*) as user_count, state'))->where('status','1')->where('state','!=',NULL)->where('state','!=','')->where('fake','0')->whereBetween('age', array(18, 25))->groupBy('state')->get();
      $count2 = DB::table('users')->select(DB::raw('count(*) as user_count, state'))->where('status','1')->where('state','!=',NULL)->where('state','!=','')->where('fake','0')->whereBetween('age', array(26, 35))->groupBy('state')->get();
      $count3 = DB::table('users')->select(DB::raw('count(*) as user_count, state'))->where('status','1')->where('state','!=',NULL)->where('state','!=','')->where('fake','0')->whereBetween('age', array(36, 45))->groupBy('state')->get();
      $count4 = DB::table('users')->select(DB::raw('count(*) as user_count, state'))->where('status','1')->where('state','!=',NULL)->where('state','!=','')->where('fake','0')->whereBetween('age', array(46, 55))->groupBy('state')->get();
      $count5 = DB::table('users')->select(DB::raw('count(*) as user_count, state'))->where('status','1')->where('state','!=',NULL)->where('state','!=','')->where('fake','0')->whereBetween('age', array(56, 99))->groupBy('state')->get();

     $real = [ $count1, $count2,$count3,$count4,$count5];       
                                                     
            }elseif($type == 'perc_state')   {
                  $totalR = DB::table('users')->where('status','1')->where('country','United Kingdom')->where('fake','0')->count();
                    
              // $totalR =  $pfTotal[0]->user_count+$pfTotal[1]->user_count; 
               // print_r($pfTotal);die;
                 //no region start
             $noreg1 = DB::table('users')
            ->where('fake','0')
            ->where('status','1')
            ->where('country','United Kingdom')
            ->whereBetween('age', array(18, 25))
            ->where(function ($query) {
                $query->where('state', '=', NULL)
                      ->orWhere('state', '=', '');
            })
            ->count();
            // print_r($noreg1); die;

            $pcent_noreg1 = ($totalR) ? round((($noreg1 / $totalR)*100),2) : 0;

            $noreg2 = DB::table('users')
            ->where('fake','0')
            ->where('status','1')
            // ->where('country','United Kingdom')
            ->whereBetween('age', array(26, 35))
            ->where(function ($query) {
                $query->where('state', '=', NULL)
                      ->orWhere('state', '=', '');
            })
            ->count();
            $pcent_noreg2 = ($totalR) ? round((($noreg2 / $totalR)*100),2) : 0;


            $noreg3 = DB::table('users')
            ->where('fake','0')
            ->where('status','1')
            // ->where('country','United Kingdom')
            ->whereBetween('age', array(36, 45))
            ->where(function ($query) {
                $query->where('state', '=', NULL)
                      ->orWhere('state', '=', '');
            })
            ->count();
            $pcent_noreg3 = ($totalR) ? round((($noreg3 / $totalR)*100),2) : 0;

            $noreg4 = DB::table('users')
            ->where('fake','0')
            ->where('status','1')
            // ->where('country','United Kingdom')
            ->whereBetween('age', array(46, 55))
            ->where(function ($query) {
                $query->where('state', '=', NULL)
                      ->orWhere('state', '=', '');
            })
            ->count();
            $pcent_noreg4 = ($totalR) ? round((($noreg4 / $totalR)*100),2) : 0;

             $noreg5 = DB::table('users')
            ->where('fake','0')
            ->where('status','1')
            // ->where('country','United Kingdom')
            ->whereBetween('age', array(56, 99))
            ->where(function ($query) {
                $query->where('state', '=', NULL)
                      ->orWhere('state', '=', '');
            })
            ->count();
           $pcent_noreg5 = ($totalR) ? round((($noreg5 / $totalR)*100),2) : 0;
           
           $fulldata = [$pcent_noreg1."%",$pcent_noreg2."%",$pcent_noreg3."%",$pcent_noreg4."%",$pcent_noreg5."%"];
           // print_r($fulldata); die;
          
                 //no region end

              
                 $count1 = DB::table('users')->select(DB::raw('count(*) as user_count, state'))->where('status','1')->where('state','!=',NULL)->where('state','!=','')->where('fake','0')->whereBetween('age', array(18, 25))->groupBy('state')->get();
                 foreach($count1 as $key=>$value1) {
                  $prec1[] = ($totalR) ? round((($value1->user_count / $totalR )*100),2) : 0;
                  // print_r($totalR[$key+2]);
                  }
         
                   
          
                    $count2 = DB::table('users')->select(DB::raw('count(*) as user_count, state'))->where('status','1')->where('state','!=',NULL)->where('state','!=','')->where('fake','0')->whereBetween('age', array(26, 35))->groupBy('state')->get();

                    foreach($count2 as $key=>$value2) {
                  $prec2[] = ($totalR) ? round((($value2->user_count / $totalR )*100),2) : 0;
                 // print_r($totalR[$key+2]);
                 
                 }

                       $count3 = DB::table('users')->select(DB::raw('count(*) as user_count, state'))->where('status','1')->where('state','!=',NULL)->where('state','!=','')->where('fake','0')->whereBetween('age', array(36, 45))->groupBy('state')->get();

                      foreach($count3 as $key=>$value3) {
                  $prec3[] = ($totalR) ? round((($value3->user_count / $totalR )*100),2) : 0;
                 // print_r($totalR[$key+2]);
                 
                 }

                      $count4 = DB::table('users')->select(DB::raw('count(*) as user_count, state'))->where('status','1')->where('state','!=',NULL)->where('state','!=','')->where('fake','0')->whereBetween('age', array(46, 55))->groupBy('state')->get();

                      foreach($count4 as $key=>$value4) {
                  $prec4[] = ($totalR) ? round((($value4->user_count / $totalR )*100),2) : 0;
                 // print_r($totalR[$key+2]);
                 
                 }

                      $count5 = DB::table('users')->select(DB::raw('count(*) as user_count, state'))->where('status','1')->where('state','!=',NULL)->where('state','!=','')->where('fake','0')->whereBetween('age', array(56, 99))->groupBy('state')->get();

                             foreach($count5 as $key=>$value5) {
                          $prec5[] = ($totalR) ? round((($value5->user_count / $totalR )*100),2) : 0;
                         // print_r($totalR[$key+2]);
                         
                         }


                    $real = [$prec1,$prec2,$prec3,$prec4,$prec5];
                    
               
               }     

                
                return json_encode(array('status'=>'true','realdata'=>json_encode($real),'realnoReg'=>json_encode($fulldata),
                  ));
    }
}