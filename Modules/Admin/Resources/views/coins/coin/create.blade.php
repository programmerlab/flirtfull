@extends('admin::layouts.master')
 
    @section('content') 
      @include('admin::partials.navigation')
      @include('admin::partials.breadcrumb')   

       @include('admin::partials.sidebar')  
      <div class="panel panel-white"> 
        
 
        <div class="panel panel-flat">
                      <div class="panel-heading">
                    <h6 class="panel-title"><b>{{$page_action ?? ''}}</b><a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
                    <div class="heading-elements">
                      <ul class="icons-list">
                        <li> <a type="button" href="{{route('coin')}}" class="btn btn-primary text-white   btn-rounded "> View {{$page_title}}<span class="legitRipple-ripple" ></span></a></li> 
                      </ul>
                    </div>
                  </div> 
            </div>
                      
                     <div class="panel-body">
                           
                            <!-- END BEGIN PROFILE SIDEBAR -->
                            <!-- BEGIN PROFILE CONTENT -->
                            <div class="profile-content col-md-10">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="portlet light bordered">
                                            
                                    
                                    
                                    {!! Form::model($coin, ['route' => ['coin.store'],'class'=>'form-basic ui-formwizard coin-form','id'=>'coins_form','enctype'=>'multipart/form-data']) !!}
                                    <div class="portlet-body">
                                        <div class="tab-content">
                                            <!-- PERSONAL INFO TAB --> 
                                                <div class="margin-top-10">
                                                    @if (count($errors) > 1000)
                                                      <div class="alert alert-danger">
                                                          <ul>
                                                              @foreach ($errors->all() as $error)
                                                                  <li>{!! $error !!}</li>
                                                              @endforeach
                                                          </ul>
                                                      </div>
                                                    @endif
                                                </div>

                                                <div class="form-group {{ $errors->first('qty', ' has-error') }}">
                                                <label class="control-label col-md-3">Quantity <span class="required"> * </span></label>
                                                <div class="col-md-12"> 
                                                    {!! Form::text('qty',null, ['class' => 'form-control','data-required'=>1])  !!} 
                                                    
                                                    <span class="help-block">{{ $errors->first('qty', ':message') }}</span>
                                                </div>
                                            </div>
                                            
                                            <div class="form-group {{ $errors->first('discount', ' has-error') }}">
                                                <label class="control-label col-md-3">Discount (in %) <span class="required"> * </span></label>
                                                <div class="col-md-12"> 
                                                    {!! Form::text('discount',null, ['class' => 'form-control','data-required'=>1])  !!} 
                                                    
                                                    <span class="help-block">{{ $errors->first('discount', ':message') }}</span>
                                                </div>
                                            </div>
                                                
                                                
                                             <div class="form-group {{ $errors->first('price', ' has-error') }}">
                                                <label class="control-label col-md-3">Price <span class="required"> * </span></label>
                                                <div class="col-md-12"> 
                                                    {!! Form::text('price',null, ['class' => 'form-control','data-required'=>1])  !!} 
                                                    
                                                    <span class="help-block">{{ $errors->first('price', ':message') }}</span>
                                                </div>
                                            </div>
                                                
                                                
                                            <div class="form-group {{ $errors->first('txn_fee', ' has-error') }}">
                                                <label class="control-label col-md-3">Transaction Charge <span class="required"> * </span></label>
                                                <div class="col-md-12"> 
                                                    {!! Form::text('txn_fee',null, ['class' => 'form-control','data-required'=>1])  !!} 
                                                    
                                                    <span class="help-block">{{ $errors->first('txn_fee', ':message') }}</span>
                                                </div>
                                            </div>                 
                          
                              
                                           <div class="col-md-12">
                                                  <div class="margin-top-10"> 
                                                       <button type="submit" class="btn green btn-primary" value="avtar" name="submit"> Save Changes </button>
                                                      <button type="reset" class="btn default "> Cancel </button>
                                                  </div> 
                                                 
                                           </div>

                                            {!! Form::close() !!} 
                                            
                                             
                                        </div>

                                    </div>
                                    </form>
                                </div>
                            </div>
                        
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
        
@stop