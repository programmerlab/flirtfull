<?php

declare(strict_types=1);

namespace Modules\Admin\Http\Controllers;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Redirect;
use Input;
use Modules\Admin\Http\Requests\ProposalCategoryRequest;
use Modules\Admin\Models\ProposalCategory;
use Route;
use View;
use Session;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Datatables;


/**
 * Class AdminController
 */
class ProposalCategoryController extends Controller
{
    /**
     * @var  Repository
     */

    /**
     * Displays all admin.
     *
     * @return \Illuminate\View\View
     */
    public function __construct()
    {
        $this->middleware('admin');
        View::share('viewPage', 'Proposal');
        View::share('helper', new Helper);
        View::share('heading', 'ProposalCategory Category');
        View::share('route_url', route('proposalCategory'));

        $this->record_per_page = Config::get('app.record_per_page');
    }

    protected $proposalCategory;

    /*
     * Dashboard
     * */

    public function index(ProposalCategory $proposalCategory, Request $request)
    {        
        $page_title  = 'ProposalCategory Category';
        $page_action = 'ProposalCategory';
        

        if ($request->ajax()) {
            $id           = $request->get('id');
            $status       = $request->get('status');
            $proposalCategory         = ProposalCategory::find($id);
            $s            = ($status == 1) ? $status=0:$status=1;
            $proposalCategory->status = $s;
            $proposalCategory->save();
            echo $s;

            exit();
        }
        
        $proposalCategory = ProposalCategory::orderBy('id', 'desc')->where('is_deleted', '0')->Paginate($this->record_per_page);
        //print_r($proposalCategory); die;        

        $js_file = ['common.js','bootbox.js','formValidate.js'];

        return view('admin::proposalCategory.proposalCategory.index', compact('js_file', 'status', 'proposalCategory', 'page_title', 'page_action'));
    }


    public function anyData()
    {
        return Datatables::of(ProposalCategory::query())->make(true);
    }

    public function test_table(ProposalCategory $proposalCategory, Request $request)
    {        
        $page_title  = 'Admin ProposalCategory';
        $page_action = 'Admin ProposalCategory';
        

        if ($request->ajax()) {
            $id           = $request->get('id');
            $status       = $request->get('status');
            $proposalCategory         = ProposalCategory::find($id);
            $s            = ($status == 1) ? $status=0:$status=1;
            $proposalCategory->status = $s;
            $proposalCategory->save();
            echo $s;

            exit();
        }
        // Search by name ,email and group
        $search    = Input::get('search');
        $status    = Input::get('status');
        $role_type = Input::get('role_type');

        if ((isset($search) && !empty($search)) or  (isset($status) && !empty($status)) or !empty($role_type)) {
            $search = isset($search) ? Input::get('search') : '';

            $proposalCategory = ProposalCategory::where(function ($query) use ($search,$status,$role_type) {
                if (!empty($search)) {
                    $query->Where('first_name', 'LIKE', "%$search%")
                        ->OrWhere('last_name', 'LIKE', "%$search%")
                        ->OrWhere('email', 'LIKE', "%$search%");
                }

                if (!empty($status)) {
                    
                    $status =  ($status == 'active')?1:0;
                    $query->Where('status', $status);
                }

                if ($role_type) {
                    $query->Where('role_type', $role_type);
                }
            })->where('role_type', 3)->Paginate($this->record_per_page);
        } else {
            $proposalCategory = ProposalCategory::orderBy('id', 'desc')->where('role_type', 3)->where('is_deleted', 0)->Paginate($this->record_per_page);
        }
        $roles = Roles::all();

        $js_file = ['common.js','bootbox.js','formValidate.js'];

        return view('admin::proposalCategory.proposalCategory.test_table', compact('js_file', 'roles', 'status', 'page_title', 'page_action', 'roles' ,'role_type'));
    }

    /*
     * create Group method
     * */

    public function create(ProposalCategory $proposalCategory)
    {
        $page_title  = 'ProposalCategory';
        $page_action = 'Create ProposalCategory';
        
        $js_file     = ['common.js','bootbox.js','formValidate.js'];


        return view('admin::proposalCategory.proposalCategory.create', compact('js_file', 'proposalCategory', 'page_title', 'page_action'));
    }

    /*
     * Save Group method
     * */

    public function store(ProposalCategoryRequest $request, ProposalCategory $proposalCategory)
    {
        
        $proposalCategory->fill(Input::all());
        
        
        $action = $request->get('submit');        
        
        
        $proposalCategory->save();
        //echo "<pre>"; print_r(Input::all()); die;
        $js_file = ['common.js','bootbox.js','formValidate.js'];

        return Redirect::to(route('proposalCategory'))
            ->with('flash_alert_notice', 'New record successfully created.');
    }

    /*
     * Edit Group method
     * @param
     * object : $proposalCategory
     * */

    public function edit(ProposalCategory $proposalCategory)
    {
        //echo "<pre>"; print_r($proposalCategory); die;
        $page_title  = 'ProposalCategory';
        $page_action = 'Show ProposalCategory';                
        
        $js_file     = ['common.js','bootbox.js','formValidate.js'];
                
        //echo "<pre>"; print_r($proposalCategory); die;
        return view('admin::proposalCategory.proposalCategory.edit', compact('js_file', 'proposalCategory', 'page_title', 'page_action'));
    }

    public function update(Request $request, ProposalCategory $proposalCategory, ProposalCategoryRequest $proposalCategoryrequest)
    {        
        //echo "<pre>"; print_r($proposalCategory->password); die;        
        $proposalCategory->fill(Input::all());
        $action          = $request->get('submit');
        $proposalCategory->save();
                
        if ($request->get('role') == 3) {
            $Redirect = 'clientproposalCategory';
        } else {
            $Redirect = 'proposalCategory';
        }

        return Redirect::to(route($Redirect))
            ->with('flash_alert_notice', 'Record successfully updated.');
    }
    /*
     *Delete ProposalCategory
     * @param ID
     *
     */
    public function destroy(Request $request, $proposalCategory)
    { //print_r($proposalCategory);
        //echo $proposalCategory->id;die;
        $proposalCategory->is_deleted = 1;
        $proposalCategory->save();
        //$proposalCategory->delete();
        return Redirect::to(route('proposalCategory'))
            ->with('flash_alert_notice', 'Record  successfully deleted.');
    }

    public function show(ProposalCategory $proposalCategory)
    {
    }
    
    
    
}
