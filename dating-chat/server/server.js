"use strict";
const path = require("path");
const http = require("http");
const publicPath = path.join(__dirname, "../public");
const express = require("express");
const socketIO = require("socket.io");
var bodyParser = require('body-parser')
// const client = require("socket.io").listen(4000).sockets;
var mongoose = require("mongoose");
var app = express();
app.use(bodyParser.json()); // for parsing application/json
//app.use(bodyParser.urlencoded({ extended: false }));
var server = http.createServer(app);
var io = socketIO(server);
const {
    generateTextMessage,
    generateFileMessage,
    generateLocationMessage
} = require("./utils/message.js");
var uri = "mongodb+srv://@crm-2laff.mongodb.net/test";
mongoose.connect(
    uri,
    {
        auth: {
            user: "torenia",
            password: "torenia@1mongo"
        }
    }
);
var Schema = {
    senderId: String,
    senderName: String,
    receiverId: String,
    receiverName: String,
    chatId: String,
    message: String,
    readStatus: Boolean,
    answereStatus: Boolean,
    senderImageUrl : String,
    receiverImageUrl : String,
    type : String,
    fileText : String,
    fileName : String,
};
var readSchema = {
    status1 : Boolean,
    status2 : Boolean,
    chatId : String,
    readStatus : Boolean,
    answereStatus : Boolean
}
var readModel = mongoose.model("readstatus", readSchema);
var modal = mongoose.model("chats", Schema);
console.log("MongoDB connected...");
const { isRealString } = require("./utils/validation.js");
const { Users } = require("./utils/users.js");

var users = new Users();

app.use(express.static(publicPath));

io.on("connection", socket => {
    console.log("New user connected");

    socket.on("join", (params, callback) => {
        console.log("asd", params);
        if (!isRealString(params.name) && !isRealString(params.room)) {
            return callback("Name and Room name are required");
        }
        readModel.find({chatId : params.room}, (err,data)=>{
            if(err){
                console.log(err);_
            }else{
                if(data.length==0){
                    readModel.create({chatId : params.room, status1 : false, status2 : false, readStatus: false, answereStatus : false}, (err, response)=>{
                        if(err){
                            console.log(err);
                        }else{
                            console.log("read Status data inserted");
                        }
                    })
                }
            }
        });
        
      
       
        
        socket.join(params.room);
        users.removeUser(socket.id);
        users.addUser(socket.id, params.name, params.room);

        io.to(params.room).emit(
            "updateUserList",
            users.getUserList(params.room)
        );

        //socket.emit("newMessage", generateMessage("", ""));
        //modal.find({ chatId: params.room }, (err, data) => {
        //    socket.emit("history", data);
        //});

        socket.broadcast
            .to(params.room)
            .emit("newMessage", generateTextMessage("", ``));

        callback();
    });
    //socket.on("onread", (readData, callback) => {
    //    modal.updateMany(
    //        { chatId: readData.messageId },
    //        { readStatus: true },
    //        (err, data) => {
    //            if (err) {
    //                console.log(err);
    //            } else {
    //                console.log(updated);
    //            }
    //        }
    //    );
    //});
    //socket.on("onanswered", (readData, callback) => {
    //    modal.updateMany(
    //        { chatId: readData.messageId },
    //        { answereStatus: true },
    //        (err, data) => {
    //            if (err) {
    //                console.log(err);
    //            } else {
    //                console.log(updated);
    //            }
    //        }
    //    );
    //});
    socket.on("createMessage", (message, callback) => {
        var user = users.getUser(socket.id);

        
        if(message.type==="text"){
            if (user && isRealString(message.text)) {
                io.to(user.room).emit(
                    "newMessage",
                    generateTextMessage(user.name, message.text, message.senderImageUrl, message.receiverImageUrl, message.type )
                );
            }
            var insertedData = {
                senderId: message.senderId,
                senderName: message.senderName,
                receiverId: message.receiverId,
                receiverName: message.receiverName,
                chatId: message.chatID,
                type : message.type,
                message: message.text,
                readStatus: message.readStatus,
                answereStatus: message.answereStatus,
                senderImageUrl : message.senderImageUrl,
                receiverImageUrl : message.receiverImageUrl,
            };
        }
        else{
            if (user) {
                io.to(user.room).emit(
                    "newMessage",
                    generateFileMessage(user.name, message.fileText, message.senderImageUrl, message.receiverImageUrl, message.fileName, message.type )
                );
            }
            var insertedData = {
                senderId: message.senderId,
                senderName: message.senderName,
                receiverId: message.receiverId,
                receiverName: message.receiverName,
                chatId: message.chatID,
                fileName: message.fileName,
                type : message.type,
                fileText : message.fileText,
                readStatus: message.readStatus,
                answereStatus: message.answereStatus,
                senderImageUrl : message.senderImageUrl,
                receiverImageUrl : message.receiverImageUrl
            };
        }
        
        modal.create(insertedData, (err, data) => {
            if (err) {
                console.log(err);
            } else {
                console.log("inserted data successfully");
            }
        });
        console.log("New Message", message);
        callback();
    });

    socket.on("createLocationMessage", coords => {
        var user = users.getUser(socket.id);

        if (user) {
            io.to(user.room).emit(
                "newLocationMessage",
                generateLocationMessage(
                    user.name,
                    coords.latitude,
                    coords.longitude
                )
            );
        }
    });

    socket.on("disconnect", () => {
        var user = users.removeUser(socket.id);

        if (user) {
            io.to(user.room).emit(
                "updateUserList",
                users.getUserList(user.room)
            );
            io.to(user.room).emit("newMessage", generateTextMessage("", ``));
        }
        console.log("User Disconnected");
    });
});



// Access-Control-Allow-Origin
app.all("*", function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header(
        "Access-Control-Allow-Methods",
        "PUT, GET, POST, DELETE, OPTIONS"
    );
    res.header("Access-Control-Allow-Headers", "Content-Type,apikey");
    //Auth Each API Request created by user.
    next();
});
app.get("/updatemessage",(req,res) =>{
res.json({msg:"msg updated",id:req.query.id})
});
app.get("/inboxmessage", (req, res) => {
    modal.find(
        { senderId: req.query.senderId },
        (err, data) => {
            if (err) {
                console.log(err);
            } else {
                res.json(data);
            }
        }
    );
});
app.get("/conversation", (req, res)=>{
    
    modal.find({chatId : req.query.chatId, senderId: req.query.receiverId}, (err,data)=>{
        if(err){
            console.log(err);
        }else{
            //if(data.length>0){
            console.log(data);
            if(data){
                res.json(data);
                //res.json({status : true})
            }else{
                res.json({status : false});
            }
        }
        }).sort({_id: -1}).limit(1)
    })
app.get("/allmessage", (req, res) => {
    modal.find({ senderId: req.query.senderId }, (err, data) => {
        if (err) {
            console.log(err);
        } else {
            res.json(data);
        }
    });
});
app.get("/unanswered", (req, res) => {
    modal.find(
        { answereStatus: false, senderId: req.query.senderId },
        (err, data) => {
            if (err) {
                console.log(err);
            } else {
                res.json(data);
            }
        }
    );
});

app.get("/history", (req,res)=>{
    modal.find({ chatId: req.query.chatId }, (err, data) => {
        if (err) {
            console.log(err);
        } else {
            res.json(data);
        }
    });
    });
app.post("/insertChatRequest", (req,res)=>{
    var data = new modal(req.body);
    console.log("data", data);
    modal.create(data, (err, data)=>{
        if(err){
            console.log(err);
            res.json({status : false});
            }else{
                res.json({status : true});
            }
            })
    });
app.get("/deleteAll", (req,res)=>{
    modal.deleteMany({}, (err, data) => {
        if (err) {
            console.log(err);
        } else {
            console.log("deleted All");
        }
    });
    })
server.listen(9090, () => {
    console.log("Started on port 9090");
});
